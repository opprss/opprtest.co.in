<?php 
/*
Description: user vehicle verify parking employ.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
// require_once '../../connection-pdo.php';
require_once 'api/reg_api.php';
// require_once 'api/global_api.php';
$response = array();
if(isAvailable(array('mobile'))){

 	if(isEmpty(array('mobile'))){

	 	$mobile = trim($_POST['mobile']);
	 	$response= user_veh_prk_verify_active($mobile);
	 	echo ($response);

 	}else{
 			$response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response); 
	 }

 }else{
	 $response['status'] = 0; 
	 $response['message'] = 'Invalid API Call';
     echo json_encode($response); 
 } 
?>