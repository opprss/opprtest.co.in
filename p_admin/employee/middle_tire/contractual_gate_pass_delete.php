<?php 
/*
Description: Parking visiter in list
Developed by: Rakhal Raj Mandal
Created Date: 31-05-2018
Update date : 
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','prk_user_username','token','contractual_gate_pass_dtl_id'))){
    if(isEmpty(array('prk_admin_id','prk_area_user_id','prk_user_username','token','contractual_gate_pass_dtl_id'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $prk_user_username = trim($_POST['prk_user_username']);
        $token = trim($_POST['token']);
        $contractual_gate_pass_dtl_id = trim($_POST['contractual_gate_pass_dtl_id']);
        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            //call Common api
            $response = contractual_gate_pass_delete($prk_admin_id,$prk_user_username,$contractual_gate_pass_dtl_id);
        }else{

            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response); 
} 
echo $response;
?> 