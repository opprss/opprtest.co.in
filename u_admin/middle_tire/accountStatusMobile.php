<?php 
/*
Description: user accountstatus found.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
    include_once 'api/reg_api.php';
    // include_once 'api/global_api.php';
    $response = array();
    if(isAvailable(array('mobile'))){
        if(isEmpty(array('mobile'))){
            $mobile = trim($_POST['mobile']);
            $response= accountStatus($mobile);
            echo ($response);
        }else{
                $response['status'] = 0;
                $response['message'] = 'All Fields Are Mandatory';
                echo json_encode($response); 
        }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        echo json_encode($response); 
    } 
?> 