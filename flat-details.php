<?php
  include('global_asset/config.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>OPPRSS | Tower & Flat Details</title>
    <link rel="shortcut icon" href="<?php echo FAVICON ?>" type="image/x-icon">
    <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="lib/select2/css/select2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="lib/jquery/jquery.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <style type="text/css">
      /*#success_message{ display: none;}*/
      .select2 .select2-container .select2-container--default{
        width: 307px !important;
      }
      .select2 {
        width:100%!important;
      }
      .error{
        display: none;
        color:red;
        font-size: 12px;
        float: left;
        margin-left: 40px;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <form class="well form-horizontal" name="tower_flat_dtl_drft" method="post"  id="tower_flat_dtl_drft">
        <fieldset>
          <legend><center><h2><b>Tower & Flat Details</b></h2></center></legend><br>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group"> 
                <label class="col-md-4 control-label">Residential Area</label>
                <div class="col-md-8 selectContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                    <select class="form-control" name="prk_admin_id" id="prk_admin_id">
                      <option value="">Select Area</option>
                    </select>
                  </div>
                  <label id="prk_admin_id_error" class="error">This field is required.</label>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group"> 
                <label class="col-md-4 control-label">Tower Name</label>
                <div class="col-md-8 selectContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                    <select class="form-control" name="tower_id" id="tower_id">
                      <option value="">Select Tower</option>
                    </select>
                  </div>
                  <label id="tower_id_error" class="error">This field is required.</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group"> 
                <label class="col-md-4 control-label">Flate Name</label>
                <div class="col-md-8 selectContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                    <select name="department" class="form-control" id="flat_id">
                      <option value="">Select Flat</option>
                    </select>
                  </div>
                  <label id="flat_id_error" class="error">This field is required.</label>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md- col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-4 control-label">Owner Name</label>  
                <div class="col-md-8 inputGroupContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input  name="owner_name" id="owner_name" placeholder="Owner Name" class="form-control"  type="text">
                  </div>
                  <label id="owner_name_error" class="error">This field is required.</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group"> 
                <label class="col-md-4 control-label">Owner Gendar</label>
                <div class="col-md-8 selectContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                    <select name="department" class="form-control" id="owner_gender">
                      <option value="">Select Gender</option>
                      <option value="M">Male</option>
                      <option value="F">Female</option>
                    </select>
                  </div>
                  <label id="owner_gender_error" class="error">This field is required.</label>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-4 control-label" >Owner Mobile Number</label> 
                <div class="col-md-8 inputGroupContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                    <input name="owner_mobile" id="owner_mobile" placeholder="Owner Mobile Number" class="form-control"  type="text" maxlength="10">
                  </div>
                  <label id="owner_mobile_error" class="error">This field is required.</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-4 control-label">Owner E-Mail</label>  
                  <div class="col-md-8 inputGroupContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                    <input name="owner_email" id="owner_email" placeholder="Owner E-Mail Address" class="form-control"  type="text">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group"> 
                <label class="col-md-4 control-label">Living Status</label>
                <div class="col-md-8 selectContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                    <select name="department" class="form-control" id="living_status">
                      <option value="">Select Living</option>
                      <option value="O">Owner</option>
                      <option value="T">Tenant</option>
                    </select>
                  </div>
                  <label id="living_status_error" class="error">This field is required.</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-4 control-label">Tenant Name</label>  
                <div class="col-md-8 inputGroupContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input  name="tenant_name" id="tenant_name" placeholder="Tenant Name" class="form-control"  type="text">
                  </div>
                  <label id="tenant_name_error" class="error">This field is required.</label>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group"> 
                <label class="col-md-4 control-label">Tenant Gendar</label>
                <div class="col-md-8 selectContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                    <select name="department" class="form-control" id="tenant_gender">
                      <option value="">Select Gender</option>
                      <option value="M">Male</option>
                      <option value="F">Female</option>
                    </select>
                  </div>
                  <label id="tenant_gender_error" class="error">This field is required.</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-4 control-label">Tenant Mobile Number</label>  
                <div class="col-md-8 inputGroupContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                    <input  name="tenant_mobile" id="tenant_mobile" placeholder="Tenant Mobile Number" class="form-control"  type="text" maxlength="10">
                  </div>
                  <label id="tenant_mobile_error" class="error">This field is required.</label>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-4 control-label">Tenant E-Mail</label>  
                <div class="col-md-8 inputGroupContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                    <input  name="tenant_email" id="tenant_email" placeholder="Tenant E-Mail Address" class="form-control"  type="text">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-4 control-label">Four Wheeler 1</label>  
                <div class="col-md-8 inputGroupContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-car" aria-hidden="true"></i></span>
                    <input  name="four_wheeler_1" id="four_wheeler_1" placeholder="Four Wheeler 1" class="form-control"  type="text">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-4 control-label">Four Wheeler 2</label>  
                <div class="col-md-8 inputGroupContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-car" aria-hidden="true"></i></span>
                    <input  name="four_wheeler_2" id="four_wheeler_2" placeholder="Four Wheeler 2" class="form-control"  type="text">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-4 control-label">Four Wheeler 3</label>  
                <div class="col-md-8 inputGroupContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-car" aria-hidden="true"></i></span>
                    <input  name="four_wheeler_3" id="four_wheeler_3" placeholder="Four Wheeler 3" class="form-control"  type="text">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-4 control-label">Tow Wheeler 1</label>  
                <div class="col-md-8 inputGroupContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-motorcycle" aria-hidden="true"></i></span>
                    <input  name="tow_wheeler_1" id="tow_wheeler_1" placeholder="Tow Wheeler 1" class="form-control"  type="text">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-4 control-label">Tow Wheeler 2</label>  
                <div class="col-md-8 inputGroupContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-motorcycle" aria-hidden="true"></i></span>
                    <input  name="tow_wheeler_2" id="tow_wheeler_2" placeholder="Tow Wheeler 2" class="form-control"  type="text">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-4 control-label">Tow Wheeler 3</label>  
                <div class="col-md-8 inputGroupContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-motorcycle" aria-hidden="true"></i></span>
                    <input  name="tow_wheeler_3" id="tow_wheeler_3" placeholder="Tow Wheeler 3" class="form-control"  type="text">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-4 control-label">Parking Space 1</label>  
                <div class="col-md-8 inputGroupContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-stop-circle-o" aria-hidden="true"></i></span>
                    <input  name="parking_space_1" id="parking_space_1" placeholder="Parking Space 1" class="form-control"  type="text">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-4 control-label">Parking Space 2</label>  
                <div class="col-md-8 inputGroupContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-stop-circle-o" aria-hidden="true"></i></span>
                    <input  name="parking_space_2" id="parking_space_2" placeholder="Parking Space 2" class="form-control"  type="text">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-4 control-label">Parking Space 3</label>  
                <div class="col-md-8 inputGroupContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-stop-circle-o" aria-hidden="true"></i></span>
                    <input  name="parking_space_3" id="parking_space_3" placeholder="Parking Space 3" class="form-control"  type="text">
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <div class="alert alert-success" role="alert" id="success_message">Submit Successfully <i class="glyphicon glyphicon-thumbs-up"></i> !.</div>
              </div>
            </div> -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top: -20px;">
              <div class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-8"><br>
                  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="button" id="save_data" class="btn btn-warning" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSUBMIT <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                </div>
              </div>
            </div>
          </div>
        </fieldset>
      </form>
    </div>
  </body>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
  <script src="lib/validation/jquery.validate.js"></script>
  <script src="lib/select2/js/select2.full.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('select').select2();
    });
  </script>
  <script type="text/javascript">
    var userUrl = "<?php echo USER_URL; ?>";
    var prkUrl = "<?php echo PRK_URL; ?>";
    var urlApartMentList = userUrl+'prk_area_list.php';
    // 'user_admin_id','token','state','city'
    $.ajax ({
      type: 'POST',
      url: urlApartMentList,
      data: {
        'user_admin_id':'',
        'token':'',
        'state':'',
        'city':''
      },
      success : function(data) {
        // alert(data);
        var obj=JSON.parse(data);
        var areaOption = "<option value=''>Select Area</option>";
        $.each(obj['prk_area'], function(val, key) {
          if( key['prk_admin_id']!='000')
          areaOption += '<option value="' + key['prk_admin_id'] + '">' + key['prk_area_name'] + '</option>';  
        });
        $("#prk_admin_id").html(areaOption);
        $('#prk_admin_id').select2();
      }
    });
    $('#prk_admin_id').on("change", function() {
      var id=($("#prk_admin_id").val());
      var urlTowerList = prkUrl+'tower_list.php';
      // 'prk_admin_id','parking_admin_name','token'
      $.ajax ({
        type: 'POST',
        url: urlTowerList,
        data: {
          'prk_admin_id':id,
          'parking_admin_name':'',
          'token':''
        },
        success : function(data) {
          //alert(data);
          var obj=JSON.parse(data);
          //console.log(obj);
          var areaOption = "<option value=''>Select Tower</option>";
          $.each(obj['tower_list'], function(val, key) {
            areaOption += '<option value="' + key['tower_id'] + '">' + key['tower_name'] + '</option>'
          });
          $("#tower_id").html(areaOption);
          $("#tower_id").select2();
        }
      });
    });
    $('#tower_id').on("change", function() {
      var prk_admin_id=($("#prk_admin_id").val());
      var id=($("#tower_id").val());
      var urlFlatList = prkUrl+'flat_list.php';
      $.ajax ({
        type: 'POST',
        url: urlFlatList,
        data: {
          tower_id:id,
          prk_admin_id:prk_admin_id,
          parking_admin_name:'',
          token:''
        },
        success : function(data) {
          // alert(data);
          var obj=JSON.parse(data);
          var areaOption = "<option value=''>Select Flat</option>";
          if (obj['status']) {
            $.each(obj['flat_list'], function(val, key) {
              areaOption += '<option value="' + key['flat_id'] + '">' + key['flat_name'] + '</option>'
            });
          }
          $("#flat_id").html(areaOption);
          $("#flat_id").select2();
        }
      });
    });
    $('#save_data').on('click', function(){
      if(prk_admin_id_fun() & tower_id_fun() & flat_id_fun() & owner_name_fun() & owner_gender_fun() & owner_mobile_fun() & living_status_fun() & tenant_name_fun() & tenant_gender_fun() & tenant_mobile_fun()){
        var inserted_by = 'NA';
        var prk_admin_id = $('#prk_admin_id').val();
        var tower_id = $('#tower_id').val();
        var flat_id = $('#flat_id').val();
        var owner_name = $('#owner_name').val();
        var owner_mobile = $('#owner_mobile').val();
        var owner_email = $('#owner_email').val();
        var owner_gender = $('#owner_gender').val();
        // var owner_gender = 'M';
        var living_status = $('#living_status').val();
        var tenant_name = $('#tenant_name').val();
        var tenant_mobile = $('#tenant_mobile').val();
        var tenant_email = $('#tenant_email').val();
        var tenant_gender = $('#tenant_gender').val();
        // var tenant_gender = 'M';
        var four_wheeler_1 = $('#four_wheeler_1').val();
        var four_wheeler_2 = $('#four_wheeler_2').val();
        var four_wheeler_3 = $('#four_wheeler_3').val();
        var tow_wheeler_1 = $('#tow_wheeler_1').val();
        var tow_wheeler_2 = $('#tow_wheeler_2').val();
        var tow_wheeler_3 = $('#tow_wheeler_3').val();
        var parking_space_1 = $('#parking_space_1').val();
        var parking_space_2 = $('#parking_space_2').val();
        var parking_space_3 = $('#parking_space_3').val();
        var urlTowerFlatDtl = prkUrl+'tower_flat_dtl_draft_add.php';
        // 'prk_admin_id','inserted_by','tower_id','flat_id','owner_name','owner_mobile','owner_email','owner_gender','living_status','tenant_name','tenant_mobile','tenant_email','tenant_gender','four_wheeler_1','four_wheeler_2','four_wheeler_3','tow_wheeler_1','tow_wheeler_2','tow_wheeler_3','parking_space_1','parking_space_2','parking_space_3'
        $.ajax({
          url :urlTowerFlatDtl,
          type:'POST',
          data :{
            'prk_admin_id':prk_admin_id,
            'inserted_by':inserted_by,
            'tower_id':tower_id,
            'flat_id':flat_id,
            'owner_name':owner_name,
            'owner_mobile':owner_mobile,
            'owner_email':owner_email,
            'owner_gender':owner_gender,
            'living_status':living_status,
            'tenant_name':tenant_name,
            'tenant_mobile':tenant_mobile,
            'tenant_email':tenant_email,
            'tenant_gender':tenant_gender,
            'four_wheeler_1':four_wheeler_1,
            'four_wheeler_2':four_wheeler_2,
            'four_wheeler_3':four_wheeler_3,
            'tow_wheeler_1':tow_wheeler_1,
            'tow_wheeler_2':tow_wheeler_2,
            'tow_wheeler_3':tow_wheeler_3,
            'parking_space_1':parking_space_1,
            'parking_space_2':parking_space_2,
            'parking_space_3':parking_space_3
          },
          dataType:'html',
          success  :function(data){
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.9em;'>Added Successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                    location.reload(true);
                  }
                }
              });
            }else{
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.9em;'>"+json.message+"</p>",
                type: 'red'
              });
            }
          }
        });
        // alert('Working !');
      }
    });
  </script>
  <script type="text/javascript">
    function prk_admin_id_fun(){
      // alert('OK');
      var prk_admin_id = $('#prk_admin_id').val();
      if (prk_admin_id=='') {
        $('#prk_admin_id_error').show();
        return false;
      }else{
        $('#prk_admin_id_error').hide();
        return true;
      }
    }
    $("#prk_admin_id").change(function(){          
      prk_admin_id_fun();        
    });
    function tower_id_fun(){
      // alert('OK');
      var tower_id = $('#tower_id').val();
      if (tower_id=='') {
        $('#tower_id_error').show();
        return false;
      }else{
        $('#tower_id_error').hide();
        return true;
      }
    }
    $("#tower_id").change(function(){          
      tower_id_fun();        
    });
    function flat_id_fun(){
      // alert('OK');
      var flat_id = $('#flat_id').val();
      if (flat_id=='') {
        $('#flat_id_error').show();
        return false;
      }else{
        $('#flat_id_error').hide();
        return true;
      }
    }
    $("#flat_id").change(function(){          
      flat_id_fun();        
    });
    function owner_name_fun(){
      // alert('OK');
      var owner_name = $('#owner_name').val();
      if (owner_name=='') {
        $('#owner_name_error').show();
        return false;
      }else{
        $('#owner_name_error').hide();
        return true;
      }
    }
    $("#owner_name").keyup(function(){          
      owner_name_fun();        
    });
    function owner_gender_fun(){
      // alert('OK');
      var owner_gender = $('#owner_gender').val();
      if (owner_gender=='') {
        $('#owner_gender_error').show();
        return false;
      }else{
        $('#owner_gender_error').hide();
        return true;
      }
    }
    $("#owner_gender").change(function(){          
      owner_gender_fun();        
    });
    function owner_mobile_fun(){
      // alert('OK');
      var owner_mobile = $('#owner_mobile').val();
      if (owner_mobile=='') {
        $('#owner_mobile_error').show();
        return false;
      }else{
        $('#owner_mobile_error').hide();
        return true;
      }
    }
    $("#owner_mobile").keyup(function(){          
      owner_mobile_fun();        
    });
    function living_status_fun(){
      // alert('OK');
      var living_status = $('#living_status').val();
      if (living_status=='') {
        $('#living_status_error').show();
        return false;
      }else{
        $('#living_status_error').hide();
        return true;
      }
    }
    $("#living_status").change(function(){          
      living_status_fun();        
    });
    function tenant_name_fun(){
      // alert('OK');
      var tenant_name = $('#tenant_name').val();
      var living_status = $('#living_status').val();
      if (living_status=='T') {
        if (tenant_name=='') {
          $('#tenant_name_error').show();
          return false;
        }else{
          $('#tenant_name_error').hide();
          return true;
        }
      }else{
        $('#tenant_name_error').hide();
        return true;
      }
    }
    $("#tenant_name").keyup(function(){          
      tenant_name_fun();        
    });
    function tenant_gender_fun(){
      // alert('OK');
      var tenant_gender = $('#tenant_gender').val();
      var living_status = $('#living_status').val();
      if (living_status=='T') {
        if (tenant_gender=='') {
          $('#tenant_gender_error').show();
          return false;
        }else{
          $('#tenant_gender_error').hide();
          return true;
        }
      }else{
        $('#tenant_gender_error').hide();
        return true;
      }
    }
    $("#tenant_gender").change(function(){          
      tenant_gender_fun();        
    });
    function tenant_mobile_fun(){
      // alert('OK');
      var tenant_mobile = $('#tenant_mobile').val();
      var living_status = $('#living_status').val();
      if (living_status=='T') {
        if (tenant_mobile=='') {
          $('#tenant_mobile_error').show();
          return false;
        }else{
          $('#tenant_mobile_error').hide();
          return true;
        }
      }else{
        $('#tenant_mobile_error').hide();
        return true;
      }
    }
    $("#tenant_mobile").keyup(function(){          
      tenant_mobile_fun();        
    });
  </script>
</html>
