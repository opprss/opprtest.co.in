<?php
session_start();
include('../global_asset/config.php');
require_once '../global_api/global_api.php';
require_once 'middle_tire/api/reg_api.php';
include 'middle_tire/api/session_api.php';

if(isset($_SESSION['user_admin_id']) && isset($_SESSION['token'])){
	//header('location:../signin.php');
	$response = user_logout($_SESSION['token']);
	$json = json_decode($response);

	if ($json->status) {
		session_destroy();
		header('location: ../signin');
	}else{
		header('location: vehicle-list');
	}
}else{
	header('location: ../signin');
}

?>