<?php
/*
Description: user vehicle list.
Developed by: Rakhal Raj Mandal
Created Date: 30-03-2018
Update date :08-05-2018
*/
    //require_once 'api/global_api.php';
    require_once 'api/reg_api.php';
    if(isAvailable(array('user_admin_id','vehicle','token'))){
 	  if(isEmpty(array('user_admin_id','token'))){
		 	$user_admin_id = trim($_POST['user_admin_id']);
		 	$vehicle = trim($_POST['vehicle']);

		 	$token = ($_POST['token']);
			$resp=user_token_check($user_admin_id,$token);
			$json = json_decode($resp);
			if($json->status=='1'){
				$respo=vehicleList($user_admin_id,$vehicle);
	        	$response = json_encode($respo);
			}else{
				$response = $resp;
			}		 	
	 	}else{
 			$response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
	 		$response = json_encode($response);
	     }
	}else{
		 $response['status'] = 0; 
		 $response['message'] = 'Invalid API Call';
		 $response = json_encode($response);
	}
	echo $response;
?>