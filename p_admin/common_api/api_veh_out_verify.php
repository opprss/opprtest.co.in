<?php
/*
Description: Parking gate pass delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function veh_out_verify($prk_admin_id,$prk_veh_trc_dtl_id){
    global $pdoconn;
    $response = array();
    $sql ="UPDATE `prk_veh_trc_dtl` SET `veh_out_verify_flag`='Y' WHERE `prk_veh_trc_dtl_id`='$prk_veh_trc_dtl_id' and `prk_admin_id`='$prk_admin_id'";

    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Vehicle Out Verify Sucessfull';
         $response = json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Vehicle Out Verify Not Sucessfull';
         $response = json_encode($response);
    }
    return $response;
}
?>