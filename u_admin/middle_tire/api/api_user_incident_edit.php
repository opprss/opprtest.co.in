<?php
/*
Description: vehicle in parking area
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_incident_edit($user_incident_id,$user_incident_name,$user_incident_type,$user_incident_dtl,$user_incident_number,$prk_admin_id,$user_admin_id,$mobile){
    global $pdoconn;
    $response = array();
    $sql = "UPDATE `user_incident` SET `updated_by`='$mobile',`updated_date`='".TIME."',`active_flag`='".FLAG_N."' WHERE `user_incident_id`='$user_incident_id'";
    $query = $pdoconn->prepare($sql); 
    if($query->execute()){
        $response = user_incident_in($user_incident_name,$user_incident_type,$user_incident_dtl,$user_incident_number,$prk_admin_id,$user_admin_id,$mobile);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
        $response = json_encode($response); 
    }   
    return $response; 

}
?>