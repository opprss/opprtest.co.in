<?php include "all_nav/header.php";  ?>
 <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Feedback</h5>
        <form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form><!-- search-bar-->
      </div><!-- am-pagetitle -->
  

        <!-- <div class="am-mainpanel"> -->
      <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title" style="text-align: center;font-size: 30px">How to Best User  Feedback</h6>
         <br>

          <div class="row mg-b-20">
            <div class="col-md">
              <div class="card card-body bg-gray-200">
                <p class="card-text">If you’re an User, find out how you really feel with our easy-to-use online user feedback form.Our form helps you learn what your employees like--and don’t like--about working for your company so you can create a better work atmosphere.</p>
              </div><!-- card -->
            </div><!-- col -->
            <div class="col-md mg-t-20 mg-md-t-0">
              <div class="card card-body bg-primary tx-white bd-0">
                <p class="card-text">
                  <b>Everyone can be evaluated</b><br>
                Even you.Evaluations go both ways. Give your staff the opportunity to evaluate anyone in the organization: management, team members, even C-level executives. When you open the door to honest conversation, you’ll help your Carrer feel valued and heard.</p>
              </div><!-- card -->
            </div><!-- col -->
            <div class="col-md mg-t-20 mg-md-t-0">
              <div class="card card-body bg-dark tx-white bd-0">
                <p class="card-text">
                  <b>Exit interviews</b><br>
Asking employees why they’re leaving will give you a whole different level of insight into your company’s hiring, management, and benefits policies. Merely asking why they’re leaving isn’t necessarily enough; ask them questions such as, “How much room for professional growth did you have at this company?” or even, “How easy was it to balance your work life and personal life while working at this company?” to get opinions on a wide variety of subjects.
                  <br>
                </p>
              </div><!-- card -->
            </div><!-- col -->
          </div><!-- row -->
        </div><!-- card -->

        <div class="card pd-20 pd-sm-40 mg-t-50">
          <h6 class="card-body-title">User rating scale </h6>
          <p class="mg-b-20 mg-sm-b-30">
            <label>
  <input type="radio" name="a" value="1" class="st"><img src="images/blank.png" id="st-1"></input>
</label>
<label>
  <input type="radio" name="a" value="2" class="st"><img src="images/blank.png" id="st-2"></input>
</label>
<label>
  <input type="radio" name="a" value="3" class="st"><img src="images/blank.png" id="st-3"></input>
</label>
<label>
  <input type="radio" name="a" value="4" class="st"><img src="images/blank.png" id="st-4"></input>
</label>
<label>
  <input type="radio" name="a" value="5" class="st"><img src="images/blank.png" id="st-5" style=" min-height:5px ; max-width: 10;"></input>
</label>

          </p>
         <b>Type your comment</b> 
   <textarea rows="3" class="form-control" placeholder="Input Message" name="message" id="message"></textarea >




</div>
<?php include"all_nav/footer.php"; ?>
<script type="text/javascript">
    $(document).ready(function(){
      $('.st').click(function(){
      var a=$(this).val();
      for(var i=1;i<=a ; i++){
        var ad="st-"+i;
        $("#"+ad).attr("src","images/full.png");
      }
        for(var f=5;f>a ; f--){
        var ad="st-"+f;
        $("#"+ad).attr("src","images/blank.png");
      }


      });

    });
  </script>
  <style>
      label>input {
        visibility: hidden;
        position: absolute;
      }
      label>input+img{
        cursor: pointer;
        border:2px solid transparent;
      }
   label>input>img {
  max-width: 10;
}

  </style>



