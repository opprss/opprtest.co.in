<?php
/*
Description: user address show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_guest_list_show($user_guest_list_id,$user_admin_id,$mobile){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql ="SELECT `user_guest_list_id`,`guest_name`, `guest_mobile`, `guest_gender`, `guest_address`, `guest_purpose`, `no_of_guest`, `guest_id_proof`, `user_admin_id`, `prk_admin_id`, `in_date`, `to_time`, `form_time`, `guest_in_verify`, `guest_out_verify`  FROM `user_guest_list` WHERE `active_flag`='".FLAG_Y."' AND  `del_flag`='".FLAG_N."' AND `user_guest_list_id`='$user_guest_list_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['user_guest_list_id'] = $val['user_guest_list_id'];
        $response['guest_name'] = $val['guest_name'];
        $response['guest_mobile'] = $val['guest_mobile'];
        $response['guest_gender'] = $val['guest_gender'];
        $response['guest_address'] = $val['guest_address'];
        $response['guest_purpose'] = $val['guest_purpose'];
        $response['no_of_guest'] = $val['no_of_guest'];
        $response['guest_id_proof'] = $val['guest_id_proof'];
        $response['user_admin_id'] = $val['user_admin_id'];
        $response['prk_admin_id'] = $val['prk_admin_id'];
        $response['in_date'] = $val['in_date'];
        $response['to_time'] = $val['to_time'];
        $response['form_time'] = $val['form_time'];
        // $response['form_date'] = $val['form_date'];
        // $response['to_date'] = $val['to_date'];
        $response['guest_in_verify'] = $val['guest_in_verify'];
        $response['guest_out_verify'] = $val['guest_out_verify'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>