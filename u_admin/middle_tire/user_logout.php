<?php 
/*
Description: user logout.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
// require_once '../../connection-pdo.php';
require_once 'api/reg_api.php';
// require_once 'api/global_api.php';
 
$response = array();
 
if(isAvailable(array('token'))){
 	if(isEmpty(array('token'))){
	 	$token = trim($_POST['token']);
	 	$response= user_logout($token);
	 	echo ($response);
 	}else{
		$response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        echo json_encode($response); 
	}

}else{
	$response['status'] = 0; 
	$response['message'] = 'Invalid API Call';
	echo json_encode($response); 
} 
?>