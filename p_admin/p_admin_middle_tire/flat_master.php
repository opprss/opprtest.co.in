<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','tower_name','flat_name','owner_name','living_status','tenant_name','effective_date','end_date','parking_admin_name','token','crud_type','flat_master_id','two_wh_spa_allw','four_wh_spa_allw','two_wh_spa','four_wh_spa','contact_number','living_name'))){
    if(isEmpty(array('prk_admin_id','parking_admin_name','token','crud_type'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
        $tower_id = trim($_POST['tower_name']);
        $flat_id = trim($_POST['flat_name']);
        $owner_id = trim($_POST['owner_name']);
        $living_status = trim($_POST['living_status']);
        $tenant_id = trim($_POST['tenant_name']);
        $effective_date = trim($_POST['effective_date']);
        $end_date = trim($_POST['end_date']);
        $inserted_by = trim($_POST['parking_admin_name']);
        $token = trim($_POST['token']);
        $crud_type = trim($_POST['crud_type']);
        $flat_master_id = trim($_POST['flat_master_id']);
        $two_wh_spa_allw = trim($_POST['two_wh_spa_allw']);
        $two_wh_spa = trim($_POST['two_wh_spa']);
        $four_wh_spa_allw = trim($_POST['four_wh_spa_allw']);
        $four_wh_spa = trim($_POST['four_wh_spa']);
        $contact_number = trim($_POST['contact_number']);
        $living_name = trim($_POST['living_name']);
        $app_use = isset($_POST['app_use']) ? trim($_POST['app_use']) : 'N'; 
        $res_mem_id = isset($_POST['res_mem_id']) ? trim($_POST['res_mem_id']) : ''; 
        $resp=prk_token_check($prk_admin_id,$token);
            $json = json_decode($resp);
            if($json->status){
                $response = flat_master($prk_admin_id,$tower_id,$flat_id,$owner_id,$living_status,$tenant_id,$effective_date,$end_date,$inserted_by,$crud_type,$flat_master_id,$two_wh_spa_allw,$four_wh_spa_allw,$two_wh_spa,$four_wh_spa,$contact_number,$living_name,$app_use);
            }else{
                $response = $resp;
            }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>