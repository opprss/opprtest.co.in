<?php
/*
Description: Parking adimn Address save.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
    // require_once 'api/add_api.php';
    // require_once 'api/global_api.php';
    require_once 'api/parkAreaReg_api.php';

    $response = array();
 
    if(isAvailable(array('address','lendmark','area','state','city','pin'))){

        if(isEmpty(array('address','lendmark','area','state','city','pin'))){
            $address = trim($_POST['address']);
            $lendmark = trim($_POST['lendmark']);
            $area = trim($_POST['area']);
            $state = trim($_POST['state']);
            $city = trim($_POST['city']);
            $pin = trim($_POST['pin']);
            $respon = address($address,$lendmark,$area,$state,$city,$pin);
            $json = json_decode($respon);
            if ($json->status == 1) {
                 echo "true";
             }else{
                echo "false";
            }
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
        }
    }else{
         $response['status'] = 0; 
         $response['message'] = 'Invalid API Call';
         echo json_encode($response);
     }
?>