<?php 
/*
Description: user address show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
require_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_admin_id','mobile','token'))){
 	if(isEmpty(array('user_admin_id','mobile','token'))){
	 	$user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
        $token = trim($_POST['token']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        $prk_area_user_id=$oth1=$oth2='';
        if($json->status=='1'){
            $prk_admin_id ='N'; 
            $sql = "SELECT `prk_admin_id` FROM `user_address` WHERE `user_admin_id`='$user_admin_id' AND `default_flag`='".FLAG_Y."' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $prk_admin_id = $val['prk_admin_id'];
            }
            $response = emergency_list($prk_admin_id,$user_admin_id,$prk_area_user_id,$oth1,$oth2);
        }else{
            $response = $resp; 
        }
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response =  json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>