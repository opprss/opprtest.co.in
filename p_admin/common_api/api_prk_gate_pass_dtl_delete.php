<?php
/*
Description: Parking gate pass delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_gate_pass_dtl_delete($prk_gate_pass_id,$prk_admin_id,$user_name){
    global $pdoconn;
    $response = array();
    //date_default_timezone_set('Asia/Kolkata');
    //$time=date("Y-m-d h:i:sa");
    $sql ="UPDATE `prk_gate_pass_dtl` SET `del_flag`='".FLAG_Y."', `updated_date`='".TIME."',`updated_by`='$user_name' WHERE `prk_admin_id`='$prk_admin_id' AND `prk_gate_pass_id`='$prk_gate_pass_id' AND `del_flag`='".FLAG_N."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Delete Sucessfull';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Delete Not Sucessfull';
        return json_encode($response);
    }
}
?>