<?php
/*
Description: Parking area vehicle rate delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function online_payment_dtl_add($payment_company,$payment_for,$transaction_by,$transaction_id_for,$status,$checksumhash,$mid,$orderid,$bankname,$txnamount,$txndate,$txnid,$respcode,$paymentmode,$banktxnid,$currency,$gatewayname,$respmsg,$inserted_by){
    global $pdoconn;
    $response = array();
    $online_payment_dtl_id=uniqid();
    $sql = "INSERT INTO `online_payment_dtl`(`online_payment_dtl_id`, `payment_company`, `payment_for`, `transaction_by`, `transaction_id_for`, `status`, `checksumhash`, `mid`, `orderid`, `bankname`, `txnamount`, `txndate`, `txnid`, `respcode`, `paymentmode`, `banktxnid`, `currency`, `gatewayname`, `respmsg`, `inserted_by`, `inserted_date`) VALUES ('$online_payment_dtl_id','$payment_company','$payment_for','$transaction_by','$transaction_id_for','$status','$checksumhash','$mid','$orderid','$bankname','$txnamount','$txndate','$txnid','$respcode','$paymentmode','$banktxnid','$currency','$gatewayname','$respmsg','$inserted_by','".TIME."')";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Sucessfull';
        $response['online_payment_dtl_id'] = $online_payment_dtl_id;
    }else{
        $response['status'] = 0;
        $response['message'] = 'Unsucessfull';
    }
    return json_encode($response);
}
?>