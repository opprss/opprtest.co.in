<!-- ################################################
  
Description: Tower detalis.
Developed by: Rakhal Raj 
Created Date: 12-03-2018

 ####################################################-->
<?php include "all_nav/header.php";?>
<?php
$crud_type='LS';
$tenant_details=tenant_details($prk_admin_id,'','','','','','','','','','','','','',$parking_admin_name,$crud_type);
$tenant_details_list = json_decode($tenant_details, true);
?>
<!-- header position -->
  <style type="text/css">
    .size{
      font-size: 11px;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .error{
      font-size: 11px;
      color: red;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .success{
      font-size: 11px;
      color: green;
    }
  </style>
  <!-- links for datatable -->
  <link href="../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
    <script src="../js/webcam.min.js"></script>
  
    <div class="am-mainpanel">

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Tenant Info<//?=$tenant_details?></h5>
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40" >
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form id="park_tenant">
              <div class="row">
                <div class="col-lg-4">
                  <div class="form-group mg-t-20">
                    <input type="file" class="form-control"  id="file_path" name="file_path">
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">EFFECTIVE DATE<span class="tx-danger">*</span></label>
                    <input type="text" name="effective_date" id="effective_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                    <span style="position: absolute; display: none;" id="error_effective_date" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">END DATE</label>
                    <input type="text" name="end_date" id="end_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                   <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group mg-t-30">
                    <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group mg-t-30">
                    <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- data table -->
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40">
          <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th >Sl No.</th>
                  <th >Tenant Name</th>
                  <th >Mobile Number</th>
                  <th >Email Id</th>
                  <th >Effective Date</th>
                  <th >End Date</th>
                  <th >Action</th>
                </tr>
              </thead>
                <?php 
                if($tenant_details_list['status']){$c=0;
                  foreach($tenant_details_list['tenant_list'] as $value){$c+=1;?>
                    <tr>
                    <td ><?=$c;?></td>
                    <td ><?=$value['tenant_name'];?></td>
                    <td ><?=$value['tenant_mobile'];?></td>
                    <td ><?=$value['tenant_email'];?></td>
                    <td ><?=$value['effective_date'];?></td>
                    <td ><?=$value['end_date'];?></td>
                    <td >
                      <a href="<?php echo 'tenant?list_id='.base64_encode($value['tenant_id']);?>" class="pd-l-10" data-toggle="tooltip" data-placement="top" title="Modify"><i class="fa fa-pencil" style="font-size:18px; font-size:18px;"></i></a>
                    </td>
                    </tr>
                  <?php }}?>
            </table>
          </div>
        </div>
      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#effective_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
        if (date < now) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        return {};
      } 
    });
  });
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#effective_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
        if (date < now_end) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
        }
        return {};
      } 
    });
  });
  $('#effective_date').focus(function(){
    $('#end_date').val('');
  });
</script>
<!-- scripts for datatable -->
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  // alert(prkUrl);
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
</script>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE ?>";
  var prk_admin_id="<?=$prk_admin_id?>"
  var parking_admin_name="<?=$parking_admin_name?>"
  var token="<?=$token?>"
  var urlPrkAreGateDtl = prkUrl+'tenant_details.php';
  $('#save').click(function(){
    var effective_date=($("#effective_date").val());
    var end_date=($("#end_date").val());
    var str_array = end_date.split('-');
    var dd = str_array[0];
    var mm = str_array[1];
    var yy = str_array[2];
    var end_dat = yy+'-'+mm+'-'+dd;
    end_date = (!end_date == '') ? end_dat : FUTURE_DATE;
    var str_array = effective_date.split('-');
    var dd = str_array[0];
    var mm = str_array[1];
    var yy = str_array[2];
    var effective_dat = yy+'-'+mm+'-'+dd;
    var file_data = $('#file_path').prop('files')[0];
    // var tenant_name=($("#tenant_name").val());
    // var tenant_mobile=($("#tenant_mobile").val());
    // var tenant_email=($("#tenant_email").val());
    // var tenant_gender =  $('input[name=tenant_gender]:checked').val();
    // var id_name=($("#id_name").val());
    // var id_number=($("#id_number").val());
    // var no_of_family=($("#no_of_family").val());
    // var tenant_agrement_copy = $('#tenant_agrement_copy').prop('files')[0];
    // var tenant_image=$('#tenant_image').val();
    // var id_image=$('#id_image').val();
    // var form_data = new FormData();  
    // form_data.append('prk_admin_id', prk_admin_id);
    // form_data.append('tenant_name', tenant_name);
    // form_data.append('tenant_mobile', tenant_mobile);
    // form_data.append('tenant_email', tenant_email);
    // form_data.append('tenant_gender', tenant_gender);
    // form_data.append('id_name', id_name);
    // form_data.append('id_number', id_number);
    // form_data.append('no_of_family', no_of_family);
    // form_data.append('parking_admin_name', parking_admin_name);
    // form_data.append('effective_date', effective_dat);
    // form_data.append('end_date', end_date);
    // form_data.append('tenant_id', '');
    // form_data.append('crud_type', 'I');
    // form_data.append('tenant_agrement_copy', tenant_agrement_copy);
    // form_data.append('token', token);
    // form_data.append('tenant_image', tenant_image);
    // form_data.append('id_image', id_image);

    var form_data = new FormData();  
    form_data.append('prk_admin_id', prk_admin_id);
    form_data.append('parking_admin_name', parking_admin_name);
    form_data.append('token', token);
    form_data.append('effective_date', effective_dat);
    form_data.append('end_date', end_date);
    form_data.append('crud_type', 'I');
    form_data.append('file', file_data);
    if($("#park_tenant").valid()){
      // alert('ok');
      var urlPrkAreGateDtlTeEx = prkUrl+'tenant_details_excel.php';
      $('#save').val('Wait ...').prop('disabled', true);
      $.ajax({
        url :urlPrkAreGateDtlTeEx,
        type:'POST',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        dataType:'html',
        success  :function(data){
          $('#save').val('SAVE').prop('disabled', false);
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>Tenant Details Added Successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                    location.reload(true);
                }
              }
            });
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>Something went wrong</p>",
              type: 'red'
            });
            }
          }
        }
      });
    }
  });
  function tower_delete(flat_id){
    // alert(tower_id);
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the Flat permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            if (flat_id != '') {
              $.ajax({
                url :urlPrkAreGateDtl,
                type:'POST',
                data :{
                  'prk_admin_id':prk_admin_id,
                  'tower_name':'',
                  'flat_name':'',
                  'effective_date':'',
                  'end_date':'',
                  'parking_admin_name':parking_admin_name,
                  'flat_id':flat_id,
                  'crud_type':'D',
                  'token': token
                },
                dataType:'html',
                success  :function(data)
                {
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Tenant Details Deleted Successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                          location.reload(true);
                        }
                      }
                    });
                  }else{
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.7em;'>Something went wrong 1</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Something went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
</script>
<script type="text/javascript">
  $( document ).ready( function () {
    $( "#park_tenant" ).validate({
      rules: {
        effective_date: "required",
        // end_date: "required",
        file_path: "required"
      },
      messages: {
      }
    });
  });
</script>