<?php 
/*
Description: Parking visiter in list
Developed by: Rakhal Raj Mandal
Created Date: 31-05-2018
Update date : 
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('visitor_gate_pass_dtl_id','prk_admin_id','user_name'))){
    if(isEmpty(array('visitor_gate_pass_dtl_id','prk_admin_id','user_name'))){            
        $visitor_gate_pass_dtl_id = trim($_POST['visitor_gate_pass_dtl_id']);
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $user_name = trim($_POST['user_name']);
        //call global
        $response = visitor_gate_pass_delete($prk_admin_id,$visitor_gate_pass_dtl_id,$user_name);
     
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response); 
} 
echo $response;
?> 