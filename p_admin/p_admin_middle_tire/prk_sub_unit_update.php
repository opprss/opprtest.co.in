<?php 
/*
Description: Parking area sub unit update.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('prk_sub_unit_name','prk_sub_unit_short_name','prk_sub_unit_address','prk_sub_unit_rep_name','prk_sub_unit_rep_mob_no','prk_sub_unit_rep_email','prk_sub_unit_eff_date','prk_sub_unit_end_date','user_name','prk_admin_id','prk_sub_unit_id','token'))){
 	if(isEmpty(array('prk_sub_unit_name','prk_sub_unit_short_name','prk_sub_unit_address','prk_sub_unit_rep_name','prk_sub_unit_rep_mob_no','prk_sub_unit_rep_email','prk_sub_unit_eff_date','prk_sub_unit_end_date','user_name','prk_admin_id','prk_sub_unit_id','token'))){

	 	$prk_sub_unit_name = $_POST['prk_sub_unit_name'];
	 	$prk_sub_unit_short_name = $_POST['prk_sub_unit_short_name'];
	 	$prk_sub_unit_address = $_POST['prk_sub_unit_address'];
	 	$prk_sub_unit_rep_name = $_POST['prk_sub_unit_rep_name'];
	 	$prk_sub_unit_rep_mob_no = trim($_POST['prk_sub_unit_rep_mob_no']);
	 	$prk_sub_unit_rep_email = trim($_POST['prk_sub_unit_rep_email']);
	 	$prk_sub_unit_eff_date = trim($_POST['prk_sub_unit_eff_date']);
	 	$prk_sub_unit_end_date = trim($_POST['prk_sub_unit_end_date']);
	 	$user_name = trim($_POST['user_name']);
	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_sub_unit_id = trim($_POST['prk_sub_unit_id']);
	 	$token = trim($_POST['token']);

	 	$resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            $respon=prk_sub_unit_update($prk_admin_id,$prk_sub_unit_id,$prk_sub_unit_name,$prk_sub_unit_short_name,$prk_sub_unit_address,$prk_sub_unit_rep_name,$prk_sub_unit_rep_mob_no,$prk_sub_unit_rep_email,$prk_sub_unit_eff_date,$prk_sub_unit_end_date,$user_name);
        	echo ($respon);
        }else{
            $response = $resp;
            echo $response;
        }
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 	echo json_encode($response);
}
?>