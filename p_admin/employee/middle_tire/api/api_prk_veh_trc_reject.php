<?php 
/*
Description: vehicle eject
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function prk_veh_trc_reject($prk_admin_id,$user_admin_id,$prk_veh_type,$prk_veh_number,$prk_user_name,$prk_veh_remark){
    global $pdoconn;
    $response = array();
    $sql = "INSERT INTO `prk_veh_trc_dtl_reject`(`user_admin_id`, `prk_admin_id`, `prk_veh_type`, `prk_veh_number`, `prk_user_name`, `prk_veh_remark`, `inserted_by`,`inserted_date`,`prk_veh_in_time`)
    VALUES ('$user_admin_id','$prk_admin_id','$prk_veh_type','$prk_veh_number','$prk_user_name','$prk_veh_remark','$prk_user_name','".TIME."','".TIME."')";
    $query = $pdoconn->prepare($sql); 
    if($query->execute()){
        $response['status'] = 1;
        $id=$pdoconn->lastInsertId();
        $response['prk_veh_trc_dtl_id'] = $id;
        $response['message'] = 'Vehicle Rejected';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Reject Unsuccessful';
        return json_encode($response);
    }   
}
?>