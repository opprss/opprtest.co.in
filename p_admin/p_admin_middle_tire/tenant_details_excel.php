<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2018
*/ 
error_reporting(E_ALL & ~E_NOTICE);
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','parking_admin_name','effective_date','end_date','token','crud_type'))){
    if(isEmpty(array('prk_admin_id','parking_admin_name','token','crud_type'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
        $parking_admin_name = trim($_POST['parking_admin_name']);
        $effective_date = trim($_POST['effective_date']);
        $end_date = trim($_POST['end_date']);
        $crud_type = trim($_POST['crud_type']);
        $token = trim($_POST['token']);

        $id_name=$id_number=$tenant_id=$tenant_agrement_copy=$id_image=$tenant_image = '';

        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            if($_FILES['file']['name']){
                $PNG_TEMP_DIR ='./../uploades/'.$parking_admin_name.'/excel/';
                if (!file_exists($PNG_TEMP_DIR)) {
                    $old_mask = umask(0);
                    mkdir($PNG_TEMP_DIR, 0777, TRUE);
                    umask($old_mask);
                }
                $file_post = $PNG_TEMP_DIR.''.time()."_".$_FILES['file']['name'];
                @copy($_FILES['file']['tmp_name'],$file_post);
                if($handle = fopen($file_post, "r")){
                    $indexing = 1;     
                    while (($Row = fgetcsv($handle, 10000, ",")) !== FALSE) {
                        if($indexing == 1){
                            $indexing++;
                            continue;
                        }
                        $tenant_name = isset($Row[0]) ? strtoupper(trim($Row[0])) : '';
                        $tenant_mobile = isset($Row[1]) ? trim($Row[1]) : '';
                        $tenant_email = isset($Row[2]) ? strtolower(trim($Row[2])) : '';
                        $tenant_gender = isset($Row[3]) ? strtoupper(trim($Row[3])) : '';
                        $no_of_family = isset($Row[4]) ? strtoupper(trim($Row[4])) : '';
                        // array_push($allplayerdata, $owner_name);
                        tenant_details($prk_admin_id,$tenant_name,$tenant_mobile,$tenant_email,$tenant_gender,$id_name,$id_number,$no_of_family,$tenant_id,$tenant_image,$id_image,$tenant_agrement_copy,$effective_date,$end_date,$parking_admin_name,$crud_type);
                    }
                }
                $response['status'] = 1;
                $response['message'] = 'Successfully';
                $response = json_encode($response);
            }else{
                $response['status'] = 0;
                $response['message'] = 'File Missing';
                $response = json_encode($response);
            }
        }else{
            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>