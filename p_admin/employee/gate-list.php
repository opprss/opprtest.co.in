<!-- ################################################
  
Description: After successfull registration this page will dispaly with some static content.
Developed by: Soemen Banerjee
Created Date: 18-03-2018

 ####################################################-->

<?php
@session_start();
if(!isset($_SESSION["token"])){
    header('location:logout');
}
//include('../global_asset/config.php');
include('../../global_asset/parking_emp_include.php');
if (isset($_SESSION['prk_admin_id'])) {
  $prk_admin_id = $_SESSION['prk_admin_id'];
  $prk_user_username = $_SESSION["prk_user_username"];
  $prk_area_user_id = $_SESSION["prk_area_user_id"];
  $token = $_SESSION["token"];
}
//echo '--'.$prk_admin_id;

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">


    <title>OPPR | GATE LIST</title>
    <link rel="shortcut icon" href="../../img/favicon.png" type="image/x-icon">
    <!-- vendor css -->
    <link href="../../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../../lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Amanda CSS -->
    <link rel="stylesheet" href="../../css/amanda.css">
    <!-- font -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- custom css -->
    <link rel="stylesheet" type="text/css" href="../../css/thank-you-style.css">
    <style type="text/css">
    /*body{
      height: 100%;
      width: 100%;
    }*/
    .centered {
      position: fixed;
    top: 50%;
    left: 50%;
    /* bring your own prefixes */
    transform: translate(-50%, -50%);
    }
    .btn-gate{
      height: 100%;
      display: block;
      background: white;
      border: 2px solid #00b8d4;
      border-radius: 20px;
      padding: 1em;
      margin-bottom: 1em; 
      font-size: 1.5em;
      text-align: center;
      box-shadow: 0px 3px 10px -2px hsla(150, 5%, 65%, 0.5);
      position: relative;
    }
    .btn-gate:hover{;
      background: #00b8d4;
      color: #FFF;
    }
    </style>
  </head>

  <body style="">
    <div class="container-fluid ht-100v">
        <div class="row justify-content-md-center">
            <div class="col-lg-8 heaing" style="">
                <img src="<?php echo LOGO ?>" width="180px" class="pd-b-0">
            </div>
            <div class="col-lg-4 heaing pd-t-10" style="text-align: right;">
               <a href="logout" style="color: #FFF"><i class="fa fa-power-off"></i> Logout</a>
            </div>
            <div class="col-lg-4 centered pd-t-50">
                <!-- <div class=" d-flex align-items-center justify-content-center">

                <div class="wd-lg-100p wd-xl-70p tx-center pd-x-40 pd-t-30"> -->
                    <!-- start -->
                      <div class="row">

                   <?php
                      $response = prk_area_gate_dtl_list($prk_admin_id);
                      $response = json_decode($response, true);
                      if (!empty($response['parking'])) {
                        $count = count($response['parking']);
                        foreach ($response['parking'] as $key => $value) {
                          //[prk_area_gate_id] => 155 [prk_area_gate_name] => BOTH [prk_area_gate_dec] => JKGJ [prk_area_gate_landmark] => JHGBHJ [prk_area_gate_type] => B )
                          $prk_area_gate_id = $value['prk_area_gate_id'];
                          $prk_area_gate_name = $value['prk_area_gate_name'];
                          $prk_area_gate_type = $value['prk_area_gate_type'];
                          if ($prk_area_gate_type == "B") {
                            $gate_type = "BOTH";
                          }else if ($prk_area_gate_type == "O") {
                            $gate_type = "OUT";
                          }else if ($prk_area_gate_type == "I") {
                            $gate_type = "IN";
                          }else if ($prk_area_gate_type == "S") {
                            $gate_type = "SKIP";
                          }else{
                            $gate_type = "";
                          }
                    ?>
                        <div class="col-lg-12">
                          <section>
                            <div class="tx-center">
                              <form method="post" action="<?php echo PRK_EMP_URL.'prk_user_gate_login_web'?>">
                                <!-- 'prk_admin_id','prk_area_gate_id','prk_user_username','prk_area_gate_name','prk_area_gate_type','prk_area_user_id','token' -->
                                <input type="hidden" name="prk_admin_id" value="<?php echo $prk_admin_id?>">
                                <input type="hidden" name="prk_area_gate_name" value="<?php echo $prk_area_gate_name?>">
                                <input type="hidden" name="prk_area_gate_type" value="<?php echo $prk_area_gate_type?>">
                                <input type="hidden" name="prk_user_username" value="<?php echo $prk_user_username?>">
                                <input type="hidden" name="prk_area_user_id" value="<?php echo $prk_area_user_id?>">
                                <input type="hidden" name="token" value="<?php echo $token?>">
                                <button type="submit" class="btn btn-block btn-gate" name="prk_area_gate_id" id="out<%=count++%>" value="<?php echo $prk_area_gate_id ?>"><?php echo $prk_area_gate_name.' <span style="font-size:0.8em;">('.$gate_type.')</span>' ?></button>
                              </form>
                            </div>
                          </section>
                        </div>
                    <?php

                        }
                      }
                   ?>
                    <!-- end -->    
            </div>
        </div>
    </div>
  </body>
   <script type="text/javascript">
   $(document).ready(function() {
        window.history.pushState(null, "", window.location.href);        
        window.onpopstate = function() {
          window.history.pushState(null, "", window.location.href);
        };
    });
  </script>
</html>

