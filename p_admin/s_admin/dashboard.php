<?php
include "all_nav/header.php";
?>
<!-- vendor css -->
  <style type="text/css">
    .size{
      font-size: 11px;
      text-transform: uppercase;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
  </style> 
  <!-- links for datatable -->
  <link href="../../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
  <div class="am-mainpanel"><!-- cloding in footer -->
    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Home</h5>
    </div><!-- am-pagetitle -->
  </div>

  <div class="am-mainpanel">
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40" style="padding: 10px;">
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <form id="duplicatePayRecForm" action="" method="post">
            <div class="row">
              <div class="col-lg-3"></div>
              <div class="col-lg-8">
                
                <img src="../../img/logo_pdf.png">
              </div>
              <div class="col-lg-2"></div>
            </div>
          </form>
        </div>
      </div>
    </div>
 
<!-- datatable -->
<?php include"all_nav/footer.php"; ?>