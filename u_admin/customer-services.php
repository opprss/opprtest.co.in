<!-- ################################################
  
Description: parking area admin can contact with oppr team with the following contact form.
Developed by: Manranjan sarkar & Soemen Banerjee
Created Date: 29-03-2018

 ####################################################-->
<?php include "all_nav/header.php";  ?>
<style type="text/css">
  .bg-info{
    background-color: #2F4F4F !important;
  }
  .card{
    min-height: 180px !important;
  }
</style>
 <div class="am-mainpanel"><!-- closing in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Customer Services</h5>
        <!-- <form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form> search-bar-->
      </div><!-- am-pagetitle -->
  


      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">For customer service you can refer the followings</h6>
          <!-- <p class="mg-b-20 mg-sm-b-30">Add an optional header within a card.</p> -->

          <div class="row">

            <div class="col-md-4 mg-t-20">
              <div class="card bd-0">
                <div class="card-header card-header-default bg-info">
                  About Us
                </div><!-- card-header -->
                <div class="card-body bd bd-t-0">
                  <p class="mg-b-0">OPPR is a fully digitize parking solution. Whether you are a vehicle owner or parking space provider... <a href="http://www.opprss.com/about-us" target="_BLANK">Read more</a></p>
                </div><!-- card-body -->
              </div><!-- card -->
            </div><!-- col -->

            <div class="col-md-4 mg-t-20">
              <div class="card bd-0">
                <div class="card-header card-header-default bg-info">
                  Contact Us
                </div><!-- card-header -->
                <div class="card-body bd bd-t-0">
                  <p class="mg-b-0">We will love to provide you with more information, answers any questions you may have arise and lead you into the feature... <a href="http://www.opprss.com/contact-us" target="_BLANK">Read more</a></p>
                </div><!-- card-body -->
              </div><!-- card -->
            </div><!-- col -->

            <div class="col-md-4 mg-t-20">
              <div class="card bd-0">
                <div class="card-header card-header-default bg-info">
                  FAQ
                </div><!-- card-header -->
                <div class="card-body bd bd-t-0">
                  <p class="mg-b-0">We try to group all the questions you may have arise about our product in one place. Please explore... <a href="http://www.opprss.com/faq" target="_BLANK">Read more</a></p>
                </div><!-- card-body -->
              </div><!-- card -->
            </div><!-- col -->
            <div class="col-md-4 mg-t-20">
              <div class="card bd-0">
                <div class="card-header card-header-default bg-info">
                  Legal
                </div><!-- card-header -->
                <div class="card-body bd bd-t-0">
                  <p class="mg-b-0"> It is hereby stated and submitted that the terms and conditions set out for the usage of our platform has been set out hereunder hereby binding you...<a href="http://www.opprss.com/legal" target="_BLANK">Read more</a></p>
                </div><!-- card-body -->
              </div><!-- card -->
            </div><!-- col -->

            <div class="col-md-4 mg-t-20">
              <div class="card bd-0">
                <div class="card-header card-header-default bg-info">
                  Feedback
                </div><!-- card-header -->
                <div class="card-body bd bd-t-0">
                  <p class="mg-b-0">Your feedback is very important to us. So, please share your opinion about our product... <a href="http://www.opprss.com/feedback" target="_BLANK">Read more</a></p>
                </div><!-- card-body -->
              </div><!-- card -->
            </div><!-- col -->            

          </div><!-- row -->
        </div><!-- card -->
      </div>

<?php include"all_nav/footer.php"; ?>
</style>




