<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
$searchstring="%";
$prk_admin_id=$_REQUEST['pid'];
$type=$_REQUEST['type'];
if ($type=='SO') {
    if (isset($_REQUEST['q'])){
        $searchstring=$_REQUEST['q'];
    }
    $maxRows="8";
    if (isset($_REQUEST['maxRows'])){
        $maxRows=$_REQUEST['maxRows'];
    }else{
        $user = $pdoconn->query("SELECT `tenant_id`,`tenant_name` FROM `tenant_details` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."' AND `tenant_name` LIKE '%".$searchstring."%'");
        $results_user = $user->fetchAll(PDO::FETCH_ASSOC);
        // get data and store in a json array
        foreach($results_user as $row) {
            $clientcompany[] = array(
                'id' => $row['tenant_id'],
                'name' => $row['tenant_name']
          );
        }
        echo json_encode($clientcompany);
    }
}else{
    if(isset($_REQUEST['q'])){
        $user = $pdoconn->query("SELECT `tenant_id`,`tenant_name`,`tenant_mobile` FROM `tenant_details` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."' AND `tenant_id`='".$_REQUEST['q']."'");
        $results_user = $user->fetchAll(PDO::FETCH_ASSOC);
        if($results_user){
          $i=2;
          foreach($results_user as $val){
            $i=$i+1;
            $response['i'] =  $i;
            $response['status'] = 1;
            $response['message'] = 'Successful';
            $response['emp_id'] =  $val['tenant_id'];
            $response['emp_name'] =  $val['tenant_name'];
            $response['tenant_mobile'] =  $val['tenant_mobile'];
          }
        }else{
          $response['status'] = 0;
          $response['message'] = 'can not match';
        }
    }else{
      $response['status'] = 0;
      $response['message'] =  'Invalid Api Call';
    }
    echo json_encode($response);
}
?>