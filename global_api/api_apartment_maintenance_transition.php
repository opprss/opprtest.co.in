<?php
/*
Description: Parking area vehicle rate delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function apartment_maintenance_transition($prk_admin_id,$tran_id,$total_amount,$payment_type,$tran_date,$tran_flag,$inserted_by,$tran_by,$tran_rece_by){
    global $pdoconn;
    $response = array();
    $apar_main_tran_id=uniqid();
    $sql="INSERT INTO `apartment_maintenance_transition`(`apar_main_tran_id`, `prk_admin_id`, `tran_id`, `total_amount`, `payment_type`, `tran_date`, `tran_flag`, `inserted_by`, `inserted_date`, `tran_by`, `tran_rece_by`) VALUES ('$apar_main_tran_id','$prk_admin_id','$tran_id','$total_amount','$payment_type','$tran_date','$tran_flag','$inserted_by','".TIME."','$tran_by','$tran_rece_by')";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Sucessfull';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Unsucessfull';
        return json_encode($response);
    }
}
?>