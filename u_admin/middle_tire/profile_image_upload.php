<?php  
/*
Description: user image upload.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
require_once 'api/reg_api.php';
//require_once 'api/global_api.php';
ini_set("memory_limit", "99M");
ini_set('post_max_size', '20M');
ini_set('max_execution_time', 600);
$user_mobile = $_POST['user_mobile'];
define('IMAGE_MEDIUM_DIR', './../uploades/'.$user_mobile.'/profile/');
define('IMAGE_MEDIUM_SIZE', 250);
$user_id = $_POST['user_id'];
$token = $_POST['token'];

if(isset($_FILES['image_upload_file'])){
	$output['status']=FALSE;
	set_time_limit(0);
	$allowedImageType = array("image/gif",   "image/jpeg",   "image/pjpeg",   "image/png",   "image/x-png"  );
	
	if ($_FILES['image_upload_file']["error"] > 0) {
		$output['error']= "Error in File";
	}
	elseif (!in_array($_FILES['image_upload_file']["type"], $allowedImageType)) {
		$output['error']= "You can only upload JPG, PNG and GIF file";
	}
	elseif (round($_FILES['image_upload_file']["size"] / 1024) > 4096) {
		$output['error']= "You can upload file size up to 4 MB";
	} else {

		createDir(IMAGE_MEDIUM_DIR);
		$path[0] = $_FILES['image_upload_file']['tmp_name'];
		$file = pathinfo($_FILES['image_upload_file']['name']);
		$fileType = $file["extension"];
		$desiredExt='jpg';
		$fileNameNew = rand(333, 999) . time() . ".$desiredExt";
		$path[1] = IMAGE_MEDIUM_DIR . $fileNameNew;
		$path[3] = 'uploades/'.$user_mobile.'/profile/'.$fileNameNew;
		

		/*start*/
        $resp=user_token_check($user_id,$token);
        $json = json_decode($resp);
        if($json->status){
            if (createThumb($path[0], $path[1], $fileType, IMAGE_MEDIUM_SIZE, IMAGE_MEDIUM_SIZE,IMAGE_MEDIUM_SIZE)) {
				if (profileImageUpload($path[3],$user_id)) {
					$output['status']=TRUE;
					$output['filepath']= $path[3];
					$output['meaasge'] = 'upload successfull';
				}
			}
        }else{
            $output['session']=FALSE;
			$output['meaasge'] = 'upload sucessfully';
        }
		/*end*/
	}
	echo json_encode($output);
}
?>	