<?php 
/*
Description: subumit lisst.
Developed by: Rakhal Raj Mandal
Created Date: 14-06-2018
Update date :---------
*/
function prk_sub_unit_list($prk_admin_id){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
	$sql="SELECT prk_sub_unit.prk_sub_unit_id,
	prk_sub_unit.prk_sub_unit_name 
	FROM prk_sub_unit
	where prk_sub_unit.prk_admin_id = '$prk_admin_id'
	AND prk_sub_unit.active_flag = '".FLAG_Y."'
	AND prk_sub_unit.del_flag ='".FLAG_N."'
	AND  '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(prk_sub_unit.prk_sub_unit_eff_date, '%d-%m-%Y'), '%Y-%m-%d') 
	AND IFnull(DATE_FORMAT(STR_TO_DATE(prk_sub_unit.prk_sub_unit_end_date, '%d-%m-%Y'), '%Y-%m-%d'),'".FUTURE_DATE_WEB."')
	union select 0 as prk_sub_unit_id, 'PAID PARKING' as prk_sub_unit_name from dual";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
	    $arr_catagory = $query->fetchAll();
	    foreach($arr_catagory as $val)
	    {
	        $response['status'] = 1;
	        $response['message'] = 'Successful';
	        $park['prk_sub_unit_id'] = $val['prk_sub_unit_id'];
	        $park['prk_sub_unit_name'] = $val['prk_sub_unit_name'];
	        array_push($allplayerdata, $park);
	        $response['prk_area'] = $allplayerdata;
	    }
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>