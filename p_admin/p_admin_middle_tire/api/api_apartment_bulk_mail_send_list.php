<?php
/*
Description: Parking area vehicle all rate dtl list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function apartment_bulk_mail_send_list($prk_admin_id,$parking_admin_name){

    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql="SELECT acm.`compose_mail_id`,
        acm.`send_to`,
        (CASE SUBSTRING_INDEX(acm.`send_to`,'-',1) WHEN 'AM' ThEN 'ALL RESIDENT MEMBER' 
            WHEN 'AF' THEN 'ALL ASSOCIATION MEMBER'
            WHEN 'AO' THEN 'ALL OWNER' 
            WHEN 'AT' THEN 'ALL TENANT' 
            WHEN 'TO' THEN CONCAT('ALL LIVING MEMBER ',(SELECT tower_name FROM tower_details WHERE tower_id=SUBSTRING_INDEX(acm.`send_to`,'-',-1) and active_flag='".FLAG_Y."' AND del_flag='".FLAG_N."' AND prk_admin_id='$prk_admin_id'))
            WHEN 'TT' THEN CONCAT('ALL TENANT ',(SELECT tower_name FROM tower_details WHERE tower_id=SUBSTRING_INDEX(acm.`send_to`,'-',-1) and active_flag='".FLAG_Y."' AND del_flag='".FLAG_N."' AND prk_admin_id='$prk_admin_id'))
        end) as 'send_to_full',
        acm.`subject`,
        acm.`message`,
        DATE_FORMAT(acm.`send_date`,'%d-%b-%Y') AS `send_date`
        FROM `apartment_compose_mail` acm
        WHERE acm.`prk_admin_id`='$prk_admin_id' 
        AND acm.`active_flag`='".FLAG_Y."' 
        AND acm.`del_flag`='".FLAG_N."'
         ORDER BY `acm`.`compose_mail_unique_id` DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['compose_mail_id'] = $val['compose_mail_id'];
            $park['send_to'] = $val['send_to'];
            $park['send_to_full'] = $val['send_to_full'];
            $park['subject'] = $val['subject'];
            $park['message'] = $val['message'];
            $park['send_date'] = $val['send_date'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['mail_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>