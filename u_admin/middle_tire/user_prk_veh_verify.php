<?php
/*
Description: vehicle QR code scan parking area gate.
Developed by: Rakhal Raj Mandal
Created Date: 25-02-2018
Update date : 11-05-2018
*/
    include_once 'api/reg_api.php';
    // include_once 'api/global_api.php';
    $response = array();
 
    if(isAvailable(array('quercode_value','user_veh_type','user_veh_number','user_mobile','user_admin_id','user_nick_name','payment_type','gate_pass_num','token'))){

        if(isEmpty(array('user_admin_id','user_veh_type','user_veh_number','user_mobile','user_nick_name','quercode_value','gate_pass_num','token'))){

            $user_admin_id = trim($_POST['user_admin_id']);
            $user_veh_type = trim($_POST['user_veh_type']);
            $user_veh_number = trim($_POST['user_veh_number']);
            $user_mobile = trim($_POST['user_mobile']);
            $user_nick_name = trim($_POST['user_nick_name']);
            $quercode_value = trim($_POST['quercode_value']);
            $payment_type = $_POST['payment_type'];
            $gate_pass_num = $_POST['gate_pass_num'];
            
            $token = ($_POST['token']);
            $resp=user_token_check($user_admin_id,$token);
            $json = json_decode($resp);
            if($json->status=='1'){
                $response = user_prk_veh_verify($quercode_value,$user_veh_type,$user_veh_number,$user_mobile,$user_admin_id,$user_nick_name,$payment_type,$gate_pass_num);
                //echo ($response);  
            }else{
                //echo ($resp);
                $response = $resp;
            }       
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
        }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        $response = json_encode($response);
    }
    echo $response;

?>