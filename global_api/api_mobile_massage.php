<?php
/*
Description: mobile message . 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function mobile_massage($mobile_no,$text){
    $mobile_no='91'.$mobile_no;
    $message = urlencode($text);
    $authKey = "193154A0PMvCMVJL5a5c40f8";
    $senderId = "opprss";
    $route = "4";
    $postData = array(
        'authkey' => $authKey,
        'mobiles' => $mobile_no,
        'message' => $message,
        'sender' => $senderId,
        'route' => $route,
        'country'=>'0'
    );
    $url="https://control.msg91.com/api/sendhttp.php";
    $ch = curl_init();
        curl_setopt_array($ch, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $postData
    ));
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $output = curl_exec($ch);
    if(curl_errno($ch))
    {
        echo 'error:' . curl_error($ch);
    }
    curl_close($ch);
}
?>