<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2019
*/ 
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','parking_admin_name','token','con_per_name','con_per_mobile','con_per_email','total_amount','payment_type','apart_tran_remark','crud_type','bank_name','cheque_number','apar_tran_by'))){
    if(isEmpty(array('prk_admin_id','parking_admin_name','token','crud_type'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $parking_admin_name = trim($_POST['parking_admin_name']);
        $token = trim($_POST['token']);
        $con_per_name = trim($_POST['con_per_name']);
        $con_per_mobile = trim($_POST['con_per_mobile']);
        $con_per_email = trim($_POST['con_per_email']);
        $total_amount = trim($_POST['total_amount']);
        $payment_type = trim($_POST['payment_type']);
        $apart_tran_remark = trim($_POST['apart_tran_remark']);
        $crud_type = trim($_POST['crud_type']);
        $bank_name = trim($_POST['bank_name']);
        $cheque_number = trim($_POST['cheque_number']);
        $apar_tran_by = trim($_POST['apar_tran_by']);
        $prk_area_user_id = isset($_POST['prk_area_user_id']) ? trim($_POST['prk_area_user_id']) : ''; 
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            $response =apartment_transaction($prk_admin_id,$parking_admin_name,$con_per_name,$con_per_mobile,$con_per_email,$total_amount,$payment_type,$apart_tran_remark,$crud_type,$bank_name,$cheque_number,$apar_tran_by,$prk_area_user_id);
        }else{
            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>