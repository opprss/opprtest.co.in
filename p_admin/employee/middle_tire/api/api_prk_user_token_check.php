<?php 
/*
Description: Parking employ token check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function prk_user_token_check($prk_area_user_id,$prk_admin_id,$token){
    global $pdoconn;
    $response = array();
    $sql = "SELECT * FROM `prk_token` WHERE `prk_admin_id`='$prk_admin_id' AND `prk_area_user_id`='$prk_area_user_id' AND `active_flag`='".FLAG_Y."' AND `token_value`='$token'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $response['status'] = 1; 
        $response['session'] = 1;
        $response['message'] = 'Successful';
    }else{
        $response['status'] = 0; 
        $response['session'] = 0;
        $response['message'] = 'Session Expired Please Login Again';
    }  
    return json_encode($response);
}
?>