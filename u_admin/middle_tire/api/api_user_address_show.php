<?php
/*
Description: user address show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_address_show($user_admin_id,$user_add_id,$mobile){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT `user_address`.`user_add_id`,
    `user_address`.`user_address`,
    `user_address`.`landmark`,
    `user_address`.`user_add_area`,
    `user_address`.`user_add_city`,
    `user_address`.`user_add_state`,
    `user_address`.`user_add_country`,
    `user_address`.`user_add_pin`,
    `user_address`.`prk_admin_id`,
    `user_address`.`tower_name`,
    `user_address`.`falt_name`,
    `state`.`state_name`,
    `city`.`city_name`
    FROM `user_address`,`state`,`city` 
    WHERE `user_admin_id`='$user_admin_id'
    AND `user_add_id`='$user_add_id' 
    AND `state`.`active_flag`='".FLAG_Y."' 
     AND `user_address`.`active_flag`='".FLAG_Y."' 
    AND `user_address`.`del_flag`='".FLAG_N."'
    AND `city`.`e_id`= `user_address`.`user_add_city`
    AND `state`.`e`= `user_address`.`user_add_state`";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['user_add_id'] = $val['user_add_id'];
        $response['user_address'] = $val['user_address'];
        $response['landmark'] = $val['landmark'];
        $response['user_add_area'] = $val['user_add_area'];
        $response['user_add_city'] = $val['user_add_city'];
        $response['user_add_state'] = $val['user_add_state'];
        $response['user_add_country'] = $val['user_add_country'];
        $response['user_add_pin'] = $val['user_add_pin'];
        $response['state_name'] = $val['state_name'];
        $response['city_name'] = $val['city_name'];
        $response['prk_admin_id'] = $val['prk_admin_id'];
        $response['tower_name'] = $val['tower_name'];
        $response['falt_name'] = $val['falt_name'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>