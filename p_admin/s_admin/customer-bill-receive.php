<?php
  session_start();
  include "all_nav/header.php";
  $previous_url =$_SERVER['HTTP_REFERER'];
  $cus_bill_dtl_id = isset($_REQUEST['list_id']) ? trim($_REQUEST['list_id']) : ''; 
  $cus_bill_show = cus_bill_generate_show($cus_bill_dtl_id);
  $cus_show = json_decode($cus_bill_show);
  $prk_admin_id=$cus_show->prk_admin_id;
  $invoice_number=$cus_show->invoice_number;
  $billing_month=$cus_show->billing_month;
  $date_of_invoice=$cus_show->date_of_invoice;
  $bill_due_date=$cus_show->bill_due_date;
  $bill_period_start_date=$cus_show->bill_period_start_date;
  $bill_period_end_date=$cus_show->bill_period_end_date;
  $bill_amount=$cus_show->bill_amount;
  $discount_amount=$cus_show->discount_amount;
  $tax_value=$cus_show->tax_value;
  $cgst_amount=$cus_show->cgst_amount;
  $sgst_amount=$cus_show->sgst_amount;
  $igst_amount=$cus_show->igst_amount;
  $round_of_amount=$cus_show->round_of_amount;
  $total_invoice_value=$cus_show->total_invoice_value;
  $date_of_invoice=$cus_show->date_of_invoice;

  $prk_inf = prk_inf($prk_admin_id);
  $prk_inf = json_decode($prk_inf, true);
  $prk_area_name = $prk_inf['prk_area_name'];

  $category_type='PAY_TYPE';
  $payment_method = common_drop_down_list($category_type);
  $payment_method_list = json_decode($payment_method, true);
?>
<style type="text/css">
  .size{
    font-size: 11px;
  }
  .error{
    font-size: 11px;
    color: red;
  }
</style>
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Customer Bill Receive</h5>
  </div>
</div>
<div class="am-mainpanel">
	<div class="am-pagebody">
    <div class="card pd-20 pd-sm-40" >
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <form id="admin_customer">
          <div class="row mg-b-25">
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Premises Name<span class="tx-danger">*</span></label>
                <input type="text" class="form-control" readonly="" placeholder="Kind Alt Name" name="prk_area_name" id="prk_area_name" value="<?=$prk_area_name?>">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Invoice Number<span class="tx-danger">*</span></label>
                <input type="text" name="invoice_number" id="invoice_number" placeholder="Invoice Number" class="form-control" autocomplete="off" readonly="" value="<?=$invoice_number?>">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Billing Month<span class="tx-danger">*</span></label>
                <input type="text" name="billing_month" id="billing_month" placeholder="Billing Month" class="form-control" autocomplete="off" readonly="" value="<?=$billing_month?>">
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Date Period<span class="tx-danger">*</span></label>
                <input type="text" name="date_period" id="date_period" placeholder="Date Period" class="form-control" autocomplete="off" readonly="" value="<?=$bill_period_start_date.' to '.$bill_period_end_date?>">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Total Invoice Value<span class="tx-danger">*</span></label>
                <input type="text" name="total_invoice_value" id="total_invoice_value" placeholder="Total Invoice Value" class="form-control" autocomplete="off" readonly="" value="<?=$total_invoice_value?>">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Payment Type<span class="tx-danger">*</span></label>
                <div class="select" style="">
                  <select name="payment_type" id="payment_type" style="font-size: 14px;">
                    <option value="">SELECT TYPE</option>
                    <?php foreach ($payment_method_list['drop_down'] as $val){ ?>  
                      <option value="<?php echo $val['sort_name']?>"><?php echo $val['full_name']?></option>
                    <?php  }?> 
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-5" id="hidden_cheque" style="display: none;">
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label size">BANK NAME<span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Bank Name" name="bank_name" id="bank_name">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label size">CHEQUE NUMBER<span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Cheque Number" name="cheque_number" id="cheque_number" maxlength="6">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="form-layout-footer mg-t-30">
                <div class="row">
                  <div class="col-lg-6">
                    <button type="button" class="btn btn-block btn-primary prk_button" name="save" id="save"><span id="refresh_button" style="display: none;"><i class="fa fa-refresh fa-spin"></i></span>Bill Receive</button>
                  </div>
                  <div class="col-lg-6">
                    <a href="<?=$previous_url?>"><button class="btn btn-block btn-primary prk_button" type="button">BACK</button></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php include"all_nav/footer.php"; ?>
<script type="text/javascript">
  var SUP_EMP_URL = "<?php echo SUP_EMP_URL ;?>";
  var inserted_by = "<?php echo $employee_user_name ;?>";
  var prk_admin_id = "<?=$prk_admin_id?>";
  var cus_bill_dtl_id = "<?=$cus_bill_dtl_id?>";
  $( document ).ready( function () {
    $('#save').click(function(){
      if($("#admin_customer").valid()){
        // alert('ok');
        var payment_type=($("#payment_type").val());
        var payment_receive_amount=($("#total_invoice_value").val());
        var payment_receive_by='SUPER_ADMIN';
        var bank_name=($("#bank_name").val());
        var bank_branch_name='';
        var beneficiary_name='';
        var cheque_number=($("#cheque_number").val());
        var online_payment_dtl_id='';
        var urlCusBillReceive = SUP_EMP_URL+'cus_bill_receive.php';
        $('#refresh_button').show();
        // 'cus_bill_dtl_id','payment_type','payment_receive_amount','payment_receive_by','inserted_by','bank_name','bank_branch_name','beneficiary_name','cheque_number','online_payment_dtl_id'
        $.ajax({
          url :urlCusBillReceive,
          type:'POST',
          data :{
            'cus_bill_dtl_id':cus_bill_dtl_id,
            'payment_type':payment_type,
            'payment_receive_amount':payment_receive_amount,
            'payment_receive_by':payment_receive_by,
            'inserted_by': inserted_by,
            'bank_name':bank_name,
            'bank_branch_name':bank_branch_name,
            'beneficiary_name':beneficiary_name,
            'cheque_number':cheque_number,
            'online_payment_dtl_id':online_payment_dtl_id
          },
          dataType:'html',
          success  :function(data){
            $('#refresh_button').hide();
            $('#save').prop('disabled', false);
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.9em;'>Receive Successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                    // location.reload(true);
                    window.location.href='<?=$previous_url;?>';
                  }
                }
              });
            }else{
              if( typeof json.session !== 'undefined'){
                if (!json.session) {
                  window.location.replace("logout.php");
                }
              }else{
                $.alert({
                  icon: 'fa fa-frown-o',
                  theme: 'modern',
                  title: 'Error !',
                  content: "<p style='font-size:0.9em;'>"+json.message+"</p>",
                  type: 'red'
                });
              }
            }
          }
        });
      }
    });
    $("#admin_customer" ).validate( {
      rules: {
        payment_type: "required",
        bank_name: "required",
        cheque_number: "required"
      }
    });
    $('#payment_type').on('change', function() {
      if(this.value == "CHEQUE") {
        $('#hidden_cheque').show(500);
      }else{
        $('#hidden_cheque').hide(500);
      }
    });
  });
</script> 