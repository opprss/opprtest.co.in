<?php
include('../../global_asset/parking_emp_include.php');
@session_start();
$prk_admin_id = $_SESSION['prk_admin_id'];
$prk_user_username = $_SESSION["prk_user_username"];
$prk_user_gate_login_id = isset($_SESSION['prk_user_gate_login_id']) ? trim($_SESSION['prk_user_gate_login_id']) : ''; 
$token =$_SESSION['token'];
prk_user_logout($token);
if (!empty($prk_user_gate_login_id)) {
	$respon = prk_user_gate_logout($prk_admin_id,$prk_user_username,$prk_user_gate_login_id);
	$json = json_decode($respon);
	if ($json->status) {
	   	session_destroy();
		header('location:'.OPPR_BASE_URL.'signin');
	}else{
	    header('location:../dashboard');    
	}
}else{
	session_destroy();
	header('location:'.OPPR_BASE_URL.'signin');
}

?>