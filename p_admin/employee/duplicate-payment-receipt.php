<!-- ################################################
  
Description: 
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
 
<?php 
include "all_nav/header.php";
if (isset($_SESSION['prk_admin_id'])) {
 //$insert_by=$_SESSION['parking_admin_name'];
  $prk_admin_id = $_SESSION['prk_admin_id'];
  $prk_user_username = $_SESSION["prk_user_username"];
  $prk_area_user_id = $_SESSION["prk_area_user_id"];
  $token = $_SESSION["token"];
  $prk_area_gate_type = $_SESSION["prk_area_gate_type"]; 

  $response = prk_area_advance_status($prk_admin_id);
  $response = json_decode($response, true);
  $advance_flag = $response['advance_flag'];
  $advance_pay = $response['advance_pay'];
  $print_in_flag = $response['print_in_flag'];
  $print_out_flag = $response['print_out_flag'];
}
 ?>
<!-- vendor css -->
  <style type="text/css">
    .size{
      font-size: 11px;
      text-transform: uppercase;
    }
  .error_size{
    font-size: 11px;
    color: red;

  }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
  </style> 
  <!-- links for datatable -->
  <link href="../../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../../lib/select2/css/select2.min.css" rel="stylesheet">

  <div class="am-mainpanel"><!-- cloding in footer -->

    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Duplicate Payment Receipt</h5>
    </div><!-- am-pagetitle -->
  </div>

  <div class="am-mainpanel">
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40" style="padding: 10px;">
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <form id="duplicatePayRecForm" action="" method="post">
            <div class="row">
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">VEHICLE NUMBER <span class="tx-danger">*</span></label>
                  <input type="text" class="form-control" placeholder="Vehicle Number" name="veh_number" id="veh_number" onkeyup="vehNoUppercase();" maxlength="10">
                  <span style="position: absolute;" id="error_veh_number" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-2 error_show">
                <div class="form-group">
                  <label class="form-control-label size">start DATE <span class="tx-danger">*</span></label>
                  <input type="text" name="start_date" id="start_date" placeholder="MM/DD/YYYY" class="form-control prevent_readonly" readonly="readonly">
                  <span style="position: absolute;" id="error_start_date" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-2 error_show">
                <div class="form-group">
                  <label class="form-control-label size">END DATE <span class="tx-danger">*</span></label>
                  <input type="text" name="end_date" id="end_date" placeholder="MM/DD/YYYY" class="form-control prevent_readonly" readonly="readonly">
                  <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                </div>
              </div>
              
              <div class="col-lg-4">
                <div class="form-layout-footer mg-t-20">
                  <div class="row pd-t-10">
                      <div class="col-lg-6">
                          <button type="button" class="btn btn-block prk_button" id="show">SHOW</button>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  <!-- datatable -->
  <div class="am-pagebody" id="data">

      <div class="card pd-20 pd-sm-40" style="padding: 10px;">
        <div class="table-wrapper">
          <table id="datatable1" class="table display responsive nowrap">
            <thead>
              <tr>
                <th class="wd-5p">Type</th>
                <th class="wd-15p">Vehicle Number</th>
                <th class="wd-15p">payment receive number</th>
                <th class="wd-15p">Ammount</th>
                <th class="wd-15p">Payment mode</th>
                <th class="wd-15p">Inserted date</th>
                <th class="wd-15p">Action</th>
              </tr>
            </thead>
            <tbody id="dataLines">
            <!-- data -->
            <?php

            //veh_number=WB12WE1212&effective_date=01-06-2018&end_date=09-06-2018&veh_rate_id=#

            if (isset($_POST['veh_number']) && isset($_POST['start_date']) && isset($_POST['end_date'])) {
              $veh_number = $_POST['veh_number'];
              $start_date = $_POST['start_date'];
              $end_date = $_POST['end_date'];
            }else{
              $veh_number ='';
              $start_date ="";
              $end_date ="";
            }

              $response = duplicate_payment_dtl_list($prk_admin_id,$veh_number,$start_date,$end_date);
              $response = json_decode($response, true);
              if (isset($response['parking'])) {
                foreach ($response['parking'] as $key => $value) {
                ?>
                <tr>
                  <td><img src="<?php echo OPPR_BASE_URL.$value['vehicle_typ_img']; ?>" style="height: 40px;"></td>
                  <td><?php echo $value['veh_number']; ?></td>
                  <td><?php echo $value['payment_receive_number']; ?></td>
                  <td><?php echo $value['payment_amount']; ?></td>
                  <td><?php echo $value['payment_type']; ?></td>
                  <td><?php echo $value['inserted_date']; ?></td>
                  <td>
                    <?php
                      if ($value['in_out_flag'] == FLAG_I) {
                        $printUrl =  PRK_EMP_URL.'cash_memo_in_print.php?id='.$value['payment_dtl_id'].'&duplicate='.FLAG_Y;
                      }else {
                        $printUrl = PRK_EMP_URL.'cash_memo_out_print.php?id='.$value['payment_dtl_id'].'&duplicate='.FLAG_Y;
                      }
                    ?>
                    <a class="out btn prk_button print" id="print" href="<?php echo $printUrl; ?>" style="color: #FFF;"><i class="fa fa-print"></i> PRINT</a></button>
                  </td>
                </tr>
                <?php
                }
              }
            ?>
            <!-- /data -->
            </tbody>
          </table>
        </div><!-- table-wrapper -->
      </div><!-- card -->
  <!-- /datatable -->
  <?php include"all_nav/footer.php"; ?>
  <script type="text/javascript">
    var pkrEmpUrl = "<?php echo PRK_EMP_URL; ?>";
    var prk_admin_id = "<?php echo $prk_admin_id; ?>";
  
    $( document ).ready( function () {

      var urlVehicleType = pkrEmpUrl+'vehicle_type.php';
      $('[data-toggle="tooltip"]').tooltip();   

      $(".print").click(function(){
        var urlPrint = $(this).attr('href');
        window.open(urlPrint,'mywindow','width=850,height=600');
        return false;
      });

      $("#veh_number").blur(function(){
          var veh_number = $('#veh_number').val();
          IsVehNumber(veh_number);
      });
      function IsVehNumber(veh_number) {
          var urlVehNoCheck = pkrEmpUrl+'v_numberChecker.php';

          $.ajax({
            url :urlVehNoCheck,
            type:'POST',
            data :
            {
              'prk_admin_id':prk_admin_id,
              'v_number':veh_number
            },
            dataType:'html',
            success  :function(data)
            {
              var json = $.parseJSON(data);
              vehNumberReturn(json.status);
            }
          });
      } 
      function vehNumberReturn(flag){
        if(!flag){
            $('#error_veh_number').text('Vehicle number is invalid');
            //return false;
        }else{
            $('#error_veh_number').text('');
            //return true;
        }
      }

      $("#start_date").blur(function () {
          var start_date = $('#start_date').val();
          if(start_date == ''){
            $('#error_start_date').text('Satrt date is required');
            return false;
          }else{
            $('#error_start_date').text('');
            return true;
          }
      });

      $("#end_date").blur(function () {
          var start_date = $('#end_date').val();
          if(start_date == ''){
            $('#error_end_date').text('End date is required');
            return false;
          }else{
            $('#error_end_date').text('');
            return true;
          }
      });

      $('#show').click(function () {

        var veh_number = $('#veh_number').val();
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();

        if(veh_number!='' && start_date!='' && end_date!=''){
          
          var urlVehNoCheck = pkrEmpUrl+'v_numberChecker.php';

          $.ajax({
            url :urlVehNoCheck,
            type:'POST',
            data :
            {
              'prk_admin_id':prk_admin_id,
              'v_number':veh_number
            },
            dataType:'html',
            success  :function(data)
            {
              //alert(data);
              var json = $.parseJSON(data);
              if (json.status) {
                  /*-----*/
                  $("#duplicatePayRecForm").submit();
                  /*------*/
                }else{
                  $('#error_veh_number').text('Vehicle number is invalid');
                }
            }
          });
        }else{
          if (veh_number == '') {
            $('#error_veh_number').text('Vehicle number required');
          }if (start_date == '') {
            $('#error_start_date').text('Satrt date is required');
          }if (end_date == '') {
            $('#error_end_date').text('End date is required');
          }
        }
      });
      
      $("#veh_number").on('input', function(evt) {
      var input = $(this);
      var start = input[0].selectionStart;
      $(this).val(function (_, val) {
        return val.toUpperCase();
      });
      input[0].selectionStart = input[0].selectionEnd = start;
    }); 

    });

  /*date*/
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#start_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
            /*if (date < now) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }*/
            $('#error_start_date').text('');
            return {};
        } 
    });
  });

 
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#start_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
            if (date < now_end) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            $('#error_end_date').text('');

            return {};
        } 
    });
  });

  $('#start_date').focus(function(){
    $('#end_date').val('');
  });
  </script>
  <!-- scripts for datatable -->
  <script src="../../lib/highlightjs/highlight.pack.js"></script>
  <script src="../../lib/datatables/jquery.dataTables.js"></script>
  <script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>

  <script>
      $(function(){
        'use strict';

        $('#datatable1').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search Here',
            sSearch: '',
            lengthMenu: '_MENU_ Page',
            sEmptyTable: 'No data found, please fill the above form.',
          }
        });

        $('#datatable2').DataTable({
          bLengthChange: false,
          searching: false,
          responsive: true
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

      });


      //functions
      function upperCase(a){
        setTimeout(function(){
            a.value = a.value.toUpperCase();
        }, 1);
      }
      function lowerCase(a){
        setTimeout(function(){
          //alert(a);
            a.value = a.value.toLowerCase();
        }, 1);
      }
      $("#name").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
            event.preventDefault();
          }
        });
  </script>
