<?php
/*
Description: parkin area in wheeler count
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function wheeler_count($prk_admin_id,$user_veh_type,$user_veh_number,$payment_type){
    $response = array();
    global $pdoconn;
    if($payment_type== CASH || $payment_type== WALLET || $payment_type== PP){
        $sql = "SELECT pr.prk_admin_id, 
            pr.prk_vehicle_type,
            pr.vehicle_type_dec,
            pr.tot_prk_space, 
            IFNULL(pt.veh_count,0) AS veh_count, 
            (IFNULL(pr.tot_prk_space,0) - IFNULL(pt.veh_count,0)) as veh_avl
            FROM 
            (SELECT `prk_admin_id`, `prk_veh_type`, COUNT(`prk_veh_type`) AS veh_count
            FROM `prk_veh_trc_dtl` 
            WHERE `prk_veh_out_time` IS NULL
            AND `payment_type` NOT in ( '".DGP."', '".TDGP."')
            GROUP BY `prk_veh_type`, `prk_admin_id`) pt right join
            (SELECT `prk_area_rate`.`prk_admin_id`, `prk_area_rate`.`prk_vehicle_type`,`vehicle_type`.`vehicle_type_dec`, `prk_area_rate`.`tot_prk_space` 
            FROM   prk_area_rate , vehicle_type 
            where   `prk_area_rate`.`prk_vehicle_type` = `vehicle_type`.`vehicle_sort_nm`
            AND   `prk_area_rate`.`active_flag`='".FLAG_Y."'
            AND   `prk_area_rate`.`del_flag`='".FLAG_N."'
            AND `prk_area_rate`.`prk_vehicle_type` = '$user_veh_type'
            AND  '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(`prk_area_rate`.`prk_rate_eff_date`, '%d-%m-%Y'), '%Y-%m-%d')    
                    AND  DATE_FORMAT(STR_TO_DATE(IFNULL(`prk_area_rate`.`prk_rate_end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')
            ) pr
            on  pr.`prk_admin_id` = pt.`prk_admin_id`AND   pr.`prk_vehicle_type` = pt.`prk_veh_type`
            where   pr.`prk_admin_id` = '$prk_admin_id'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $val = $query->fetch();
        $tot_prk_space = $val['tot_prk_space'];
        $veh_count = $val['veh_count'];
        if($veh_count< $tot_prk_space){
            $respons['status'] = 1;
            $respons['message'] = 'Parking Area empty';
            $return = json_encode($respons);
        }else{
            $respons['status'] = 0;
            $respons['message'] = 'Sorry !! Parking Area Full';
            $return = json_encode($respons);
        }
    }else if($payment_type== DGP){
        $resp= gate_pass_match($prk_admin_id,$user_veh_type,$user_veh_number);
        $json = json_decode($resp);
        if($json->status=='1'){
            $gate_pass_num=$json->prk_gate_pass_num;                        
            $prk_sub_unit_id=$json->prk_sub_unit_id;
            $sql = "SELECT pr.prk_admin_id, 
               pr.prk_sub_unit_id,
               pr.veh_type,
               pr.vehicle_type_dec,
               pr.veh_space, 
               IFNULL(pt.veh_count,0) AS veh_count, 
               (IFNULL(pr.veh_space,0) - IFNULL(pt.veh_count,0)) as veh_avl
                FROM 
                (SELECT `prk_admin_id`, `prk_veh_type`,`prk_sub_unit_id`, COUNT(`prk_veh_type`) AS veh_count
                FROM `prk_veh_trc_dtl` 
                WHERE `prk_veh_out_time` IS NULL
                AND `payment_type` not in ( 'tdgp', 'cash', 'wallet')
                GROUP BY `prk_veh_type`, `prk_admin_id`,`prk_sub_unit_id`) pt right join
                (SELECT `prk_sub_unit_veh_space`.`prk_admin_id`, `prk_sub_unit_veh_space`.`prk_sub_unit_id`,
                `prk_sub_unit_veh_space`.`veh_type`,
                `vehicle_type`.`vehicle_type_dec`, `prk_sub_unit_veh_space`.`veh_space` 
                FROM   prk_sub_unit_veh_space , vehicle_type 
                where   `prk_sub_unit_veh_space`.`veh_type` = `vehicle_type`.`vehicle_sort_nm`
                AND   `prk_sub_unit_veh_space`.`active_flag`='".FLAG_Y."' 
                AND `prk_sub_unit_veh_space`.`del_flag`='".FLAG_N."' 
                AND  '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(`prk_sub_unit_veh_space`.`prk_sub_unit_veh_space_eff_date`, '%d-%m-%Y'), '%Y-%m-%d')    
                AND  DATE_FORMAT(STR_TO_DATE(IFNULL(`prk_sub_unit_veh_space`.`prk_sub_unit_veh_space_end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')) pr
                on    pr.`prk_admin_id` = pt.`prk_admin_id`
                AND   pr.`veh_type` = pt.`prk_veh_type`
                AND   pr.`prk_sub_unit_id`  = pt.`prk_sub_unit_id`
                where   pr.`prk_admin_id` = '$prk_admin_id'
                AND pr.prk_sub_unit_id = '$prk_sub_unit_id'
                AND pr.veh_type = '$user_veh_type'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $val = $query->fetch();
            $veh_space = $val['veh_space'];
            $veh_count = $val['veh_count'];
            if($veh_space> $veh_count){
                $response['status'] = 1;
                $response['message'] = 'Subunit parking empty';
                $return = json_encode($response);
            }else{
                $response['status'] = 0;
                $response['message'] = 'Sorry Your Subunit !! Parking Area Full';
                $return = json_encode($response);
            }
        }else{
            $response['status'] = 0;
            $response['message'] = 'Your Gate Pass Not Match';
            $return = json_encode($response);
        }
    }else if($payment_type == TDGP){
        $response['status'] = 1;
        $response['message'] = 'parking Alow';
        $return = json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Technical Issue';
        $return = json_encode($response);
    }
    return $return;
}
?>