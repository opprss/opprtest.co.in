<?php
/*
Description: Parking visiter in list
Developed by: Rakhal Raj Mandal
Created Date: 31-05-2018
Update date : 
*/ 
function visit_in_list($prk_admin_id,$start_date,$end_date,$visit_name,$visit_mobile,$tower_id,$flat_id){
    if(!empty($start_date) && !empty($end_date)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($start_date)) == date($start_date) && date($format, strtotime($end_date)) == date($end_date)) {
            $start_date= $start_date;
            $end_date=  $end_date ;
        } else {
            $start_date = date("Y-m-d", strtotime($start_date));
            $end_date = date("Y-m-d", strtotime($end_date));
        }
    }else{
        DATE;
        $end_date= TIME_TRN;
        $start_date= date("Y-m-d",strtotime("-1 Days"));
    }
    global $pdoconn;
    $response = array();
    $resp = array();
    $re = array();
    $sql="SELECT `prk_area_name` FROM `prk_area_dtl` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $prk_area_name = $val['prk_area_name'];
    $sql = "SELECT `prk_area_dtl`.`prk_area_name`,
    `pvd`. `prk_visit_dtl_id`,
    `pvd`.`visit_name`,
    `pvd`.`visit_gender`,
    `pvd`.`visit_mobile`,
    `pvd`.`visit_purpose`,
    `pvd`.`visit_meet_to`,
    `pvd`.`visit_address`,
    `pvd`.`visit_with_veh`,
    `pvd`.`visit_card_id`,
    `pvd`.`visitor_meet_to_name`,
    IF(`pvd`.`visitor_meet_to_address` IS NULL or `pvd`.`visitor_meet_to_address`='', 'NA', `pvd`.`visitor_meet_to_address`) as 'visitor_meet_to_address',
    (SELECT `ad`.`full_name` FROM `all_drop_down` `ad` where `ad`.`category`='VIS_L' AND `ad`.`sort_name`= `pvd`.`visitor_type`) as 'visitor_type',
    `pvd`.`no_of_guest`,
    (CASE pvd.visitor_type WHEN 'O' THEN IF(pvd.visit_img IS NULL or pvd.visit_img = '', 
                (CASE WHEN pvd.visit_gender= '".FLAG_M."' then '".MALE_IMG_D."' WHEN pvd.visit_gender= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".PRK_BASE_URL."',pvd.visit_img))
    
            WHEN 'V' THEN (SELECT IF(vgp.visitor_img IS NULL or vgp.visitor_img = '', (CASE WHEN pvd.visit_gender= '".FLAG_M."' then '".MALE_IMG_D."' WHEN pvd.visit_gender= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".PRK_BASE_URL."',vgp.visitor_img)) FROM visitor_gate_pass_dtl vgp WHERE visitor_gate_pass_dtl_id=pvd.unique_id)

            WHEN 'C' THEN (SELECT IF(cgp.visitor_img IS NULL or cgp.visitor_img = '', (CASE WHEN pvd.visit_gender= '".FLAG_M."' then '".MALE_IMG_D."' WHEN pvd.visit_gender= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".PRK_BASE_URL."',cgp.visitor_img)) FROM contractual_gate_pass cgp WHERE cgp.contractual_gate_pass_dtl_id=pvd.unique_id)
 
            WHEN 'G' THEN IF(pvd.visit_img IS NULL or pvd.visit_img = '', (CASE WHEN pvd.visit_gender= '".FLAG_M."' then '".MALE_IMG_D."' WHEN pvd.visit_gender= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".PRK_BASE_URL."',pvd.visit_img))
 
            WHEN 'U' THEN (SELECT IF(ud.user_img IS NULL or ud.user_img = '', (CASE WHEN pvd.visit_gender= '".FLAG_M."' then '".MALE_IMG_D."' WHEN pvd.visit_gender= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".USER_BASE_URL."',ud.user_img)) FROM user_detail ud where ud.user_admin_id= pvd.unique_id AND ud.active_flag='".FLAG_Y."') 
        end) as 'visit_img',
    DATE_FORMAT(`pvd`.`start_date`,'%d-%m-%Y') AS `visiter_in_date`,
    DATE_FORMAT(`pvd`.`start_date`,'%I:%i %p') AS `visiter_in_time`,
    DATE_FORMAT(`pvd`.`end_date`,'%d-%m-%Y') AS `visiter_out_date`,
    DATE_FORMAT(`pvd`.`end_date`,'%I:%i %p') AS `visiter_out_time`,
    IF(`pvd`.`veh_type` IS NULL or `pvd`.`veh_type` = '', 'NA', `pvd`.`veh_type`) as `veh_type`,
    IF(`pvd`.`veh_number` IS NULL or `pvd`.`veh_number` = '', 'NA', `pvd`.`veh_number`) as `veh_number`
    FROM `prk_visit_dtl` pvd,`prk_area_dtl`
    WHERE `pvd`.`prk_admin_id` ='$prk_admin_id'
    AND `pvd`.`prk_admin_id` = `prk_area_dtl`.`prk_admin_id`
    AND `prk_area_dtl`.`active_flag`='".FLAG_Y."'
    AND `pvd`.`end_date` is NOT NULL
    AND DATE_FORMAT(`pvd`.`start_date`,'%Y-%m-%d')  BETWEEN '$start_date' AND '$end_date'
    AND `pvd`.`visit_name` LIKE CONCAT('%', IF('$visit_name' IS NULL OR '$visit_name'='',`pvd`.`visit_name`,'$visit_name') ,'%')
    AND `pvd`.`visit_mobile` LIKE CONCAT('%', IF('$visit_mobile' IS NULL OR '$visit_mobile'='',`pvd`.`visit_mobile`,'$visit_mobile') ,'%')
    AND IF('$tower_id' IS NULL OR '$tower_id'='',`pvd`.`tower_id`,'$tower_id') = `pvd`.`tower_id`
    AND IF('$flat_id'IS NULL OR '$flat_id'='',`pvd`.`flat_id`,'$flat_id') = `pvd`.`flat_id`
    ORDER BY `pvd`.`prk_visit_dtl_id` DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $resp['prk_visit_dtl_id'] =$val['prk_visit_dtl_id'];
            $resp['visit_name'] =$val['visit_name'];
            $resp['visit_gender'] =$val['visit_gender'];
            $resp['visit_mobile'] =$val['visit_mobile'];
            $resp['visit_purpose'] =$val['visit_purpose'];
            $resp['visit_meet_to'] =$val['visit_meet_to'];
            $resp['visit_address'] =$val['visit_address'];
            $resp['visit_with_veh'] =$val['visit_with_veh'];
            $resp['visit_card_id'] =$val['visit_card_id'];
            $resp['visit_img'] =$val['visit_img'];
            $resp['veh_type'] =$val['veh_type'];
            $resp['veh_number'] =$val['veh_number'];
            $resp['visiter_in_date'] =$val['visiter_in_date'];
            $resp['visiter_in_time'] =$val['visiter_in_time'];
            $resp['visiter_out_date'] =$val['visiter_out_date'];
            $resp['visiter_out_time'] =$val['visiter_out_time'];
            $resp['visitor_meet_to_name'] =($val['visitor_meet_to_name']=='Select Name')?'':$val['visitor_meet_to_name'];
            $resp['visitor_meet_to_address'] =$val['visitor_meet_to_address'];
            $resp['visitor_type'] = $val['visitor_type'];
            $resp['no_of_guest'] = $val['no_of_guest'];
            $re[$val['visiter_in_date']][]= $resp;
        }
        $response['status']=1;
        $response['message'] = 'Successful';
        $response['prk_area_name'] = $prk_area_name;
        $response['start_date'] = $start_date;
        $response['end_date'] = $end_date;
        $response['visiter'] = $re;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>