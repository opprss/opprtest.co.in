<?php 
/*
Description: Current all vehicle parking list 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function prk_veh_trc_dtl_list($prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
	$sql = "SELECT  `payment_receive`.`payment_receive_id`,
        `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id`,
        `payment_receive`.`in_out_flag`,
		`prk_veh_trc_dtl`.`prk_veh_type`,
		`prk_veh_trc_dtl`.`veh_number`,
		`prk_veh_trc_dtl`.`veh_token`,
		`prk_veh_trc_dtl`.`veh_out_verify_flag`,
     	DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%d-%m-%Y') AS `prk_veh_in_date`,
     	DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%I:%i %p') AS `prk_veh_in_time`,
     	DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%d-%m-%Y') AS `prk_veh_out_date`,
     	DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%I:%i %p') AS `prk_veh_out_time`,
		`vehicle_type`.`vehicle_typ_img`
		FROM `prk_veh_trc_dtl`join `vehicle_type` 
		on `prk_veh_trc_dtl`.`prk_veh_type`=`vehicle_type`.`vehicle_sort_nm`
        left join `payment_receive` 
        on `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id` =  `payment_receive`.`prk_veh_trc_dtl_id`
        where `prk_veh_trc_dtl`.`prk_admin_id`= '$prk_admin_id'
		AND `prk_veh_trc_dtl`.`prk_veh_out_time` IS NULL
		ORDER BY `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id` ASC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
	    $arr_catagory = $query->fetchAll();
	    foreach($arr_catagory as $val) 
	    {
	        $response['status'] = 1;
	        $response['message'] = 'data Successful';
	        $park['payment_receive_id'] = $val['payment_receive_id'];
	        $park['in_out_flag'] = $val['in_out_flag'];
	        $park['prk_veh_trc_dtl_id'] = $val['prk_veh_trc_dtl_id'];
	        $park['prk_veh_type'] = $val['prk_veh_type'];
	        $park['veh_number'] = $val['veh_number'];
	        $park['prk_veh_in_date'] = $val['prk_veh_in_date'];
	        $park['prk_veh_in_time'] = $val['prk_veh_in_time'];
	        $park['prk_veh_out_date'] = $val['prk_veh_out_date'];
	        $park['prk_veh_out_time'] = $val['prk_veh_out_time'];
	        $park['vehicle_typ_img'] = $val['vehicle_typ_img'];
	        $park['veh_token'] = $val['veh_token'];
	        $park['veh_out_verify_flag'] = $val['veh_out_verify_flag'];
	        array_push($allplayerdata, $park);
	        $response['parking'] = $allplayerdata;
	    }
	}else{
		$response['status'] = 0;
	    $response['message'] = 'List Empty';
	}
    return json_encode($response);
}
?>