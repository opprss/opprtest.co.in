<?php 
/*
Description: parking employ gate Logout.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :07-04-2018
*/ 
    // require_once 'api/global_api.php';
    require_once 'api/parkAreaReg_api.php';
    $response = array();
    if(isset($_REQUEST['one']) && isset($_REQUEST['two']) && isset($_REQUEST['prk_user_gate_login_id'])){

        $prk_admin_id = trim($_REQUEST['one']);
        $out_prk_user_username = trim($_REQUEST['two']);
        $prk_user_gate_login_id = trim($_REQUEST['prk_user_gate_login_id']);
       
        $respon = prk_user_gate_logout($prk_admin_id,$out_prk_user_username,$prk_user_gate_login_id);
        $json = json_decode($respon);

        if ($json->status) {
            unset($_SESSION["prk_area_gate_id"]);
            unset($_SESSION["prk_area_gate_type"]);
            unset($_SESSION["prk_qr_code"]);
            unset($_SESSION["prk_user_gate_login_id"]);
            header('location:../gate-list');
        }else
            header('location:../dashboard');    
    }else{
         header('location:../dashboard'); 
    }
 
?>