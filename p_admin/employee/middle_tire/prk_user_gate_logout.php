<?php 
/*
Description: parking employ gate Logout.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :07-04-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','out_prk_user_username','prk_user_gate_login_id'))){
    if(isEmpty(array('prk_admin_id','out_prk_user_username','prk_user_gate_login_id'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $out_prk_user_username = trim($_POST['out_prk_user_username']);
        $prk_user_gate_login_id = trim($_POST['prk_user_gate_login_id']);
       
        echo $respon = prk_user_gate_logout($prk_admin_id,$out_prk_user_username,$prk_user_gate_login_id);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        echo json_encode($response);
    }
}else{
     $response['status'] = 0; 
     $response['message'] = 'Invalid API Call';
     echo json_encode($response);
}
?>