<?php 
/*
Description: parking in vehicle gate pass find api
Developed by: Rakhal Raj Mandal
Created Date: 04-05-2018
Update date : -------
*/ 
function prk_gate_pass_find_search($prk_admin_id,$veh_number){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT `prk_gate_pass_dtl`.`prk_gate_pass_num`,
	`prk_gate_pass_dtl`.`veh_type`,
	`prk_gate_pass_dtl`.`mobile`, 
	`vehicle_type`.`vehicle_type_dec`,
	`vehicle_type`.`vehicle_typ_img`
	FROM `prk_gate_pass_dtl`,`vehicle_type` 
	WHERE `prk_gate_pass_dtl`.`prk_admin_id`='$prk_admin_id' 
	AND `prk_gate_pass_dtl`.`veh_number` LIKE '%$veh_number%' 
	AND `prk_gate_pass_dtl`.`active_flag`='".FLAG_Y."' 
	AND `prk_gate_pass_dtl`.`del_flag`='".FLAG_N."'
	AND `vehicle_type`.`vehicle_sort_nm` = `prk_gate_pass_dtl`.`veh_type`
	AND `vehicle_type`.`active_flag`= '".FLAG_Y."'
	limit 10";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
	    $arr_catagory = $query->fetchAll();
	    foreach($arr_catagory as $val) 
	    {
	        $response['status'] = 1;
	        $response['message'] = 'Successful';
	        $response['prk_gate_pass_num'] = $val['prk_gate_pass_num'];
	        $response['veh_type'] = $val['veh_type'];
	        $response['vehicle_type_dec'] = $val['vehicle_type_dec'];
	        $response['vehicle_typ_img'] = $val['vehicle_typ_img'];
	        $response['mobile'] = $val['mobile'];
	        $response['payment_type'] = DGP;
	        $response['payment_type_full'] = DGP_F;
	    }
	}else{
		$response['status'] = 0;
	    $response['message'] = 'Gate Pass Not Issue';
	}
    return json_encode($response);
}
?>