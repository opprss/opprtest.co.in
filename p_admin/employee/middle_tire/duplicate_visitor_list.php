<?php 
/*
Description: Parking employ Payment report download
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
include_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','visit_mobile','visit_name','start_date','end_date'))){
 	if(isEmpty(array('prk_admin_id'))){
	 	$prk_admin_id = trim($_POST['prk_admin_id']);
        $visit_name = $_POST['visit_name'];
	 	$visit_mobile = $_POST['visit_mobile'];
        $start_date =$_POST['start_date'];
        $end_date =$_POST['end_date'];
        $visit_name = isset($_REQUEST['visit_name']) ? trim($_REQUEST['visit_name']) : ''; 
        $visit_mobile = isset($_REQUEST['visit_mobile']) ? trim($_REQUEST['visit_mobile']) : ''; 
        $tower_id = isset($_REQUEST['tower_id']) ? trim($_REQUEST['tower_id']) : ''; 
        $flat_id = isset($_REQUEST['flat_id']) ? trim($_REQUEST['flat_id']) : ''; 
        $response = visit_in_list($prk_admin_id,$start_date,$end_date,$visit_name,$visit_mobile,$tower_id,$flat_id);
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>