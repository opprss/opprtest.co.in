<!-- ################################################
Description: User Address verify
Developed by: Rakhal Raj Mandal
Created Date: 27-08-2019
####################################################-->
<?php 
  include "all_nav/header.php";
   $respon=visitor_gate_pass_dtl_list($prk_admin_id);
   $respon = json_decode($respon, true);
?>
<style type="text/css">
  .size{
    font-size: 11px;
    text-transform: uppercase;
  }
  .error_size{
    font-size: 11px;
    color: red;
  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
  th,tr{
    text-align: center;
  }
</style> 
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
<div class="am-mainpanel"><!-- cloding in footer -->
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Visitor Gate Pass List</h5>
      <!--<form id="searchBar" class="search-bar" action="#">
      <div class="form-control-wrapper">
        <input type="search" class="form-control bd-0" placeholder="Search...">
      </div>
      <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
    </form>search-bar-->
  </div><!-- am-pagetitle -->
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40 col-md-12">
      <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
        <div class="col-md-4">
          <a style="margin-bottom: 10px;" href="visitor-gate-pass-issue" class="btn btn-primary pull-right">
                <i class="fa fa-plus"></i> Add Visitor
            </a>
        </div>
      </div>
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <div class="table-wrapper">
        <table id="datatable1" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="wd-5p">Image</th>
              <th class="">Name</th>
              <th class="wd-15p">Mobile</th>
              <th class="wd-15p">Effective Date</th>
              <th class="wd-15p">End Date</th>
              <th class="wd-5p">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            if($respon['status']){
            foreach($respon['state'] as $value){?>
            <tr>
              <td><img src="<?php echo $value['visitor_img']; ?>" style="height: 40px;"> </td>
              <td><?php echo $value['visitor_name']; ?></td>
              <td><?php echo $value['visitor_mobile']; ?></td>
              <td><?php echo $value['effective_date']; ?></td>
              <td><?php echo $value['end_date']; ?></td>
              <td>

                <a href="visitor-gate-pass-modify?list_id=<?php echo base64_encode($value['visitor_gate_pass_dtl_id']); ?>" data-toggle="tooltip" data-placement="top" title="Modify"><!-- <button type="button" class="btn btn-info btn-md"> --><i class="fa fa-edit" style="font-size:18px"></i><!-- </button> --></a>

                <button type="button" class="clean-button" name="delete" id="delete<%=count++%>" value="<?php echo $value['visitor_gate_pass_dtl_id']; ?>" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>

                <!-- <a href="" data-toggle="tooltip" data-placement="top" title="Modify"><button type="button" class="btn btn-info btn-md" value="" id="<?php echo base64_encode($value['visitor_gate_pass_dtl_id']); ?>" onclick="visitor_id_print(this.id)">Visitor ID</button></a> -->
             </td>
            </tr>
            <?php }} ?>
          </tbody>
        </table>
      </div>
      </div>
    </div>
  </div>
<?php include"all_nav/footer.php"; ?>
<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
</script>
<script type="text/javascript">
  $('button[id^="delete"]').on('click', function() {
    var visitor_gate_pass_dtl_id = this.value;
    // alert(visitor_gate_pass_dtl_id);
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the item permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            if (visitor_gate_pass_dtl_id != '') {
              var urlDtl = pkrEmpUrl+'visitor_gate_pass_issue_delete.php';
              // 'visitor_gate_pass_dtl_id','prk_admin_id','prk_area_user_id','prk_user_username','token'
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :
                {
                  'visitor_gate_pass_dtl_id':visitor_gate_pass_dtl_id,
                  'prk_admin_id':prk_admin_id,
                  'prk_area_user_id':prk_area_user_id,
                  'prk_user_username':prk_user_username,
                  'token':token
                },
                dataType:'html',
                success  :function(data)
                {
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Visitor Gate Pass Delete successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                            location.reload(true);
                        }
                      }
                    });
                  }else{
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  });
</script>
