<?php 
/*
Description: parking area visit the people all list. 
Developed by: Rakhal Raj Mandal
Created Date: 09-04-2018
Update date : 27-05-2018,28-05-2018
*/
function prk_visit_in_check($prk_admin_id,$visit_mobile){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT prk_visit_dtl_id,visit_name,visit_mobile FROM prk_visit_dtl
        WHERE end_date IS NULL
        AND prk_admin_id='$prk_admin_id'
        AND visit_mobile='$visit_mobile'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $response['status'] = 0;
        $response['message'] = 'Visitor is already inside the list';
    }else{
        $response['status'] = 1;
        $response['message'] = 'Successful';
    }
    return json_encode($response);
}
?> 