<?php
/*
Description: Parking area account status check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function p_employee_permi_form_list($prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT pup.permission_id,
        pup.prk_admin_id,
        pup.user_name,
        pup.form_id,
        fd.form_name,
        fd.form_display_name,
        fd.form_php_name
        FROM prk_user_permission pup,form_dtl fd
        WHERE pup.form_id=fd.form_id
        AND fd.active_flag='Y'
        AND pup.active_flag='Y'
        AND pup.del_flag='N'
        AND pup.prk_admin_id='$prk_admin_id' 
        ORDER BY fd.form_order ASC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['prk_admin_id'] = $val['prk_admin_id'];
            $park['form_id'] = $val['form_id'];
            $park['form_name'] = $val['form_name'];
            $park['form_display_name'] = $val['form_display_name'];
            $park['form_php_name'] = $val['form_php_name'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['from_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
function p_employee_permi_form($prk_admin_id,$prk_area_user_id,$user_name,$form_id,$inserted_by){
    $response = array();
    global $pdoconn;
     $sql = "DELETE FROM admin_prk_user_permission WHERE `prk_admin_id`='$prk_admin_id' AND `prk_area_user_id`='$prk_area_user_id'";   
        $pdoconn->exec($sql);
    foreach($form_id as $val){
        $sql="INSERT INTO `admin_prk_user_permission`(`prk_admin_id`, `prk_area_user_id`, `user_name`, `form_id`, `inserted_by`, `inserted_date`) VALUES ('$prk_admin_id','$prk_area_user_id','$user_name','$val','$inserted_by','".TIME."')";
        $sqls=$pdoconn->prepare($sql);
        if($sqls->execute()){
            $response['status'] = 1;
            $response['message'] = 'Permission Save';
        }else{
            $response['status'] = 0;
            $response['message'] = 'Permission Not Save ';
        }
    }
    return json_encode($response); 
}
function p_employee_form_permi_select_list($form_id,$prk_admin_id,$prk_area_user_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql="SELECT permission_id,user_name
        FROM admin_prk_user_permission
        WHERE del_flag='".FLAG_N."'
        AND active_flag='".FLAG_Y."'
        AND prk_admin_id='$prk_admin_id'
        AND prk_area_user_id='$prk_area_user_id'
        AND form_id='$form_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $response['status'] = 1;
        $response['message'] = 'Successful';
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>