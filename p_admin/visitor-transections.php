<!-- ################################################
  
Description: Parking area admin can add/delete parking rate by this web page.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
  include "all_nav/header.php";
  $response = prk_inf($prk_admin_id);
  $asas = $response;
  $response = json_decode($response, true);
  $parking_type = $response['parking_type'];
  $cal_md_val=($parking_type=='C')?'3':'2';
?>
<style>
  .size{
    font-size: 11px;
  }
  .success{
    font-size: 11px;
    color: green;
  }
  .error_size{
    font-size:11px;
    color: red;
  }
</style>
<link href="../lib/highlightjs/github.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../lib/select2/css/select2.min.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Visitor Transactions</h5>
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40" style="padding: 10px;">
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <form id="duplicatePayRecForm" action="" method="post">
          <div class="row">
            <div class="col-lg-10">
              <div class="row">
                <div class="col-lg-<?=$cal_md_val?>">
                  <div class="form-group">
                    <label class="form-control-label size">Visitor Name <span class="tx-danger"></span></label>
                    <input type="text" name="visit_name" id="visit_name" placeholder="Visitor Name" class="form-control">
                    <span style="position: absolute;" id="error_visit_name" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-<?=$cal_md_val?>">
                  <div class="form-group">
                    <label class="form-control-label size">Visitor Mobile <span class="tx-danger"></span></label>
                    <input type="text" name="visit_mobile" id="visit_mobile" placeholder="Mobile" class="form-control">
                    <span style="position: absolute;" id="error_visit_mobile" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2" style="display: <?=($parking_type=='C')?'none':''?>;">
                  <div class="form-group" style="margin-top: 10px;">
                    <label class="form-control-label size">Tower Name<span class="tx-danger"></span></label>
                    <select name="tower_id" id="tower_id" value="" style="opacity: 0.8; font-size:14px">
                      <option value="">Select Tower</option>
                    </select>
                    <span style="position: absolute;" id="error_tower_id" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2" style="display: <?=($parking_type=='C')?'none':''?>;">
                  <div class="form-group" style="margin-top: 10px;">
                    <label class="form-control-label size">Flat Name<span class="tx-danger"></span></label>
                    <select name="flat_id" id="flat_id" value="" style="opacity: 0.8; font-size:14px">
                      <option value="">Select Flat</option>
                    </select>
                    <span style="position: absolute;" id="error_flat_id" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-<?=$cal_md_val?>">
                  <div class="form-group">
                    <label class="form-control-label size">start DATE <span class="tx-danger">*</span></label>
                    <input type="text" name="start_date" id="start_date" placeholder="MM/DD/YYYY" class="form-control prevent_readonly" readonly="readonly">
                    <span style="position: absolute;" id="error_start_date" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-<?=$cal_md_val?>">
                  <div class="form-group">
                    <label class="form-control-label size">END DATE <span class="tx-danger">*</span></label>
                    <input type="text" name="end_date" id="end_date" placeholder="MM/DD/YYYY" class="form-control prevent_readonly" readonly="readonly">
                    <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-layout-footer mg-t-30">
                <button type="button" class="btn btn-block prk_button" id="show">Search</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>   
  <div class="am-pagebody">
    <div class="card">
      <div class="card pd-20 pd-sm-40" style="padding: 10px;">
        <div class="table-wrapper">
          <table id="datatable1" class="table display responsive nowrap" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="wd-5p">Img</th>
                <th class="wd-20p">Visitor Name</th>
                <th class="wd-10p">Visitor Mobile</th>
                <th class="wd-20p">Meet To Name</th>
                <th class="wd-20p">Meet To Mobile</th>
                <th class="wd-10p">In Date</th>
                <th class="wd-10p">In Time</th>
                <th class="wd-10p">Out Date</th>
                <th class="wd-10p">Out Time</th>
                <th class="wd-20p">Meet Address</th>
                <th class="wd-10p">Visitor Address</th>
                <th class="wd-10p">Purpose</th>
              </tr>
            </thead>
            <tbody id="dataLines">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include"all_nav/footer.php"; ?>
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="../lib/datatables-responsive/jquery.dataTables.min.js"></script>
<script src="../lib/datatables-responsive/dataTables.buttons.min.js"></script>
<script src="../lib/datatables-responsive/jszip.min.js"></script>
<script src="../lib/datatables-responsive/pdfmake.min.js"></script>
<script src="../lib/datatables-responsive/vfs_fonts.js"></script>
<script src="../lib/datatables-responsive/buttons.html5.min.js"></script>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var prk_admin_id = "<?=$prk_admin_id?>";
  var parking_admin_name = "<?=$parking_admin_name?>";
  var token = "<?=$token?>";
  $( document ).ready( function () {
    visitor_transactions_dtl('','','','','','');
    $("#start_date").blur(function () {
      var start_date = $('#start_date').val();
      if(start_date == ''){
        $('#error_start_date').text('Satrt date is required');
        return false;
      }else{
        $('#error_start_date').text('');
        return true;
      }
    });
    $("#end_date").blur(function () {
      var start_date = $('#end_date').val();
      if(start_date == ''){
        $('#error_end_date').text('End date is required');
        return false;
      }else{
        $('#error_end_date').text('');
        return true;
      }
    });
    $('#show').click(function () {
      var start_date = $('#start_date').val();
      var end_date = $('#end_date').val();
      var visit_name = $('#visit_name').val();
      var visit_mobile = $('#visit_mobile').val();
      var tower_id = $('#tower_id').val();
      var flat_id = $('#flat_id').val();
      if(start_date!='' && end_date!=''){
        // alert('ok');
        visitor_transactions_dtl(start_date,end_date,visit_name,visit_mobile,tower_id,flat_id);
      }else{
        if (start_date == '') {
          $('#error_start_date').text('Satrt date is required');
        }if (end_date == '') {
          $('#error_end_date').text('End date is required');
        }
      }
    });
    addEventListener('DOMContentLoaded', function () {
      var now = new Date;
      var now = now.setDate(now.getDate());
      pickmeup('#start_date', {
        default_date   : false,
        position       : 'bottom',
        hide_on_select : true,
        render : function (date) {
          if (now < date) {
              return {
                disabled : true,
                class_name : 'date-in-past'
              };
          }
          $('#error_effective_date').hide();
          return {};
        } 
      });
    });
    $('#end_date').focus(function () {
      var start = new Date;
      var end = $('#start_date').val();
      var d = new Date(end.split("-").reverse().join("-"));
      var dd=d.getDate();
      var mm=d.getMonth()+1;
      var yy=d.getFullYear();
      var hh = start.getHours();
      var min = start.getMinutes();
      var sec = start.getSeconds();
      var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
      var end = new Date(newdate);
      days = (end - start) / (1000 * 60 * 60 * 24)-1;
      days = Math.round(days);
      now_end = start.setDate(start.getDate() + days);
      pickmeup('#end_date', {
        default_date   : false,
        /*position       : 'top',*/
        hide_on_select : true,
        render : function (date) {
              if (date < now_end) {
                  return {
                    disabled : true,
                    class_name : 'date-in-past'
                  };
              }
              return {};
        } 
      });
    });
    $('#start_date').focus(function(){
      // code
      $('#end_date').val('');
    });
    $("#tower_id").select2();
    $("#flat_id").select2();
    var urlTowerList = prkUrl+'tower_list.php';
    // 'prk_admin_id','parking_admin_name','token'
    $.ajax ({
      type: 'POST',
      url: urlTowerList,
      data: {
        'prk_admin_id':prk_admin_id,
        'parking_admin_name':parking_admin_name,
        'token':token
      },
      success : function(data) {
        // alert(data);
        var obj=JSON.parse(data);
        //console.log(obj);
        var areaOption = "<option value=''>Select Tower</option>";
        $.each(obj['tower_list'], function(val, key) {
          areaOption += '<option value="' + key['tower_id'] + '">' + key['tower_name'] + '</option>'
        });
        $("#tower_id").html(areaOption);
        $("#tower_id").select2();
      }
    });
    $('#tower_id').on("change", function() {
      var id=($("#tower_id").val());
      var urlFlatList = prkUrl+'flat_list.php';
      // 'prk_admin_id','parking_admin_name','token','tower_id'
      $.ajax ({
        type: 'POST',
        url: urlFlatList,
        data: {
          'prk_admin_id':prk_admin_id,
          'parking_admin_name':parking_admin_name,
          'token':token,
          'tower_id':id
        },
        success : function(data) {
          // alert(data);
          var obj=JSON.parse(data);
          var areaOption = "<option value=''>Select Flat</option>";
          if (obj['status']) {
            $.each(obj['flat_list'], function(val, key) {
              areaOption += '<option value="' + key['flat_id'] + '">' + key['flat_name'] + '</option>'
            });
          }
          $("#flat_id").html(areaOption);
          $("#flat_id").select2();
        }
      });
    });
  });
  function visitor_transactions_dtl(start_date,end_date,visit_name,visit_mobile,tower_id,flat_id){
    var urlVehNoCheck = prkUrl+'visit_in_list.php';
    // 'prk_admin_id','start_date','end_date','token',visit_name,visit_mobile,tower_id,flat_id
    $.ajax({
      url :urlVehNoCheck,
      type:'POST',
      data :{
        'prk_admin_id':prk_admin_id,
        'visit_name':visit_name,
        'visit_mobile':visit_mobile,
        'tower_id':tower_id,
        'flat_id':flat_id,
        'start_date':start_date,
        'end_date':end_date,
        'token':token
      },
      dataType:'html',
      // dataType:'json',
      success  :function(data){
        // alert(data);
        $('#datatable1').dataTable().fnDestroy();
        var demoLines = '';
        var json = $.parseJSON(data);
        if (json.status) {
          // alert('ok');
          for (var key in json.visiter) {
            // alert(key);
            $.each(json.visiter[key], function() {
              // alert(this["visit_name"]);
              demoLines += '<tr>\
                <td class="wd-5p"><img src="'+this["visit_img"]+'" style="    height: 30px;"></td>\
                <td class="wd-20p">'+this["visit_name"]+'</td>\
                <td class="wd-10p">'+this["visit_mobile"]+'</td>\
                <td class="wd-20p">'+this["visitor_meet_to_name"]+'</td>\
                <td class="wd-20p">'+this["visit_meet_to"]+'</td>\
                <td class="wd-10p">'+this["visiter_in_date"]+'</td>\
                <td class="wd-10p">'+this["visiter_in_time"]+'</td>\
                <td class="wd-10p">'+this["visiter_out_date"]+'</td>\
                <td class="wd-10p">'+this["visiter_out_time"]+'</td>\
                <td class="wd-20p">'+this["visitor_meet_to_address"]+'</td>\
                <td class="wd-10p">'+this["visit_address"]+'</td>\
                <td class="wd-10p">'+this["visit_purpose"]+'</td>\
              </tr>';
            });
          }
        }else{
          // alert('No');
        }
        $("#dataLines").html(demoLines);
        datatable_show();
      }
    });
  }
  function datatable_show(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      language: {
      searchPlaceholder: 'Search Here',
      sSearch: '',
      lengthMenu: '_MENU_ Page',
      },
      // lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      buttons: [
        { 
          extend: 'copyHtml5', 
          footer: true,
          title: 'VISITOR TRANSACTION DETAILS SHEET' 
        },
        { 
          extend: 'excelHtml5', 
          footer: true,
          title: 'VISITOR TRANSACTION DETAILS SHEET'  
        },
        {
            extend: 'pdfHtml5',
            footer: true,
            title: 'VISITOR TRANSACTION DETAILS SHEET',
            customize: function(doc) {
              doc.content[1].margin = [ 0, 0, 0, 0 ] //left, top, right, bottom
            },
            exportOptions: {
                columns: [1,2,3,4,5,6,7,8,9]
            }
        },
      ]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  }
</script>