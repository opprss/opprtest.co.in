<?php
/*
Description: parking area vehicle verify show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
require_once 'api/parkAreaReg_api.php';
$response = array();

if(isAvailable(array('prk_user_name','prk_admin_id','prk_area_gate_id','prk_area_user_id','token'))){

	if(isEmpty(array('prk_user_name','prk_admin_id','prk_area_gate_id','prk_area_user_id','token'))){
	
	    $prk_user_name = trim($_POST['prk_user_name']);
	    $prk_admin_id = trim($_POST['prk_admin_id']);
	    $prk_area_gate_id = trim($_POST['prk_area_gate_id']);
	    $prk_area_user_id = trim($_POST['prk_area_user_id']);
	    $token = trim($_POST['token']);
	    global $pdoconn;
	    $sql = "SELECT * FROM `prk_token` WHERE `prk_area_user_id`='$prk_area_user_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".ACTIVE_FLAG_Y."' AND `token_value`='$token'";
	    $query  = $pdoconn->prepare($sql);
	    $query->execute();
	    $count=$query->rowCount();
	    if($count>0){
	        $response = user_prk_veh_verify_show($prk_user_name,$prk_admin_id,$prk_area_gate_id);
	        echo ($response); 
	    }else{ 
	        $response['status'] = 0; 
	        $response['session'] = 0;
	        $response['message'] =  'Session Expired Please Login Again';
	        echo json_encode($response);
	    }
	            
	}else{
	    $response['status'] = 0;
	    $response['message'] = 'All Fields Are Mandatory';
	    echo json_encode($response);
	}
}else{
 $response['status'] = 0; 
 $response['message'] = 'Invalid API Call';
 echo json_encode($response);
}
?>