<?php
/*
Description: user gate pass delete.
Developed by: Rakhal Raj Mandal
Created Date: 18-02-2018
Update date : 18-05-2018
*/
function user_address_delete($user_add_id,$user_admin_id,$mobile){
    global $pdoconn;
    $response = array();
    $sql = "UPDATE `user_address` SET `updated_by`='$mobile',`updated_date`='".TIME."',`del_flag`='".FLAG_Y."' WHERE `user_add_id`='$user_add_id' AND `user_admin_id`='$user_admin_id'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Delete Successful';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Delete Not Successful';
        return json_encode($response); 
    }
} 
?>