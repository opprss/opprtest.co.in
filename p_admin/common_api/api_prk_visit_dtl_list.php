<?php 
/*
Description: parking area visit the people all list. 
Developed by: Rakhal Raj Mandal
Created Date: 09-04-2018
Update date : 27-05-2018,28-05-2018
*/
function prk_visit_dtl_list($prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT `pvd`.`prk_visit_dtl_id`,
        `pvd`.`visit_name`,
        `pvd`.`visit_gender`,
        `pvd`.`visit_mobile`,
        `pvd`.`visit_purpose`,
        `pvd`.`visit_meet_to`,
        `pvd`.`visit_address`,
        `pvd`.`visit_with_veh`,
        `pvd`.`visit_card_id`,
        (CASE pvd.visitor_type WHEN 'O' THEN IF(pvd.visit_img IS NULL or pvd.visit_img = '', 
                (CASE WHEN pvd.visit_gender= '".FLAG_M."' then '".MALE_IMG_D."' WHEN pvd.visit_gender= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".PRK_BASE_URL."',pvd.visit_img))
    
            WHEN 'V' THEN (SELECT IF(vgp.visitor_img IS NULL or vgp.visitor_img = '', (CASE WHEN pvd.visit_gender= '".FLAG_M."' then '".MALE_IMG_D."' WHEN pvd.visit_gender= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".PRK_BASE_URL."',vgp.visitor_img)) FROM visitor_gate_pass_dtl vgp WHERE visitor_gate_pass_dtl_id=pvd.unique_id)

            WHEN 'C' THEN (SELECT IF(cgp.visitor_img IS NULL or cgp.visitor_img = '', (CASE WHEN pvd.visit_gender= '".FLAG_M."' then '".MALE_IMG_D."' WHEN pvd.visit_gender= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".PRK_BASE_URL."',cgp.visitor_img)) FROM contractual_gate_pass cgp WHERE cgp.contractual_gate_pass_dtl_id=pvd.unique_id)
 
            WHEN 'G' THEN IF(pvd.visit_img IS NULL or pvd.visit_img = '', (CASE WHEN pvd.visit_gender= '".FLAG_M."' then '".MALE_IMG_D."' WHEN pvd.visit_gender= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".PRK_BASE_URL."',pvd.visit_img))
 
            WHEN 'U' THEN (SELECT IF(ud.user_img IS NULL or ud.user_img = '', (CASE WHEN pvd.visit_gender= '".FLAG_M."' then '".MALE_IMG_D."' WHEN pvd.visit_gender= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".USER_BASE_URL."',ud.user_img)) FROM user_detail ud where ud.user_admin_id= pvd.unique_id AND ud.active_flag='".FLAG_Y."') 
        end) as 'visit_img',
        `pvd`.`veh_type`,
        IF(`pvd`.`visitor_in_verify_flag`='".FLAG_Y."', (CASE `pvd`.`visitor_out_verify_flag` WHEN '".FLAG_N."' THEN 'IN VERIFIED' WHEN '".FLAG_Y."' THEN 'VERIFIED' WHEN '".FLAG_B."' THEN 'BLOCKED' end), (CASE `pvd`.`visitor_in_verify_flag` WHEN '".FLAG_D."' THEN 'PENDING' WHEN '".FLAG_Y."' THEN 'IN VERIFIED' WHEN '".FLAG_R."' THEN 'REJECTED' end)) as 'in_verify_status',
        `pvd`.`visitor_in_verify_flag`,
        IF(`pvd`.`visitor_in_verify_flag`='".FLAG_D."' OR `pvd`.`visitor_in_verify_flag`='".FLAG_R."', '".FLAG_N."', `pvd`.`visitor_out_verify_flag`) as `visitor_out_verify_flag`,
        `pvd`.`no_of_guest`,
        `pvd`.`visitor_in_verify_date`,
        `pvd`.`visitor_out_verify_date`,
        `pvd`.`visitor_meet_to_name`,
        IF(`pvd`.`visitor_meet_to_address` IS NULL or `pvd`.`visitor_meet_to_address`='', 'NA', `pvd`.`visitor_meet_to_address`) as 'visitor_meet_to_address',
        (SELECT `ad`.`full_name` FROM `all_drop_down` `ad` where `ad`.`category`='VIS_L' AND `ad`.`sort_name`= `pvd`.`visitor_type`) as 'visitor_type',
        `pvd`.`visitor_token`,
        `vehicle_type`.`vehicle_type_dec`,
        `pvd`.`veh_number`,
        `prk_area_dtl`.`prk_area_name`,
        DATE_FORMAT(`pvd`.`start_date`,'%d-%m-%Y') AS `visit_in_date`,
        DATE_FORMAT(`pvd`.`start_date`,'%I:%i %p') AS `visit_in_time` 
        FROM `prk_visit_dtl` pvd join `prk_area_dtl` 
        on `pvd`.`prk_admin_id` = `prk_area_dtl`.`prk_admin_id`
        left join `vehicle_type` 
        on `pvd`.`veh_type`  = `vehicle_type`.`vehicle_sort_nm`
        WHERE `pvd`.`prk_admin_id`='$prk_admin_id' 
        AND `prk_area_dtl`.`active_flag` = '".FLAG_Y."'
        AND `pvd`.`end_date` is NULL 
        ORDER BY 1 desc";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['prk_visit_dtl_id'] = $val['prk_visit_dtl_id'];
            $park['visit_name'] = $val['visit_name'];
            $park['visit_gender'] = $val['visit_gender'];
            $park['visit_mobile'] = $val['visit_mobile'];
            $park['visit_purpose'] = $val['visit_purpose'];
            $park['visit_meet_to'] = $val['visit_meet_to'];
            $park['visit_address'] = $val['visit_address'];
            $park['visit_with_veh'] = $val['visit_with_veh'];
            $park['visit_card_id'] = $val['visit_card_id'];
            $park['visit_img'] = $val['visit_img'];
            $park['veh_type'] = $val['veh_type'];
            $park['vehicle_type_dec'] = $val['vehicle_type_dec'];
            $park['veh_number'] = $val['veh_number'];
            $park['visit_in_date'] = $val['visit_in_date'];
            $park['visit_in_time'] = $val['visit_in_time'];
            $park['in_verify_status'] = $val['in_verify_status'];
            $park['visitor_in_verify_flag'] = $val['visitor_in_verify_flag'];
            $park['visitor_in_verify_date'] = $val['visitor_in_verify_date'];
            $park['visitor_out_verify_flag'] = $val['visitor_out_verify_flag'];
            $park['visitor_out_verify_date'] = $val['visitor_out_verify_date'];
            $park['visitor_meet_to_name'] = $val['visitor_meet_to_name'];
            $park['visitor_meet_to_address'] = $val['visitor_meet_to_address'];
            $park['visitor_token'] = $val['visitor_token'];
            $park['visitor_type'] = $val['visitor_type'];
            $park['no_of_guest'] = $val['no_of_guest'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['session'] = 1;
        $response['message'] = 'Successful';
        $response['prk_area_name'] = $val['prk_area_name'];
        $response['visit'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['session'] = 1;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?> 