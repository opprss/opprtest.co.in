<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','prk_area_user_id','prk_user_username','token','tower_id','flat_id'))){
    if(isEmpty(array('prk_admin_id','prk_area_user_id','prk_user_username','token','tower_id','flat_id'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $prk_user_username = trim($_POST['prk_user_username']);
        $token = trim($_POST['token']);
        $tower_name = trim($_POST['tower_id']);
        $flat_name = trim($_POST['flat_id']);
        // $user_ins_number = isset($_POST['user_ins_number']) ? trim($_POST['user_ins_number']) : '';
        $owner_name = isset($_POST['owner_name']) ? trim($_POST['owner_name']) : '';
        // $owner_name = trim($_POST['owner_name']);
        $living_status = isset($_POST['living_status']) ? trim($_POST['living_status']) : '';
        // $living_status = trim($_POST['living_status']);
        $tenant_name = isset($_POST['tenant_name']) ? trim($_POST['tenant_name']) : '';
        // $tenant_name = trim($_POST['tenant_name']);
        $effective_date = isset($_POST['effective_date']) ? trim($_POST['effective_date']) : '';
        // $effective_date = trim($_POST['effective_date']);
        $end_date = isset($_POST['end_date']) ? trim($_POST['end_date']) : '';
        // $end_date = trim($_POST['end_date']);
        $parking_admin_name = $prk_user_username;
        // $parking_admin_name = trim($_POST['parking_admin_name']);
        $crud_type = isset($_POST['crud_type']) ? trim($_POST['crud_type']) : 'S';
        // $crud_type = trim($_POST['crud_type']);
        $flat_master_id = isset($_POST['flat_master_id']) ? trim($_POST['flat_master_id']) : '';
        // $flat_master_id = trim($_POST['flat_master_id']);
        $two_wh_spa_allw = isset($_POST['two_wh_spa_allw']) ? trim($_POST['two_wh_spa_allw']) : '';
        // $two_wh_spa_allw = trim($_POST['two_wh_spa_allw']);
        $two_wh_spa = isset($_POST['two_wh_spa']) ? trim($_POST['two_wh_spa']) : '';
        // $two_wh_spa = trim($_POST['two_wh_spa']);
        $four_wh_spa_allw = isset($_POST['four_wh_spa_allw']) ? trim($_POST['four_wh_spa_allw']) : '';
        // $four_wh_spa_allw = trim($_POST['four_wh_spa_allw']);
        $four_wh_spa = isset($_POST['four_wh_spa']) ? trim($_POST['four_wh_spa']) : '';
        // $four_wh_spa = trim($_POST['four_wh_spa']);
        $contact_number = isset($_POST['contact_number']) ? trim($_POST['contact_number']) : '';
        // $contact_number = trim($_POST['contact_number']);
        $living_name = isset($_POST['living_name']) ? trim($_POST['living_name']) : '';
        // $living_name = trim($_POST['living_name']);
        $app_use = isset($_POST['app_use']) ? trim($_POST['app_use']) : 'N'; 
        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $response = flat_master($prk_admin_id,$tower_name,$flat_name,$owner_name,$living_status,$tenant_name,$effective_date,$end_date,$parking_admin_name,$crud_type,$flat_master_id,$two_wh_spa_allw,$four_wh_spa_allw,$two_wh_spa,$four_wh_spa,$contact_number,$living_name,$app_use);
        }else{
            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
 }
echo $response;
?>