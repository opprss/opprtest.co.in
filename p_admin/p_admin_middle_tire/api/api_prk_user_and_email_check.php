<?php
/*
Description: Parking area registration email check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_user_and_email_check($user_name,$email){
    global $pdoconn;
    $response = array();
        $sql="SELECT `prk_area_admin`.`prk_admin_id` FROM `prk_area_admin`,`prk_area_dtl` where `prk_area_admin`.`prk_admin_id` = `prk_area_dtl`.`prk_admin_id` AND `prk_area_dtl`.`active_flag`='".FLAG_Y."' AND `prk_area_admin`.`prk_admin_name`='$user_name' AND `prk_area_dtl`.`prk_admin_id` IN (SELECT `prk_admin_id` FROM `prk_area_dtl` WHERE `prk_area_email` = '$email')";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $count=$query->rowCount();
        if($count>0){
            $response['status'] = 1;
            $response['message'] = 'Right Email Or User Name';
            return json_encode($response);
        }else{
            $response['status'] = 0;
            $response['message'] = 'Email and Username mismatch';
            return json_encode($response);
        }
}
?>