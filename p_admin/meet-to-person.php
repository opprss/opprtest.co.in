<!-- ################################################
  
Description: Parking area can add/delete parking gate with description.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php include "all_nav/header.php";
  $me_to_per_id = isset($_REQUEST['list_id']) ? trim($_REQUEST['list_id']) : ''; 
  $meet_to_person_show1 = meet_to_person_show($prk_admin_id,$me_to_per_id);
  $meet_to_per_show = json_decode($meet_to_person_show1, true);
  $tage_show='Office Member Add';
  $tage_sh='ADD';
  $me_to_per_name='';
  $me_to_per_mobile='';
  $me_to_per_department='';
  $eff_date='';
  $end_date='';
  $tage_url='meet_to_person_add';
  if($meet_to_per_show['status']){
    $tage_show='Office Member Modify';
    $tage_sh='MOD';
    $me_to_per_id=$meet_to_per_show['me_to_per_id'];
    $me_to_per_name=$meet_to_per_show['me_to_per_name'];
    $me_to_per_mobile=$meet_to_per_show['me_to_per_mobile'];
    $me_to_per_department=$meet_to_per_show['me_to_per_department'];
    $eff_date=$meet_to_per_show['eff_date'];
    $end_date=$meet_to_per_show['end_date'];
    $tage_url='meet_to_person_edite';
  }
?>
<style type="text/css">
  .size{
    font-size: 11px;
  }
  .error{
    font-size: 11px;
    color: red;
  }
  .error_size{
    font-size: 11px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
  input::-webkit-input-placeholder {
    font-size: 11px;
    line-height: 3;
  }
</style>
<link href="../lib/highlightjs/github.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../lib/select2/css/select2.min.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title"><?=$tage_show?></h5>
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40" >
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <form id="meet_to_per">
          <div class="row mg-b-25">
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">NAME <span class="tx-danger">*</span></label>
                <input type="text" class="form-control" placeholder="Name" name="me_to_per_name" id="me_to_per_name">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">MOBILE NUMBER <span class="tx-danger">*</span></label>
                <input type="text" class="form-control" placeholder="Mobile Number" name="me_to_per_mobile" id="me_to_per_mobile">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">DEPARMENT<span class="tx-danger"></span></label>
                <input type="text" class="form-control" placeholder="Deparment" name="me_to_per_department" id="me_to_per_department">
              </div>
            </div>
            <div class="col-lg-3">
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label size">EFFECTIVE DATE<span class="tx-danger">*</span></label>
                    <input type="text" name="eff_date" id="eff_date" placeholder="DD-MM-YYYY" class="form-control prevent_readonly" readonly="readonly">
                    <span style="position: absolute; display: none;" id="error_eff_date" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label size">END DATE</label>
                    <input type="text" name="end_date" id="end_date" placeholder="DD-MM-YYYY" class="form-control prevent_readonly" readonly="readonly">
                   <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-layout-footer mg-t-30">
                <div class="row">
                  <div class="col-lg-6 pd-b-10">
                    <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
                  </div>
                  <div class="col-lg-6">
                    <a href="meet-to-person-list"><button class="btn btn-block btn-primary prk_button" type="button">BACK</button></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php include"all_nav/footer.php"; ?>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE_WEB = "<?php echo FUTURE_DATE_WEB?>";
  var prk_admin_id="<?=$prk_admin_id?>"
  var parking_admin_name="<?=$parking_admin_name?>"
  var token="<?=$token?>"
  var tage_sh="<?=$tage_sh?>";
  var tage_url="<?=$tage_url?>";
  var me_to_per_id="<?=$me_to_per_id?>";
  if(tage_sh=='MOD'){
    $('#me_to_per_name').val("<?=$me_to_per_name?>");
    $('#me_to_per_mobile').val("<?=$me_to_per_mobile?>");
    $('#me_to_per_department').val("<?=$me_to_per_department?>");
    $('#eff_date').val("<?=$eff_date?>");
    if(FUTURE_DATE_WEB!="<?=$end_date?>"){
      $('#end_date').val("<?=$end_date?>");
    }
  }
  $( document ).ready( function () {
    $( "#meet_to_per" ).validate( {
      rules: {
        me_to_per_name: "required",
        me_to_per_mobile: {
          required: true,
          number: true,
          minlength: 10,
          maxlength: 10
        },
        eff_date: "required"
      },
      messages: {
        n_per_mobile: {
          required: true,
          required: 'This field is required.',
          number: 'Enter valid mobile Number.',
          minlength: 'Minimum Length 10th Digit.',
          maxlength: 'Maximum Length 10th Digit.'
        }
      }
    });
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>
<script type="text/javascript">
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#eff_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
        if (date < now) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        return {};
      } 
    });
  });
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#eff_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      hide_on_select : true,
      render : function (date) {
        if (date < now_end) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        return {};
      } 
    });
  });
  $('#eff_date').focus(function(){
    if(tage_sh=='ADD'){
      $('#end_date').val('');
    }
  });
</script>
<script type="text/javascript">
  $('#save').click(function(){
    var me_to_per_name=$("#me_to_per_name").val();
    var me_to_per_mobile=$("#me_to_per_mobile").val();
    var me_to_per_department=$("#me_to_per_department").val();
    var eff_date=$("#eff_date").val();
    var end_date=$("#end_date").val();
    if($("#meet_to_per").valid()){
      $('#save').val('Wait ...').prop('disabled', true);
      var urlPrkAreGateDtl = prkUrl+''+tage_url;
      // 'prk_admin_id','parking_admin_name','token','me_to_per_name','me_to_per_mobile','me_to_per_department','eff_date','end_date'
      $.ajax({
        url :urlPrkAreGateDtl,
        type:'POST',
        data :{
          'prk_admin_id':prk_admin_id,
          'parking_admin_name':parking_admin_name,
          'token': token,
          'me_to_per_name':me_to_per_name,
          'me_to_per_mobile':me_to_per_mobile,
          'me_to_per_department':me_to_per_department,
          'eff_date':eff_date,
          'end_date':end_date,
          'me_to_per_id':me_to_per_id
        },
        dataType:'html',
        success  :function(data){
          $('#save').val('SAVE').prop('disabled', false);
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>Successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                  // location.reload(true);
                  window.location='meet-to-person-list';
                  // $('#meet_to_per')[0].reset();
                }
              }
            });
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>"+json.message+"</p>",
              type: 'red'
            });
            }
          }
        }
      });
    }
  });
</script>