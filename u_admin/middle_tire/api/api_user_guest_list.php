<?php 
/*
Description: subumit lisst.
Developed by: Rakhal Raj Mandal
Created Date: 14-06-2018
Update date :---------
*/
function user_guest_list($user_admin_id){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
	$sql="SELECT `user_guest_list`.`user_guest_list_id`,
		`user_guest_list`.`guest_name`,
		`user_guest_list`.`guest_mobile`,
		`user_guest_list`.`guest_gender`,
		`user_guest_list`.`guest_address`,
		`user_guest_list`.`guest_purpose`,
		`user_guest_list`.`no_of_guest`,
		`user_guest_list`.`guest_id_proof`,
		`user_guest_list`.`user_admin_id`,
		`user_guest_list`.`prk_admin_id`,
		`prk_area_dtl`.`prk_area_name`,
		`user_guest_list`.`in_date`,
		`user_guest_list`.`to_time`,
		`user_guest_list`.`form_time`,
		(CASE `user_guest_list`.`visitor_in_complete` WHEN '".FLAG_Y."' THEN '".FLAG_R."' WHEN '".FLAG_N."' THEN (IF(CONCAT(SUBSTRING_INDEX(SUBSTRING_INDEX(`user_guest_list`.`in_date`,'-',3),'-',-1),'-',SUBSTRING_INDEX(SUBSTRING_INDEX(`user_guest_list`.`in_date`,'-',2),'-',-1),'-',SUBSTRING_INDEX(`user_guest_list`.`in_date`,'-',1))>='".TIME_TRN."', 'M', 'R')) end) as permission,
		`user_guest_list`.`guest_in_verify`,
		`user_guest_list`.`guest_out_verify`
		FROM `user_guest_list`,`prk_area_dtl` 
		WHERE `user_guest_list`.`user_admin_id` ='$user_admin_id'
		AND `prk_area_dtl`.`prk_admin_id`=`user_guest_list`.`prk_admin_id`
        AND `prk_area_dtl`.`active_flag`='".FLAG_Y."'
		AND `user_guest_list`.`active_flag`='".FLAG_Y."'
		AND `user_guest_list`.`del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
	    $arr_catagory = $query->fetchAll();
	    foreach($arr_catagory as $val)
	    {
	        $park['user_guest_list_id'] = $val['user_guest_list_id'];
	        $park['guest_name'] = $val['guest_name'];
	        $park['guest_mobile'] = $val['guest_mobile'];
	        $park['prk_admin_id'] = $val['prk_admin_id'];
	        $park['prk_area_name'] = $val['prk_area_name'];
	        $park['guest_gender'] = $val['guest_gender'];
	        $park['guest_address'] = $val['guest_address'];
	        $park['guest_purpose'] = $val['guest_purpose'];
	        $park['no_of_guest'] = $val['no_of_guest'];
	        $park['guest_id_proof'] = $val['guest_id_proof'];
	        $park['in_date'] = $val['in_date'];
	        $park['to_time'] = $val['to_time'];
	        $park['form_time'] = $val['form_time'];
	        $park['permission'] = $val['permission'];
	        $park['guest_in_verify'] = $val['guest_in_verify'];
	        $park['guest_out_verify'] = $val['guest_out_verify'];
	        array_push($allplayerdata, $park);
	    }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['guest_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>