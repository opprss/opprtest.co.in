<?php 
/*
Description: Parking visiter in list
Developed by: Rakhal Raj Mandal
Created Date: 31-05-2018
Update date : 
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','token'))){
    if(isEmpty(array('prk_admin_id','prk_area_user_id','token'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $token = trim($_POST['token']);
        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $response = visitor_vehicle_parking_space_list($prk_admin_id); 
        }else{

            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response); 
} 
echo $response;
?> 