<?php
/*
Description: user licence add.
Developed by: Rakhal Raj Mandal/Manoranjan Sarkar
Created Date: 08-05-2018
Update date : -------
*/
@include '../../../../library/dompdf/autoload.inc.php';
@include '../../../library/dompdf/autoload.inc.php';
@include '../../library/dompdf/autoload.inc.php';
@include '../library/dompdf/autoload.inc.php';
use Dompdf\Dompdf;
//ob_start();
function user_payment_history_pdf($response){
    $res=json_decode($response,true);
    $words=api_number_to_word($res['total']);
    $html="<html>
        <head>
        <style>
        .tbl_body {
            width:92%;
            border-collapse: collapse;
            margin-left: 50px;
        }
        th,td{
            border-bottom: 1px solid #ddd;
            padding: 4px;
        }
        .center_align{
        }
        .header{
            float: left;
            margin: 1px 0 50px;
        }
        </style>
        </head>
        <body>
        <div>
        <div class='header' style='width:400px;padding-left: 62px;'><img style='height: 70px;padding-left: 50px;' src='../../img/logo_pdf.png'></div>
        <div class='header' style='width:300px;'>".$res['user_name']."<br>User Payment Report</div>
        <div class='header' style='width:200px;'>Start Date: ".$res['start_date']."<br>End Date: ".$res['end_date']."</div>
        </div>
        <table class='tbl_body' style='padding-top:100px;'>
            <tr>
                <th>Vehicle Type</th>
                <th>Vehicle Number</th>
                <th>Vehicle In Date</th>
                <th>Vehicle Out Date</th>
                <th>Payment Type</th>
                <th>Total Pay</th>
            </tr>";
            $html3="";
            foreach ($res['payment_history'] as $key => $item1){
                foreach ($item1 as $item){
                    // echo $item['veh_number'];
                    $html1="<tr class='center_align'>
                                <td>".$item['vehicle_type_dec']."</td>
                                <td>".$item['veh_number']."</td>
                                <td>".$item['veh_in_date']."</td>
                                <td>".$item['veh_out_date']."</td>
                                <td>".$item['payment_type']."</td>
                                <td>".$item['total_pay']."</td>
                            </tr>";
                    $html3.=$html1;
                }
            }

            $html2="<tr>
                <th colspan='4'></th>
                <th colspan='2'>Grand Total RS. ".$res['total']."/-</th>
            </tr>
            <tr>
                <td colspan='6'>RUPEES: ".$words."</td>
            </tr>
        </table>
        </body>
        </html>";
    // echo ($mess);
    $html=$html."".$html3."".$html2;
    // echo ($html);
    // use Dompdf\Dompdf;
    // instantiate and use the dompdf class
    $dompdf = new Dompdf();
    $dompdf->loadHtml($html);
    // (Optional) Setup the paper size and orientation
    $dompdf->setPaper('A4', 'landscape');
    // Render the HTML as PDF
    $dompdf->render();
    // Output the generated PDF to Browser
    $name= uniqid();
    /*web pdf*/
        // $fulpath=$name.'.pdf';
        // $dompdf->stream($name,array('Attachment'=>0));
    /*web pdf end*/
    /*mobile pdf*/
    $filename='../user-pay-pdf/';
    if (!file_exists($filename)) {
        $old_mask = umask(0);
        mkdir($filename, 0777, TRUE);
        umask($old_mask);
    }
    $fulpath=$filename.'/'.$name.".pdf";
    $path='user-pay-pdf/'.$name.".pdf";
    if(file_put_contents($fulpath, $dompdf->output())){
        $report['status']=1;
        $report['url']= $path;
        $report['message']='PDF Created Successful';
    }else{
        $report['status']=0;
        $report['message']='PDF Created Failed';
    }
    return json_encode($report);
}
?>