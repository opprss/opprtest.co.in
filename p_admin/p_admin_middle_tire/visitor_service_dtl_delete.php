<?php 
/*
Description: Parking visiter in list
Developed by: Rakhal Raj Mandal
Created Date: 31-05-2018
Update date : 
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','visitor_service_dtl_id','parking_admin_name'))){
    if(isEmpty(array('prk_admin_id','visitor_service_dtl_id','parking_admin_name'))){            
        $prk_admin_id = trim($_POST['prk_admin_id']);            
        $visitor_service_dtl_id = trim($_POST['visitor_service_dtl_id']);
        $parking_admin_name = trim($_POST['parking_admin_name']);
        //call global
        $response = visitor_service_dtl_delete($prk_admin_id,$visitor_service_dtl_id,$parking_admin_name);
     
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response); 
} 
echo $response;
?> 