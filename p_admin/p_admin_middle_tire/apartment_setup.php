<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','parking_admin_name','token','last_due_day','choose_due_day','flat_sq_ft','rate_sq_ft','maint_start_date','adv_payment_month','due_payment_month','crud_type','late_fine_payable_time','late_fine_charrge','late_fine_rate_type','state_mant_date'))){
    if(isEmpty(array('prk_admin_id','parking_admin_name','token','crud_type'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
        $parking_admin_name = trim($_POST['parking_admin_name']);
        $token = trim($_POST['token']);
        $last_due_day = trim($_POST['last_due_day']);
        $choose_due_day = trim($_POST['choose_due_day']);
        $flat_sq_ft = trim($_POST['flat_sq_ft']);
        $rate_sq_ft = trim($_POST['rate_sq_ft']);
        $maint_start_date = trim($_POST['maint_start_date']);
        $adv_payment_month = trim($_POST['adv_payment_month']);
        $due_payment_month = trim($_POST['due_payment_month']);
        $crud_type = trim($_POST['crud_type']);
        $late_fine_payable_time = trim($_POST['late_fine_payable_time']);
        $late_fine_charrge = trim($_POST['late_fine_charrge']);
        $late_fine_rate_type = trim($_POST['late_fine_rate_type']);
        $state_mant_date = trim($_POST['state_mant_date']);
        $resp=prk_token_check($prk_admin_id,$token);
            $json = json_decode($resp);
            if($json->status){
                $response = apartment_setup($prk_admin_id,$parking_admin_name,$last_due_day,$choose_due_day,$flat_sq_ft,$rate_sq_ft,$maint_start_date,$adv_payment_month,$due_payment_month,$crud_type,$late_fine_payable_time,$late_fine_charrge,$late_fine_rate_type,$state_mant_date);
            }else{
                $response = $resp;
            }
    }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
 }
echo $response;
?>