<?php
/*
Description: user token check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function veh_out_otp_check($prk_veh_trc_dtl_id,$veh_out_otp){
    global $pdoconn;
    $response = array();

    $sql="SELECT * FROM prk_veh_trc_dtl where prk_veh_trc_dtl_id = $prk_veh_trc_dtl_id and veh_out_otp=$veh_out_otp and payment_status='".FLAG_F."'";
    $query = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $response['status'] = 1;
        $response['message'] = 'OTP Match';
        $return = json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'OTP Not Match';
        $return = json_encode($response);
    }
    return $return;
}
?>