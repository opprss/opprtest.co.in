<?php
/*
Description: Parking gate pass insert.
Developed by: Rakhal Raj Mandal
Created Date: 31-03-2018
Update date : 27-05-2018
*/ 
function prk_gate_pass_dtl($prk_admin_id,$veh_type,$veh_number,$prk_sub_unit_id,$mobile,$end_date,$veh_owner_name,$effective_date,$prk_gate_pass_num,$user_name,$tower_id,$flat_id,$living_status,$park_lot_no){
    global $pdoconn;
    $response = array();
    $gate_pass_add_ch=prk_gate_pass_dtl_add_check($prk_admin_id,$veh_type,$veh_number);
    $json = json_decode($gate_pass_add_ch);
    if($json->status=='1'){     
        $end_date=(empty($end_date))?FUTURE_DATE_WEB:$end_date;
        $user_admin_id='';
        $user_ac_status='';
        $user_veh_insert='';
        $password='';
        if(!empty($mobile)){
            $draft_r=draft_registration($mobile,$veh_type,$veh_number);
            $json = json_decode($draft_r);
            if($status=$json->status=='1'){ 
                $user_admin_id=$json->user_admin_id;
                $user_ac_status=$json->user_ac_status;
                $user_veh_insert=$json->user_veh_insert;
                $password=$json->password;
            }
        }
        #---------------------------#
        $sql = "INSERT INTO `prk_gate_pass_dtl`(`prk_admin_id`,`user_admin_id`,`veh_type`,`veh_number`,`prk_sub_unit_id`,`mobile`,`end_date`,`veh_owner_name`,`effective_date`,`prk_gate_pass_num`,`inserted_by`,`inserted_date`,`tower_id`,`flat_id`,`living_status`,`park_lot_no`) VALUE ('$prk_admin_id','$user_admin_id','$veh_type','$veh_number','$prk_sub_unit_id','$mobile','$end_date','$veh_owner_name','$effective_date','$prk_gate_pass_num','$user_name','".TIME."','$tower_id','$flat_id','$living_status','$park_lot_no')";
        $query = $pdoconn->prepare($sql);
        if($query->execute()){
            if(!empty($mobile)&&!empty($tower_id)&&!empty($flat_id)&&!empty($veh_owner_name)){
                $user_email=$user_gender=$oth1=$oth2=$oth3=$oth4=$oth5='';
                connect_flat_master($prk_admin_id,$veh_owner_name,$mobile,$user_email,$user_gender,$tower_id,$flat_id,$user_name,$oth1,$oth2,$oth3,$oth4,$oth5);
            }
            if (!empty($user_admin_id)) {
                $prk_admin_name='';
                $prk_in =prk_inf($prk_admin_id);
                $json = json_decode($prk_in);
                if($json->status=='1'){

                    $prk_admin_name=$json->prk_area_name;
                }
                $user_gate = user_gate_pass_mess($mobile,$prk_gate_pass_num,$veh_type,$veh_number,$prk_admin_name);
                $json = json_decode($user_gate);
                if($json->status=='1'){
                    if($user_ac_status== FLAG_D){
                        $draft_reg = draft_registration_sms($password,$mobile);
                    }
                    if($user_veh_insert== FLAG_Y){
                        vehicle_add_user_mess($mobile,$veh_type,$veh_number);
                    }                    
                    $response['status'] = 1;
                    $response['user_admin_id'] = $user_admin_id;
                    $response['message'] = 'Gate Pass Generator Successful';
                    $response = json_encode($response);
                }else{
                    $response['status'] = 0;
                    $response['message'] = 'Gate Pass Not Generator';
                    $response = json_encode($response);                   
                }
            }else{
                $response['status'] = 1;
                $response['message'] = 'Gate Pass Generator Successful with out Mobile';
                $response = json_encode($response);
            }
        }else{
            $response['status'] = 0;
            $response['message'] = 'Data Not Save';
            $response = json_encode($response);                   
        }
    }else{
        $response = $gate_pass_add_ch;
    }
    return $response;
}
function prk_gate_pass_dtl_add_check($prk_admin_id,$veh_type,$veh_number){
    global $pdoconn;
    $sql = "SELECT `prk_gate_pass_id` FROM `prk_gate_pass_dtl` WHERE `prk_admin_id`='$prk_admin_id' AND `veh_number`='$veh_number' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $response['status'] = 0;
        $response['message'] = 'Already Gate Pass Always this Vehicle';
    }else{
        $response['status'] = 1;
        $response['message'] = 'Successful';
    }
    return json_encode($response);                   
}
?>