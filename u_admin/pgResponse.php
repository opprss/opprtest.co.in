<?php
	header("Pragma: no-cache");
	header("Cache-Control: no-cache");
	header("Expires: 0");
	$list_id=($_REQUEST['aParam']);
	// following files need to be included
	require_once("./lib/config_paytm.php");
	require_once("./lib/encdec_paytm.php");
	$paytmChecksum = "";
	$paramList = array();
	$isValidChecksum = "FALSE";
	$paramList = $_POST;
	/*$paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; 
	$isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); 
	if($isValidChecksum == "TRUE") {
		// echo "<b>Checksum matched and following are the transaction details:</b>" . "<br/>";
		if ($_POST["STATUS"] == "TXN_SUCCESS") {
			// echo "<b>Transaction status is success</b>" . "<br/>";
		}else {
			// echo "<b>Transaction status is failure</b>" . "<br/>";
		}
		// if (isset($_POST) && count($_POST)>0 ){ 
		// 	foreach($_POST as $paramName => $paramValue) {
		// 		echo "<br/>" . $paramName . " = " . $paramValue;
		// 	}
		// }
		// 'user_admin_id','mobile','token','payment_company','payment_for','transaction_id_for','status','checksumhash','mid','orderid','bankname','txnamount','txndate','txnid','respcode','paymentmode','banktxnid','currency','gatewayname','respmsg'
	}else {
		echo "<b>Checksum mismatched.</b>";
		//Process transaction as suspicious.
	}*/
	$orderid=isset($_POST["ORDERID"]) ? trim($_POST['ORDERID']) : ''; 
	$mid=isset($_POST["MID"]) ? trim($_POST['MID']) : '';
	$txnid=isset($_POST["TXNID"]) ? trim($_POST['TXNID']) : '';
	$txnamount=isset($_POST["TXNAMOUNT"]) ? trim($_POST['TXNAMOUNT']) : '';
	$paymentmode=isset($_POST["PAYMENTMODE"]) ? trim($_POST['PAYMENTMODE']) : '';
	$currency=isset($_POST["CURRENCY"]) ? trim($_POST['CURRENCY']) : '';
	$txndate=isset($_POST["TXNDATE"]) ? trim($_POST['TXNDATE']) : '';
	$status=isset($_POST["STATUS"]) ? trim($_POST['STATUS']) : '';
	$respcode=isset($_POST["RESPCODE"]) ? trim($_POST['RESPCODE']) : '';
	$respmsg=isset($_POST["RESPMSG"]) ? trim($_POST['RESPMSG']) : '';
	$gatewayname=isset($_POST["GATEWAYNAME"]) ? trim($_POST['GATEWAYNAME']) : '';
	$banktxnid=isset($_POST["BANKTXNID"]) ? trim($_POST['BANKTXNID']) : '';
	$bankname=isset($_POST["BANKNAME"]) ? trim($_POST['BANKNAME']) : '';
	$checksumhash=isset($_POST["CHECKSUMHASH"]) ? trim($_POST['CHECKSUMHASH']) : '';
	// echo('<br>old page data<br>');
	// print_r($list_id);
	$user_admin_id=$list_id['user_admin_id'];
	$mobile=$list_id['mobile'];
	$token=$list_id['token'];
	$tower_id=$list_id['tower_id'];
	$flat_id=$list_id['flat_id'];
	// $flat_master_id=$list_id['flat_master_id'];
	// $flat_size=$list_id['flat_size'];
	$owner_id=$list_id['owner_id'];
	// $owner_name=$list_id['owner_name'];
	// $living_status=$list_id['living_status'];
	$tenant_id=$list_id['tenant_id'];
	// $tenant_name=$list_id['tenant_name'];
	// $effective_date=$list_id['effective_date'];
	// $owner_email=$list_id['owner_email'];
	// $tenant_email=$list_id['tenant_email'];
	$m_account_number=$list_id['m_account_number'];
	// $flat_name=$list_id['json_flat_name'];
	// $tower_name=$list_id['json_tower_name'];
	// $main_tran_by=$list_id['main_tran_by'];
	$prk_admin_id=$list_id['prk_admin_id'];
	$amount=$list_id['amount'];
	// $payment_type=$list_id['payment_type'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Payment Process</title>
<link href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style type="text/css">
    body {
		font-family: 'Varela Round', sans-serif;
	}
	.modal-confirm {		
		color: #434e65;
		width: 525px;
	}
	.modal-confirm .modal-content {
		padding: 20px;
		font-size: 16px;
		border-radius: 5px;
		border: none;
	}
	.modal-confirm .modal-header {
		background: #47c9a2;
		border-bottom: none;   
        position: relative;
		text-align: center;
		margin: -20px -20px 0;
		border-radius: 5px 5px 0 0;
		padding: 35px;
	}
	.modal-confirm .modal-header2 {
		background: #e85e6c;
		border-bottom: none;   
        position: relative;
		text-align: center;
		margin: -20px -20px 0;
		border-radius: 5px 5px 0 0;
		padding: 35px;
	}
	.modal-confirm h4 {
		text-align: center;
		font-size: 36px;
		margin: 10px 0;
	}
	.modal-confirm .form-control, .modal-confirm .btn {
		min-height: 40px;
		border-radius: 3px; 
	}
	.modal-confirm .close {
        position: absolute;
		top: 15px;
		right: 15px;
		color: #fff;
		text-shadow: none;
		opacity: 0.5;
	}
	.modal-confirm .close:hover {
		opacity: 0.8;
	}
	.modal-confirm .icon-box {
		color: #fff;		
		width: 95px;
		height: 95px;
		display: inline-block;
		border-radius: 50%;
		z-index: 9;
		border: 5px solid #fff;
		padding: 15px;
		text-align: center;
	}
	.modal-confirm .icon-box i {
		font-size: 64px;
		margin: -4px 0 0 -4px;
	}
	.modal-confirm.modal-dialog {
		margin-top: 80px;
	}
    .modal-confirm .btn {
        color: #fff;
        border-radius: 4px;
		background: #eeb711;
		text-decoration: none;
		transition: all 0.4s;
        line-height: normal;
		border-radius: 30px;
		margin-top: 10px;
		padding: 6px 20px;
        border: none;
    }
	.modal-confirm .btn:hover, .modal-confirm .btn:focus {
		background: #eda645;
		outline: none;
	}
	.modal-confirm .btn span {
		margin: 1px 3px 0;
		float: left;
	}
	.modal-confirm .btn i {
		margin-left: 1px;
		font-size: 20px;
		float: right;
	}
	.trigger-btn {
		display: inline-block;
		margin: 100px auto;
	}
</style>
</head>
<body>
<div class="text-center">
	<!-- Button HTML (to Trigger Modal) -->
	<a href="#myModal" class="trigger-btn successfully" data-toggle="modal"></a>
	<!-- <a href="#myModal" class="trigger-btn successfully" data-toggle="modal">Click to Open Success Modal</a> -->
</div>

<!-- Modal HTML -->
<div id="myModal" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="material-icons">&#xE876;</i>
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body text-center">
				<h4>Payment Successfully</h4>	
				<div class="successful_date"><p>Your account has been created successfully.</p></div>
				<button class="btn btn-success" data-dismiss="modal" onclick="page_redirect()"><span>Go to Home Page</span> <i class="material-icons">&#xE5C8;</i></button>
			</div>
		</div>
	</div>
</div> 
<div class="text-center">
	<!-- Button HTML (to Trigger Modal) -->
	<a href="#myModal2" class="trigger-btn cancle" data-toggle="modal"></a>
	<!-- <a href="#myModal2" class="trigger-btn cancle" data-toggle="modal">Click to Open Error Modal</a> -->
</div>

<!-- Modal HTML -->
<div id="myModal2" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header2">
				<div class="icon-box">
					<i class="material-icons">&#xE5CD;</i>
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body text-center">
				<h4>Ooops! Payment Cancel</h4>	
				<div class="successful_date"><p>Something went wrong. File was not uploaded.</p></div>
				<button class="btn btn-success" data-dismiss="modal" onclick="page_redirect()"><span>Go to Home Page</span> <i class="material-icons">&#xE5C8;</i></button>
			</div>
		</div>
	</div>
</div>  
<script type="text/javascript">
	var user_admin_id= "<?=$user_admin_id?>";
	var mobile= "<?=$mobile?>";
	var token= "<?=$token?>";
	var prk_admin_id= "<?=$prk_admin_id?>";
	var m_account_number= "<?=$m_account_number?>";
	var tower_id = "<?=$tower_id?>";
	var flat_id = "<?=$flat_id?>";
	var owner_id = "<?=$owner_id?>";
	var tenant_id = "<?=$tenant_id?>";
	var amount= "<?=$amount?>";
	var orderid= "<?=$orderid?>";
	var areaOption="<p>Order ID: "+orderid+"</p>"
	$(".successful_date").html(areaOption);
	$( document ).ready( function () {
		$('#myModal').on('hidden.bs.modal', function () {
			// alert('successfully');
		 	// location.reload();
		 	page_redirect();
		});
		$('#myModal2').on('hidden.bs.modal', function () {
			// alert('cancle');
		 	// location.reload();
		 	page_redirect();
		});
		// $(".foo").trigger('click');
		// maintenance_pay();
		online_transaction();
	});
	function maintenance_pay(online_payment_dtl_id,orderid){
		var payment_ty = "PAYTM";
		// var payment_ty = "<?//=$payment_type?>";
		var bank_na = '';
		var cheque_num = '';
		var urlmaint = 'middle_tire/maintenance_pay.php';
      	// 'user_admin_id','mobile','token','prk_admin_id','m_account_number','tower_id','flat_id','owner_id','tenant_id','amount','payment_type','bank_name','cheque_number'
      	$.ajax({  
	        url:urlmaint,  
	        method:"POST",  
	        data: {
	          'user_admin_id':user_admin_id,
	          'mobile':mobile,
	          'token':token,
	          'prk_admin_id':prk_admin_id,
	          'm_account_number':m_account_number,
	          'tower_id':tower_id,
	          'flat_id':flat_id,
	          'owner_id':owner_id,
	          'tenant_id':tenant_id,
	          'amount':amount,
	          'payment_type':payment_ty,
	          'bank_name':bank_na,
	          'cheque_number':cheque_num,
	          'online_payment_dtl_id':online_payment_dtl_id,
	          'orderid':orderid
	        },  
	        success:function(data){  
				// alert(data);
				$(".successfully").trigger('click');
	        }  
      	});
	}
	function online_transaction(){
		var mid= "<?=$mid?>";
		var txnid= "<?=$txnid?>";
		var txnamount= "<?=$txnamount?>";
		var paymentmode= "<?=$paymentmode?>";
		var currency= "<?=$currency?>";
		var txndate= "<?=$txndate?>";
		var status= "<?=$status?>";
		var respcode= "<?=$respcode?>";
		var respmsg= "<?=$respmsg?>";
		var gatewayname= "<?=$gatewayname?>";
		var banktxnid= "<?=$banktxnid?>";
		var bankname= "<?=$bankname?>";
		var checksumhash= "<?=$checksumhash?>";
		var payment_company='PAYTM'
		var payment_for='MAINTENANCE_PAY'
		var transaction_id_for='NA';
		var urlmaint = 'middle_tire/online_payment_dtl_add.php';
      	// 'user_admin_id','mobile','token','payment_company','payment_for','transaction_id_for','status','checksumhash','mid','orderid','bankname','txnamount','txndate','txnid','respcode','paymentmode','banktxnid','currency','gatewayname','respmsg'
      	$.ajax({  
	        url:urlmaint,  
	        method:"POST",  
	        data: {
	          'user_admin_id':user_admin_id,
	          'mobile':mobile,
	          'token':token,
	          'payment_company':payment_company,
	          'payment_for':payment_for,
	          'transaction_id_for':transaction_id_for,
	          'status':status,
	          'checksumhash':checksumhash,
	          'mid':mid,
	          'orderid':orderid,
	          'bankname':bankname,
	          'txnamount':txnamount,
	          'txndate':txndate,
	          'txnid':txnid,
	          'respcode':respcode,
	          'paymentmode':paymentmode,
	          'banktxnid':banktxnid,
	          'currency':currency,
	          'gatewayname':gatewayname,
	          'respmsg':respmsg
	        },  
	        success:function(data){  
				// alert(data);
				// online_payment_dtl_id
				if(status=='TXN_SUCCESS'){
					var json = $.parseJSON(data);
      				if (json.status) {
      					var online_payment_dtl_id=json.online_payment_dtl_id;
      					maintenance_pay(online_payment_dtl_id,orderid);
      				}else{
      					alert('Technical Problem Contact to OPPRSS')
      				}
				}else{
					//-----------------//
					$(".cancle").trigger('click');
					// alert("Na");
				}
	        }  
      	});
	}
	function page_redirect(){
		// window.location='vehicle-list';
		window.location='maintenance-area';
	}
</script>  
</body>
</html>