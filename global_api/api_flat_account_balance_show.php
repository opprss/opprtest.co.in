<?php
/*
Description: Parking area sub unit insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function flat_account_balance_show($prk_admin_id,$m_account_number){
    global $pdoconn;
    $response = array();
    $sql = "SELECT `account_due` FROM `flat_account` WHERE `prk_admin_id`='$prk_admin_id' AND `m_account_number`='$m_account_number' AND `acc_status`='".FLAG_A."' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query = $pdoconn->prepare($sql);
    $query->execute();
    if($query->execute()){
        $val = $query->fetch();
        $account_due = $val['account_due'];
        $due_balance = ($account_due<0)?0:$account_due;
        $advance_balance = ($account_due<0)?($account_due*-1):0;
        $response['status'] = 1;
        $response['message'] = 'Sucessfull';
        $response['due_balance'] = $due_balance;
        $response['advance_balance'] = $advance_balance;
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not sucessfull';
        return json_encode($response);
    }
}
?>