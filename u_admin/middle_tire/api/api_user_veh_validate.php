<?php
/*
Description: user vehicle validate.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_veh_validate($user_admin_id,$user_veh_dtl_id){
   global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT `user_mobile`,`user_nick_name`,`user_veh_type`,`user_veh_number`FROM `user_admin`,`user_vehicle_detail`,`user_detail` WHERE `user_detail`.`user_admin_id` ='$user_admin_id' AND `user_admin`.`user_admin_id` ='$user_admin_id' AND `user_vehicle_detail`.`active_flag`='".FLAG_Y."' AND `user_vehicle_detail`.`user_veh_dtl_id` ='$user_veh_dtl_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $response['status'] = 1;
    $response['message'] = 'Successful';
    $response['user_nick_name'] = $val['user_nick_name'];
    $response['user_mobile'] = $val['user_mobile'];
    $response['user_veh_type'] = $val['user_veh_type'];
    $response['user_veh_number'] = $val['user_veh_number'];
    return json_encode($response);
}
?>