<?php 
/*
Description: Parking visiter in list
Developed by: Rakhal Raj Mandal
Created Date: 31-05-2018
Update date : 
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','prk_user_username','token','prk_gate_pass_id','user_admin_id','tower_id','tower_name','flat_id','flat_name','owner_name','mobile','veh_type','veh_number','allocated_park_space','park_space','remarks','park_veh_for','vehicle_type_dec'))){
    if(isEmpty(array('prk_admin_id','prk_area_user_id','prk_user_username','token','prk_gate_pass_id','tower_id','tower_name','flat_id','flat_name','owner_name','mobile','veh_type','veh_number','park_veh_for','vehicle_type_dec'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $inserted_by = trim($_POST['prk_user_username']);
        $token = trim($_POST['token']);
        $prk_gate_pass_id = trim($_POST['prk_gate_pass_id']);
        $user_admin_id = trim($_POST['user_admin_id']);
        $tower_id = trim($_POST['tower_id']);
        $tower_name = trim($_POST['tower_name']);
        $flat_id = trim($_POST['flat_id']);
        $flat_name = trim($_POST['flat_name']);
        $owner_name = trim($_POST['owner_name']);
        $mobile = trim($_POST['mobile']);
        $veh_type = trim($_POST['veh_type']);
        $veh_number = trim($_POST['veh_number']);
        $allocated_park_space = trim($_POST['allocated_park_space']);
        $park_space = trim($_POST['park_space']);
        $remarks = trim($_POST['remarks']);
        $park_veh_for = trim($_POST['park_veh_for']);
        $vehicle_type_dec = trim($_POST['vehicle_type_dec']);
        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            // $response = vehicle_audit_check_add($prk_admin_id,$prk_gate_pass_id,$user_admin_id,$veh_type,$veh_number,$allocated_park_space,$park_space,$remarks,$inserted_by);
            $response = vehicle_audit_check_add($prk_admin_id,$prk_gate_pass_id,$user_admin_id,$tower_id,$tower_name,$flat_id,$flat_name,$owner_name,$mobile,$veh_type,$veh_number,$allocated_park_space,$park_space,$remarks,$park_veh_for,$inserted_by,$vehicle_type_dec);
        }else{

            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response); 
} 
echo $response;
?> 