<?php
/*
Description: Parking area login.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
    // require_once 'api/global_api.php';
    require_once 'api/parkAreaReg_api.php';
    $response = array();
 
    if(isAvailable(array('prk_admin_name','prk_admin_password'))){

        if(isEmpty(array('prk_admin_name','prk_admin_password'))){
            $prk_admin_name = trim($_POST['prk_admin_name']);
            $prk_admin_password = trim($_POST['prk_admin_password']);
            $respon = prk_area_login($prk_admin_name,$prk_admin_password);
            
            echo ($respon);
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
        }
    }else{
         $response['status'] = 0; 
         $response['message'] = 'Invalid API Call';
         echo json_encode($response);
     }
?>