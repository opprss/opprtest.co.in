<!-- ################################################
Description: List of user's vechile | user can delete vehicle from this page.
Developed by: Rakhal raj mandal
Created Date: 16-10-2019
 ####################################################-->
<?php 
  include "all_nav/header.php";
  $list_id = isset($_REQUEST['list_id']) ? trim($_REQUEST['list_id']) : '';
  $user_apar_id =base64_decode($list_id);
  $resebutton_show='';
  $button_call_md=4;
  $user_apar_name='';
  $user_apar_address='';
  $user_apar_type='';
  $user_apar_configuration='';
  $user_apar_house_no='';
  $user_apar_built_up_area='';
  $user_apar_bedrooms='';
  $user_apar_bathrooms='';
  $user_apar_balconies='';
  $user_apar_furnished='';
  $user_apar_wardrobe='';
  $user_apar_fans='';
  $user_apar_light='';
  $user_apar_modular_kitchen='';
  $user_apar_furnished_name='';
  $user_apar_reserved_parking='';
  $save_url='user_apartment_add.php';
  $heding_show='User Property Add';
  if (!empty($user_apar_id)) {
    $resebutton_show='none';
    $button_call_md=6;
    $save_url='user_apartment_modify.php';
    $heding_show='User Property Modify';
    $apartment_show1 = user_apartment_show($user_admin_id,$user_apar_id);
    $apartment_show = json_decode($apartment_show1, true);
    if($apartment_show['status']){
      // $user_apar_id=$apartment_show['user_apar_id'];
      $user_apar_name=$apartment_show['user_apar_name'];
      $user_apar_address=$apartment_show['user_apar_address'];
      $user_apar_type=$apartment_show['user_apar_type'];
      $user_apar_configuration=$apartment_show['user_apar_configuration'];
      $user_apar_house_no=$apartment_show['user_apar_house_no'];
      $user_apar_built_up_area=$apartment_show['user_apar_built_up_area'];
      $user_apar_bedrooms=$apartment_show['user_apar_bedrooms'];
      $user_apar_bathrooms=$apartment_show['user_apar_bathrooms'];
      $user_apar_balconies=$apartment_show['user_apar_balconies'];
      $user_apar_furnished=$apartment_show['user_apar_furnished'];
      $user_apar_wardrobe=$apartment_show['user_apar_wardrobe'];
      $user_apar_fans=$apartment_show['user_apar_fans'];
      $user_apar_light=$apartment_show['user_apar_light'];
      $user_apar_modular_kitchen=$apartment_show['user_apar_modular_kitchen'];
      $user_apar_furnished_name=$apartment_show['user_apar_furnished_name'];
      $user_apar_reserved_parking=$apartment_show['user_apar_reserved_parking'];
    }
  }
  $property_type1=common_drop_down_list('PROPERTY_TYPE');
  $property_type = json_decode($property_type1, true);
  $property_config1=common_drop_down_list('PROPERTY_CONFIGURATION');
  $property_config = json_decode($property_config1, true);
  $property_furnished1=common_drop_down_list('PROPERTY_FURNISHED');
  $property_furnished = json_decode($property_furnished1, true);
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
  .error{
    font-size: 11px;
    color: red;
  }
  .furnished_dtl{
    display: none;
  }
</style>
<!-- Time Picker -->
  <script src="js/timepicker.min.js"></script>
  <link href="css/timepicker.min.css" rel="stylesheet">
  <script src="../lib/validation/jquery.validate.js"></script>
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
  <div class="am-mainpanel"><!-- cloding in footer -->
    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title"><?=$heding_show ?></h5>
    </div><!-- am-pagetitle -->
    
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40">
        <!-- <h6 class="card-body-title">Add Employee</h6> -->
        <div class="editable tx-16 bd pd-30 tx-inverse">
          <form role="form" id="addApartment" method="post"  enctype="multipart/form-data">
          <div class="row mg-b-5">
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Property Name<span class="tx-danger">*</span></label>
                <input type="text"  name="user_apar_name" id="user_apar_name" class="form-control" placeholder="Property Name">
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Property Address<span class="tx-danger">*</span></label>
                <input type="text" name="user_apar_address" id="user_apar_address" class="form-control" placeholder="Property Address">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Property Type<span class="tx-danger">*</span></label>
                <div class="select" style="font-size:14px">
                  <select name="user_apar_type" id="user_apar_type">
                    <option value="">Property Type</option>
                    <?php
                      if($property_type['status']){
                      foreach ($property_type['drop_down'] as $val){ ?>  
                        <option value="<?=$val['sort_name']?>"><?=$val['full_name']?></option>
                    <?php }}?> 
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Property Size<span class="tx-danger">*</span></label>
                <div class="select" style="font-size:14px">
                  <select name="user_apar_configuration" id="user_apar_configuration">
                    <option value="">Select Type</option>
                    <?php
                      if($property_config['status']){
                      foreach ($property_config['drop_down'] as $val){ ?>  
                        <option value="<?=$val['sort_name']?>"><?=$val['full_name']?></option>
                    <?php }}?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">House No<span class="tx-danger">*</span></label>
                <input type="text" name="user_apar_house_no" id="user_apar_house_no" class="form-control" style="border-right-style: none; border-left-style: none; border-top-style: none; border-color: #00b8d4; border-width: 1px;" placeholder="House No">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Built Up Area(sq.ft)<span class="tx-danger">*</span></label>
                <input type="text" name="user_apar_built_up_area" id="user_apar_built_up_area" class="form-control" style="border-right-style: none; border-left-style: none; border-top-style: none; border-color: #00b8d4; border-width: 1px;" placeholder="Built Up Area">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">No of Bedrooms<span class="tx-danger">*</span></label>
                <input type="text" name="user_apar_bedrooms" id="user_apar_bedrooms" class="form-control" style="border-right-style: none; border-left-style: none; border-top-style: none; border-color: #00b8d4; border-width: 1px;" placeholder="Bedrooms">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">No of Bathrooms<span class="tx-danger">*</span></label>
                <input type="text" name="user_apar_bathrooms" id="user_apar_bathrooms" class="form-control" style="border-right-style: none; border-left-style: none; border-top-style: none; border-color: #00b8d4; border-width: 1px;" placeholder="Bathrooms">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">No of Balconies<span class="tx-danger">*</span></label>
                <input type="text" name="user_apar_balconies" id="user_apar_balconies" class="form-control" style="border-right-style: none; border-left-style: none; border-top-style: none; border-color: #00b8d4; border-width: 1px;" placeholder="Balconies">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Reserved Parking<span class="tx-danger">*</span></label>
                <div class="select" style=" margin-top: -4px; font-size:14px">
                  <select name="user_apar_reserved_parking" id="user_apar_reserved_parking">
                    <option value="">Select Type</option>
                    <option value="Y">Yes</option>
                    <option value="N">No</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Furnished<span class="tx-danger">*</span></label>
                <div class="select" style=" margin-top: -4px; font-size:14px">
                  <select name="user_apar_furnished" id="user_apar_furnished">
                    <option value="">Select Type</option>
                    <?php
                      if($property_furnished['status']){
                      foreach ($property_furnished['drop_down'] as $val){ ?>  
                        <option value="<?=$val['sort_name']?>"><?=$val['full_name']?></option>
                    <?php }}?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-2 furnished_dtl">
              <div class="form-group">
                <label class="form-control-label size">No of Wardrobes<span class="tx-danger">*</span></label>
                <input type="text" name="user_apar_wardrobe" id="user_apar_wardrobe" class="form-control" style="border-right-style: none; border-left-style: none; border-top-style: none; border-color: #00b8d4; border-width: 1px;" placeholder="Wardrobe">
              </div>
            </div>
            <div class="col-lg-2 furnished_dtl">
              <div class="form-group">
                <label class="form-control-label size">No of Fans<span class="tx-danger">*</span></label>
                <input type="text" name="user_apar_fans" id="user_apar_fans" class="form-control" style="border-right-style: none; border-left-style: none; border-top-style: none; border-color: #00b8d4; border-width: 1px;" placeholder="Fans">
              </div>
            </div>
            <div class="col-lg-2 furnished_dtl">
              <div class="form-group">
                <label class="form-control-label size">No of Lights<span class="tx-danger">*</span></label>
                <input type="text" name="user_apar_light" id="user_apar_light" class="form-control" style="border-right-style: none; border-left-style: none; border-top-style: none; border-color: #00b8d4; border-width: 1px;" placeholder="Light">
              </div>
            </div>
            <div class="col-lg-2 furnished_dtl">
              <div class="form-group">
                <label class="form-control-label size">Modular Kitchen<span class="tx-danger">*</span></label>
                <div class="select" style=" margin-top: -4px; font-size:14px">
                  <select name="user_apar_modular_kitchen" id="user_apar_modular_kitchen">
                    <option value="">Select Type</option>
                    <option value="Y">Yes</option>
                    <option value="N">No</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-4 furnished_dtl">
              <div class="form-group">
                <label class="form-control-label size">Any Other Furnished Name<span class="tx-danger">*</span></label>
                <input type="text" name="user_apar_furnished_name" id="user_apar_furnished_name" class="form-control" style="border-right-style: none; border-left-style: none; border-top-style: none; border-color: #00b8d4; border-width: 1px;" placeholder="Furnished Name">
              </div>
            </div>
            <div class="col-lg-5">
              <div class="row mg-t-35">
                <div class="col-lg-<?=$button_call_md?>">
                    <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="add_apartment" id="add_apartment">
                </div>
                <div class="col-lg-<?=$button_call_md?>" style="display: <?=$resebutton_show?>">
                  <button type="reset" class="btn btn-block prk_button btn-primary">Reset</button>
                </div>
                <div class="col-lg-<?=$button_call_md?>">
                  <button type="button" class="btn btn-block prk_button btn-primary" onclick="document.location.href='user-apartment-list';">Back</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div><!-- am-pagebody -->
  </div>
  <!-- footer part -->
  <?php include"all_nav/footer.php"; ?>
  <!-- footer part -->
<script type="text/javascript">
  $('#user_apar_name').val("<?=$user_apar_name?>");
  $('#user_apar_type').val("<?=$user_apar_type?>");
  $('#user_apar_configuration').val("<?=$user_apar_configuration?>");
  $('#user_apar_house_no').val("<?=$user_apar_house_no?>");
  $('#user_apar_built_up_area').val("<?=$user_apar_built_up_area?>");
  $('#user_apar_bedrooms').val("<?=$user_apar_bedrooms?>");
  $('#user_apar_bathrooms').val("<?=$user_apar_bathrooms?>");
  $('#user_apar_balconies').val("<?=$user_apar_balconies?>");
  $('#user_apar_furnished').val("<?=$user_apar_furnished?>");
  $('#user_apar_wardrobe').val("<?=$user_apar_wardrobe?>");
  $('#user_apar_fans').val("<?=$user_apar_fans?>");
  $('#user_apar_light').val("<?=$user_apar_light?>");
  $('#user_apar_modular_kitchen').val("<?=$user_apar_modular_kitchen?>");
  $('#user_apar_furnished_name').val("<?=$user_apar_furnished_name?>");
  $('#user_apar_reserved_parking').val("<?=$user_apar_reserved_parking?>");
  $('#user_apar_address').val("<?=$user_apar_address?>");
  furnished_dtl_show("<?=$user_apar_furnished?>");
  var userUrl = "<?php echo USER_URL; ?>";
  var user_admin_id = "<?php echo $user_admin_id; ?>";
  var token = "<?php echo $token; ?>";
  var mobile = "<?php echo $user_mobile; ?>";
  var user_apar_id = "<?=$user_apar_id; ?>";
  var save_url = "<?=$save_url; ?>";
  // alert(mobile);
  $( document ).ready( function () {  
    $('#add_apartment').click(function () {
      if($("#addApartment").valid()){
        // alert('ok');
        var user_apar_name=$('#user_apar_name').val();
        var user_apar_type=$('#user_apar_type').val();
        var user_apar_configuration=$('#user_apar_configuration').val();
        var user_apar_house_no=$('#user_apar_house_no').val();
        var user_apar_built_up_area=$('#user_apar_built_up_area').val();
        var user_apar_bedrooms=$('#user_apar_bedrooms').val();
        var user_apar_bathrooms=$('#user_apar_bathrooms').val();
        var user_apar_balconies=$('#user_apar_balconies').val();
        var user_apar_furnished=$('#user_apar_furnished').val();
        var user_apar_wardrobe=$('#user_apar_wardrobe').val();
        var user_apar_fans=$('#user_apar_fans').val();
        var user_apar_light=$('#user_apar_light').val();
        var user_apar_modular_kitchen=$('#user_apar_modular_kitchen').val();
        var user_apar_furnished_name=$('#user_apar_furnished_name').val();
        var user_apar_reserved_parking=$('#user_apar_reserved_parking').val();
        var user_apar_address=$('#user_apar_address').val();
        $(this).val('Wait ...').prop('disabled', true);
        var apartment_add = userUrl+''+save_url;
        // 'user_admin_id','mobile','token','user_apar_name','user_apar_address'
        $.ajax({
          url :apartment_add,
          type:'POST',
          data :{
            'user_admin_id':user_admin_id,
            'mobile':mobile,
            'token':token,
            'user_apar_name':user_apar_name,
            'user_apar_type':user_apar_type,
            'user_apar_configuration':user_apar_configuration,
            'user_apar_house_no':user_apar_house_no,
            'user_apar_built_up_area':user_apar_built_up_area,
            'user_apar_bedrooms':user_apar_bedrooms,
            'user_apar_bathrooms':user_apar_bathrooms,
            'user_apar_balconies':user_apar_balconies,
            'user_apar_furnished':user_apar_furnished,
            'user_apar_wardrobe':user_apar_wardrobe,
            'user_apar_fans':user_apar_fans,
            'user_apar_light':user_apar_light,
            'user_apar_modular_kitchen':user_apar_modular_kitchen,
            'user_apar_furnished_name':user_apar_furnished_name,
            'user_apar_reserved_parking':user_apar_reserved_parking,
            'user_apar_address':user_apar_address,
            'user_apar_id':user_apar_id
          },
          dataType:'html',
          success  :function(data){
            $('#add_apartment').val('SAVE').prop('disabled', false);
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.9em;'>Successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                      window.location='user-apartment-list';
                      // location.reload();
                  }
                }
              });
            }else{
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
                type: 'red'
              });
              if (json.session==0) {
                window.location.replace("logout.php");
              }
            }
          }
        });
      }
    });
    $( "#addApartment" ).validate( {
      rules: {
        user_apar_name: "required",
        user_apar_type: "required",
        user_apar_configuration: "required",
        user_apar_house_no: "required",
        user_apar_built_up_area:{
          required:true,
          number:true
        },
        user_apar_bedrooms: {
          required:true,
          number:true
        },
        user_apar_bathrooms: {
          required:true,
          number:true
        },
        user_apar_balconies: {
          required:true,
          number:true
        },
        user_apar_furnished: "required",
        user_apar_wardrobe:{
          required:true,
          number:true
        },
        user_apar_fans:{
          required:true,
          number:true
        },
        user_apar_light:{
          required:true,
          number:true
        },
        user_apar_modular_kitchen: "required",
        user_apar_furnished_name: "required",
        user_apar_reserved_parking: "required",
        user_apar_address: "required"
      },
      messages: {
        user_apar_built_up_area:{
          required: 'This field is required.',
          number: 'Enter valid Number.'
        },
        user_apar_bedrooms:{
          required: 'This field is required.',
          number: 'Enter valid Number.'
        },
        user_apar_bathrooms:{
          required: 'This field is required.',
          number: 'Enter valid Number.'
        },
        user_apar_balconies:{
          required: 'This field is required.',
          number: 'Enter valid Number.'
        },
        user_apar_wardrobe:{
          required: 'This field is required.',
          number: 'Enter valid Number.'
        },
        user_apar_fans:{
          required: 'This field is required.',
          number: 'Enter valid Number.'
        },
        user_apar_light:{
          required: 'This field is required.',
          number: 'Enter valid Number.'
        }
      }
    });
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
  $('#user_apar_furnished').on('change', function () {
    var furnished = $('#user_apar_furnished').val();
    furnished_dtl_show(furnished);
  });
  function furnished_dtl_show(furnished){
    if(furnished=='Furnished' || furnished=='Semifurnished'){
      $('.furnished_dtl').show();
    }else{
      $('.furnished_dtl').hide();
    }
  }
</script>