<!-- ################################################
  
Description: this is user home page after login user redirectd to this web page, here will show all the list of user's vehicle with all details.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
include "all_nav/header.php";
// @include('../../phpqrcode/qrlib.php'); 
?>
<!-- header position -->
<?php ?>
    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Home Page</h5>
        <!-- <form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form> search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card single pd-20 pd-sm-40 col-md-12">
            <input type="button" value="Vehicle QR Code Print" class="btn btn-primary pull-right" name="multiple_qr_code_print" id="multiple_qr_code_print" style="margin-bottom: 10px;" disabled="" onclick="multiple_vehicle_qrcode_print()">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <div class="row">
              <div class="col-md-12">
                  <div class="list-group widget-list-group">
                    <?php
                      $user_id = $_SESSION['user_admin_id'];
                      //$user_id = 99;
                      /*if (isset($_POST['v_type']))
                        $vehicle = $_POST['v_type'];
                      else*/
                        $vehicle = '';
                      if(isset($user_id) ){
                        $response = array();
                        $response = vehicleList($user_id,$vehicle);
                        //print_r($response);
                        if($response['status']){
                          $count = count($response['vehicle']);

                        for ($i=0; $i < $count ; $i++) { 
                          $img = $response['vehicle'][$i]['vehicle_typ_img'];
                          $user_veh_type = $response['vehicle'][$i]['user_veh_type'];
                          $vehicle_type_dec = $response['vehicle'][$i]['vehicle_type_dec'];
                          $user_veh_number = $response['vehicle'][$i]['user_veh_number'];
                          $v_reg = $response['vehicle'][$i]['user_veh_reg'];
                          $v_exp = $response['vehicle'][$i]['user_veh_reg_exp_dt'];
                          $veh_qr_code = $response['vehicle'][$i]['veh_qr_code'];

                          $user_ins_number = $response['vehicle'][$i]['user_ins_number'];
                          $user_ins_exp_dt = $response['vehicle'][$i]['user_ins_exp_dt'];
                          $user_emu_number = $response['vehicle'][$i]['user_emu_number'];
                          $user_emu_exp_dt = $response['vehicle'][$i]['user_emu_exp_dt'];
                          $qr_code_print=base64_encode($veh_qr_code.'||'.$user_veh_number.'||'.$user_veh_type.'||'.$vehicle_type_dec);
                        ?>
                        <div class="list-group-item rounded-top-0">
                          <div class="media pd--b-20">
                            <div class="d-flex mg-l-0 mg-t-10 mg-r-20 wd-40">
                              <img src='<?php echo OPPR_BASE_URL.$img; ?>' style='width: 50px; height: 50px;'>
                            </div>
                            <div class="media-body">
                              <div class="row">
                                <div class="col-lg-12 tx-18 pd-b-10">
                                    <?php echo $user_veh_number; ?>
                                </div>
                                <div class="col-lg-3">
                                   <p class="mg-b-0 tx-12">
                                    Reg No-
                                     <?php
                                    if (!empty($v_reg))
                                       echo $v_reg;
                                     else
                                      echo "N/A";
                                  ?>
                                   <br>
                                    Exp Date- 
                                    <?php
                                      if (!empty($v_exp))
                                         echo $v_exp;
                                       else
                                        echo "N/A";
                                     ?>
                                   </p>
                                </div>
                                <div class="col-lg-3">
                                   <p class="mg-b-0 tx-12">
                                    Inc No-
                                     <?php
                                    if (!empty($user_ins_number))
                                       echo $user_ins_number;
                                     else
                                      echo "N/A";
                                  ?>
                                   <br>
                                    Exp Date- 
                                    <?php
                                      if (!empty($user_ins_exp_dt))
                                         echo $user_ins_exp_dt;
                                       else
                                        echo "N/A";
                                     ?>
                                   </p>
                                </div>
                                <div class="col-lg-3">
                                   <p class="mg-b-0 tx-12">
                                    PUC No-
                                     <?php
                                    if (!empty($user_emu_number))
                                       echo $user_emu_number;
                                     else
                                      echo "N/A";
                                  ?>
                                   <br>
                                    Exp Date- 
                                    <?php
                                      if (!empty($user_emu_exp_dt))
                                         echo $user_emu_exp_dt;
                                       else
                                        echo "N/A";
                                     ?>
                                   </p>
                                </div>
                              </div>                              
                            </div>
                            <div class="d-flex">
                              <div class="row">
                                <div class="col-lg-6">
                                  <img style="height: 90px;" src="./../library/QRCodeGenerate/generate.php?text=<?=$veh_qr_code?>" alt="">
                                </div>
                                <div class="col-lg-4 pd-t-25">
                                  <a href="" data-toggle="tooltip" data-placement="top" title="Print QR Code"><button type="button" class="btn btn-info btn-md" value="" id="P||<?=$qr_code_print?>" onclick="vehicle_qrcode_print(this.id)">QR Code</button></a>
                                </div>
                                <div class="col-lg-1 pd-t-35">
                                  <input type="checkbox" name="" class="p_'$qr_code_print'" id="<?=$qr_code_print?>" onclick="multiple_qr_print(this.id)">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    <?php
                        }
                      }else{
                    ?>
                    <h5 class="text-center">Vehicle Not Found</h5>
                    <?php
                      }
                    }else{
                      //when user_id is missing this part will work. 
                      //$response['status'] = 0; 
                      $response['message'] = 'Somthing went wrong';
                      echo json_encode($response);
                    }
                    ?>
                  </div>

              </div>
              
            </div>
          </div>
        </div><!-- card -->
        
      </div><!-- am-pagebody -->   
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script type="text/javascript">
        $(document).ready(function() {
          window.history.pushState(null, "", window.location.href);        
          window.onpopstate = function() {
            window.history.pushState(null, "", window.location.href);
          };
        });
        function vehicle_qrcode_print(str) {
          // alert(str);
          var res = str.split("||");
          str=(res[1]);
          urlPrint ="middle_tire/vehicle_qrcode_print.php?list_id="+str;
          window.open(urlPrint,'mywindow','width=850,height=600');
          location.reload();
        }
        var myArray = [];
        function multiple_qr_print(str) {
          if(document.getElementById(str).checked){
            myArray[str] = str;
            // alert('ok');
          }else{
            delete myArray[str];
            // alert('no');
          }
          disable_submit();
        }
        function disable_submit(){
          var size = Object.keys(myArray).length;
          if(size>0){
            $('#multiple_qr_code_print').prop('disabled', false);
          }else{
            $('#multiple_qr_code_print').prop('disabled', true);
          }
        }
        function multiple_vehicle_qrcode_print() {
          var obj1=Object.values(myArray);
          str=obj1;
          urlPrint ="middle_tire/vehicle_qrcode_print.php?list_id="+str;
          window.open(urlPrint,'mywindow','width=850,height=600');
          location.reload();
        }
      </script>
      <!-- footer part -->
<?php include "all_nav/footer.php"; ?>
      <!-- footer part -->
      
      