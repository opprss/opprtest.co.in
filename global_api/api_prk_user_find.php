<?php
/*
Description: QR code scan and parking area gate and user found.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function prk_user_find($quercode_value){
    $pieces = explode("-", $quercode_value);
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT prk_area_admin.prk_admin_id, 
        prk_area_gate_dtl.prk_area_gate_type,
        prk_user_gate_login_dtl.prk_user_username as user_name        
        FROM   prk_area_admin, prk_user_gate_login_dtl, prk_area_gate_dtl
        WHERE  prk_area_admin.prk_admin_id = prk_user_gate_login_dtl.prk_admin_id
        AND    prk_area_admin.prk_admin_id = prk_area_gate_dtl.prk_admin_id 
        AND    prk_area_admin.account_number = '$pieces[0]'
        AND    prk_user_gate_login_dtl.end_date is NULL
        AND    prk_area_gate_dtl.prk_area_gate_id = prk_user_gate_login_dtl.prk_area_gate_id
        AND    prk_area_gate_dtl.prk_area_gate_id = '$pieces[1]'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['prk_admin_id'] = $val['prk_admin_id'];
        $response['user_name'] = $val['user_name'];
        $response['prk_area_gate_id'] = $pieces[1];
        $response['prk_area_gate_type'] = $val['prk_area_gate_type'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'Parking Representative will back shortly';
    }
    return json_encode($response);
}
?>