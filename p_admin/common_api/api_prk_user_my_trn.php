<?php
/*
Description: Parking area parkinng employ today earning.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018,15-06-2018
*/
function prk_user_my_trn($prk_admin_id,$payment_rec_emp_name,$start_date,$end_date){
    global $pdoconn;
    $total_p= 0;
    $total_v= 0;
    $response = array();
    $allplayerdata = array();
    $park = array();
    if(!empty($start_date) && !empty($end_date)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($start_date)) == date($start_date) && date($format, strtotime($end_date)) == date($end_date)) {
            $start_date= $start_date;
            $end_date=  $end_date ;
        } else {
            $start_date = date("Y-m-d", strtotime($start_date));
            $end_date = date("Y-m-d", strtotime($end_date));
        }
    }else{
        DATE;
        $end_date= TIME_TRN;
        $start_date= TIME_TRN;
    }
    $sql = "SELECT   x.payment_type, 
        COUNT(1) as tot_veh,  
        SUM( x.payment_amount) as tot_pay
     FROM       
    (SELECT  `prk_veh_trc_dtl`.`prk_admin_id`,
            `payment_receive`.`payment_rec_by` as prk_in_rep_name,
            `prk_veh_trc_dtl`.`prk_veh_type`,
            `prk_veh_trc_dtl`.`veh_number`,
            `payment_receive`.`payment_amount` ,
            DATE_FORMAT(`payment_receive`.`payment_rec_date`,'%D-%M-%Y') AS `payment_rec_date`,
            DATE_FORMAT(`payment_receive`.`payment_rec_date`,'%I:%i %p') AS `payment_rec_time`,
            `payment_receive`.`payment_type`,
            `vehicle_type`.`vehicle_type_dec`,
            `vehicle_type`.`vehicle_typ_img`
            FROM `prk_veh_trc_dtl`, `payment_receive`, `vehicle_type`
            WHERE `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id` = `payment_receive`.`prk_veh_trc_dtl_id`
            AND   `prk_veh_trc_dtl`.`prk_veh_type` = `vehicle_type`.`vehicle_sort_nm`
            AND   `payment_receive`.`payment_type` IN ('CASH', 'WALLET')
            AND   `vehicle_type`.`active_flag`='".FLAG_Y."'
            AND DATE_FORMAT(`payment_receive`.`payment_rec_date`,'%Y-%m-%d')  BETWEEN '$start_date' AND '$end_date'
            UNION         
            SELECT  `prk_veh_trc_dtl`.`prk_admin_id`,
            `prk_veh_trc_dtl`.`prk_out_rep_name` as prk_in_rep_name,
            `prk_veh_trc_dtl`.`prk_veh_type`,
            `prk_veh_trc_dtl`.`veh_number`,
            0 AS payment_amount ,
            DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%D-%M-%Y') AS `payment_rec_date`,
            DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%I:%i %p') AS `payment_rec_time`,
            `prk_veh_trc_dtl`.`payment_type`,
            `vehicle_type`.`vehicle_type_dec`,
            `vehicle_type`.`vehicle_typ_img`
            FROM `prk_veh_trc_dtl`,  `vehicle_type`
            WHERE `prk_veh_trc_dtl`.`prk_veh_type` = `vehicle_type`.`vehicle_sort_nm`
             AND   `prk_veh_trc_dtl`.`payment_type` IN ('DGP', 'TDGP')
             AND   `vehicle_type`.`active_flag`='".FLAG_Y."'
            AND DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%Y-%m-%d')  BETWEEN '$start_date' AND '$end_date'
    ) x
    WHERE    prk_admin_id ='$prk_admin_id'
      AND prk_in_rep_name ='$payment_rec_emp_name'
    GROUP BY x.payment_type";

    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val)
    {
        $response['status'] = 1;
        $response['message'] = 'Sucessfull';
        $park['payment_type'] = $val['payment_type'];
        $park['tot_veh'] = $val['tot_veh'];
        $park['tot_pay'] = $val['tot_pay'];
        $total_p = $total_p + $val['tot_pay'];
        $total_v = $total_v + $val['tot_veh'];
        array_push($allplayerdata, $park);
    }
    $response['status'] = 1;
    $response['message'] = 'Sucessfull';
    $response['total_veh_all'] = $total_v;
    $response['total_pay_all'] = $total_p;
    $response['payment_trn'] = $allplayerdata;
    return json_encode($response);
}
/*function prk_user_my_trn($prk_admin_id,$payment_rec_emp_name,$start_date,$end_date){
    global $pdoconn;
    $total_p= 0;
    $total_v= 0;
    $response = array();
    $allplayerdata = array();
    $park = array();
    if(!empty($start_date) && !empty($end_date)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($start_date)) == date($start_date) && date($format, strtotime($end_date)) == date($end_date)) {
            $start_date= $start_date;
            $end_date=  $end_date ;
        } else {
            $start_date = date("Y-m-d", strtotime($start_date));
            $end_date = date("Y-m-d", strtotime($end_date));
        }
    }else{
        DATE;
        $end_date= TIME_TRN;
        $start_date= TIME_TRN;
    }
    $sql = "SELECT   x.payment_type, 
        COUNT(1) as tot_veh,  
        SUM( x.payment_amount) as tot_pay
 FROM       
(SELECT  `prk_veh_trc_dtl`.`prk_admin_id`,
        `prk_veh_trc_dtl`.`prk_in_rep_name`,
        `prk_veh_trc_dtl`.`prk_veh_type`,
        `prk_veh_trc_dtl`.`veh_number`,
        `payment_receive`.`payment_amount` ,
        DATE_FORMAT(`payment_receive`.`payment_rec_date`,'%D-%M-%Y') AS `payment_rec_date`,
        DATE_FORMAT(`payment_receive`.`payment_rec_date`,'%I:%i %p') AS `payment_rec_time`,
        `prk_veh_trc_dtl`.`payment_type`,
        `vehicle_type`.`vehicle_type_dec`,
        `vehicle_type`.`vehicle_typ_img`
        FROM `prk_veh_trc_dtl`, `payment_receive`, `vehicle_type`
        WHERE `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id` = `payment_receive`.`prk_veh_trc_dtl_id`
        AND   `prk_veh_trc_dtl`.`prk_veh_type` = `vehicle_type`.`vehicle_sort_nm`
        AND   `prk_veh_trc_dtl`.`payment_type` IN ('CASH', 'WALLET')
        AND   `vehicle_type`.`active_flag`='".FLAG_Y."'
        AND DATE_FORMAT(`payment_receive`.`payment_rec_date`,'%Y-%m-%d')  BETWEEN '$start_date' AND '$end_date'
        UNION         
        SELECT  `prk_veh_trc_dtl`.`prk_admin_id`,
        `prk_veh_trc_dtl`.`prk_in_rep_name`,
        `prk_veh_trc_dtl`.`prk_veh_type`,
        `prk_veh_trc_dtl`.`veh_number`,
        0 AS payment_amount ,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%D-%M-%Y') AS `payment_rec_date`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%I:%i %p') AS `payment_rec_time`,
        `prk_veh_trc_dtl`.`payment_type`,
        `vehicle_type`.`vehicle_type_dec`,
        `vehicle_type`.`vehicle_typ_img`
        FROM `prk_veh_trc_dtl`,  `vehicle_type`
        WHERE `prk_veh_trc_dtl`.`prk_veh_type` = `vehicle_type`.`vehicle_sort_nm`
         AND   `prk_veh_trc_dtl`.`payment_type` IN ('DGP', 'TDGP')
         AND   `vehicle_type`.`active_flag`='".FLAG_Y."'
        AND DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%Y-%m-%d')  BETWEEN '$start_date' AND '$end_date'
) x
WHERE    prk_admin_id ='$prk_admin_id'
  AND prk_in_rep_name ='$payment_rec_emp_name'
GROUP BY x.payment_type";

    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val)
    {
        $response['status'] = 1;
        $response['message'] = 'Sucessfull';
        $park['payment_type'] = $val['payment_type'];
        $park['tot_veh'] = $val['tot_veh'];
        $park['tot_pay'] = $val['tot_pay'];
        $total_p = $total_p + $val['tot_pay'];
        $total_v = $total_v + $val['tot_veh'];
        array_push($allplayerdata, $park);
    }
    $response['status'] = 1;
    $response['message'] = 'Sucessfull';
    $response['total_veh_all'] = $total_v;
    $response['total_pay_all'] = $total_p;
    $response['payment_trn'] = $allplayerdata;
    return json_encode($response);
}*/
?>