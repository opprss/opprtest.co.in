<?php 
/*  Description:  Apartment Add.
    Developed by: Rakhal Raj Mandal
    Created Date: 06-10-2019
    Update date : 06-10-2019
*/ 
include_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_admin_id','mobile','token','user_apar_id','user_relation_mobile','user_relation_name','willing_to_rent_out','rent_argument_month','elect_and_water_charge','security_deposit_amount','user_relation_agreement_amount','user_relation_agreement_start_date','user_relation_agreement_end_date'))){
    if(isEmpty(array('user_admin_id','mobile','token','user_apar_id','user_relation_mobile','user_relation_name'))){
        $user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
        $token = trim($_POST['token']);
        $user_apar_id = trim($_POST['user_apar_id']);
        $user_relation_mobile = trim($_POST['user_relation_mobile']);
        $user_relation_name = trim($_POST['user_relation_name']);
        $willing_to_rent_out = trim($_POST['willing_to_rent_out']);
        $rent_argument_month = trim($_POST['rent_argument_month']);
        $elect_and_water_charge = trim($_POST['elect_and_water_charge']);
        $security_deposit_amount = trim($_POST['security_deposit_amount']);
        $user_relation_agreement_amount = trim($_POST['user_relation_agreement_amount']);
        $user_relation_agreement_start_date = trim($_POST['user_relation_agreement_start_date']);
        $user_relation_agreement_end_date = trim($_POST['user_relation_agreement_end_date']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            // api_user_to_user_relation_dtl
            $user_relation_id=uniqid();
            $response = user_to_user_relation_add($user_admin_id,$mobile,$user_apar_id,$user_relation_mobile,$user_relation_name,$willing_to_rent_out,$rent_argument_month,$elect_and_water_charge,$security_deposit_amount,$user_relation_agreement_amount,$user_relation_agreement_start_date,$user_relation_agreement_end_date,$user_relation_id);
        }else{
            #..
            $response= $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response= json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response= json_encode($response);
}
echo ($response);
?>