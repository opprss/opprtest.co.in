<!-- ################################################
Description: List of user's vechile | user can delete vehicle from this page.
Developed by: Soemen Banerjee
Created Date: 17-03-2018
 ####################################################-->
<?php  
  include "all_nav/header.php";
  $prk_area_list1=user_add_prk_area_list($user_admin_id);
  $prk_area_list = json_decode($prk_area_list1, true);
  $incident_type_list=incident_type_list($user_admin_id);
  $incident_type_list = json_decode($incident_type_list, true);
  $user_incident_id=base64_decode($_REQUEST['list_id']);
  $incident_show1=incident_show($user_incident_id);
  $incident_show = json_decode($incident_show1, true);
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
  .error{
    font-size: 11px;
    color: red;
  }
</style>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script src="../lib/validation/jquery.validate.js"></script>
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
  <div class="am-mainpanel"><!-- cloding in footer -->
    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Incident Edit<?php// echo $incident_show1 ?></h5>
    </div><!-- am-pagetitle -->
    
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40" >
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <form role="form" id="addGuest" method="post"  enctype="multipart/form-data">
          <div class="row mg-b-25">
            <input type="hidden" name="user_incident_number" id="user_incident_number">
            <div class="col-lg-4">
              <div class="form-group">
                <label class="form-control-label size">Area Choose<span class="tx-danger">*</span></label>
                <div class="select" style="font-size:14px">
                  <select name="prk_admin_id" id="prk_admin_id" style="color: skyblue">
                    <option value="">SELECT AREA</option>
                     <?php
                      foreach ($prk_area_list['prk_area'] as $val){ ?>  
                        <option value="<?php echo $val['prk_admin_id']?>"><?php echo $val['prk_area_name']?></option>
                      <?php          
                      }
                    ?> 
                  </select>
                </div>
                <span style="position: absolute; display: none;" id="error_prk_admin_id" class="error_size">Select Area</span>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="form-group">
                <label class="form-control-label size">Incident Name<span class="tx-danger">*</span></label>
                <input type="text"  name="inc_name" class="form-control" placeholder="Incident Name" id="inc_name">
              </div>
            </div>
            <div class="col-lg-4">
              <div class="form-group">
                <label class="form-control-label size">Incident Type<span class="tx-danger">*</span></label>
                <div class="select" style="font-size:14px">
                  <select name="inc_typ" id="inc_typ" style="color: skyblue">
                    <option value="">SELECT TYPE</option>
                     <?php
                      foreach ($incident_type_list['type_list'] as $val){ ?>  
                        <option value="<?php echo $val['all_drop_down_id']?>"><?php echo $val['full_name']?></option>
                      <?php          
                      }
                    ?> 
                  </select>
                </div>
                <span style="position: absolute; display: none;" id="error_inc_typ" class="error_size">Select Type</span>
              </div>
            </div>
            <div class="col-lg-8">
              <div class="form-group">
                <label class="form-control-label size">Incident Dtl<span class="tx-danger">*</span></label>
                <input type="text"  name="inc_dtl" class="form-control" placeholder="Incident Dtl" id="inc_dtl">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group mg-t-35">
                <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="vis_save" id="vis_save">
              </div>
            </div>
            <!-- <div class="col-lg-2">
              <div class="form-group mg-t-35">
                <button type="reset" class="btn btn-block prk_button btn-primary">RESET</button>
              </div>
            </div> -->
            <div class="col-lg-2">
              <div class="form-group mg-t-35">
                <button type="button" id="back" class="btn btn-block prk_button btn-primary">BACK</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div><!-- am-pagebody -->
  </div>
  <!-- footer part -->
  <?php include"all_nav/footer.php"; ?>
  <!-- footer part -->
<script type="text/javascript">
  var userUrl = "<?php echo USER_URL; ?>";
  var user_admin_id = "<?php echo $user_admin_id; ?>";
  var token = "<?php echo $token; ?>";
  var mobile = "<?php echo $user_mobile; ?>";
  var user_incident_id = "<?php echo $incident_show['user_incident_id']; ?>";
  var user_incident_number = "<?php echo $incident_show['user_incident_number']; ?>";
  // alert(mobile);
  $( document ).ready( function () { 
    var user_incident_name="<?php echo $incident_show['user_incident_name'] ?>";
    $('#inc_name').val("<?php echo $incident_show['user_incident_name'] ?>");
    $('#inc_typ').val("<?php echo $incident_show['user_incident_type'] ?>");
    $('#prk_admin_id').val("<?php echo $incident_show['prk_admin_id'] ?>");
    $('#inc_dtl').val("<?php echo $incident_show['user_incident_dtl'] ?>");
    // alert(user_incident_name); 
    $('#back').click(function () {
      window.location='user-incident-in';
    });
    $('#vis_save').click(function () {
      // alert('ok');
      var inc_name=$('#inc_name').val();
      var inc_typ=$('#inc_typ').val();
      var inc_dtl=$('#inc_dtl').val();
      var prk_admin_id=$('#prk_admin_id').val();
      if($("#addGuest").valid()){
        $(this).val('Wait ...').prop('disabled', true);
        var user_incident_in = userUrl+'user_incident_edit.php';
        $.ajax({
          url :user_incident_in,
          type:'POST',
          data :{
            'user_incident_id':user_incident_id,
            'user_incident_number':user_incident_number,
            'user_incident_name':inc_name,
            'user_incident_type':inc_typ,
            'user_incident_dtl':inc_dtl,
            'prk_admin_id':prk_admin_id,
            'user_admin_id':user_admin_id,
            'mobile':mobile,
            'token':token
          },
          dataType:'html',
          success  :function(data){
            $('#vis_save').val('SAVE').prop('disabled', false);
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.9em;'>Incident Edit Successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                    window.location='user-incident-in';
                    // location.reload();
                  }
                }
              });
            }else{
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
                type: 'red'
              });
              if (json.session==0) {
                window.location.replace("logout.php");
              }
            }
          }
        });
      }
    });
    $( "#addGuest" ).validate( {
      rules: {
        prk_admin_id:"prk_admin_id_check",
        inc_name: "required",
        inc_typ: "inc_typ_check",
        inc_dtl: "required"
      },
      messages: {
        inc_name:"Please Enter the Name"
        // inc_dtl:"Please Enter the Address"
      }
    });
    $.validator.addMethod("inc_typ_check", function(value){
      // alert(value);
      arg = "";
      if (value != "") {
        //return true;
        $("#error_inc_typ").hide();
        return arg !== value;
      }else{
        $("#error_inc_typ").show();
        return false;
      }
    });
    $.validator.addMethod("prk_admin_id_check", function(value){
      // alert(value);
      arg = "";
      if (value != "") {
        //return true;
        $("#error_prk_admin_id").hide();
        return arg !== value;
      }else{
        $("#error_prk_admin_id").show();
        return false;
      }
    });
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>
