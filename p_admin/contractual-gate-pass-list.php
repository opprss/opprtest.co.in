<!-- ################################################
  
  Description: Parking area can issue gate pass to a particular Visitor.
  Developed by: Soemen Banerjee
  Created Date: 17-03-2018
####################################################-->
<?php 
  include "all_nav/header.php";
  if (isset($_SESSION['prk_admin_id'])) {
    $prk_admin_id = $_SESSION['prk_admin_id'];
  }
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: gray;

  }
  .success{
    font-size: 11px;
    color: green;
  }
  th{
    text-align: center;
  }
</style>
<link href="../lib/highlightjs/github.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../lib/select2/css/select2.min.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Contractual Gate Pass List</h5>
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40 col-md-12">
    	<div class="row">
    		<div class="col-md-4"></div>
    		<div class="col-md-4"></div>
      	<div class="col-md-4">
        	<a style="margin-bottom: 10px;" href="contractual-gate-pass-issue" class="btn btn-primary pull-right">
            <i class="fa fa-plus"></i> Add Member
          </a>
        </div>
      </div>
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <div class="table-wrapper">
        <table id="datatable1" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="wd-5p">Image</th>
              <th class="wd-10p">Name</th>
              <th class="wd-10p">Mobile</th>
              <th class="wd-10p">Contractor</th>
              <th class="wd-10p">Department</th>
              <!-- <th class="wd-10p">P.F Number</th> -->
              <!-- <th class="wd-10p">E.S.I Number</th> -->
              <th class="wd-5p">Start Date</th>
              <th class="wd-5p">End Date</th>
              <th class="wd-15p">Action</th>
            </tr>
          </thead>
          <tbody id="list_ve">
          </tbody>
        </table>
      </div>
      </div>
    </div>
  </div>
  <div class="am-pagebody">
<?php include"all_nav/footer.php"; ?>
<style>
  .error_size{
    font-size: 11px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){

    contractual_gate_pass_list_loadAjax();
  });
</script>
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="../lib/datatables-responsive/jquery.dataTables.min.js"></script>
<script src="../lib/datatables-responsive/dataTables.buttons.min.js"></script>
<script src="../lib/datatables-responsive/jszip.min.js"></script>
<script src="../lib/datatables-responsive/pdfmake.min.js"></script>
<script src="../lib/datatables-responsive/vfs_fonts.js"></script>
<script src="../lib/datatables-responsive/buttons.html5.min.js"></script>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var prk_admin_id = "<?php echo $prk_admin_id; ?>";
  var parking_admin_name='<?php echo $parking_admin_name; ?>';

  function contractual_gate_pass_list_loadAjax(){
    $('#refresh_button').show();
    var gate_pass_list = prkUrl+'contractual_gate_pass_list.php';
    $.ajax({
      url :gate_pass_list,
      type:'POST',
      data :
      {
        'prk_admin_id':prk_admin_id
      },
      dataType:'html',
      success  :function(data){
        // alert(data);
        var demoLines = '';
        var json = $.parseJSON(data);
        $('#datatable1').dataTable().fnDestroy();
        if (json.status){
          for (var key in json.contr_gate_pass_list) {
            var keyy = json.contr_gate_pass_list[key];
            /*<td>'+keyy['visitor_pf_number']+'</td>\
              <td>'+keyy['visitor_esi_number']+'</td>\*/
            demoLines += '<tr>\
              <td> <img src="'+keyy['visitor_img']+'" style="height: 40px;"></td>\
              <td>'+keyy['visitor_name']+'</td>\
              <td>'+keyy['visitor_mobile']+'</td>\
              <td>'+keyy['visitor_contractor']+'</td>\
              <td>'+keyy['visitor_department']+'</td>\
              <td>'+keyy['effective_date']+'</td>\
              <td>'+keyy['end_date']+'</td>\
              <td class="text-center">';

              demoLines+='<a href="contractual-gate-pass-modify?list_id='+keyy['contractual_gate_pass_dtl_id']+'" data-toggle="tooltip" data-placement="top" title="Modify"><i class="fa fa-edit" style="font-size:18px"></i></a>\
                <button type="button" class="clean-button" name="delete" id="'+keyy['contractual_gate_pass_dtl_id']+'"  onclick="contractual_gate_pass_delete(this.id)" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>\
                <a href="" data-toggle="tooltip" data-placement="top" title="Id Card"><button type="button" class="btn btn-info btn-md" value="" id="'+keyy['contractual_gate_pass_dtl_id']+'" onclick="contractual_gate_pass_id_print(this.id)">ID Card</button></a>';

            demoLines += '</td>\
              </tr>';
          }
        }
        $("#list_ve").html(demoLines);
        datatable_show();
      }
    });
  }
  function datatable_show(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      language: {
      searchPlaceholder: 'Search Here',
      sSearch: '',
      lengthMenu: '_MENU_ Page',
      },
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      buttons: [
        {
          text: '<span id="refresh_button"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
          action: function ( e, dt, node, config ) {
              // alert( 'Button activated' );
              contractual_gate_pass_list_loadAjax();
          }
        },
        { 
          extend: 'copyHtml5', 
          //footer: true,
          title: 'CURRENT VEHICLE SHEET' 
        },
        { 
          extend: 'excelHtml5', 
          //footer: true,
          title: 'CURRENT VEHICLE SHEET'  
        },
        {
          extend: 'pdfHtml5',
          //footer: true,
          title: 'CURRENT VEHICLE SHEET',
          customize: function(doc) {
            doc.content[1].margin = [ 60, 0, 0, 0 ]
          }
        },
      ]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    $('#refresh_button').hide();
  }
  function contractual_gate_pass_delete(contractual_gate_pass_dtl_id) {
    // alert(contractual_gate_pass_dtl_id);
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the item permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            if (contractual_gate_pass_dtl_id != '') {
              // alert(contractual_gate_pass_dtl_id);
              var urlDtl = prkUrl+'contractual_gate_pass_delete.php';
              // alert(urlDtl);
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :{
                  'prk_admin_id':prk_admin_id,
                  'parking_admin_name':parking_admin_name,
                  'contractual_gate_pass_dtl_id':contractual_gate_pass_dtl_id,
                },
                dataType:'html',
                success  :function(data){
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Delete successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                          // location.reload(true);
                          contractual_gate_pass_list_loadAjax();
                        }
                      }
                    });
                  }else{
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>"+json.message+"</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
  function contractual_gate_pass_id_print(str) {
    // var print_in_flag = "<?php //echo $print_in_flag ?>";
    urlPrint ="p_admin_middle_tire/contractual_id_card_print?list_id="+str;
    window.open(urlPrint,'mywindow','width=850,height=600');
    location.reload();
  }
</script>
