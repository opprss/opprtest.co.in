<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2018
*/ 
error_reporting(E_ALL & ~E_NOTICE);
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','tenant_name','tenant_mobile','tenant_email','tenant_gender','id_name','id_number','no_of_family','parking_admin_name','effective_date','end_date','tenant_id','token','crud_type'))){
    if(isEmpty(array('prk_admin_id','parking_admin_name','token','crud_type'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
        $tenant_name = trim($_POST['tenant_name']);
        $tenant_mobile = trim($_POST['tenant_mobile']);
        $tenant_email = trim($_POST['tenant_email']);
        $tenant_gender = trim($_POST['tenant_gender']);
        $id_name = trim($_POST['id_name']);
        $id_number = trim($_POST['id_number']);
        $no_of_family = trim($_POST['no_of_family']);

        $parking_admin_name = trim($_POST['parking_admin_name']);
        $effective_date = trim($_POST['effective_date']);
        $end_date = trim($_POST['end_date']);
        $tenant_id = trim($_POST['tenant_id']);
        $crud_type = trim($_POST['crud_type']);
        $token = trim($_POST['token']);
        
        $tenant_agrement_copy='';

        $tenant_image = isset($_POST['tenant_image']) ? trim($_POST['tenant_image']) : '';
        $id_image = isset($_POST['id_image']) ? trim($_POST['id_image']) : '';
        if (!empty($tenant_image)) {      
            $PNG_TEMP_DIR ='./../uploades/'.$parking_admin_name.'/tenant_dtl_img/';       
            if (!file_exists($PNG_TEMP_DIR)) {
                $old_mask = umask(0);
                mkdir($PNG_TEMP_DIR, 0777, TRUE);
                umask($old_mask);
            }
            $image_parts = explode(";base64,", $tenant_image);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
          
            $image_base64 = base64_decode($image_parts[1]);
            $fileName = uniqid() . '.png';
            $file = $PNG_TEMP_DIR . $fileName;
            file_put_contents($file, $image_base64);
            $tenant_image = "uploades/".$parking_admin_name."/tenant_dtl_img/".$fileName;
        }else{

            $tenant_image = '';
        }if (!empty($id_image)) {      
            $PNG_TEMP_DIR ='./../uploades/'.$parking_admin_name.'/tenant_dtl_img/';       
            if (!file_exists($PNG_TEMP_DIR)) {
                $old_mask = umask(0);
                mkdir($PNG_TEMP_DIR, 0777, TRUE);
                umask($old_mask);
            }
            $image_parts = explode(";base64,", $id_image);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
          
            $image_base64 = base64_decode($image_parts[1]);
            $fileName = uniqid() . '.png';
            $file = $PNG_TEMP_DIR . $fileName;
            file_put_contents($file, $image_base64);
            $id_image = "uploades/".$parking_admin_name."/tenant_dtl_img/".$fileName;
        }else{

            $id_image = '';
        }
        if($_FILES['tenant_agrement_copy']['name']){
            $PNG_TEMP_DIR ='./../uploades/'.$parking_admin_name.'/agrement_copy/';       
            if (!file_exists($PNG_TEMP_DIR)) {
                $old_mask = umask(0);
                mkdir($PNG_TEMP_DIR, 0777, TRUE);
                umask($old_mask);
            }
            $fileName = uniqid() ."_".$_FILES['tenant_agrement_copy']['name'];
            $file_post = $PNG_TEMP_DIR.''.$fileName;
            @copy($_FILES['tenant_agrement_copy']['tmp_name'],$file_post);
            $tenant_agrement_copy = "uploades/".$parking_admin_name."/agrement_copy/".$fileName;
        }
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            $response = tenant_details($prk_admin_id,$tenant_name,$tenant_mobile,$tenant_email,$tenant_gender,$id_name,$id_number,$no_of_family,$tenant_id,$tenant_image,$id_image,$tenant_agrement_copy,$effective_date,$end_date,$parking_admin_name,$crud_type);
        }else{
            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
 }
echo $response;
?>