<?php 
/*
Description: User add vehicle.
Developed by: Rakhal Raj Mandal
Created Date: 15-01-2018
Update date : 06-05-2018
*/ 
include_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_admin_id','user_veh_type','user_veh_number','user_veh_reg','user_veh_reg_exp_dt','mobile','token'))){
    if(isEmpty(array('user_admin_id','user_veh_type','user_veh_number','mobile','token'))){
        $user_veh_type = trim($_POST['user_veh_type']);
        $user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
        $user_veh_number = trim($_POST['user_veh_number']);
        $user_veh_reg = trim($_POST['user_veh_reg']);
        $user_veh_reg_exp_dt = ($_POST['user_veh_reg_exp_dt']);
        $token = ($_POST['token']);

        $user_ins_number = isset($_POST['user_ins_number']) ? trim($_POST['user_ins_number']) : '';
        $user_ins_exp_dt = isset($_POST['user_ins_exp_dt']) ? trim($_POST['user_ins_exp_dt']) : '';
        $user_emu_number = isset($_POST['user_emu_number']) ? trim($_POST['user_emu_number']) : '';
        $user_emu_exp_dt = isset($_POST['user_emu_exp_dt']) ? trim($_POST['user_emu_exp_dt']) : '';

        $rc_img = isset($_POST['rc_img']) ? trim($_POST['rc_img']) : '';
        $ins_img = isset($_POST['ins_img']) ? trim($_POST['ins_img']) : '';
        $emu_img = isset($_POST['emu_img']) ? trim($_POST['emu_img']) : '';
        $oth_doc_no1_img = isset($_POST['oth_doc_no1_img']) ? trim($_POST['oth_doc_no1_img']) : '';
        $oth_doc_no2_img = isset($_POST['oth_doc_no2_img']) ? trim($_POST['oth_doc_no2_img']) : '';

        $web_rc_img = isset($_POST['web_rc_img']) ? trim($_POST['web_rc_img']) : '';
        $web_ins_img = isset($_POST['web_ins_img']) ? trim($_POST['web_ins_img']) : '';
        $web_emu_img = isset($_POST['web_emu_img']) ? trim($_POST['web_emu_img']) : '';
        $web_oth_doc_no1_img = isset($_POST['web_oth_doc_no1_img']) ? trim($_POST['web_oth_doc_no1_img']) : '';
        $web_oth_doc_no2_img = isset($_POST['web_oth_doc_no2_img']) ? trim($_POST['web_oth_doc_no2_img']) : '';
       /* if (!empty($rc_img)) {
            # code...
            $rc_img =veh_img_upload_all($mobile,$rc_img);
        }else{

            $rc_img =web_veh_img_upload_all($mobile,$web_rc_img);
        }
        if (!empty($ins_img)) {
            # code...
            $ins_img =veh_img_upload_all($mobile,$ins_img);
        }else{

            $ins_img =web_veh_img_upload_all($mobile,$web_ins_img);
        }
        if (!empty($emu_img)) {
            # code...
            $emu_img =veh_img_upload_all($mobile,$emu_img);
        }else{

            $emu_img =web_veh_img_upload_all($mobile,$web_emu_img);
        }
        if (!empty($oth_doc_no1_img)) {
            # code...
            $oth_doc_no1_img =veh_img_upload_all($mobile,$oth_doc_no1_img);
        }else{

            $oth_doc_no1_img =web_veh_img_upload_all($mobile,$web_oth_doc_no1_img);
        }
        if (!empty($oth_doc_no2_img)) {
            # code...
            $oth_doc_no2_img =veh_img_upload_all($mobile,$oth_doc_no2_img);
        }else{

            $oth_doc_no2_img =web_veh_img_upload_all($mobile,$web_oth_doc_no2_img);
        }*/
        $oth_doc_no1 = isset($_POST['oth_doc_no1']) ? trim($_POST['oth_doc_no1']) : '';
        $oth_doc_no1_exp_dt = isset($_POST['oth_doc_no1_exp_dt']) ? trim($_POST['oth_doc_no1_exp_dt']) : '';
        $oth_doc_no2 = isset($_POST['oth_doc_no2']) ? trim($_POST['oth_doc_no2']) : '';
        $oth_doc_no2_exp_dt = isset($_POST['oth_doc_no2_exp_dt']) ? trim($_POST['oth_doc_no2_exp_dt']) : '';

        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            
            $respon=add_vehicle($user_admin_id,$user_veh_type,$user_veh_number,$user_veh_reg,$user_veh_reg_exp_dt,$user_ins_number,$user_ins_exp_dt,$user_emu_number,$user_emu_exp_dt,$mobile,$rc_img,$ins_img,$emu_img,$oth_doc_no1,$oth_doc_no1_exp_dt,$oth_doc_no1_img,$oth_doc_no2,$oth_doc_no2_exp_dt,$oth_doc_no2_img,$web_rc_img,$web_ins_img,$web_emu_img,$web_oth_doc_no1_img,$web_oth_doc_no2_img);
            echo ($respon);
        }else{
            echo $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        echo json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response);
}
?>