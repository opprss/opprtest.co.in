<?php
/*
Description: user vehicle delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function add_vehicle_delete($user_veh_dtl_id,$user_admin_id,$mobile){
	global $pdoconn;
	$response = array();
	$sql ="UPDATE `user_vehicle_detail` SET `updated_by`='$mobile', `updated_date`='".TIME."', `del_flag`='".FLAG_Y."' WHERE `user_admin_id`='$user_admin_id' AND `user_veh_dtl_id`='$user_veh_dtl_id' AND `del_flag`='".FLAG_N."'";
	$query = $pdoconn->prepare($sql);
	if($query->execute()){
	    $response['status'] = 1;
	    $response['message'] = 'Delete Successful';
	    return json_encode($response);
	}else{
	    $response['status'] = 0;
	    $response['message'] = 'Delete Not Successful';
	    return json_encode($response); 
	}
}			
?>