<?php
/*
Description: parking area employ delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
    // require_once 'api/global_api.php';
    require_once 'api/parkAreaReg_api.php';
//     $response = array();
//   $prk_admin_id=$_REQUEST['prk_admin_id'];
// $prk_user_username=$_REQUEST['prk_user_username'];

//  $respon = prk_area_user_delete($prk_admin_id,$prk_user_username);
//  $response= json_encode($respon);
//  $row=count($response);
//  if($row>0){
//      header("location:../vender-add-user");
//  }


 $response = array();
 if(isAvailable(array('prk_admin_id','prk_user_username','token'))){
 	if(isEmpty(array('prk_admin_id','prk_user_username','token'))){ 
	 	$prk_admin_id = $_POST['prk_admin_id'];
	 	$prk_user_username = trim($_POST['prk_user_username']);
	 	$token = trim($_POST['token']);
	 	
        $resp=prk_token_check($prk_admin_id,$token);
		$json = json_decode($resp);
		// echo $resp;
		if($json->status){
		    $response = prk_area_user_delete($prk_admin_id,$prk_user_username);
		    
		}else{
		    $response = $resp;
		    //echo $response;
		}
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 	$response = json_encode($response);
}
echo $response;
?>