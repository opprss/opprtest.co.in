<?php
/*
Description: Parking gate delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
$response = array();
if(isAvailable(array('user_incident_id','prk_admin_id','prk_area_user_id','status','remarks','token'))){
 	if(isEmpty(array('user_incident_id','prk_admin_id','prk_area_user_id','status','token'))){ 
	 	$user_incident_id = $_POST['user_incident_id'];
	 	$prk_admin_id = $_POST['prk_admin_id'];
	 	$prk_area_user_id = $_POST['prk_area_user_id'];
	 	$status = trim($_POST['status']);
	 	$remarks = trim($_POST['remarks']);
	 	$token = trim($_POST['token']);

        $resp=prk_token_check($prk_admin_id,$token);
		$json = json_decode($resp);
		if($json->status){
		    $response = prk_admin_incident_verify($user_incident_id,$prk_admin_id,$prk_area_user_id,$status,$remarks);
		}else{
		    $response = $resp;
		}
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 	$response = json_encode($response);
}
echo $response;
?>