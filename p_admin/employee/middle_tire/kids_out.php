<?php 
/*
Description: parking employ out vehicle
Developed by: Rakhal Raj Mandal
Created Date: 06-04-2018
Update date : 25-05-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
$re = array();
$resp = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','prk_user_username','token','status','kids_allow_id','prk_area_gate_id'))){
 	if(isEmpty(array('prk_admin_id','prk_area_user_id','prk_user_username','token','status','kids_allow_id','prk_area_gate_id'))){
 		
	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_area_user_id = trim($_POST['prk_area_user_id']);
        $prk_user_username = trim($_POST['prk_user_username']);
        $token = ($_POST['token']);
        $status = trim($_POST['status']);
        $kids_allow_id = trim($_POST['kids_allow_id']);
        $prk_area_gate_id = trim($_POST['prk_area_gate_id']);
        $token_ch=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($token_ch);
		if($json->status=='1'){			
	        $response = kids_out($prk_admin_id,$prk_user_username,$status,$kids_allow_id,$prk_area_gate_id);
        }else{
            $response = $token_ch;
        }
 	}else{
        $response['status'] = 0;
    	$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>