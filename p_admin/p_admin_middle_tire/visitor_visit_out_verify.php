<?php
/*
Description: user vehicle show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_visit_dtl_id','prk_admin_id','user_name','token'))){
 	if(isEmpty(array('prk_visit_dtl_id','prk_admin_id','user_name','token'))){
        $prk_visit_dtl_id = trim($_POST['prk_visit_dtl_id']);
	 	$prk_admin_id = trim($_POST['prk_admin_id']);
        $user_name = trim($_POST['user_name']);
        $token = trim($_POST['token']);
        $sql = "UPDATE `prk_visit_dtl` SET `visitor_out_verify_date`='".TIME."',`visitor_out_verify_flag`='".FLAG_Y."',`visitor_in_verify_flag`='".FLAG_Y."',`visitor_out_verify_name`='$user_name' WHERE `prk_visit_dtl_id`='$prk_visit_dtl_id'";
        $query  = $pdoconn->prepare($sql);
        if($query->execute()){ 
            $response['status']=1;
            $response['message'] = 'Visitor is Verified for OUT';
            $response = json_encode($response);
        }else{
            $response['status']=0;
            $response['message'] = 'Visitor is not Verified for OUT';
            $response = json_encode($response);
        } 
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
 }
 echo $response;
?>