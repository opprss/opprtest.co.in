<!-- ################################################
Description: User Address verify
Developed by: Rakhal Raj Mandal
Created Date: 27-08-2019
####################################################-->
<?php 
  include "all_nav/header.php";
  $request_tower_id = isset($_REQUEST['list1_id']) ? trim($_REQUEST['list1_id']) : ''; 
  $request_flat_id = isset($_REQUEST['list2_id']) ? trim($_REQUEST['list2_id']) : ''; 
  $request_flat_name = isset($_REQUEST['list2_name']) ? trim($_REQUEST['list2_name']) : '';
  $tower_lis = tower_list($prk_admin_id);
  $tower_list = json_decode($tower_lis, true);
  $category_type='PAY_TYPE';
  $payment_method = common_drop_down_list($category_type);
  $payment_method_list = json_decode($payment_method, true);
?>
<style type="text/css">
  .size{
    font-size: 11px;
    text-transform: uppercase;
  }
  .error,.error_size{
    font-size: 11px;
    color: red;
  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
  th,td{
    text-align: center;
  }
</style> 
<link href="./../lib/highlightjs/github.css" rel="stylesheet">
<link href="./../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="./../lib/select2/css/select2.min.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">

    <h5 class="am-title">Maintenance Pay<?//=$apartment_transaction?></h5>
  </div>
  <form id="maint_charge">
    <input type="hidden" name="tower_id" id="tower_id">
    <input type="hidden" name="flat_id" id="flat_id">
    <input type="hidden" name="flat_master_id" id="flat_master_id">
    <input type="hidden" name="flat_size" id="flat_size">
    <input type="hidden" name="owner_id" id="owner_id">
    <input type="hidden" name="owner_name" id="owner_name">
    <input type="hidden" name="living_status" id="living_status">
    <input type="hidden" name="tenant_id" id="tenant_id">
    <input type="hidden" name="tenant_name" id="tenant_name">
    <input type="hidden" name="effective_date" id="effective_date">
    <input type="hidden" name="owner_email" id="owner_email">
    <input type="hidden" name="tenant_email" id="tenant_email">
    <input type="hidden" name="m_account_number" id="m_account_number">
    <input type="hidden" name="json_flat_name" id="json_flat_name">
    <input type="hidden" name="json_tower_name" id="json_tower_name">
    <input type="hidden" name="high_priority" id="high_priority">
    <input type="hidden" name="main_tran_by" id="main_tran_by" value="E">
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40" >
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <div class="row mg-b-10">
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">TOWER NAME <span class="tx-danger">*</span></label>
                <div class="select">
                  <select name="tower_name" id="tower_name" style="opacity: 0.8;font-size:14px">
                    <option value="">Select Tower</option>
                    <?php if($tower_list['status']){
                      foreach($tower_list['tower_list'] as $value){?>
                      <option value="<?=$value['tower_id'];?>"><?=$value['tower_name'];?></option>
                    <?php }}?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">FLAT NAME <span class="tx-danger">*</span></label>
                <div class="select">
                  <select name="flat_name" id="flat_name" style="opacity: 0.8;font-size:14px">
                    <option value="">Select Flat</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-layout-footer mg-t-30">
                <div class="row">
                  <div class="col-lg-4">
                    <input type="button" value="DETAILS SHOW" class="btn btn-block btn-primary prk_button mg-b-10" name="save" id="save">
                  </div>
                  <div class="col-lg-4">
                    <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip mg-b-10">RESET</button>
                  </div>
                  <div class="col-lg-4">
                    <a href="flat-master"><button class="btn btn-block btn-primary prk_button mg-b-10" type="button">BACK</button></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="editable tx-16 bd pd-30 tx-inverse" id="pay_dtl_show" style="padding-bottom: 0px;padding: 5px; display: none;" >
          <div class="row mg-b-10">   
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Due Balance<span class="tx-danger"></span></label>
                <input type="text" class="form-control" placeholder="Due Balance" name="due_balance" id="due_balance" readonly="">
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Advance Balance<span class="tx-danger"></span></label>
                <input type="text" class="form-control" placeholder="Advance Balance" name="advance_balance" id="advance_balance" readonly="">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="form-control-label size">Payment<span class="tx-danger">*</span></label>
                <label class="rdiobox">
                  <input type="radio" name="payment_radio" id="full_payment" value="F" type="radio">
                  <span>Full Payment</span>
                </label>
                <label class="rdiobox">
                  <input type="radio" name="payment_radio" id="part_payment" value="P" type="radio">
                  <span>Partially Payment </span>
                </label>
                <span style="position: absolute;" id="error_payment_radio" class="error_size"></span>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Amount<span class="tx-danger"></span></label>
                <input type="text" class="form-control" placeholder="Amount" name="amount" id="amount">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">PAYMENT METHOD<span class="tx-danger">*</span></label>
                <div class="select" style="">
                  <select name="payment_type" id="payment_type" style="font-size: 14px;">
                    <option value="">SELECT TYPE</option>
                    <?php foreach ($payment_method_list['drop_down'] as $val){ ?>  
                      <option value="<?php echo $val['sort_name']?>"><?php echo $val['full_name']?></option>
                    <?php  }?> 
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-5" id="hidden_cheque" style="display: none;">
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label size">BANK NAME<span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Bank Name" name="bank_name" id="bank_name">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label size">CHEQUE NUMBER<span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Cheque Number" name="cheque_number" id="cheque_number" maxlength="6">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 pd-t-25">
              <input type="hidden" name="main_tran_by" value="A">
              <input type="button" value="RECEIVE PAYMENT" class="btn btn-block btn-primary prk_button" name="pay_resiv" id="pay_resiv">
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
  <div class="am-pagebody" id="main_trn_datatable" style="display: none;">
    <div class="card pd-20 pd-sm-40">
      <div class="table-wrapper">
        <table id="datatable2" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th >SL No.</th>
              <th >Transactions Date</th>
              <th >Payment Head</th>
              <th >Payment Type</th>
              <th >Amount</th>
            </tr>
          </thead>
          <tbody id="trn_his_show"></tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include"all_nav/footer.php"; ?>
<script src="./../lib/highlightjs/highlight.pack.js"></script>
<script src="./../lib/datatables/jquery.dataTables.js"></script>
<script src="./../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE ?>";
  var prk_admin_id="<?=$prk_admin_id?>";
  var parking_admin_name="<?=$parking_admin_name?>";
  var token="<?=$token?>";
  $("#tower_name").val("<?=$request_tower_id?>");
  if ("<?=$request_flat_id?>"!='') {
    var demoLin = '';
    demoLin +='<option value='+"<?=$request_flat_id?>"+'>'+"<?=$request_flat_name?>"+'</option>'; 
    $("#flat_name").html(demoLin);
  }
  function datatable_show(){
    $(function(){
      'use strict';
      $('#datatable2').DataTable({
        responsive: true,
        language: {
          searchPlaceholder: 'Search Here',
          sSearch: '',
          lengthMenu: '_MENU_ Page',
        }/*,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]*/
      });
      $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    });
  }
</script>
<script type="text/javascript">
  $('#save').click(function(){
    if($("#maint_charge").valid()){
      // alert('ok');
      var tower_id=($("#tower_name").val());
      var flat_id=($("#flat_name").val());
      var urlCity = prkUrl+'flat_master.php';
      // 'prk_admin_id','prk_area_user_id','prk_user_username','token','tower_id','flat_id'
      $.ajax ({
        type: 'POST',
        url: urlCity,
        data: {
          'prk_admin_id':prk_admin_id,
          'parking_admin_name':parking_admin_name,
          'token':token,
          'crud_type':'S',
          'tower_name':tower_id,
          'flat_name':flat_id,
          'owner_name':'',
          'living_status':'',
          'tenant_name':'',
          'effective_date':'',
          'end_date':'',
          'flat_master_id':'',
          'two_wh_spa_allw':'',
          'four_wh_spa_allw':'',
          'two_wh_spa':'',
          'four_wh_spa':'',
          'contact_number':'',
          'living_name':''
        },
        success : function(data) {
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status) {
            // alert('ok');
            $('#pay_dtl_show').show(700);
            $('#tower_id').val(json.tower_id);
            $('#flat_id').val(json.flat_id);
            $('#flat_master_id').val(json.flat_master_id);
            $('#json_flat_name').val(json.flat_name);
            $('#flat_size').val(json.flat_size);
            $('#json_tower_name').val(json.tower_name);
            $('#owner_id').val(json.owner_id);
            $('#owner_name').val(json.owner_name);
            $('#living_status').val(json.living_status);
            $('#tenant_id').val(json.tenant_id);
            $('#tenant_name').val(json.tenant_name);
            $('#effective_date').val(json.effective_date);
            $('#owner_email').val(json.owner_email);
            $('#tenant_email').val(json.tenant_email);
            $('#m_account_number').val(json.m_account_number);
            var m_account_number=json.m_account_number;
            var flat_master_id=json.flat_master_id;
            var tower_id=json.tower_id;
            var flat_id=json.flat_id;
            flat_account_balance_show(m_account_number);
            maintenance_tran_hist(m_account_number);
            // $('#pay_dtl_show').show();
          }else{
            $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>No Person Is There In The Flat</p>",
              type: 'red'
            });
          }
        }
      });
    }
  });
  $('#pay_resiv').click(function(){
    if($("#maint_charge").valid()&payment_radio_check()){
      // alert('okk');
      $('#pay_resiv').val('Wait ...').prop('disabled', true);
      var payment_ty = $("#payment_type").val();
      var bank_na = $("#bank_name").val();
      var cheque_num = $("#cheque_number").val();
      var tower_id = $("#tower_id").val();
      var flat_id = $("#flat_id").val();
      var owner_id = $("#owner_id").val();
      var tenant_id = $("#tenant_id").val();
      var m_account_number= $("#m_account_number").val();
      var amount= $("#amount").val();
      var urlmaint = prkUrl+'maintenance_pay.php';
      // 'prk_admin_id','prk_area_user_id','prk_user_username','token','m_account_number','tower_id','flat_id','owner_id', 'tenant_id','amount','payment_type','bank_name','cheque_number'
      $.ajax({  
        url:urlmaint,  
        method:"POST",  
        data: {
          'prk_admin_id':prk_admin_id,
          'parking_admin_name':parking_admin_name,
          'token':token,
          'm_account_number':m_account_number,
          'tower_id':tower_id,
          'flat_id':flat_id,
          'owner_id':owner_id,
          'tenant_id':tenant_id,
          'amount':amount,
          'payment_type':payment_ty,
          'bank_name':bank_na,
          'cheque_number':cheque_num
        },  
        success:function(data){  
          // alert(data);
          $('#pay_resiv').val('RECEIVE PAYMENT').prop('disabled', false);
          var json = $.parseJSON(data);
          if (json.status){
            $('#pay_resiv').prop('disabled', true);
            $('#save').prop('disabled', true);
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success',
              content: "<p style='font-size:0.9em;'>"+json.message+"</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                }
              }
            });
            maintenance_tran_hist(m_account_number);
          }else{
            $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.7em;'>"+json.message+"</p>",
              type: 'red'
            });
          }
        }  
      });
    }
  });
</script>
<script type="text/javascript">
  $( document ).ready( function () {
    $( "#maint_charge" ).validate( {
      rules: {
        flat_name: "required",
        tower_name: "required",
        amount:{
          required: true,
          number: true
        },
        payment_type: "required",
        bank_name: "required",
        cheque_number: {
          required: true,
          number: true
        }
      },
      messages: {
        amount: {
          required: 'This field is required.',
          number: 'Enter valid Amount.'
        },
        cheque_number: {
          required: 'This field is required.',
          number: 'Enter valid cheque Number.'
        }
      }
    });
  });
  function payment_radio_check(){
    if (!$('input[name=payment_radio]:checked').val() ) {
      $('#error_payment_radio').text('This field is required.');
      return false;
    }else{
      $('#error_payment_radio').text('');
      return true;
    }
  }
  function flat_account_balance_show(m_account_number){
    var urlCity = prkUrl+'flat_account_balance_show.php';
    // 'prk_admin_id','prk_area_user_id','prk_user_username','token','m_account_number'
    $.ajax ({
      type: 'POST',
      url: urlCity,
      data: {
        'prk_admin_id':prk_admin_id,
        'parking_admin_name':parking_admin_name,
        'token':token,
        'm_account_number':m_account_number
      },
      success : function(data) {
        //alert(data);
        var json = $.parseJSON(data);
        if (json.status) {
          $('#due_balance').val(json.due_balance);
          $('#advance_balance').val(json.advance_balance);
        }else{
          alert('Not Assigned');
        }
      }
    });
  }
  function maintenance_tran_hist(m_account_number){
    var urlPrkAreGateDtl = prkUrl+'flat_account_transaction_history.php';
    // 'prk_admin_id','prk_area_user_id','prk_user_username','token','m_account_number'
    $.ajax({
      url :urlPrkAreGateDtl,
      type:'POST',
      data :{
        'prk_admin_id':prk_admin_id,
        'parking_admin_name':parking_admin_name,
        'token': token,
        'm_account_number':m_account_number
      },
      dataType:'html',
      success  :function(data){
        // alert(data);
        var json = $.parseJSON(data);
        if (json.status){
          $("#main_trn_datatable").show();
          $('#datatable2').dataTable().fnDestroy();
          var demoLines="";
          var c = 0;
          for (var key in json.tower_list) {
            c +=1;
            var keyy = json.tower_list[key];
            var payment_type = (keyy['payment_type']=='CR')?'CREDIT':'DEBIT';
            // alert(keyy['total_amount']);
            demoLines += '<tr>\
            <td >'+c+'</td>\
              <td >'+keyy['transitions_date']+'</td>\
              <td >'+keyy['payment_head']+'</td>\
              <td >'+payment_type+'</td>\
              <td >'+keyy['amount']+'</td>\
            </tr>';
          }
          $("#trn_his_show").html(demoLines);
          datatable_show();
        }else{
          $("#main_trn_datatable").hide();
          if( typeof json.session !== 'undefined'){
            if (!json.session) {
              window.location.replace("logout.php");
            }
          }else{
            // $.alert({
            //   icon: 'fa fa-frown-o',
            //   theme: 'modern',
            //   title: 'Error !',
            //   content: "<p style='font-size:0.9em;'>"+json.message+"</p>",
            //   type: 'red'
            // });
          }
        }
      }
    });
  }
  function hide_ditails() {
    $('#pay_resiv').prop('disabled', false);
    $('#save').prop('disabled', false);
    $('input[name="payment_radio"]').prop('checked', false);
    $('#amount').val('');
    $('#payment_type').val('');
    $("#main_trn_datatable").hide(700);
    $('#pay_dtl_show').hide(700);
  }
</script>
<script type="text/javascript">
  $('#tower_name').on("change", function() {
    hide_ditails();
    var id=($("#tower_name").val());
    var urlCity = prkUrl+'flat_list.php';
    // 'prk_admin_id','prk_area_user_id','prk_user_username','token','tower_id'
    $.ajax ({
      type: 'POST',
      url: urlCity,
      data: {
        'tower_id':id,
        'prk_admin_id':prk_admin_id,
        'parking_admin_name':parking_admin_name,
        'token':token
      },
      success : function(data) {
        var obj=JSON.parse(data);
        var areaOption = "<option value=''>Select Flat</option>";
        if (obj['status']) {
          $.each(obj['flat_list'], function(val, key) {
            areaOption += '<option value="' + key['flat_id'] + '">' + key['flat_name'] + '</option>'
          });
        }
        $("#flat_name").html(areaOption);
      }
    });
  });
  $('#flat_name').on("change", function() {

    hide_ditails();
  });
  $('input[name=payment_radio]').change(function(){
    payment_radio_check();
    var payment_radio = $('input[name=payment_radio]:checked').val();
    if(payment_radio == 'F') {
      $('#amount').val($('#due_balance').val());
      $('#amount').prop('disabled', true);           
    }else{
      $('#amount').val($('#due_balance').val());
      $('#amount').prop('disabled', false);   
    }
  });
  $('#payment_type').on('change', function() {
    if(this.value == "CHEQUE") {
      $('#hidden_cheque').show(500);
    }else{
      $('#hidden_cheque').hide(500);
    }
  });
</script>