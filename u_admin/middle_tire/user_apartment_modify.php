<?php 
/*  Description:  Apartment Add.
    Developed by: Rakhal Raj Mandal
    Created Date: 06-10-2019
    Update date : 06-10-2019
*/ 
include_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_admin_id','mobile','token','user_apar_name','user_apar_address','user_apar_id','user_apar_type','user_apar_configuration','user_apar_house_no','user_apar_built_up_area','user_apar_bedrooms','user_apar_bathrooms','user_apar_balconies','user_apar_furnished','user_apar_wardrobe','user_apar_fans','user_apar_light','user_apar_modular_kitchen','user_apar_furnished_name','user_apar_reserved_parking'))){
    if(isEmpty(array('user_admin_id','mobile','token','user_apar_name','user_apar_address','user_apar_id'))){
        $user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
        $token = trim($_POST['token']);
        $user_apar_name = trim($_POST['user_apar_name']);
        $user_apar_type = trim($_POST['user_apar_type']);
        $user_apar_configuration = trim($_POST['user_apar_configuration']);
        $user_apar_house_no = trim($_POST['user_apar_house_no']);
        $user_apar_built_up_area = trim($_POST['user_apar_built_up_area']);
        $user_apar_bedrooms = trim($_POST['user_apar_bedrooms']);
        $user_apar_bathrooms = trim($_POST['user_apar_bathrooms']);
        $user_apar_balconies = trim($_POST['user_apar_balconies']);
        $user_apar_furnished = trim($_POST['user_apar_furnished']);
        $user_apar_wardrobe = trim($_POST['user_apar_wardrobe']);
        $user_apar_fans = trim($_POST['user_apar_fans']);
        $user_apar_light = trim($_POST['user_apar_light']);
        $user_apar_modular_kitchen = trim($_POST['user_apar_modular_kitchen']);
        $user_apar_furnished_name = trim($_POST['user_apar_furnished_name']);
        $user_apar_reserved_parking = trim($_POST['user_apar_reserved_parking']);
        $user_apar_address = trim($_POST['user_apar_address']);
        $user_apar_id = trim($_POST['user_apar_id']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            // api_user_apartment_dtl
            $response = user_apartment_modify($user_admin_id,$mobile,$user_apar_name,$user_apar_type,$user_apar_configuration,$user_apar_house_no,$user_apar_built_up_area,$user_apar_bedrooms,$user_apar_bathrooms,$user_apar_balconies,$user_apar_furnished,$user_apar_furnished_name,$user_apar_reserved_parking,$user_apar_address,$user_apar_id,$user_apar_wardrobe,$user_apar_fans,$user_apar_light,$user_apar_modular_kitchen);
        }else{
            #..
            $response= $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response= json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response= json_encode($response);
}
echo ($response);
?>