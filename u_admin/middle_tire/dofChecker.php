<?php 
/*
Description: date fo birth check user 18 years old.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
 include_once 'api/reg_api.php';
    // include_once 'api/global_api.php';
    $response = array();
    if(isAvailable(array('dob'))){
        if(isEmpty(array('dob'))){
            $dob = trim($_POST['dob']);
            $response= dofChecker($dob);
            echo ($response);
        }else{
                $response['status'] = 0;
                $response['message'] = 'All Fields Are Mandatory';
                echo json_encode($response); 
        }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        echo json_encode($response); 
    } 
?> 