<?php 
/*
Description: parking area visit the people all list. 
Developed by: Rakhal Raj Mandal
Created Date: 09-04-2018
Update date : -------
*/
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
$response = array();
$resp = array();
if(isAvailable(array('prk_admin_id'))){
 	if(isEmpty(array('prk_admin_id'))){ 

	 	$prk_admin_id = trim($_POST['prk_admin_id']);       
        $response = prk_veh_type($prk_admin_id);

 	}else{
            $response['status'] = 0;
	    	$response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>