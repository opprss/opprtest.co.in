<?php
  session_start();
  include "all_nav/header.php";
  if (isset($_REQUEST["prk_admin_id"])) {
    $prk_admin_id= trim($_REQUEST["prk_admin_id"]);
    if(empty($prk_admin_id)){
     header('Location: prk-list.php');
    }
  }else{

    header('Location: prk-list');
  }
  $kind_alt='';
  $gst_number='';
  $bill_to_add='';
  $shipped_to_add='';
  $payment_mode='';
  $par_month_rate='';
  $tax_type='';
  $bill_date='';
  $payment_due_day='';
  $service_name='';
  $discount='';
  $cus_dtl_show = prk_area_cus_dtl_show($prk_admin_id);
  $cus = json_decode($cus_dtl_show);
  if ($cus->status) {
    // $prk_area_cus_dtl_id=$cus->prk_area_cus_dtl_id;
    // $prk_admin_id=$cus->prk_admin_id;
    $kind_alt=$cus->kind_alt;
    $gst_number=$cus->gst_number;
    $bill_to_add=$cus->bill_to_add;
    $shipped_to_add=$cus->shipped_to_add;
    $payment_mode=$cus->payment_mode;
    $par_month_rate=$cus->par_month_rate;
    $tax_type=$cus->tax_type;
    $bill_date=$cus->bill_date;
    $payment_due_day=$cus->payment_due_day;
    $service_name=$cus->service_name;
    $discount=$cus->discount;
  }

  $prk_inf = prk_inf($prk_admin_id);
  $prk_inf = json_decode($prk_inf, true);
  $prk_area_name = $prk_inf['prk_area_name'];

  $next_bill_gene1 = cus_next_bill_generate_date_show($prk_admin_id);
  $next_bill_gene = json_decode($next_bill_gene1, true);
  $next_bill_date = $next_bill_gene['next_bill_date'];
?>
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="../../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
<style type="text/css">
  .size{
    font-size: 11px;
  }
  .error{
    font-size: 11px;
    color: red;
  }
  th,td{
    text-align: center;
  }
  .button_print{
    border: none;
    background: none;
  }
</style>
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Customer Bill Generate<?//=$next_bill_date?></h5>
  </div>
</div>
<div class="am-mainpanel">
	<div class="am-pagebody">
    <div class="card pd-20 pd-sm-40" >
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <form id="admin_customer">
          <div class="row mg-b-25">
            <div class="col-lg-5">
              <div class="form-group">
                <label class="form-control-label size">Premises Name<span class="tx-danger">*</span></label>
                <input type="text" class="form-control" readonly="" placeholder="Kind Alt Name" name="prk_area_name" id="prk_area_name" value="<?=$prk_area_name?>">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Next Bill Date<span class="tx-danger">*</span></label>
                <input type="text" name="bill_generate_date" id="bill_generate_date" placeholder="dd-mm-yyyy" class="form-control" autocomplete="off" readonly="" value="<?=$next_bill_date?>">
              </div>
            </div>
            <div class="col-lg-4">
              <div class="form-layout-footer mg-t-30">
                <div class="row">
                  <div class="col-lg-6">
                    <button type="button" class="btn btn-block btn-primary prk_button" name="save" id="save"><span id="refresh_button" style="display: none;"><i class="fa fa-refresh fa-spin"></i></span> &nbsp;Generate Bill</button>
                  </div>
                  <div class="col-lg-6">
                    <a href="prk-list"><button class="btn btn-block btn-primary prk_button" type="button">BACK</button></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40">
      <div class="table-wrapper">
        <table id="datatable1" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="wd-5p">Sl No.</th>
              <th class="wd-10p">Invoice No.</th>
              <th class="wd-10p">Bill Month</th>
              <th class="wd-15p">Bill Period</th>
              <th class="wd-10p">Bill Due Date</th>
              <th class="wd-10p">Total Amount</th>
              <th class="wd-10p">Payment Receive Date</th>
              <th class="wd-5p">Action</th>
            </tr>
          </thead>
          <tbody id="cus_bill_list_table"></tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include"all_nav/footer.php"; ?>
<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="../../lib/datatables-responsive/jquery.dataTables.min.js"></script>
<script src="../../lib/datatables-responsive/dataTables.buttons.min.js"></script>
<script src="../../lib/datatables-responsive/jszip.min.js"></script>
<script src="../../lib/datatables-responsive/pdfmake.min.js"></script>
<script src="../../lib/datatables-responsive/vfs_fonts.js"></script>
<script src="../../lib/datatables-responsive/buttons.html5.min.js"></script>
<script type="text/javascript">
  var SUP_EMP_URL = "<?php echo SUP_EMP_URL ;?>";
  var inserted_by = "<?php echo $employee_user_name ;?>";
  var prk_admin_id = "<?=$prk_admin_id?>";
  /*addEventListener('DOMContentLoaded', function () {
    pickmeup('#bill_generate_date', {
      default_date   : false,
      position       : 'bottom',
      format  : 'd-m-Y',
      hide_on_select : true,
      render : function (date) {
        return {
          disabled : false,
          class_name : 'date-in-past'
        };
      } 
    });
  });*/
  $( document ).ready( function () {
    $('#save').click(function(){
      if($("#admin_customer").valid()){
        // alert('ok');
        var bill_generate_date=($("#bill_generate_date").val());
        var urlCusDtlAdd = SUP_EMP_URL+'cus_bill_generate.php';
        $('#refresh_button').show();
        // 'prk_admin_id','bill_generate_date','inserted_by'
        $.ajax({
          url :urlCusDtlAdd,
          type:'POST',
          data :{
            'prk_admin_id':prk_admin_id,
            'bill_generate_date':bill_generate_date,
            'inserted_by': inserted_by
          },
          dataType:'html',
          success  :function(data){
            $('#refresh_button').hide();
            $('#save').prop('disabled', false);
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.9em;'>Bill Generate Successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                    location.reload(true);
                    // window.location.href='flat-details';
                  }
                }
              });
            }else{
              if( typeof json.session !== 'undefined'){
                if (!json.session) {
                  window.location.replace("logout.php");
                }
              }else{
                $.alert({
                  icon: 'fa fa-frown-o',
                  theme: 'modern',
                  title: 'Error !',
                  content: "<p style='font-size:0.9em;'>"+json.message+"</p>",
                  type: 'red'
                });
              }
            }
          }
        });
      }
    });
    $("#admin_customer" ).validate( {
      rules: {
        // gst_number: "required",
        bill_generate_date: "required"
      }
    });
  });
</script> 
<script type="text/javascript">
  // datatable_show();
  cus_bill_list();
  function cus_bill_list() {
    $('#refresh_button_table').show();
    var cusBillGenerateList = SUP_EMP_URL+'cus_bill_generate_list.php';
    $.ajax({
      url :cusBillGenerateList,
      type:'POST',
      data :{
        'prk_admin_id':prk_admin_id
      },
      dataType:'html',
      success  :function(data){
        // alert(data);
        var demoLines = '';
        var json = $.parseJSON(data);
        if (json.status){
            var c = 0;
          for (var key in json.cus_bill_gen_list) {
            var keyy = json.cus_bill_gen_list[key];
            c +=1;
            demoLines += '<tr>\
              <td> '+c+'</td>\
              <td>'+keyy['invoice_number']+'</td>\
              <td>'+keyy['billing_month']+'</td>\
              <td>'+keyy['bill_period_start_date']+' To '+keyy['bill_period_end_date']+'</td>\
              <td>'+keyy['bill_due_date']+'</td>\
              <td>'+keyy['total_invoice_value']+'</td>\
              <td>'+keyy['payment_receive_date']+'</td>\
              <td>';
              if(keyy['payment_receive_flag']=='N'){

                demoLines += '<a href="customer-bill-receive?list_id='+keyy['cus_bill_dtl_id']+'" data-toggle="tooltip" data-placement="top" title="Bill Receive"><i class="fa fa-archive" style="font-size:18px"></i></a>&nbsp;&nbsp;';
              }else{

                demoLines += '<i class="fa fa-archive" style="font-size:18px"></i>&nbsp;&nbsp;';
              }
              demoLines += '<button id="'+keyy['cus_bill_dtl_id']+'" class="button_print" onclick="customer_bill_print(this.id)"><a href="#" data-toggle="tooltip" data-placement="top" title="Bill Download"><i class="fa fa-print" style="font-size:18px"></i></a></button>';
            demoLines += '</td></tr>';
          }
        }
        $('#datatable1').dataTable().fnDestroy();
        $("#cus_bill_list_table").html(demoLines);
        datatable_show();
      }
    });
  }
  function datatable_show(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      language: {
      searchPlaceholder: 'Search Here',
      sSearch: '',
      lengthMenu: '_MENU_ Page',
      },
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      buttons: [
        {
          text: '<span id="refresh_button_table"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
          action: function ( e, dt, node, config ) {
              // alert( 'Button activated' );
              cus_bill_list();
          }
        },
        { 
          extend: 'copyHtml5', 
          //footer: true,
          title: 'Customer Bill List' 
        },
        { 
          extend: 'excelHtml5', 
          //footer: true,
          title: 'Customer Bill List'  
        },
        {
          extend: 'pdfHtml5',
          //footer: true,
          title: 'Customer Bill List',
          customize: function(doc) {
            doc.content[1].margin = [ 60, 0, 0, 0 ]
          }
        },
      ]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    $('#refresh_button_table').hide();
  }
  function customer_bill_print(str) {
    // alert(str);
    urlPrint = SUP_EMP_URL+"customer_bill_print?list_id="+str;
    window.open(urlPrint,'mywindow','width=850,height=600');
    location.reload();
  }
</script>