<?php
/*
Description: user Account status check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function accountStatus($mobile){
	    $response = array();
	    global $pdoconn;
	    $sql ="SELECT `user_admin_id`,`user_ac_status` FROM `user_admin` WHERE `user_mobile`=:user_mobile";
	    $query  = $pdoconn->prepare($sql);
	    $query->execute(array('user_mobile'=>$mobile));
	    $count=$query->rowCount();
	    if($count>0){
	        $row = $query->fetch();
	        $ac_sta =  $row['user_ac_status'];
	        if($ac_sta=='A'){
	            $response['status'] = 0;
	            $response['message'] = 'User Already Exists';
	        }
	        if($ac_sta=='T'){
	            $response['status'] = 0;
	            $response['message'] = 'User Temporary Locked';
	        }
	        if($ac_sta=='L'){
	            $response['status'] = 0;
	            $response['message'] = 'User Locked';
	        }
	        if($ac_sta=='D'){
	            $response['status'] = 0;
	            $response['message'] = 'User Regidtered as Draft.';
	        }
	        if($ac_sta=='P'){
	            $response['status'] = 0;
	            $response['message'] = 'User Payment due Please Contact Customer Care';
	        }
	    }else{

            $response['status'] = 1;
            $response['message'] = 'Welcome';
	    }
	    return json_encode($response);
	}	
?>