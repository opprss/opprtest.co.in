<!-- ################################################
Description: User Address verify
Developed by: Rakhal Raj Mandal
Created Date: 27-08-2019
####################################################-->
<?php 
  include "all_nav/header.php";
  $respon1=prk_user_add_list($prk_admin_id);
  $respon = json_decode($respon1, true);
  $start_date=TIME_TRN;
  $end_date=TIME_TRN;
  $kids_list1= kids_list($prk_admin_id,$start_date,$end_date);
  $kids_list = json_decode($kids_list1, true);

?>
<style type="text/css">
  .size{
    font-size: 11px;
    text-transform: uppercase;
  }
  .error_size{
    font-size: 11px;
    color: red;
  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
  th,td{
    text-align: center;
  }
</style> 
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Kids Allow<?//=$kids_list1?></h5>
  </div>
</div>
<div class="am-mainpanel">
  <div class="am-pagebody" id="data">
    <div class="card pd-20 pd-sm-40" style="padding: 10px;">
      <div class="table-wrapper">
        <table id="datatable1" class="table display responsive nowrap" cellspacing="0" width="100%">
          <thead>
          <tr>
            <th class="wd-5p">Image</th>
            <th class="">Kid Name</th>
            <th class="">Date</th>
            <th class="">Time</th>
            <th class="">Alone</th>
            <th class="">Guardian Name</th>
            <th class="wd-10p">Guardian Number</th>
            <th class="wd-5p" style="text-align: center;">Action</th>
          </tr>
          </thead>
          <?php if($kids_list['status']){
            foreach($kids_list['kids_list'] as $value){
              $alone = ($value['alone_flag']==FLAG_Y)?'YES':'NO';
          ?>
          <tr>
            <td><img src="<?php echo $value['user_family_img']; ?>" style="height: 40px; border-radius: 20%;"> </td>
            <td><?php echo $value['user_family_name']; ?></td>
            <td><?php echo $value['date']; ?></td>
            <td><?=date('h:i a', strtotime($value['from_time'])).'<br>'.date('h:i a', strtotime($value['to_time'])); ?></td>
            <td><?php echo $alone; ?></td>
            <td><?php echo $value['guardian_name']; ?></td>
            <td><?php echo $value['guardian_mobile']; ?></td>
            <td style="text-align: center;">
              <button type="button" class="clean-button" name="delete" id="<?php echo $value['kids_allow_id']; ?>" onclick="kids_out(this.id)" data-toggle="tooltip" data-placement="top" title="Allow Kid"><i class="icon ion-arrow-right-a" style="font-size:22px; color:#4be1f4;"></i></button>
            </td>
          </tr><?php }}?>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include"all_nav/footer.php"; ?>
<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
</script>
<script type="text/javascript">
  function kids_out(kids_allow_id){
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>Do You Want to Allow</p>",
      theme: 'modern',
      type: 'green',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Out',
          btnClass: 'btn-green',
          keys: ['Y', 'shift'],
          action: function(){
            if (kids_allow_id != '') {
              var urlDtl = pkrEmpUrl+'kids_out.php';
              // 'prk_admin_id','prk_area_user_id','prk_user_username','token','status','kids_allow_id','prk_area_gate_id'
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :{
                  'prk_admin_id':prk_admin_id,
                  'prk_area_user_id':prk_area_user_id,
                  'prk_user_username':prk_user_username,
                  'token':token,
                  'status':'O',
                  'kids_allow_id':kids_allow_id,
                  'prk_area_gate_id':prk_area_gate_id
                },
                dataType:'html',
                success  :function(data){
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Kid Out Successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                          location.reload(true);
                          // visitor_list();
                        }
                      }
                    });
                  }else{
                    if (json.session=='0') {
                      window.location.replace("logout.php");
                    }
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
</script>
