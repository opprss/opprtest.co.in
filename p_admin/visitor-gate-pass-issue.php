<!-- ################################################
  
  Description: Parking area can issue gate pass to a particular Visitor.
  Developed by: Soemen Banerjee
  Created Date: 17-03-2018
####################################################-->
<?php 
  include "all_nav/header.php";
  if (isset($_SESSION['prk_admin_id'])) {
    $user_name=$_SESSION['prk_admin_id'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
    $parking_admin_name=$_SESSION['parking_admin_name'];
  }
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
  .error{
    font-size: 11px;
    color: red;
  }
</style>
    <script src="../js/webcam.min.js"></script>
    <!-- for datatable -->
    <link href="../lib/highlightjs/github.css" rel="stylesheet">
    <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet"> 
    <!-- header position -->
    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Visitor Gate Pass</h5>
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card pd-20 pd-sm-40 col-md-12">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <div class="row">
              <div class="col-md-12">
                
                  <form role="form" id="addVis" method="post"  enctype="multipart/form-data">
                    <div class="form-layout">
                      <div class="row mg-b-25">
                        <div class="col-lg-3">
                          <div class="form-group">
                              <label class="form-control-label size">Name<?php //echo $parking_admin_name;?> <span class="tx-danger">*</span></label>
                              <input type="text" class="form-control" placeholder="Visitor Name" name="visitor_name" id="visitor_name" onkeyup="vehNoUppercase();">
                              <input type="hidden" name="inserted_by" id="inserted_by" class="form-control name_list" readonly="" value="<?php echo $parking_admin_name; ?>">
                              <input type="hidden" name="prk_admin_id" id="prk_admin_id" class="form-control name_list" readonly="" value="<?php echo $prk_admin_id; ?>">
                              <input type="hidden" name="visitor_gate_pass_dtl_id" id="visitor_gate_pass_dtl_id" class="form-control name_list" readonly="" >
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="form-control-label size">Mobile Number<span class="tx-danger">*</span></label>
                            <input type="text" class="form-control" placeholder="Mobile Number" name="visitor_mobile" id="visitor_mobile" maxlength="10">
                            <span style="position: absolute;" id="error_vis_mobile" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label class="form-control-label size">Gender<span class="tx-danger">*</span></label>
                              <label class="rdiobox">
                                <input type="radio" name="visitor_gender" id="vis_gender_m" value="M" type="radio" checked>
                                <span>Male </span>
                              </label>

                              <label class="rdiobox">
                                <input type="radio" name="visitor_gender" id="vis_gender_f" value="F" type="radio">
                                <span>Female </span>
                              </label>
                              <span style="position: absolute;" id="error_vis_gender" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label class="form-control-label size">Select Vehicle Type<span class="tx-danger"></span></label>
                            <div class="select">
                              <select name="visitor_veh_type" id="visitor_veh_type" value="" style="opacity: 0.8; font-size:14px"></select>
                            </div>
                            <span style="position: absolute; display: none;" id="error_vis_veh_type" class="error_size">Please Select Vehicle type</span>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label class="form-control-label size">Vehicle Number<span class="tx-danger"></span></label>
                            <input type="text" class="form-control " placeholder="Vehicle Number" name="visitor_veh_num" id="visitor_veh_num" style="background-color: transparent;">
                            <span style="position: absolute;" id="error_vis_veh_num" class="error_size"></span>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-7">
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label class="form-control-label size">Address 1<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="Address 1" name="visitor_add1" id="visitor_add1">
                                <span style="position: absolute;" id="error_vis_add1" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label class="form-control-label size">Address 2<span class="tx-danger"></span></label>
                                <input type="text" class="form-control" placeholder="Address 2" name="visitor_add2" id="visitor_add2">
                                <span style="position: absolute;" id="error_vis_add2" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label class="form-control-label size">Landmark<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="Landmark" name="visitor_landmark" id="visitor_landmark" style="background-color: transparent;">
                                <span style="position: absolute;" id="error_vis_landmark" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <label class="form-control-label size">Country Name<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control readonly" placeholder="Gate Pass Number" name="visitor_country" id="visitor_country" value="INDIA" style="background-color: transparent;" readonly="">
                                <span style="position: absolute;" id="error_vis_country" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <label class="form-control-label size">State Name<span class="tx-danger">*</span></label>
                                <div class="select">
                                  <select name="visitor_state" id="visitor_state" style="opacity: 0.8;font-size:14px">
                                      <option value="">SELECT STATE</option>
                                  </select>
                                </div>
                                <span style="position: absolute; display: none; " id="error_vis_state_name" class="error_size">This field is required.</span>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <label class="form-control-label size">City Name<span class="tx-danger">*</span></label>
                                <div class="select">
                                  <select name="visitor_city" id="visitor_city" style="opacity: 0.8;font-size:14px">
                                      <option value="">SELECT CITY</option>
                                  </select>
                                </div>
                                <span style="position: absolute; display: none;" id="error_vis_city_name" class="error_size">This field is required.</span>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <label class="form-control-label size">Pin Code<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control " placeholder="Pin Code" name="visitor_pin_cod" id="visitor_pin_cod" style="background-color: transparent;" maxlength="6">
                                <span style="position: absolute;" id="error_vis_pin_code" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-lg-3 ">
                              <div class="form-group">
                                <label class="form-control-label size">Select ID Type<span class="tx-danger">*</span></label>
                                  <div class="select">
                                    <select name="id_name" id="id_name" style="opacity: 0.8;font-size:14px">
                                        <option value="">SELECT ID TYPE</option>
                                    </select>
                                  </div>
                                  <span style="position: absolute; display: none;" id="error_id_name" class="error_size">This field is required.</span>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <label class="form-control-label size">ID Number<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="Id Number" name="id_number" id="id_number">
                                <!-- <span style="position: absolute;" id="error_vis_id_number" class="error_size"></span> -->
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="form-group">
                                <label class="form-control-label size">EFFECTIVE DATE <span class="tx-danger">*</span></label>
                                <input type="text" name="effective_date" id="effective_date" placeholder="Effective Date" class="form-control prevent_readonly" readonly="readonly">
                                <span style="position: absolute;" id="error_effective_date" class="error_size">This field is required.</span>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="form-group">
                                <label class="form-control-label size">END DATE</label>
                                <input type="text" name="end_date" id="end_date" placeholder="End Date" class="form-control prevent_readonly" readonly="readonly">
                                <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-5">
                          <div class="row">

                            <div class="col-md-4">
                              <div class="form-group">
                                <div id="my_camera"></div><br/>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <button type="button" class="btn btn-block prk_button" onClick="take_snapshot()"><p style="margin-bottom: 0px;font-size: 11px;margin-left: -3px;">Capture<br>Image</p></button>
                                <button type="button" class="btn btn-block prk_button" id="my-button"><p style="margin-bottom: 0px;font-size: 11px;margin-left: -2px;">Upload<br>Image</p></button>
                                <input type="file" name="visitor_img" accept="image/*" id="visitor_img" style="visibility: hidden;">
                                <input type="hidden" name="vis_image" class="vis_image" id="vis_image">
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <div id="results">captured Visitor image will appear here...</div>
                                <!-- <span  id="error_vis_image" class="error_size"></span> -->
                                <label style="position: absolute;" class="error_size" id="error_vis_image"></label>
                              </div>
                            </div><div class="col-md-1"></div>


                            <div class="col-md-4">
                              <div class="form-group">
                                <div id="my_camera2"></div><br/>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <button type="button" class="btn btn-block prk_button" onClick="take_snapshot2()"><p style="margin-bottom: 0px;font-size: 11px;margin-left: -3px;">Capture<br>Image</p></button>
                                <button type="button" class="btn btn-block prk_button" id="my-button2"><p style="margin-bottom: 0px;font-size: 11px;margin-left: -2px;">Upload<br>Image</p></button>
                                <input type="file" name="visitor_img2" accept="image/*" id="visitor_img2" style="visibility: hidden;">
                                <input type="hidden" name="id_image" class="id_image" id="id_image">
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <div id="results2">captured choose Id image will appear here...</div>
                                <!-- <span style="position: absolute;" id="error_id_image" class="error_size"></span> -->
                                <label style="position: absolute;" class="error_size" id="error_id_image"></label>
                              </div>
                            </div><div class="col-md-1"></div>
                          </div>
                        </div>
                      </div>
                      <div class="row mg-b-25">
                        <div class="col-lg-2">
                          <div class="form-layout-footer mg-t-30">
                            <input type="button" value="OTP Send" class="btn btn-block btn-primary prk_button" name="send_otp" id="send_otp">
                          </div>
                        </div>
                        <div class="col-lg-3">
                          <div class="form-group">
                            <label class="form-control-label size">OTP Number</label>
                            <input type="text" name="vis_otp" id="vis_otp" placeholder="OTP" class="form-control prevent_readonly" maxlength="6">
                            <span style="position: absolute;" id="error_vis_otp" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-2">
                          <div class="form-layout-footer mg-t-30">
                            <input type="button" value="Verify" class="btn btn-block btn-primary prk_button" name="otp_verify" id="otp_verify">
                          </div>
                        </div>
                        <div class="col-lg-3">
                          <div class="form-group">
                            <label class="form-control-label size">Visitor Gate Pass Number</label>
                            <input type="text" name="visitor_gate_pass_num" id="visitor_gate_pass_num" placeholder="Visitor Gate Pass Number" class="form-control prevent_readonly" readonly="" style="font-size:13px;">
                            <span style="position: absolute;" id="error_vis_gate_pass_num" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-layout-footer mg-t-30">
                            <input type="button" value="Save" class="btn btn-block btn-primary prk_button" name="visitor_save" id="visitor_save">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-layout-footer mg-t-30">
                              <button type="button" class="btn btn-block prk_button" name="vis_work_add" id="vis_work_add">+ ADD ROW</button>
                            </div>
                        </div>
                         <div class="col-lg-6">
                         </div>
                         <!-- style="margin-bottom:10px;" -->
                        <div class="col-lg-3">
                            <div class="form-layout-footer mg-t-30">
                              <button  type="button" class="btn btn-block prk_button" id="back">BACK</button>
                            </div>
                        </div>
                      </div>
                      </form>
                      <div id="table_data" style="display: none;">
                        <div>
                          <table border="1" cellspacing="0" class="table table-bordered" id="dynamic_field">
                            <tr style="font-size: 14px;text-align: center;">
                              <th>Serial No</th>
                              <th>Tower</th>
                              <th>Flat No</th>
                              <th>In Time </br>Use 24-hour</th>
                              <th>Out Time </br>Use 24-hour</th>
                              <th>Remark</th>
                              <th>Action</th>
                            </tr>
                            <tr>
                              <td>1</td>
                              <td>
                                <input type="text" name="tower_name[]" id="tower_name" class="form-control name_list datetimepicker-input" placeholder="Tower Name" required=""/>
                              </td>
                              <td>
                                  <input type="text" placeholder="Flat Name" name="falt_name[]" id="falt_name" class="form-control name_list" required=""/>
                              </td>
                              <td>
                                  <input type="text" placeholder="HH:MM" name="start_date[]" id="start_date" class="form-control name_list time_pic" required=""/>
                              </td>
                              <td>
                                  <input type="text" placeholder="HH:MM" name="end_date1[]" id="end_date1" class="form-control name_list time_pic" required=""/>
                              </td>
                              <td>
                                  <input type="text" placeholder="Remark" name="remark[]" id="remark" class="form-control name_list"/>
                              </td>
                              <!-- <input type="hidden" name="prk_admin_id[]" id="prk_admin_id" class="form-control name_list" readonly="" value="<?php echo $prk_admin_id; ?>"> -->
                              <td>
                                <button type="button" name="remove" id="'+i+'" class="btn btn-success fa fa-check"></button>
                              </td>
                            </tr>
                          </table> 
                        </div>
                        <div class="col-lg-2">
                          <div class="form-layout-footer mg-t-30">
                              <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="vis_work_save" id="vis_work_save">
                          </div>
                        </div>
                    </div>
                  <!-- </form> -->
              </div>
              
            </div>
          </div>
        </div>
      </div>
      <div class="am-pagebody">
      <!-- footer part -->
      <?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE_WEB ?>"
  var prk_admin_id = "<?php echo $prk_admin_id; ?>";
  $( document ).ready( function () {
    jQuery('input').keyup(function() {
      this.value = this.value.toLocaleUpperCase();
    });
    $('#vis_work_add').attr("disabled", true);
    $('#otp_verify').attr("disabled", true);
    $('#visitor_save').attr("disabled", true);
    $('#vis_otp').attr("disabled", true);
    // alert('ok');
    var j=1;
    var t=2;
    var i=2;
    $("#back").on('click',function(){
      window.location.href='visitor-gate-pass-issue-list';
    });
    $("#vis_work_add").on('click',function(){
      // alert('ok');
      i++;
      t++;
      j++;
      count=$('table tr').length;
      // alert(count);
      var data="<tr id='row"+i+"'><td><span id='snum"+i+"'>"+count+".</span></td>";


      data +='<td><input type="text" name="tower_name[]" id="tower_name' +i+ '" class="form-control name_list" placeholder="Tower Name" required=""/></td><td><input type="text" placeholder="Flat Name" name="falt_name[]" id="falt_name' +i+ '" class="form-control name_list" required=""/></td><td><input type="text" placeholder="HH:MM" name="start_date[]" id="start_date' +i+ '" class="form-control name_list time_pic" required=""/></td><td><input type="text" placeholder="HH:MM" name="end_date1[]" id="end_date1' +i+ '" class="form-control name_list time_pic" required=""/></td><td><input type="text" placeholder="Remark" name="remark[]" id="remark' +i+ '" class="form-control name_list"/></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td><td style="visibility:hidden;"><input type="hidden" name="hide_select[]" id="myText'+t+'"></td></tr>';
      $('table').append(data);
      $(document).on('click', '.btn_remove', function(){
        var button_id = $(this).attr("id"); 
        $('#row'+button_id+'').remove();
      });
    });
    var url_id_proof_list = prkUrl+'id_proof_list.php';
    $.ajax ({
      type: 'POST',
      url: url_id_proof_list,
      data: "",
      success : function(data) {
        var obj=JSON.parse(data);
        var areaOption = "<option value=''>SELECT ID TYPE</option>";
        $.each(obj['state'], function(val, key) {
          areaOption += '<option value="' + key['all_drop_down_id'] + '">' + key['full_name'] + '</option>'
        });
        $("#id_name").html(areaOption);
      }
    });
    var urlState = prkUrl+'state.php';
    $.ajax ({
      type: 'POST',
      url: urlState,
      data: "",
      success : function(data) {
        var obj=JSON.parse(data);
        var areaOption = "<option value=''>SELECT STATE</option>";
        $.each(obj['state'], function(val, key) {
          areaOption += '<option value="' + key['sta_id'] + '">' + key['state_name'] + '</option>'
        });
        $("#visitor_state").html(areaOption);
      }
    });
    $('#visitor_state').on("change", function() {
      var id=($("#visitor_state").val());
      var urlCity = prkUrl+'city.php';
      $.ajax ({
        type: 'POST',
        url: urlCity,
        data: {id:id},
        success : function(data) {
          var obj=JSON.parse(data);
          var areaOption = "<option value=''>SELECT CITY</option>";
          $.each(obj['city'], function(val, key) {
            areaOption += '<option value="' + key['city_id'] + '">' + key['city_name'] + '</option>'
          });
          $("#visitor_city").html(areaOption);
        }
      });
    });

    $('#visitor_save').click(function(){
      
      form_contact();
    });
    $('#send_otp').click(function(){
      var effective_date=($("#effective_date").val());
      if($("#addVis").valid()&effective_date!=''&id_image()==true&vis_image()==true){
        // alert('ok');
        var mobile=($("#visitor_mobile").val());
        var name=($("#visitor_name").val());
        var email='';
        // 'mobile','name','email'
        var urlgate_pass = prkUrl+'otpSend.php';
        $('#send_otp').val('Wait ...').prop('disabled', true);
        $.ajax({
          url :urlgate_pass,
          type:'POST',
          data :
          {
            'mobile':mobile,
            'name':name,
            'email':email
          },
          dataType:'html',
          success  :function(data)
          {
            $('#send_otp').val('OTP Send').prop('disabled', false);
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status) {
              $('#otp_verify').attr("disabled", false);
              $('#vis_otp').attr("disabled", false);
              $('#send_otp').attr("disabled", true);
            }
          }
        });
        // form_contact();
        var fewSeconds = 20;
        setTimeout(function(){
            $('#send_otp').attr("disabled", false);
        }, fewSeconds*1000);
      }
    });
    $("#otp_verify").on('click',function(){
      // alert('ok');
      var mobile=($("#visitor_mobile").val());
      var otp=($("#vis_otp").val());
      var email='';
      var otp_val = prkUrl+'otpVal.php';
      $('#otp_verify').val('Wait ...').prop('disabled', true);
      $.ajax({
        url :otp_val,
        type:'POST',
        data :
        {
          'mobile':mobile,
          'otp':otp,
          'email':email
        },
        dataType:'html',
        success  :function(data)
        {
          $('#otp_verify').val('Verify').prop('disabled', true);
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status) {
            $("#error_vis_otp").text('');
            var urlgate_pass = prkUrl+'visitor_gate_pass_generate.php';
            $.ajax({
              url :urlgate_pass,
              type:'POST',
              data :
              {
                'prk_admin_id':prk_admin_id
              },
              dataType:'html',
              success  :function(data)
              {
                // alert(data);
                var json = $.parseJSON(data);

                if (json.status) {
                  $('#send_otp').attr("disabled", true);
                  $('#otp_verify').attr("disabled", true);
                  $('#visitor_save').attr("disabled", false);
                  $('#visitor_gate_pass_num').val(json.gate_pass);
                  // $('#mobile').attr("disabled", true);
                }
              }
            });
          }else{
            $("#error_vis_otp").text(json.message);
          }
        }
      });
    });
    $('#vis_work_save').click(function(){
      var urlCity = prkUrl+'visitor_gate_pass_work_dtl.php';  
      $('#vis_work_save').val('Wait ...').prop('disabled', true);
      $.ajax({  
        url:urlCity,  
        method:"POST",  
        data:$('#addVis').serialize(),  
        success:function(data)  
        {  
          $('#vis_work_save').val('SAVE').prop('disabled', false);
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success',
              content: "<p style='font-size:0.9em;'>Visitor Gatepass Issue successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                  location.reload(true);
                }
              }
            });
          }else{
             $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.7em;'>Somting went wrong 1</p>",
              type: 'red'
            });
          }
        }  
      });  
    });
  });
  function form_contact(){
    var urlCity = prkUrl+'visitor_gate_pass_dtl.php';  
    var prk_admin_id='<?php echo $prk_admin_id; ?>';
    var visitor_mobile=$('#visitor_mobile').val();
    var visitor_name=$('#visitor_name').val();
    // var visitor_gender=$('#visitor_gender').val();
    var visitor_gender =  $('input[name=visitor_gender]:checked').val();
    var visitor_add1=$('#visitor_add1').val();
    var visitor_add2=$('#visitor_add2').val();
    var visitor_state=$('#visitor_state').val();
    var visitor_city=$('#visitor_city').val();
    var visitor_landmark=$('#visitor_landmark').val();
    var visitor_country=$('#visitor_country').val();
    var visitor_pin_cod=$('#visitor_pin_cod').val();
    var id_name=$('#id_name').val();
    var id_number=$('#id_number').val();
    var visitor_gate_pass_num=$('#visitor_gate_pass_num').val();
    var effective_date=$('#effective_date').val();
    var end_date=$('#end_date').val();
    var visitor_veh_type=$('#visitor_veh_type').val();
    var visitor_veh_num=$('#visitor_veh_num').val();
    end_date = (!end_date == '') ? end_date : FUTURE_DATE;

    var inserted_by='<?php echo $parking_admin_name; ?>';

    var visitor_img=$('#vis_image').val();
    var id_proof_img=$('#id_image').val();
    // var id_proof_img = $('#id_proof_img').prop('files')[0];

    var form_data = new FormData(); 
    form_data.append('prk_admin_id', prk_admin_id); 
    form_data.append('visitor_mobile', visitor_mobile);
    form_data.append('visitor_name', visitor_name);
    form_data.append('visitor_gender', visitor_gender);
    form_data.append('visitor_add1', visitor_add1);
    form_data.append('visitor_add2', visitor_add2);
    form_data.append('visitor_state', visitor_state);
    form_data.append('visitor_city', visitor_city);
    form_data.append('visitor_landmark', visitor_landmark);
    form_data.append('visitor_country', visitor_country);
    form_data.append('visitor_pin_cod', visitor_pin_cod);
    form_data.append('id_name', id_name);
    form_data.append('id_number', id_number);
    form_data.append('visitor_gate_pass_num', visitor_gate_pass_num);
    form_data.append('effective_date', effective_date);
    form_data.append('end_date', end_date);
    form_data.append('inserted_by', inserted_by);
    form_data.append('visitor_vehicle_type', visitor_veh_type);
    form_data.append('visitor_vehicle_number', visitor_veh_num);

    form_data.append('visitor_img', visitor_img);
    form_data.append('id_proof_img', id_proof_img);
    // alert(urlCity);
    $('#visitor_save').val('Wait ...').prop('disabled', true);
    $.ajax({
      type: "POST",           
      url: urlCity,
      dataType: 'text',  // what to expect back from the PHP script, if anything
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,                         
      type: 'post',
      dataType:'html',
      success  :function(data) { 
        $('#visitor_save').val('Save').prop('disabled', true);
        // alert(data);
        var json = $.parseJSON(data);
        if (json.status){
          $('#visitor_gate_pass_dtl_id').val(json.visitor_gate_pass_dtl_id);
          $("#table_data").css("display","");         
          $('#vis_work_add').attr("disabled", false);
          $('#visitor_save').attr("disabled", true);
        }else{

          alert(data);
        }
      }
    });
  } 
  /*image preview*/
  $(function() {
    $("#visitor_img").change(function() {
      var file = this.files[0];
      var imagefile = file.type;
      var match= ["image/jpeg","image/png","image/jpg"];
      if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
      {
        $('#previewing').attr('src','../images/noimage.jpg');
        return false;
      }else{
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(this.files[0]);
      }
    });
  });
  function imageIsLoaded(e) {
    $("#visitor_img").css("color","green");
    $('#image_preview').css("display", "block");
    $('#previewing').attr('src', e.target.result);
    $('#img').attr('width', '130px');
    $('#img').attr('height', '100px');
    document.getElementById('results').innerHTML = '<img style="height: 100px;width: 130px;" src="'+e.target.result+'"/>';
  };
</script>
<script>
  $(document).ready(function(){

    $("#effective_date").blur(function () {
      var effective_date = $('#effective_date').val();
      if(effective_date == ''){
        $('#error_effective_date').show();
        return false;
      }else{
        $('#error_effective_date').hide();
        return true;
      }
    });
    addEventListener('DOMContentLoaded', function () {
      var now = new Date;
      var now = now.setDate(now.getDate() - 1);
      pickmeup('#effective_date', {
        default_date   : false,
        position       : 'bottom',
        hide_on_select : true,
        render : function (date) {
              if (date < now) {
                  return {
                    disabled : true,
                    class_name : 'date-in-past'
                  };
              }
              $('#error_effective_date').hide();
              return {};
          } 
      });
    });
    $('#end_date').focus(function () {
      var start = new Date;
      var end = $('#effective_date').val();
      var d = new Date(end.split("-").reverse().join("-"));
      var dd=d.getDate();
      var mm=d.getMonth()+1;
      var yy=d.getFullYear();
      var hh = start.getHours();
      var min = start.getMinutes();
      var sec = start.getSeconds();
      var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
      var end = new Date(newdate);
      days = (end - start) / (1000 * 60 * 60 * 24)-1;
      days = Math.round(days);
      now_end = start.setDate(start.getDate() + days);
      pickmeup('#end_date', {
        default_date   : false,
        /*position       : 'top',*/
        hide_on_select : true,
        render : function (date) {
              if (date < now_end) {
                  return {
                    disabled : true,
                    class_name : 'date-in-past'
                  };
              }
              return {};
        } 
      });
    });
    $('#effective_date').focus(function(){
      $('#end_date').val('');
    });
  });
</script>
<!-- validation -->
<script>
  $( document ).ready( function () {  
    var prk_admin_id = "<?php echo $prk_admin_id; ?>";
    var prkUrl = "<?php echo PRK_URL; ?>";
    var urlVehicleType = prkUrl+'vehicle_type.php';
    $.ajax ({
      type: 'POST',
      url: urlVehicleType,
      data: "",
      success : function(data) {
        // alert(data);
        var obj=JSON.parse(data);
        var areaOption = "<option value='' style='color:gray;' selected='selected'>SELECT TYPE</option>";
        $.each(obj, function(val, key) {
          // alert("Value is :" + key['e']);
          areaOption += '<option value="' + key['vehicle_Sort_nm'] + '">' + key['vehicle_type'] + '</option>'
        });
        $("#visitor_veh_type").html(areaOption);
      }
    });
    var urlUserMobileCheck = prkUrl+'visitor_mobile_check.php';  
    $( "#addVis" ).validate( {
      rules: {
        'terms[]': { required: true },
        visitor_name: "required",
        visitor_mobile: {
          required: true,
          number: true,
          minlength: 10,
          remote: {
            url: urlUserMobileCheck,
            type: "POST",
            data: { prk_admin_id: prk_admin_id},
            cache: false,
            dataType: "json",
            dataFilter: function(data) {
                var json = $.parseJSON(data);                      
                if (json.status) {
                    return true;
                } else {
                    return false;
                }
              }
            }
          },
        visitor_gender: "required",
        visitor_state: "valueNotEqualsState",
        visitor_city: "valueNotEqualsCity",
        visitor_pin_cod:{
          required:true,
          maxlength:6,
          minlength:6
        },
        id_name: "valueNotEqualsId",
        id_number: "required",
        effective_date: "valueNotEqualsEff",
        visitor_add1: "required"
      },
      messages: {
        visitor_mobile: {
          required: "This field is required.",
          number: "Enter valid mobile Numberr.",
          minlength: "Enter valid mobile Number.",
          remote: "Already allow the Gatepass."
        },
        visitor_gender:"This field is required.",
        visitor_pin_cod:{
          required:"This field is required.",
          minlength:"Minimum length is 6",
          maxlength:"Maximum length is 6"
        },
        id_number:"This field is required.",
        // visitor_city:"Please Select the City",
        visitor_add1:"This field is required."
      }
    });
    $("#visitor_state").on('change', function(){
      var v_type = $('#visitor_state').val();
      if(v_type == "")
      {
        $('#error_vis_state_name').show();
        return false;
      }else{
          $('#error_vis_state_name').hide();
          return true;
      }
    });
    $("#visitor_city").on('change', function(){
      var v_type = $('#visitor_city').val();
      if(v_type == "")
      {
        $('#error_vis_city_name').show();
        return false;
      }else{
          $('#error_vis_city_name').hide();
          return true;
      }
    });
    $("#id_name").on('change', function(){
      var v_type = $('#id_name').val();
      if(v_type == "")
      {
        $('#error_id_name').show();
        return false;
      }else{
          $('#error_id_name').hide();
          return true;
      }
    });
    $("#visitor_name").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 60) && (inputValue != 0 & inputValue !=32)){
        // if(!(inputValue >= 97) && (inputValue != 46)){
        event.preventDefault();
      }
    });
    $("#visitor_mobile,#visitor_pin_cod").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0)){
        event.preventDefault();
      }
    });
    $(".time_pic").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0 &inputValue != 58)){
        event.preventDefault();
      }
    });
  });
  $.validator.addMethod("valueNotEqualsState", function(value){
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_vis_state_name").show();
      return false;
    }
  });
  $.validator.addMethod("valueNotEqualsCity", function(value){
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_vis_city_name").show();
      return false;
    }
  });
  $.validator.addMethod("valueNotEqualsId", function(value){
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_id_name").show();
      return false;
    }
  });
  $.validator.addMethod("valueNotEqualsEff", function(value){
    arg = "";
    if (value != "") {
      return arg !== value;
    }else{
      $("#error_effective_date").show();
      return true;
    }
  });
  function vis_image() {
    var name = $("#vis_image").val();
    if(name== ''){
      $("#error_vis_image").text('This field is required.');
      return false;
    }else{
      $("#error_vis_image").text('');
      return true;
    }
  }
  function id_image() {
    var name = $("#id_image").val();
    if(name== ''){
      $("#error_id_image").text('This field is required.');
      return false;
    }else{
      $("#error_id_image").text('');
      return true;
    }
  }
</script>
<script language="JavaScript">
  Webcam.set({
      width: 130,
      height: 100,
      image_format: 'jpeg',
      jpeg_quality: 90
  });
  Webcam.attach( '#my_camera' );
  function take_snapshot() {
    Webcam.snap( function(data_uri) {
      $(".vis_image").val(data_uri);
      document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
      vis_image();
    });
  }
  $(function() {
    $("#visitor_img").change(function() {
      var file = this.files[0];
      var imagefile = file.type;
      var match= ["image/jpeg","image/png","image/jpg"];
      if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
      {
        $('#previewing').attr('src','../images/noimage.jpg');
        return false;
      }else{
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(this.files[0]);
      }
    });
  });
  function imageIsLoaded(e) {
    // alert('ok');
    $(".vis_image").val(e.target.result);
    document.getElementById('results').innerHTML = '<img style="height: 100px;width: 130px;" src="'+e.target.result+'"/>';
    vis_image();
  };
  $('#my-button').click(function(){
    $('#visitor_img').click();
  });
</script>
<script language="JavaScript">
  Webcam.set({
    width: 130,
    height: 100,
    image_format: 'jpeg',
    jpeg_quality: 90
  });
  Webcam.attach( '#my_camera2' );
  function take_snapshot2() {
    Webcam.snap( function(data_uri) {
      $(".id_image").val(data_uri);
      document.getElementById('results2').innerHTML = '<img src="'+data_uri+'"/>';
      id_image();
    });
  }
  $(function() {
    $("#visitor_img2").change(function() {
      var file = this.files[0];
      var imagefile = file.type;
      var match= ["image/jpeg","image/png","image/jpg"];
      if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
      {
        $('#previewing').attr('src','../images/noimage.jpg');
        return false;
      }else{
        var reader = new FileReader();
        reader.onload = imageIsLoadedaa;
        reader.readAsDataURL(this.files[0]);
      }
    });
  });
  function imageIsLoadedaa(e) {
    // alert('okk');
    $(".id_image").val(e.target.result);
    document.getElementById('results2').innerHTML = '<img style="height: 100px;width: 130px;" src="'+e.target.result+'"/>';
    id_image();
  };
  $('#my-button2').click(function(){
    $('#visitor_img2').click();
  });
</script>