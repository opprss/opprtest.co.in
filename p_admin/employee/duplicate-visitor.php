<!-- ################################################
  
Description: 
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
 
<?php include "all_nav/header.php";?>
  <!-- vendor css -->
  <style type="text/css">
    .size{
      font-size: 11px;
      text-transform: uppercase;
    }
    .error_size,.error{
      font-size: 11px;
      color: red;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
    th,tr{
      text-align: center;
    }
  </style> 
  <!-- links for datatable -->
  <link href="../../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../../lib/select2/css/select2.min.css" rel="stylesheet">

  <link href="../../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
  <link href="../../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
  <div class="am-mainpanel">
    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Duplicate Visitor</h5>
    </div>
  </div>
  <div class="am-mainpanel">
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40" style="padding: 10px;">
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <form id="duplicatePayRecForm" action="" method="post">
            <div class="row">
              <div class="col-lg-10">
                <div class="row">
                  <div class="col-lg-2">
                    <div class="form-group">
                      <label class="form-control-label size">Visitor Name <span class="tx-danger"></span></label>
                      <input type="text" name="visit_name" id="visit_name" placeholder="Visitor Name" class="form-control">
                      <span style="position: absolute;" id="error_visit_name" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <label class="form-control-label size">Visitor Mobile <span class="tx-danger"></span></label>
                      <input type="text" name="visit_mobile" id="visit_mobile" placeholder="Mobile" class="form-control">
                      <span style="position: absolute;" id="error_visit_mobile" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group" style="margin-top: 10px;">
                      <label class="form-control-label size">Tower Name<span class="tx-danger"></span></label>
                      <select name="tower_id" id="tower_id" value="" style="opacity: 0.8; font-size:14px">
                        <option value="">Select Tower</option>
                      </select>
                      <span style="position: absolute;" id="error_tower_id" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group" style="margin-top: 10px;">
                      <label class="form-control-label size">Flat Name<span class="tx-danger"></span></label>
                      <select name="flat_id" id="flat_id" value="" style="opacity: 0.8; font-size:14px">
                        <option value="">Select Flat</option>
                      </select>
                      <span style="position: absolute;" id="error_flat_id" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <label class="form-control-label size">start DATE <span class="tx-danger">*</span></label>
                      <input type="text" name="start_date" id="start_date" placeholder="MM/DD/YYYY" class="form-control prevent_readonly" readonly="readonly">
                      <span style="position: absolute;" id="error_start_date" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <label class="form-control-label size">END DATE <span class="tx-danger">*</span></label>
                      <input type="text" name="end_date" id="end_date" placeholder="MM/DD/YYYY" class="form-control prevent_readonly" readonly="readonly">
                      <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-layout-footer mg-t-30">
                  <button type="button" class="btn btn-block prk_button" id="show">Search</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="am-pagebody" id="data">
      <div class="card pd-20 pd-sm-40" style="padding: 10px;">
        <div class="table-wrapper">
          <table id="datatable1" class="table display responsive nowrap" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="wd-5p">Image</th>
                <th class="wd-15p">Name</th>
                <th class="wd-10p">Mobile</th>
                <th class="wd-10p">In Date</th>
                <th class="wd-10p">In Time</th>
                <th class="wd-10p">Out Date</th>
                <th class="wd-10p">Out Time</th>
                <th class="wd-5p">Action</th>
              </tr>
            </thead>
            <tbody id="dataLines">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <?php include"all_nav/footer.php"; ?>
  <script type="text/javascript">
    var vis_prient_in_flag = "<?php echo $vis_prient_in_flag ;?>";
    var vis_prient_out_flag = "<?php echo $vis_prient_out_flag ;?>";
    $( document ).ready( function () {
      duplicateVisitorTransactions('','','','','','');
      $('#show').click(function () {
        if($("#duplicatePayRecForm").valid()){
          // alert('ok');
          var start_date = $('#start_date').val();
          var end_date = $('#end_date').val();
          var visit_name = $('#visit_name').val();
          var visit_mobile = $('#visit_mobile').val();
          var tower_id = $('#tower_id').val();
          var flat_id = $('#flat_id').val();
          duplicateVisitorTransactions(start_date,end_date,visit_name,visit_mobile,tower_id,flat_id);
        }
      });
    });
    addEventListener('DOMContentLoaded', function () {
      var now = new Date;
      var now = now.setDate(now.getDate() - 1);
      pickmeup('#start_date', {
        default_date   : false,
        position       : 'bottom',
        hide_on_select : true,
        render : function (date) {
          return {};
        } 
      });
    });
    $('#end_date').focus(function () {
      var start = new Date;
      var end = $('#start_date').val();
      var d = new Date(end.split("-").reverse().join("-"));
      var dd=d.getDate();
      var mm=d.getMonth()+1;
      var yy=d.getFullYear();
      var hh = start.getHours();
      var min = start.getMinutes();
      var sec = start.getSeconds();
      var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
      var end = new Date(newdate);
      days = (end - start) / (1000 * 60 * 60 * 24)-1;
      days = Math.round(days);
      now_end = start.setDate(start.getDate() + days);
      pickmeup('#end_date', {
        default_date   : false,
        hide_on_select : true,
        render : function (date) {
          if (date < now_end) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
          }
          return {};
        } 
      });
    });
    $('#start_date').focus(function(){

      $('#end_date').val('');
    });
  </script>
  <script src="../../lib/highlightjs/highlight.pack.js"></script>
  <script src="../../lib/datatables/jquery.dataTables.js"></script>
  <script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
  <script src="../../lib/datatables-responsive/jquery.dataTables.min.js"></script>
  <script src="../../lib/datatables-responsive/dataTables.buttons.min.js"></script>
  <script src="../../lib/datatables-responsive/jszip.min.js"></script>
  <script src="../../lib/datatables-responsive/pdfmake.min.js"></script>
  <script src="../../lib/datatables-responsive/vfs_fonts.js"></script>
  <script src="../../lib/datatables-responsive/buttons.html5.min.js"></script>
  <script>
    function duplicateVisitorTransactions(start_date,end_date,visit_name,visit_mobile,tower_id,flat_id){
      $('#refresh_button').show();
      var urlVehNoCheck = pkrEmpUrl+'duplicate_visitor_list.php';
      // 'prk_admin_id','visit_mobile','visit_name','start_date','end_date'
      $.ajax({
        url :urlVehNoCheck,
        type:'POST',
        data :{
          'prk_admin_id':prk_admin_id,
          'prk_area_user_id':prk_area_user_id,
          'visit_name':visit_name,
          'visit_mobile':visit_mobile,
          'tower_id':tower_id,
          'flat_id':flat_id,
          'start_date':start_date,
          'end_date':end_date,
          'token':token
        },
        dataType:'html',
        success  :function(data){
          // alert(data);
          var demoLines = '';
          var json = $.parseJSON(data);
          if (json.status) {
            for (var key in json.visiter) {
              // alert(key);
              $.each(json.visiter[key], function() {
                demoLines += '<tr>\
                  <td class="wd-5p"><img src="'+this['visit_img']+'" style="height: 40px; border-radius: 50%;"></td>\
                  <td class="wd-10p">'+this['visit_name']+'</td>\
                  <td class="wd-10p">'+this['visit_mobile']+'</td>\
                  <td class="wd-10p">'+this['visiter_in_date']+'</td>\
                  <td class="wd-10p">'+this['visiter_in_time']+'</td>\
                  <td class="wd-10p">'+this['visiter_out_date']+'</td>\
                  <td class="wd-10p">'+this['visiter_out_time']+'</td>\
                  <td class="wd-5p">';
                if(vis_prient_out_flag == 'Y'){
                  demoLines += '<button class="out btn prk_button print" id="'+this['prk_visit_dtl_id']+'" onclick="duplicatePrint(this.id)" style="color: #FFF;"><i class="fa fa-print"></i> PRINT</button></td></tr>';
                }else{
                  demoLines += '<button class="out btn prk_button print" id="'+this['prk_visit_dtl_id']+'" onclick="duplicatePrint(this.id)" style="color:white; background-color:#808080;" disabled><i class="fa fa-print"></i> PRINT</button></td></tr>';
                }
              });
            }
          }
          $('#datatable1').dataTable().fnDestroy();
          $("#dataLines").html(demoLines);
          datatable_show();
        }
      });
    }
    function datatable_show(){
      'use strict';
      $('#datatable1').DataTable({
        responsive: true,
        dom: 'Blfrtip',
        language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
        },
        // lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
          {
            text: '<span id="refresh_button"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
            action: function ( e, dt, node, config ) {
              duplicateVisitorTransactions('','');
              $('#start_date').val('');
              $('#end_date').val('');
            }
          },
          { 
            extend: 'copyHtml5', 
            footer: true,
            title: 'VISITOR TRANSACTION DETAILS SHEET' 
          },
          { 
            extend: 'excelHtml5', 
            footer: true,
            title: 'VISITOR TRANSACTION DETAILS SHEET'  
          },
          {
              extend: 'pdfHtml5',
              footer: true,
              title: 'VISITOR TRANSACTION DETAILS SHEET',
              customize: function(doc) {
                doc.content[1].margin = [ 60, 0, 0, 0 ] //left, top, right, bottom
              },
              exportOptions: {
                  columns: [1,2,3,4,5,6]
              }
          },
        ]
      });
      $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
      $('#refresh_button').hide();
    }
    function upperCase(a){
      setTimeout(function(){
          a.value = a.value.toUpperCase();
      }, 1);
    }
    function duplicatePrint(str){
      urlPrint = pkrEmpUrl+"visitor_out_print?id="+str+'&duplicate=Y';
      window.open(urlPrint,'mywindow','width=850,height=600');
      location.reload();
    }
  </script>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#tower_id").select2();
      $("#flat_id").select2();
      var urlTowerList = pkrEmpUrl+'tower_list.php';
      // 'prk_admin_id','prk_area_user_id','prk_user_username','token'
      $.ajax ({
        type: 'POST',
        url: urlTowerList,
        data: {
          'prk_admin_id':prk_admin_id,
          'prk_area_user_id':prk_area_user_id,
          'prk_user_username':prk_user_name,
          'token':token
        },
        success : function(data) {
          // alert(data);
          var obj=JSON.parse(data);
          //console.log(obj);
          var areaOption = "<option value=''>Select Tower</option>";
          $.each(obj['tower_list'], function(val, key) {
            areaOption += '<option value="' + key['tower_id'] + '">' + key['tower_name'] + '</option>'
          });
          $("#tower_id").html(areaOption);
          $("#tower_id").select2();
        }
      });
      $('#tower_id').on("change", function() {
        var id=($("#tower_id").val());
        var urlFlatList = pkrEmpUrl+'flat_list.php';
        // 'prk_admin_id','prk_area_user_id','prk_user_username','token','tower_id'
        $.ajax ({
          type: 'POST',
          url: urlFlatList,
          data: {
            prk_admin_id:prk_admin_id,
            prk_area_user_id:prk_area_user_id,
            prk_user_username:prk_user_name,
            token:token,
            tower_id:id
          },
          success : function(data) {
            // alert(data);
            var obj=JSON.parse(data);
            var areaOption = "<option value=''>Select Flat</option>";
            if (obj['status']) {
              $.each(obj['flat_list'], function(val, key) {
                areaOption += '<option value="' + key['flat_id'] + '">' + key['flat_name'] + '</option>'
              });
            }
            $("#flat_id").html(areaOption);
            $("#flat_id").select2();
          }
        });
      });
    });
  </script>