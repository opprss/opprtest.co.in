<!-- ################################################
  
Description: Parking area can issue gate pass to a particular user.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
 <?php 
include "all_nav/header.php";
if (isset($_SESSION['prk_admin_id'])) {

  $user_name=$_SESSION['parking_admin_name'];
  $prk_admin_id = $_SESSION['prk_admin_id'];
}
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: gray;

  }
  .success{
    font-size: 11px;
    color: green;
}
</style>
    <!-- for datatable -->
    <link href="../lib/highlightjs/github.css" rel="stylesheet">
    <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet"> 

<!-- header position -->

    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Money Allocation</h5>
        <!--<form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card pd-20 pd-sm-40 col-md-12">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <div class="row">
            <div class="col-md-12">
              
                <form id="moneyAllocForm" method="get" action="">
                  <div class="form-layout">
                    <div class="row mg-b-25">
                      <div class="col-lg-3 error_show">
                        <div class="form-group">
                            <label class="form-control-label size">EMPLOYEE NAME <span class="tx-danger">*</span></label>
                            <div class="select">
                              <select name="employee" id="employee" style="font-size: 14px; opacity: 0.8;">
                                  <option value="">SELECT EMPLOYEE</option>
                                  <?php
                                      $respon=prk_sub_user_list($prk_admin_id);
                                      foreach($respon as $value){
                                     ?>
                                      <option value="<?php echo $value['prk_area_user_id'];?>"><?php echo $value['prk_user_name'];?></option>

                                      <?php
                                      }
                                  ?>
                              </select>
                            </div>
                            <span style="position: absolute;" id="error_employee" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="form-control-label size">DATE <span class="tx-danger">*</span></label>
                          <input type="text" class="form-control readonly" placeholder="DATE" name="date" id="date" readonly="readonly" value="<?php echo date("d-m-Y") ?>" style="background-color: transparent;">
                          <span style="position: absolute;" id="error_prk_gate_pass_num" class="error_size"></span>
                        </div>
                      </div>
                    </div>
                    <div class="row mg-b-25">
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            RS. 2000 X
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <!-- <label class="form-control-label size">RS. 2000 X </label> -->
                              <input type="text" class="form-control mg-0" style="margin-top: -10px;" placeholder="" name="rs2000" id="rs2000" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="getResult('2000',this.value);">
                              <span style="position: absolute;" id="error_rs2000" class="error_size"></span>
                            </div>
                          </div>
                          <div class="col-md-5">
                            = Rs <span id="rs2000_res">0</span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            RS. 200 X
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <!-- <label class="form-control-label size">RS. 2000 X </label> -->
                              <input type="text" class="form-control" style="margin-top: -10px;" name="rs200" id="rs200" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="getResult('200',this.value);">
                              <span style="position: absolute;" id="error_rs200" class="error_size"></span>
                            </div>
                          </div>
                          <div class="col-md-5">
                            = Rs <span id="rs200_res">0</span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            RS. 100 X
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <!-- <label class="form-control-label size">RS. 2000 X </label> -->
                              <input type="text" class="form-control" style="margin-top: -10px;" name="rs100" id="rs100" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="getResult('100',this.value);">
                              <span style="position: absolute;" id="error_rs100" class="error_size"></span>
                            </div>
                          </div>
                          <div class="col-md-5">
                           = Rs <span id="rs100_res">0</span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            RS. 50 X
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <!-- <label class="form-control-label size">RS. 2000 X </label> -->
                              <input type="text" class="form-control mg-0" style="margin-top: -10px;"name="rs50" id="rs50" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="getResult('50',this.value);">
                              <span style="position: absolute;" id="error_rs50" class="error_size"></span>
                            </div>
                          </div>
                          <div class="col-md-5">
                            = Rs <span id="rs50_res">0</span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            RS. 20 X
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <!-- <label class="form-control-label size">RS. 2000 X </label> -->
                              <input type="text" class="form-control" style="margin-top: -10px;" name="rs20" id="rs20" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="getResult('20',this.value);">
                              <span style="position: absolute;" id="error_rs20" class="error_size"></span>
                            </div>
                          </div>
                          <div class="col-md-5">
                            = Rs <span id="rs20_res">0</span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            RS. 10 X
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <!-- <label class="form-control-label size">RS. 2000 X </label> -->
                              <input type="text" class="form-control" style="margin-top: -10px;" name="rs10" id="rs10" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="getResult('10',this.value);">
                              <span style="position: absolute;" id="error_rs10" class="error_size"></span>
                            </div>
                          </div>
                          <div class="col-md-5">
                            = Rs <span id="rs10_res">0</span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            RS. 5 X
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <!-- <label class="form-control-label size">RS. 2000 X </label> -->
                              <input type="text" class="form-control" style="margin-top: -10px;" name="rs5" id="rs5" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="getResult('5',this.value);">
                              <span style="position: absolute;" id="error_rs5" class="error_size"></span>
                            </div>
                          </div>
                          <div class="col-md-5">
                            = Rs <span id="rs5_res">0</span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            COIN (RS)
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <!-- <label class="form-control-label size">RS. 2000 X </label> -->
                              <input type="text" class="form-control" style="margin-top: -10px;" name="coin" id="coin" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="getResult('coin',this.value);">
                              <span style="position: absolute;" id="error_one_hundredd" class="error_size"></span>
                            </div>
                          </div>
                          <div class="col-md-5" >
                            = Rs <span id="rscoin_res">0</span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            <b>TOTAL = 
                          </div>
                          <div class="col-md-8">
                            Rs <span id="total_res">0</span></b>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-layout-footer mg-t-30">
                          <div class="row">
                            <div class="col-lg-6">
                              <button type="button" class="btn btn-block prk_button" name="save" id="save">Save</button>
                            <span style="position: absolute; color: red;" id="error_all" class="error_size"></span>
                            </div>
                            <div class="col-lg-6">
                              <button type="reset" class="btn btn-block prk_button_skip" name="reset" id="reset">RESET</button>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div><!-- form-layout -->
                </form>
            </div>
            
          </div>
        </div>
        </div><!-- card -->
        <!-- /add employee form -->
      </div><!-- am-pagebody -->
      
      <!-- datatable -->
      <div class="am-pagebody">

        <div class="card pd-20 pd-sm-40">
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive">
              <thead>
                <tr>
                  <th class="">Employee Name</th>
                  <th class="">Allocation Date</th>
                  <th class="">Amount</th>
                  <th class="">Refund Status</th>
                  <th class="">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $respon= prk_emp_money_aloc_list($prk_admin_id);
                $respon = json_decode($respon, true);
                //print_r($respon);
              foreach($respon['allplayerdata'] as $value){
                $prk_emp_money_aloc_id = $value['prk_emp_money_aloc_id'];
                //echo "<br><br><br><br>".$prk_emp_id;
                // [0] => Array ( [prk_emp_id] => 3 [prk_emp_name] => SUBHENDU BERA [issued_date] => 01-07-2018 [rs2000] => 0 [rs200] => 0 [rs100] => 0 [rs50] => 0 [rs20] => 0 [rs10] => 0 [rs5] => 0 [coin] => 0 [total] => 0 [refund_flag] => N [refund_date] => )
                 ?>

               <tr style="<?php if($value['refund_flag'] == FLAG_Y) echo 'background-color: #CCFFCC'; ?>">
                 <!-- <td><img src="<?php //echo OPPR_BASE_URL.$value['vehicle_typ_img']; ?>" style="height: 40px;"> </td> -->
                 
                 <td  style="<?php if($value['refund_flag'] == FLAG_Y) echo 'background-color: #CCFFCC'; ?>"><?php echo $value['prk_emp_name']; ?></td>
                 <td><?php echo $value['issued_date']; ?></td>
                 <td><?php echo $value['total']; ?></td>
                 <td>
                  <?php 
                    if ($value['refund_flag'] == FLAG_Y) {
                      echo "YES - ".$value['refund_date'];
                    }else{
                      echo 'NOT REFUND YET';
                    }
                  ?>  
                  </td>
                 <td>
                  <button type="button" class="clean-button" name="refund" id="refund<%=count++%>" value="<?php echo $prk_emp_money_aloc_id; ?>" data-toggle="tooltip" data-placement="top" title="refund" <?php if($value['refund_flag'] == FLAG_Y) echo 'disabled=disabled style="background-color:transparent; border:none"';  ?>><i class="fa fa-retweet" style="font-size:18px;"></i></button>

                  <?php 
                    if($value['refund_flag'] != FLAG_Y){
                      $href = 'href=employee-money-allocation-update?id='.$prk_emp_money_aloc_id;
                    }else{
                      $href = '';
                    }
                  ?>

                    <a <?php echo $href; ?> data-toggle="tooltip" data-placement="top" title="Modify"><i class="fa fa-edit" style="font-size:18px"></i></a>

                    <button type="button" class="clean-button" name="delete" id="delete<%=count++%>" value="<?php echo $prk_emp_money_aloc_id; ?>" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>

                 </td>
              </tr>
                <?php } ?>
         
                </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->
      <!-- /datatable -->
      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<style>
 .error_size{
  font-size: 11px;
  color: red;

}
.success{
  font-size: 11px;
  color: green;
}
</style>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE_WEB ?>";
 /*delete popup*/  
  $('button[id^="delete"]').on('click', function() {
    var prk_admin_id = "<?php echo $prk_admin_id;?>";
    var user_name=' <?php echo $parking_admin_name; ?>';
    var id = this.value;
    /*'prk_admin_id','prk_emp_money_aloc_id','user_name','token'*/
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the item permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            if (id != '') {
              var urlDtl = prkUrl+'prk_emp_money_aloc_list_delete.php';
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :
                {
                  'prk_emp_money_aloc_id':id,
                  'user_name':user_name,
                  'prk_admin_id':prk_admin_id,
                  'token': '<?php echo $token;?>'
                },
                dataType:'html',
                success  :function(data)
                {
                  var json = $.parseJSON(data);
                  if (json.status){
                    location.reload(true);
                  }else{
                    if( typeof json.session !== 'undefined'){
                      if (!json.session) {
                        window.location.replace("logout.php");
                      }
                    }else{
                      $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                    }
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  });
 /*/delete popup*/

 /*refund popup*/  
  $('button[id^="refund"]').on('click', function() {
    var prk_admin_id = "<?php echo $prk_admin_id;?>";
    var user_name=' <?php echo $parking_admin_name; ?>';
    var id = this.value;

    $.confirm({
      title: 'Choose Refund Date',
      useBootstrap: false,
      boxWidth: '30%',
      theme: 'modern',
     content: '' +
    '<form action="" class="formName">' +
    '<div class="form-group">' +
    '<section><article><div class="single"></div></article></section>' +
    '</div>' +
    '</form>',
      type: 'greens',
      buttons: {
        formSubmit: {
            text: 'Refund done',
            btnClass: 'btn-green',
            action: function () {
                var password = this.$content.find('.single').val();
                var date = pickmeup('.single').get_date(true);
                //'prk_emp_money_aloc_id','prk_admin_id','refund_date','token'
                var urlEmpForgetPass = prkUrl+'prk_emp_money_aloc_refund.php';
                $.ajax({
                  url :urlEmpForgetPass,
                  type:'POST',
                  data :
                  {
                    'prk_admin_id':prk_admin_id,
                    'prk_emp_money_aloc_id':id,
                    'refund_date':date,
                    'token': '<?php echo $token;?>'
                  },
                  dataType:'html',
                  success  :function(data)
                  {
                    var json = $.parseJSON(data);
                    if (json.status){
                        $.alert({
                        icon: 'fa fa-smile-o',
                        theme: 'modern',
                        title: 'Success',
                        content: "Refund updated successfully",
                        type: 'green',
                        buttons: {
                          Ok: function () {
                              //location.reload(true);
                          }
                        }
                      });
                    }else{
                      if( typeof json.session !== 'undefined'){
                          if (!json.session) { 
                            window.location.replace("logout.php");                    
                          }                  
                        }else{                    
                          $.alert({                    
                            icon: 'fa fa-frown-o',                    
                            theme: 'modern',                    
                            title: 'Error !',                    
                            content: "Somting went wrong",                    
                            type: 'red'                  
                          });                  
                        }                
                      }
                  }
                });
            }
        },
        cancel: function () {
            //close
        },
      },
      onContentReady: function () {
            pickmeup('.single', {
              flat : true
            });
        }
    });
  });
 


  /*addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#start_date', {
      default_date   : false,
      hide_on_select : true,
      render : function (date) {
            if (date < now) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            $('#error_date').text('');
            return {};
        } 
    });
  });*/

 /*/refund popup*/
 /*rs200_res*/
  function getResult(id, value){
    /*alert(value);
      if (value== '') {
        //alert('nunnn');
        $("#rs2000").val("0");
      }*/
      var field_id = "rs"+id;
      var result_id = "rs"+id+"_res";
      var result = id*value;

      if (id == "coin") {
        var result = 1*value;
      }

       $('#' + result_id).text(result);

       //var rs2000 = $('#rs2000').val();
       var total = parseInt($("#rs2000_res").html())+parseInt($("#rs200_res").html())+parseInt($("#rs100_res").html())+parseInt($("#rs50_res").html())+parseInt($("#rs20_res").html())+parseInt($("#rs10_res").html())+parseInt($("#rs5_res").html())+parseInt($("#rscoin_res").html());
       //alert(total);
       $('#total_res').text(total);
  }

  $( document ).ready( function () {

  	$('#reset').click(function(){
  	 $('.error_size').text('');
  	});
      
    /*$( "#employee" ).change(function() {
      //$('#moneyAllocForm')[0].reset();
      alert(this.text)
      var emp_name = $('option:selected').text();
      alert(emp_name);
      var prk_sub_unit_id = $('#sub_unit_name').val();
      var prk_admin_id = "<?php //echo $prk_admin_id;?>";
      var urlGatePassGenerate = prkUrl+'gate_pass_generate.php';
    });	*/
    /*FORM SUBMIT*/
    $('#save').click(function(){
        var prk_admin_id = "<?php echo $prk_admin_id;?>";
        var emp_name = $("#employee").find(":selected").text();
        var emp_id = $("#employee").find(":selected").val();
        var inserted_by = "<?php echo $user_name; ?>";
        var token = "<?php echo $token; ?>";
        var date = $('#date').val();
        var rs2000 = $('#rs2000_res').html();
        var rs200 = $('#rs200_res').html();
        var rs100 = $('#rs100_res').html();
        var rs50 = $('#rs50_res').html();
        var rs20 = $('#rs20_res').html();
        var rs10 = $('#rs10_res').html();
        var rs5 = $('#rs5_res').html();
        var rscoin = $('#rscoin_res').html();
        var total = $('#total_res').html();

        //alert(prk_admin_id+"--"+emp_name+"--"+emp_id+"--"+inserted_by+"--"+token+"--"+date+"--"+rs2000+"--"+rs200+"--"+rs100+"--"+rs50+"--"+rs20+"--"+rs10+"--"+rs5+"--"+rscoin+"--"+total)
        //alert(emp_id);
        //end_date = (!end_date == '') ? end_date : FUTURE_DATE;

        if (emp_id!='') {

          var urlEmpMoneyAlloc = prkUrl+'prk_emp_money_aloc.php';
          //'prk_emp_id', 'prk_admin_id', 'prk_emp_name', 'rs2000', 'rs200', 'rs100','rs50', 'rs20', 'rs10', 'rs5', 'coin', 'total', 'issued_date', 'inserted_by', 'inserted_date','token'
          alert(urlEmpMoneyAlloc);
          $.ajax({
            url :urlEmpMoneyAlloc,
            type:'POST',
            data :
            {
              'prk_emp_id':emp_id,
              'prk_admin_id':prk_admin_id,
              'prk_emp_name':emp_name,
              'rs2000':rs2000,
              'rs200':rs200,
              'rs100':rs100,
              'rs50':rs50,
              'rs20':rs20,
              'rs10':rs10,
              'rs5':rs5,
              'coin':rscoin,
              'total':total,
              'issued_date':date,
              'inserted_by':"<?php echo $user_name; ?>",
              'token':"<?php echo $token; ?>"  
            },
            dataType:'html',
            success  :function(data)
            {
              var json = $.parseJSON(data);
              if (json.status){
                  $.alert({
                  icon: 'fa fa-smile-o',
                  theme: 'modern',
                  title: 'Success !',
                  content: "<p style='font-size:0.9em;'>Money allocate successfully</p>",
                  type: 'green',
                  buttons: {
                    Ok: function () {
                        location.reload(true);
                    }
                  }
                });
              }else{
                if( typeof json.session !== 'undefined'){
                  if (!json.session) {
                    window.location.replace("logout.php");
                  }
                }else{
                  $.alert({
                  icon: 'fa fa-frown-o',
                  theme: 'modern',
                  title: 'Error !',
                  content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                  type: 'red'
                });
                }
              }
              
            }
          });
          
        }else{
          if (emp_id == '') {
            $('#error_employee').text('Employee name is required');
          }
        }   
    });
  
    $("#employee").blur(function () {
      var employee = $('#employee').val();
      if(employee == ''){
      $('#error_employee').text('Employee name is required');
      return false;
      }else{
        $('#error_employee').text('');
        return true;
      }
    });
  });
</script>
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>

<script>
  $(function(){
    'use strict';

    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Pages',
      }
    });

    $('#datatable2').DataTable({
      bLengthChange: false,
      searching: false,
      responsive: true
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

  });
</script>