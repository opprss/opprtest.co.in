<?php
/*
Description: User Registration.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date : 30-03-2018,04-05-2018
*/

include_once 'api/reg_api.php';
 $response = array();
 if(isAvailable(array('mobile','password','repassword','name','email','dob','gender','v_type','v_number','t_c_flag'))){
 	if(isEmpty(array('mobile','password','repassword','name','email','dob','gender','v_type','v_number','t_c_flag'))){

	 	$mobile = trim($_POST['mobile']);
	 	$password = trim($_POST['password']);
	 	$repassword = trim($_POST['repassword']);
	 	$name = trim($_POST['name']);
	 	$email = trim($_POST['email']);
	 	$dob = trim($_POST['dob']);
	 	$gender = trim($_POST['gender']);
	 	$v_type = trim($_POST['v_type']);
	 	$v_number = trim($_POST['v_number']);
		$t_c_flag = trim($_POST['t_c_flag']);
		$device_token_id = isset($_POST['device_token_id']) ? $_POST['device_token_id'] : '';
		$device_type = isset($_POST['device_type']) ? $_POST['device_type'] : '';
        
        $web_regis = web_registration($mobile,$password,$repassword,$email,$name,$dob,$gender,$v_number,$v_type,$t_c_flag,$device_token_id,$device_type);
        $json = json_decode($web_regis);
        if($json->status=='1'){
        	$_SESSION["user_admin_id"]=$json->user_admin_id;
        	$_SESSION["mobile"]=$json->mobile;
        	$_SESSION["token"]=$json->token;
	 			//header('location: ../vehicle-list');
	 		$response['status'] = 1; 
            $response['message'] = 'Login successfull';
            $response['next_page'] = 'address';
            echo json_encode($response);
        }else{
	 	 	$response['status'] = 0; 
            $response['message'] = $json->message;
            echo json_encode($response);
	 	}
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 	echo json_encode($response);
 }
?>