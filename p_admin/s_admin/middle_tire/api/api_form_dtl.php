<?php
/*
Description: Parking area account status check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function form_dtl(){
    global $pdoconn;
  // $sql = "SELECT * FROM `company` WHERE `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $sql = "SELECT * FROM `form_dtl` where `active_flag`='Y' AND `del_flag`='N' AND `form_seq`=2 AND `group_form_id`<>0";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetchALL();
    // print_r($val);
    $x=0;
    $response=array();
    foreach($val as $row){
        $response[$x]['form_id'] = $row['form_id'];
        $response[$x]['form_name'] = $row['form_name'];
        $response[$x]['form_display_name'] = $row['form_display_name'];
        $response[$x]['form_php_name'] = $row['form_php_name'];
        $response[$x]['form_seq'] = $row['form_seq'];
        
        $x=$x+1;
    }
    return ($response);
}
function form_dtl_super_admin(){
    global $pdoconn;
  // $sql = "SELECT * FROM `company` WHERE `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $sql = "SELECT * FROM `form_dtl` where `active_flag`='Y' AND `del_flag`='N' AND `form_seq`=1 AND `group_form_id`<>0";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetchALL();
    // print_r($val);
    $x=0;
    $response=array();
    foreach($val as $row){
        $response[$x]['form_id'] = $row['form_id'];
        $response[$x]['form_name'] = $row['form_name'];
        $response[$x]['form_display_name'] = $row['form_display_name'];
        $response[$x]['form_php_name'] = $row['form_php_name'];
        $response[$x]['form_seq'] = $row['form_seq'];
        
        $x=$x+1;
    }
    return ($response);
}
?>