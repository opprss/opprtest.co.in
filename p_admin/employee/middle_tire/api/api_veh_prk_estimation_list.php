<?php 
/*
Description: veh_prk_estimation  list 
Developed by: Rakhal Raj Mandal
Created Date: 08-06-2018
Update date : --------
*/ 
function veh_prk_estimation_list($prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT pve.`prk_veh_prk_est_id`,
       pve.`veh_number`,
       pve.`veh_type`,
       vt.`vehicle_typ_img`,
       pve.`veh_rate_id`,
       pve.`veh_rate_hour`,
       pve.`veh_rate`,
       pve.`no_of_time_multiple`,
       pve.`total_pay`,
       pve.`issue_date`,
       pve.`rate_validity`
		FROM   `prk_veh_prk_est` pve JOIN `vehicle_type` vt
		ON     pve.`veh_type` = vt.`vehicle_sort_nm`
		WHERE  pve.`prk_admin_id`='$prk_admin_id'
		AND    vt.`active_flag` = '".FLAG_Y."'
		AND   pve.`del_flag`='".FLAG_N."'
		ORDER BY  pve.`prk_veh_prk_est_id` DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
	    $arr_catagory = $query->fetchAll();
	    foreach($arr_catagory as $val) 
	    {
	        $response['status'] = 1;
	        $response['message'] = 'Successful';
	        $park['prk_veh_prk_est_id'] = $val['prk_veh_prk_est_id'];
	        $park['veh_number'] = $val['veh_number'];
	        $park['veh_type'] = $val['veh_type'];
	        $park['veh_rate_id'] = $val['veh_rate_id'];
	        $park['veh_rate_hour'] = $val['veh_rate_hour'];
	        $park['veh_rate'] = $val['veh_rate'];
	        $park['no_of_time_multiple'] = $val['no_of_time_multiple'];
	        $park['total_pay'] = $val['total_pay'];
	        $park['issue_date'] = $val['issue_date'];
	        $park['rate_validity'] = $val['rate_validity'];
	        $park['vehicle_typ_img'] = $val['vehicle_typ_img'];
	        array_push($allplayerdata, $park);
	        $response['estimation'] = $allplayerdata;
	    }
	}else{
		$response['status'] = 0;
	    $response['message'] = 'List Empty';
	}
    return json_encode($response);
}
?>