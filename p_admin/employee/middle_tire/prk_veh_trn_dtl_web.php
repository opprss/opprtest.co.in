<?php 
/*
Description: parking employ out vehicle
Developed by: Rakhal Raj Mandal
Created Date: 06-04-2018
Update date : 25-05-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
$re = array();
$resp = array();
if(isAvailable(array('prk_veh_trc_dtl_id','prk_out_rep_name','prk_admin_id','prk_area_user_id','prk_area_gate_id','token'))){
 	if(isEmpty(array('prk_veh_trc_dtl_id','prk_out_rep_name','prk_admin_id','prk_area_user_id','prk_area_gate_id','token'))){
 		
	 	$prk_veh_trc_dtl_id = trim($_POST['prk_veh_trc_dtl_id']);
	 	$prk_out_rep_name = trim($_POST['prk_out_rep_name']);
	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_area_user_id = trim($_POST['prk_area_user_id']);
        $prk_area_gate_id = trim($_POST['prk_area_gate_id']);
        $token = ($_POST['token']);
        $who_veh_out = FLAG_P;
        // $prk_area_short_name = 'HDA';
        $token_ch=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($token_ch);
		if($json->status=='1'){			
	        $prk_inf =prk_inf($prk_admin_id);
	        $json = json_decode($prk_inf);
	        if($json->status=='1'){
	            $prk_admin_name = $json->prk_admin_name;
	            $advance_flag = $json->advance_flag;
	            $prient_in_flag = $json->prient_in_flag;
	            $prient_out_flag = $json->prient_out_flag;
	            $sms_flag = $json->sms_flag;
	            $prk_area_name = $json->prk_area_name;
	            $prk_area_short_name = $json->prk_area_short_name;

				$rate_cal = rate_calculate($prk_veh_trc_dtl_id,$prk_out_rep_name,$prk_admin_id,$prk_area_gate_id,$who_veh_out );
				$json = json_decode($rate_cal);
			 	if($json->status=='1'){
			 		$payment_dtl_id = $json->payment_dtl_id;
	            	$veh_out_dtl = prk_veh_out_dtl($payment_dtl_id);
	            	$json = json_decode($veh_out_dtl);
	 				if($json->status=='1'){
	 					$prk_area_name=$json->prk_area_name;
	 					$veh_in_date=$json->veh_in_date;
	 					$veh_in_time=$json->veh_in_time;
	 					$veh_out_time=$json->veh_out_time;
	 					$veh_out_date=$json->veh_out_date;
	 					$total_pay=$json->total_pay;
					 	$tot_hr=$json->tot_hr;
					 	$round_hr=$json->round_hr;
					 	$payment_type=$json->payment_type;
					 	$user_mobile=$json->user_mobile;
	 					if(!empty($user_mobile)){
						 	#---------------#
			 				if($sms_flag == FLAG_Y){
			 					prk_veh_out_dtl_sms($user_mobile,$prk_area_name,$veh_in_date,$veh_in_time,$veh_out_date,$veh_out_time,$total_pay,$tot_hr,$round_hr); 
			 				}
	  					}
	  					payment_receive($payment_dtl_id,$prk_out_rep_name,$payment_type);
	  					$response = $veh_out_dtl;
	  				}
			 	}else{
			 		// $response['status'] = 0; 
	                // $response['message'] = 'Transition Failed 2';
	                // $response = json_encode($response);
	                $response = $rate_cal;
			 	}
			}else{
		 		$response['status'] = 0; 
                $response['message'] = 'Transition Failed 1';
                $response = json_encode($response);
			}
        }else{
            $response = $token_ch;
        }
 	}else{
        $response['status'] = 0;
    	$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>