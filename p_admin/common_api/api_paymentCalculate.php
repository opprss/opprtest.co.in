<?php 
/*
Description: vehicle payment calculate.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :04-04-2018
*/ 
function paymentCalculate($prk_veh_trc_dtl_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT * FROM `prk_veh_trc_dtl` WHERE `prk_veh_trc_dtl_id`='$prk_veh_trc_dtl_id' AND `prk_veh_out_time` IS NULL";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $prk_admin_id= $val['prk_admin_id'];
        $veh_number = $val['veh_number'];
        $user_admin_id = $val['user_admin_id'];
        $prk_veh_in_time = $val['prk_veh_in_time'];
        $payment_type = $val['payment_type'];
        $gate_pass_num = $val['gate_pass_num'];
        $prk_veh_type = $val['prk_veh_type'];
        $advance_pay = $val['advance_pay'];

        $start_time = new DateTime($prk_veh_in_time);
        $date= TIME;
        $out_date   = new DateTime(TIME);

        $year= $start_time->diff($out_date)->y;
        $month= $start_time->diff($out_date)->m;
         $day= $start_time->diff($out_date)->d;

        $hour= $start_time->diff($out_date)->h;
        $hour=$hour+($day*24)+($month*30*24)+($year*12*30*24);
        $minute= $start_time->diff($out_date)->i;
        $totalMinutes=($hour*60)+$minute;
        $to_ho = sprintf("%'.02d", $hour);
        $to_mi = sprintf("%'.02d", $minute);
        $to_h = $to_ho.':'.$to_mi;          
        if($payment_type == PP || $payment_type == CASH || $payment_type == WALLET){
           $sql = "SELECT * FROM `prk_area_rate` 
            WHERE `prk_vehicle_type`='$prk_veh_type' 
            AND `prk_admin_id`='$prk_admin_id' 
            AND `active_flag`='".FLAG_Y."' 
            AND `del_flag`='".FLAG_N."'
            AND  '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(`prk_area_rate`.`prk_rate_eff_date`, '%d-%m-%Y'), '%Y-%m-%d')    
            AND  DATE_FORMAT(STR_TO_DATE(IFNULL(`prk_area_rate`.`prk_rate_end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')";
            
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $prk_rate_type = $val['prk_rate_type'];
                $prk_ins_no_of_hr = $val['prk_ins_no_of_hr'];
                $prk_ins_hr_rate = $val['prk_ins_hr_rate'];
                $veh_rate_hour = $val['veh_rate_hour'];            
                $veh_rate = $val['veh_rate'];  
                          
                $prk_no_of_mt_dis = $val['prk_no_of_mt_dis'];
                $pieces = explode(":", $prk_no_of_mt_dis);
                $dis_total_minute=($pieces[0]*60)+$pieces[1];
                $ne_minute=$totalMinutes-$dis_total_minute;
                $hours = intval($ne_minute/60);
                $minutes = $ne_minute - ($hours * 60);
                if($hours>=1){
                    if(0<$minutes){
                        $total_hour=$hours+1; 
                    }else{
                        $total_hour=$hours;
                    }
                }else{
                    $total_hour=1;
                }
                $hou = $total_hour - $prk_ins_no_of_hr;
                //if($hou =<0){
                if($hou <=0){
                    $pay_total_hour = $prk_ins_no_of_hr;
                    $hur_pay = $prk_ins_hr_rate;
                }else{
                    @$cal_hr= $hou/$veh_rate_hour;
                    $round = ceil($cal_hr);
                    $pay_t = $veh_rate_hour * $round;
                    $pay_total_hour = $prk_ins_no_of_hr + $pay_t;
                    $hur_pay = $prk_ins_hr_rate + ($round *  $veh_rate);
                }
                $total_rs = $hur_pay - $advance_pay;
                if($total_rs<=0){
                    $rs= 0;
                }else{
                    $rs= $total_rs;
                }
                if($prk_rate_type =='F'){
                    $rs = $prk_ins_hr_rate -$advance_pay;
                    $response['status'] = 1;
                    $response['message'] = 'Successful';
                    $response['prk_vehicle_type'] = $prk_veh_type;
                    $response['payment_type'] = $payment_type;
                    $response['veh_number'] = $veh_number;
                    $response['Out_time'] = $date;
                    $response['prk_admin_id'] = $prk_admin_id;
                    $response['user_admin_id'] = $user_admin_id;
                    $response['need_hour'] = $to_h;
                    $response['total_hour'] = $to_h;
                    $response['advance_pay'] = $advance_pay;
                    $response['total_pay'] = $prk_ins_hr_rate;
                    $response['need_pay'] = $rs;
                    $response['gate_pass_num'] = $gate_pass_num;//14
                }elseif ($prk_rate_type =='D'){
                    $pay_total_hour = sprintf("%'.02d", $pay_total_hour);
                    $response['status'] = 1;
                    $response['message'] = 'Successful';
                    $response['prk_vehicle_type'] = $prk_veh_type;
                    $response['payment_type'] = $payment_type;
                    $response['veh_number'] = $veh_number;
                    $response['Out_time'] = $date;
                    $response['prk_admin_id'] = $prk_admin_id;
                    $response['user_admin_id'] = $user_admin_id;
                    $response['need_hour'] = $to_h;
                    $response['total_hour'] = $pay_total_hour.':00';
                    $response['advance_pay'] = $advance_pay;
                    $response['total_pay'] = $hur_pay;
                    $response['need_pay'] = $rs;
                    $response['gate_pass_num'] = $gate_pass_num;//14
                }else{
                    $response['status'] = 0;
                    $response['message'] = 'Server Down';
                }
            }else{
                $response['status'] = 0;
                $response['message'] = 'Rate Not Calculate';
            }
        }elseif ($payment_type == TDGP) {
            $response['status'] = 1;
            $response['message'] = 'Successful';
            $response['prk_vehicle_type'] = $prk_veh_type;
            $response['payment_type'] = TDGP;
            $response['veh_number'] = $veh_number;
            $response['Out_time'] = $date;
            $response['prk_admin_id'] = $prk_admin_id;
            $response['user_admin_id'] = $user_admin_id;
            $response['need_hour'] = $to_h;
            $response['total_hour'] = $to_h;
            $response['advance_pay'] = 0;
            $response['total_pay'] = 0;
            $response['need_pay'] = 0;
            $response['gate_pass_num'] = $gate_pass_num;
        }elseif ($payment_type == DGP) {
            $response['status'] = 1;
            $response['message'] = 'Successful';
            $response['prk_vehicle_type'] = $prk_veh_type;
            $response['payment_type'] = DGP;
            $response['veh_number'] = $veh_number;
            $response['Out_time'] = $date;
            $response['prk_admin_id'] = $prk_admin_id;
            $response['user_admin_id'] = $user_admin_id;
            $response['need_hour'] = $to_h;
            $response['total_hour'] = $to_h;
            $response['advance_pay'] = 0;
            $response['total_pay'] = 0;
            $response['need_pay'] = 0;
            $response['gate_pass_num'] = $gate_pass_num;
        }else{
            $response['status'] = 0;
            $response['message'] = 'Technical Issue';
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'Vehicle Allready Out';
    }
    return json_encode($response);
}
?>