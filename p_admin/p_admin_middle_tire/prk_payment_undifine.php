<?php
/*
Description: Parking adimn payment undifine.
Developed by: Manoranjan Sarkar
Created Date: 22-05-2018
Update date :
*/ 
    // require_once 'api/global_api.php';
    require_once 'api/parkAreaReg_api.php';
    $response = array();
 
    if(isAvailable(array('prk_admin_id'))){

        if(isEmpty(array('prk_admin_id'))){
            $prk_admin_id = trim($_POST['prk_admin_id']);
            
            $respon = prk_payment_undifine($prk_admin_id);
            echo ($respon);
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
        }
    }else{
         $response['status'] = 0; 
         $response['message'] = 'Invalid API Call';
         echo json_encode($response);
     }
?>