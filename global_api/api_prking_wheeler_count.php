<?php
/*
Description: new user vehicle parking parking area 
Developed by: Rakhal Raj Mandal
Created Date: 10-04-2018
Update date : 25-05-2018,
*/
function prking_wheeler_count($prk_admin_id,$veh_type,$veh_number){
    $response = array();
    $total_hwe=0;
    global $pdoconn;
    $sql = "SELECT `prk_admin_id` FROM `prk_veh_trc_dtl` WHERE `prk_admin_id`='$prk_admin_id' AND `prk_veh_type`='$veh_type' AND `veh_number`='$veh_number' AND `prk_veh_out_time` is NULL";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $response['status'] = 0;
        $response['message'] = 'This vehicle is already inside the parking area';
    }else{
        $response['status'] = 1;
        $response['message'] = 'User Entry Now';
    }
    return json_encode($response);
}
?>