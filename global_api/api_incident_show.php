<?php
/*
Description: user address show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function incident_show($user_incident_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql ="SELECT unc.`user_incident_id`, 
        unc.`user_incident_name`, 
        unc.`user_incident_type`, 
        unc.`user_incident_dtl`, 
        unc.`user_incident_number`, 
        unc.`status`, 
        unc.`incident_in_date`, 
        unc.`prk_admin_id`,
        unc.`prk_area_user_id`,
        al_d.`full_name` as `user_incident_type_name`
        FROM `user_incident` unc,`all_drop_down` al_d 
        WHERE al_d.`all_drop_down_id`=unc.`user_incident_type` 
        AND unc.`active_flag`='".FLAG_Y."'
        AND unc.`del_flag`='".FLAG_N."'
        AND unc.`user_incident_id`='$user_incident_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['user_incident_id'] = $val['user_incident_id'];
        $response['user_incident_name'] = $val['user_incident_name'];
        $response['user_incident_type'] = $val['user_incident_type'];
        $response['user_incident_dtl'] = $val['user_incident_dtl'];
        $response['user_incident_number'] = $val['user_incident_number'];
        $response['u_status'] = $val['status'];
        $response['incident_in_date'] = $val['incident_in_date'];
        $response['prk_admin_id'] = $val['prk_admin_id'];
        $response['user_incident_type_name'] = $val['user_incident_type_name'];
        $response['prk_area_user_id'] = $val['prk_area_user_id'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>