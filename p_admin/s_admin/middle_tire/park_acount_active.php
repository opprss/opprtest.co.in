<?php
/*
Description: Parking area login.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
    //oppr_super_employee_login
    require_once 'api/parkAreaReg_api.php';
    $response = array();
 
    if(isAvailable(array('parking_type','acount_status','employee_user_name','prk_admin_id'))){

        if(isEmpty(array('parking_type','acount_status','employee_user_name','prk_admin_id'))){
            $parking_type = trim($_POST['parking_type']);
            $acount_status = trim($_POST['acount_status']);
            $employee_user_name = trim($_POST['employee_user_name']);
            $prk_admin_id = trim($_POST['prk_admin_id']);
            $response = park_acount_active($parking_type,$acount_status,$employee_user_name,$prk_admin_id);
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandator';
            $response = json_encode($response);
        }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        $response = json_encode($response);
    }
    echo $response;
?>