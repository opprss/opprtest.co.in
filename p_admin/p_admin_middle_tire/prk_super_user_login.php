<?php
/*
Description: Parking area login.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
    //require_once 'api/global_api.php';
    require_once 'api/parkAreaReg_api.php';
    $response = array();
 
    if(isAvailable(array('prk_admin_name','password'))){

        if(isEmpty(array('prk_admin_name','password'))){
            $prk_admin_name = trim($_POST['prk_admin_name']);
            $prk_admin_password = trim($_POST['password']);
            $respon = prk_area_login($prk_admin_name,$prk_admin_password);

            $json = json_decode($respon);
            //print_r($json);
            if ($json->status == 0) {
                //header('Location: ' . $_SERVER['HTTP_REFERER']);
                $response['status'] = 0; 
                $response['message'] = $json->message;
                echo json_encode($response);     
            }else{
                $_SESSION["parking_admin_name"]=$json->parking_admin_name;
                $_SESSION["prk_admin_id"]=$json->prk_admin_id;
                $_SESSION["token"]=$json->token;
                //header('location: ../dashboard');

                $response['status'] = 1; 
                $response['message'] = 'Login successfull';
                $response['next_page'] = 'dashboard';
                echo json_encode($response);              
            }
            //echo ($respon);
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatoryyyyy';
            echo json_encode($response);
        }
    }else{
         $response['status'] = 0; 
         $response['message'] = 'Invalid API Calllll';
         echo json_encode($response);
     }
?>