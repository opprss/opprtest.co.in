<!-- ################################################
Description: User Address verify
Developed by: Rakhal Raj Mandal
Created Date: 27-08-2019
####################################################-->
<?php 
  include "all_nav/header.php";
  $visitor_gate_pass_dt_id=$visitor_gate_pass_dtl_id=base64_decode($_REQUEST['list_id']);
  $visitor_gate_pass_dtl=visitor_gate_pass_dtl_show($visitor_gate_pass_dtl_id);
  $visitor_gate_pass_dtl_id = json_decode($visitor_gate_pass_dtl, true);
  $visitor_gender = $visitor_gate_pass_dtl_id['visitor_gender'];
  $id_proof_list=id_proof_list();
  $id_proof_list = json_decode($id_proof_list, true);
  $state = state();
  $state = json_decode($state, true);

?>
<style type="text/css">
  .size{
    font-size: 11px;
    text-transform: uppercase;
  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
  .error_size,.error{
    font-size: 11px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
  select{
    opacity: 0.8;
    font-size: 12px;
  }
</style> 
<script src="../../js/webcam.min.js"></script>
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
<!-- header position -->
    <div class="am-mainpanel"><!-- cloding in footer -->
      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Visitor Gate Pass Modify</h5>
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card pd-20 pd-sm-40 col-md-12">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <div class="row">
              <div class="col-md-12">
                
                  <form role="form" id="addVis" method="post"  enctype="multipart/form-data">
                    <div class="form-layout">
                      <div class="row mg-b-25">
                        <input type="hidden" name="visitor_img" id="visitor_img" value="<?php echo $visitor_gate_pass_dtl_id['visitor_img']; ?>">
                        <input type="hidden" name="vis_id_prooof_img" id="vis_id_prooof_img" value="<?php echo $visitor_gate_pass_dtl_id['visitor_id_img']; ?>">
                        <input type="hidden" name="visitor_gate_pass_dtl_id" id="visitor_gate_pass_dtl_id" value="<?php echo $visitor_gate_pass_dt_id; ?>">
                        <input type="hidden" name="visitor_gate_pass_num" id="visitor_gate_pass_num" value="<?=$visitor_gate_pass_dtl_id['visitor_gate_pass_num']; ?>">
                        <div class="col-lg-3">
                          <div class="form-group">
                            <label class="form-control-label size">Name <span class="tx-danger">*</span></label>
                            <input type="text" class="form-control readonly" placeholder="Visitor Name" name="visitor_name" id="visitor_name" onkeyup="vehNoUppercase();" style="background-color: transparent;" value="<?php echo $visitor_gate_pass_dtl_id['visitor_name']; ?>" readonly>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="form-control-label size">Mobile Number<span class="tx-danger">*</span></label>
                            <input type="text" class="form-control readonly" placeholder="Mobile Number" name="visitor_mobile" id="visitor_mobile" maxlength="10" style="background-color: transparent;" value="<?php echo $visitor_gate_pass_dtl_id['visitor_mobile']; ?>" readonly>
                            <span style="position: absolute;" id="error_vis_mobile" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label class="form-control-label size">Gender<span class="tx-danger">*</span></label>
                            <?php
                            if ($visitor_gender == "M") {
                            ?>
                              <label class="rdiobox">
                                <input type="radio" name="visitor_gender" id="vis_gender_m" value="M" type="radio" checked>
                                <span>Male </span>
                              </label>
                              <label class="rdiobox">
                                <input type="radio" name="visitor_gender" id="vis_gender_f" value="F" type="radio" disabled="disabled">
                                <span>Female </span>
                              </label>
                              <?php
                              }else{
                              ?>
                              <label class="rdiobox">
                                <input type="radio" name="visitor_gender" id="vis_gender_m" value="M" type="radio" disabled="disabled">
                                <span>Male </span>
                              </label>
                              <label class="rdiobox">
                                <input type="radio" name="visitor_gender" id="vis_gender_f" value="F" type="radio" checked>
                                <span>Female </span>
                              </label>
                              <span style="position: absolute;" id="error_vis_gender" class="error_size"></span>
                              <?php
                              }
                              ?>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label class="form-control-label size">Select Vehicle Type<span class="tx-danger"></span></label>
                            <div class="select">
                              <input type="text" class="form-control " placeholder="Vehicle Type" name="visitor_veh_type" id="visitor_veh_type" value="<?php echo $visitor_gate_pass_dtl_id['vehicle_type_dec']; ?>" style="background-color: transparent; font-size: 12px;">
                            </div>
                            <span style="position: absolute; display: none;" id="error_vis_veh_type" class="error_size">Please Select Vehicle type</span>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label class="form-control-label size">Vehicle Number<span class="tx-danger"></span></label>
                            <input type="text" class="form-control" value="<?php echo $visitor_gate_pass_dtl_id['visitor_vehicle_number']; ?>" placeholder="Vehicle Number" name="visitor_veh_num" id="visitor_veh_num" style="background-color: transparent;">
                            <span style="position: absolute;" id="error_vis_veh_num" class="error_size"></span>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-10">
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label class="form-control-label size">Address 1<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control readonly" placeholder="Address 1" name="visitor_add1" id="visitor_add1" style="background-color: transparent;" value="<?php echo $visitor_gate_pass_dtl_id['visitor_add1']; ?>" readonly>
                                <span style="position: absolute;" id="error_vis_add1" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label class="form-control-label size">Address 2<span class="tx-danger"></span></label>
                                <input type="text" class="form-control readonly" placeholder="Address 2" name="visitor_add2" id="visitor_add2" style="background-color: transparent;" value="<?php echo $visitor_gate_pass_dtl_id['visitor_add2']; ?>" readonly>
                                <span style="position: absolute;" id="error_vis_add2" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label class="form-control-label size">Landmark<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control readonly" placeholder="Landmark" name="visitor_landmark" id="visitor_landmark" style="background-color: transparent;" value="<?php echo $visitor_gate_pass_dtl_id['visitor_landmark']; ?>" readonly>
                                <span style="position: absolute;" id="error_vis_landmark" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <label class="form-control-label size">Country Name<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control readonly" placeholder="Gate Pass Number" name="visitor_country" id="visitor_country" value="INDIA" style="background-color: transparent;" readonly="">
                                <span style="position: absolute;" id="error_vis_country" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <label class="form-control-label size">State Name<span class="tx-danger">*</span></label>
                                <div class="select">
                                  <select name="visitor_state_o" id="visitor_state_o" style="opacity: 0.8;font-size:14px" >
                                      <option value="">Select State</option>
                                       <?php
                                        foreach ($state['state'] as $val){ ?>
                                          <option value="<?php echo $val['sta_id']?>"><?php echo $val['state_name']?></option>
                                        <?php
                                        }
                                      ?>
                                  </select>
                                  <input type="hidden" name="visitor_state" id="visitor_state">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <label class="form-control-label size">City Name<span class="tx-danger">*</span></label>
                                <div class="select">
                                  <select name="visitor_city_o" id="visitor_city_o" style="opacity: 0.8;font-size:14px" >
                                      <option value="">SELECT CITY</option>
                                  </select>
                                  <input type="hidden" name="visitor_city" id="visitor_city">
                                </div>
                                <span style="position: absolute;" id="error_vis_city_name" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <label class="form-control-label size">Pin Code<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control readonly" placeholder="Pin Code" name="visitor_pin_cod" id="visitor_pin_cod" style="background-color: transparent;" maxlength="6" value="<?php echo $visitor_gate_pass_dtl_id['visitor_pin_cod']; ?>" readonly>
                                <span style="position: absolute;" id="error_vis_pin_code" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-lg-3 ">
                              <div class="form-group">
                                <label class="form-control-label size">Select ID Type <?php //echo($id_proof_list);?><span class="tx-danger">*</span></label>
                                  <div class="select">
                                    <select name="id_name_o" id="id_name_o" style="opacity: 0.8;font-size:14px" >
                                        <option value="">SELECT ID TYPE</option>
                                        <?php
                                        foreach ($id_proof_list['state'] as $val){ ?>
                                          <option value="<?php echo $val['all_drop_down_id']?>" <?php  if($visitor_gate_pass_dtl_id['id_name'] == $val['all_drop_down_id']) { echo "selected='selected'"; }   ?>><?php echo $val['full_name']?></option>
                                        <?php
                                        }
                                      ?>
                                    </select>
                                    <input type="hidden" name="id_name" id="id_name" value="<?php echo $visitor_gate_pass_dtl_id['id_name']; ?>">
                                  </div>
                                  <span style="position: absolute;" id="error_vis_id_name" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <label class="form-control-label size">ID Number<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control readonly" placeholder="Id Number" name="id_number" id="id_number" maxlength="10" style="background-color: transparent;" value="<?php echo $visitor_gate_pass_dtl_id['id_number']; ?>" readonly>
                                <span style="position: absolute;" id="error_vis_id_number" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="form-group">
                                <label class="form-control-label size">EFFECTIVE DATE <span class="tx-danger">*</span></label>
                                <input type="text" name="effective_date_o" id="effective_date_o" placeholder="Effective Date" class="form-control prevent_readonly" value="<?php echo $visitor_gate_pass_dtl_id['effective_date']; ?>" >
                                <span style="position: absolute;" id="error_effective_date" class="error_size"></span>

                                <input type="hidden" name="effective_date" id="effective_date" value="<?php echo $visitor_gate_pass_dtl_id['effective_date']; ?>">
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="form-group">
                                <label class="form-control-label size">END DATE</label>
                                <input type="text" name="end_date" id="end_date" placeholder="End Date" class="form-control prevent_readonly" readonly="readonly" value="<?php echo $visitor_gate_pass_dtl_id['end_date']; ?>">
                                <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-2">
                          <div class="row">

                            <div class="col-md-11">
                              <div class="form-group">
                                <div id="results">
                                <label class="form-control-label size">Visitor Image /ID Proof</label>
                                  <img id="previewing" src="<?php echo PRK_BASE_URL.''.$visitor_gate_pass_dtl_id['visitor_img']; ?>" style="height: 100px;width: 130px;">
                                </div>
                                <!-- <span  id="error_vis_image" class="error_size"></span> -->
                                <label style="position: absolute;" class="error_size" id="error_vis_image"></label>
                              </div>
                            </div><div class="col-md-1"></div>
                            <div class="col-md-11">
                              <div class="form-group">
                                <div id="results2">
                                  <img id="previewing" src="<?php echo PRK_BASE_URL.''.$visitor_gate_pass_dtl_id['visitor_id_img']; ?>" style="height: 100px;width: 130px;">
                                </div>
                                <!-- <span style="position: absolute;" id="error_id_image" class="error_size"></span> -->
                                <label style="position: absolute;" class="error_size" id="error_id_image"></label>
                              </div>
                            </div><div class="col-md-1"></div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-2">
                        <div class="form-layout-footer mg-t-30" style="display: none;">
                          <button style="margin-bottom:10px;" type="button" class="btn btn-block prk_button" name="vis_work_add" id="vis_work_add">+ ADD ROW</button>
                        </div>
                      </div>
                      <div class="row mg-b-25">
                      <div class="col-lg-2">
                        <div class="form-layout-footer mg-t-30">
                              <!-- <button type="submit" class="btn btn-block prk_button" name="vis_work_save" id="vis_work_save">SAVE</button> -->
                              <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="vis_work_save" id="vis_work_save">
                        </div>
                      </div>
                      <div class="col-lg-8"></div>
                      <div class="col-lg-2">
                        <div class="form-layout-footer mg-t-30">
                              <button type="button" class="btn btn-block prk_button" id="back">BACK</button>
                        </div>
                      </div>
                      </div>
                    </div>
                  </form>
              </div>
              
            </div>
          </div>
        </div>
      </div>
      <!-- footer part -->
      <?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
  var visitor_city = "<?php echo $visitor_gate_pass_dtl_id['visitor_city']; ?>";
  var city_name = "<?php echo $visitor_gate_pass_dtl_id['city_name']; ?>";
  var visitor_state = "<?php echo $visitor_gate_pass_dtl_id['visitor_state']; ?>";
  $( document ).ready( function () {
    $('#visitor_state_o').val(visitor_state);
    $('#visitor_state').val(visitor_state);
    $('#visitor_city').val(visitor_city);
    var demoLin = '';
    demoLin +='<option value='+visitor_city+'>'+city_name+'</option>';
    $("#visitor_city_o").html(demoLin);
    $('#send_otp').attr("disabled", true);
    $('#otp_verify').attr("disabled", true);
    $('#vis_otp').attr("disabled", true);
    $('#effective_date_o').attr("disabled", true);
    $('#visitor_state_o').attr("disabled", true);
    $('#visitor_city_o').attr("disabled", true);
    $('#id_name_o').attr("disabled", true);
    $('#visitor_veh_type').attr("disabled", true);
    $('#visitor_veh_num').attr("disabled", true);
    $('#vis_work_save').bind('click', function() {
      var visitor_gate_pass_dtl_id=$('#visitor_gate_pass_dtl_id').val();
      var end_date=$('#end_date').val();
      var urlgate_pass = pkrEmpUrl+'visitor_gate_pass_dtl_update.php';
      $('#vis_work_save').val('Wait ...').prop('disabled', true);
      // alert(visitor_gate_pass_dtl_id);
      // 'prk_admin_id','prk_area_user_id','prk_user_username','token','visitor_gate_pass_dtl_id','end_date'
      $.ajax({
        url :urlgate_pass,
        type:'POST',
        data :{
          'prk_admin_id':prk_admin_id,
          'prk_area_user_id':prk_area_user_id,
          'prk_user_username':prk_user_username,
          'token':token,
          'visitor_gate_pass_dtl_id':visitor_gate_pass_dtl_id,
          'end_date':end_date
        },
        dataType:'html',
        success  :function(data){
          // alert(data);
          $('#vis_work_save').val('SAVE').prop('disabled', false);
          var json = $.parseJSON(data);
          if (json.status) {
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success',
              content: "<p style='font-size:0.9em;'>Visitor Gatepass Modify successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                  // location.reload(true);
                  window.location='visitor-gate-pass';
                }
              }
            });
          }else{
            $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.7em;'>Somting went wrong 1</p>",
              type: 'red'
            });
          }
        }
      });
    });
    $("#back").on('click',function(){

      window.location.href='visitor-gate-pass';
    });
  });
</script>
<!-- <script type="text/javascript">
  $( document ).ready( function () {
    var j=1;
    var t=2;
    var i=2;
    $('#vis_work_add').attr("disabled", true);
    $('#otp_verify').attr("disabled", true);
    $('#visitor_save').attr("disabled", true);
    $('#vis_otp').attr("disabled", true);
    jQuery('input').keyup(function() {

      this.value = this.value.toLocaleUpperCase();
    });
    $("#back").on('click',function(){

      window.location.href='visitor-gate-pass';
    });
    $("#vis_work_add").on('click',function(){
      i++;
      t++;
      j++;
      count=$('table tr').length;
      // alert(count);
      var data="<tr id='row"+i+"'><td><span id='snum"+i+"'>"+count+".</span></td>";


      data +='<td><input type="text" name="tower_name[]" id="tower_name' +i+ '" class="form-control name_list" placeholder="Tower Name" required=""/></td><td><input type="text" placeholder="Flat Name" name="falt_name[]" id="falt_name' +i+ '" class="form-control name_list" required=""/></td><td><input type="text" placeholder="HH:MM" name="start_date[]" id="start_date' +i+ '" class="form-control name_list time_pic" required=""/></td><td><input type="text" placeholder="HH:MM" name="end_date1[]" id="end_date1' +i+ '" class="form-control name_list time_pic" required=""/></td><td><input type="text" placeholder="Remark" name="remark[]" id="remark' +i+ '" class="form-control name_list"/></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td><td style="visibility:hidden;"><input type="hidden" name="hide_select[]" id="myText'+t+'"></td></tr>';
      $('table').append(data);
      $(document).on('click', '.btn_remove', function(){
        var button_id = $(this).attr("id"); 
        $('#row'+button_id+'').remove();
      });
    });
    var urlVehicleType = pkrEmpUrl+'vehicle_type.php';
    $.ajax ({
      type: 'POST',
      url: urlVehicleType,
      data: "",
      success : function(data) {
        // alert(data);
        var obj=JSON.parse(data);
        var areaOption = "<option value='' style='color:gray;' selected='selected'>SELECT TYPE</option>";
        $.each(obj, function(val, key) {
          // alert("Value is :" + key['e']);
          areaOption += '<option value="' + key['vehicle_Sort_nm'] + '">' + key['vehicle_type'] + '</option>'
        });
        $("#visitor_veh_type").html(areaOption);
      }
    });
    var url_id_proof_list = pkrEmpUrl+'id_proof_list.php';
    $.ajax ({
      type: 'POST',
      url: url_id_proof_list,
      data: "",
      success : function(data) {
        var obj=JSON.parse(data);
        var areaOption = "<option value=''>SELECT ID TYPE</option>";
        $.each(obj['state'], function(val, key) {
          areaOption += '<option value="' + key['all_drop_down_id'] + '">' + key['full_name'] + '</option>'
        });
        $("#id_name").html(areaOption);
      }
    });
    var urlState = pkrEmpUrl+'state.php';
    $.ajax ({
      type: 'POST',
      url: urlState,
      data: "",
      success : function(data) {
        var obj=JSON.parse(data);
        var areaOption = "<option value=''>SELECT STATE</option>";
        $.each(obj['state'], function(val, key) {
          areaOption += '<option value="' + key['sta_id'] + '">' + key['state_name'] + '</option>'
        });
        $("#visitor_state").html(areaOption);
      }
    });
    $('#visitor_state').on("change", function() {
      var id=($("#visitor_state").val());
      var urlCity = pkrEmpUrl+'city.php';
      $.ajax ({
        type: 'POST',
        url: urlCity,
        data: {id:id},
        success : function(data) {
          var obj=JSON.parse(data);
          var areaOption = "<option value=''>SELECT CITY</option>";
          $.each(obj['city'], function(val, key) {
            areaOption += '<option value="' + key['city_id'] + '">' + key['city_name'] + '</option>'
          });
          $("#visitor_city").html(areaOption);
        }
      });
    });
    $('#send_otp').click(function(){
      if($("#addVis").valid() & id_image() & vis_image()){
        // alert('ok');
        var mobile=($("#visitor_mobile").val());
        var name=($("#visitor_name").val());
        var email='';
        var urlgate_pass = pkrEmpUrl+'otpSend.php';
        $('#send_otp').val('Wait ...').prop('disabled', true);
        // 'mobile','name','email'
        $.ajax({
          url :urlgate_pass,
          type:'POST',
          data :{
            'mobile':mobile,
            'name':name,
            'email':email
          },
          dataType:'html',
          success  :function(data){
            // alert(data);
            $('#send_otp').val('OTP Send').prop('disabled', false);
            var json = $.parseJSON(data);
            if (json.status) {
              $('#otp_verify').attr("disabled", false);
              $('#vis_otp').attr("disabled", false);
              $('#send_otp').attr("disabled", true);
            }
          }
        });
        var fewSeconds = 20;
        setTimeout(function(){
            $('#send_otp').attr("disabled", false);
        }, fewSeconds*1000);
      }
    });
    $("#otp_verify").on('click',function(){
      // alert('ok');
      var mobile=($("#visitor_mobile").val());
      var otp=($("#vis_otp").val());
      var email='';
      var otp_val = pkrEmpUrl+'otpVal.php';
      $('#otp_verify').val('Wait ...').prop('disabled', true);
      $.ajax({
        url :otp_val,
        type:'POST',
        data :
        {
          'mobile':mobile,
          'otp':otp,
          'email':email
        },
        dataType:'html',
        success  :function(data)
        {
          $('#otp_verify').val('Verify').prop('disabled', false);
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status) {
            $("#error_vis_otp").text('');
            var urlgate_pass = pkrEmpUrl+'visitor_gate_pass_generate.php';
            // 'prk_admin_id','prk_area_user_id','token'
            $.ajax({
              url :urlgate_pass,
              type:'POST',
              data :
              {
                'prk_admin_id':prk_admin_id,
                'prk_area_user_id':prk_area_user_id,
                'token':token
              },
              dataType:'html',
              success  :function(data){
                // alert(data);
                var json = $.parseJSON(data);

                if (json.status) {
                  $('#send_otp').attr("disabled", true);
                  $('#otp_verify').attr("disabled", true);
                  $('#visitor_save').attr("disabled", false);
                  $('#visitor_gate_pass_num').val(json.gate_pass);
                  // $('#mobile').attr("disabled", true);
                }
              }
            });
          }else{
            $("#error_vis_otp").text(json.message);
          }
        }
      });
    });
    $('#visitor_save').click(function(){
      
      form_contact();
    });
    /*$('#vis_work_save').click(function(){
      var urlCity = prkUrl+'visitor_gate_pass_work_dtl.php';  
      $('#vis_work_save').val('Wait ...').prop('disabled', true);
      $.ajax({  
        url:urlCity,  
        method:"POST",  
        data:$('#addVis').serialize(),  
        success:function(data)  
        {  
          $('#vis_work_save').val('SAVE').prop('disabled', false);
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success',
              content: "<p style='font-size:0.9em;'>Visitor Gatepass Issue successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                  location.reload(true);
                }
              }
            });
          }else{
             $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.7em;'>Somting went wrong 1</p>",
              type: 'red'
            });
          }
        }  
      });  
    });*/
  });
</script> -->
<script type="text/javascript">
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#effective_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
            if (date < now) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            $('#error_effective_date').hide();
            return {};
        } 
    });
  });
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#effective_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
            if (date < now_end) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            return {};
      } 
    });
  });
  $('#effective_date').focus(function(){
    $('#end_date').val('');
  });
</script>
<!-- <script type="text/javascript">
  $(document).ready( function () {
    var urlUserMobileCheck = pkrEmpUrl+'visitor_mobile_check.php';  
    var urlVehNoCheck = prkUrl+'v_numberChecker.php';
    $( "#addVis" ).validate( {
      rules: {
        visitor_name: "required",
        visitor_mobile: {
          required: true,
          number: true,
          minlength: 10,
          remote: {
            url: urlUserMobileCheck,
            type: "POST",
            data: { prk_admin_id: prk_admin_id},
            cache: false,
            dataType: "json",
            dataFilter: function(data) {
              var json = $.parseJSON(data);                      
              if (json.status) {
                  return true;
              } else {
                  return false;
              }
            }
          }
        },
        visitor_gender: "required",
        visitor_state: "required",
        visitor_city: "required",
        visitor_pin_cod:{
          required:true,
          maxlength:6,
          minlength:6
        },
        id_name: "required",
        id_number: "required",
        effective_date: "required",
        visitor_add1: "required",
        visitor_veh_type: {
          required: function(){
            if($("#visitor_veh_num").val() !=""){
              return true;
            }else{
              return false;
            }
          }
        },
        visitor_veh_num: {
          required: function(){
            if($("#visitor_veh_type").val() !=""){
              return true;
            }else{
              return false;
            }
          },
          remote: {
            url: urlVehNoCheck,
            type: "POST",
            data: {
              v_number: function() {
                return $( "#visitor_veh_num" ).val();
              }
            },
            cache: false,
            dataType: "json",
            dataFilter: function(data) {
              // alert(data);
              var json = $.parseJSON(data);                      
              if (json.status) {
                  return true;
              } else {
                  return false;
              }
            }
          }
        }
      },
      messages: {
        visitor_mobile: {
          required: "This field is required.",
          number: "Enter valid mobile Numberr.",
          minlength: "Enter valid mobile Number.",
          remote: "Already allow the Gatepass."
        },
        visitor_gender:"This field is required.",
        visitor_pin_cod:{
          required:"This field is required.",
          minlength:"Minimum length is 6",
          maxlength:"Maximum length is 6"
        },
        visitor_veh_num: {
          required: "This field is required.",
          remote: "Vehicle number is invalid."
        }
      }
    });
    $("#visitor_name").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 60) && (inputValue != 0 & inputValue !=32)){
        // if(!(inputValue >= 97) && (inputValue != 46)){
        event.preventDefault();
      }
    });
    $("#visitor_mobile,#visitor_pin_cod").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0)){
        event.preventDefault();
      }
    });
    $(".time_pic").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0 &inputValue != 58)){
        event.preventDefault();
      }
    });
  });
  function vis_image() {
    var name = $("#vis_image").val();
    if(name== ''){
      $("#error_vis_image").text('This field is required.');
      return false;
    }else{
      $("#error_vis_image").text('');
      return true;
    }
  }
  function id_image() {
    var name = $("#id_image").val();
    if(name== ''){
      $("#error_id_image").text('This field is required.');
      return false;
    }else{
      $("#error_id_image").text('');
      return true;
    }
  }
  function form_contact(){
    var visitor_mobile=$('#visitor_mobile').val();
    var visitor_name=$('#visitor_name').val();
    var visitor_gender =  $('input[name=visitor_gender]:checked').val();
    var visitor_add1=$('#visitor_add1').val();
    var visitor_add2=$('#visitor_add2').val();
    var visitor_state=$('#visitor_state').val();
    var visitor_city=$('#visitor_city').val();
    var visitor_landmark=$('#visitor_landmark').val();
    var visitor_country=$('#visitor_country').val();
    var visitor_pin_cod=$('#visitor_pin_cod').val();
    var id_name=$('#id_name').val();
    var id_number=$('#id_number').val();
    var visitor_gate_pass_num=$('#visitor_gate_pass_num').val();
    var effective_date=$('#effective_date').val();
    var end_date=$('#end_date').val();
    var visitor_veh_type=$('#visitor_veh_type').val();
    var visitor_veh_num=$('#visitor_veh_num').val();
    // end_date = (!end_date == '') ? end_date : FUTURE_DATE;
    var visitor_img=$('#vis_image').val();
    var id_proof_img=$('#id_image').val();

    var form_data = new FormData(); 
    form_data.append('prk_admin_id', prk_admin_id); 
    form_data.append('prk_area_user_id', prk_area_user_id); 
    form_data.append('prk_user_username', prk_user_username); 
    form_data.append('token', token); 
    form_data.append('visitor_mobile', visitor_mobile);
    form_data.append('visitor_name', visitor_name);
    form_data.append('visitor_gender', visitor_gender);
    form_data.append('visitor_add1', visitor_add1);
    form_data.append('visitor_add2', visitor_add2);
    form_data.append('visitor_state', visitor_state);
    form_data.append('visitor_city', visitor_city);
    form_data.append('visitor_landmark', visitor_landmark);
    form_data.append('visitor_country', visitor_country);
    form_data.append('visitor_pin_cod', visitor_pin_cod);
    form_data.append('id_name', id_name);
    form_data.append('id_number', id_number);
    form_data.append('visitor_gate_pass_num', visitor_gate_pass_num);
    form_data.append('effective_date', effective_date);
    form_data.append('end_date', end_date);
    form_data.append('visitor_vehicle_type', visitor_veh_type);
    form_data.append('visitor_vehicle_number', visitor_veh_num);

    form_data.append('visitor_img', visitor_img);
    form_data.append('id_proof_img', id_proof_img);
    form_data.append('call_type', 'WEB');
    var urlCity = pkrEmpUrl+'visitor_gate_pass_dtl.php';  
    $('#visitor_save').val('Wait ...').prop('disabled', true);
    // 'prk_admin_id','prk_area_user_id','prk_user_username','token','visitor_mobile','visitor_name','visitor_gender','visitor_add1','visitor_add2','visitor_state','visitor_city','visitor_landmark','visitor_country','visitor_pin_cod','id_name','id_number','visitor_gate_pass_num','effective_date','end_date','visitor_vehicle_type','visitor_vehicle_number'
    $.ajax({
      type: "POST",           
      url: urlCity,
      dataType: 'text',  // what to expect back from the PHP script, if anything
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,                         
      type: 'post',
      dataType:'html',
      success  :function(data) { 
        $('#visitor_save').val('Save').prop('disabled', true);
        // alert(data);
        var json = $.parseJSON(data);
        if (json.status){
          $.alert({
            icon: 'fa fa-smile-o',
            theme: 'modern',
            title: 'Success',
            content: "<p style='font-size:0.9em;'>Visitor Gatepass Issue successfully</p>",
            type: 'green',
            buttons: {
              Ok: function () {
                // location.reload(true);
                window.location='visitor-gate-pass';
              }
            }
          });
          // $('#visitor_gate_pass_dtl_id').val(json.visitor_gate_pass_dtl_id);
          // $("#table_data").css("display","");         
          // $('#vis_work_add').attr("disabled", false);
          // $('#visitor_save').attr("disabled", true);
        }else{
          $.alert({
            icon: 'fa fa-frown-o',
            theme: 'modern',
            title: 'Error !',
            content: "<p style='font-size:0.7em;'>Somting went wrong 1</p>",
            type: 'red'
          });
          // alert(data);
        }
      }
    });
  } 
</script> -->
<!-- <script language="JavaScript">
  Webcam.set({
      width: 130,
      height: 100,
      image_format: 'jpeg',
      jpeg_quality: 90
  });
  Webcam.attach( '#my_camera' );
  function take_snapshot() {
    Webcam.snap( function(data_uri) {
      $(".vis_image").val(data_uri);
      document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
      vis_image();
    });
  }
  $(function() {
    $("#visitor_img").change(function() {
      var file = this.files[0];
      var imagefile = file.type;
      var match= ["image/jpeg","image/png","image/jpg"];
      if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
      {
        $('#previewing').attr('src','../images/noimage.jpg');
        return false;
      }else{
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(this.files[0]);
      }
    });
  });
  function imageIsLoaded(e) {
    // alert('ok');
    $(".vis_image").val(e.target.result);
    document.getElementById('results').innerHTML = '<img style="height: 100px;width: 130px;" src="'+e.target.result+'"/>';
    vis_image();
  };
  $('#my-button').click(function(){
    $('#visitor_img').click();
  });
</script>
<script language="JavaScript">
  Webcam.set({
    width: 130,
    height: 100,
    image_format: 'jpeg',
    jpeg_quality: 90
  });
  Webcam.attach( '#my_camera2' );
  function take_snapshot2() {
    Webcam.snap( function(data_uri) {
      $(".id_image").val(data_uri);
      document.getElementById('results2').innerHTML = '<img src="'+data_uri+'"/>';
      id_image();
    });
  }
  $(function() {
    $("#visitor_img2").change(function() {
      var file = this.files[0];
      var imagefile = file.type;
      var match= ["image/jpeg","image/png","image/jpg"];
      if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
      {
        $('#previewing').attr('src','../images/noimage.jpg');
        return false;
      }else{
        var reader = new FileReader();
        reader.onload = imageIsLoadedaa;
        reader.readAsDataURL(this.files[0]);
      }
    });
  });
  function imageIsLoadedaa(e) {
    // alert('okk');
    $(".id_image").val(e.target.result);
    document.getElementById('results2').innerHTML = '<img style="height: 100px;width: 130px;" src="'+e.target.result+'"/>';
    id_image();
  };
  $('#my-button2').click(function(){
    $('#visitor_img2').click();
  });
</script> -->
