<?php 
/*
Description: parking employ vehicle QR code scan and vehicle entry
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
//require_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('qr_code'))){
 	if(isEmpty(array('qr_code'))){

	 	$qr_code = trim($_POST['qr_code']);
        $response= qr_code_division($qr_code);
        echo ($response);
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?>