<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2019
*/ 
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','parking_admin_name','token','adm_mer_dtl_name','adm_mer_dtl_number','adm_mer_dtl_remarks'))){
    if(isEmpty(array('prk_admin_id','parking_admin_name','token','adm_mer_dtl_name','adm_mer_dtl_number'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
        $user_admin_id = isset($_REQUEST['user_admin_id']) ? trim($_REQUEST['user_admin_id']) : '';
        $adm_mer_dtl_name = trim($_POST['adm_mer_dtl_name']);
        $adm_mer_dtl_number = trim($_POST['adm_mer_dtl_number']);
        $adm_mer_dtl_remarks = trim($_POST['adm_mer_dtl_remarks']);
        $inserted_status = isset($_REQUEST['inserted_status']) ? trim($_REQUEST['inserted_status']) : 'A';
        $inserted_by = trim($_POST['parking_admin_name']);
        $token = trim($_POST['token']);
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            
            $response = emergency_add($prk_admin_id,$user_admin_id,$adm_mer_dtl_name,$adm_mer_dtl_number,$adm_mer_dtl_remarks,$inserted_status,$inserted_by);
        }else{

            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>