<!-- ################################################
  
Description: Tower detalis.
Developed by: Rakhal Raj 
Created Date: 12-03-2018

 ####################################################-->
<?php include "all_nav/header.php";
$user_admin_id=$prk_area_user_id=$oth1=$oth2='';
$emergency_list1=emergency_list($prk_admin_id,$user_admin_id,$prk_area_user_id,$oth1,$oth2);
$emergency_list = json_decode($emergency_list1, true);
?>
  <style type="text/css">
    .size{
      font-size: 11px;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .error{
      font-size: 11px;
      color: red;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .success{
      font-size: 11px;
      color: green;
    }
    th,td{
      text-align: center;
    }
  </style>
  <link href="../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
  <div class="am-mainpanel">
    <div class="am-pagetitle" id="nav_1">

      <h5 class="am-title">Emergency Details<?//=$emergency_list1?></h5>
    </div>
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40" >
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <form id="park_tower">
            <!-- <form role="form" id="park_tower" method="post"  enctype="multipart/form-data"> -->
            <div class="row mg-b-25">
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">Emergency Name <span class="tx-danger">*</span></label>
                  <input type="text" class="form-control" placeholder="Emergency Name" name="adm_mer_dtl_name" id="adm_mer_dtl_name">
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">Emergency Number <span class="tx-danger"></span></label>
                  <input type="text" class="form-control" placeholder="Emergency Number" name="adm_mer_dtl_number" id="adm_mer_dtl_number">
                </div>
              </div>
              <div class="col-lg-5">
                <div class="form-layout-footer mg-t-30">
                  <div class="row">
                    <div class="col-lg-6">
                      <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
                    </div>
                    <div class="col-lg-6">
                      <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40">
        <div class="table-wrapper">
          <table id="datatable1" class="table display" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="wd-5p">Sl No.</th>
                <th >Name</th>
                <th >Mobile Number</th>
                <th >Action</th>
              </tr>
            </thead>
              <?php 
              if($emergency_list['status']){$c=0;
                foreach($emergency_list['tower_list'] as $value){$c+=1;?>
                  <tr>
                  <td ><?=$c;?></td>
                  <td ><?=$value['adm_mer_dtl_name'];?></td>
                  <td ><?=$value['adm_mer_dtl_number'];?></td>
                  <td >
                    <?php  if($value['inserted_status']=='A'){?>
                    <button type="button" class="clean-button" style="background-color: snow; border: none;" name="delete" id="<?=$value['adm_mer_dtl_id']?>" data-toggle="tooltip" onclick="tower_delete(this.id)" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
                    <?php }else{?>
                    <button type="button" class="clean-button" style="background-color: snow; border: none;" name="delete" id="" data-toggle="tooltip" data-placement="top" title="Delete" disabled=""><i class="fa fa-trash-o" style="font-size:18px; color:#797676;"></i></button>
                    <?php }?>
                  </td>
                  </tr>
                <?php }}?>
          </table>
        </div>
      </div>
<?php include"all_nav/footer.php"; ?>
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }/*,
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]*/
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
</script>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var prk_admin_id="<?=$prk_admin_id?>"
  var parking_admin_name="<?=$parking_admin_name?>"
  var token="<?=$token?>"
  $('#save').click(function(){
    if($("#park_tower").valid()){
      var adm_mer_dtl_name=($("#adm_mer_dtl_name").val());
      var adm_mer_dtl_number=($("#adm_mer_dtl_number").val());
      var adm_mer_dtl_remarks='';
      var urlPrkAreGateDtl = prkUrl+'emergency_add.php';
      $('#save').val('Wait ...').prop('disabled', true);
      // 'prk_admin_id','parking_admin_name','token','adm_mer_dtl_name','adm_mer_dtl_number','adm_mer_dtl_remarks'
      $.ajax({
        url :urlPrkAreGateDtl,
        type:'POST',
        data :{
          'prk_admin_id':prk_admin_id,
          'parking_admin_name':parking_admin_name,
          'token': token,
          'adm_mer_dtl_name':adm_mer_dtl_name,
          'adm_mer_dtl_number':adm_mer_dtl_number,
          'adm_mer_dtl_remarks':adm_mer_dtl_remarks,
          'inserted_status':'A'
        },
        dataType:'html',
        success  :function(data){
          // alert(data);
          $('#save').val('SAVE').prop('disabled', false);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>Emergency Added Successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                    location.reload(true);
                }
              }
            });
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>Something went wrong</p>",
              type: 'red'
            });
            }
          }
        }
      });
    }
  });
  function tower_delete(adm_mer_dtl_id){
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the Emergency Details permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            if (adm_mer_dtl_id != '') {
              var urlPrkAreGateDtl = prkUrl+'emergency_delete.php';
              // 'prk_admin_id','parking_admin_name','token','adm_mer_dtl_id'
              $.ajax({
                url :urlPrkAreGateDtl,
                type:'POST',
                data :{
                  'prk_admin_id':prk_admin_id,
                  'parking_admin_name':parking_admin_name,
                  'token': token,
                  'adm_mer_dtl_id':adm_mer_dtl_id
                },
                dataType:'html',
                success  :function(data){
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Delete Successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                          location.reload(true);
                        }
                      }
                    });
                  }else{
                     $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.7em;'>Something went wrong 1</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
</script>
<script type="text/javascript">
  $( document ).ready( function () {
    $( "#park_tower" ).validate( {
      rules: {
        adm_mer_dtl_name: "required",
        adm_mer_dtl_number: {
          required: true,
          number: true
        }
      },
      messages: {
        adm_mer_dtl_number: {
          required: 'This field is required.',
          number: 'Enter valid Number.'
        }
      }
    });
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>