<?php
/*
Description: Parking area to day earning.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_today_earning($prk_admin_id){
    $allplayerdata = array();
    global $pdoconn;
    $total_pay=0;
    $sql = "SELECT `payment_dtl_id`,`total_pay` 
        FROM `payment_dtl` 
        WHERE `prk_admin_id`='$prk_admin_id' 
        AND `payment_dtl`.`pay_dtl_del_flag`='".FLAG_N."'
        AND DATE_FORMAT(`payment_rec_date`,'%Y-%m-%d')  BETWEEN '".TIME_TRN."' AND '".TIME_TRN."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val)
    {
         $total_pay=$total_pay+$val['total_pay'];
    }

    $response['status'] = 1;
    $response['message'] = 'data ssucessfull';
    $response['total_pay'] =$total_pay;

    return json_encode($response);
}
?>