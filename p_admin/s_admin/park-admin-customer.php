<?php
  session_start();
  include "all_nav/header.php";
  function dropdown_list(){
    $html=" <option value=''>Select Option</option><option value='Y'>YES</option><option value='N'>No</option>";
    return $html;
  }
  if (isset($_REQUEST["prk_admin_id"])) {
    $prk_admin_id= trim($_REQUEST["prk_admin_id"]);
    if(empty($prk_admin_id)){
     header('Location: prk-list.php');
    }
  }else{

     header('Location: prk-list');
  }
  $kind_alt='';
  $gst_number='';
  $bill_to_add='';
  $shipped_to_add='';
  $payment_mode='';
  $par_month_rate='';
  $tax_type='';
  $bill_date='';
  $payment_due_day='';
  $service_name='';
  $discount='';
  $cus_dtl_show = prk_area_cus_dtl_show($prk_admin_id);
  $cus = json_decode($cus_dtl_show);
  if ($cus->status) {
    // $prk_area_cus_dtl_id=$cus->prk_area_cus_dtl_id;
    // $prk_admin_id=$cus->prk_admin_id;
    $kind_alt=$cus->kind_alt;
    $gst_number=$cus->gst_number;
    $bill_to_add=$cus->bill_to_add;
    $shipped_to_add=$cus->shipped_to_add;
    $payment_mode=$cus->payment_mode;
    $par_month_rate=$cus->par_month_rate;
    $tax_type=$cus->tax_type;
    $bill_date=$cus->bill_date;
    $payment_due_day=$cus->payment_due_day;
    $service_name=$cus->service_name;
    $discount=$cus->discount;
  }
?>
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
<style type="text/css">
  .size{
    font-size: 11px;
  }
  .error{
    font-size: 11px;
    color: red;
  }
</style>
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Customer Details</h5>
  </div>
</div>
<div class="am-mainpanel">
	<div class="am-pagebody">
    <div class="card pd-20 pd-sm-40" >
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <form id="admin_customer">
          <div class="row mg-b-25">
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Kind Alt Name<span class="tx-danger">*</span></label>
                <input type="text" class="form-control" placeholder="Kind Alt Name" name="kind_alt" id="kind_alt">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">GST Number <span class="tx-danger"></span></label>
                <input type="text" class="form-control" placeholder="GST Number" name="gst_number" id="gst_number">
              </div>
            </div>
            <div class="col-lg-4">
              <div class="form-group">
                <label class="form-control-label size">Bill To Address<span class="tx-danger">*</span></label>
                <input type="text" class="form-control" placeholder="Bill To Address" name="bill_to_add" id="bill_to_add">
              </div>
            </div>
            <div class="col-lg-4">
              <div class="form-group">
                <label class="form-control-label size">Shipped To Address<span class="tx-danger">*</span></label>
                <input type="text" class="form-control" placeholder="Shipped To Address" name="shipped_to_add" id="shipped_to_add">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Payment Mode<span class="tx-danger">*</span></label>
                <div class="select">
                  <select name="payment_mode" id="payment_mode" style="opacity: 0.8;font-size:14px">
                    <option value="">Select Type</option>
                    <option value="M">Monthly</option>
                    <option value="Q">Quarterly</option>
                    <option value="Y">Yearly</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Par Month Rate<span class="tx-danger">*</span></label>
                <input type="text" class="form-control" placeholder="Par Month Rate" name="par_month_rate" id="par_month_rate">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Tax Type<span class="tx-danger">*</span></label>
                <div class="select">
                  <select name="tax_type" id="tax_type" style="opacity: 0.8;font-size:14px">
                    <option value="">Select Type</option>
                    <option value="CGST-SGST">CGST-SGST</option>
                    <option value="IGST">IGST</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Bill Date<span class="tx-danger">*</span></label>
                <input type="text" name="bill_date" id="bill_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" autocomplete="off" readonly="">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Payment Due Day<span class="tx-danger">*</span></label>
                <input type="text" name="payment_due_day" id="payment_due_day" placeholder="Due Day" class="form-control">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Discount Percentage<span class="tx-danger">*</span></label>
                <input type="text" name="discount" id="discount" placeholder="Discount" class="form-control">
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Service Name<span class="tx-danger">*</span></label>
                <input type="text" name="service_name" id="service_name" placeholder="Service Name" class="form-control">
              </div>
            </div>
            <div class="col-lg-4">
              <div class="form-layout-footer mg-t-30">
                <div class="row">
                  <div class="col-lg-6">
                    <button type="button" class="btn btn-block btn-primary prk_button" name="save" id="save"><span id="refresh_button"><i class="fa fa-refresh fa-spin"></i></span> &nbsp;SAVE</button>
                  </div>
                  <div class="col-lg-6">
                    <a href="prk-list"><button class="btn btn-block btn-primary prk_button" type="button">BACK</button></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>   
<!-- datatable -->
<?php include"all_nav/footer.php"; ?>
<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
  addEventListener('DOMContentLoaded', function () {
    pickmeup('#bill_date', {
      default_date   : false,
      position       : 'bottom',
      format  : 'd-m-Y',
      hide_on_select : true,
      render : function (date) {
        return {
          disabled : false,
          class_name : 'date-in-past'
        };
      } 
    });
  });
  $( document ).ready( function () {
    $("#kind_alt").val('<?=$kind_alt?>');
    $("#gst_number").val('<?=$gst_number?>');
    $("#bill_to_add").val('<?=$bill_to_add?>');
    $("#shipped_to_add").val('<?=$shipped_to_add?>');
    $("#payment_mode").val('<?=$payment_mode?>');
    $("#par_month_rate").val('<?=$par_month_rate?>');
    $("#tax_type").val('<?=$tax_type?>');
    $("#bill_date").val('<?=$bill_date?>');
    $("#payment_due_day").val('<?=$payment_due_day?>');
    $("#service_name").val('<?=$service_name?>');
    $("#discount").val('<?=$discount?>');
    // ---------- //
    $('#refresh_button').hide();
    $('#save').click(function(){
      if($("#admin_customer").valid()){
        // alert('ok');
        var SUP_EMP_URL = "<?php echo SUP_EMP_URL ;?>";
        var inserted_by = "<?php echo $employee_user_name ;?>";
        var prk_admin_id = "<?=$prk_admin_id?>";
        var kind_alt=($("#kind_alt").val());
        var gst_number=($("#gst_number").val());
        var bill_to_add=($("#bill_to_add").val());
        var shipped_to_add=($("#shipped_to_add").val());
        var payment_mode=($("#payment_mode").val());
        var par_month_rate=($("#par_month_rate").val());
        var tax_type=($("#tax_type").val());
        var bill_date=($("#bill_date").val());
        var payment_due_day=($("#payment_due_day").val());
        var service_name=($("#service_name").val());
        var discount=($("#discount").val());
        var urlCusDtlAdd = SUP_EMP_URL+'prk_area_cus_dtl_add.php';
        $('#refresh_button').show();
        // 'prk_admin_id','kind_alt','gst_number','bill_to_add','shipped_to_add','payment_mode','par_month_rate','tax_type','bill_date','payment_due_day','service_name','discount','inserted_by'
        $.ajax({
          url :urlCusDtlAdd,
          type:'POST',
          data :{
            'prk_admin_id':prk_admin_id,
            'kind_alt':kind_alt,
            'gst_number':gst_number,
            'bill_to_add':bill_to_add,
            'shipped_to_add':shipped_to_add,
            'payment_mode':payment_mode,
            'par_month_rate':par_month_rate,
            'tax_type':tax_type,
            'bill_date':bill_date,
            'payment_due_day':payment_due_day,
            'service_name':service_name,
            'discount': discount,
            'inserted_by': inserted_by
          },
          dataType:'html',
          success  :function(data){
            $('#refresh_button').hide();
            $('#save').prop('disabled', false);
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.9em;'>Successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                    location.reload(true);
                    // window.location.href='flat-details';
                  }
                }
              });
            }else{
              if( typeof json.session !== 'undefined'){
                if (!json.session) {
                  window.location.replace("logout.php");
                }
              }else{
                $.alert({
                  icon: 'fa fa-frown-o',
                  theme: 'modern',
                  title: 'Error !',
                  content: "<p style='font-size:0.9em;'>"+json.message+"</p>",
                  type: 'red'
                });
              }
            }
          }
        });
      }
    });
    $("#admin_customer" ).validate( {
      rules: {
        kind_alt: "required",
        // gst_number: "required",
        bill_to_add: "required",
        shipped_to_add: "required",
        payment_mode: "required",
        par_month_rate: "required",
        tax_type: "required",
        bill_date: "required",
        payment_due_day: "required",
        service_name: "required",
        discount: "required"
      }
    });
  });
</script>