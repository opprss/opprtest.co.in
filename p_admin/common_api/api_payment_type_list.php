<?php
function payment_type_list($prk_admin_id){
    global $pdoconn;
    $result=array();
    $allplayerdata = array();
    // $parking_type ='R';
    $prk_in =prk_inf($prk_admin_id);
    $json = json_decode($prk_in);
    if($json->status=='1'){
        $parking_type = $json->parking_type;
        $office_flag = $json->office_flag;
	    if ($parking_type=='C') {
	    	if ($office_flag=='N') {
	    		$sql="SELECT '' as payment_type_short,'Select Payment' as payment_type UNION SELECT payment_type_short,payment_type FROM `payment_type_list`WHERE `payment_type_short`<>'DGP'";
	    	}else{
	    		$sql="SELECT '' as payment_type_short,'Select Payment' as payment_type UNION SELECT payment_type_short,payment_type FROM `payment_type_list`WHERE `payment_type_short`<>'DGP' AND `payment_type_short`<>'CASH'";
	    	}
	    }else{
	    	$sql="SELECT '' as payment_type_short,'Select Payment' as payment_type UNION SELECT payment_type_short,payment_type FROM `payment_type_list`WHERE `payment_type_short`='TDGP'";
	    }
	    $query  = $pdoconn->prepare($sql);
	    $query->execute();
	    $arr_catagory = $query->fetchAll();
	    foreach($arr_catagory as $val)
	    {
	        $vehicle['payment_type_short'] = $val['payment_type_short'];
	        $vehicle['payment_type'] = $val['payment_type'];
	        array_push($allplayerdata, $vehicle);
	    }
	    $response['status'] = 1;
	    $response['message'] = 'Sucessfully';
	    return json_encode($allplayerdata);
	}
}
?>