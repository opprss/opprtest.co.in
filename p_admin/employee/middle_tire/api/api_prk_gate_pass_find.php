<?php 
/*
Description: parking in vehicle gate pass find api
Developed by: Rakhal Raj Mandal
Created Date: 04-05-2018
Update date : -------
*/ 
function prk_gate_pass_find($prk_admin_id,$veh_number){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT `prk_gate_pass_dtl`.`prk_gate_pass_id`,
    	`prk_gate_pass_dtl`.`user_admin_id`,
    	`prk_gate_pass_dtl`.`prk_gate_pass_num`,
		`prk_gate_pass_dtl`.`veh_type`,
	    `prk_gate_pass_dtl`.`veh_number`,
	    `prk_gate_pass_dtl`.`tower_id`,
	    `prk_gate_pass_dtl`.`flat_id`,
	    `prk_gate_pass_dtl`.`living_status`,
	    `prk_gate_pass_dtl`.`park_lot_no`,
	    `prk_gate_pass_dtl`.`veh_owner_name`,
		`prk_gate_pass_dtl`.`mobile`, 
		`vehicle_type`.`vehicle_type_dec`,
		`vehicle_type`.`vehicle_typ_img`,
	    `tower_details`.`tower_name`,
	    `flat_details`.`flat_name`
		FROM `prk_gate_pass_dtl` JOIN `vehicle_type` 
		LEFT JOIN `tower_details` ON `tower_details`.`tower_id`=`prk_gate_pass_dtl`.`tower_id`
	    AND `tower_details`.`active_flag`='".FLAG_Y."'
	    AND `tower_details`.`del_flag`='".FLAG_N."'
	    LEFT JOIN `flat_details` ON `flat_details`.`flat_id`=`prk_gate_pass_dtl`.`flat_id`
	    AND `flat_details`.`active_flag`='".FLAG_Y."'
	    AND `flat_details`.`del_flag`='".FLAG_N."'
		WHERE `prk_gate_pass_dtl`.`prk_admin_id`='$prk_admin_id' 
		AND `prk_gate_pass_dtl`.`veh_number`='$veh_number' 
		AND `prk_gate_pass_dtl`.`active_flag`='".FLAG_Y."' 
		AND `prk_gate_pass_dtl`.`del_flag`='".FLAG_N."'
		AND `vehicle_type`.`vehicle_sort_nm` = `prk_gate_pass_dtl`.`veh_type`
		AND `vehicle_type`.`active_flag`= '".FLAG_Y."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
	    $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['prk_gate_pass_id'] = $val['prk_gate_pass_id'];
        $response['user_admin_id'] = $val['user_admin_id'];
        $response['prk_gate_pass_num'] = $val['prk_gate_pass_num'];
        $response['veh_type'] = $val['veh_type'];
        $response['veh_number'] = $val['veh_number'];
        $response['tower_id'] = $val['tower_id'];
        $response['tower_name'] = $val['tower_name'];
        $response['flat_id'] = $val['flat_id'];
        $response['flat_name'] = $val['flat_name'];
        $response['living_status'] = $val['living_status'];
        $response['park_lot_no'] = park_lot_no_list_combo($prk_admin_id,$val['tower_id'],$val['flat_id']);
        $response['veh_owner_name'] = $val['veh_owner_name'];
        $response['vehicle_type_dec'] = $val['vehicle_type_dec'];
        $response['vehicle_typ_img'] = $val['vehicle_typ_img'];
        $response['mobile'] = $val['mobile'];
        $response['payment_type'] = DGP;
        $response['payment_type_full'] = DGP_F;
	}else{
		$response['status'] = 0;
	    $response['message'] = 'Gate Pass Not Issue';
	}
    return json_encode($response);
}
?>