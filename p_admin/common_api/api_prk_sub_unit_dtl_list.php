<?php
/*
Description: Parking area all sub unit list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_sub_unit_dtl_list($prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    //$sql = "SELECT * FROM `prk_sub_unit` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".ACTIVE_FLAG_Y."' AND `del_flag`='".DEL_FLAG_N."'";
    $sql = "SELECT * FROM `prk_sub_unit` 
    WHERE `prk_admin_id`='$prk_admin_id' 
    AND `active_flag`='".FLAG_Y."' 
    AND `del_flag`='".FLAG_N."'
    AND  '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(`prk_sub_unit`.`prk_sub_unit_eff_date`, '%d-%m-%Y'), '%Y-%m-%d')    
    AND  DATE_FORMAT(STR_TO_DATE(IFNULL(`prk_sub_unit`.`prk_sub_unit_end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')";
    
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val)
    {
        $response['status'] = 1;
        $response['message'] = 'data ssucessfull';
        $res['prk_sub_unit_id'] = $val['prk_sub_unit_id'];
        $res['prk_sub_unit_name'] = $val['prk_sub_unit_name'];
        $res['prk_sub_unit_short_name'] = $val['prk_sub_unit_short_name'];
        $res['prk_sub_unit_address'] = $val['prk_sub_unit_address'];
        $res['prk_sub_unit_rep_name'] = $val['prk_sub_unit_rep_name'];
        $res['prk_sub_unit_rep_mob_no'] = $val['prk_sub_unit_rep_mob_no'];
        $res['prk_sub_unit_rep_email'] = $val['prk_sub_unit_rep_email'];
        $res['prk_sub_unit_eff_date'] = $val['prk_sub_unit_eff_date'];
        $res['prk_sub_unit_end_date'] = $val['prk_sub_unit_end_date'];
        //$allplayerdata = array_merge($allplayerdata, $response);
        //echo json_encode($response);
        array_push($allplayerdata, $res);
        $response['prk_sub_unit'] = $allplayerdata;
    }
    return $allplayerdata;
}
?>