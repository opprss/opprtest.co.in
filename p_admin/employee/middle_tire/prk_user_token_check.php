<?php 
/*
Description: parkin employ username and passwod check
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_area_user_id','prk_admin_id','token'))){
 	if(isEmpty(array('prk_area_user_id','prk_admin_id','token'))){
	 	$prk_area_user_id = trim($_POST['prk_area_user_id']);
        $prk_admin_id = trim($_POST['prk_admin_id']);
	 	$token = trim($_POST['token']);
        
        $response=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response= json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response= json_encode($response);
}
echo ($response);
?>