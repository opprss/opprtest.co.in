<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','veh_number','veh_name','veh_seat_capa','veh_min_seat','parking_admin_name','token','veh_avl_from','veh_avl_to','veh_status','crud_type','book_dept_id','book_driver_dtl_id'))){
    if(isEmpty(array('prk_admin_id','veh_number','veh_name','veh_seat_capa','veh_min_seat','parking_admin_name','token','veh_avl_from','veh_avl_to','veh_status','crud_type','book_dept_id','book_driver_dtl_id'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
        $veh_number = trim($_POST['veh_number']);
        $veh_name = trim($_POST['veh_name']);
        $veh_seat_capa = trim($_POST['veh_seat_capa']);
        $veh_min_seat = trim($_POST['veh_min_seat']);
        $parking_admin_name = trim($_POST['parking_admin_name']);
        $veh_avl_from = trim($_POST['veh_avl_from']);
        $veh_avl_to = trim($_POST['veh_avl_to']);
        $veh_status = trim($_POST['veh_status']);
        $crud_type = trim($_POST['crud_type']);
        $token = trim($_POST['token']);
        $book_dept_id = trim($_POST['book_dept_id']);
        $book_driver_dtl_id = trim($_POST['book_driver_dtl_id']);
        $resp=prk_token_check($prk_admin_id,$token);
            $json = json_decode($resp);
            if($json->status){
                $response = vehicle_booking_master($prk_admin_id,$veh_number,$veh_name,$veh_seat_capa,$veh_min_seat,$parking_admin_name,$veh_avl_from,$veh_avl_to,$veh_status,$crud_type,$book_dept_id,$book_driver_dtl_id);
            }else{
                $response = $resp;
            }
    }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
 }
echo $response;
?>