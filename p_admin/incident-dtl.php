<!-- ################################################
  
  Description: Parking area can issue gate pass to a particular Visitor.
  Developed by: Soemen Banerjee
  Created Date: 17-03-2018
####################################################-->
<?php 
  include "all_nav/header.php";
  if (isset($_SESSION['prk_admin_id'])) {
    $user_name=$_SESSION['prk_admin_id'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
  }
  $respon1=admin_incident_list($prk_admin_id);
 // echo $respon1;
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: gray;

  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
    <!-- for datatable -->
    <link href="../lib/highlightjs/github.css" rel="stylesheet">
    <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet"> 
    <!-- header position -->
    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Incident List <?php //echo $respon1;?></h5>
      </div><!-- am-pagetitle -->
      	
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card pd-20 pd-sm-40 col-md-12">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="wd-5p">Image</th>
                  <th class="">Name</th>
                  <th class="">Mobile</th>
                  <th class="">Incident Name</th>
                  <th class="">Status</th>
                  <th class="">Submit Date</th>
                  <th class="wd-5p">Action</th>
                </tr>
              </thead>
              <tbody>
              	<?php 
                // echo($respon1);
                $respon = json_decode($respon1, true);
                if($respon['status']){
                  foreach($respon['incident_list'] as $value){
                ?>
                <tr>
               	  <td><img src="<?php echo $value['user_img']; ?>" style="height: 40px; border-radius: 20%;"> </td>
                  <td><?php echo $value['user_name']; ?></td>
                  <td><?php echo $value['user_mobile']; ?></td>
                  <td><?php echo $value['user_incident_name']; ?></td>
                  <td><?php echo $value['status']; ?></td>
	                <td><?php echo $value['incident_in_date']; ?></td>
	                <td>
                    <a href="incident-verify?list_id=<?php echo base64_encode($value['user_incident_id']); ?>" data-toggle="tooltip" data-placement="top" title="Verify"><i class="fa fa-share" style="font-size:30px"></i></a>

                  </td>
                </tr>
                <?php }} ?>
              </tbody>
            </table>
          </div>
          </div>
        </div>
      </div>
      <div class="am-pagebody">
      <!-- footer part -->
      <?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<style>
  .error_size{
    font-size: 11px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
<script>
  $(function(){
    'use strict';

    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });

    $('#datatable2').DataTable({
      bLengthChange: false,
      searching: false,
       "scrollX": true
    });
    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
</script>
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
