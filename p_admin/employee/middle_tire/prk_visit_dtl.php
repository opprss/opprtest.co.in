<?php 
/*
Description: parking area visit the any visiter.
Developed by: Rakhal Raj Mandal
Created Date: 29-04-2018
Update date : 26-05-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
$resp = array();
if(isAvailable(array('visit_name','visit_gender','visit_mobile','visit_purpose','visit_meet_to','visit_address','visit_with_veh','visit_card_id','visit_img','prk_admin_id','prk_area_user_id','prk_user_username','veh_type','veh_number','token','visitor_meet_to_name','visitor_meet_to_address','visitor_token','unique_id','visitor_type','in_verify','out_verify','tower_id','flat_id'))){
    if(isEmpty(array('visit_name','visit_gender','visit_mobile','visit_with_veh','prk_admin_id','prk_area_user_id','prk_user_username','token','visitor_type'))){
        
        $unique_id = trim($_POST['unique_id']);
        $visitor_type = trim($_POST['visitor_type']);
        $in_verify = trim($_POST['in_verify']);
        $out_verify = trim($_POST['out_verify']);
         
        $visit_name = trim($_POST['visit_name']);
        $visit_gender = trim($_POST['visit_gender']);
        $visit_mobile = trim($_POST['visit_mobile']); 

        $visit_purpose = trim($_POST['visit_purpose']);
        $visit_meet_to = trim($_POST['visit_meet_to']);
        $visit_address = trim($_POST['visit_address']);
        $visit_with_veh = trim($_POST['visit_with_veh']);
        $visit_card_id = trim($_POST['visit_card_id']);
        $visit_img = trim($_POST['visit_img']);

        $prk_admin_id = trim($_POST['prk_admin_id']);
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $prk_user_username = trim($_POST['prk_user_username']);
        $veh_type = trim($_POST['veh_type']);
        $veh_number = trim($_POST['veh_number']);
        $visitor_meet_to_name = trim($_POST['visitor_meet_to_name']);
        $visitor_meet_to_address = trim($_POST['visitor_meet_to_address']);
        $visitor_token = trim($_POST['visitor_token']);
        $token = trim($_POST['token']);
        $veh_out_otp=NULL;
        $veh_token=NULL;
        $prk_area_short_name = '';
        $v_nu='';
        $veh_chek='';
        $call_type = isset($_POST['call_type']) ? trim($_POST['call_type']) : 'M'; 
        $no_of_guest = isset($_POST['no_of_guest']) ? trim($_POST['no_of_guest']) : '1'; 
        $gate_pass_num = isset($_POST['vis_prk_id']) ? trim($_POST['vis_prk_id']) : TDGP; 
        $tower_id = isset($_POST['tower_id']) ? trim($_POST['tower_id']) : ''; 
        $flat_id = isset($_POST['flat_id']) ? trim($_POST['flat_id']) : ''; 
        $veh_add_respond='';
        $veh_add_status='Y';
        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $in_check= prk_visit_in_check($prk_admin_id,$visit_mobile);
            $json = json_decode($in_check);
            if($json->status=='1'){
                $user_admin_id = '';
                $payment_type = TDGP;
                $prk_veh_trc_dtl_id = NULL;
                // $gate_pass_num = TDGP;
                if($visit_with_veh == 'Y'){
                    $prk_inf =prk_inf($prk_admin_id);
                    $json = json_decode($prk_inf);
                    if($json->status=='1'){
                        $prk_area_short_name = $json->prk_area_short_name;
                        $veh_out_otp_flag = $json->veh_out_otp_flag;
                        $veh_valid_flag = $json->veh_valid_flag;
                        if($veh_valid_flag==FLAG_Y){
                            $v_nu =v_numberChecker($veh_number);
                            $json = json_decode($v_nu);
                            if($json->status=='1'){
                                $veh_chek=FLAG_Y;
                            }else{
                                $veh_chek=FLAG_N;
                            }
                        }else{
                            $veh_chek=FLAG_Y;
                        }
                        if($veh_chek==FLAG_Y){
                            $prking_whe=prking_wheeler_count($prk_admin_id,$veh_type,$veh_number);
                            $json = json_decode($prking_whe);
                            if($json->status=='1'){
                                if (!empty($visit_mobile)) {
                                    $draft_re = draft_registration($visit_mobile,$veh_type,$veh_number);
                                    $json = json_decode($draft_re);
                                    if($json->status=='1'){ 
                                    $user_admin_id=$json->user_admin_id;
                                    $user_ac_status=$json->user_ac_status;     
                                    $password=$json->password;                                
                                        if($user_ac_status== FLAG_D){
                                            draft_registration_sms($password,$visit_mobile);
                                        }
                                    }
                                }
                                if ($veh_out_otp_flag==FLAG_Y) {
                                    $veh_out_otp = mt_rand(100000,999999);
                                    $otp_mes =", Vehicle Out OTP :".$veh_out_otp;
                                } 
                                $prk_sub_unit_id = NULL;
                                $advance_pay = 0;
                                $veh_trc = prk_veh_trc_dtl($prk_admin_id,$user_admin_id,$veh_type,$veh_number,$prk_user_username,$prk_user_username,$payment_type,$gate_pass_num,$prk_sub_unit_id,$advance_pay,$visit_mobile,$prk_area_short_name,$veh_out_otp,$veh_token);
                                $json = json_decode($veh_trc);
                                $status=$json->status;
                                if($status=='1'){
                                    $prk_veh_trc_dtl_id=$json->prk_veh_trc_dtl_id;
                                    $sql="SELECT `vis_prk_short_name`,
                                        `vis_prk_full_name`,
                                        `vis_prk_gps_location_address`,
                                        `vis_prk_gps_location`,
                                        `vis_prk_longitude`,
                                        `vis_prk_latitude` 
                                        FROM `visitor_parking_dtl` 
                                        WHERE `vis_prk_id`='$gate_pass_num'
                                        AND `prk_admin_id`='$prk_admin_id'
                                        AND `active_flag`='Y'
                                        AND `del_flag`='N'";
                                    $query  = $pdoconn->prepare($sql);
                                    $query->execute();
                                    $count=$query->rowCount();
                                    if($count>0){
                                        $val = $query->fetch();
                                        $vis_prk_full_name = $val['vis_prk_full_name'];
                                        $vis_prk_longitude = $val['vis_prk_longitude'];
                                        $vis_prk_latitude = $val['vis_prk_latitude'];
                                        $sql="SELECT `token`.`device_token_id`,`token`.`device_type`,`token`.`token_value` FROM `token` WHERE `token`.`active_flag`='".FLAG_Y."' AND `token`.`user_admin_id`='$user_admin_id'";
                                        $query  = $pdoconn->prepare($sql);
                                        $query->execute();
                                        $count=$query->rowCount();
                                        if($count>0){
                                            $val = $query->fetch();
                                            $device_token_id =  $val['device_token_id'];
                                            $device_type =  $val['device_type'];
                                            if ($device_type=='ANDR') {
                                                $payload = array();
                                                $payload['vis_prk_longitude'] = $vis_prk_longitude;
                                                $payload['vis_prk_latitude'] = $vis_prk_latitude;
                                                $payload['in_date'] = TIME;
                                                $payload['url'] = 'VIS_PRK_LOC';
                                                $title = 'OPPRSS';
                                                $message = 'PARKING LOCATION OF '.$vis_prk_full_name.' FOR '.$veh_number.'.';
                                                $push_type = 'individual';
                                                $include_image='';
                                                $clickaction='VIS_PRK_LOC';
                                                $oth1='';
                                                $oth2='';
                                                $oth3='';
                                                $paylo = push_send($title,$message,$push_type,$include_image,$device_token_id,$payload,$clickaction,$oth1,$oth2,$oth3);
                                            }
                                        }
                                    }
                                }
                            }else{
                                // echo($prking_whe);
                                // die(); 
                                $veh_add_respond=$prking_whe;
                                $veh_add_status='N';
                            }
                        }else{
                            // echo $v_nu;
                            // die(); 
                            $veh_add_respond = $v_nu;
                            $veh_add_status = 'N';
                        }
                    }
                }
                if ($veh_add_status=='Y') {
                    $response = prk_visit_dtl($visit_name,$visit_gender,$visit_mobile,$visit_purpose,$visit_meet_to,$visit_address,$visit_with_veh,$visit_card_id,$visit_img,$prk_admin_id,$prk_area_user_id,$prk_user_username,$veh_type,$veh_number,$prk_veh_trc_dtl_id,$visitor_meet_to_name,$visitor_meet_to_address,$visitor_token,$unique_id,$visitor_type,$in_verify,$out_verify,$no_of_guest,$call_type,$tower_id,$flat_id);
                    // echo $response;
                }else{

                    $response =$veh_add_respond;
                }
            }else{
                #.....
                $response = $in_check;
            }    
        }else{
            #.....
            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>