<?php
/*
Description: user licence update.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date : 08-04-2018
*/
function user_lic_dtl_update($user_lic_id,$user_admin_id,$user_lic_number,$user_lic_name,$user_lic_veh_class,$user_lic_issue_date,$user_lic_issued_by,$user_lic_valid_till,$mobile,$s_d_w_of,$blood_gr,$date_of_birth,$address,$user_lic_img, $user_lic_img_status){
    global $pdoconn;

    $response = array();
    $sql ="UPDATE `user_lic_dtl` SET `active_flag`='".FLAG_N."', `updated_date`='".TIME."',`updated_by`='$mobile' WHERE `user_admin_id`='$user_admin_id' AND `user_lic_id`='$user_lic_id' AND `active_flag`='".FLAG_Y."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
         $response = user_lic_dtl($user_admin_id,$user_lic_number,$user_lic_name,$user_lic_veh_class,$user_lic_issue_date,$user_lic_issued_by,$user_lic_valid_till,$mobile,$s_d_w_of,$blood_gr,$date_of_birth,$address,$user_lic_img,$user_lic_img_status);
    }else{
        $response['status'] = 0;
        $response['message'] = 'User Licane Updated Not Successful';
        $response= json_encode($response);  
    } 
    return $response;
}
?>