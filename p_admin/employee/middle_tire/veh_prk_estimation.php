<?php 
/*
Description: parking employ out vehicle
Developed by: Rakhal Raj Mandal
Created Date: 06-04-2018
Update date : 25-05-2018
*/ 
require_once 'api/parkAreaReg_api.php';
//require_once 'api/global_api.php';
$response = array();
$re = array();
$resp = array();
if(isAvailable(array('veh_number','veh_type','veh_rate_id','veh_rate_hour','veh_rate','no_of_time_multiple','total_pay','rate_validity','prk_user_username','prk_admin_id'))){
 	if(isEmpty(array('veh_number','veh_type','veh_rate_id','veh_rate_hour','veh_rate','no_of_time_multiple','total_pay','rate_validity','prk_user_username','prk_admin_id'))){
 		
	 	$veh_number = trim($_POST['veh_number']);
	 	$veh_type = trim($_POST['veh_type']);
	 	$veh_rate_id = trim($_POST['veh_rate_id']);
	 	$veh_rate_hour = trim($_POST['veh_rate_hour']);
        $veh_rate = trim($_POST['veh_rate']);
        $no_of_time_multiple = ($_POST['no_of_time_multiple']);
        $total_pay = ($_POST['total_pay']);
        $rate_validity = ($_POST['rate_validity']);
        $prk_user_username = ($_POST['prk_user_username']);
        $prk_admin_id = ($_POST['prk_admin_id']);
        $response = veh_prk_estimation($veh_number,$veh_type,$veh_rate_id,$veh_rate_hour,$veh_rate,$no_of_time_multiple,$total_pay,$rate_validity,$prk_user_username,$prk_admin_id);

 	}else{
        $response['status'] = 0;
    	$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>