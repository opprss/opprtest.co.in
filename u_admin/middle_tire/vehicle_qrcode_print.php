<?php
	$dd=($_REQUEST['list_id']);
	// $list_id=base64_decode($_REQUEST['list_id']);
	// $dd="OTY0NzQ1MTM3OC0yVy1XQjg4NDQ3N3x8V0I4ODQ0Nzd8fDJXfHxUd28gV2hlZWxlcnM=,OTY0NzQ1MTM3OC00Vy1ERDEyMzQ1Nnx8REQxMjM0NTZ8fDRXfHxGb3VyIFdoZWVsZXJz,OTY0NzQ1MTM3OC0yVy1XQjEyREQxMjM0fHxXQjEyREQxMjM0fHwyV3x8VHdvIFdoZWVsZXJz,OTY0NzQ1MTM3OC00Vy1XQjA3UzE1MDd8fFdCMDdTMTUwN3x8NFd8fEZvdXIgV2hlZWxlcnM=";
    $pieces12 = explode(",", $dd);//$pieces[0]
    // echo(count($pieces12)).'</br>';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Generate QR Code</title>
    <style>
        body, html {
            height: 100%;
            width: 100%;
        }
        .bg {
            background-image: url("images/bg.jpg");
            height: 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
        @media print{
			@page {
			   margin-top: 0px;
			   margin-bottom: 0;
			   size: portrait;
			}
			body  {
			   /*padding-top: 85px;
			   padding-bottom: 72px ;
			   margin-left: 150px;*/
			}
		 	#Header, #Footer { display: none !important; }
		}
    </style>
</head>
<body class="bg">
    <table>
    	<?php  
    	$count_list=0;
    	$html_show ='';
    	foreach($pieces12 as $i => $item) {
	    	$list_id=base64_decode($pieces12[$i]);
	    	// $list_id="9647451378-4W-WB07S1507||WB07S1507||4W||Four Wheelers";
		    $pieces = explode("||", $list_id);//$pieces[0]
		    // $veh_qr_code.'||'.$user_veh_number.'||'.$user_veh_type.'||'.$vehicle_type_dec
		    $veh_qr_code=$pieces[0];
		    $user_veh_number=$pieces[1];
		    $user_veh_type=$pieces[2];
		    $vehicle_type_dec=$pieces[3];
		    if ($count_list==0) {
    			$html_show.='<tr style="display: inline-block; page-break-after: auto;">
    			<td>
	    			<div class="panel-heading">
	                    <h1>Vehicle No. '.$user_veh_number.'</h1>
	                </div>
	                <hr>
	                <div id="qrbox" style="text-align: center;">
	                    <img src="./../../library/QRCodeGenerate/generate.php?text='.$veh_qr_code.'" alt="">
	                </div>
	                <hr>
	    		</td>';
		    }else{
		    	$html_show.='<td style="padding-left: 35px;">
		    			<div class="panel-heading">
		                    <h1>Vehicle No. '.$user_veh_number.'</h1>
		                </div>
		                <hr>
		                <div id="qrbox" style="text-align: center;">
		                    <img src="./../../library/QRCodeGenerate/generate.php?text='.$veh_qr_code.'" alt="">
		                </div>
		                <hr>
		    		</td>
		    	</tr>';
		    }
    		$count_list+=1;
    		$count_list=($count_list==2)?0:$count_list;
    	}
    	echo($html_show);
    	?>
    </table>
</body>
</html>
<script  src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="  crossorigin="anonymous"></script>
<script type="text/javascript">
	$(window).on('load', function() {
	 	window.print();
		setTimeout(function(){window.close();}, 1);
	});
</script>
