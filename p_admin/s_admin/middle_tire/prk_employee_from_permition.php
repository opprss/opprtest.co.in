<?php
/*
Description: Parking area login.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
    //oppr_super_employee_login
    require_once 'api/parkAreaReg_api.php';
    $response = array();
    unset($_SESSION["products"]);

    if(isAvailable(array('prk_admin_id','user_name','checkbox','inserted_by'))){
        if(isEmpty(array('prk_admin_id','user_name','checkbox','inserted_by'))){
            $prk_admin_id = trim($_POST['prk_admin_id']);
            $user_name = trim($_POST['user_name']);
            $form_id = $_POST['checkbox'];
            $inserted_by = trim($_POST['inserted_by']);
            $response = prk_employee_from_permition($prk_admin_id,$user_name,$form_id,$inserted_by);
            $_SESSION["products"]=1;
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandator';
            $response = json_encode($response);
        }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        $response = json_encode($response);
    }
    echo $response;
    header('Location: ../user-from-permission');
?>