<?php
/*
Description: Parking area vehicle all rate dtl list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
@include_once '../../connection-pdo.php';
@include_once '../../global_asset/config.php';
@include_once '../../global_asset/config.php';
@include_once '../global_asset/config.php';
@include_once 'global_asset/config.php';
function park_area_rate_list($prk_admin_id){

    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();

         $sql="SELECT `prk_area_rate`.`prk_rate_id`,
        `prk_area_rate`.`prk_vehicle_type`,
        `prk_area_rate`.`prk_rate_type`,
        `prk_area_rate`.`prk_ins_no_of_hr`,
        `prk_area_rate`.`prk_ins_hr_rate`,
        `prk_area_rate`.`prk_no_of_mt_dis`,
        `prk_area_rate`.`prk_rate_eff_date`,
        `prk_area_rate`.`prk_rate_end_date`,
        `prk_area_rate`.`prk_admin_id`,
        `prk_area_rate`.`tot_prk_space`,
        `vehicle_type`.`vehicle_typ_img`,
        `prk_area_rate`.`veh_rate_hour`,
        `prk_area_rate`.`veh_rate`
        FROM `prk_area_rate`,`vehicle_type` 
        WHERE `prk_area_rate`.`prk_admin_id`='$prk_admin_id' 
        AND `prk_area_rate`.`active_flag`='".FLAG_Y."' 
        AND `prk_area_rate`.`del_flag`='".FLAG_N."'
        AND `vehicle_type`.`active_flag`='".FLAG_Y."'
        AND  '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(`prk_area_rate`.`prk_rate_eff_date`, '%d-%m-%Y'), '%Y-%m-%d')    
        AND  DATE_FORMAT(STR_TO_DATE(IFNULL(`prk_area_rate`.`prk_rate_end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')
        AND `vehicle_type`.`vehicle_sort_nm`=`prk_area_rate`.`prk_vehicle_type`
        ORDER BY  `prk_area_rate`.`prk_rate_id` DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val)
    {
        $response['status'] = 1;
        $response['message'] = 'data ssucessfull';
        $park['prk_rate_id'] = $val['prk_rate_id'];
        $park['prk_vehicle_type'] = $val['prk_vehicle_type'];
        $park['prk_rate_type'] = $val['prk_rate_type'];
        $park['prk_ins_no_of_hr'] = $val['prk_ins_no_of_hr'];
        $park['prk_ins_hr_rate'] = $val['prk_ins_hr_rate'];
        $park['prk_no_of_mt_dis'] = $val['prk_no_of_mt_dis'];
        $park['prk_rate_eff_date'] = $val['prk_rate_eff_date'];
        $park['prk_rate_end_date'] = $val['prk_rate_end_date'];
        $park['veh_rate_hour'] = $val['veh_rate_hour'];
        $park['veh_rate'] = $val['veh_rate'];
        $park['tot_prk_space'] = $val['tot_prk_space'];
        $park['vehicle_typ_img'] = $val['vehicle_typ_img'];

        array_push($allplayerdata, $park);
    }
    // return json_encode($allplayerdata);
    return $allplayerdata;
}
?>