<!-- ################################################
  
Description: Parkig admin can register(login ID and password)/delete/change password/activate-deactivate there employee(security gurd) for there gate managment. with this login credentials parking area employee can lgin intu the parking gate managment mobile app.
Developed by: Soemen Banerjee,Rakhal Raj Mandal
Created Date: 17-03-2018

 ####################################################-->
 
<?php 
  include "all_nav/header.php";
  if (isset($_SESSION['prk_admin_id'])) {
    $insert_by=$_SESSION['parking_admin_name'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
  }
  $category_type='P_EMP_CAT';
  $prk_user_emp_cat = common_drop_down_list($category_type);
  $prk_user_emp_category = json_decode($prk_user_emp_cat, true);
?>
<!-- vendor css -->
  <style type="text/css">
    .size{
      font-size: 11px;
    }

    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
  </style> 
  <!-- links for datatable -->
  <link href="../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
    <div class="am-mainpanel"><!-- cloding in footer -->
      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Users</h5>
      </div><!-- am-pagetitle -->
    </div>
    <div class="am-mainpanel">
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40" >
          <!-- <h6 class="card-body-title">Add Employee</h6> -->
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form name="add_user" action="#" method="post">
              <div class="row mg-b-25">
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">CATEGORY<span class="tx-danger">*</span></label>
                      <div class="select">
                        <select name="category" id="category" style="opacity: 0.8;font-size:12px">
                            <option value="">SELECT CATEGORY</option>
                            <?php
                              foreach ($prk_user_emp_category['drop_down'] as $val){ ?>  
                                <option value="<?php echo $val['all_drop_down_id']?>"><?php echo $val['full_name']?></option>
                              <?php          
                              }
                            ?> 
                        </select>
                      </div>
                    <span style=" position: absolute;" id="error_category" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">EMPLOYEE NUMBER <span class="tx-danger">*</span></label>
                    <input class="form-control error_msg" type="text" name="employee_number" placeholder="Enter number" id="employee_number" onkeydown="upperCase(this)">
                      <span style="position: absolute;" id="error_number" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label class="form-control-label size">EMPLOYEE NAME <span class="tx-danger">*</span></label>
                    <input class="form-control error_msg" type="text" name="employee_name"  placeholder="Enter name" id="employee_name" onkeydown="upperCase(this)">
                    <span style="position: absolute;" id="error_employee_name" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label class="form-control-label size"> USER NAME <span class="tx-danger"></span></label>
                    <input class="form-control readonly" type="text" name="user_name"  placeholder="User name" id="user_name" readonly="" style="background-color: white" onkeydown="upperCase(this)">
                      <span style=" position: absolute;" id="error_user_name" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size"> USER MOBILE <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="user_mobile"  placeholder="User Mobile" id="user_mobile" style="background-color: white" maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                      <span style=" position: absolute;" id="error_user_mobile" class="error_size"></span>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label class="form-control-label size">Gender<span class="tx-danger">*</span></label>
                      <label class="rdiobox">
                        <input type="radio" name="visitor_gender" id="vis_gender_m" value="M" type="radio" checked>
                        <span>Male </span>
                      </label>

                      <label class="rdiobox">
                        <input type="radio" name="visitor_gender" id="vis_gender_f" value="F" type="radio">
                        <span>Female </span>
                      </label>
                      <span style="position: absolute;" id="error_vis_gender" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">PASSWORD <span class="tx-danger">*</span></label>
                    <input class="form-control" type="password" name="password" placeholder="Password" id="password">
                    <span style="position: absolute;" id="error_password" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">CONFIRM PASSWORD<span class="tx-danger">*</span></label>
                    <input class="form-control" type="password" name="repassword"  placeholder="Repassword" id="repassword">
                    <span style=" position: absolute;" id="error_repassword" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label class="form-control-label size">ADDRESS<span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="address"  placeholder="Address" id="address">
                    <span style=" position: absolute;" id="error_address" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label class="form-control-label size">LANDMARK<span class="tx-danger"></span></label>
                    <input class="form-control" type="text" name="landmark"  placeholder="Landmark" id="landmark">
                    <span style=" position: absolute;" id="error_landmark" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label class="form-control-label size">STATE<span class="tx-danger">*</span></label>
                      <div class="select">
                        <select name="state" id="state" style="opacity: 0.8;font-size:14px">
                            <option value="">SELECT STATE</option>
                        </select>
                      </div>
                    <span style=" position: absolute;" id="error_state" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">CITY<span class="tx-danger">*</span></label>
                      <div class="select">
                        <select name="city" id="city" style="opacity: 0.8;font-size:14px">
                            <option value="">SELECT CITY</option>
                        </select>
                      </div>
                    <span style=" position: absolute;" id="error_city" class="error_size"></span>
                  </div>
                </div>

                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">PIN CODE<span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="pincode"  placeholder="Pin Code" id="pincode" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" maxlength="6">
                    <span style=" position: absolute;" id="error_pincode" class="error_size"></span>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="form-control-label user-lebel">Upload Image<span class="tx-danger"></span></label>
                    <input type="file" name="user_img" accept="image/*" id="user_img" style="font-size: 13px;">
                    <input type="hidden" name="user_image" class="user_image" id="user_image">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <div id="results">captured image will appear here...</div>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-layout-footer mg-t-30">
                    <div class="row">
                      <div class="col-lg-6">
                        <input type="button" value="ADD" class="btn btn-block btn-primary prk_button" name="submit" id="submit">
                        <div style="position: absolute;" id="success_submit" class="success"></div>
                      </div>
                      <div class="col-lg-6">
                        <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                      </div>
                    </div>
                  </div>
                </div>
            </form>
          </div>
        </div>
        </div>
      </div>
    <!-- datatable -->
    <!-- <div class="am-mainpanel"> -->
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40">
          <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="wd-5p">Serial No.</th>
                  <th class="wd-20p">Employee Name</th>
                  <th class="wd-20p">Username</th>
                  <th class="wd-15p">Employee Number</th>
                  <th class="wd-20p">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $respon=prk_sub_user_list($prk_admin_id);
                $x=1;
                foreach($respon as $value){ 
                  $prk_user_username=$value['prk_user_username'];
                  $prk_area_user_id=$value['prk_area_user_id'];?>
                <tr class="<?php if($value['active_flag'] == ACTIVE_FLAG_N) echo 'deactivate-bg'; ?>">
                  <td class="<?php if($value['active_flag'] == ACTIVE_FLAG_N) echo 'deactivate-bg'; ?>"><?php echo $x; ?></td>
                  <td ><?php echo $value['prk_user_name']; ?></td>
                  <td><?php echo $value['prk_user_username']; ?></td>
                  <td><?php echo $value['prk_user_emp_no']; ?></td>
                  <!-- <td><?php //echo $prk_area_user_id; ?></td> -->
                  <td>
                    <button type="button" name="permission" id="permission" data-target="#user_parmission<?=$prk_area_user_id?>" value="<?php echo $prk_admin_id; ?>" data-toggle="modal" data-placement="top" title="Get Permission" style="background: transparent; border: 0px;"><i class="fa fa-database" style="font-size:15px"></i></button>

                    <button type="button" name="forget_password" id="out<%=count++%>" value="<?php echo $value['prk_area_user_id']; ?>" data-toggle="tooltip" data-placement="top" title="Change Password" style="background: transparent; border: 0px;"><i class="fa fa-key" style="font-size:18px"></i></button>

                    <?php if ($value['active_flag'] == ACTIVE_FLAG_N) {?>
                      <a class="activate pd-l-10" href='<?php echo PRK_URL?>prk_emp_active.php?prk_admin_id=<?php echo $prk_admin_id; ?>&prk_admin_name=<?php echo $prk_user_username; ?>&prk_area_user_id=<?php echo $value['prk_area_user_id']; ?>&active=<?php echo ACTIVE_FLAG_Y ?>' data-toggle="tooltip" data-placement="top" title="Activate"><i class="fa fa-lock" style="font-size:18px;"></i></a>&nbsp;
                    <?php }else{?>
                      <a class="deactivate pd-l-10" href='<?php echo PRK_URL?>prk_emp_active.php?prk_admin_id=<?php echo $prk_admin_id; ?>&prk_admin_name=<?php echo $prk_user_username; ?>&prk_area_user_id=<?php echo $value['prk_area_user_id']; ?>&active=<?php echo ACTIVE_FLAG_N ?>' data-toggle="tooltip" data-placement="top" title="Deactivate"><i class="fa fa-unlock" style="font-size:18px;"></i></a>
                    <?php }?>&nbsp;
                    <button type="button" class="clean-button" name="delete" id="<?php echo($prk_area_user_id); ?>" data-toggle="tooltip" data-placement="top" title="ID Card" onclick="emp_id_print(this.id)"><i class="fa fa-id-card" style="font-size:18px;"></i></button>

                    <a href="<?php echo 'vender-add-user-modify?list_id='.base64_encode($value['prk_area_user_id']);?>" class="pd-l-10" data-toggle="tooltip" data-placement="top" title="Modify">
                      <i class="fa fa-pencil" style="font-size:18px;"></i>
                    </a>

                    <button type="button" class="clean-button" name="delete" id="delete<%=count++%>" value="<?php echo $prk_user_username; ?>" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
                  </td>
                </tr>
                <div id="user_parmission<?=$prk_area_user_id?>" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title" style="font-size: 18px; text-align: center;">Get Permission : <?=$value['prk_user_name']?></h4>
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                          <form method="post" id="form" action="p_admin_middle_tire/p_employee_permi_form.php">
                            <?php
                              $form_dtl=p_employee_permi_form_list($prk_admin_id);
                              $form_dtl = json_decode($form_dtl, true);
                              if ($form_dtl['status']) {
                              foreach($form_dtl['from_list'] as $value){
                                $select_list=p_employee_form_permi_select_list($value['form_id'],$prk_admin_id,$prk_area_user_id);
                                $select_li = json_decode($select_list);
                                $sta=$select_li->status;
                            ?>
                            <div class="row">
                              <div class="col-sm-1">
                                <input type="hidden" name="user_name" id="user_name" value="<?=$prk_user_username?>">
                                <input type="hidden" name="inserted_by" id="inserted_by" value="<?php echo $insert_by?>">
                                <input type="hidden" name="prk_admin_id" id="prk_admin_id" value="<?=$prk_admin_id?>">
                                <input type="hidden" name="token" id="token" value="<?=$token?>">
                                <input type="hidden" name="prk_area_user_id" id="prk_area_user_id" value="<?=$prk_area_user_id?>">
                              </div>
                              <div class="col-sm-8">
                                <?=$value['form_name']?>
                              </div>
                              <div class="col-sm-1">
                                 <input id="checkbox" type="checkbox" name="checkbox[]" value="<?php echo $value['form_id'];?>" <?=($sta=='1')?'checked':'';?>>
                              </div>
                              <div class="col-sm-2"></div>
                            </div>
                            <?php }} ?>
                            <div class="row">
                              <div class="col-sm-7"></div>
                              <div class="col-sm-5">
                                <input type="submit" value="Submit" name="submit_permission" id="" class="btn btn-block btn-primary prk_button">
                              </div> 
                            </div>
                          </form>
                        </div>
                    </div>
                  </div>
                </div>
                <?php $x++; } ?>
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->
  <?php include"all_nav/footer.php"; ?>
  <style>
    .error_size{
      font-size: 11px;
      color: red;
    }
    .success{
      font-size: 11px;
      color: green;
    }
  </style>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";

  $('button[id^="delete"]').on('click', function() {
    var prk_admin_id = "<?php echo $prk_admin_id;?>";
    var prk_user_username= this.value;
    //alert(prk_rate_id);
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the item permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            if (prk_admin_id != '') {
              //alert(prk_rate_id);
              var urlDtl = prkUrl+'prk_area_user_delete.php';
              //alert(urlDtl);
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :
                {
                  // 'prk_rate_id':prk_rate_id,
                  'prk_admin_id':prk_admin_id,
                  'prk_user_username':prk_user_username,
                  'token': '<?php echo $token;?>'
                },
                dataType:'html',
                success  :function(data)
                {
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    location.reload(true);
                  }else if(!json.session){
                    window.location.replace("logout.php");
                  }else{
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  });
  // end
  $('a.deactivate').confirm({
    title: 'Do you want to deactivate?',
    content: "<p style='font-size:0.9em;'>It will deactivate your employee.</p>",
    type: 'blue',
    icon: 'fa fa-unlock',
    theme: 'modern',
    buttons: {
      tryAgain: {
          text: 'Deactivate',
          btnClass: 'btn-blue',
          action: function(){
            location.href = this.$target.attr('href');
          }
      },
      close: function () {
      }
    }
  });
  $('a.activate').confirm({
    title: 'Do you want to activate?',
    content: "<p style='font-size:0.9em;'>It will activate your employee.</p>",
    type: 'green',
    icon: 'fa fa-lock',
    theme: 'modern',
    buttons: {
      tryAgain: {
          text: 'activate',
          btnClass: 'btn-green',
          action: function(){
            location.href = this.$target.attr('href');
          }
      },
      close: function () {
      }
    }
  });
  // forget password
  $('button[id^="out"]').on('click', function() {
    var prk_admin_id = "<?php echo $prk_admin_id;?>";
    var prk_admin_name = "<?php echo $insert_by;?>";
    var prk_area_user_id = this.value;
    
      $.confirm({
        title: 'Change Password',
        useBootstrap: false,
        boxWidth: '30%',
        theme: 'modern',
        content: '' +
    '<form action="" class="formName">' +
    '<div class="form-group">' +
    '<input type="password" placeholder="Password" class="password form-control" required /><br>' +
    '<input type="password" placeholder="Confirm Password" class="confirm_password form-control" required />' +
    '</div>' +
    '</form>',
        buttons: {
            formSubmit: {
                text: 'Change',
                btnClass: 'btn-green',
                action: function () {
                    var password = this.$content.find('.password').val();
                    var repassword = this.$content.find('.confirm_password').val();

                    if(password == '' ){
                      $.alert({
                        title:'Error',
                        theme: 'modern',
                        content: '<p style="font-size:0.9em;">Password is empty.</p>',
                        type: 'red'
                      });
                        return false;
                    }else if(password.length < 5){
                      $.alert({
                        title:'Error',
                        theme: 'modern',
                        content: '<p style="font-size:0.9em;">Password must be minimum 5 characters long.</p>',
                        type: 'red'
                      });
                      return false;
                    }else if(repassword == ''){
                      $.alert({
                        title:'Error',
                        theme: 'modern',
                        content: '<p style="font-size:0.9em;">Confirm password is empty.</p>',
                        type: 'red'
                      });
                      return false;
                    }else if(repassword.length < 5){
                      $.alert({
                        title:'Error',
                        theme: 'modern',
                        content: '<p style="font-size:0.9em;">Password must be minimum 5 characters long.</p>',
                        type: 'red'
                      });
                      return false;
                    }else if(password != repassword){
                      $.alert({
                        title:'Error',
                        theme: 'modern',
                        content: '<p style="font-size:0.9em;">Password & confirm password must be same</p>',
                        type: 'red'
                      });
                      return false;
                    }
                    //$.alert('Your name is ' + password+'---'+repassword);
                    //'prk_admin_id','prk_area_user_id','prk_admin_name','password','repassword'
                    var urlEmpForgetPass = prkUrl+'prk_emp_forget_password.php';

                    $.ajax({
                      url :urlEmpForgetPass,
                      type:'POST',
                      data :
                      {
                        'prk_admin_id':prk_admin_id,
                        'prk_area_user_id':prk_area_user_id,
                        'prk_admin_name':prk_admin_name,
                        'password':password,
                        'repassword':repassword,
                        'token': '<?php echo $token;?>'
                      },
                      dataType:'html',
                      success  :function(data)
                      {
                        var json = $.parseJSON(data);
                        if (json.status){
                            $.alert({
                            icon: 'fa fa-smile-o',
                            theme: 'modern',
                            title: 'Success',
                            content: "Password updated successfully",
                            type: 'green',
                            buttons: {
                              Ok: function () {
                                  //location.reload(true);
                              }
                            }
                          });
                        }
                        // else{
                        //   $.alert({
                        //     icon: 'fa fa-frown-o',
                        //     theme: 'modern',
                        //     title: 'Error !',
                        //     content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                        //     type: 'red'
                        //   });
                        // }
                        else{
                          if( typeof json.session !== 'undefined'){
                              if (!json.session) { 
                                window.location.replace("logout.php");                    
                              }                  
                            }else{                    
                              $.alert({                    
                                icon: 'fa fa-frown-o',                    
                                theme: 'modern',                    
                                title: 'Error !',                    
                                content: "Somting went wrong",                    
                                type: 'red'                  
                              });                  
                            }                
                          }
                      }
                    });
                }
            },
            cancel: function () {
                //close
            },
        },
        onContentReady: function () {
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
  });
  $(document).ready(function(){
    var urlState = prkUrl+'state.php';
    $.ajax ({
      type: 'POST',
      url: urlState,
      data: "",
      success : function(data) {
        var obj=JSON.parse(data);
        var areaOption = "<option value=''>SELECT STATE</option>";
        $.each(obj['state'], function(val, key) {
          areaOption += '<option value="' + key['sta_id'] + '">' + key['state_name'] + '</option>'
        });
        $("#state").html(areaOption);
      }
    });
    $('#state').on("change", function() {
      var id=($("#state").val());
      var urlCity = prkUrl+'city.php';
      $.ajax ({
        type: 'POST',
        url: urlCity,
        data: {id:id},
        success : function(data) {
          var obj=JSON.parse(data);
          var areaOption = "<option value=''>SELECT CITY</option>";
          $.each(obj['city'], function(val, key) {
            areaOption += '<option value="' + key['city_id'] + '">' + key['city_name'] + '</option>'
          });
          $("#city").html(areaOption);
        }
      });
    });
    $('#submit').click(function(){
      var category=$('#category').val();
      var employee_number=$('#employee_number').val();
      var employee_name=$('#employee_name').val();
      var user_name=$('#user_name').val();
      var password=$('#password').val();
      var repassword=$('#repassword').val();
      var user_mobile=$('#user_mobile').val();
      var address=$('#address').val();
      var pincode=$('#pincode').val();
      var state=$('#state').val();
      var city=$('#city').val();
      var landmark=$('#landmark').val();
      var gender =  $('input[name=visitor_gender]:checked').val();
      var user_img =  $('#user_image').val();
       
      var prk_admin_id=' <?php echo $prk_admin_id; ?>';
      var insert_by=' <?php echo $insert_by; ?>';
      var token=' <?php echo $token; ?>';

      var form_data = new FormData();
      form_data.append('prk_admin_id', prk_admin_id);
      form_data.append('prk_user_emp_category', category);
      form_data.append('prk_user_emp_no', employee_number);
      form_data.append('prk_user_username', user_name);
      form_data.append('prk_user_name', employee_name);
      form_data.append('prk_user_password', password);
      form_data.append('prk_user_repassword', repassword);
      form_data.append('prk_user_mobile', user_mobile);
      form_data.append('prk_user_gender', gender);
      form_data.append('prk_user_address', address);
      form_data.append('prk_user_landmark', landmark);
      form_data.append('prk_user_state', state);
      form_data.append('prk_user_city', city);
      form_data.append('prk_user_pincode', pincode);
      form_data.append('inserted_by', insert_by);
      form_data.append('prk_user_img', user_img);
      form_data.append('token', token);

      // alert('mano');
      if(employee_name_check(employee_name)==true & employee_number_check(employee_number)==true & password_check(password)==true & repassword_check(password,repassword)==true & user_mobile_check(user_mobile)==true & address_check(address)==true & pincode_check(pincode)==true & state_check(state)==true & city_check(city)==true & category_check(category)==true ){
        $('#submit').val('Wait ...').prop('disabled', true);
        // siblit function
        // alert('hi');
        var urlPrkAreUser = prkUrl+'prk_area_user.php';
        // if(repassword==password){
        $.ajax({
          url :urlPrkAreUser,
          type:'POST',
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,                         
          dataType:'html',
          success  :function(data)
          {
            $('#submit').val('ADD').prop('disabled', false);
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.9em;'>Employee added successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                      location.reload(true);
                  }
                }
              });
            }else{
              if( typeof json.session !== 'undefined'){
                  if (!json.session) { 
                    window.location.replace("logout.php");                    
                  }                  
                }else{                    
                  $.alert({                    
                    icon: 'fa fa-frown-o',                    
                    theme: 'modern',                    
                    title: 'Error !',                    
                    content: "Somting went wrong",                    
                    type: 'red'                  
                  });                  
                }                
              }
          }
        });
      }
    });
    $('#reset').click(function(){

      $('.error_size').text('');
    });
    $('#category').on("change", function() {
      var category=$('#category').val();
      category_check(category);
    });
    $("#employee_number").blur(function () {
      var employee_number=$('#employee_number').val();
      employee_number_check(employee_number);
    });
    $("#employee_name").blur(function () {
      var employee_name=$('#employee_name').val();
      employee_name_check(employee_name);
      user_name_genarate(employee_name);
    });
    $("#user_mobile").blur(function () {
      var user_mobile=$('#user_mobile').val();
      user_mobile_check(user_mobile);
    });
    $("#repassword").blur(function () {
      var password=$('#password').val();
      var repassword=$('#repassword').val();
      repassword_check(password,repassword);
    });
    $("#password").blur(function () {
      var password=$('#password').val();
      password_check(password);
    });
    $("#address").blur(function () {
      var address=$('#address').val();
      address_check(address);
    });
    $("#pincode").blur(function () {
      var pincode=$('#pincode').val();
      pincode_check(pincode);
    });
    $('#state').on("change", function() {
      var state=$('#state').val();
      state_check(state);
    });
    $('#city').on("change", function() {
      var city=$('#city').val();
      city_check(city);
    });
    $("#user_img").blur(function () {
      var user_img=$('#user_image').val();
      user_img(user_img);
    });
    // only aplhabets
    $("#employee_name").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)){
            event.preventDefault();
        }
    });
  });
  function category_check(category){
    // alert("ok");
    if(category==''){
      $('#error_category').text('Select Category');
      return false;
    }else{
      $('#error_category').text('');
      return true;
    }
  }
  function employee_name_check(employee_name){
    if(employee_name.trim()==''){
      $('#error_employee_name').text('Employee Name Required');
      return false;
    }else{
      $('#error_employee_name').text('');
      return true;
    }
  }
  function user_name_genarate(employee_name){
    if(employee_name!=''){
      // parking_area_name = employee_name.replace(/^([^\s]*)(.*)/, "$1");
      // var random_number = Math.floor(1000 + Math.random() * 9000);
      // var g_username = (parking_area_name+'_'+random_number);
      // $("#user_name").val(g_username);
      // $('#user_mobile').focus();
      $.ajax({
        url :'p_admin_middle_tire/employee_username_generate.php',
        type:'POST',
        data :{
          'employee_name':employee_name
        },
        dataType:'html',
        success  :function(data){
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $("#user_name").val(json.username);
            $('#user_mobile').focus();
          }else{
            alert("somthing went wrong");
            $('#employee_name').focus();
          }
        }
      });
    }else{
      $("#user_name").val('');
    }
  }
  function employee_number_check(employee_number){
    if(employee_number==''){
      $('#error_number').text('Employee Number Required');
      return false;
    }else{
      $('#error_number').text('');
      return true;
    }
  }
  function password_check(password){
    password_le= password.length;
    if(password==''){
      $('#error_password').text('Password Required');
      return false;
    }else if (password_le<5){
      $('#error_password').text('Minimum 5 characters');
      return false;
    }else{
      $('#error_password').text('');
      return true;
    }
  }
  function repassword_check(password,repassword){
    password_le= repassword.length;
    if(repassword==''){
      $('#error_repassword').text('Password Required');
      return false;
    }else if (password_le<5){
      $('#error_repassword').text('Minimum 5 characters');
      return false;
    }else if (password!=repassword){
      $('#error_repassword').text('Password mismatch');
      return false;
    }else{
      $('#error_repassword').text('');
      return true;
    }
  }
  function user_mobile_check(val){
    var n = val.length;
    // alert(n);
    if(val==''){
      $('#error_user_mobile').text('Mobile Number Required');
      return false;
    }else{
      if(n>=10){
        $('#error_user_mobile').text('');
        return true;
      }else{
        $('#error_user_mobile').text('Mobile Number Must be 10 Digits');
        return false;
      }
    }
  }
  function address_check(val){
    if(val==''){
      $('#error_address').text('Address Required');
      return false;
    }else{
      $('#error_address').text('');
      return true;
    }
  }
  function state_check(val){
    if(val==''){
      $('#error_state').text('Select State');
      return false;
    }else{
      $('#error_state').text('');
      return true;
    }
  }
  function city_check(val){
    if(val==''){
      $('#error_city').text('Select City');
      return false;
    }else{
      $('#error_city').text('');
      return true;
    }
  }
  function pincode_check(val){
    if(val==''){
      $('#error_pincode').text('Pincode Required');
      return false;
    }else{
      $('#error_pincode').text('');
      return true;
    }
  }
  function user_img(val){
    if(val==''){
      $('#error_user_image').text('This field is Required');
      return false;
    }else{
      $('#error_user_image').text('');
      return true;
    }
  }
</script>
<!-- scripts for datatable -->
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';

    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });

    $('#datatable2').DataTable({
      bLengthChange: false,
      searching: false,
      responsive: true
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
</script>
<script>
  $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();   
  });
  function upperCase(a){
        setTimeout(function(){
            a.value = a.value.toUpperCase();
        }, 1);
  }
  function emp_id_print(prk_area_user_id){
    var prkUrl = "<?php echo PRK_URL; ?>";
    // alert(prkUrl);
    urlPrint = prkUrl+"employee_id_qr_code_print.php?prk_area_user_id="+prk_area_user_id;
    window.open(urlPrint,'-blank');
    location.reload();
  }
</script>
<script type="text/javascript">
  $(function() {
    $("#user_img").change(function() {
      //$("#message").empty(); // To remove the previous error message
      var file = this.files[0];
      var imagefile = file.type;
      var match= ["image/jpeg","image/png","image/jpg"];
      if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
      {
      return false;
      }
      else
      {
      var reader = new FileReader();
      reader.onload = imageIsLoaded;
      reader.readAsDataURL(this.files[0]);
      }
    });
  });
  function imageIsLoaded(e) {
    $(".user_image").val(e.target.result);
    document.getElementById('results').innerHTML = '<img style="height: 80px;width: 100px;" src="'+e.target.result+'"/>';
  };
</script>
