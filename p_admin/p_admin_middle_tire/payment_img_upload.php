<?php
require_once 'api/parkAreaReg_api.php';

ini_set("memory_limit", "99M");
ini_set('post_max_size', '20M');
ini_set('max_execution_time', 600);
define('IMAGE_SMALL_DIR', './../uploades/small/');
define('IMAGE_SMALL_SIZE', 50);
/*Parking admin name for create directory*/
$parking_admin_name = $_POST['parking_admin_name'];
define('IMAGE_MEDIUM_DIR', './../uploades/'.$parking_admin_name.'/payment/');

$image_info = getimagesize($_FILES["image_upload_file"]["tmp_name"]);
$image_width = $image_info[0];
$image_height = $image_info[1];

define('IMAGE_MEDIUM_SIZE', $image_height);
$prk_admin_id = $_POST['user_name'];
$token = ($_POST['token']);


if(isset($_FILES['image_upload_file'])){
	$output['status']=FALSE;
	set_time_limit(0);
	$allowedImageType = array("image/gif",   "image/jpeg",   "image/pjpeg",   "image/png",   "image/x-png"  );
	
	if ($_FILES['image_upload_file']["error"] > 0) {
		$output['error']= "Error in File";
	}
	elseif (!in_array($_FILES['image_upload_file']["type"], $allowedImageType)) {
		$output['error']= "You can only upload JPG, PNG and GIF file";
	}
	elseif (round($_FILES['image_upload_file']["size"] / 1024) > 4096) {
		$output['error']= "You can upload file size up to 4 MB";
	} else {

		/*create directory with 777 permission if not exist - start*/
		createDir(IMAGE_SMALL_DIR);
		createDir(IMAGE_MEDIUM_DIR);
		/*create directory with 777 permission if not exist - end*/
		$path[0] = $_FILES['image_upload_file']['tmp_name'];
		$file = pathinfo($_FILES['image_upload_file']['name']);
		$fileType = $file["extension"];
		$desiredExt='jpg';
		$fileNameNew = rand(333, 999) . time() . ".$desiredExt";
		$path[1] = IMAGE_MEDIUM_DIR . $fileNameNew;
		$path[2] = IMAGE_SMALL_DIR . $fileNameNew;
		$path[3] = 'uploades/'.$parking_admin_name.'/payment/'.$fileNameNew;
		
		/*start*/
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            if (createThumb($path[0], $path[1], $fileType, IMAGE_MEDIUM_SIZE, IMAGE_MEDIUM_SIZE)) {
				if (paymentImageUpload($path[3],$prk_admin_id)) {
					$output['status']=TRUE;
					$output['image_medium']= $path[1];
					$output['image_small']= $path[2];
					$output['meaasge'] = 'upload sucessfully';
					$output['image_height'] = $image_height;
				}
			}
        }else{
            $output['session']=FALSE;
			$output['meaasge'] = 'upload sucessfully';
        }
		/*end*/
	}
	echo json_encode($output);
}
?>	