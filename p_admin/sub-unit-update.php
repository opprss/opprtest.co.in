<!-- ################################################
  
Description: Parking area can modify subunit 
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
  include "all_nav/header.php";
  if (isset($_SESSION['prk_admin_id'])) {
    $insert_by=$_SESSION['parking_admin_name'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
    $user_name = $sessionDetails['parking_admin_name'];
  }
  if (isset($_REQUEST['list_id'])) {
    $prk_sub_unit_id = base64_decode($_REQUEST['list_id']);

    $response = array();
    $response = prk_sub_unit_show($prk_sub_unit_id);
    $response = json_decode($response , true);

     if (!empty($response)) {

      $prk_sub_unit_id = $response['prk_sub_unit_id']; 
      $prk_sub_unit_name = $response['prk_sub_unit_name'];
      $prk_sub_unit_short_name = $response['prk_sub_unit_short_name'];
      $prk_sub_unit_address = $response['prk_sub_unit_address'];
      $prk_sub_unit_rep_name = $response['prk_sub_unit_rep_name'];
      $prk_sub_unit_rep_mob_no = $response['prk_sub_unit_rep_mob_no'];
      $prk_sub_unit_rep_email = $response['prk_sub_unit_rep_email'];
      $prk_sub_unit_eff_date = $response['prk_sub_unit_eff_date'];
      $end_date = $response['prk_sub_unit_end_date'];
    }
  }

?>
<!-- vendor css -->
<style type="text/css">
 .size{
    font-size: 11px;
  }
</style>
<link href="../lib/highlightjs/github.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../lib/select2/css/select2.min.css" rel="stylesheet">




 <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Subunit Update</h5>
        <!--<form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
    </div>
    <div class="am-mainpanel">
      <div class="am-pagebody">

        <div class="card single pd-20 pd-sm-40">
          <!-- <h6 class="card-body-title">Subunit</h6> -->
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">

            <div class="row mg-b-25">
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">NAME <span class="tx-danger">*</span></label>
                  <input class="form-control error_msg" type="text" name="prk_sub_unit_name" placeholder="Subunit Name" id="prk_sub_unit_name" value="<?php echo $prk_sub_unit_name; ?>" onkeydown="upperCase(this)">
                  <!-- autofocus="autofocus"  for auto set cursor position after reload page-->
                    <span style="position: absolute;" id="error_name" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">SHORT NAME <span class="tx-danger">*</span></label>
                  <input class="form-control error_msg" type="text" name="prk_sub_unit_short_name"  placeholder="Subunit Short Name" id="prk_sub_unit_short_name" value="<?php echo $prk_sub_unit_short_name; ?>" onkeydown="upperCase(this)">
                    <span style="position: absolute;" id="error_short_name" class="error_size"></span>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">ADDRESS <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="prk_sub_unit_address"  placeholder="Address" id="prk_sub_unit_address" style="background-color: white" value="<?php echo $prk_sub_unit_address; ?>" onkeydown="upperCase(this)">
                    <span style=" position: absolute;" id="error_address" class="error_size"></span>
                </div>
              </div><!-- col-4-->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">REPRESENTATIVE NAME <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="prk_sub_unit_rep_name"  placeholder="Reprentitive Name" id="prk_sub_unit_rep_name" style="background-color: white" value="<?php echo $prk_sub_unit_rep_name; ?>" onkeydown="upperCase(this)">
                    <span style=" position: absolute;" id="error_rep_name" class="error_size"></span>
                </div>
              </div>
            <!-- </div>
            
            <div class="row mg-b-25"> -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">REPRESENTAITIVE MOBILE NUMBER <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="prk_sub_unit_rep_no"  placeholder="Reprentitive MOBILE Number" id="prk_sub_unit_rep_no" style="background-color: white" maxlength="10" value="<?php echo $prk_sub_unit_rep_mob_no; ?>" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                    <span style=" position: absolute;" id="error_rep_number" class="error_size"></span>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">REPRESENTATIVE EMAIL <span class="tx-danger">*</span></label>
                  <input class="form-control" type="email" name="prk_sub_unit_rep_email" placeholder="REPRESENTATIVE EMAIL" id="prk_sub_unit_rep_email" style="background-color: white" value="<?php echo $prk_sub_unit_rep_email; ?>">
                    <span style=" position: absolute;" id="error_rep_email" class="error_size"></span>
                </div>
              </div>

              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">EFFECIVE DATE <span class="tx-danger">*</span></label>
                  <input class="form-control prevent_readonly" type="text" name="prk_sub_unit_eff_date" placeholder="Effective date" id="prk_sub_unit_eff_date" style="background-color: white" value="<?php echo $prk_sub_unit_eff_date; ?>" readonly="readonly">
                    <span style=" position: absolute;" id="error_eff_date" class="error_size"></span>
                </div>
              </div>

              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">END DATE</label>
                  <input class="form-control prevent_readonly" type="text" name="end_date" placeholder="End date" id="end_date" style="background-color: white" value="<?php if($end_date == FUTURE_DATE_WEB) echo ''; else echo $end_date; ?>" readonly="readonly">
                    <span style=" position: absolute;" id="error_eff_date" class="error_size"></span>
                </div>
              </div>

              <div class="col-lg-4">
                <div class="form-layout-footer mg-t-30">
                  <div class="row">
                    <div class="col-lg-6">
                      <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" id="submit">
                    </div>
                    <div class="col-lg-6">
                        <button class="btn btn-block prk_button_skip" onclick="document.location.href='sub-unit';">BACK</button>
                      </div>
                  </div>
                </div><!-- form-layout-footer -->
              </div>
            </div>
          </div>
        </div>
      </div>

<!-- datatable -->
<?php include"all_nav/footer.php"; ?>

<style>
.error_size{
  font-size: 11px;
  color: red;

}
.success{
  font-size: 11px;
  color: green;
}
</style>



<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE_WEB ?>";

  /*delete popup*/
  $('a.delete').confirm({
    content: "<p style='font-size:0.9em;'>It will delete the item permanently</p>",
  });
  $('a.delete').confirm({
      buttons: {
         btnClass: 'btn-red',
          hey: function(){
              location.href = this.$target.attr('href');
          }
      }
  });

  $(document).ready(function(){

    $('#submit').click(function(){
      //alert("click");
      var prk_sub_unit_name =$('#prk_sub_unit_name').val();
      var prk_sub_unit_short_name=$('#prk_sub_unit_short_name').val();
      var prk_sub_unit_address=$('#prk_sub_unit_address').val();
      var prk_sub_unit_rep_name=$('#prk_sub_unit_rep_name').val();
      var prk_sub_unit_rep_no=$('#prk_sub_unit_rep_no').val();
      var prk_sub_unit_rep_email=$('#prk_sub_unit_rep_email').val();
      var prk_sub_unit_eff_date = $('#prk_sub_unit_eff_date').val();
      var end_date = $('#end_date').val();
      end_date = (!end_date == '') ? end_date : FUTURE_DATE;
      var prk_admin_id = "<?php echo $prk_admin_id;?>";
      var user_name = "<?php echo $user_name;?>";
      var prk_sub_unit_id = "<?php echo $prk_sub_unit_id;?>";

      //alert(prk_sub_unit_name);

      if(prk_sub_unit_name!='' && prk_sub_unit_short_name!='' && prk_sub_unit_address!='' && prk_sub_unit_rep_name!='' && prk_sub_unit_rep_no!='' && prk_sub_unit_rep_email!='' && prk_sub_unit_eff_date!=''){
        // alert(repassword);
        // siblit function
        var urlPrkSubUnitUpdate = prkUrl+'prk_sub_unit_update.php';

        //'prk_sub_unit_name','prk_sub_unit_short_name','prk_sub_unit_address','prk_sub_unit_rep_name','prk_sub_unit_rep_mob_no','prk_sub_unit_rep_email','prk_sub_unit_eff_date','user_name','prk_admin_id'
          $('#submit').val('Wait ...').prop('disabled', true);
          $.ajax({
              url :urlPrkSubUnitUpdate,
              type:'POST',
              data :
              {
                'prk_sub_unit_name':prk_sub_unit_name,
                'prk_sub_unit_short_name':prk_sub_unit_short_name,
                'prk_sub_unit_address':prk_sub_unit_address,
                'prk_sub_unit_rep_name':prk_sub_unit_rep_name,
                'prk_sub_unit_rep_mob_no':prk_sub_unit_rep_no,
                'prk_sub_unit_eff_date':prk_sub_unit_eff_date,
                'prk_sub_unit_rep_email':prk_sub_unit_rep_email,
                'prk_admin_id':prk_admin_id,
                'user_name':user_name,
                'prk_sub_unit_id':prk_sub_unit_id,
                'prk_sub_unit_end_date':end_date,
                'token': '<?php echo $token;?>'                             
              },
              dataType:'html',
              success  :function(data)
              {
              $('#submit').val('SAVE').prop('disabled', false);
                //alert(data);
                var json = $.parseJSON(data);
                //alert(json.status);
                  if (json.status){
                    //window.location='park_gate.php';
                      $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success !',
                      content: "<p style='font-size:0.9em;'>Subunit updated successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                            window.location='sub-unit';
                        }
                      }
                    });
                  }else{
                    if( typeof json.session !== 'undefined'){
                      if (!json.session) {
                        window.location.replace("logout.php");
                      }
                    }else{
                      $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                    }
                }
              }
            });     
        
        }else{
        if (prk_sub_unit_name == '') {
              $('#error_name').text('Required name');
              //return false;
          }if (prk_sub_unit_address == '') {
            $('#error_address').text('Required address');
            //return false;
          }if (prk_sub_unit_short_name == '') {
            $('#error_short_name').text('Required short name');
            //return false;
          }if (prk_sub_unit_rep_name == '') {
            $('#error_rep_name').text('Required representative name');
            //return false;
          }if (prk_sub_unit_rep_no == '') {
            $('#error_rep_number').text('Required representative number');
            //return false;
          }if (prk_sub_unit_rep_email == '') {
            $('#error_rep_email').text('Required representative email');
            //return false;
          }if (prk_sub_unit_eff_date == '') {
            $('#error_eff_date').text('Required effective date');
            //return false;
          }
          return false;
        }

    });

    $("#prk_sub_unit_name").blur(function () {
      var prk_sub_unit_name=$('#prk_sub_unit_name').val();
      if(prk_sub_unit_name==''){
        // alert("Hurray!!!");
        $('#error_name').text('Required name');

      }else{
        $('#error_name').text('');
      }
    });

    $("#prk_sub_unit_short_name").blur(function () {
      var prk_sub_unit_short_name=$('#prk_sub_unit_short_name').val();
      if(prk_sub_unit_short_name==''){
        // alert("Hurray!!!");
        $('#error_short_name').text('Required short name');

      }else{
        $('#error_short_name').text('');
      }
    });

    $("#prk_sub_unit_address").blur(function () {
      var prk_sub_unit_address=$('#prk_sub_unit_address').val();
      if(prk_sub_unit_address==''){
        // alert("Hurray!!!");
        $('#error_address').text('Required address');

      }else{
        $('#error_address').text('');
      }
    });

    $("#prk_sub_unit_rep_name").blur(function () {
      var prk_sub_unit_rep_name=$('#prk_sub_unit_rep_name').val();
      if(prk_sub_unit_rep_name==''){
        // alert("Hurray!!!");
        $('#error_rep_name').text('Required representative name');

      }else{
        $('#error_rep_name').text('');
      }
    });

    $("#prk_sub_unit_rep_no").blur(function () {
      var prk_sub_unit_rep_no=$('#prk_sub_unit_rep_no').val();
      if(prk_sub_unit_rep_no==''){
        // alert("Hurray!!!");
        $('#error_rep_number').text('Required representative number');

      }else{
        $('#error_rep_number').text('');
      }
    });

    /*$("#prk_sub_unit_rep_email").blur(function () {
      var prk_sub_unit_rep_email=$('#prk_sub_unit_rep_email').val();
      if(prk_sub_unit_rep_email==''){
        // alert("Hurray!!!");
        $('#error_rep_email').text('Required representative email');
        //$('#prk_sub_unit_rep_email').focus();

      }else{
        $('#error_rep_email').hide();
      }
    });*/
    $("#prk_sub_unit_rep_email").blur(function () {
      var prk_sub_unit_rep_email=$('#prk_sub_unit_rep_email').val();
      if(prk_sub_unit_rep_email==''){
        // alert("Hurray!!!");
        $('#error_rep_email').text('Required representative email');
        return false;
      }else if(!isEmail(prk_sub_unit_rep_email)){
        $('#error_rep_email').text('Email is not valid');
        //$('#prk_sub_unit_rep_email').focus();
        return false;

      }else{
        $('#error_rep_email').text('');
      }
    });

    $("#prk_sub_unit_eff_date").blur(function () {
      var prk_sub_unit_eff_date=$('#prk_sub_unit_eff_date').val();
      if(prk_sub_unit_eff_date==''){
        // alert("Hurray!!!");
        $('#error_eff_date').text('Required Effective Date');
        $('#prk_sub_unit_eff_date').focus();

      }else{
        $('#error_eff_date').text('');
      }
    });
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#prk_sub_unit_eff_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
            if (date < now_end) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            return {};
        } 
    });
  });

    $('#prk_sub_unit_eff_date').focus(function(){
      $('#end_date').val('');
    });
// end

  })



</script>
 <script src="../lib/highlightjs/highlight.pack.js"></script>
    <script src="../lib/datatables/jquery.dataTables.js"></script>
    <script src="../lib/datatables-responsive/dataTables.responsive.js"></script>

    <script>
      $(function(){
        'use strict';

        $('#datatable1').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ Items/Page',
          }
        });

        $('#datatable2').DataTable({
          bLengthChange: false,
          searching: false,
          responsive: true
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

      });
    </script>

    <!-- for tool tipe -->
    <script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });

    addEventListener('DOMContentLoaded', function () {

    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    //alert(now);
    pickmeup('#prk_sub_unit_eff_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
            if (date < now) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            return {};
        } 
    });
  });
function upperCase(a){
      setTimeout(function(){
          a.value = a.value.toUpperCase();
      }, 1);
  }

/*alphabets only*/
  $("#prk_sub_unit_rep_name").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
        event.preventDefault();
    }
  });

</script>
