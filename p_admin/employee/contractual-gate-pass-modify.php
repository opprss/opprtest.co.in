<!-- ################################################
Description: User Address verify
Developed by: Rakhal Raj Mandal
Created Date: 27-08-2019
####################################################-->
<?php 
  include "all_nav/header.php";
  $contractual_gate_pass_dtl_id = isset($_REQUEST['list_id']) ? trim($_REQUEST['list_id']) : '';
  $category_type='BLOOD_TYPE';
  $blood_group_type1 = common_drop_down_list($category_type);
  $blood_group_type = json_decode($blood_group_type1, true);
?>
<style type="text/css">
  .size{
    font-size: 11px;
    text-transform: uppercase;
  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
  .error_size,.error{
    font-size: 11px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
  select{
    opacity: 0.8;
    font-size: 12px;
  }
</style> 
<script src="../../js/webcam.min.js"></script>
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Contractual Gate Pass Modify</h5>
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40 col-md-12">
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <div class="row">
          <div class="col-md-12">
            <form role="form" id="addVis" method="post"  enctype="multipart/form-data">
              <div class="form-layout">
                <div class="row mg-b-25">
                  <div class="col-md-7">
                    <div class="row">
                      <div class="col-lg-5">
                        <div class="form-group">
                            <label class="form-control-label size">Name<span class="tx-danger">*</span></label>
                            <input type="text" class="form-control" placeholder="Visitor Name" name="visitor_name" id="visitor_name" onkeyup="vehNoUppercase();" autocomplete="off">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label size">Mobile Number<span class="tx-danger">*</span></label>
                          <input type="text" class="form-control" placeholder="Mobile Number" name="visitor_mobile" id="visitor_mobile" maxlength="10" autocomplete="off">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="form-control-label size">Gender<span class="tx-danger">*</span></label>
                            <label class="rdiobox">
                              <input type="radio" name="visitor_gender" id="vis_gender_m" value="M" type="radio" checked>
                              <span>Male </span>
                            </label>

                            <label class="rdiobox">
                              <input type="radio" name="visitor_gender" id="vis_gender_f" value="F" type="radio">
                              <span>Female </span>
                            </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <div id="my_camera"></div><br/>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <button type="button" class="btn btn-block prk_button" onClick="take_snapshot()"><p style="margin-bottom: 0px;font-size: 11px;margin-left: -3px;">Capture<br>Image</p></button>
                          <button type="button" class="btn btn-block prk_button" id="my-button"><p style="margin-bottom: 0px;font-size: 11px;margin-left: -2px;">Upload<br>Image</p></button>
                          <input type="file" name="visitor_img" accept="image/*" id="visitor_img" style="visibility: hidden;">
                          <input type="hidden" name="vis_image" class="vis_image" id="vis_image">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <div id="results">captured Visitor image will appear here...</div>
                          <!-- <span  id="error_vis_image" class="error_size"></span> -->
                          <label style="position: absolute;" class="error_size" id="error_vis_image"></label>
                        </div>
                      </div><div class="col-md-1"></div>
                    </div>
                  </div>
                </div>
                <div class="row" style="margin-top: -60px;">
                  <div class="col-md-2">
                    <div class="form-group">
                      <label class="form-control-label size">Emergency No<span class="tx-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="Emergency No" name="emergency_no" id="emergency_no" autocomplete="off" maxlength="10">
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label class="form-control-label size">Date of Birth<span class="tx-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="Date of Birth" name="visitor_date_of_birth" id="visitor_date_of_birth" autocomplete="off">
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label class="form-control-label size">State Blood Group<span class="tx-danger">*</span></label>
                      <div class="select">
                        <select name="visitor_blood_group" id="visitor_blood_group" style="opacity: 0.8;font-size:12px">
                          <option value="">Select Type</option>
                          <?php foreach ($blood_group_type['drop_down'] as $key => $value) {
                            # code...
                            echo '<option value="'.$value['sort_name'].'">'.$value['full_name'].'</option>';

                          } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label class="form-control-label size">Name of Contractor<span class="tx-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="Contractor" name="visitor_contractor" id="visitor_contractor">
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label class="form-control-label size">Name of Department<span class="tx-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="Department" name="visitor_department" id="visitor_department">
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label class="form-control-label size">P.F Number<span class="tx-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="P.F Number" name="visitor_pf_number" id="visitor_pf_number">
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label class="form-control-label size">E.S.I Number<span class="tx-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="E.S.I Number" name="visitor_esi_number" id="visitor_esi_number">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="form-control-label size">Address<span class="tx-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="Address " name="visitor_add" id="visitor_add">
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label class="form-control-label size">Landmark<span class="tx-danger"></span></label>
                      <input type="text" class="form-control" placeholder="Landmark" name="visitor_landmark" id="visitor_landmark">
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label class="form-control-label size">Country Name<span class="tx-danger">*</span></label>
                      <input type="text" class="form-control readonly" placeholder="Gate Pass Number" name="visitor_country" id="visitor_country" value="INDIA" style="background-color: transparent;" readonly="">
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label class="form-control-label size">State Name<span class="tx-danger">*</span></label>
                      <div class="select">
                        <select name="visitor_state" id="visitor_state" style="opacity: 0.8;font-size:12px">
                          <option value="">SELECT STATE</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label class="form-control-label size">City Name<span class="tx-danger">*</span></label>
                      <div class="select">
                        <select name="visitor_city" id="visitor_city" style="opacity: 0.8;font-size:12px">
                          <option value="">SELECT CITY</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label class="form-control-label size">Pin Code<span class="tx-danger">*</span></label>
                      <input type="text" class="form-control " placeholder="Pin Code" name="visitor_pin_cod" id="visitor_pin_cod" style="background-color: transparent;" maxlength="6">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <label class="form-control-label size">EFFECTIVE DATE <span class="tx-danger">*</span></label>
                      <input type="text" name="effective_date" id="effective_date" placeholder="Effective Date" class="form-control prevent_readonly" readonly="readonly">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <label class="form-control-label size">END DATE</label>
                      <input type="text" name="end_date" id="end_date" placeholder="End Date" class="form-control prevent_readonly" readonly="readonly">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-layout-footer mg-t-30">
                    <input type="button" value="Save" class="btn btn-block btn-primary prk_button" name="visitor_save" id="visitor_save">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-layout-footer mg-t-30">
                      <button  type="button" class="btn btn-block prk_button" id="back">BACK</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php include"all_nav/footer.php"; ?>
<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#visitor_date_of_birth', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
        if (date > now) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        return {};
      } 
    });
  });
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#effective_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
        if (date < now) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        return {};
      } 
    });
  });
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#effective_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
            if (date < now_end) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            return {};
      } 
    });
  });
  $('#effective_date').focus(function(){
    $('#end_date').val('');
  });
</script>
<script type="text/javascript">
  var FUTURE_DATE = "<?php echo FUTURE_DATE_WEB ?>"
  var contractual_gate_pass_dtl_id='<?php echo $contractual_gate_pass_dtl_id; ?>';
  $( document ).ready( function (){
    $('#effective_date').attr("disabled", true);
    $('#visitor_mobile').attr("disabled", true);
    stateList();
    detailsShow();
    jQuery('input').keyup(function() {
      this.value = this.value.toLocaleUpperCase();
    });
    $("#back").on('click',function(){
      window.location.href='contractual-gate-pass-list';
    });
    /*var urlState = pkrEmpUrl+'state.php';
    $.ajax ({
      type: 'POST',
      url: urlState,
      data: "",
      success : function(data) {
        var obj=JSON.parse(data);
        var areaOption = "<option value=''>SELECT STATE</option>";
        $.each(obj['state'], function(val, key) {
          areaOption += '<option value="' + key['sta_id'] + '">' + key['state_name'] + '</option>'
        });
        $("#visitor_state").html(areaOption);
      }
    });*/
    $('#visitor_state').on("change", function() {
      var id=($("#visitor_state").val());
      var urlCity = pkrEmpUrl+'city.php';
      $.ajax ({
        type: 'POST',
        url: urlCity,
        data: {id:id},
        success : function(data) {
          var obj=JSON.parse(data);
          var areaOption = "<option value=''>SELECT CITY</option>";
          $.each(obj['city'], function(val, key) {
            areaOption += '<option value="' + key['city_id'] + '">' + key['city_name'] + '</option>'
          });
          $("#visitor_city").html(areaOption);
        }
      });
    });
    $('#visitor_save').click(function(){
      if($("#addVis").valid()){
      
        form_contact();
      }
    });
  });
  function form_contact(){
    var visitor_name=$('#visitor_name').val();
    var visitor_mobile=$('#visitor_mobile').val();
    var visitor_gender =  $('input[name=visitor_gender]:checked').val();
    var emergency_no=$('#emergency_no').val();
    var visitor_date_of_birth=$('#visitor_date_of_birth').val();
    var visitor_blood_group=$('#visitor_blood_group').val();
    var visitor_contractor=$('#visitor_contractor').val();
    var visitor_department=$('#visitor_department').val();
    var visitor_pf_number=$('#visitor_pf_number').val();
    var visitor_esi_number=$('#visitor_esi_number').val();
    var visitor_add=$('#visitor_add').val();
    var visitor_landmark=$('#visitor_landmark').val();
    var visitor_country=$('#visitor_country').val();
    var visitor_state=$('#visitor_state').val();
    var visitor_city=$('#visitor_city').val();
    var visitor_pin_cod=$('#visitor_pin_cod').val();
    var effective_date=$('#effective_date').val();
    var end_date=$('#end_date').val();
    var visitor_img=$('#vis_image').val();
    var form_data = new FormData(); 
    form_data.append('prk_admin_id', prk_admin_id); 
    form_data.append('visitor_name', visitor_name);
    form_data.append('visitor_mobile', visitor_mobile);
    form_data.append('visitor_gender', visitor_gender);
    form_data.append('emergency_no', emergency_no);
    form_data.append('visitor_date_of_birth', visitor_date_of_birth);
    form_data.append('visitor_blood_group', visitor_blood_group);
    form_data.append('visitor_contractor', visitor_contractor);
    form_data.append('visitor_department', visitor_department);
    form_data.append('visitor_pf_number', visitor_pf_number);
    form_data.append('visitor_esi_number', visitor_esi_number);
    form_data.append('visitor_add', visitor_add);
    form_data.append('visitor_landmark', visitor_landmark);
    form_data.append('visitor_country', visitor_country);
    form_data.append('visitor_state', visitor_state);
    form_data.append('visitor_city', visitor_city);
    form_data.append('visitor_pin_cod', visitor_pin_cod);
    form_data.append('effective_date', effective_date);
    form_data.append('end_date', end_date);
    form_data.append('inserted_by', prk_user_username);
    form_data.append('visitor_img', visitor_img);
    form_data.append('contractual_gate_pass_dtl_id', contractual_gate_pass_dtl_id);
    // alert('ok');
    var urlCity = pkrEmpUrl+'contractual_gate_pass_modify.php';  
    // alert(urlCity);
    $('#visitor_save').val('Wait ...').prop('disabled', true);
    $.ajax({
      type: "POST",           
      url: urlCity,
      dataType: 'text',
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,                         
      type: 'post',
      dataType:'html',
      success  :function(data) { 
        $('#visitor_save').val('Save').prop('disabled', true);
        // alert(data);
        var json = $.parseJSON(data);
        if (json.status){
          $.alert({
            icon: 'fa fa-smile-o',
            theme: 'modern',
            title: 'Success',
            content: "<p style='font-size:0.9em;'>Modify Successfully</p>",
            type: 'green',
            buttons: {
              Ok: function () {
                location.reload(true);
              }
            }
          });
        }else{
          $.alert({
            icon: 'fa fa-frown-o',
            theme: 'modern',
            title: 'Error !',
            content: "<p style='font-size:0.7em;'>"+json.message+"</p>",
            type: 'red'
          });
        }
      }
    });
  }
  function detailsShow(){
    // 'prk_admin_id','prk_area_user_id','contractual_gate_pass_dtl_id','token'
    var gate_pass_list = pkrEmpUrl+'contractual_gate_pass_show.php';
    $.ajax({
      url :gate_pass_list,
      type:'POST',
      data :
      {
        'prk_admin_id':prk_admin_id,
        'prk_area_user_id':prk_area_user_id,
        'token':token,
        'contractual_gate_pass_dtl_id':contractual_gate_pass_dtl_id
      },
      dataType:'html',
      success  :function(data){
        // alert(data);
        var json = $.parseJSON(data);
        if (json.status){
          // alert(json.visitor_name);
          $('#visitor_name').val(json.visitor_name);
          $('#visitor_mobile').val(json.visitor_mobile);
          $('input:radio[name="visitor_gender"][value="'+json.visitor_gender+'"]').attr('checked',true);
          $('#emergency_no').val(json.emergency_no);
          $('#visitor_date_of_birth').val(json.visitor_date_of_birth);
          $('#visitor_blood_group').val(json.visitor_blood_group);
          $('#visitor_contractor').val(json.visitor_contractor);
          $('#visitor_department').val(json.visitor_department);
          $('#visitor_pf_number').val(json.visitor_pf_number);
          $('#visitor_esi_number').val(json.visitor_esi_number);
          $('#visitor_add').val(json.visitor_add);
          $('#visitor_landmark').val(json.visitor_landmark);
          $('#visitor_country').val(json.visitor_country);
          $('#visitor_state').val(json.visitor_state);
          $('#visitor_pin_cod').val(json.visitor_pin_cod);
          $('#effective_date').val(json.effective_date);
          var end_date_o='';
          if (json.end_date==FUTURE_DATE) {
            var end_date_o='';
          }else{

            var end_date_o=json.end_date;
          }
          $('#end_date').val(end_date_o);
          document.getElementById('results').innerHTML = '<img style="height: 100px;width: 130px;" src="'+json.visitor_img+'"/>';
          demoLin ='<option value='+json.visitor_city+'>'+json.city_name+'</option>'; 
          $("#visitor_city").html(demoLin);

        }
      }
    });
  }
  function stateList() {
    var urlState = pkrEmpUrl+'state.php';
    $.ajax ({
      type: 'POST',
      url: urlState,
      data: "",
      success : function(data) {
        var obj=JSON.parse(data);
        var areaOption = "<option value=''>SELECT STATE</option>";
        $.each(obj['state'], function(val, key) {
          areaOption += '<option value="' + key['sta_id'] + '">' + key['state_name'] + '</option>'
        });
        $("#visitor_state").html(areaOption);
      }
    });
  }
</script>
<script>
  $( document ).ready( function () {  
    var prkUrl = "<?php echo PRK_URL; ?>";
    var urlUserMobileCheck = prkUrl+'contractual _mobile_check.php';  
    $( "#addVis" ).validate( {
      rules: {
        visitor_name: "required",
        visitor_mobile: {
          required: true,
          number: true,
          minlength: 10,
          remote: {
            url: urlUserMobileCheck,
            type: "POST",
            data: { prk_admin_id: prk_admin_id},
            cache: false,
            dataType: "json",
            dataFilter: function(data) {
              var json = $.parseJSON(data);                      
              if (json.status) {
                  return true;
              } else {
                  return false;
              }
            }
          }
        },
        emergency_no: {
          required: true,
          number: true,
          minlength: 10
        },
        visitor_date_of_birth: "required",
        visitor_blood_group : "required",
        visitor_contractor: "required",
        visitor_department: "required",
        visitor_pf_number: "required",
        visitor_esi_number: "required",
        visitor_state: "required",
        visitor_city: "required",
        visitor_pin_cod:{
          required:true,
          maxlength:6,
          minlength:6
        },
        effective_date: "required",
        visitor_add: "required"
      },
      messages: {
        visitor_mobile: {
          required: "This field is required.",
          number: "Enter valid mobile Numberr.",
          minlength: "Enter valid mobile Number.",
          remote: "Already add This Member."
        },
        emergency_no: {
          required: "This field is required.",
          number: "Enter valid mobile Numberr.",
          minlength: "Enter valid mobile Number."
        },
        visitor_pin_cod:{
          required:"This field is required.",
          minlength:"Minimum length is 6",
          maxlength:"Maximum length is 6"
        }
      }
    });
    $("#visitor_name").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 60) && (inputValue != 0 & inputValue !=32)){
        // if(!(inputValue >= 97) && (inputValue != 46)){
        event.preventDefault();
      }
    });
    $("#visitor_mobile,#visitor_pin_cod").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0)){
        event.preventDefault();
      }
    });
  });
  function vis_image() {
    var name = $("#vis_image").val();
    if(name== ''){
      $("#error_vis_image").text('This field is required.');
      return false;
    }else{
      $("#error_vis_image").text('');
      return true;
    }
  }
</script>
<script language="JavaScript">
  Webcam.set({
    width: 130,
    height: 100,
    image_format: 'jpeg',
    jpeg_quality: 90
  });
  Webcam.attach( '#my_camera' );
  function take_snapshot() {
    Webcam.snap( function(data_uri) {
      $(".vis_image").val(data_uri);
      document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
      vis_image();
    });
  }
  $(function() {
    $("#visitor_img").change(function() {
      var file = this.files[0];
      var imagefile = file.type;
      var match= ["image/jpeg","image/png","image/jpg"];
      if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
      {
        $('#previewing').attr('src','../images/noimage.jpg');
        return false;
      }else{
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(this.files[0]);
      }
    });
  });
  function imageIsLoaded(e) {
    // alert('ok');
    $(".vis_image").val(e.target.result);
    document.getElementById('results').innerHTML = '<img style="height: 100px;width: 130px;" src="'+e.target.result+'"/>';
    vis_image();
  };
  $('#my-button').click(function(){
    $('#visitor_img').click();
  });
</script>
