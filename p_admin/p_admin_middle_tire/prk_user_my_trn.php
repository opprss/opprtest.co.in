<?php 
/*
Description: Parking area parkinng employ today earning.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
$response = array();
$resp = array();
if(isAvailable(array('prk_admin_id','payment_rec_emp_name','prk_area_user_id','start_date','end_date'))){
    if(isEmpty(array('prk_admin_id','payment_rec_emp_name'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $payment_rec_emp_name = trim($_POST['payment_rec_emp_name']);
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $start_date = trim($_POST['start_date']);
        $end_date = trim($_POST['end_date']);
        
            $response= prk_user_my_trn($prk_admin_id,$payment_rec_emp_name,$start_date,$end_date);
            echo ($response);
    }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response); 
}
?> 