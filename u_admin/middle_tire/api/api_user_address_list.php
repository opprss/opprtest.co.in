<?php 
/*
Description: subumit lisst.
Developed by: Rakhal Raj Mandal
Created Date: 14-06-2018
Update date :---------
*/
function user_address_list($user_admin_id){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
	$sql = "SELECT IF(`prk_area_dtl`.`prk_area_name` IS NULL or `prk_area_dtl`.`prk_area_name` = '', 'OTHER AREA', `prk_area_dtl`.`prk_area_name`) as `prk_area_name`,
		`user_address`.`user_add_id`,
		`user_address`.`user_address`,
		`user_address`.`landmark`,
		`user_address`.`user_add_area`,
		`user_address`.`user_add_city`,
		`user_address`.`user_add_state`,
		`state`.`state_name`,
		`city`.`city_name`,
		`user_address`.`user_add_country`,
		`user_address`.`user_add_pin`,
		`user_address`.`user_admin_id`,
		`user_address`.`prk_admin_id`,
		`user_address`.`tower_name` as 'tower_id',
		`user_address`.`falt_name` as 'flat_id', 
		`user_address`.`default_flag`,
		`user_address`.`user_add_verify_flag`,
        `tower_details`.`tower_name`,
        `flat_details`.`flat_name` as 'falt_name'
		FROM `user_address` JOIN `state` JOIN `city` LEFT JOIN `prk_area_dtl`
		ON `user_address`.`prk_admin_id`=`prk_area_dtl`.`prk_admin_id`  AND `prk_area_dtl`.`active_flag`='".FLAG_Y."'
        LEFT JOIN `tower_details` ON `tower_details`.`tower_id`=`user_address`.`tower_name`
        AND `tower_details`.`prk_admin_id`=`user_address`.`prk_admin_id` AND `tower_details`.`active_flag`='".FLAG_Y."' 
        AND `tower_details`.`del_flag`='".FLAG_N."'
        LEFT JOIN  `flat_details` ON `flat_details`.`flat_id`=`user_address`.`falt_name` 
        AND `flat_details`.`prk_admin_id`=`user_address`.`prk_admin_id` AND `flat_details`.`active_flag`='".FLAG_Y."' 			AND `flat_details`.`del_flag`='".FLAG_N."'
		WHERE `user_address`.`user_admin_id`='$user_admin_id' 
		AND `user_address`.`active_flag`='".FLAG_Y."'
		AND `user_address`.`del_flag`='".FLAG_N."'
		AND `state`.`active_flag`='".FLAG_Y."'
		AND `city`.`e_id`= `user_address`.`user_add_city`
		AND `state`.`e`= `user_address`.`user_add_state`
		ORDER BY 2 DESC";

    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){

	    $arr_catagory = $query->fetchAll();
	    foreach($arr_catagory as $val)
	    {
	        $park['user_add_id'] = $val['user_add_id'];
	        $park['prk_area_name'] = $val['prk_area_name'];
	        $park['user_address'] = $val['user_address'];
	        $park['landmark'] = $val['landmark'];
	        $park['user_add_city'] = $val['user_add_city'];
	        $park['user_add_state'] = $val['user_add_state'];
	        $park['state_name'] = $val['state_name'];
	        $park['city_name'] = $val['city_name'];
	        $park['user_add_country'] = $val['user_add_country'];
	        $park['user_add_pin'] = $val['user_add_pin'];
	        $park['prk_admin_id'] = $val['prk_admin_id'];
	        $park['tower_id'] = $val['tower_id'];
	        $park['tower_name'] = $val['tower_name'];
	        $park['flat_id'] = $val['flat_id'];
	        $park['falt_name'] = $val['falt_name'];
	        $park['default_flag'] = $val['default_flag'];
	        $park['user_add_verify_flag'] = $val['user_add_verify_flag'];
	        $park['qrcode_valu'] = $val['user_add_id'].'-oppr';
	        array_push($allplayerdata, $park);
	    }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['user_address'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>