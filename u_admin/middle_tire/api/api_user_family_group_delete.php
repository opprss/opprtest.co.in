<?php
/*
Description: user licence delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_family_group_delete($user_family_id,$user_add_id,$user_admin_id,$mobile){
    global $pdoconn;
    $response = array();
    $sql="UPDATE `user_family_group` SET `updated_by`='$mobile',`updated_date`='".TIME."',`del_flag`='".FLAG_Y."' WHERE `user_family_id`='$user_family_id' AND `user_admin_id`='$user_admin_id' AND `user_add_id`='$user_add_id' AND `del_flag`='".FLAG_N."' AND `active_flag` ='".FLAG_Y."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Successful';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
        return json_encode($response); 
    }
}
?>