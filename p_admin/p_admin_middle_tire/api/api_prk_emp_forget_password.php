<?php
/*
Description: parking employ forget password.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_emp_forget_password($prk_admin_id,$prk_area_user_id,$prk_admin_name,$password,$repassword){
    global $pdoconn;
    $response = array();
    //date_default_timezone_set('Asia/Kolkata');
    //$time=date("Y-m-d h:i:sa");
    if(passwordChecker($password,$repassword)=='true'){
        $password=md5($password);
        $sql ="UPDATE `prk_area_user` SET `prk_user_password`='$password',`updated_by`='$prk_admin_name',`updated_date`='".TIME."' WHERE `prk_admin_id`='$prk_admin_id' AND `prk_area_user_id`='$prk_area_user_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
        $query = $pdoconn->prepare($sql);
        if($query->execute()){
            $response['status'] = 1;
            $response['message'] = 'Password Updated Sucessfully';
        }else{
            $response['status'] = 0;
            $response['message'] = 'Password Updated Not Sucessfully';
        }
    }else{
        $response=passwordChecker($password,$repassword);
    }
    return json_encode($response);
}
?>