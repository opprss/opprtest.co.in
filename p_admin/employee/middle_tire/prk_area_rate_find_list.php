<?php 
/*
Description: parking area visit find the rate. 
Developed by: Rakhal Raj Mandal
Created Date: 09-06-2018
Update date : ----------
*/
 require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
 $response = array();
 $resp= array();
 if(isAvailable(array('prk_admin_id','veh_type'))){
 	if(isEmpty(array('prk_admin_id','veh_type'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
	 	$veh_type = trim($_POST['veh_type']);
        $response = prk_area_rate_find_list($prk_admin_id,$veh_type);       
        	//echo ($respon);    
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?> 