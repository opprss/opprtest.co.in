<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','parking_admin_name','token','tower_id'))){
    if(isEmpty(array('prk_admin_id','tower_id'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $parking_admin_name = trim($_POST['parking_admin_name']);
        $token = trim($_POST['token']);
        $tower_id = trim($_POST['tower_id']);
        // $resp=prk_token_check($prk_admin_id,$token);
        // $json = json_decode($resp);
        // if($json->status){
            $response = flat_list($prk_admin_id,$tower_id);
        // }else{
        //     $response = $resp;
        // }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
 }
echo $response;
?>