<?php 
/*
Description: parking area visit the people all list. 
Developed by: Rakhal Raj Mandal
Created Date: 09-04-2018
Update date : -------
*/
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
$response = array();
$resp = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','token'))){
 	if(isEmpty(array('prk_admin_id','prk_area_user_id','token'))){ 	

	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_area_user_id = trim($_POST['prk_area_user_id']);
        $token = ($_POST['token']);

        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
		if($json->status=='1'){
        	$response = prk_inf($prk_admin_id);
        	echo $response;
        }else{
            echo $resp;
        }
 	}else{
            $response['status'] = 0;
	    	$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response);
}
?>