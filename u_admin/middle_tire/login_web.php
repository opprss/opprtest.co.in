<?php 
/*
Description: user login. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
require_once 'api/reg_api.php'; 
$response = array();
 
if(isAvailable(array('mobile','password'))){

 	if(isEmpty(array('mobile', 'password'))){
		$device_token_id = isset($_POST['device_token_id']) ? $_POST['device_token_id'] : '';
		$device_type = isset($_POST['device_type']) ? $_POST['device_type'] : '';
	 	$mobile = trim($_POST['mobile']);
	 	$password = trim($_POST['password']);
	 	$respon=login($mobile, $password,$device_token_id,$device_type);
	 	//echo $respon;
        $json = json_decode($respon);
	 	if ($json->status == 1) {
	 	 	$user_ac_status = $json->user_ac_status;
	 		$_SESSION["user_admin_id"]=$json->user_admin_id;
	    	$_SESSION["mobile"]=$json->mobile;
	    	$_SESSION["token"]=$json->token;

	 	 	switch ($user_ac_status) {
	 	 		case 'A':
	 	 			//header('location: ../vehicle-list');
	 	 			$response['status'] = 1; 
	                $response['message'] = 'Login successfull';
	                $response['next_page'] = 'vehicle-list';
	                echo json_encode($response);        
	 	 			break;
	 	 		case 'T':
	 	 			//$_SESSION['error'] = "User temporary locked";
	 	 	 		$response['status'] = 0; 
	                $response['message'] = "User temporary locked";
	                echo json_encode($response);   
	 	 			break;
	 	 		case 'L':
	 	 			//$_SESSION['error'] = "User locked";
	 	 	 		$response['status'] = 0; 
	                $response['message'] = "User locked";
	                echo json_encode($response);
	 	 			break;
	 	 		case 'P':
	 	 			//$_SESSION['error'] = "User payment due, Please contact customer care";
	 	 			$response['status'] = 0; 
	                $response['message'] = "User locked";
	                echo json_encode($response);
	 	 			break;
	 	 		case 'D':
	 	 			//header('location: ../signup-complete');
	 	 			$response['status'] = 1;
	                $response['next_page'] = 'signup-complete';
	                $response['message'] = "login successfull";
	                echo json_encode($response);
	 	 			break;
	 	 		default:
	 	 			//$_SESSION['error'] = "Somthing went wrong, Please Contact Customer Care";
	 	 			$response['status'] = 0; 
	                $response['message'] = "Somthing went wrong, Please Contact Customer Care";
	                echo json_encode($response);
	 	 			break;
	 	 	}
	 	}else{
	 	 	$response['status'] = 0; 
            $response['message'] = $json->message;
            echo json_encode($response);
	 	}
 	}else{
 			$response['status'] = 0;
	 		$response['message'] = 'Mobile or password is empty';
            echo json_encode($response); 
	 }
}else{
	 $response['status'] = 0; 
	 $response['message'] = 'Invalid API Call';
     echo json_encode($response); 
} 
?>