<!-- ################################################
  
Description: Parking area can issue gate pass to a particular user.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
 <?php 
include "all_nav/header.php";
if (isset($_SESSION['prk_admin_id'])) {

  $user_name=$_SESSION['parking_admin_name'];
  $prk_admin_id = $_SESSION['prk_admin_id'];
}
$id = $_REQUEST['id'];
$response = prk_emp_money_aloc_list_update($id);
$response = json_decode($response, true);
//Array ( [allplayerdata] => Array ( [0] => Array ( [prk_emp_money_aloc_id] => 16 [prk_emp_id] => 4 [prk_emp_name] => IN USER [issued_date] => 01-07-2018 [rs2000] => 4000 [rs200] => 0 [rs100] => 0 [rs50] => 0 [rs20] => 0 [rs10] => 0 [rs5] => 0 [coin] => 0 [total] => 4000 [refund_flag] => N [refund_date] => ) ) [status] => 1 [message] => data ssucessfull )
if ($response['status']) {
  $response = $response['allplayerdata'][0];

  $emp_name = $response['prk_emp_name'];
  $emp_id = $response['prk_emp_id'];
  $rs2000 = $response['rs2000'];
  $rs200 = $response['rs200'];
  $rs100 = $response['rs100'];
  $rs50 = $response['rs50'];
  $rs20 = $response['rs20'];
  $rs10 = $response['rs10'];
  $rs5 = $response['rs5'];
  $rscoin = $response['coin'];
  $total = $response['total'];
  $date = $response['issued_date'];
  //$emp_name = $response['total'];

  

}
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: gray;

  }
  .success{
    font-size: 11px;
    color: green;
}
</style>

<!-- header position -->

    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Money Allocation Modify</h5>
        <!--<form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card pd-20 pd-sm-40 col-md-12 single">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <div class="row">
            <div class="col-md-12">
                <form id="moneyAllocForm" method="get" action="">
                  <div class="form-layout">
                    <div class="row mg-b-25">
                      <div class="col-lg-3 error_show">
                        <div class="form-group">
                            <label class="form-control-label size">EMPLOYEE NAME <span class="tx-danger">*</span></label>
                            <div class="select">
                              <select name="employee" id="employee" style="font-size: 14px; opacity: 0.8;" class="readonly" disabled="disabled">
                                  <option value="<?php if (isset($emp_id)) echo $emp_id;
                                   ?>"><?php if (isset($emp_name)) echo $emp_name;
                                   ?></option>
                              </select>
                            </div>
                            <span style="position: absolute;" id="error_employee" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="form-control-label size">DATE <span class="tx-danger">*</span></label>
                          <input type="text" class="form-control readonly" placeholder="DATE" name="date" id="date" readonly="readonly" value="<?php if (isset($date)) echo $date;?>" style="background-color: transparent;">
                          <span style="position: absolute;" id="error_prk_gate_pass_num" class="error_size"></span>
                        </div>
                      </div>
                    </div>
                    <div class="row mg-b-25">
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            RS. 2000 X
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <!-- <label class="form-control-label size">RS. 2000 X </label> -->
                              <input type="text" class="form-control mg-0" style="margin-top: -10px;" placeholder="" name="rs2000" id="rs2000" value="<?php if (isset($rs2000)) echo $rs2000/2000;?>" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="getResult('2000',this.value);">
                              <span style="position: absolute;" id="error_rs2000" class="error_size"></span>
                            </div>
                          </div>
                          <div class="col-md-5">
                            = Rs <span id="rs2000_res"><?php if (isset($rs2000)) echo $rs2000;?></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            RS. 200 X
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <!-- <label class="form-control-label size">RS. 2000 X </label> -->
                              <input type="text" class="form-control" style="margin-top: -10px;" name="rs200" id="rs200" value="<?php if (isset($rs200)) echo $rs200/200;?>" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="getResult('200',this.value);">
                              <span style="position: absolute;" id="error_rs200" class="error_size"></span>
                            </div>
                          </div>
                          <div class="col-md-5">
                            = Rs <span id="rs200_res"><?php if (isset($rs200)) echo $rs200;?></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            RS. 100 X
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <!-- <label class="form-control-label size">RS. 2000 X </label> -->
                              <input type="text" class="form-control" style="margin-top: -10px;" name="rs100" id="rs100" value="<?php if (isset($rs100)) echo $rs100/100;?>" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="getResult('100',this.value);">
                              <span style="position: absolute;" id="error_rs100" class="error_size"></span>
                            </div>
                          </div>
                          <div class="col-md-5">
                           = Rs <span id="rs100_res"><?php if (isset($rs100)) echo $rs200;?></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            RS. 50 X
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <!-- <label class="form-control-label size">RS. 2000 X </label> -->
                              <input type="text" class="form-control mg-0" style="margin-top: -10px;"name="rs50" id="rs50" value="<?php if (isset($rs50)) echo $rs50/50;?>" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="getResult('50',this.value);">
                              <span style="position: absolute;" id="error_rs50" class="error_size"></span>
                            </div>
                          </div>
                          <div class="col-md-5">
                            = Rs <span id="rs50_res"><?php if (isset($rs50)) echo $rs50;?></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            RS. 20 X
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <!-- <label class="form-control-label size">RS. 2000 X </label> -->
                              <input type="text" class="form-control" style="margin-top: -10px;" name="rs20" id="rs20" value="<?php if (isset($rs20)) echo $rs20/20;?>" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="getResult('20',this.value);">
                              <span style="position: absolute;" id="error_rs20" class="error_size"></span>
                            </div>
                          </div>
                          <div class="col-md-5">
                            = Rs <span id="rs20_res"><?php if (isset($rs20)) echo $rs20;?></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            RS. 10 X
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <!-- <label class="form-control-label size">RS. 2000 X </label> -->
                              <input type="text" class="form-control" style="margin-top: -10px;" name="rs10" id="rs10" value="<?php if (isset($rs10)) echo $rs10/10;?>" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="getResult('10',this.value);">
                              <span style="position: absolute;" id="error_rs10" class="error_size"></span>
                            </div>
                          </div>
                          <div class="col-md-5">
                            = Rs <span id="rs10_res"><?php if (isset($rs10)) echo $rs10;?></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            RS. 5 X
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <!-- <label class="form-control-label size">RS. 2000 X </label> -->
                              <input type="text" class="form-control" style="margin-top: -10px;" name="rs5" id="rs5" value="<?php if (isset($rs5)) echo $rs5/5;?>" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="getResult('5',this.value);">
                              <span style="position: absolute;" id="error_rs5" class="error_size"></span>
                            </div>
                          </div>
                          <div class="col-md-5">
                            = Rs <span id="rs5_res"><?php if (isset($rs5)) echo $rs5;?></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            COIN (RS)
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <!-- <label class="form-control-label size">RS. 2000 X </label> -->
                              <input type="text" class="form-control" style="margin-top: -10px;" name="coin" id="coin" value="<?php if (isset($rscoin)) echo $rscoin;?>" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="getResult('coin',this.value);">
                              <span style="position: absolute;" id="error_one_hundredd" class="error_size"></span>
                            </div>
                          </div>
                          <div class="col-md-5" >
                            = Rs <span id="rscoin_res"><?php if (isset($rscoin)) echo $rscoin;?></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-4">
                            <b>TOTAL = 
                          </div>
                          <div class="col-md-8">
                            Rs <span id="total_res"><?php if (isset($total)) echo $total;?></span></b>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-layout-footer mg-t-30">
                          <div class="row">
                            <div class="col-lg-6">
                              <button type="button" class="btn btn-block prk_button" name="save" id="save">Save</button>
                            <span style="position: absolute; color: red;" id="error_all" class="error_size"></span>
                            </div>
                            <div class="col-lg-6">
                              <button type="button" class="btn btn-block prk_button_skip" onclick="document.location.href='employee-money-allocation';">BACK</button>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div><!-- form-layout -->
                </form>
            </div>
            
          </div>
        </div>
        </div><!-- card -->
        <!-- /add employee form -->
      </div><!-- am-pagebody -->
      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<style>
 .error_size{
  font-size: 11px;
  color: red;

}
.success{
  font-size: 11px;
  color: green;
}
</style>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE_WEB ?>";
  function getResult(id, value){
    /*alert(value);
      if (value== '') {
        //alert('nunnn');
        $("#rs2000").val("0");
      }*/
      var field_id = "rs"+id;
      var result_id = "rs"+id+"_res";
      var result = id*value;

      if (id == "coin") {
        var result = 1*value;
      }

       $('#' + result_id).text(result);

       //var rs2000 = $('#rs2000').val();
       var total = parseInt($("#rs2000_res").html())+parseInt($("#rs200_res").html())+parseInt($("#rs100_res").html())+parseInt($("#rs50_res").html())+parseInt($("#rs20_res").html())+parseInt($("#rs10_res").html())+parseInt($("#rs5_res").html())+parseInt($("#rscoin_res").html());
       //alert(total);
       $('#total_res').text(total);
  }

  $( document ).ready( function () {

  	$('#reset').click(function(){
  	 $('.error_size').text('');
  	});
    /*FORM SUBMIT*/
    $('#save').click(function(){
        var prk_admin_id = "<?php echo $prk_admin_id;?>";
        var emp_name = $("#employee").find(":selected").text();
        var emp_id = $("#employee").find(":selected").val();
        var inserted_by = "<?php echo $user_name; ?>";
        var token = "<?php echo $token; ?>";
        var date = $('#date').val();
        var rs2000 = $('#rs2000_res').html();
        var rs200 = $('#rs200_res').html();
        var rs100 = $('#rs100_res').html();
        var rs50 = $('#rs50_res').html();
        var rs20 = $('#rs20_res').html();
        var rs10 = $('#rs10_res').html();
        var rs5 = $('#rs5_res').html();
        var rscoin = $('#rscoin_res').html();
        var total = $('#total_res').html();
        if (emp_id!='') {

          var urlEmpMoneyAlloc = prkUrl+'prk_emp_money_aloc_update.php';
          
          $.ajax({
            url :urlEmpMoneyAlloc,
            type:'POST',
            data :
            {
              'prk_emp_id':emp_id,
              'prk_admin_id':prk_admin_id,
              'prk_emp_name':emp_name,
              'rs2000':rs2000,
              'rs200':rs200,
              'rs100':rs100,
              'rs50':rs50,
              'rs20':rs20,
              'rs10':rs10,
              'rs5':rs5,
              'coin':rscoin,
              'total':total,
              'issued_date':date,
              'user_name':"<?php echo $user_name; ?>",
              'prk_emp_money_aloc_id':"<?php echo $id ?>",
              'token':"<?php echo $token; ?>"
            },
            dataType:'html',
            success  :function(data)
            {
              //alert(data);                  
              var json = $.parseJSON(data);
              if (json.status){
                  $.alert({
                  icon: 'fa fa-smile-o',
                  theme: 'modern',
                  title: 'Success !',
                  content: "<p style='font-size:0.9em;'>Money allocate update successfully</p>",
                  type: 'green',
                  buttons: {
                    Ok: function () {
                        window.location='employee-money-allocation';
                    }
                  }
                });
              }else{
                if( typeof json.session !== 'undefined'){
                  if (!json.session) {
                    window.location.replace("logout.php");
                  }
                }else{
                  $.alert({
                  icon: 'fa fa-frown-o',
                  theme: 'modern',
                  title: 'Error !',
                  content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                  type: 'red'
                });
                }
              }
              
            }
          });
          
        }else{
          if (emp_id == '') {
            $('#error_employee').text('Employee name is required');
          }
        }   
    });
  
    $("#employee").blur(function () {
      var employee = $('#employee').val();
      if(employee == ''){
      $('#error_employee').text('Employee name is required');
      return false;
      }else{
        $('#error_employee').text('');
        return true;
      }
    });
  });
</script>