<?php 
/*
Description: found the parking area list.
Developed by: Rakhal Raj Mandal
Created Date: 30-05-2018
Update date : ----------
*/
require_once 'api/reg_api.php';
// require_once 'api/global_api.php';
$response = array();
if(isAvailable(array('user_admin_id','token','prk_area_name'))){
    if(isEmpty(array('user_admin_id','token'))){
        $user_admin_id = trim($_POST['user_admin_id']);
        $token = trim($_POST['token']);
        $prk_area_name = trim($_POST['prk_area_name']);
        $token_ch=user_token_check($user_admin_id,$token);
        $json = json_decode($token_ch);
        if($json->status=='1'){

            $response = prk_area_list_search($prk_area_name);
            //echo json_encode($response);
        }else{
        $response = $token_ch; 
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response); 
} 
echo $response;
?>