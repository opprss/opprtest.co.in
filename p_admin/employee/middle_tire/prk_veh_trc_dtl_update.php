<?php 
/*
Description: vehicle out update out time 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/  
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('prk_veh_trc_dtl_id','prk_out_rep_name'))){
 	if(isEmpty(array('prk_veh_trc_dtl_id','prk_out_rep_name'))){

        $prk_veh_trc_dtl_id = trim($_POST['prk_veh_trc_dtl_id']);
        $prk_out_rep_name = trim($_POST['prk_out_rep_name']);

        
        $respon=prk_veh_trc_dtl_update($prk_veh_trc_dtl_id,$prk_out_rep_name);
        echo ($respon);
        
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?>