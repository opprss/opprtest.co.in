<?php
/*
Description: random passwors generator.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function random_password(){
    $result="";
    $char="abcdefghijklmnopqrst@0123456789";
    $chararray=str_split($char);
    for ($i=0; $i < 6; $i++) { 
    $randItem=array_rand($chararray);
    $result.="".$chararray[$randItem];
    }
    return $result;
}
?>