<?php
/*
Description: Parking area particular one sub unit data show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_sub_unit_show($prk_sub_unit_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT * FROM `prk_sub_unit` WHERE `prk_sub_unit_id`='$prk_sub_unit_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $response['status'] = 1;
    $response['message'] = 'data ssucessfull';
    $response['prk_sub_unit_id'] = $val['prk_sub_unit_id'];
    $response['prk_sub_unit_name'] = $val['prk_sub_unit_name'];
    $response['prk_sub_unit_short_name'] = $val['prk_sub_unit_short_name'];
    $response['prk_sub_unit_address'] = $val['prk_sub_unit_address'];
    $response['prk_sub_unit_rep_name'] = $val['prk_sub_unit_rep_name'];
    $response['prk_sub_unit_rep_mob_no'] = $val['prk_sub_unit_rep_mob_no'];
    $response['prk_sub_unit_rep_email'] = $val['prk_sub_unit_rep_email'];
    $response['prk_sub_unit_eff_date'] = $val['prk_sub_unit_eff_date'];
    $response['prk_sub_unit_end_date'] = $val['prk_sub_unit_end_date'];

    return json_encode($response);
}
?>