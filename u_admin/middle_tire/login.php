<?php 
/*
Description: user login. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
require_once 'api/reg_api.php'; 
$response = array();
 
if(isAvailable(array('mobile','password'))){
 	if(isEmpty(array('mobile', 'password'))){
		$device_token_id = isset($_POST['device_token_id']) ? $_POST['device_token_id'] : '';
		$device_type = isset($_POST['device_type']) ? $_POST['device_type'] : 'OTH';
	 	$mobile = trim($_POST['mobile']);
	 	$password = trim($_POST['password']);
	 	$respon=login($mobile, $password,$device_token_id,$device_type);
	 	echo $respon;
	}else{
		$response['status'] = 0;
		$response['message'] = 'Mobile or password is empty';
		echo json_encode($response); 
	}
}else{
	$response['status'] = 0; 
	$response['message'] = 'Invalid API Call';
	echo json_encode($response); 
} 
?>