<?php
/*
Description: Parking adimn payment undifine update.
Developed by: Manoranjan Sarkar
Created Date: 22-05-2018
Update date :
*/ 
    // require_once 'api/global_api.php';
    require_once 'api/parkAreaReg_api.php';
    $response = array();
 
    if(isAvailable(array('payment_dtl_id'))){

        if(isEmpty(array('payment_dtl_id'))){
            $payment_dtl_id = trim($_POST['payment_dtl_id']);
            
            $respon = prk_payment_undifine_update($payment_dtl_id);
            echo ($respon);
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
        }
    }else{
         $response['status'] = 0; 
         $response['message'] = 'Invalid API Call';
         echo json_encode($response);
     }
?>