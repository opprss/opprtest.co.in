<?php
/*
Description: user payment history.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_kids_dropdown_list($user_admin_id,$prk_admin_id){
    global $pdoconn;
    $allplayerdata = array();
    $sql = "SELECT ufg.`user_family_id`,
        ufg.`user_family_name`,
        ufg.`user_family_gender`,
        IF(ufg.`user_family_img` IS NULL OR ufg.`user_family_img`='','',CONCAT('".USER_BASE_URL."',ufg.`user_family_img`)) as user_family_img
        FROM `user_family_group` ufg,`user_address` u_add 
        WHERE u_add.`user_add_id`=ufg.`user_add_id` 
        AND ufg.`user_admin_id`='$user_admin_id' 
        AND ufg.`prk_admin_id`='$prk_admin_id' 
        AND ufg.`active_flag`='".FLAG_Y."' 
        AND ufg.`del_flag`='".FLAG_N."' 
        AND ufg.`user_family_relation`='KID'
        AND u_add.`active_flag`='".FLAG_Y."' 
        AND u_add.`del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    $group = array();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['user_family_id'] = $val['user_family_id'];
            $park['user_family_name'] = $val['user_family_name'];
            $park['user_family_gender'] = $val['user_family_gender'];
            $park['user_family_img'] = $val['user_family_img'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['kids_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>