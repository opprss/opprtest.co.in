<?php
/*
Description: parking area vehicle verify show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_prk_veh_verify_show($prk_user_name,$prk_admin_id,$prk_area_gate_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT `user_prk_veh_verify`.`user_veh_type`,
        `user_prk_veh_verify`.`user_veh_number`,
        `user_prk_veh_verify`.`user_prk_veh_varify_id`,
        `vehicle_type`.`vehicle_typ_img`
        FROM `user_prk_veh_verify`,`vehicle_type` 
        WHERE `user_prk_veh_verify`.`prk_admin_id`='$prk_admin_id' 
        AND `user_prk_veh_verify`.`prk_area_gate_id`='$prk_area_gate_id' 
        AND `user_prk_veh_verify`.`user_prk_veh_verify_status`='".FLAG_F."'
        AND `user_prk_veh_verify`.`active_flag`='".FLAG_Y."' 
        AND `vehicle_type`.`active_flag`='".FLAG_Y."'
        AND `vehicle_type`.`vehicle_sort_nm`=`user_prk_veh_verify`.`user_veh_type`
        ORDER BY   `user_prk_veh_verify`.`user_prk_veh_varify_id` ASC";

    $query  = $pdoconn->prepare($sql);
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['session'] = 1;
        $response['message'] = 'Successful';
        $response['user_prk_veh_varify_id'] = $val['user_prk_veh_varify_id'];
        $response['user_veh_type'] = $val['user_veh_type'];
        $response['user_veh_number'] = $val['user_veh_number'];
        $response['vehicle_typ_img'] = $val['vehicle_typ_img'];
    }else{
        $response['status'] = 0;
        $response['session'] = 1;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>