<!-- ################################################
  
Description: list of all transection made by the paricular user with details.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
include "all_nav/header.php";
?>

<!-- header position -->

    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">My Transactions</h5>
        <!-- <form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card single pd-20 pd-sm-40 col-md-12">

          <div class="row" style="padding-bottom: 20px;">
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label user-lebel">By Month</label>
                <div class="select" style="">
                  <select name="month" id="month" style="opacity: 0.8">
                      <option value="">ALL</option>
                      <option value="1">Last 1 Month</option>
                      <option value="3">Last 3 Month</option>
                      <option value="6">Last 6 Month</option>
                      <option value="D">Select Date</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-1">
              
            </div>
            <div class="col-md-2">
              <div class="form-group">
                  <label class="form-control-label user-lebel">By Selected Date</label>

                  <input type="text" class="form-control" placeholder="start date" name="start_date" id="start_date" readonly="readonly" disabled="disabled">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label user-lebel">&nbsp;</label><!-- prevent_readonly -->
                  <input type="text" class="form-control " placeholder="end date" name="end_date" id="end_date" disabled="disabled">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <!-- <label class="form-control-label user-lebel">&nbsp;</label> --><!-- prevent_readonly -->
                  <button class="btn btn-block user_button" id="ok" style="margin-top: 29px;">OK</button>
              </div>
            </div>

            <div class="col-md-1">
              
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label user-lebel">Filter By</label>

                <div class="select" style="">
                  <select name="pay_type" id="pay_type" style="opacity: 0.8">
                      <option value="">ALL</option>
                      <option value="<?php echo CASH ?>"><?php echo CASH ?></option>
                      <option value="<?php echo WALLET ?>"><?php echo WALLET ?></option>
                      <option value="<?php echo DGP ?>"><?php echo DGP_F ?></option>
                      <option value="<?php echo TDGP ?>"><?php echo TDGP_F ?></option>
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            
            <div class="row">
              <div class="col-md-12">                  
                  <div class="list-group widget-list-group" id="list">                      
                        <!-- listing -->
                        <div class="text-center">
                          LOADING...
                        </div>
                        <!-- listing -->
                    <!-- <i class="fa fa-times text-center tx-60"></i>
                    <h5 class="text-center">You have not done any transection</h5> -->
                  </div>

              </div>
              
            </div>
          </div>
        </div><!-- card -->
      </div><!-- am-pagebody -->
      

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
 
  /*for Tool Tip*/
  $(document).ready(function(){

    var userUrl = "<?php echo USER_URL; ?>";
    var imagUrl = "<?php echo PRK_BASE_URL?>";
     var user_admin_id = "<?php echo $_SESSION['user_admin_id'] ?>";
      var month = $('#month :selected').val();
      //alert(month);
      var start_date = '';
      var end_date = '';
      var payment_type = '';
      callAjax(user_admin_id,month,start_date,end_date,payment_type);

      $('#month').change(function(){
        var month = $('#month :selected').val();
        if (month == "D") {
          //alert("D");
          $("#start_date").prop("disabled", false);
          $("#start_date").addClass("prevent_readonly");
          $("#end_date").prop("disabled", false);
          $("#end_date").addClass("prevent_readonly");
        }
        callAjax(user_admin_id,month,start_date,end_date,payment_type);
      });

      $('#ok').click(function(){
        start_date = $('#start_date').val();
        end_date = $('#end_date').val();
        //alert(start_date);
        callAjax(user_admin_id,month,start_date,end_date,payment_type);
        //alert("hello");
      });
      
      $('#pay_type').change(function(){
        var payment_type = $('#pay_type :selected').val();
        //alert(payment_type);
        callAjax(user_admin_id,month,start_date,end_date,payment_type);
      });

      function callAjax(user_admin_id,month,start_date,end_date,payment_type){
        //alert("ajax call")
        var urluserPaymentHistory = userUrl+'user_payment_history.php';

          $.ajax({
            url :urluserPaymentHistory,
            type:'POST',
            data :
            {
              'user_admin_id':user_admin_id,
              'month':month,
              'start_date':start_date,
              'end_date':end_date,
              'payment_type':payment_type
            },
            dataType:'json',
            success  :function(data)
            {
              var demoLines = '';
              if(data.status){
              	for (var key in data.payment_history) {
                //alert(key);
                //console.log(data.payment_history[key]);
                //alert(data.payment_history[key][0].total_hr);
                //$("#list").html(key);
                demoLines += "<p style='padding:10px; margin:-3px; background-color:#f1f1f1;'><b>"+key+"</b><p>";
                $.each(data.payment_history[key], function() {
                  	//alert(this["prk_area_pro_img"]);
	                 // var img = "opprss.com/oppr/p_admin/"+this["prk_area_pro_img"];
	                  //alert(img);
	                  
	                  if (this["payment_type"] == '<?php echo DGP ?>') {
                      this["payment_type"] = 'Digital Gate Pass';
                    }else if(this["payment_type"] == '<?php echo TDGP ?>'){
                       this["payment_type"] = 'Temp Digital Gate Pass';
                    }
	                  
	                  /*testing*/
	                  demoLines += '<div class="list-group-item rounded-top-0">\
	                  <div class="media pd--b-20">\
	                    <div class="d-flex mg-l-0 pd-l-0 mg-t-5 mg-r-20">\
	                    <img src="'+imagUrl+this["prk_area_pro_img"]+'" style="width: 60px; height: 60px; border-radius:20px;">\
	                  </div>\
	                  <div class="media-body">\
	                  <div class="row">\
	                  <div class="col-lg-12 tx-18 pd-b-10">\
	                  '+this["veh_number"]+'('+this["vehicle_type"]+')\
	                  </div>\
	                  <div class="col-lg-3">\
	                  <p class="mg-b-0 tx-12">\
	                  IN DATE-\
	                  '+this["veh_in_date"]+'\
	                  <br>\
	                  IN TIME-\
	                  '+this["veh_in_time"]+'\
	                  </p>\
	                  </div>\
	                  <div class="col-lg-3">\
	                  <p class="mg-b-0 tx-12">\
	                  OUT DATE-\
	                  '+this["veh_out_date"]+'\
	                  <br>\
	                  OUT TIME- \
	                  '+this["veh_out_time"]+'\
	                  </p>\
	                  </div>\
	                  <div class="col-lg-3">\
	                  <p class="mg-b-0 tx-12">\
	                  TOTAL HOUR-\
	                  '+this["total_hr"]+'\
	                  <br>\
	                  ROUND UP TIME- \
	                  '+this["round_hr"]+'\
	                  </p>\
	                  </div>\
	                  </div>\
	                  </div>\
	                  <div class="tx-inverse tx-12" style="text-align: right;">\
	                  <P class="mg-b-0">'+this["transition_date"]+'</P>\
	                  <P class="mg-b-0"> Rs. '+this["total_pay"]+'</P>\
	                  <P class="mg-b-0">'+this["payment_type"]+'</P>\
	                  </div>\
	                  </div>\
	                  </div>';
	
	                });
              	}
              }else{
              	demoLines += "<p style='padding:10px; margin:-3px; text-align:center'><b>List is empty</b><p>";
              }
              $("#list").html(demoLines);
            }
          });
      }
    

    $('[data-toggle="tooltip"]').tooltip();

  });
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#start_date', {
      default_date   : false,
        /*format  : 'Y-m-d',*/
        /*position       : 'top',*/
        hide_on_select : true,
        /*render : function (date) {
              if (date < now) {
                  return {
                    disabled : true,
                    class_name : 'date-in-past'
                  };
              }
              return {};
          } */
    });
  });

  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#start_date').val();
    //alert(end);
    var d=new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var newdate=yy+"/"+mm+"/"+dd;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24);
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    //delete days;
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
            if (date < now_end) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            return {};
        } 
    });
  });

  $('#start_date').focus(function(){
    $('#end_date').val('');
  });
  /*$('#end_date').click(function(){
    if ($('#start_date').val('') == '') {
      
    }
  });*/
</script>