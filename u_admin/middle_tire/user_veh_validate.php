<?php 
/*
Description: user vehicle verify validate.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
	require_once 'api/reg_api.php';
	// require_once 'api/global_api.php';
	$response = array();
	if(isAvailable(array('user_admin_id','user_veh_dtl_id'))){
	 	if(isEmpty(array('user_admin_id','user_veh_dtl_id'))){

		 	$user_admin_id = trim($_POST['user_admin_id']);
		 	$user_veh_dtl_id = trim($_POST['user_veh_dtl_id']);
	        $respon=user_veh_validate($user_admin_id,$user_veh_dtl_id);
	        echo ($respon);
	 	}else{
	            $response['status'] = 0;
		 		$response['message'] = 'All Fields Are Mandatory';
	            echo json_encode($response);
	    }
	}else{
	    $response['status'] = 0; 
	    $response['message'] = 'Invalid API Call';
	 	echo json_encode($response);
	}
?>