<!-- ################################################
  
Description: Parking area admin can change the login password.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
include "all_nav/header.php"; 
@session_start();
if (isset($_SESSION['prk_admin_id'])) {
  $parking_admin_name=$_SESSION['parking_admin_name'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
    }
?>
<style>
  .size{
    font-size: 11px;
  }
.error_size{
  font-size: 11px;
  color: red;

}
.success{
  font-size: 11px;
  color: green;
}
</style>
<!-- header position -->


    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Change Password</h5>
        <!--<form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card single pd-20 pd-sm-40 col-md-12">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <div class="row">
              <div class="col-md-12">
                
                  <form id="regForm2" method="post" action="">
                    <div class="form-layout">
                      <div class="row mg-b-25">
                        <div class="col-lg-3">
                          <div class="form-group">
                            <label class="form-control-label size">NAME</label>
                            <input type="text" class="form-control readonly" placeholder="prk_admin_name" name="prk_admin_name" id="prk_admin_name" value="<?php echo $_SESSION["parking_admin_name"] ?>" readonly="readonly" style="background-color: transparent;">
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label size">OLD PASSWORD <span class="tx-danger">*</span></label>
                            <input type="password" class="form-control" placeholder="Old Password" name="oldPasword" id="oldPasword">
                            <span style="position: absolute;" id="error_oldPasword" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label size">NEW PASSWORD <span class="tx-danger">*</span></label>
                            <input type="password" class="form-control readonly" placeholder="New Password" name="password" id="password" disabled="disabled" style="background-color: transparent;">
                            <span style="position: absolute;" id="error_password" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label size">CONFIRM PASSWORD <span class="tx-danger">*</span></label>
                            <input type="password" class="form-control readonly" placeholder="Confirm Password" name="repassword" id="repassword" disabled="disabled" style="background-color: transparent;">
                            <span style="position: absolute;" id="error_repassword" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-4">
                          <div class="form-layout-footer mg-t-30">
                            <div class="row">
                              <div class="col-lg-6">
                                <button type="button" class="btn btn-block prk_button" name="change" id="change" disabled="disabled">CHANGE</button>
                              </div>
                              <div class="col-lg-6">
                                <button type="button" class="btn btn-block prk_button_skip" name="skip" id="skip" onclick="document.location.href='dashboard';">SKIP</button>

                              </div>
                            </div>
                          </div>
                        </div>
                    </div><!-- form-layout -->
                  </form>
              </div>
              
            </div>
          </div>
        </div><!-- card -->
        <!-- /add employee form -->
      </div><!-- am-pagebody -->
      

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
 var prk_admin_id =" <?php echo $prk_admin_id;?>";
        /*validation*/
        $( document ).ready( function () {

            /*custom validation*/

            $("#oldPasword").blur(function () {
              var oldPasword=$('#oldPasword').val();
              var prk_admin_name=$('#prk_admin_name').val();
              if(oldPasword==''){
                $('#error_oldPasword').text('Please enter your old password');
                $('#oldPasword').focus();

                }else if(oldPasword != ''){
                    var urlCheckOldPass = prkUrl+'check_old_password.php';
                    // alert(urlCheckOldPass);
                    $.ajax({
                      url :urlCheckOldPass,
                      type:'POST',
                      data :
                      {
                       'prk_admin_name':prk_admin_name,
                       'password':oldPasword,
                      },
                      dataType:'html',
                      success  :function(data)
                      {
                        // alert(data);
                        var json = $.parseJSON(data); // create an object with the key of the array
                        if (json.status) {
                          $('#error_oldPasword').hide();
                          $('#password').prop("disabled",false);
                          $('#password').removeClass("readonly");
                          $('#repassword').prop("disabled",false);
                          $('#repassword').removeClass("readonly");
                          $('#password').focus();
                          $('#oldPasword').prop("disabled",true);
                          $('#oldPasword').addClass("readonly");
                          $('#change').prop("disabled",false);
                          
                        }else{
                          $('#error_oldPasword').text('Incorrect old password');
                           $('#oldPasword').focus();
                        }
                      }
                    }); 
                }else{
                  $('#error_oldPasword').hide();
              }

            });

           $('#change').click(function(){
              var prk_admin_id =" <?php echo $prk_admin_id;?>";
              // alert(prk_admin_id);
              var token='<?php echo $token;?>';
              // alert(token);
              var password = $('#password').val();
              var repassword = $('#repassword').val();
              var user_name = $('#prk_admin_name').val();
              var email = $('#email').val();
              if(password!='' && repassword!=''){

                if (password == repassword) {
                  var urlChangePasswordPrkAdmin = prkUrl+'changePasswordPrkAdmin.php';
                  $.ajax({
                    url :urlChangePasswordPrkAdmin,
                    type:'POST',
                    data :
                    {
                      'user_name':user_name,
                      'password':password,
                      'repassword':repassword,
                      'prk_admin_id': prk_admin_id,
                      'token': token
                    },
                    dataType:'html',
                    success  :function(data)
                    {
                      // alert(data);
                      var json = $.parseJSON(data);
                        if (json.status) {
                          $.alert({
                            icon: 'fa fa-smile-o',
                            theme: 'modern',
                            title: 'Success !',
                            content: "<p style='font-size:0.7em;'>Password changed successfully</p>",
                            type: 'green',
                            buttons: {
                              Ok: function () {
                                  window.location='dashboard';
                              }
                            }
                          });
                        }else{
                          if( typeof json.session !== 'undefined'){
                            if (!json.session) {
                              // alert("ok");
                             // window.location.replace("logout.php");
                            }
                          }else{
                            $.alert({
                            icon: 'fa fa-frown-o',
                            theme: 'modern',
                            title: 'Error !',
                            content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                            type: 'red'
                          });
                          }
                        }
                      }
                    });  
                  }else{
                    $('#error_repassword').text('password mismatch');
                    $('#repassword').focus();
                }
              }else{
                $('#error_password').text('Required Password');
                $('#error_repassword').text('Required Confirm Password');                
              }

            });

            $("#password").blur(function () {
              var password = $('#password').val();
               
              if(password==''){
                $('#error_password').text('Required password');
                $('#password').focus();
              }else if(!validatePassword(password)){
                $('#error_password').text('Minimum 8 characters, atleast 1 number, 1 alphabet & 1 spcial character.');
                $('#password').focus();
              }else{
                $('#error_password').hide();
              }
            });

            $("#repassword").blur(function () {
              var repassword=$('#repassword').val();
              var password=$('#password').val();
              if(repassword==''){
                $('#error_repassword').text('Required confirm password');
                $('#repassword').focus();
              }else if(password != repassword){
                $('#error_repassword').text('Password missmatch');
                //$('#repassword').focus();
              }else if(!validatePassword(repassword)){
                $('#error_repassword').text('Minimum 8 character, atleast 1 number, 1 alphabet & 1 spcial character.');
                $('#repassword').focus();
              }else{
                $('#error_repassword').hide();
              }
            });


            function validatePassword(txt) {
              var filter = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/;
              if (filter.test(txt)) {
                  return true;
                  //alert("ok");
              }
              else {
                  return false;
              }
            }
        } );
  </script>