<!-- ################################################
  
  Description: Parking area can issue gate pass to a particular Visitor.
  Developed by: Soemen Banerjee
  Created Date: 17-03-2018
####################################################-->
<?php 
  include "all_nav/header.php";
?>
<link href="../lib/highlightjs/github.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../lib/select2/css/select2.min.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
<style>
  .tx-16,.size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: gray;

  }
  .success{
    font-size: 11px;
    color: green;
  }
  th,td{
    text-align: center;
  }
</style>
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Office Member List</h5>
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40 col-md-12">
    	<div class="row">
    		<div class="col-md-4"></div>
    		<div class="col-md-4"></div>
      	<div class="col-md-4">
        	<a style="margin-bottom: 10px;" href="meet-to-person" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add Office Member</a>
        </div>
      </div>
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <div class="table-wrapper">
        <table id="datatable1" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="wd-5p">Sl No.</th>
              <th class="wd-15p">Name</th>
              <th class="wd-10p">Mobile</th>
              <th class="wd-10p">Deparment</th>
              <th class="wd-10p">Eff Date</th>
              <th class="wd-10p">End Date</th>
              <th class="wd-10p">Action</th>
            </tr>
          </thead>
          <tbody id="list_ve">
          </tbody>
        </table>
      </div>
      </div>
    </div>
  </div>
<?php include"all_nav/footer.php"; ?>
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="../lib/datatables-responsive/jquery.dataTables.min.js"></script>
<script src="../lib/datatables-responsive/dataTables.buttons.min.js"></script>
<script src="../lib/datatables-responsive/jszip.min.js"></script>
<script src="../lib/datatables-responsive/pdfmake.min.js"></script>
<script src="../lib/datatables-responsive/vfs_fonts.js"></script>
<script src="../lib/datatables-responsive/buttons.html5.min.js"></script>
<script type="text/javascript">
  var prkUrl = "<?=PRK_URL; ?>";
  var prk_admin_id = "<?=$prk_admin_id; ?>";
  var token = "<?=$token; ?>";
  var parking_admin_name = "<?=$parking_admin_name;?>";
  function datatable_show(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      },
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      buttons: [
        {
          text: '<span id="refresh_button"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
          action: function ( e, dt, node, config ) {
            // alert( 'Button activated' );
            // $('#datatable1').dataTable().fnDestroy();
            meet_to_person_list_loadAjax();
          }
        },
        { 
          extend: 'copyHtml5', 
          //footer: true,
          title: 'Office Member List' 
        },
        { 
          extend: 'excelHtml5', 
          //footer: true,
          title: 'Office Member List'  
        },
        {
          extend: 'pdfHtml5',
          //footer: true,
          title: 'Office Member List',
          customize: function(doc) {
            doc.content[1].margin = [ 60, 0, 0, 0 ]
          }
        },
      ]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    $('#refresh_button').hide();
  }
  function meet_to_person_list_loadAjax(){
    $('#refresh_button').show();
    var current_veh = prkUrl+'meet_to_person_list.php';
    // 'prk_admin_id','parking_admin_name','token'
    $.ajax({
      url :current_veh,
      type:'POST',
      data :{
        'prk_admin_id':prk_admin_id,
        'parking_admin_name':parking_admin_name,
        'token':token
      },
      dataType:'html',
      success  :function(data){
        // alert(data);
        var demoLines = '';
        var json = $.parseJSON(data);
        $('#datatable1').dataTable().fnDestroy();
        if (json.status){
          var c = 0;
          for (var key in json.meet_to_person_list) {
            c +=1;
            var keyy = json.meet_to_person_list[key];
            demoLines += '<tr>\
              <td>'+c+'</td>\
              <td>'+keyy['me_to_per_name']+'</td>\
              <td>'+keyy['me_to_per_mobile']+'</td>\
              <td>'+keyy['me_to_per_department']+'</td>\
              <td>'+keyy['eff_date']+'</td>\
              <td>'+keyy['end_date']+'</td>\
              <td><a href="meet-to-person?list_id='+keyy['me_to_per_id']+'" data-toggle="tooltip" data-placement="top" title="Modify"><i class="fa fa-edit" style="font-size:18px"></i></a>\
              <button type="button" class="clean-button" name="delete" id="'+keyy['me_to_per_id']+'" onclick=" meet_to_person_list_delete(this.id)" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button></td>\
              </tr>';
          }
        }
        $("#list_ve").html(demoLines);
        datatable_show();
      }
    });
  }
  function meet_to_person_list_delete(me_to_per_id) {
    // alert(me_to_per_id);
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the item permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            if (me_to_per_id != '') {
              var urlDtl = prkUrl+'meet_to_person_delete.php';
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :{
                  'me_to_per_id':me_to_per_id,
                  'prk_admin_id':prk_admin_id,
                  'parking_admin_name':parking_admin_name,
                  'token':token
                },
                dataType:'html',
                success  :function(data){
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Delete successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                          // location.reload(true);
                          meet_to_person_list_loadAjax();
                        }
                      }
                    });
                  }else{
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>"+json.message+"</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
  meet_to_person_list_loadAjax();
</script>
