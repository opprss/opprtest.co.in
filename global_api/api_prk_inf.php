<?php 
/*
Description: user vehicle out QR code scan and notify show parking employ .
Developed by: Rakhal Raj Mandal
Created Date: 25-05-2018
Update date : -------
*/ 
function prk_inf($prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql="SELECT `prk_area_admin`.`prk_admin_name`,
        `prk_area_admin`.`park_pay`,
        `prk_area_admin`.`account_number`,
        `prk_area_admin`.`payment_dtl_img`,
        `prk_area_admin`.`prient_in_flag`,
        `prk_area_admin`.`prient_out_flag`,
        `prk_area_admin`.`advance_flag`,
        `prk_area_admin`.`advance_pay`,
        `prk_area_admin`.`ad_flag`,
        `prk_area_admin`.`sms_flag`,
        `prk_area_admin`.`t_c_flag`,
        `prk_area_admin`.`veh_out_otp_flag`,
        `prk_area_admin`.`veh_token_flag`,
        `prk_area_admin`.`veh_valid_flag`,
        `prk_area_admin`.`parking_type`,
        `prk_area_admin`.`visitor_token_flag`,
        `prk_area_admin`.`visitor_out_verify_flag`,
        `prk_area_admin`.`veh_out_verify_flag`,
        `prk_area_admin`.`visitor_otp_flag`,
        `prk_area_admin`.`visitor_delay_time`,
        `prk_area_admin`.`alarm_time`,
        `prk_area_admin`.`bb_flag`,
        `prk_area_admin`.`visitor_park_flag`,
        `prk_area_admin`.`visitor_image_flag`,
        `prk_area_admin`.`resident_flag`,
        `prk_area_admin`.`vis_prient_in_flag`,
        `prk_area_admin`.`vis_prient_out_flag`,
        `prk_area_admin`.`office_flag`,
        `prk_area_admin`.`printer_name`,
        `prk_area_dtl`.`prk_area_name`,
        `prk_area_dtl`.`prk_area_short_name`,
        `prk_area_dtl`.`prk_area_display_name`,
        `prk_area_dtl`.`prk_area_logo_img`,
        `apartment_setup`.`last_due_day`,
        `apartment_setup`.`choose_due_day`,
        `apartment_setup`.`flat_sq_ft`,
        `apartment_setup`.`rate_sq_ft`,
        `apartment_setup`.`maint_start_date`,
        `apartment_setup`.`adv_payment_month`,
        `apartment_setup`.`due_payment_month`,
        `apartment_setup`.`late_fine_payable_time`,
        `apartment_setup`.`late_fine_rate_type`,
        `apartment_setup`.`state_mant_date`,
        `apartment_setup`.`late_fine_charrge`
        FROM `prk_area_admin` JOIN `prk_area_dtl` 
        ON `prk_area_dtl`.`prk_admin_id`= `prk_area_admin`.`prk_admin_id`
        AND `prk_area_dtl`.`active_flag` = '".FLAG_Y."'
        AND `prk_area_admin`.`prk_admin_id`= '$prk_admin_id'
        LEFT JOIN `apartment_setup` ON `apartment_setup`.`prk_admin_id`=`prk_area_admin`.`prk_admin_id`
        AND `apartment_setup`.`active_flag`='".FLAG_Y."'
        AND `apartment_setup`.`del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
    	$val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['prk_admin_name'] = $val['prk_admin_name'];
        $response['park_pay'] = $val['park_pay'];
        $response['account_number'] = $val['account_number'];
        $response['payment_dtl_img'] = $val['payment_dtl_img'];
        $response['prient_in_flag'] = $val['prient_in_flag'];
        $response['prient_out_flag'] = $val['prient_out_flag'];
        $response['advance_flag'] = $val['advance_flag'];
        $response['advance_pay'] = $val['advance_pay'];
        $response['ad_flag'] = $val['ad_flag'];
        $response['sms_flag'] = $val['sms_flag'];
        $response['t_c_flag'] = $val['t_c_flag'];
        $response['prk_area_name'] = $val['prk_area_name'];
        $response['prk_area_short_name'] = $val['prk_area_short_name'];
        $response['prk_area_display_name'] = $val['prk_area_display_name'];
        $response['prk_area_logo_img'] = $val['prk_area_logo_img'];
        $response['veh_out_otp_flag'] = $val['veh_out_otp_flag'];
        $response['veh_token_flag'] = $val['veh_token_flag'];
        $response['veh_valid_flag'] = $val['veh_valid_flag'];
        $response['parking_type'] = $val['parking_type'];
        $response['visitor_token_flag'] = $val['visitor_token_flag'];
        $response['visitor_out_verify_flag'] = $val['visitor_out_verify_flag'];
        $response['veh_out_verify_flag'] = $val['veh_out_verify_flag'];
        $response['last_due_day'] = $val['last_due_day'];
        $response['choose_due_day'] = $val['choose_due_day'];
        $response['flat_sq_ft'] = $val['flat_sq_ft'];
        $response['rate_sq_ft'] = $val['rate_sq_ft'];
        $response['maint_start_date'] = $val['maint_start_date'];
        $response['adv_payment_month'] = $val['adv_payment_month'];
        $response['due_payment_month'] = $val['due_payment_month'];
        $response['late_fine_payable_time'] = $val['late_fine_payable_time'];
        $response['late_fine_charrge'] = $val['late_fine_charrge'];
        $response['late_fine_rate_type'] = $val['late_fine_rate_type'];
        $response['state_mant_date'] = $val['state_mant_date'];
        $response['visitor_otp_flag'] = $val['visitor_otp_flag'];
        $response['visitor_delay_time'] = $val['visitor_delay_time'];
        $response['alarm_time'] = $val['alarm_time'];
        $response['bb_flag'] = $val['bb_flag'];
        $response['visitor_park_flag'] = $val['visitor_park_flag'];
        $response['visitor_image_flag'] = $val['visitor_image_flag'];
        $response['resident_flag'] = $val['resident_flag'];
        $response['vis_prient_in_flag'] = $val['vis_prient_in_flag'];
        $response['vis_prient_out_flag'] = $val['vis_prient_out_flag'];
        $response['office_flag'] = $val['office_flag'];
        $response['printer_name'] = $val['printer_name'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
    }
	return json_encode($response); 
}
?>