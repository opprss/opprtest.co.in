<?php
/*
Description: payment area sub unit list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function sub_unit_list($prk_admin_id){
    $allplayerdata = array();
    global $pdoconn;
   /* $sql = "SELECT `prk_sub_unit_name`,`prk_sub_unit_short_name` FROM `prk_sub_unit` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".ACTIVE_FLAG_Y."' AND `del_flag`='".DEL_FLAG_N."'";*/
   $sql = "SELECT * FROM `prk_sub_unit` 
    WHERE `prk_admin_id`='$prk_admin_id' 
    AND `active_flag`='".FLAG_Y."' 
    AND `del_flag`='".FLAG_N."'
    AND  '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(`prk_sub_unit`.`prk_sub_unit_eff_date`, '%d-%m-%Y'), '%Y-%m-%d')    
    AND  DATE_FORMAT(STR_TO_DATE(IFNULL(`prk_sub_unit`.`prk_sub_unit_end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val)
    {
        $response['status'] = 1;
        $response['message'] = 'data ssucessfull';
        $vehicle['prk_sub_unit_name'] = $val['prk_sub_unit_name'];
        $vehicle['prk_sub_unit_short_name'] = $val['prk_sub_unit_short_name'];
        array_push($allplayerdata, $vehicle);
        $response['sub_unit'] = $allplayerdata;

    }
    return json_encode($allplayerdata);
}
?>