<?php
/*
Description: vehicle in parking area the show in details.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
require_once 'api/reg_api.php';
if(isAvailable(array('prk_admin_id'))){
	if(isEmpty(array('prk_admin_id'))){
	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	//global api call
	 	$response = prk_area_address_show($prk_admin_id);
 	}else{
		$response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
 		$response = json_encode($response);
    }
}else{
	$response['status'] = 0; 
	$response['message'] = 'Invalid API Call';
	$response = json_encode($response);
}
echo ($response);
?>

