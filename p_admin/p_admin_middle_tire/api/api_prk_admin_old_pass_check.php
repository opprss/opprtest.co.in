<?php
/*
Description: Parking area login.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_admin_old_pass_check($prk_admin_name,$prk_admin_password){
    if(accountStatusLogin($prk_admin_name)=='true'){
       global $pdoconn;
        $response = array();
        $sql ="SELECT `prk_admin_id` FROM `prk_area_admin` WHERE `prk_admin_name`=:prk_admin_name AND `prk_admin_password`=:prk_admin_password";
        $query  = $pdoconn->prepare($sql);
        $query->execute(array('prk_admin_name'=>$prk_admin_name,'prk_admin_password'=>md5($prk_admin_password)));
        $count=$query->rowCount();
        if($count>0){
            $response['status'] = 1;
            $response['prk_admin_name'] = $prk_admin_password;
            $response['message'] = 'username or password is right';
            $response = json_encode($response);
        }
        else{
            $response['status'] = 0;
            $response['message'] = 'username or password is wrong';
            $response = json_encode($response);
        }
    }else{
        $response=accountStatusLogin($prk_admin_name);
        $response = json_encode($response);
    }
    return $response;
}
?>