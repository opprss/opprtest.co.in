<!-- ################################################
  
Description: Parking area admin can see today's earnings for each employee.
Developed by: Soemen Banerjee
Created Date: 30-03-2018

 ####################################################-->
 <?php 
include "all_nav/header.php"; 
@session_start();
if (isset($_SESSION['prk_admin_id'])) {
  $parking_admin_name=$_SESSION['parking_admin_name'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
    //$prk_admin_id = 431;
    }

?>
<style>
  .size{
    font-size: 11px;
    text-transform: uppercase;
  }
.success{
  font-size: 11px;
  color: green;
}
</style>
<!-- vendor css -->
   
    <link href="../lib/highlightjs/github.css" rel="stylesheet">
    <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">

    <div class="am-mainpanel">
      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Employee Transactions</h5>
        <!--<form id="searchBar" class="search-bar" action="">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>-->
      </div>

      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40" >
          <!-- <h6 class="card-body-title">Add Employee</h6> -->
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form>
            <div class="row mg-b-25">
              <div class="col-lg-3">
                <div class="form-group">
                    <label class="form-control-label size">Choose Employee Name<span class="tx-danger">*</span></label>
                      <div class="select">
                        <select name="payment_rec_emp_name" id="payment_rec_emp_name" style="opacity: 0.8">
                            <option value="all">EMPLOYEE NAME</option>
                            <?php
                                $response = array();
                                $response =prk_sub_user_list($prk_admin_id);
                                //$response = json_decode($response, true);
                                $count = count($response);
                                if ($count == 0) {
                            ?>
                                <option value="NA">NA</option>
                            <?php     
                                }else{
                                  for ($i=0; $i < $count ; $i++) { 
                               ?>
                                  <option value="<?php echo $response[$i]['prk_area_user_id']?>"><?php echo $response[$i]['prk_user_username']?></option>

                                <?php
                              }  }
                            ?>
                        </select>
                      </div>
                      <span style="position: absolute;" id="error_sub_unit_name" class="error_size"></span>
                  </div>
              </div>
              <div class="col-lg-1">
                
              </div>
              <!-- <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">By Payemnt</label>

                  <div class="select" style="">
                    <select name="pay_type" id="pay_type" style="opacity: 0.8">
                        <option value="">ALL</option>
                      <option value="cash">Cash</option>
                      <option value="wallet">Wallet</option>
                      <option value="dig_g_p_p">Digital Gate Pass</option>
                      <option value="tdgp">Tem Digital Gate Pass</option>
                    </select>
                  </div>
                </div>
              </div> -->
            </form>
          </div>
        </div>
        </div>
      </div>   

      <div class="am-pagebody">

        <div class="card pd-20 pd-sm-40">
          <div class="col-md-12">                  
              <div class="list-group widget-list-group" id="list" style="min-height: 200px;">                      
                    <!-- listing -->
                    <div class="ajax-loader text-center">
                     <h5>Please select employee username first </h5>
                  </div>
                    <!-- listing -->
                <!-- <i class="fa fa-times text-center tx-60"></i>
                <h5 class="text-center">You have not done any transection</h5> -->
              </div>

          </div>
        </div><!-- card -->
<!-- footer part -->
<?php include"all_nav/footer.php"; ?>
<style type="text/css">
  .error_size{
  font-size:11px;
  color: red;
}
</style>
<script type="text/javascript">

  $(document).ready(function(){

    var prkUrl = "<?php echo PRK_URL; ?>";
    var imagUrl = "<?php echo OPPR_BASE_URL?>";
     var prk_admin_id = "<?php echo $prk_admin_id ;?>";

      $('#payment_rec_emp_name').change(function(){
        var prk_area_user_id = $('#payment_rec_emp_name :selected').val();
        var payment_rec_emp_name = $('#payment_rec_emp_name :selected').text();
        callAjax(prk_admin_id,payment_rec_emp_name,prk_area_user_id);
      });
      
      function callAjax(prk_admin_id,payment_rec_emp_name,prk_area_user_id){

        start_date ='';
        end_date = '';
        var urluserPaymentHistory = prkUrl+'prk_user_my_trn.php';
        // alert(urluserPaymentHistory);
        // prk_admin_id','payment_rec_emp_name','prk_area_user_id','start_date','end_date'
          $.ajax({
            url :urluserPaymentHistory,
            type:'POST',
            beforeSend: function(){
              $('.ajax-loader').css("visibility", "visible");
            },
            data :
            {
              'prk_admin_id':prk_admin_id,
              'payment_rec_emp_name':payment_rec_emp_name,
              'prk_area_user_id':prk_area_user_id,
              'start_date':start_date,
              'end_date':end_date
            },
            dataType:'json',
            success  :function(data)
            {
              // {"status":1,"message":"Sucessfull","total_veh_all":3,"total_pay_all":200,"payment_trn":[{"payment_type":"CASH","tot_veh":"1","tot_pay":"50"},{"payment_type":"WALLET","tot_veh":"2","tot_pay":"150"}]}
              //alert(data);
              //alert(data.payment_trn[0].payment_type);

              //var a = data.payment_trn[0]['tot_veh'];
              //alert(a);
             /* for (var key in data.payment_trn) {
                   //alert(key);total_pay_all
                   alert(data.payment_trn[key]['payment_type']); 
              }*/
            // alert(data.total_pay_all);
            if (data.payment_trn.length === 0) {
              demoLines = '<h5 style="text-align:center">No Data Found</h5>';
            }else{
              //payment_trn
              ca ='all';
              demoLines = '<div class="list-group-item rounded-top-0">\
                  <div class="media pd--b-20">\
                  <div class="media-body">\
                  <div class="table-responsive table-bordered text-center"">\
                    <table class="table mg-b-0">\
                      <thead class="bg-info">\
                        <tr>\
                          <th class="text-center">Payment Type</th>\
                          <th class="text-center">Total vehicle</th>\
                          <th class="text-center">Total Pay</th>\
                          <th class="text-center">Action</th>\
                        </tr>\
                      </thead>\
                      <tbody>';
              for (var key in data.payment_trn) {
                   // alert(data.payment_trn[key]['payment_type']); 
                demoLines += '<tr>\
                          <td>'+data.payment_trn[key]['payment_type']+'</td>\
                          <td>'+data.payment_trn[key]['tot_veh']+'</td>\
                          <td>'+data.payment_trn[key]['tot_pay']+'</td>\
                          <td>\
                            <a href="prk-emp-payment-report?p_id='+prk_admin_id+'&emp_name='+payment_rec_emp_name+'&type='+data.payment_trn[key]['payment_type']+'&start_date ='+start_date+'&end_date ='+end_date+'" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-info-circle" style="font-size:18px"></i></a>\
                          </td>\
                        </tr>';
                }
                 demoLines += '</tbody>\
                      <thead class="bg-info">\
                        <tr>\
                          <th class="text-center">Payment Type</th>\
                          <th class="text-center">'+data.total_veh_all+'</th>\
                          <th class="text-center">'+data.total_pay_all+'</th>\
                           <td>\
                            <a href="prk-emp-payment-report?p_id='+prk_admin_id+'&emp_name='+payment_rec_emp_name+'&type='+ca+'&start_date ='+start_date+'&end_date ='+end_date+'" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-info-circle" style="font-size:18px"></i></a>\
                          </td>\
                        </tr>\
                      </thead>\
                    </table>\
                  </div>\
                  <p class="pd-t-5">Note: All the above transactions are based on today 00:00 hrs to 24:00 hrs.</p>\
                  </div>\
                  </div>\
                  </div>';
            }
              
              $("#list").html(demoLines);
            },
            complete: function(){
              $('.ajax-loader').css("visibility", "hidden");
            }
          });
      }
    
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>