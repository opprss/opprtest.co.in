<?php 
	include 'api/parkAreaReg_api.php';
	$oppr_gst_number='19AACCO5460H1ZB';
	$oppr_pan_number='AACCO5460H';
	$oppr_bank_account_no='201002420128';
	$oppr_bank_ifsc_code='INDB0000305';
	$oppr_bank_branch_name='Kankurgachi';

	$cus_bill_dtl_id = isset($_REQUEST['list_id']) ? trim($_REQUEST['list_id']) : ''; 
	$cus_bill_show = cus_bill_generate_show($cus_bill_dtl_id);
    $cus_show = json_decode($cus_bill_show);
    $prk_admin_id=$cus_show->prk_admin_id;
    $invoice_number=$cus_show->invoice_number;
    $billing_month=$cus_show->billing_month;
    $date_of_invoice=$cus_show->date_of_invoice;
    $bill_due_date=$cus_show->bill_due_date;
    $bill_period_start_date=$cus_show->bill_period_start_date;
    $bill_period_end_date=$cus_show->bill_period_end_date;
    $bill_amount=$cus_show->bill_amount;
    $discount_amount=$cus_show->discount_amount;
    $tax_value=$cus_show->tax_value;
    $cgst_amount=$cus_show->cgst_amount;
    $sgst_amount=$cus_show->sgst_amount;
    $igst_amount=$cus_show->igst_amount;
    $round_of_amount=$cus_show->round_of_amount;
    $total_invoice_value=$cus_show->total_invoice_value;
    $date_of_invoice=$cus_show->date_of_invoice;

	$prk_inf = prk_inf($prk_admin_id);
	$prk_inf = json_decode($prk_inf, true);
	$prk_area_name = $prk_inf['prk_area_name'];
	$prk_area_short_name = $prk_inf['prk_area_short_name'];
	$prk_area_logo_img = $prk_inf['prk_area_logo_img'];

	$area_address = prk_area_address_show($prk_admin_id);
	$area_address = json_decode($area_address, true);
	$prk_address = $area_address['prk_address'];
	$prk_land_mark = $area_address['prk_land_mark'];
	$prk_add_area = $area_address['prk_add_area'];
	$city_name = $area_address['city_name'];
	$state_name = $area_address['state_name'];
	$prk_add_pin = $area_address['prk_add_pin'];
	$prk_add_country = $area_address['prk_add_country'];
	$address =$prk_address.','.$city_name.','.$state_name;
	$address_pin=$city_name.' - '.$prk_add_pin;

	$cus_dtl_show = prk_area_cus_dtl_show($prk_admin_id);
    $cus = json_decode($cus_dtl_show);
    $kind_alt=$cus->kind_alt;
    $gst_number=$cus->gst_number;
    // $bill_to_add=$cus->bill_to_add;
    // $shipped_to_add=$cus->shipped_to_add;
    $payment_mode=$cus->payment_mode;
    $par_month_rate=$cus->par_month_rate;
    // $tax_type=$cus->tax_type;
    // $bill_date=$cus->bill_date;
    $payment_due_day=$cus->payment_due_day;
    $service_name=$cus->service_name;
    // $discount=$cus->discount;
    $par_month_rate=number_format(($par_month_rate), 2, '.', '');

    $payment_no_of_month=0;
    switch ($payment_mode) {
        case 'M':
            $payment_no_of_month=1;
            break;
        case 'Q':
            $payment_no_of_month=3;
            break;
        case 'Y':
            $payment_no_of_month=12;
            break;
        default:
        $payment_no_of_month=0;
    }
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="../../../img/favicon.png" type="image/x-icon">
	<title>Customer Bill</title>
	<link rel="stylesheet" type="text/css" href="../cus_bill_assets/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="../cus_bill_assets/css/style.css" />
</head>
<body>
	<style type="text/css">
        @media print{
            @page {
               margin-top: 10px;
               margin-bottom: 10px;
               
            }
            body  {
               padding-top: 50px;
               padding-bottom: 20px ;
            }
            .table-bordered th,
			.table-bordered td {
			  border: 2px solid #000 !important;
			}
		    .table-active th {
		        color: #000 !important;
		        background-color: #aba6a6 !important;
		        -webkit-print-color-adjust: exact;
		    }
        }
        @media screen {
			.table-bordered th,
			.table-bordered td {
			  border: 2px solid #000 !important;
			}
		}
        table .tble_brdr{
        	border-right: 1px solid;
        	border-bottom: 1px solid white;
        }
        table .tble_brdr_right{
        	border-right: 1px solid;
        }
        table .tble_brdr_last{
        	border-right: 1px solid;
        	border-bottom: 1px solid;
        }
        table .tble_brdr_top{
        	border-top: 1px solid;
        }
    </style>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="logo" style="margin-top: 20px;">
					<img src="../../../img/logo_pdf.png" style="height: 70px;">

				</div>
			</div>
			<div class="col-md-4">
				<div class="heading text-center py-3">
					<h2>TAX INVOICE</h2>
				</div>
			</div>			
			<div class="col-md-3 offset-md-1">
				<div class="right-details text-left py-3" style="font-size: 13px;">
					<p class="m-0">[ ] Original for Recipient</p>
					<p class="m-0">[ ] Duplicate for supplier</p>
					<p class="m-0">[ ] Extra Copy</p>
				</div>
			</div>
			<div class="col-md-12">
				<div class="big-heading text-center" style="margin-top: -19px;">
					<h5 class="m-0">OPPR SOFTWARE SOLUTION PVT. LTD.</h5>
					<h5 class="m-0">Krishnapur, Kestopur,Mahisgote 2nd lane,Kolkata - 700102</h5>
				</div>	
			</div>
			<div class="col-md-5">
				<div class="bill-to">
					<p class="m-0 pt-5"><strong>Billed To,</strong></p>
					<ul class="pl-5 ml-3" style="margin-left: -1rem!important;">
						<li><strong><?php echo ucwords(strtolower($prk_area_name))?></strong></li>
						<li><?php echo ucwords(strtolower($address))?></li>
						<li><?php echo ucwords(strtolower($address_pin))?></li>
						<li><?php echo ('Kind Att : <b>'.ucwords(strtolower($kind_alt)).'</b>')?></li>
						<li>GSTIN/Unique ID: <strong><?php echo $gst_number?></strong></li>
					</ul>
				</div>
			</div>
			<div class="col-md-4">
				<div class="bill-to mt-5">
					<p class="m-0 pt-5"><strong>Shipped To,</strong></p>
					<ul class="pl-5 ml-3" style="margin-left: -1rem!important;">
						<li><strong><?php echo ucwords(strtolower($prk_area_name))?></strong></li>
						<li><?php echo ucwords(strtolower($address))?></li>
						<li><?php echo ucwords(strtolower($address_pin))?></li>
					</ul>
				</div>
			</div>
			<div class="col-md-3" style="margin-top: 23px;">
				<div class="bill-to" style="margin-left: -50px;">
					<ul class=" pl-0">
						<li>Invoice No.:  <strong style="font-size: 13px;"><?=$invoice_number ?></strong></li>
						<li>Billing Month:  <strong><?=$billing_month;?></strong></li>
						<li>Date of Invoice:  <strong><?=$date_of_invoice;?></strong></li>
						<li>Bill Due Date:  <strong><?=$bill_due_date;?></strong></li>
						<li>GST No:  <strong><?=$oppr_gst_number;?></strong></li>
						<li>PAN No:  <strong><?=$oppr_pan_number;?></strong></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered mb-0 table-hover table-responsive mt-3">
					<thead>
						<tr class="table-active">
							<th class="no-w"><p class="text-center m-0" style="font-size: 13px;margin: -7px!important;">Sl. No.</th>
							<th><p class="text-center m-0" style="font-size: 13px;margin: -7px!important;">Description of Services</p></th>
							<th><p class="text-center m-0" style="font-size: 13px;margin: -7px!important;">Rate/Month</p></th>
							<th><p class="text-center m-0" style="font-size: 13px;margin: -7px!important;">No. of Months</p></th>
							<th><p class="text-center m-0" style="font-size: 13px;margin: -7px!important;">Total (Rs)</p></th>
							<th><p class="text-center m-0" style="font-size: 13px;margin: -7px!important;">Discount Value</p></th>
							<th><p class="text-center m-0" style="font-size: 13px;margin: -7px!important;">Taxable value (Rs.)</p></th>
							<th><p class="text-center m-0" style="font-size: 13px;margin: -7px!important;">CGST @ 9%</p></th>
							<th><p class="text-center m-0" style="font-size: 13px;margin: -7px!important;">SGST @ 9%</p></th>
							<th><p class="text-center m-0" style="font-size: 13px;margin: -7px!important;">IGST @ 18%</p></th>
						</tr>
					</thead>
					<tbody>
						<tr style="border-bottom-style: hidden;">
							<td>1</td>
							<td style="font-size: 13px;">
								<?= ucwords(strtolower($service_name));?>
							</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						
						<tr >
							<td></td>
							<td class="td-w" style="font-size: 13px;"><strong>For The Period Of: <?=$bill_period_start_date.' To '.$bill_period_end_date;?></strong></td>
							<td><?=$par_month_rate;?></td>
							<td><?=$payment_no_of_month;?></td>
							<td><?=$bill_amount?></td>
							<td><?=$discount_amount?></td>
							<td><?=$tax_value;?></td>
							<td><?=$cgst_amount?></td>
							<td><?=$sgst_amount?></td>
							<td><?=$igst_amount?></td>
						</tr>
						<tr>
							<td colspan="6" rowspan="4" class="tble_brdr">
								<p class="text-right m-0" style="margin: 7px !important"><strong>Sub Total</strong></p><br>
								<p class="text-right m-0" style="margin: 7px !important">Total Value</p><br>
								<p class="text-right m-0 pt-1" style="margin: 7px !important">R/off</p><br>
								<p class="text-right m-0" style="margin: 7px !important"><strong>Total Invoice Value (In figure)</strong></p>
							</td>
							<td rowspan="1" class="tble_brdr" style="line-height: 12px;"><strong><?=$tax_value."<br>";?></strong><br>

							</td>
							<td class="tble_brdr" style="line-height: 12px;"><strong><?=$cgst_amount."<br>";?></strong></td>
							<td class="tble_brdr" style="line-height: 12px;"><strong><?=$sgst_amount."<br>";?></strong></td>
							<td class="tble_brdr" style="line-height: 12px;"><strong><?=$igst_amount."<br>";?></strong></td>
						</tr>
						<tr>
							<td class="tble_brdr_last"><?=$tax_value."<br>";?></td>
							<td class="tble_brdr_last"><?=$cgst_amount."<br>";?></td>
							<td class="tble_brdr_last"><?=$sgst_amount."<br>";?></td>
							<td class="tble_brdr_last"><?=$igst_amount."<br>";?></td>
						</tr>
						<tr>
							<td colspan="4" class="tble_brdr_last"><?php echo $round_of_amount; ?></td>
							
						</tr>
						<tr>
							<td class="table-active tble_brdr_last" colspan="4" style="color: #000 !important;background-color: #aba6a6 !important;-webkit-print-color-adjust: exact;"><h4 class="m-0"><strong><?=number_format($total_invoice_value,2)."<br>"; ?></strong></h4></td>
						</tr>
						<tr>
							<td colspan="10" class="tble_brdr_top"><h5 style="font-size: 17px;"><strong>#Total Invoice Value (In Words): -</strong> Rupees <?=api_number_to_word($total_invoice_value)?>  </h5></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-8">
				<p class="text-left">* Amount of Tax subject to Reverse Charges not applicable</p>
			</div>
			<div class="col-md-4">
				<p class="text-right"></p>
			</div>
		</div>
	</div>	
	<div class="container-fluid" style="margin-top: 3px;">
		<div class="row">
			<div class="col-md-8">
				<div class="footer-content">
					<h5 style="font-size: 14px;"><strong>Payment Terms</strong></h5>
					<h5 style="font-size: 14px;">1. Payment has to be made within <?=$payment_due_day;?> days from the date of invoice <br>vide  a cheque in favour of OPPR Software Solution Pvt. Ltd. <br>For NEFT below is the bank details</h5>
					<h5 style="font-size: 14px;">Beneficiary Name : OPPR Software Solution Private Limited<br>Account No. : <?=$oppr_bank_account_no;?><br>IFSC Code : <?=$oppr_bank_ifsc_code;?><br>Branch Name : <?=$oppr_bank_branch_name;?></h5>
					<br>
					<h5 style="font-size: 14px;">2. Delayed payment will be charged intrest @ 12% p.a.</h5>
					<h5 style="font-size: 14px;">3. All complaint on this billshould reach us within 5 days of the receipt<br>&nbsp;&nbsp;&nbsp;&nbsp;of the bill, failing which the bill becomes fully payable.</h5>
					<h5 style="font-size: 14px;">4. For any clarification mail to admin@opprss.com</h5>
					<h5 style="font-size: 14px;">5. All disputes to be subject to jurisdiction under kolkata courts.</h5>

				</div>
			</div>
			<div class="col-md-4">
				<div class="footer-content text-right">
					<h5 style="font-size: 14px;"><strong>For, OPPR Software Solution Pvt. Ltd.</strong></h5>					
				</div>
			</div>	
		</div>
	</div>
	<div class="container-fluid" style="margin-top: 3px;">
		<div class="row">
			<div class="col-md-12">
				<div class="d-flex justify-content-between">
					<p></p>
					<p></p>
					<p><strong>(Authorised Signatory)</strong></p>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="../cus_bill_assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="../cus_bill_assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../cus_bill_assets/js/popper.min.js"></script>
	<script
	  src="https://code.jquery.com/jquery-3.3.1.min.js"
	  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	  crossorigin="anonymous">
	</script>
	<script type="text/javascript">
	    $(window).on('load', function() {
	         window.print();
	        setTimeout(function(){window.close();}, 1);
	    });
	</script>
</body>
</html>