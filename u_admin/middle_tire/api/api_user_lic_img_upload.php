<?php  
/*
Description: user licence image upload.
Developed by: Rakhal Raj Mandal
Created Date: 02-04-2018
Update date :02-04-2018
*/
function user_lic_img_upload($user_lic_img, $user_lic_id, $user_admin_id,$mobile){
	global $pdoconn;
	//$mobile=md5($mobile);
    $response = array();
    $PNG_TEMP_DIR ='./../uploades/'.$mobile.'/licence/';
    include "phpqrcode/qrlib.php";
    if (!file_exists($PNG_TEMP_DIR)) {
        $old_mask = umask(0);
        mkdir($PNG_TEMP_DIR, 0777, TRUE);
        umask($old_mask);
    }
	$decode_file = base64_decode($user_lic_img);
	$file= uniqid().'.png';
	$file_dir=$PNG_TEMP_DIR.$file;
    if(file_put_contents($file_dir, $decode_file)){
	    $file='uploades/'.$mobile.'/licence/'.$file;
	    
	    $sql ="UPDATE `user_lic_dtl` SET `user_lic_img`='$file',`user_lic_img_status`='".FLAG_F."' WHERE `user_admin_id`='$user_admin_id' AND `user_lic_id`='$user_lic_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";


	    $query = $pdoconn->prepare($sql);
	    if($query->execute()){
	    	$response['status'] = 1;
	    	$response['user_lic_img'] = $file;
	        $response['message'] = 'Upload Successfully';
	    }else{
	        $response['status'] = 0;
	        $response['message'] = 'Upload Not Successfully';
	    } 
	}else{
        $response['status'] = 0;
        $response['message'] = 'Not Image Upload';
	}
	return json_encode($response); 
}
?>