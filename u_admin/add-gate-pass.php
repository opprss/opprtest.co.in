<!-- header position -->
<?php 
include "all_nav/header.php";
//session_start();

?>
<style>
  .size{
    font-size: 11px;
  }
.error_size{
  font-size: 11px;
  color: gray;

}
.success{
  font-size: 11px;
  color: green;
}
</style>

<!-- header position -->
    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Add New Gate Pass</h5>
        <!--<form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form> search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card pd-20 pd-sm-40 col-md-6">
          <!-- <h6 class="card-body-title">Sign Up Here</h6>
          <p class="mg-b-20 mg-sm-b-30">A form with a label on top of each form control.</p> -->
          
          <div class="row">
            <!-- <dir class="col-md-1">
              
            </dir> -->
            <div class="col-md-12">
              
                <form id="modifyVehicle" method="post" action="">
                  <div class="form-layout">
                    <div class="row mg-b-25">
                      <div class="col-lg-6 error_show">
                        <div class="form-group">
                            <div class="select">
                              <select name="user_veh_type" id="user_veh_type" style="opacity: 0.8">
                                  <option value="">Select Vehicle Type</option>
                                  <?php
                                      $response = array();
                                      $response =vehicleType();
                                      //print_r($response);
                                      $count = count($response);

                                      for ($i=0; $i < $count ; $i++) { 
                                     ?>
                                      <option value="<?php echo $response[$i]['vehicle_Sort_nm']?>"><?php echo $response[$i]['vehicle_type']?></option>

                                      <?php
                                      }
                                  ?>
                              </select>
                            </div>
                            <span style="position: absolute;" id="error_user_veh_type" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-lg-6 error_show">
                        <div class="form-group">
                          <div class="select">
                              <select name="user_vehicle_no" id="user_vehicle_no" style="opacity: 0.8">
                                  <option value="">Vehicle Number</option>
                                  <?php
                                      
                                  $user_id = $_SESSION['user_admin_id'];
                                  //$user_id = 99;
                                  if (isset($_POST['v_type']))
                                      $vehicle = $_POST['v_type'];
                                  else
                                      $vehicle = '';
                                              
                                  if(isset($user_id) ){
                                      $response = array();
                                      $response = vehicleList($user_id,$vehicle);
                                      if(!empty($response)){
                                          $count = count($response);
                                          for ($i=0; $i < $count ; $i++) { 
                                            //echo $response[$i]['user_veh_number']; 
                                    ?>  
                                            <option value="<?php echo $response[$i]['user_veh_number']?>"><?php echo $response[$i]['user_veh_number']?></option>                          
                                      <?php          
                                          }
                                      }else{
                                            //vechicle not found
                                          echo "<option value=''>vechicle not found</option>";
                                         }
                                  }else{
                                    //vechicle not found
                                    echo "<option value=''>vechicle not found</option>";  
                                }

                              ?>
                              </select>
                            </div>
                            <span style="position: absolute;" id="error_user_vehicle_no" class="error_size"></span>                          
                        </div>
                      </div>
                      <div class="col-lg-6 error_show">
                        <div class="form-group">
                            <div class="select">
                              <select name="state" id="state" value="null" style="opacity: 0.8">
                              </select>
                            </div>
                            <span style="position: absolute;" id="error_state" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-lg-6 error_show">
                        <div class="form-group">
                          <div class="select">
                            <select name="city" id="city" value="null" style="opacity: 0.8">
                              <option value="">Select City</option>
                            </select>
                        </div>
                        <span style="position: absolute;" id="error_city" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-lg-6 error_show">
                        <div class="form-group">
                          <div class="select">
                              <select name="parking_area_id" id="parking_area_id" value="null" style="opacity: 0.8">
                              <option value="">Select Parking Area</option>
                            </select>
                            </div>
                            <span style="position: absolute;" id="error_parking_area_id" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text" class="form-control" placeholder="Gate Pass Number" name="user_gate_pass_num" maxlength="10" id="user_gate_pass_num" onkeyup="gatePassUppercase();">
                          <span style="position: absolute;" id="error_user_gate_pass_num" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-lg-6 error_show">
                        <div class="form-group">
                          <input type="text" name="effective_date" id="effective_date" placeholder="Effective date (MM/DD/YYYY)" class="form-control">
                          <span style="position: absolute;" id="error_effective_date" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-lg-6 error_show">
                        <div class="form-group">
                          <input type="text" name="end_date" id="end_date" placeholder="End Date (MM/DD/YYYY)" class="form-control">
                          <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="form-group">
                          <span style="position: absolute;" id="error_all" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="form-group">
                          
                        </div>
                      </div>
                      <div class="col-lg-6 error_show">
                        <div class="form-group">
                          <button type="button" class="btn btn-block btn-secondary" name="skip" id="skip" onclick="document.location.href='gate-pass';">Back</button>
                        </div>
                      </div>

                      <div class="col-lg-6 error_show">
                        <div class="form-group">
                          <button type="button" class="btn btn-block btn-info" name="save" id="save">Save</button>
                        </div>
                      </div>

                    <!-- <div class="form-layout-footer"> -->
                      <!-- <button class="btn btn-info mg-r-5">Submit Form</button> -->
                    
                      <!-- <button class="btn btn-secondary">Cancel</button> -->
                    <!-- </div> --><!-- form-layout-footer -->
                  </div><!-- form-layout -->
                </form>
            </div>
            
          </div>
        </div><!-- card -->
        <!-- /add employee form -->
        <!-- <span class="pd-20"></span> -->

      </div><!-- am-pagebody -->
      

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">

    var userUrl = "<?php echo USER_URL; ?>";


        $( document ).ready( function () {

          /*state*/
          var urlState = userUrl+'state.php';

          $.ajax ({
                type: 'POST',
                url: urlState,
                data: "",
                success : function(data) {
                  //alert(data);
                    var obj=JSON.parse(data);
                     // var xx=obj[0]['e'];
                   // alert(obj);
                   // console.log(obj);
                   var areaOption = "<option value=''>Select State</option>";
                   $.each(obj, function(val, key) {
                    // alert("Value is :" + key['e']);
                    areaOption += '<option value="' + key['sta_id'] + '">' + key['state_name'] + '</option>'
                   });

                $("#state").html(areaOption);
              }
          });

          /*city*/
          $('#state').on("change", function() {
            // alert($("#state").val());
            var id=($("#state").val());
            //alert(id);
             var urlCity = userUrl+'city.php';

             $.ajax ({
                  type: 'POST',
                  url: 'web/city.php',
                  data: {sta_id:id},
                  success : function(data) {
                    //alert(data);
                      var obj=JSON.parse(data);
                       
                     // alert(obj[0]['city_name']);
                     console.log(obj);
                     var areaOption = "<option value=''>Select City</option>";
                     $.each(obj, function(val, key) {
                      // alert("Value is :" + key['e']);
                      areaOption += '<option value="' + key['city_id'] + '">' + key['city_name'] + '</option>'
                      // alert(key['city_name']);
                     });

                    $("#city").html(areaOption);
                    //$( "#city" ).append(areaOption);

                  }
              });

            });

          /*park area list*/
          $('#city').on("change", function() {
              // alert($("#state").val());
              var id = $("#city").val();
              //var id = 5;
              //alert(id);
             var urlPrkAreaList = userUrl+'prk_area_list.php';

               $.ajax ({
                  type: 'POST',
                  url: urlPrkAreaList,
                  data: {city:id},
                  success : function(data) {
                    //alert(data);
                      var obj=JSON.parse(data);
                       
                     // alert(obj[0]['city_name']);
                     console.log(obj);
                     var areaOption = "<option value=''>Select Parking Area</option>";
                     $.each(obj, function(val, key) {
                      // alert("Value is :" + key['e']);
                      areaOption += '<option value="' + key['prk_admin_id'] + '">' + key['prk_area_name'] + '</option>'
                      // alert(key['city_name']);
                     });

                    $("#parking_area_id").html(areaOption);
                    //$( "#city" ).append(areaOption);
                  }
              });

            });

          /*FORM SUBMIT*/
          $('#save').click(function(){

              //alert("save");
              //var user_veh_type = $('#user_veh_type').val();
              var user_vehicle_no = $('#user_vehicle_no').val();
              var parking_area_id = $('#parking_area_id').val();
              var user_gate_pass_num = $('#user_gate_pass_num').val();
              var effective_date = $('#effective_date').val();
              var end_date = $('#end_date').val();
              var user_admin_id = "<?php echo $_SESSION['user_admin_id'] ?>";
              var mobile ="<?php echo $user_mobile ?>";
              //alert(mobile);
              if (user_vehicle_no!='' && parking_area_id!='' &&user_gate_pass_num!='' && effective_date!='' &&end_date!='') {

                var urlUserGatePass = userUrl+'user_gate_pass.php';

                $.ajax({
                      url :urlUserGatePass,
                      type:'POST',
                      data :
                      {
                        'user_gate_pass_num':user_gate_pass_num,
                        'parking_area_id':parking_area_id,
                        'user_vehicle_no':user_vehicle_no,
                        'effective_date':effective_date,
                        'end_date':end_date,
                        'user_admin_id':user_admin_id,
                        'mobile':mobile,
                      },
                      dataType:'html',
                      success  :function(data)
                      {
                        //alert(data);
                        //if (data = true) {
                          window.location='gate-pass';
                        //}else{
                         // alert("some problem in save");
                        //}
                      }
                  });
              
              }else{
                $('#error_all').text('ALL ARE REQUIRED');
              }
              
          });

           

      } );


        /* end velidation*/

        /*date*/
        addEventListener('DOMContentLoaded', function () {

          var now = new Date;
          //alert(now);
          pickmeup('#effective_date', {
            default_date   : false,
            position       : 'bottom',
            hide_on_select : true,
            render : function (date) {
                  if (date < now) {
                      return {
                        disabled : true,
                        class_name : 'date-in-past'
                      };
                  }
                  return {};
              } 
          });

          pickmeup('#end_date', {
                  default_date   : false,
                  position       : 'top',
                  hide_on_select : true,
                  render : function (date) {
                        if (date < now) {
                            return {
                              disabled : true,
                              class_name : 'date-in-past'
                            };
                        }
                        return {};
                    } 
                });
        });


        function gatePassUppercase() {
          var x = document.getElementById("user_gate_pass_num");
          x.value = x.value.toUpperCase();
        }

    </script>