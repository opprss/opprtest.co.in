<?php
/*
Description: all city name show state id respect. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function city($sta_id){
    $allplayerdata = array();
    $response = array();
    $vehicle = array();
    global $pdoconn;
    $sql = "SELECT `city_name`,`e_id` FROM `city` WHERE `state_id`='$sta_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val) 
    {
        $city_name=$val['city_name'];
        $city_id=$val['e_id'];
        $response['status'] = 1;
        $response['message'] = 'Successfully';
        $vehicle['city_id'] = $city_id;
        $vehicle['city_name'] = $city_name;
        array_push($allplayerdata, $vehicle);
        $response['city'] = $allplayerdata;

    }
    return json_encode($response);
}
?>