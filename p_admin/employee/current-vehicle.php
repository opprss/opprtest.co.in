<!-- ################################################
  
Description: 
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
 
<?php include "all_nav/header.php";?>
<style type="text/css">
  .size{
    font-size: 11px;
    text-transform: uppercase;
  }
  .error_size,.error{
    font-size: 11px;
    color: red;
  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
  th,tr{
    text-align: center;
  }
</style> 
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="../../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Current Vehicle</h5>
  </div>
</div>
<div class="am-mainpanel">
  <div class="am-pagebody" id="data">
    <div class="card pd-20 pd-sm-40" style="padding: 10px;">
      <div class="table-wrapper">
        <table id="datatable1" class="table display responsive nowrap" cellspacing="0" width="100%">
          <thead>
          <tr>
            <th class="wd-5p">Type</th>
            <th class="wd-10p">VEHICLE NUMBER</th>
            <th class="wd-10p">Token</th>
            <th class="wd-10p">In date</th>
            <th class="wd-10p">In Time</th>
            <th class="wd-10p text-center">Action</th>
          </tr>
        </thead>
        <tbody id="list_ve"></tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include"all_nav/footer.php"; ?>
<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="../../lib/datatables-responsive/jquery.dataTables.min.js"></script>
<script src="../../lib/datatables-responsive/dataTables.buttons.min.js"></script>
<script src="../../lib/datatables-responsive/jszip.min.js"></script>
<script src="../../lib/datatables-responsive/pdfmake.min.js"></script>
<script src="../../lib/datatables-responsive/vfs_fonts.js"></script>
<script src="../../lib/datatables-responsive/buttons.html5.min.js"></script>
<script>
  $( document ).ready( function () {
    // code...
    current_veh_loadAjax();
  });
  function current_veh_loadAjax(){
    $('#refresh_button').show();
    var current_veh = pkrEmpUrl+'prk_veh_trc_dtl_list.php';
    // 'prk_admin_id','prk_area_user_id','prk_area_gate_id','token'
    $.ajax({
      url :current_veh,
      type:'POST',
      data :{
        'prk_admin_id':prk_admin_id,
        'prk_area_user_id':prk_area_user_id,
        'prk_area_gate_id':prk_area_gate_id,
        'token':token
      },
      dataType:'html',
      success  :function(data){
        // alert(data);
        var demoLines = '';
        var json = $.parseJSON(data);
        $('#datatable1').dataTable().fnDestroy();
        if (json.status){
          var c = 0;
          for (var key in json.parking) {
            c +=1;
            var keyy = json.parking[key];
            var img_url= "<?php echo OPPR_BASE_URL; ?>"+keyy['vehicle_typ_img'];
            demoLines += '<tr>\
              <td> <img src="'+img_url+'" style="height: 40px;"></td>\
              <td>'+keyy['veh_number']+'</td>\
              <td>'+keyy['veh_token']+'</td>\
              <td>'+keyy['prk_veh_in_date']+'</td>\
              <td>'+keyy['prk_veh_in_time']+'</td>\
              <td class="text-center">';
            if(keyy['veh_out_verify_flag'] == 'Y'){
              var dis_val = 'Y';
            }else{
              var dis_val = 'N';
            }
            if(dis_val == 'N'){
              demoLines +='<button style="border: none;" type="button" class="activate pd-l-10 btn btn-link " id="'+keyy['prk_veh_trc_dtl_id']+'" onclick="cu_veh_out(this.id)" data-toggle="tooltip" data-placement="top" ><i class="fa fa-check" id="in_verify_logo" style="font-size:32px;color:green;margin-top:-14px;"></i></button>';
            }else{
              demoLines +='<button style="border: none;" type="button" class="activate pd-l-10 btn btn-link" id="'+keyy['prk_veh_trc_dtl_id']+'" onclick="cu_veh_out(this.id)" data-toggle="tooltip" data-placement="top" disabled><i class="fa fa-check" id="in_verify_logo" style="font-size:32px;color:grey;margin-top:-14px;"></i></button>'; 
            }
            demoLines += '</td>\
              </tr>';
          }
        }
        $("#list_ve").html(demoLines);
        datatable_show();
      }
    });
  }
  function cu_veh_out(prk_veh_trc_dtl_id){
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will Out Verify Vehicle</p>",
      theme: 'modern',
      type: 'green',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Verify',
          btnClass: 'btn-green',
          keys: ['Y', 'shift'],
          action: function(){
            // alert(prk_veh_trc_dtl_id);
            if (prk_veh_trc_dtl_id != '') {
              var urlDtl = pkrEmpUrl+'veh_out_verify.php';
              // 'prk_admin_id','prk_area_user_id','prk_user_username','token','prk_veh_trc_dtl_id'
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :{
                  'prk_veh_trc_dtl_id':prk_veh_trc_dtl_id,
                  'prk_admin_id':prk_admin_id,
                  'prk_area_user_id':prk_area_user_id,
                  'prk_user_username':prk_user_username,
                  'token':token
                },
                dataType:'html',
                success  :function(data){
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    // location.reload(true);
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Vehicle Out Verify successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                            // location.reload(true);
                          current_veh_loadAjax();
                        }
                      }
                    });
                  }else{
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
  function datatable_show(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      language: {
      searchPlaceholder: 'Search Here',
      sSearch: '',
      lengthMenu: '_MENU_ Page',
      },
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      buttons: [
        {
          text: '<span id="refresh_button"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
          action: function ( e, dt, node, config ) {
              // alert( 'Button activated' );
              current_veh_loadAjax();
          }
        },
        { 
          extend: 'copyHtml5', 
          //footer: true,
          title: 'CURRENT VEHICLE SHEET' 
        },
        { 
          extend: 'excelHtml5', 
          //footer: true,
          title: 'CURRENT VEHICLE SHEET'  
        },
        {
          extend: 'pdfHtml5',
          //footer: true,
          title: 'CURRENT VEHICLE SHEET',
          customize: function(doc) {
            doc.content[1].margin = [ 0, 0, 0, 0 ]
          }
        },
      ]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    $('#refresh_button').hide();
  }
</script>
