<?php
/*
Description: Parking area vehicle rate delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function maintenance_pay($prk_admin_id,$m_account_number,$tower_id,$flat_id,$owner_id,$tenant_id,$amount,$payment_type,$bank_name,$cheque_number,$main_tran_by,$main_tran_rece_by,$inserted_by,$online_payment_dtl_id,$orderid,$oth1,$oth2,$oth3,$oth4,$oth5){
    global $pdoconn;
    $response = array();
    $id=uniqid();
    $sql = "INSERT INTO `maintenance_pay`(`main_pay_id`, `prk_admin_id`, `m_account_number`, `tower_id`, `flat_id`, `owner_id`, `tenant_id`, `amount`, `payment_type`, `bank_name`, `cheque_number`, `main_tran_by`, `main_tran_rece_by`, `main_pay_date`, `inserted_by`, `inserted_date`, `online_payment_dtl_id`, `orderid`) VALUES ('$id','$prk_admin_id','$m_account_number','$tower_id','$flat_id','$owner_id','$tenant_id','$amount','$payment_type','$bank_name','$cheque_number','$main_tran_by','$main_tran_rece_by','".TIME."','$inserted_by','".TIME."','$online_payment_dtl_id','$orderid')";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $main_pay_id = $id;
        $payment_head = 'Maintenance Pay';
        $amount = $amount;
        $payment_type = 'CR';
        // $main_tran_by = $main_tran_by;
        // $main_tran_rece_by = '';
        flat_account_transaction($prk_admin_id,$m_account_number,$payment_head,$payment_type,$amount,$main_tran_by,$main_tran_rece_by,$inserted_by);
        $crud_type='U';
        $account_due=($amount*(-1));
        flat_account_details($prk_admin_id,$tower_id,$flat_id,$m_account_number,$account_due,$inserted_by,$crud_type);
        $main_pay_bill_pdf=main_pay_bill_pdf_mail($prk_admin_id,$main_pay_id,$owner_id,$tenant_id);
        $response['status'] = 1;
        $response['message'] = 'Sucessfull';
        $response['main_pay_id'] = $main_pay_id;
        $response['main_pay_bill_pdf'] = $main_pay_bill_pdf;
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Unsucessfull';
        return json_encode($response);
    }
}
?>