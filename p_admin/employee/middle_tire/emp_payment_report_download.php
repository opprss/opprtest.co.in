<?php 
/*
Description: Parking employ Payment report download
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
include_once 'api/parkAreaReg_api.php';
// include_once 'api/global_api.php';

 $response = array();
 if(isAvailable(array('prk_admin_id','payment_rec_emp_name','start_date','end_date','payment_type'))){
 	if(isEmpty(array('prk_admin_id','payment_rec_emp_name','start_date','end_date','payment_type'))){

	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$payment_rec_emp_name = $_POST['payment_rec_emp_name'];
        $start_date =$_POST['start_date'];
        $end_date =$_POST['end_date'];
        $payment_type =$_POST['payment_type'];
        
        $response= emp_payment_report_download($prk_admin_id,$payment_rec_emp_name,$start_date,$end_date,$payment_type);
            //echo ($response);
            $json = json_decode($response);
            if($json->status=='1'){
            emp_pay_pdf_dwn($response);
            }
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response);
 }
?>