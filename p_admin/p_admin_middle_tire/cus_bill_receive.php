<?php
/*
Description: Parking area login.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
if(isAvailable(array('cus_bill_dtl_id','payment_type','payment_receive_amount','payment_receive_by','inserted_by','bank_name','bank_branch_name','beneficiary_name','cheque_number','online_payment_dtl_id'))){
    if(isEmpty(array('cus_bill_dtl_id','payment_type','payment_receive_amount','payment_receive_by','inserted_by'))){
        $cus_bill_dtl_id = trim($_POST['cus_bill_dtl_id']);
        $payment_type = trim($_POST['payment_type']);
        $payment_receive_amount = trim($_POST['payment_receive_amount']);
        $payment_receive_by = trim($_POST['payment_receive_by']);
        $inserted_by = trim($_POST['inserted_by']);
        $bank_name = trim($_POST['bank_name']);
        $bank_branch_name = trim($_POST['bank_branch_name']);
        $beneficiary_name = trim($_POST['beneficiary_name']);
        $cheque_number = trim($_POST['cheque_number']);
        $online_payment_dtl_id = trim($_POST['online_payment_dtl_id']);
        $response = cus_bill_receive($cus_bill_dtl_id,$payment_type,$payment_receive_amount,$payment_receive_by,$inserted_by,$bank_name,$bank_branch_name,$beneficiary_name,$cheque_number,$online_payment_dtl_id);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandator';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>