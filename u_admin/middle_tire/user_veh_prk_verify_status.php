<?php 
/*
Description: user vehicle verify parking employ.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
 // require_once '../../connection-pdo.php';
require_once 'api/reg_api.php';
// require_once 'api/global_api.php';
$response = array();
if(isAvailable(array('user_prk_veh_varify_id'))){

 	if(isEmpty(array('user_prk_veh_varify_id'))){

	 	$user_prk_veh_varify_id = trim($_POST['user_prk_veh_varify_id']);
	 	$response= user_veh_prk_verify_status($user_prk_veh_varify_id);
	 	echo ($response);

 	}else{
 			$response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response); 
	 }
 }else{
	 $response['status'] = 0; 
	 $response['message'] = 'Invalid API Call';
     echo json_encode($response); 
 } 
?>