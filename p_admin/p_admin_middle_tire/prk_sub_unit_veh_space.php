<?php 
/*
Description: Parking area sub unit vehicle space insert.
Developed by: Rakhal Raj Mandal
Created Date: 05-05-2018
Update date : -------
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('prk_sub_unit_id','prk_admin_id','veh_type','veh_space','prk_sub_unit_veh_space_eff_date','prk_sub_unit_veh_space_end_date','user_name','token'))){
 	if(isEmpty(array('prk_sub_unit_id','prk_admin_id','veh_type','veh_space','prk_sub_unit_veh_space_eff_date','prk_sub_unit_veh_space_end_date','user_name','token'))){

	 	$prk_sub_unit_id = $_POST['prk_sub_unit_id'];
	 	$prk_admin_id = $_POST['prk_admin_id'];
	 	$veh_type = $_POST['veh_type'];
	 	$veh_space = $_POST['veh_space'];
	 	$prk_sub_unit_veh_space_eff_date = trim($_POST['prk_sub_unit_veh_space_eff_date']);
	 	$prk_sub_unit_veh_space_end_date = trim($_POST['prk_sub_unit_veh_space_end_date']);
	 	$user_name = trim($_POST['user_name']);
	 	$token = ($_POST['token']);
	 	$resp=prk_token_check($prk_admin_id,$token);
	 	 $json = json_decode($resp);
	 	 if($json->status){
        $response=prk_sub_unit_veh_space($prk_sub_unit_id,$prk_admin_id,$veh_type,$veh_space,$prk_sub_unit_veh_space_eff_date,$prk_sub_unit_veh_space_end_date,$user_name);
        echo $response;
        }else{
        	echo $response=$resp;
        }

 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            // $response = json_encode($response);
            echo json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 	// $response = json_encode($response);
}
// echo $response;
?>