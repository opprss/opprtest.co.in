<?php
/*
Description: Parking adimn gate pass genaret for user.
Developed by: Rakhal Raj Mandal
Created Date: 31-02-2018
Update date : 25-05-2018
*/ 
function gate_pass_generate($prk_admin_id,$prk_sub_unit_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    #-------------------#
    $sql = "SELECT `prk_sub_unit_short_name` FROM `prk_sub_unit` WHERE `prk_sub_unit_id`='$prk_sub_unit_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $prk_sub_unit_short_name= $val['prk_sub_unit_short_name'];
    #-------------------#
    $sql = "SELECT `prk_area_admin`.`prk_admin_id`,`prk_area_admin`.`prk_admin_name`,`prk_area_address`.`prk_add_pin`
        FROM `prk_area_admin`, `prk_area_address`
        where `prk_area_address`.`active_flag`='".FLAG_Y."'
        AND `prk_area_address`.`del_flag`='".FLAG_N."'
        AND `prk_area_admin`.`prk_admin_id` = `prk_area_address`.`prk_admin_id`
        AND `prk_area_address`.`prk_admin_id`='$prk_admin_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $prk_admin_name= $val['prk_admin_name'];
    $prk_add_pin = $val['prk_add_pin'];

    $pieces = explode("_", $prk_admin_name);//$pieces[0]
    if($prk_sub_unit_short_name == ''){
        $gate=$pieces[0]."-".$prk_add_pin."-";
        $i=2;
    }else{
        $gate=$pieces[0]."-".$prk_sub_unit_short_name."-".$prk_add_pin."-";
        $i=3;
    }
    $sql = "SELECT MAX(`prk_gate_pass_num`) as gate_pass FROM `prk_gate_pass_dtl` WHERE `prk_gate_pass_num` LIKE '$gate%'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $gate_pass= $val['gate_pass'];
    $piec = explode("-", $gate_pass);//$pieces[0]
    if(!empty($gate_pass))
    {
        $gate_pass=$piec[$i]+1;
    }else{
        $gate_pass=1;
    }
    $gate_pass = sprintf("%'.07d", $gate_pass);

    $gate=$gate."".$gate_pass;
    $response['status'] = 1;
    $response['message'] = 'data ssucessfull';
    $response['gate_pass'] = $gate;
    return json_encode($response);
}
?>