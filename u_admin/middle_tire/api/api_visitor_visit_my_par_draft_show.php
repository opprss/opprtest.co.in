<?php
/*
Description: user licence list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function visitor_visit_my_par_draft_show($user_mobile,$prk_visit_dtl_id){
    $response = array();
    $allplayerdata = array();
    $gate_pass = array();
    global $pdoconn;
    $sql = "SELECT `pvd`.`prk_visit_dtl_id`,
        `pvd`.`visit_name`,
        `pvd`.`visit_gender`,
        `pvd`.`visit_mobile`,
        `pvd`.`visit_purpose`,
        `pvd`.`visit_meet_to`,
        `pvd`.`visit_address`,
        `pvd`.`visit_with_veh`,
        `pvd`.`visit_card_id`,
        (CASE pvd.visitor_type WHEN 'O' THEN IF(pvd.visit_img IS NULL or pvd.visit_img = '', 
                (CASE WHEN pvd.visit_gender= '".FLAG_M."' then '".MALE_IMG_D."' WHEN pvd.visit_gender= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".PRK_BASE_URL."',pvd.visit_img))
    
            WHEN 'V' THEN (SELECT IF(vgp.visitor_img IS NULL or vgp.visitor_img = '', (CASE WHEN pvd.visit_gender= '".FLAG_M."' then '".MALE_IMG_D."' WHEN pvd.visit_gender= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".PRK_BASE_URL."',vgp.visitor_img)) FROM visitor_gate_pass_dtl vgp WHERE visitor_gate_pass_dtl_id=pvd.unique_id)
 
            WHEN 'G' THEN IF(pvd.visit_img IS NULL or pvd.visit_img = '', (CASE WHEN pvd.visit_gender= '".FLAG_M."' then '".MALE_IMG_D."' WHEN pvd.visit_gender= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".PRK_BASE_URL."',pvd.visit_img))
 
            WHEN 'U' THEN (SELECT IF(ud.user_img IS NULL or ud.user_img = '', (CASE WHEN pvd.visit_gender= '".FLAG_M."' then '".MALE_IMG_D."' WHEN pvd.visit_gender= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".USER_BASE_URL."',ud.user_img)) FROM user_detail ud where ud.user_admin_id= pvd.unique_id AND ud.active_flag='".FLAG_Y."') 
        end) as 'visit_img',
        `pvd`.`veh_type`,
        `pvd`.`visitor_out_verify_flag`,
        `pvd`.`visitor_in_verify_flag`,
        `pvd`.`no_of_guest`,
        `pvd`.`visitor_meet_to_name`,
        `pvd`.`visitor_meet_to_address`,
        (SELECT `ad`.`full_name` FROM `all_drop_down` `ad` where `ad`.`category`='VIS_L' AND `ad`.`sort_name`= `pvd`.`visitor_type`) as 'visitor_type',
        `pvd`.`visitor_token`,
        `vehicle_type`.`vehicle_type_dec`,
        `pvd`.`veh_number`,
        `prk_area_dtl`.`prk_area_name`,
        DATE_FORMAT(`pvd`.`start_date`,'%d-%m-%Y') AS `visit_in_date`,
        DATE_FORMAT(`pvd`.`start_date`,'%I:%i %p') AS `visit_in_time` 
        FROM `prk_visit_dtl` pvd join `prk_area_dtl` 
        on `pvd`.`prk_admin_id` = `prk_area_dtl`.`prk_admin_id`
        left join `vehicle_type` 
        on `pvd`.`veh_type`  = `vehicle_type`.`vehicle_sort_nm`
        WHERE `pvd`.`visit_meet_to`='$user_mobile'
        AND `pvd`.`prk_visit_dtl_id`='$prk_visit_dtl_id'
        AND `prk_area_dtl`.`active_flag` = '".FLAG_Y."'
        AND `pvd`.`visitor_in_verify_flag`='".FLAG_D."'
        AND `pvd`.`end_date` is NULL 
        ORDER BY 1 desc";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['prk_visit_dtl_id'] = $val['prk_visit_dtl_id'];
        $response['visit_name'] = $val['visit_name'];
        $response['visit_gender'] = $val['visit_gender'];
        $response['visit_mobile'] = $val['visit_mobile'];
        $response['visit_purpose'] = $val['visit_purpose'];
        $response['visit_address'] = $val['visit_address'];
        $response['visit_with_veh'] = $val['visit_with_veh'];
        $response['visit_card_id'] = $val['visit_card_id'];
        $response['visit_img'] = $val['visit_img'];
        $response['veh_type'] = $val['veh_type'];
        $response['visitor_out_verify_flag'] = $val['visitor_out_verify_flag'];
        $response['visitor_in_verify_flag'] = $val['visitor_in_verify_flag'];
        $response['visitor_meet_to_name'] = $val['visitor_meet_to_name'];
        $response['visitor_meet_to_address'] = $val['visitor_meet_to_address'];
        $response['visitor_type'] = $val['visitor_type'];
        $response['visitor_token'] = $val['visitor_token'];
        $response['vehicle_type_dec'] = $val['vehicle_type_dec'];
        $response['veh_number'] = $val['veh_number'];
        $response['visit_in_date'] = $val['visit_in_date'];
        $response['visit_in_time'] = $val['visit_in_time'];
        $response['no_of_guest'] = $val['no_of_guest'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>