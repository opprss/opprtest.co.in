<?php
/*
Description: user draft registration.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function draft_registration_sms($password,$mobile){
    global $pdoconn;
    $result=array();

    $mobile_no=$mobile;
    $text = "Dear User, Your login details for OPPR App. Username: ".$mobile." & password: ".$password.". Please don’t share to any one, it may lead to damage or loss. Further details ".SMS_URL.".";
    mobile_massage($mobile_no,$text);

    $response['status'] = 1;
    $response['message'] = 'SMS Send Sucessfully';
    return json_encode($response);
}
?>