<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','res_mem_name','res_mem_mobile','res_mem_email','res_mem_gender','parking_admin_name','eff_date','end_date','token','crud_type','res_mem_id'))){
    if(isEmpty(array('prk_admin_id','parking_admin_name','token','crud_type'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
        $res_mem_name = trim($_POST['res_mem_name']);
        $res_mem_mobile = trim($_POST['res_mem_mobile']);
        $res_mem_email = trim($_POST['res_mem_email']);
        $res_mem_gender = trim($_POST['res_mem_gender']);

        $inserted_by = trim($_POST['parking_admin_name']);
        $eff_date = trim($_POST['eff_date']);
        $end_date = trim($_POST['end_date']);
        $crud_type = trim($_POST['crud_type']);
        $token = trim($_POST['token']);
        $res_mem_id = trim($_POST['res_mem_id']);
        
        $resp=prk_token_check($prk_admin_id,$token);
            $json = json_decode($resp);
            if($json->status){
            // $res_mem_id=($crud_type=='I')?uniqid():$res_mem_id;
            $oth1=$oth2=$oth3=$oth4=$oth5='';
                $response = resident_member_details($prk_admin_id,$res_mem_name,$res_mem_mobile,$res_mem_email,$res_mem_gender,$inserted_by,$crud_type,$res_mem_id,$eff_date,$end_date,$oth3,$oth4,$oth5);
            }else{
                $response = $resp;
            }
    }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
 }
echo $response;
?>