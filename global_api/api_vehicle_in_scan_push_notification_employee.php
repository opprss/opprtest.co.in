<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function vehicle_in_scan_push_notification_employee($prk_admin_id,$prk_area_gate_id,$veh_number){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
    $sql = "SELECT pugld.`prk_user_gate_login_id`,
        pt.`token_value`,
        pt.`device_token_id`,
        pt.`device_type` 
        FROM `prk_user_gate_login_dtl` pugld, `prk_token` pt
        WHERE pt.`prk_admin_id`=pugld.`prk_admin_id` 
        AND pt.`prk_area_user_id`=pugld.`prk_area_user_id`
        AND pt.`active_flag`='".FLAG_Y."'
        AND pugld.`prk_admin_id`='$prk_admin_id' 
        AND pugld.`prk_area_gate_id`='$prk_area_gate_id'
        AND pugld.`end_date` IS NULL";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $device_token_id =  $val['device_token_id'];
            $device_type =  $val['device_type'];
            if ($device_type=='ANDR') {
                $payload = array();
                // $payload['area_name'] = $prk_area_name;
                // $payload['name'] = $user_family_name;
                $payload['in_date'] = TIME;
                $payload['url'] = 'VEHICLE IN REQUEST';
                $title = 'OPPRSS';
                $message = 'VEHICLE IN REQUEST FOR '.$veh_number.'.';
                $push_type = 'individual';
                $include_image='';
                $clickaction='VEHICLE IN REQUEST';
                $oth1='EMP';
                $oth2='';
                $oth3='';
                $paylo = push_send($title,$message,$push_type,$include_image,$device_token_id,$payload,$clickaction,$oth1,$oth2,$oth3);
            }
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        // $response['tower_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>