<?php 
/*
Description: Parking employ today particular payment type transition
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function  prk_user_payment_type_trn($prk_admin_id,$payment_rec_emp_name,$payment_type,$start_date,$end_date){
    global $pdoconn;
    if(!empty($start_date) && !empty($end_date)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($start_date)) == date($start_date) && date($format, strtotime($end_date)) == date($end_date)) {
            $start_date= $start_date;
            $end_date=  $end_date ;
        } else {
            $start_date = date("Y-m-d", strtotime($start_date));
            $end_date = date("Y-m-d", strtotime($end_date));
        }
    }else{
        DATE;
        $end_date= TIME_TRN;
        $start_date= TIME_TRN;
    }
    $respons= array();
    $resp = array();
    $re = array();
    $allplayerdata = array();
    $total_pay=0;
    $total_veh=0;
    switch ($payment_type) {
        case "all":
            $w2= CASH;
            $w3= WALLET;
            $w4= DGP;
            $w8= TDGP;
            break;
        case CASH:
            $w2= CASH;
            $w3= 'NA';
            $w4= 'NA';
            $w8= 'NA';
            break;
        case WALLET:
            $w2= 'NA';
            $w3= WALLET;
            $w4= 'NA';
            $w8= 'NA';
            break;
        case DGP:
            $w2= 'NA';
            $w3= 'NA';
            $w4= DGP;
            $w8= 'NA';
            break;
        case TDGP:
            $w2= 'NA';
            $w3= 'NA';
            $w4= 'NA';
            $w8= TDGP;
            break;
        default:
            $w2= 'NA';
            $w3= 'NA';
            $w4= 'NA';
            $w8= 'NA';
    }
    $sql="SELECT `prk_area_name` FROM `prk_area_dtl` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $prk_area_name = $val['prk_area_name'];

    $sql = "SELECT  x.prk_veh_type,
                    x.veh_number,
                    x.payment_amount,
                    x.prk_veh_in_date,
                    x.prk_veh_in_time,
                    x.payment_rec_date,
                    x.payment_rec_time,
                    x.payment_type,
                    x.vehicle_type_dec  ,
                    x.vehicle_typ_img,
                    x.prk_area_name
            FROM (
            SELECT  `prk_veh_trc_dtl`.`prk_admin_id`,
                    `prk_area_dtl`.`prk_area_name`,
                    `payment_receive`.`payment_rec_by` as prk_in_rep_name,
                    `prk_veh_trc_dtl`.`prk_veh_type`,
                    `prk_veh_trc_dtl`.`veh_number`,
                    `payment_receive`.`payment_amount` ,
                    DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%d-%b-%Y') AS `prk_veh_in_date`,
                    DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%I:%i %p') AS `prk_veh_in_time`,
                    DATE_FORMAT(`payment_receive`.`payment_rec_date`,'%d-%b-%Y') AS `payment_rec_date`,
                    DATE_FORMAT(`payment_receive`.`payment_rec_date`,'%I:%i %p') AS `payment_rec_time`,
                    (CASE `payment_receive`.`payment_type` WHEN '".CASH."' ThEN '".CASH."' WHEN '".WALLET."' THEN '".WALLET."' WHEN '".TDGP."' THEN '".TDGP_F."' WHEN '".DGP."' THEN '".DGP_F."' WHEN '".PP."' THEN '".CASH."' end) as payment_type,
                    `vehicle_type`.`vehicle_type_dec`,
                    `vehicle_type`.`vehicle_typ_img`
                    FROM `prk_veh_trc_dtl`, `payment_receive`, `vehicle_type`,`prk_area_dtl`
                    WHERE `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id` = `payment_receive`.`prk_veh_trc_dtl_id`
                    AND   `prk_veh_trc_dtl`.`prk_veh_type` = `vehicle_type`.`vehicle_sort_nm`
                    AND   `payment_receive`.`payment_type` IN ('$w2', '$w3')
                    AND   `vehicle_type`.`active_flag`='".FLAG_Y."'
                     AND  `prk_area_dtl`.`prk_admin_id`=`prk_veh_trc_dtl`.`prk_admin_id`
                     AND  `prk_area_dtl`.`active_flag`='".FLAG_Y."'
                    AND DATE_FORMAT(`payment_receive`.`payment_rec_date`,'%Y-%m-%d')  BETWEEN '$start_date' AND '$end_date'
                    UNION         
                    SELECT  `prk_veh_trc_dtl`.`prk_admin_id`,
                    `prk_area_dtl`.`prk_area_name`,
                    `prk_veh_trc_dtl`.`prk_out_rep_name` as prk_in_rep_name,
                    `prk_veh_trc_dtl`.`prk_veh_type`,
                    `prk_veh_trc_dtl`.`veh_number`,
                    0 AS payment_amount ,
                    DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%d-%b-%Y') AS `prk_veh_in_date`,
                    DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%I:%i %p') AS `prk_veh_in_time`,
                    DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%d-%b-%Y') AS `payment_rec_date`,
                    DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%I:%i %p') AS `payment_rec_time`,
                    (CASE `prk_veh_trc_dtl`.`payment_type` WHEN '".CASH."' ThEN '".CASH."' WHEN '".WALLET."' THEN '".WALLET."' WHEN '".TDGP."' THEN '".TDGP_F."' WHEN '".DGP."' THEN '".DGP_F."' WHEN '".PP."' THEN '".CASH."' end) as payment_type,
                    `vehicle_type`.`vehicle_type_dec`,
                    `vehicle_type`.`vehicle_typ_img`
                    FROM `prk_veh_trc_dtl`,  `vehicle_type`,`prk_area_dtl`
                    WHERE `prk_veh_trc_dtl`.`prk_veh_type` = `vehicle_type`.`vehicle_sort_nm`
                     AND   `prk_veh_trc_dtl`.`payment_type` IN ('$w4', '$w8')
                     AND   `vehicle_type`.`active_flag`='".FLAG_Y."'
                     AND  `prk_area_dtl`.`prk_admin_id`=`prk_veh_trc_dtl`.`prk_admin_id`
                     AND  `prk_area_dtl`.`active_flag`='".FLAG_Y."'
                    AND DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%Y-%m-%d')  BETWEEN '$start_date' AND '$end_date'
            ) x
            WHERE    prk_admin_id ='$prk_admin_id'
            AND prk_in_rep_name ='$payment_rec_emp_name'
            ORDER BY ( CASE payment_type WHEN 'CASH' THEN 1 WHEN 'WALLET' THEN 2 WHEN 'DGP' THEN 3 WHEN 'TDGP' THEN 4 END) desc,payment_rec_date desc";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    // $val = $query->fetch();
    // $prk_area_name = $val['prk_area_name'];
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val)
        {
            $total_pay=$val['payment_amount']+$total_pay;
            $total_veh=$total_veh+1;
            $resp['vehicle_type'] =$val['prk_veh_type'];
            $resp['veh_number'] =$val['veh_number'];
            $resp['payment_amount'] =$val['payment_amount'];
            $resp['prk_veh_in_date'] =$val['prk_veh_in_date'];
            $resp['prk_veh_in_time'] =$val['prk_veh_in_time'];
            $resp['payment_rec_date'] =$val['payment_rec_date'];
            $resp['payment_rec_time'] =$val['payment_rec_time'];
            $resp['payment_type'] =$val['payment_type'];
            $resp['vehicle_typ_img'] =$val['vehicle_typ_img'];
            $resp['vehicle_type_dec'] =$val['vehicle_type_dec'];
            $re[$val['payment_type']][]= $resp;
        }
        $response['status']=1;
        $response['session'] = 1;
        $response['message'] = 'Successful';
        $response['prk_area_name'] = $prk_area_name;
        $response['total_veh'] = $total_veh;
        $response['total_pay'] = $total_pay;
        $response['start_date'] = $start_date;
        $response['end_date'] = $end_date;
        $response['payment_history'] = $re;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>