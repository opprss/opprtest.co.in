<?php 
/*
Description: parking vehicle out otp check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date : 12-09-2018
*/ 
// require_once 'api/global_api.php';
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_veh_trc_dtl_id','veh_out_otp'))){
    if(isEmpty(array('prk_veh_trc_dtl_id','veh_out_otp'))){
        $prk_veh_trc_dtl_id = trim($_POST['prk_veh_trc_dtl_id']);
        $veh_out_otp = trim($_POST['veh_out_otp']);
        
        $response = veh_out_otp_check($prk_veh_trc_dtl_id,$veh_out_otp);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>