<!-- ################################################
  
Description: Report for each employee (gate man) in daily basis
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
include "all_nav/header.php"; 
if (isset($_SESSION['prk_admin_id'])) {
    $insert_by=$_SESSION['parking_admin_name'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
    $sessionDetails = array();
    $sessionDetails = getSessionDetails();
    //$parking_admin_name = $sessionDetails['parking_admin_name'];
    $prk_area_name = $sessionDetails['prk_area_name'];
}
  $prk_admin_id= $_REQUEST['p_id'];
  $payment_rec_emp_name= $_REQUEST['emp_name'];
  $payment_type= $_REQUEST['type'];
  if (isset( $_REQUEST['start_date'])) {
    $start_date= $_REQUEST['start_date'];
  }else{
    $start_date='';
  }
  if (isset( $_REQUEST['end_date'])) {
    $end_date= $_REQUEST['end_date'];
  }else{
    $end_date='';
  }
?>
<!-- vendor css -->
   
  <link href="../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
 <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Employee Payment Details</h5>
        <!-- <form id="searchBar" class="search-bar" action="index.html">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form> --><!-- search-bar-->
      </div><!-- am-pagetitle -->
    </div>
        <div class="am-mainpanel">

    <!-- datatable -->

 
    <!-- <div class="am-mainpanel"> -->
      <div class="am-pagebody">

        <div class="card single pd-20 pd-sm-40">
          <div class="row">
            <div class="col-md-2 pd-b-20">
              <button type="button" class="btn btn-block prk_button_skip" name="skip" id="skip" onclick="document.location.href='employee-transections';">BACK</button>
            </div>
            <div class="col-md-2 pd-b-20">
              <form action="<?php echo PRK_EMP_URL ?>prk_user_payment_type_trn_web_pdf" method="post">
                <input type="hidden" name="prk_admin_id" value="<?php echo $prk_admin_id;?>">
                <input type="hidden" name="payment_rec_emp_name" value="<?php echo $payment_rec_emp_name; ?>">
                <input type="hidden" name="payment_type" value="<?php echo $payment_type; ?>">
                <input type="hidden" name="insert_by" value="<?php echo $prk_area_name; ?>">
                <input type="hidden" name="start_date" value="<?php echo $start_date; ?>">
                <input type="hidden" name="end_date" value="<?php echo $end_date; ?>">

                <button type="submit" class="btn btn-block prk_button" name="skip" id="skip">Download</button>
              </form>
              
            </div>
          </div>
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap table-responsive table-bordered">
              <thead>
                <tr>
                  <th class="wd-5p" style="background-color: #00b8d4;">Type</th>
                  <th class="wd-15p" style="background-color: #00b8d4;">Vehicle Number</th>
                  <th class="wd-20p" style="background-color: #00b8d4;">Payment type</th>
                  <th class="wd-20p" style="background-color: #00b8d4;">Payment Receive Date</th>
                  <th class="wd-20p" style="background-color: #00b8d4;">Payment Receive Time</th>
                  <th class="wd-15p" style="background-color: #00b8d4;">total pay</th>
                </tr>
              </thead>
              <tbody>
                <?php 
  				$response = array();
  				if(isset($payment_rec_emp_name) && isset($prk_admin_id) && isset($payment_type)){
				    $start_date ='';
            $end_date = '';
  				  $response= prk_user_payment_type_trn($prk_admin_id,$payment_rec_emp_name,$payment_type,$start_date,$end_date);
  					$response = json_decode($response, true);  

		           $x=1;
		           if (!empty($response['payment_history'])) {

               foreach ($response['payment_history'] as $key => $item) {
                ?>
                <tr class="text-center">
                  <td style="font-weight: bold; font-size: 1.2em; background-color: #C0C0C0;" colspan="6">
                    <?php
                    echo $key;
                    ?>
                  </td>
                </tr>
                <?php
		          foreach ($response['payment_history'][$key] as $key_p => $item) {
	            ?>

                <tr>

                 <td><img src='<?php echo OPPR_BASE_URL.$item['vehicle_typ_img']; ?>' style='width: 40px; height: 40px;'></td>
                 <td><?php echo $item['veh_number']; ?></td>
                 <td>
                  <?php
                   echo $item ['payment_type'];
                  ?>
                    
                  </td>
                 <td><?php echo $item ['payment_rec_date']; ?></td>
                 <td><?php echo $item ['payment_rec_time']; ?></td>
                 <td><?php echo $item ['payment_amount']; ?></td>
                 </tr>
                <?php $x++;  }}?>
                <tr style="text-align: right;">
                    <td colspan="5" style="background-color: #00b8d4; color: #FFF;">
                      <b>Total Vehicle - <?php echo $response['total_veh']; ?></b>
                    </td>
                      <td  style="background-color: #00b8d4; color: #FFF;">
                        <b>Amount - Rs. <?php echo $response['total_pay']; ?></b>
                      </td>
                    </tr>

              <?php }} ?>
         
                </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->
        

    
<!-- datatable -->
<?php include"all_nav/footer.php"; ?>

<style>
  .size{
    font-size: 11px;
  }
.error_size{
  font-size: 11px;
  color: red;

}
.success{
  font-size: 11px;
  color: green;
}
</style>

