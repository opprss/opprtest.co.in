<?php
/*
Description: Parking visiter in list
Developed by: Rakhal Raj Mandal
Created Date: 31-05-2018
Update date : 
*/ 
function balance_sheet($prk_admin_id,$start_date,$end_date){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $resp = array();
    /*$sql = "SELECT 
    `apar_main_tran_id`,
    `tran_id`,
    `total_amount`,
    `payment_type`,
    DATE_FORMAT(`tran_date`,'%d-%b-%Y') AS `tran_date`,
    `tran_flag` 
    FROM `apartment_maintenance_transition` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";*/
    /*$sql="SELECT 
        `apar_main_tran_id`,
        `tran_id`,
        `total_amount`,
        `payment_type`,
        DATE_FORMAT(`tran_date`,'%d-%b-%Y') AS `tran_date`,
        `tran_flag` 
        FROM `apartment_maintenance_transition` 
        WHERE `prk_admin_id`='$prk_admin_id' 
        AND `active_flag`='".FLAG_Y."' 
        AND `del_flag`='".FLAG_N."'
        AND DATE_FORMAT(`tran_date`,'%Y')='$year'
        AND DATE_FORMAT(`tran_date`,'%m')='$month'";*/
    $sql="SELECT 
        `apar_main_tran_id`,
        `tran_id`,
        `total_amount`,
        `payment_type`,
        DATE_FORMAT(`tran_date`,'%d-%b-%Y') AS `tran_date`,
        DATE_FORMAT(`tran_date`,'%Y-%m-%d') AS `tran_dateaaa`,
        `tran_flag` 
        FROM `apartment_maintenance_transition` 
        WHERE `prk_admin_id`='$prk_admin_id' 
        AND `active_flag`='".FLAG_Y."' 
        AND `del_flag`='".FLAG_N."'
        AND DATE_FORMAT(`tran_date`,'%Y-%m-%d') BETWEEN '$start_date' AND '$end_date'";
    $query = $pdoconn->prepare($sql);
    $query->execute();
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $resp['apar_main_tran_id'] =$val['apar_main_tran_id'];
            $resp['tran_id'] =$val['tran_id'];
            $resp['total_amount'] =$val['total_amount'];
            $resp['payment_type'] =$val['payment_type'];
            $resp['tran_date'] =$val['tran_date'];
            $resp['tran_flag'] =$val['tran_flag'];
            array_push($allplayerdata, $resp);
        }
        $response['status']=1;
        $response['message'] = 'Successful';
        $response['balance_sheet_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>