<?php
/*
Description: Parking gate pass update.
Developed by: Rakhal Raj Mandal
Created Date: 31-01-2018
Update date : 27-05-2018
*/ 
function prk_gate_pass_dtl_update($prk_gate_pass_id,$prk_admin_id,$user_admin_id,$veh_type,$veh_number,$prk_sub_unit_id,$mobile,$end_date,$veh_owner_name,$effective_date,$prk_gate_pass_num,$user_name){
    global $pdoconn;
    $response = array();
    $sql="SELECT * FROM prk_gate_pass_dtl WHERE prk_gate_pass_id='$prk_gate_pass_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $prk_gate_pass_id = $val['prk_gate_pass_id'];
    $prk_admin_id= $val['prk_admin_id'];
    $user_admin_id= $val['user_admin_id'];
    $prk_sub_unit_id= $val['prk_sub_unit_id'];
    $veh_type= $val['veh_type'];
    $veh_number= $val['veh_number'];
    $sub_unit_name= $val['sub_unit_name'];
    // $veh_owner_name= $val['veh_owner_name'];
    $mobile= $val['mobile'];
    $effective_date= $val['effective_date'];
    $end_date=empty($end_date)?$val['end_date']:$end_date;
    // $end_date= $val['end_date'];
    $prk_gate_pass_num= $val['prk_gate_pass_num'];
    $tower_id= $val['tower_id'];
    $flat_id= $val['flat_id'];
    $living_status= $val['living_status'];
    $park_lot_no= $val['park_lot_no'];
    $sql ="UPDATE `prk_gate_pass_dtl` SET `active_flag`='".FLAG_N."', `updated_date`='".TIME."',`updated_by`='$user_name' WHERE `prk_admin_id`='$prk_admin_id' AND `prk_gate_pass_id`='$prk_gate_pass_id' AND `active_flag`='".FLAG_Y."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $sql = "INSERT INTO `prk_gate_pass_dtl`(`prk_admin_id`,`user_admin_id`,`veh_type`,`veh_number`,`prk_sub_unit_id`,`mobile`,`end_date`,`veh_owner_name`,`effective_date`,`prk_gate_pass_num`,`inserted_by`,`inserted_date`,`tower_id`,`flat_id`,`living_status`,`park_lot_no`) VALUE ('$prk_admin_id','$user_admin_id','$veh_type','$veh_number','$prk_sub_unit_id','$mobile','$end_date','$veh_owner_name','$effective_date','$prk_gate_pass_num','$user_name','".TIME."','$tower_id','$flat_id','$living_status','$park_lot_no')";
        $query = $pdoconn->prepare($sql);
        if($query->execute()){
            $response['status'] = 1;
            $response['message'] = 'Update sucessfull';
            return json_encode($response);
        }else{
            $response['status'] = $prk_admin_id;
            $response['message'] = 'Update Not Save';
            return json_encode($response);
        }
    }
}
?>