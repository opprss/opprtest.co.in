<?php 
/*
Description: Parking gate update.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('prk_area_gate_id','prk_admin_id','prk_area_gate_name','prk_area_gate_dec','prk_area_gate_landmark','prk_area_gate_type','effective_date','end_date','user_name','token'))){
 	if(isEmpty(array('prk_area_gate_id','prk_admin_id','prk_area_gate_name','prk_area_gate_dec','prk_area_gate_type','effective_date','user_name','token'))){

        $prk_area_gate_id = trim($_POST['prk_area_gate_id']);
	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_area_gate_name = trim($_POST['prk_area_gate_name']);
	 	$prk_area_gate_dec = trim($_POST['prk_area_gate_dec']);
        $prk_area_gate_landmark = trim($_POST['prk_area_gate_landmark']);
	 	$prk_area_gate_type = trim($_POST['prk_area_gate_type']);
        $effective_date = trim($_POST['effective_date']);
        $end_date = trim($_POST['end_date']);
        $user_name = trim($_POST['user_name']);
        $token = trim($_POST['token']);
        
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            $respon=prk_area_gate_dtl_update($prk_area_gate_id,$prk_admin_id,$prk_area_gate_name,$prk_area_gate_dec,$prk_area_gate_landmark,$effective_date,$end_date,$prk_area_gate_type,$user_name);
            echo ($respon); 
        }else{
            $response = $resp;
            echo $response;
        }
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?>