<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function flat_account_transaction_history($prk_admin_id,$m_account_number){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
    $sql = "SELECT `flat_account_tran_id`,
        DATE_FORMAT(`transitions_date`,'%d-%b-%Y') AS `transitions_date`,
        `payment_head`,
        `payment_type`,
        `amount` 
        FROM `flat_account_transaction` 
        WHERE `prk_admin_id`='$prk_admin_id' 
        AND `m_account_number`='$m_account_number' 
        AND `active_flag`='".FLAG_Y."' 
        AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['flat_account_tran_id'] = $val['flat_account_tran_id'];
            $park['transitions_date'] = $val['transitions_date'];
            $park['payment_head'] = $val['payment_head'];
            $park['payment_type'] = $val['payment_type'];
            $park['amount'] = $val['amount'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['tower_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>