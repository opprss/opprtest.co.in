<?php 
/*
Description: vehicle out all dtl .
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function prk_veh_out_dtl($payment_dtl_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
     $sql = "SELECT  `prk_area_dtl`.`prk_area_name`, 
        `prk_area_address`.`prk_add_area`, 
        `prk_area_address`.`prk_add_city`,
        `city`.`city_name`,
        `prk_area_address`.`prk_add_state`,
        `state`.`state_name`,
        `prk_area_address`.`prk_add_country`, 
        `prk_veh_trc_dtl`.`prk_veh_type`,
        `vehicle_type`.`vehicle_type_dec`,
        `vehicle_type`.`vehicle_typ_img`,
        `prk_veh_trc_dtl`.`veh_number`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%D-%M-%Y') AS `veh_in_date`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%I:%i %p') AS `veh_in_time`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%D-%M-%Y') AS `veh_out_date`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%I:%i %p') AS `veh_out_time`,
        `payment_dtl`.`total_hr`,
        `payment_dtl`.`round_hr`,
        `payment_dtl`.`payment_rec_status`,
        `payment_dtl`.`total_pay`,
        `payment_dtl`.`payment_type`
        FROM `prk_area_dtl`, `prk_area_address`, `prk_veh_trc_dtl`,`city`,`state`,`payment_dtl`,`vehicle_type`
        where `prk_veh_trc_dtl`.`prk_admin_id` = `prk_area_dtl`.`prk_admin_id`
        AND `prk_veh_trc_dtl`.`prk_admin_id` = `prk_area_address`.`prk_admin_id`
        AND `payment_dtl`.`prk_admin_id` = `prk_area_address`.`prk_admin_id` 
        AND `city`.`e_id`= `prk_area_address`.`prk_add_city`
        AND `state`.`e`= `prk_area_address`.`prk_add_state`
        AND `prk_area_dtl`.`active_flag`='".FLAG_Y."'
        AND `prk_area_address`.`active_flag`='".FLAG_Y."' 
        AND `prk_area_address`.`del_flag`='".FLAG_N."'
        AND `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id` = `payment_dtl`.`prk_veh_trc_dtl_id`  
       -- AND `payment_dtl`.`pay_dtl_del_flag`='".FLAG_N."' 
        AND `payment_dtl`.`payment_dtl_id` = '$payment_dtl_id'
        AND `vehicle_type`.`vehicle_sort_nm` = `prk_veh_trc_dtl`.`prk_veh_type`
        AND `vehicle_type`.`active_flag` = '".FLAG_Y."'";
    /*$sql="SELECT  `prk_area_dtl`.`prk_area_name`, 
        `prk_area_address`.`prk_add_area`, 
        `prk_area_address`.`prk_add_city`,
        `city`.`city_name`,
        `prk_area_address`.`prk_add_state`,
        `state`.`state_name`,
        `prk_area_address`.`prk_add_country`, 
        `prk_veh_trc_dtl`.`prk_veh_type`,
        `prk_veh_trc_dtl`.`veh_number`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%D-%M-%Y') AS `veh_in_date`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%I:%i %p') AS `veh_in_time`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%D-%M-%Y') AS `veh_out_date`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%I:%i %p') AS `veh_out_time`,
        `payment_dtl`.`total_hr`,
        `payment_dtl`.`round_hr`,
        `payment_dtl`.`payment_rec_status`,
        `payment_dtl`.`total_pay`,
        `payment_dtl`.`payment_type`
        FROM `prk_area_dtl`, `prk_area_address`, `prk_veh_trc_dtl`, `prk_area_rate`,`city`,`state`,`payment_dtl`
        where `prk_veh_trc_dtl`.`prk_admin_id` = `prk_area_dtl`.`prk_admin_id`
        AND `prk_veh_trc_dtl`.`prk_admin_id` = `prk_area_address`.`prk_admin_id`
        AND `prk_area_rate`.`prk_admin_id` = `prk_area_address`.`prk_admin_id` 
        AND `prk_area_rate`.`prk_vehicle_type` = `prk_veh_trc_dtl`.`prk_veh_type`
        AND `city`.`e_id`= `prk_area_address`.`prk_add_city`
        AND `state`.`e`= `prk_area_address`.`prk_add_state`
        AND `prk_area_rate`.`active_flag` = '".ACTIVE_FLAG_Y."'
        AND `prk_area_rate`.`del_flag` = '".DEL_FLAG_N."'
        AND `prk_area_dtl`.`active_flag`='".ACTIVE_FLAG_Y."'
        AND `prk_area_address`.`active_flag`='".ACTIVE_FLAG_Y."' 
        AND `prk_area_address`.`del_flag`='".DEL_FLAG_N."'
        AND `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id` = `payment_dtl`.`prk_veh_trc_dtl_id`  
        AND `payment_dtl`.`pay_dtl_del_flag`='".DEL_FLAG_N."' 
        AND `payment_dtl`.`payment_dtl_id` = '$payment_dtl_id'";*/

    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $response['status'] = 1;
    $response['session'] = 1;
    $response['message'] = 'Successful';
    $response['payment_dtl_id'] = $payment_dtl_id;
    $response['user_nick_name'] = "NICK NAME TESTING";
    $response['prk_area_name'] = $val['prk_area_name'];
    $response['prk_add_area'] = $val['prk_add_area'];
    $response['prk_add_city'] = $val['city_name'];
    $response['prk_add_state'] = $val['state_name'];
    $response['prk_add_country'] = $val['prk_add_country'];
    $response['prk_veh_type'] = $val['prk_veh_type'];
    $response['veh_number'] = $val['veh_number'];
    $response['veh_in_date'] = $val['veh_in_date'];
    $response['veh_in_time'] = $val['veh_in_time'];
    $response['payment_rec_status'] = $val['payment_rec_status'];
    
    $response['veh_out_date'] = $val['veh_out_date'];
    $response['veh_out_time'] = $val['veh_out_time'];
    $response['tot_hr'] = $val['total_hr'];
    $response['round_hr'] = $val['round_hr'];
    $response['total_pay'] = $val['total_pay'];
    $response['payment_type'] = $val['payment_type'];

    return json_encode($response);
}
?>