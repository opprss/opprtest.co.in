<?php
/*
Description: vehicle check 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function vehicle_count($vehicle_number,$user_admin_id){
    global $pdoconn;
    $sql = "SELECT * FROM `user_vehicle_detail` WHERE `user_veh_number`='$vehicle_number' AND `user_admin_id`='$user_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_Y."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $response['status'] = 1;
        $response['message'] = 'Vehicle Already Registered';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Vehicle Not Registered';
    }
    return json_encode($response);
}
?>