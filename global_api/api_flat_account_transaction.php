<?php
    /*
    Description: State select.
    Developed by: Rakhal Raj Mandal
    Created Date: -------
    Update date :30-03-2018
    */
    function flat_account_transaction($prk_admin_id,$m_account_number,$payment_head,$payment_type,$amount,$main_tran_by,$main_tran_rece_by,$inserted_by){
        $response = array();
        global $pdoconn;
        $id=uniqid();
        $sql ="INSERT INTO `flat_account_transaction`(`flat_account_tran_id`, `prk_admin_id`, `m_account_number`, `transitions_date`, `payment_head`, `payment_type`, `amount`, `main_tran_by`, `main_tran_rece_by`, `inserted_by`, `inserted_date`) VALUES ('$id','$prk_admin_id','$m_account_number','".TIME."','$payment_head','$payment_type','$amount','$main_tran_by','$main_tran_rece_by','$inserted_by','".TIME."')";
        $query  = $pdoconn->prepare($sql);
        if($query->execute()){
            $response['status'] = 1;
            $response['message'] = 'Successful';
        }else{
            $response['status'] = 0;
            $response['message'] = 'Not Successful';
        }
        return json_encode($response);
    }
?>