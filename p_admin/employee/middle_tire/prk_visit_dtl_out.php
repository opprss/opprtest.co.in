<?php
/*
Description: parking area visit the people out. 
Developed by: Rakhal Raj Mandal
Created Date: 09-04-2018
Update date : 27-05-2018
*/
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
$response = array();
$resp = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','token','prk_visit_dtl_id','prk_user_username','prk_area_gate_id'))){
 	if(isEmpty(array('prk_admin_id','prk_area_user_id','token','prk_visit_dtl_id','prk_user_username','prk_area_gate_id'))){ 	

	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_area_user_id = trim($_POST['prk_area_user_id']);
        $token = trim($_POST['token']);
        $prk_visit_dtl_id = trim($_POST['prk_visit_dtl_id']);
        $prk_user_username = trim($_POST['prk_user_username']);
        $prk_area_gate_id = trim($_POST['prk_area_gate_id']);
        $remarks = isset($_POST['remarks']) ? trim($_POST['remarks']) : ''; 
        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
		if($json->status=='1'){
        	$response = prk_visit_dtl_out($prk_visit_dtl_id,$prk_user_username,$prk_area_gate_id,$prk_area_user_id,$remarks);
        	// echo $response;
        }else{
            $response = $resp;
        }
 	}else{
            $response['status'] = 0;
	    	$response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
/*
Description: parking area visit the people out. 
Developed by: Rakhal Raj Mandal
Created Date: 09-04-2018
Update date : -------

require_once 'api/parkAreaReg_api.php';
require_once 'api/global_api.php';
$response = array();
$resp = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','token','prk_visit_dtl_id','prk_user_username'))){
 	if(isEmpty(array('prk_admin_id','prk_area_user_id','token','prk_visit_dtl_id','prk_user_username'))){ 	

	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_area_user_id = trim($_POST['prk_area_user_id']);
        $token = trim($_POST['token']);
        $prk_visit_dtl_id = trim($_POST['prk_visit_dtl_id']);
        $prk_user_username = trim($_POST['prk_user_username']);

        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
		if($json->status=='1'){
        	$response = prk_visit_dtl_out($prk_visit_dtl_id,$prk_user_username);
        	echo $response;
        }else{
            echo $resp;
        }
 	}else{
            $response['status'] = 0;
	    	$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response);
}*/
?>