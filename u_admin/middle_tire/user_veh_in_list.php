<?php 
/*
Description: user licence list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
 require_once 'api/reg_api.php';
// include_once 'api/global_api.php'; 
$response = array();
if(isAvailable(array('user_admin_id','token'))){
 	if(isEmpty(array('user_admin_id','token'))){
	 	$user_admin_id = trim($_POST['user_admin_id']);

	 	$token = ($_POST['token']);
        $user_to=user_token_check($user_admin_id,$token);
        $json = json_decode($user_to);
        if($json->status=='1'){
            $response = user_veh_in_list($user_admin_id);
        }else{
            // $response['status'] = 0; 
            // $response['session'] = 0;
            // $response['message'] =  'Session Expired Please Login Again';
            // $response = json_encode($response);
            $response = $user_to;
        }
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call'; 
    $response = json_encode($response);
 }
echo ($response);
?>