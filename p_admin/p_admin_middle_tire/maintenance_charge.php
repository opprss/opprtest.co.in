<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2019
*/ 
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','maint_type','maint_charge','payable_time','holder_type','effective_date','end_date','parking_admin_name','token','crud_type','maint_id'))){
    if(isEmpty(array('prk_admin_id','parking_admin_name','token','crud_type'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
        $maint_type = trim($_POST['maint_type']);
        $maint_type_short = isset($_POST['maint_type_short']) ? trim($_POST['maint_type_short']) : '';
        $maint_charge = trim($_POST['maint_charge']);
        $payable_time = trim($_POST['payable_time']);
        $holder_type = trim($_POST['holder_type']);
        $effective_date = trim($_POST['effective_date']);
        $end_date = trim($_POST['end_date']);
        $parking_admin_name = trim($_POST['parking_admin_name']);
        $token = trim($_POST['token']);
        $crud_type = trim($_POST['crud_type']);
        $maint_id = trim($_POST['maint_id']);
        $resp=prk_token_check($prk_admin_id,$token);
            $json = json_decode($resp);
            if($json->status){
                $response = maintenance_charge($prk_admin_id,$maint_type,$maint_type_short,$maint_charge,$payable_time,$holder_type,$effective_date,$end_date,$parking_admin_name,$crud_type,$maint_id);
            }else{
                $response = $resp;
            }
    }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
 }
echo $response;
?>