<?php
  session_start();
  include "all_nav/header.php";
  function dropdown_list(){
    $html=" <option value=''>Select Option</option><option value='Y'>YES</option><option value='N'>No</option>";
    return $html;
    # code...
  }
  if (isset($_REQUEST["prk_admin_id"])) {
    $prk_admin_id= trim($_REQUEST["prk_admin_id"]);
    if(empty($prk_admin_id)){
     header('Location: prk-list.php');
    }
  }else{

    header('Location: prk-list');
  }
  $admin_dtl_show = p_admin_dtl_show($prk_admin_id);
  $admin_dtl_sh = json_decode($admin_dtl_show);
  $prient_in_flag=$admin_dtl_sh->prient_in_flag;
  $prient_out_flag=$admin_dtl_sh->prient_out_flag;
  $advance_flag=$admin_dtl_sh->advance_flag;
  $advance_pay=$admin_dtl_sh->advance_pay;
  $ad_flag=$admin_dtl_sh->ad_flag;
  $sms_flag=$admin_dtl_sh->sms_flag;
  $veh_out_otp_flag=$admin_dtl_sh->veh_out_otp_flag; 
  $veh_token_flag=$admin_dtl_sh->veh_token_flag;
  $visitor_token_flag=$admin_dtl_sh->visitor_token_flag;
  $veh_valid_flag=$admin_dtl_sh->veh_valid_flag;
  $parking_type=$admin_dtl_sh->parking_type;
  $visitor_out_verify_flag=$admin_dtl_sh->visitor_out_verify_flag;
  $veh_out_verify_flag=$admin_dtl_sh->veh_out_verify_flag;
  $prk_area_name=$admin_dtl_sh->prk_area_name;
  $visitor_otp_flag=$admin_dtl_sh->visitor_otp_flag;
  $visitor_delay_time=$admin_dtl_sh->visitor_delay_time;
  $alarm_time=$admin_dtl_sh->alarm_time;
  $bb_flag=$admin_dtl_sh->bb_flag;
  $visitor_park_flag=$admin_dtl_sh->visitor_park_flag;
  $visitor_image_flag=$admin_dtl_sh->visitor_image_flag;
  $resident_flag=$admin_dtl_sh->resident_flag;
  $vis_prient_in_flag=$admin_dtl_sh->vis_prient_in_flag;
  $vis_prient_out_flag=$admin_dtl_sh->vis_prient_out_flag;
  $office_flag=$admin_dtl_sh->office_flag;
  $printer_name=$admin_dtl_sh->printer_name;

  $category_type='PRINTER_TYPE';
  $printer_name1 = common_drop_down_list($category_type);
  $printer_list = json_decode($printer_name1, true);
?>
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
<style type="text/css">
  .size{

    font-size: 11px;
  }
  .select{

    font-size: 13px;
  }
  input::-webkit-input-placeholder {

    font-size: 13px;
  }
</style>
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Permission ( Premises Area Name: <?php echo $prk_area_name; ?>)</h5>
  </div>
</div>
<div class="am-mainpanel">
	<div class="am-pagebody">
    <div class="card pd-20 pd-sm-40" >
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <form role="form" id="addVis" method="post"  enctype="multipart/form-data">
          <div class="row mg-b-25">
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Parking Type<span class="tx-danger"></span></label>
                <div class="select">
                  <select id="parking_type">
                    <option value="">Select Parking Type</option>
                    <option value="R">Residential Parking</option>
                    <option value="C">Commercial Parking</option>
                  </select>
                  <span style="position: absolute;" id="error_parking_type" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Advance Pay<span class="tx-danger"></span></label>
                <div class="select">
                  <select id="advance_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_advance_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Advance Pay <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="advance_pay" placeholder="Advance Pay" id="advance_pay" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                  <span style="position: absolute;" id="error_advance_pay" class="error_size"></span>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Vehicle Print In Flag<span class="tx-danger">*</span></label>
                <div class="select">
                  <select id="prient_in_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_prient_in_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Vehicle Print Out Flag<span class="tx-danger">*</span></label>
                <div class="select">
                  <select id="prient_out_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_prient_out_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Advertise Flag<span class="tx-danger"></span></label>
                <div class="select">
                  <select id="ad_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_ad_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">SMS Flag<span class="tx-danger"></span></label>
                <div class="select">
                  <select id="sms_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_sms_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Vehicle Out OTP<span class="tx-danger"></span></label>
                <div class="select">
                  <select id="veh_out_otp_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_veh_out_otp_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Vehicle Out Token<span class="tx-danger"></span></label>
                <div class="select">
                  <select id="veh_token_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_veh_token_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Vehicle Number validate<span class="tx-danger"></span></label>
                <div class="select">
                  <select id="veh_valid_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_veh_valid_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Vehicle Out Verify<span class="tx-danger"></span></label>
                <div class="select">
                  <select id="veh_out_verify_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_veh_out_verify_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Visitor Out Token<span class="tx-danger"></span></label>
                <div class="select">
                  <select id="visitor_token_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_visitor_token_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Visitor Out Verify<span class="tx-danger"></span></label>
                <div class="select">
                  <select id="visitor_out_verify_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_visitor_out_verify_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Visitor OTP Verify<span class="tx-danger"></span></label>
                <div class="select">
                  <select id="visitor_otp_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_visitor_otp_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Visitor Delay Time (minutes)<span class="tx-danger"></span></label>
                <input class="form-control" type="text" name="visitor_delay_time" placeholder="Visitor Delay Time" value="0" id="visitor_delay_time" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                  <span style="position: absolute;" id="error_visitor_delay_time" class="error_size"></span>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Alarm Time (minutes) <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="alarm_time" placeholder="Alarm Time" value="0" id="alarm_time" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                  <span style="position: absolute;" id="error_alarm_time" class="error_size"></span>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Boom Barrier Flag<span class="tx-danger"></span></label>
                <div class="select">
                  <select id="bb_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_bb_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Visiter Parking Flag<span class="tx-danger"></span></label>
                <div class="select">
                  <select id="visitor_park_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_visitor_park_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Visitor Image Flag<span class="tx-danger"></span></label>
                <div class="select">
                  <select id="visitor_image_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_bb_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Resident Flag<span class="tx-danger"></span></label>
                <div class="select">
                  <select id="resident_flag">
                    <option value=''>Select Option</option>
                    <option value="R">Resident</option>
                    <option value="O">Owner</option>
                  </select>
                  <span style="position: absolute;" id="error_resident_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Visitor Print In Flag<span class="tx-danger">*</span></label>
                <div class="select">
                  <select id="vis_prient_in_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_vis_prient_in_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Visitor Print Out Flag<span class="tx-danger">*</span></label>
                <div class="select">
                  <select id="vis_prient_out_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_vis_prient_out_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Office Flag<span class="tx-danger">*</span></label>
                <div class="select">
                  <select id="office_flag">
                    <?php echo dropdown_list(); ?>
                  </select>
                  <span style="position: absolute;" id="error_office_flag" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Printer Name<span class="tx-danger">*</span></label>
                <div class="select">
                  <select id="printer_name">
                    <option value=''>Select Option</option>
                    <?php 
                      foreach ($printer_list['drop_down'] as $key => $value) {
                        # code...
                        echo '<option value="'.$value['sort_name'].'">'.$value['full_name'].'</option>';
                    }?>
                  </select>
                  <span style="position: absolute;" id="error_printer_name" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <!-- <button style="margin-top: 33px;" type="button" class="btn btn-block prk_button" name="visitor_save" id="visitor_save">SAVE</button> -->
                 <input type="button" value="SAVE" style="margin-top: 33px;" class="btn btn-block btn-primary prk_button" name="visitor_save" id="visitor_save">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <button style="margin-top: 33px;" type="reset" class="btn btn-block prk_button">RESET</button>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <button style="margin-top: 33px;" type="button" id="back" class="btn btn-block prk_button">BACK</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>   
<!-- datatable -->
<?php include"all_nav/footer.php"; ?>

<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>

<script>
  var SUP_EMP_URL = "<?php echo SUP_EMP_URL ;?>";
  var employee_user_id = "<?php echo $employee_user_id ;?>";
  var employee_user_name = "<?php echo $employee_user_name ;?>";
  var prk_admin_id = "<?php echo $prk_admin_id ;?>";

  var prient_in_flag = "<?php echo $prient_in_flag ;?>";
  var prient_out_flag = "<?php echo $prient_out_flag ;?>";
  var advance_flag = "<?php echo $advance_flag ;?>";
  var advance_pay = "<?php echo $advance_pay ;?>";
  var ad_flag = "<?php echo $ad_flag ;?>";
  var sms_flag = "<?php echo $sms_flag ;?>";
  var veh_out_otp_flag = "<?php echo $veh_out_otp_flag ;?>";
  var veh_token_flag = "<?php echo $veh_token_flag ;?>";
  var visitor_token_flag = "<?php echo $visitor_token_flag ;?>";
  var veh_valid_flag = "<?php echo $veh_valid_flag ;?>";
  var parking_type = "<?php echo $parking_type ;?>";
  var visitor_out_verify_flag = "<?php echo $visitor_out_verify_flag ;?>";
  var veh_out_verify_flag = "<?php echo $veh_out_verify_flag ;?>";
  var visitor_otp_flag = "<?php echo $visitor_otp_flag ;?>";
  var visitor_delay_time = "<?php echo $visitor_delay_time ;?>";
  var alarm_time = "<?php echo $alarm_time ;?>";
  var bb_flag = "<?php echo $bb_flag ;?>";
  var visitor_park_flag = "<?php echo $visitor_park_flag ;?>";
  var visitor_image_flag = "<?php echo $visitor_image_flag ;?>";
  var resident_flag = "<?php echo $resident_flag ;?>";
  var vis_prient_in_flag = "<?php echo $vis_prient_in_flag ;?>";
  var vis_prient_out_flag = "<?php echo $vis_prient_out_flag ;?>";
  var office_flag = "<?php echo $office_flag ;?>";
  var printer_name = "<?php echo $printer_name ;?>";

  $( document ).ready( function () {
    $('#prient_in_flag').val(prient_in_flag);
    $('#prient_out_flag').val(prient_out_flag);
    $('#advance_flag').val(advance_flag);
    $('#advance_pay').val(advance_pay);
    $('#ad_flag').val(ad_flag);
    $('#sms_flag').val(sms_flag);
    $('#veh_out_otp_flag').val(veh_out_otp_flag);
    $('#veh_token_flag').val(veh_token_flag);
    $('#visitor_token_flag').val(visitor_token_flag);
    $('#veh_valid_flag').val(veh_valid_flag);
    $('#parking_type').val(parking_type);
    $('#visitor_out_verify_flag').val(visitor_out_verify_flag);
    $('#veh_out_verify_flag').val(veh_out_verify_flag);
    $('#visitor_otp_flag').val(visitor_otp_flag);
    $('#visitor_delay_time').val(visitor_delay_time);
    $('#alarm_time').val(alarm_time);
    $('#bb_flag').val(bb_flag);
    $('#visitor_image_flag').val(visitor_image_flag);
    $('#visitor_park_flag').val(visitor_park_flag);
    $('#resident_flag').val(resident_flag);
    $('#vis_prient_in_flag').val(vis_prient_in_flag);
    $('#vis_prient_out_flag').val(vis_prient_out_flag);
    $('#office_flag').val(office_flag);
    $('#printer_name').val(printer_name);
    $('#visitor_save').click(function(){
      // alert('ok');
      var prient_in_flag=$('#prient_in_flag').val();
      var prient_out_flag=$('#prient_out_flag').val();
      var advance_flag=$('#advance_flag').val();
      var advance_pay=$('#advance_pay').val();
      var ad_flag=$('#ad_flag').val();
      var sms_flag=$('#sms_flag').val();
      var veh_out_otp_flag=$('#veh_out_otp_flag').val();
      var veh_token_flag=$('#veh_token_flag').val();
      var visitor_token_flag=$('#visitor_token_flag').val();
      var veh_valid_flag=$('#veh_valid_flag').val();
      var parking_type=$('#parking_type').val();
      var visitor_out_verify_flag=$('#visitor_out_verify_flag').val();
      var veh_out_verify_flag=$('#veh_out_verify_flag').val();
      var visitor_otp_flag=$('#visitor_otp_flag').val();
      var visitor_delay_time=($('#visitor_delay_time').val()=='')?'0':$('#visitor_delay_time').val();
      var alarm_time=($('#alarm_time').val()=='')?'0':$('#alarm_time').val();
      var bb_flag=$('#bb_flag').val();
      var visitor_park_flag=$('#visitor_park_flag').val();
      var visitor_image_flag=$('#visitor_image_flag').val();
      var resident_flag=$('#resident_flag').val();
      var vis_prient_in_flag=$('#vis_prient_in_flag').val();
      var vis_prient_out_flag=$('#vis_prient_out_flag').val();
      var office_flag=$('#office_flag').val();
      var printer_name=$('#printer_name').val();
      // alert(parking_type);
      var urlgate_pass = SUP_EMP_URL+'park_admin_flag_permition.php';
      // alert(urlgate_pass);
      $(this).val('Wait ...').attr('disabled','disabled');
      $.ajax({
        url :urlgate_pass,
        type:'POST',
        data :{
          'prient_in_flag':prient_in_flag,
          'prient_out_flag':prient_out_flag,
          'advance_flag':advance_flag,
          'advance_pay':advance_pay,
          'ad_flag':ad_flag,
          'sms_flag':sms_flag,
          'veh_out_otp_flag':veh_out_otp_flag,
          'veh_token_flag':veh_token_flag,
          'visitor_token_flag':visitor_token_flag,
          'veh_valid_flag':veh_valid_flag,
          'parking_type':parking_type,
          'visitor_out_verify_flag':visitor_out_verify_flag,
          'veh_out_verify_flag':veh_out_verify_flag,
          'employee_user_name':employee_user_name,
          'visitor_otp_flag':visitor_otp_flag,
          'visitor_delay_time':visitor_delay_time,
          'alarm_time':alarm_time,
          'bb_flag':bb_flag,
          'visitor_park_flag':visitor_park_flag,
          'visitor_image_flag':visitor_image_flag,
          'resident_flag':resident_flag,
          'vis_prient_in_flag':vis_prient_in_flag,
          'vis_prient_out_flag':vis_prient_out_flag,
          'office_flag':office_flag,
          'printer_name':printer_name,
          'prk_admin_id':prk_admin_id
        },
        dataType:'html',
        success  :function(data){
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success',
              content: "<p style='font-size:0.9em;'>Permission Set successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                  location.reload(true);
                }
              }
            });
          }else{
            $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.7em;'>Somting went wrong 1</p>",
              type: 'red'
            });
          }
        }
      });
    });
    $('#back').click(function(){
      window.location='prk-list';
    });
  });
</script>