<!-- ################################################
  
Description: It will dispaly list of all current vehicle availabe in the parking area
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
include "all_nav/header.php"; 
if (isset($_SESSION['prk_admin_id'])) {
    $insert_by=$_SESSION['parking_admin_name'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
}
?>
<!-- refresh the page after 30s. -->
<!-- <meta http-equiv="refresh" content="30"/> -->

<!-- vendor css -->
   
  <link href="../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
 <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Payment Pending Details</h5>
        <form id="searchBar" class="search-bar" action="index.html">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form><!-- search-bar-->
      </div><!-- am-pagetitle -->
    </div>
        <div class="am-mainpanel">

    <!-- datatable -->

 
    <!-- <div class="am-mainpanel"> -->
      <div class="am-pagebody">

        <div class="card single pd-20 pd-sm-40">
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-5p">Sl No.</th>
                  <th class="wd-5p">Vehicle Type</th>
                  <th class="wd-20p">Vehicle Number</th>
                  <!-- <th class="wd-15p">In Time</th> -->
                  <th class="wd-10p">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                //$prk_admin_id = 102;
               $respon = prk_payment_undifine($prk_admin_id);
               $respon = json_decode($respon, true);
               
               $x=1;
               $count = count($respon['tran']);
              for ($i=0; $i < $count ; $i++) { 

                $veh_number=$respon['tran'][$i]['veh_number'];
                $vehicle_typ_img=$respon['tran'][$i]['vehicle_typ_img'];
                $payment_dtl_id = $respon['tran'][$i]['payment_dtl_id'];
              ?>

               <tr>
                 <td><?php echo $x; ?></td>
                 <td><img src='<?php echo OPPR_BASE_URL.$vehicle_typ_img; ?>' style='width: 50px; height: 50px;'></td>
                 <td><?php echo $veh_number; ?></td>
                 <td class="text-center">
                  <button type="button" class="out btn prk_button" name="out" id="out<%=count++%>" value="<?php echo $payment_dtl_id; ?>" data-toggle="tooltip" data-placement="top" title="Reset"><i class="fa fa-sign-out" style="font-size:18px"></i> RESET</button>
                 </td>
                 </tr>
                <?php $x++; } ?>
         
                </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->
        

    
<!-- datatable -->
<?php include"all_nav/footer.php"; ?>

<style>
  .size{
    font-size: 11px;
  }
.error_size{
  font-size: 11px;
  color: red;

}
.success{
  font-size: 11px;
  color: green;
}
</style>


<script type="text/javascript">
 

</script>
  <script src="../lib/highlightjs/highlight.pack.js"></script>
  <script src="../lib/datatables/jquery.dataTables.js"></script>
  <script src="../lib/datatables-responsive/dataTables.responsive.js"></script>

    <script>
      $(function(){
        'use strict';

        $('#datatable1').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search Here',
            sSearch: '',
            lengthMenu: '_MENU_ Page',
          }
        });

        $('#datatable2').DataTable({
          bLengthChange: false,
          searching: false,
          responsive: true
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

      });
    </script>

    <!-- for tool tipe -->
    <script>
      $(document).ready(function(){
        var prkUrl = "<?php echo PRK_URL; ?>";

        $('button[id^="out"]').on('click', function() {

          var prk_admin_id = "<?php echo $prk_admin_id;?>";
          /*var prk_out_rep_name = "<?php //echo $insert_by;?>";*/
          var payment_dtl_id = this.value;
          /*testing*/
          $.confirm({
            title: 'Are You Sure',
            content: "<p style='font-size:0.9em;'>It will reset the vehicle</p>",
            theme: 'modern',
            type: 'red',
            buttons: {
                cancel: function () {
                    
                },
                somethingElse: {
                    text: 'Reset',
                    btnClass: 'btn-red',
                    keys: ['Y', 'shift'],
                    action: function(){
                      //alert(payment_dtl_id);
                        if (payment_dtl_id!='') {
                        var urlPrkVehTrnDtl = prkUrl+'prk_payment_undifine_update.php';
                        $.ajax({
                              url :urlPrkVehTrnDtl,
                              type:'POST',
                              data :
                              {
                                'payment_dtl_id':payment_dtl_id
                              },
                              dataType:'html',
                              success  :function(data)
                              {
                                //alert(data);
                                var json = $.parseJSON(data);
                                  
                                if (json.status){
                                  $.alert({
                                  icon: 'fa fa-smile-o',
                                  theme: 'modern',
                                  title: 'Success !',
                                  content: "<p style='font-size:0.9em;'>Vehicle Reset Successfully</p>",
                                  type: 'green',
                                  buttons: {
                                    Ok: function () {
                                        location.reload(true);
                                    }
                                  }
                                });
                              }else{
                                $.alert({
                                  icon: 'fa fa-frown-o',
                                  theme: 'modern',
                                  title: 'Error !',
                                  content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
                                  type: 'red'
                                });
                              }


                              }
                        });
                      
                      }else{
                        alert("Somting went wrong");
                      }
                    }
                }
            }
        });
      });

    $('[data-toggle="tooltip"]').tooltip();   
  });
</script>
