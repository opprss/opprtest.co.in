<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function visitor_gate_pass_dtl_list($prk_admin_id){
    $allplayerdata = array();
    $response = array();
    $vehicle = array();
    global $pdoconn;
    /*$sql = "SELECT vg.`visitor_gate_pass_dtl_id`, vg.`visitor_mobile`, vg.`visitor_name`, vg.`visitor_gender`, vg.`visitor_id_img`, vg.`visitor_add1`, vg.`visitor_add2`, vg.`visitor_state`, vg.`visitor_city`, vg.`visitor_landmark`, vg.`visitor_country`, vg.`visitor_pin_cod`, vg.`visitor_img`, vg.`id_name`, vg.`id_number`, vg.`visitor_gate_pass_num`, vg.`effective_date`, vg.`end_date`,`state`.`state_name`,`city`.`city_name`
        FROM `visitor_gate_pass_dtl` vg,`state`,`city`
        WHERE vg.`prk_admin_id`='$prk_admin_id'
        AND vg.`active_flag`='".FLAG_Y."' 
        AND vg.`del_flag`='".FLAG_N."'
        AND `state`.`e`=vg.`visitor_state`
        AND `city`.`e_id`=vg.`visitor_city`";*/
    $sql = "SELECT vg.`visitor_gate_pass_dtl_id`,
        vg.`visitor_mobile`, 
        vg.`visitor_name`, 
        vg.`visitor_gender`, 
        vg.`visitor_id_img`, 
        vg.`visitor_add1`, 
        vg.`visitor_add2`, 
        vg.`visitor_state`, 
        vg.`visitor_city`, 
        vg.`visitor_landmark`, 
        vg.`visitor_country`, 
        vg.`visitor_pin_cod`, 
        vg.`visitor_img`, 
        vg.`id_name`, 
        vg.`id_number`, 
        vg.`visitor_gate_pass_num`, 
        vg.`effective_date`, 
        vg.`end_date`,
        `state`.`state_name`,
        `city`.`city_name`, 
        vg.`visitor_vehicle_type`, 
        vg.`visitor_vehicle_number`,
        adrd.`full_name` as 'id_full_name',
        vty.`vehicle_type_dec`
        FROM `visitor_gate_pass_dtl` vg JOIN `state` JOIN `city`  LEFT JOIN`all_drop_down` adrd 
        ON adrd.`all_drop_down_id`=vg.`id_name` LEFT JOIN vehicle_type vty 
        ON vty.`vehicle_sort_nm`=vg.`visitor_vehicle_type`
        WHERE vg.`active_flag`='".FLAG_Y."' 
        AND vg.`del_flag`='".FLAG_N."'
        AND `state`.`e`=vg.`visitor_state`
        AND `city`.`e_id`=vg.`visitor_city`
        AND vg.`prk_admin_id`='$prk_admin_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $vehicle['visitor_gate_pass_dtl_id'] = $val['visitor_gate_pass_dtl_id'];
            $vehicle['visitor_mobile'] = $val['visitor_mobile'];
            $vehicle['visitor_name'] = $val['visitor_name'];
            $vehicle['visitor_gender'] = $val['visitor_gender'];
            $vehicle['visitor_id_img'] = (empty($val['visitor_id_img']))?'':PRK_BASE_URL.''.$val['visitor_id_img'];
            $vehicle['visitor_add1'] = $val['visitor_add1'];
            $vehicle['visitor_add2'] = $val['visitor_add2'];
            $vehicle['visitor_state'] = $val['visitor_state'];
            $vehicle['state_name'] = $val['state_name'];
            $vehicle['visitor_city'] = $val['visitor_city'];
            $vehicle['city_name'] = $val['city_name'];
            $vehicle['visitor_landmark'] = $val['visitor_landmark'];
            $vehicle['visitor_country'] = $val['visitor_country'];
            $vehicle['visitor_pin_cod'] = $val['visitor_pin_cod'];
            $vehicle['visitor_img'] = (empty($val['visitor_img']))?'':PRK_BASE_URL.''.$val['visitor_img'];
            $vehicle['id_name'] = $val['id_name'];
            $vehicle['id_number'] = $val['id_number'];
            $vehicle['visitor_gate_pass_num'] = $val['visitor_gate_pass_num'];
            $vehicle['effective_date'] = $val['effective_date'];
            $vehicle['end_date'] = $val['end_date'];
            $vehicle['visitor_vehicle_type'] = $val['visitor_vehicle_type'];
            $vehicle['visitor_vehicle_number'] = $val['visitor_vehicle_number'];
            $vehicle['id_full_name'] = $val['id_full_name'];
            $vehicle['vehicle_type_dec'] = $val['vehicle_type_dec'];
            array_push($allplayerdata, $vehicle);
        }
        $response['status'] = 1;
        $response['message'] = 'Successfully';
        $response['state'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>