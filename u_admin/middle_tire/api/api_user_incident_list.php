<?php
/*
Description: vehicle in parking area
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_incident_list($user_admin_id,$mobile){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park= array();
    $sql="SELECT  `prk_area_dtl`.`prk_area_name`,
        (SELECT `prk_user_name` FROM `prk_area_user` WHERE `prk_area_user_id`=`user_incident`.`prk_area_user_id`) as prk_user_name,
        `user_incident`.`user_incident_id`,
        `user_incident`.`user_incident_name`,
        `user_incident`.`user_incident_dtl`, 
        `user_incident`.`user_incident_number`,
        `user_incident`.`status` as 'short_status', 
        (CASE `user_incident`.`status` WHEN 'D' THEN 'DRAFT' WHEN 'I' THEN 'IN PROGRESS' WHEN 'F' THEN 'FIXED' WHEN 'A' THEN 'APPROVE' WHEN 'N' THEN 'NOT FIXED' END)AS `status`,
        `user_incident`.`prk_admin_id`, 
        `user_incident`.`prk_area_user_id`,
        DATE_FORMAT(`user_incident`.`incident_in_date`,'%d-%m-%Y %I:%i %p') AS `incident_in_date`,
        `user_incident`.`user_incident_type` as 'user_incident_short_type',
        `all_drop_down`.`full_name` as 'user_incident_type',
        DATE_FORMAT(`user_incident`.`prk_admin_approve_date`,'%d-%m-%Y %I:%i %p') AS `prk_admin_approve_date`
        FROM `user_incident`, `all_drop_down`,`prk_area_dtl`
        WHERE `user_incident`.`user_admin_id`='$user_admin_id' 
        AND `prk_area_dtl`.`prk_admin_id`=`user_incident`.`prk_admin_id`
        AND `prk_area_dtl`.`active_flag`='".FLAG_Y."'
        AND `all_drop_down`.`all_drop_down_id`=`user_incident`.`user_incident_type`
        AND `all_drop_down`.`del_flag`='".FLAG_N."'
        AND `user_incident`.`active_flag`='".FLAG_Y."' 
        AND `user_incident`.`del_flag`='".FLAG_N."' 
        ORDER BY `user_incident`.`incident_in_date` DESC";

    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val)
        {
            $park['prk_admin_id'] = $val['prk_admin_id'];
            $park['area_name'] = $val['prk_area_name'];
            $park['user_incident_id'] = $val['user_incident_id'];
            $park['user_incident_name'] = $val['user_incident_name'];
            $park['user_incident_short_type'] = $val['user_incident_short_type'];
            $park['user_incident_type'] = $val['user_incident_type'];
            $park['user_incident_dtl'] = $val['user_incident_dtl'];
            $park['user_incident_number'] = $val['user_incident_number'];
            $park['incident_in_date'] = $val['incident_in_date'];
            $park['assign_to_user'] = $val['prk_user_name'];
            $park['assign_to_date'] = $val['prk_admin_approve_date'];
            $park['short_status'] = $val['short_status'];
            $park['status'] = $val['status'];
            // $park['prk_area_user_id'] = $val['prk_area_user_id'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['incident_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response); 
} 
?>