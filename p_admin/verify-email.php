<!-- ################################################
  
Description: Parking area admin can verify his/her email by sending otp.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
include "all_nav/header.php";
$sessionDetails = getSessionDetails();
$parking_admin_name = $sessionDetails['parking_admin_name'];

if (isset($_SESSION['prk_admin_id'])) {

  $user_name=$_SESSION['prk_admin_id'];
  $prk_admin_id = $_SESSION['prk_admin_id'];

  $response = array();

  $response = prk_area_profile_show($prk_admin_id);
  $response = json_decode($response, true);
  if (!empty($response)) {
    $prk_area_name = $response['prk_area_name'];
    $prk_area_email = $response['prk_area_email'];
  }
}
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
<!-- header position -->


    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Verify Your Email</h5>
        <!--<form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form> search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card single pd-20 pd-sm-40 col-md-12">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">      
            <div class="row">
              <div class="col-md-12">
                
                  <form id="emailverify" method="post" action="">
                    <div class="form-layout">
                      <div class="row mg-b-0">
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label size">EMAIL</label>
                            <input type="email" class="form-control readonly" placeholder="Enter your email" name="email" id="email" value="<?php echo $prk_area_email; ?>" style="background-color: transparent;" readonly="readonly">
                            <span style="position: absolute;" id="error_email" class="error_size"></span>
                          </div>
                        </div>

                        <div class="col-lg-2">
                          <div class="form-group">
                            <label class="form-control-label size">OTP<span class="tx-danger">*</span></label>
                            <input type="text" class="form-control readonly" placeholder="Enter OTP" name="otp" id="otp" maxlength="6" style="background-color: transparent;" disabled="disabled">
                            <span style="position: absolute;" id="msg_otp" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-2">
                          <div class="form-layout-footer mg-t-30">
                            <button type="button" class="btn btn-block prk_button" name="send_otp" id="send_otp">GET OTP</button>
                          </div>
                        </div>
                        <div class="col-lg-1">
                          <br>
                        </div>
                        <div class="col-lg-4">
                          <div class="form-layout-footer mg-t-30">
                            <div class="row">
                              <div class="col-lg-6">
                                <button type="button" class="btn btn-block prk_button" name="verify" id="verify" disabled="disabled">VERIFY</button>
                              </div>
                              <div class="col-lg-6">
                                <button type="button" class="btn btn-block prk_button_skip" name="skip" id="skip" onclick="document.location.href='parking-area-profle';">BACK</button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div><!-- form-layout -->
                  </form>
              </div>
              
            </div>
        </div>
        </div><!-- card -->
        <!-- /add employee form -->
      </div><!-- am-pagebody -->
      

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">
  
  var prkUrl = "<?php echo PRK_URL; ?>";

  /*validation*/
  $( document ).ready( function () {

    $('#send_otp').click(function () {

      var email = $('#email').val();
      var name = "<?php echo $prk_area_name ?>";
      var mobile = "";
          
      if(email!=''){
        var urlOtpSend = prkUrl+'otpSend.php';
        $.ajax({
            url :urlOtpSend,
            type:'POST',
            data :
            {
              'mobile':mobile,
              'name':name,
              'email':email,
              'token': '<?php echo $token;?>',
              'prk_admin_id': "<?php echo $prk_admin_id ?>"
            },
            dataType:'html',
            success  :function(data)
            {
              var fewSeconds = 5;
                  
              var json = $.parseJSON(data);

                if (json.status) {
                  $('#otp').removeClass("readonly");
                  $('#otp').prop("disabled",false);
                  $('#otp').focus();
                  $('#msg_otp').html('OTP Sent, Please check');
                }else{
                  if( typeof json.session !== 'undefined'){
                    if (!json.session) {
                      window.location.replace("logout.php");
                    }
                  }else{
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                    }
              }
            }
        }).responseText;

        var fewSeconds = 20;
        var btn = $(this);
        btn.prop('disabled', true);
        setTimeout(function(){
            btn.prop('disabled', false);
        }, fewSeconds*1000);

      }else{
        $('#error_email').text('Please enter email ');
      }
    });

           
    $('#otp').keyup(function(){
       var ch = $('#otp').val().length;
         var i = 0;
          if (ch == 6) {
            $('#verify').prop("disabled",false);
          }
    });

        $('#verify').click(function () {
        // var prk_admin_id = "<?php //echo $_SESSION['prk_admin_id'] ?>";
        var email = $('#email').val();
        var mobile = "";
        var otp = $('#otp').val();
        var urlOtpVal = prkUrl+'otpVal.php';
        $.ajax({
          url :urlOtpVal,
          type:'POST',
          data :
          {
            'mobile':mobile,
            'otp':otp,
            'email':email,
            'token': '<?php echo $token;?>',
            'prk_admin_id': "<?php echo $prk_admin_id; ?>"
          },
          dataType:'html',
          success  :function(data)
          {
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status) {
              var email = $('#email').val();
              var prk_admin_id = "<?php echo $_SESSION['prk_admin_id'] ?>";
              var urlEmailVerify = prkUrl+'prk_email_verify.php';
              $.ajax({
                url :urlEmailVerify,
                type:'POST',
                data :
                {
                  'prk_area_email':email,
                  'prk_admin_id':prk_admin_id,
                  'token': '<?php echo $token;?>'
                },
                dataType:'html',
                success  :function(data)
                {
                 var json = $.parseJSON(data);

                  if (json.status) {
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success !',
                      content: "<p style='font-size:0.7em;'>Email Varified</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                            window.location='parking-area-profle';
                        }
                      }
                    });
                  }else{
                    if( typeof json.session !== 'undefined'){
                      if (!json.session) {
                        window.location.replace("logout.php");
                      }
                    }else{
                      $.alert({
                        icon: 'fa fa-frown-o',
                        theme: 'modern',
                        title: 'Error !',
                        content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                        type: 'red'
                      });
                    }
                  }
                }
              });
            }else{
              if( typeof json.session !== 'undefined'){
                      if (!json.session) {
                        window.location.replace("logout.php");
                      }
              }else{
                $('#msg_otp').html('OTP Not Matched');

              }
            }
          }
      });
    });

  } );
</script>