<?php 
/*
Description: user vehicle show. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function getVehicleDetails($user_veh_dtl_id){
    global $pdoconn;
    $response = array();
    $sql = "SELECT `user_vehicle_detail`.`user_veh_dtl_id`,
        `user_vehicle_detail`.`user_veh_type`,
        `user_vehicle_detail`.`user_veh_number`,
        `user_vehicle_detail`.`user_veh_reg`,
        `user_vehicle_detail`.`user_veh_reg_exp_dt`,
        `user_vehicle_detail`.`user_ins_number`,
        `user_vehicle_detail`.`user_ins_exp_dt`,
        IF(`user_vehicle_detail`.`user_ins_img` IS NULL or `user_vehicle_detail`.`user_ins_img` = '', '', CONCAT('".USER_BASE_URL."',`user_vehicle_detail`.`user_ins_img`)) as 'user_ins_img',
        `user_vehicle_detail`.`user_ins_img_status`,
        `user_vehicle_detail`.`user_emu_number`,
        `user_vehicle_detail`.`user_emu_exp_dt`,
        IF(`user_vehicle_detail`.`user_emu_img` IS NULL or `user_vehicle_detail`.`user_emu_img` = '', '', CONCAT('".USER_BASE_URL."',`user_vehicle_detail`.`user_emu_img`)) as 'user_emu_img',
        `user_vehicle_detail`.`user_emu_img_status`,
        `user_vehicle_detail`.`veh_qr_code`,
        `vehicle_type`.`vehicle_type_dec`,
        `vehicle_type`.`vehicle_typ_img`,
        IF(`user_vehicle_detail`.`rc_img` IS NULL or `user_vehicle_detail`.`rc_img` = '', '', CONCAT('".USER_BASE_URL."',`user_vehicle_detail`.`rc_img`)) as 'rc_img',
        `user_vehicle_detail`.`rc_img_status`,
        `user_vehicle_detail`.`oth_doc_no1`,
        `user_vehicle_detail`.`oth_doc_no1_exp_dt`,
        IF(`user_vehicle_detail`.`oth_doc_no1_img` IS NULL or `user_vehicle_detail`.`oth_doc_no1_img` = '', '', CONCAT('".USER_BASE_URL."',`user_vehicle_detail`.`oth_doc_no1_img`)) as 'oth_doc_no1_img',
        `user_vehicle_detail`.`oth_doc_no1_img_status`,
        `user_vehicle_detail`.`oth_doc_no2`,
        `user_vehicle_detail`.`oth_doc_no2_exp_dt`,
        IF(`user_vehicle_detail`.`oth_doc_no2_img` IS NULL or `user_vehicle_detail`.`oth_doc_no2_img` = '', '', CONCAT('".USER_BASE_URL."',`user_vehicle_detail`.`oth_doc_no2_img`)) as 'oth_doc_no2_img',
        `user_vehicle_detail`.`oth_doc_no2_img_status` 
        FROM `user_vehicle_detail`, `vehicle_type`
        WHERE `user_vehicle_detail`.`user_veh_dtl_id`='$user_veh_dtl_id' 
        AND `user_vehicle_detail`.`active_flag`='".FLAG_Y."'
        AND `user_vehicle_detail`.`del_flag` = '".FLAG_N."'
        AND `user_vehicle_detail`.`user_veh_type` = `vehicle_type`.`vehicle_sort_nm`
        AND `vehicle_type`.`active_flag` = '".FLAG_Y."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $row = $query->fetch();
    $response['status'] = 1;
    $response['message'] = 'data Successful';
    $response['user_veh_dtl_id'] = $row['user_veh_dtl_id'];
    $response['user_veh_type'] = $row['user_veh_type'];
    $response['user_veh_number'] = $row['user_veh_number'];
    $response['user_veh_reg'] = $row['user_veh_reg'];
    $response['user_veh_reg_exp_dt'] = $row['user_veh_reg_exp_dt'];
    $response['user_ins_number'] = $row['user_ins_number'];
    $response['user_ins_exp_dt'] = $row['user_ins_exp_dt'];
    $response['user_ins_img'] = $row['user_ins_img'];
    $response['user_ins_img_status'] = $row['user_ins_img_status'];
    $response['user_emu_number'] = $row['user_emu_number'];
    $response['user_emu_exp_dt'] = $row['user_emu_exp_dt'];
    $response['user_emu_img'] = $row['user_emu_img'];
    $response['user_emu_img_status'] = $row['user_emu_img_status'];
    $response['veh_qr_code'] = $row['veh_qr_code'];
    $response['vehicle_type_dec'] = $row['vehicle_type_dec'];
    $response['vehicle_typ_img'] = $row['vehicle_typ_img'];
    $response['rc_img'] = $row['rc_img'];
    $response['rc_img_status'] = $row['rc_img_status'];
    $response['oth_doc_no1'] = $row['oth_doc_no1'];
    $response['oth_doc_no1_exp_dt'] = $row['oth_doc_no1_exp_dt'];
    $response['oth_doc_no1_img'] = $row['oth_doc_no1_img'];
    $response['oth_doc_no1_img_status'] = $row['oth_doc_no1_img_status'];
    $response['oth_doc_no2'] = $row['oth_doc_no2'];
    $response['oth_doc_no2_exp_dt'] = $row['oth_doc_no2_exp_dt'];
    $response['oth_doc_no2_img'] = $row['oth_doc_no2_img'];
    $response['oth_doc_no2_img_status'] = $row['oth_doc_no2_img_status'];
    return $response; 
}
?>