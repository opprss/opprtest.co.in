<?php
/*
Description: user vehicle show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
require_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_mobile','user_admin_id','token'))){
 	if(isEmpty(array('user_mobile','user_admin_id','token'))){
	 	$user_mobile = trim($_POST['user_mobile']);
        $user_admin_id = trim($_POST['user_admin_id']);
        $token = trim($_POST['token']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $response = visitor_visit_my_par_draft_list($user_mobile);
        }else{
            $response = $resp;
        }
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
 }
 echo $response;
?>