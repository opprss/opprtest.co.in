<?php 
/*
Description: Parking visiter in list
Developed by: Rakhal Raj Mandal
Created Date: 31-05-2018
Update date : 
*/ 
//require_once 'api/global_api.php';
require_once 'api/parkAreaReg_api.php';
$allplayerdata = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','start_date','end_date','token'))){
    if(isEmpty(array('prk_admin_id','prk_area_user_id','token'))){            
        $prk_admin_id = trim($_POST['prk_admin_id']);            
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $start_date = trim($_POST['start_date']);
        $end_date = trim($_POST['end_date']);
        $token = ($_POST['token']);
        $visit_name = isset($_REQUEST['visit_name']) ? trim($_REQUEST['visit_name']) : ''; 
        $visit_mobile = isset($_REQUEST['visit_mobile']) ? trim($_REQUEST['visit_mobile']) : ''; 
        $tower_id = isset($_REQUEST['tower_id']) ? trim($_REQUEST['tower_id']) : ''; 
        $flat_id = isset($_REQUEST['flat_id']) ? trim($_REQUEST['flat_id']) : ''; 

        $token_che=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($token_che);
        if($json->status=='1'){
            $visitor_in_ist1 = visit_in_list($prk_admin_id,$start_date,$end_date,$visit_name,$visit_mobile,$tower_id,$flat_id);
            $visitor_in_ist = json_decode($visitor_in_ist1);
            if($visitor_in_ist->status){
                foreach ($visitor_in_ist->visiter as $key => $value) {
                    $in_vis=count($value);
                    $key_list = $key.' ('.$in_vis.')';
                    $resp[$key_list] = $value;
                }
                $response['status']=1;
                $response['message'] = 'Successful';
                $response['prk_area_name'] = $visitor_in_ist->prk_area_name;
                $response['start_date'] = $visitor_in_ist->start_date;
                $response['end_date'] = $visitor_in_ist->end_date;
                $response['visiter'] = $resp;
                $response = json_encode($response);
            }else{
                # code...
                $response = $visitor_in_ist1;
            }
        }else{
            # code...
            $response = $token_che;
        }        
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response); 
} 
echo $response;
?> 