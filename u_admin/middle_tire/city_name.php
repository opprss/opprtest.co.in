<?php 
/*
Description: all city name show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
    require_once 'api/reg_api.php';

    // require_once 'api/global_api.php';
    $response = array();
    if(isAvailable(array('city_id'))){
        if(isEmpty(array('city_id'))){
            $city_id = trim($_POST['city_id']);
            $response=city_name($city_id);
            echo $response;
        }else{
                $response['status'] = 0;
                $response['message'] = 'All Fields Are Mandatory';
                echo json_encode($response); 
        }

    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        echo json_encode($response); 
    } 
?>