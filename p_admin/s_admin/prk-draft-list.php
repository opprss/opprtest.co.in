<?php
session_start();
include "all_nav/header.php";
$insert_by=$employee_user_name;
// $products = $_SESSION["products"];
?>
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">

<div class="am-mainpanel"><!-- cloding in footer -->
    <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Premises List</h5>
    </div>
</div>
<div class="am-mainpanel">
	<div class="am-pagebody">
        <div class="card pd-20 pd-sm-40">
          <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
              	
                <tr>
                  <th class="wd-5p">Sl NO.</th>
                  <th class="wd-10p">Premises Name</th>
                  <th class="wd-10p">User Name</th>
                  <th class="wd-10p">Representative Name</th>
                  <th class="wd-10p">Email</th>
                  <th class="wd-10p">Mobile</th>
                  <th class="wd-10p">Account Satatus</th>
                  <th class="wd-5p">Action</th>
                  
                </tr>
              </thead>
              <tbody>
              	<?php 
  	              $respon=prk_admin_draft_list();
                  // echo $respon;
                  $respon = json_decode($respon, true);
                  $c=0;
  	              foreach($respon['parking_area'] as $value){
                    $c+=1;
  	              	$prk_admin_name = $value['prk_admin_name'];
  	              	$prk_admin_id = $value['prk_admin_id']; 
                    $park_ac_status_full='';
                    switch ($value['park_ac_status']) {
                        case "D":
                            $park_ac_status_full= "Draft";
                        break;
                        case "A":
                        $park_ac_status_full= "Active";
                        break;
                        case "L":
                        $park_ac_status_full= "Logged";
                        break;
                        case "T":
                        $park_ac_status_full= "Temporary Logged";
                        break;
                        case "P":
                        $park_ac_status_full= "Payment Due";
                        break;
                        default:
                        $park_ac_status_full=  "OTHER!";
                    }
                  ?>
                 	<tr>
                   <td><?php echo $c; ?></td>
                   <td><?php echo $value['prk_area_name']; ?></td>
                   <td><?php echo $value['prk_admin_name']; ?></td>
                   <td><?php echo $value['prk_area_rep_name']; ?></td>
                   <td><?php echo $value['prk_area_email']; ?></td>
                   <td><?php echo $value['prk_area_rep_mobile']; ?></td>
                   <td style="text-align: center;"><?=$park_ac_status_full?>
                   </td>
                   <td style="text-align: center;">
                   <a href="park-acount-active?prk_admin_id=<?php echo $value['prk_admin_id']; ?>" data-toggle="tooltip" data-placement="top" title="Modify"><i class="fa fa-edit" style="font-size:18px"></i></a>
                   </td>
                  </tr>
         		     <?php } ?>
                </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->
    </div>
</div>   
<!-- datatable -->
<?php include"all_nav/footer.php"; ?>

<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>

<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

  });
</script>