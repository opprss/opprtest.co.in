<?php
/*
Description: user gate pass delete.
Developed by: Rakhal Raj Mandal
Created Date: 18-02-2018
Update date : 18-05-2018
*/
function user_gate_pass_dtl_delete($prk_gate_pass_id,$user_admin_id,$mobile){
    global $pdoconn;
    $response = array();
    $sql = "UPDATE `prk_gate_pass_dtl` 
        SET `updated_by`='$mobile',`updated_date`='".TIME."',`del_flag`='".FLAG_Y."' 
        WHERE `prk_gate_pass_id`='$prk_gate_pass_id' 
        AND `user_admin_id`='$user_admin_id' 
        AND `active_flag`='".FLAG_Y."' 
        AND `del_flag`='".FLAG_N."'";

    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Delete Successful';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Delete Not Successful';
        return json_encode($response); 
    }
} 
?>