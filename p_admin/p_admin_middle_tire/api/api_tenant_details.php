<?php
/*
Description: parking area employ create.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function tenant_details($prk_admin_id,$tenant_name,$tenant_mobile,$tenant_email,$tenant_gender,$id_name,$id_number,$no_of_family,$tenant_id,$tenant_image,$id_image,$tenant_agrement_copy,$effective_date,$end_date,$parking_admin_name,$crud_type){
   $response = array();
   $allplayerdata = array();
    global $pdoconn;
    switch ($crud_type) {
        case 'I':
            $id=uniqid();
            //$sql="INSERT INTO `owner_details`(`owner_id`, `prk_admin_id`, `owner_name`, `owner_mobile`, `owner_email`, `owner_gender`, `eff_date`, `end_date`, `inserted_by`, `inserted_date`) VALUES ('$owner_id','$prk_admin_id','$owner_name','$owner_mobile','$owner_email','$owner_gender','$effective_date','$end_date','$parking_admin_name','".TIME."')";

            $sql="INSERT INTO `tenant_details`(`tenant_id`, `prk_admin_id`, `tenant_name`, `tenant_mobile`, `tenant_email`, `tenant_gender`, `tenant_image`, `id_name`, `id_number`, `id_image`, `no_of_family`, `tenant_agrement_copy`, `effective_date`, `end_date`, `inserted_by`, `inserted_date`) VALUES ('$id','$prk_admin_id','$tenant_name','$tenant_mobile','$tenant_email','$tenant_gender','$tenant_image','$id_name','$id_number','$id_image','$no_of_family','$tenant_agrement_copy','$effective_date','$end_date','$parking_admin_name','".TIME."')";

            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Tenant Save';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Tenant Not Save ';
            }  
            break;
        case 'U':
            $sql="SELECT `tenant_image`,`id_image`,`tenant_agrement_copy` FROM `tenant_details` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."' AND `tenant_id`='$tenant_id'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $val = $query->fetch();
            // $response['id_image'] =  $val['id_image'];

            $sql="UPDATE `tenant_details` SET `active_flag`='".FLAG_N."',`updated_by`='$parking_admin_name',`updated_date`='".TIME."' WHERE `tenant_id`='$tenant_id' AND `prk_admin_id`='$prk_admin_id' AND `del_flag`='".FLAG_N."' AND `active_flag`='".FLAG_Y."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $tenant_image = empty($tenant_image) ? $val['tenant_image'] : $tenant_image;
            $id_image = empty($id_image) ? $val['id_image'] : $id_image;
            $tenant_agrement_copy = empty($tenant_agrement_copy) ? $val['tenant_agrement_copy'] : $tenant_agrement_copy;

            #------------------------------------------#
            // $id=uniqid();
            $sql="INSERT INTO `tenant_details`(`tenant_id`, `prk_admin_id`, `tenant_name`, `tenant_mobile`, `tenant_email`, `tenant_gender`, `tenant_image`, `id_name`, `id_number`, `id_image`, `no_of_family`, `tenant_agrement_copy`, `effective_date`, `end_date`, `inserted_by`, `inserted_date`) VALUES ('$tenant_id','$prk_admin_id','$tenant_name','$tenant_mobile','$tenant_email','$tenant_gender','$tenant_image','$id_name','$id_number','$id_image','$no_of_family','$tenant_agrement_copy','$effective_date','$end_date','$parking_admin_name','".TIME."')";

            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Tenant Modify Save';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Tenant Modify Not Save ';
            }  
            break;
        case 'S':
            $sql="SELECT `tenant_id`, 
                `tenant_name`,
                `tenant_mobile`,
                `tenant_email`,
                `tenant_gender`,
                `tenant_image`,
                `id_name`,
                `id_number`,
                `id_image`,
                `no_of_family`,
                `tenant_agrement_copy`,
                DATE_FORMAT(`effective_date`,'%d-%m-%Y') AS `effective_date`,
                DATE_FORMAT(`end_date`,'%d-%m-%Y') AS `end_date`
                FROM `tenant_details` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."' AND `tenant_id`='$tenant_id'  ORDER BY `tenant_unique_id` DESC";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['tenant_id'] =  $val['tenant_id'];
                $response['tenant_name'] =  $val['tenant_name'];
                $response['tenant_mobile'] =  $val['tenant_mobile'];
                $response['tenant_email'] =  $val['tenant_email'];
                $response['tenant_gender'] =  $val['tenant_gender'];
                $response['tenant_image'] =  $val['tenant_image'];
                $response['id_name'] =  $val['id_name'];
                $response['id_number'] =  $val['id_number'];
                $response['id_image'] =  $val['id_image'];
                $response['no_of_family'] =  $val['no_of_family'];
                $response['tenant_agrement_copy'] =  $val['tenant_agrement_copy'];
                $response['effective_date'] =  $val['effective_date'];
                $response['end_date'] =  $val['end_date'];
            }else{
                $response['status'] = $tenant_id;
                $response['message'] = 'Not Successful';
            }  
            break;
        case 'LS':
            $sql="SELECT `tenant_id`, `tenant_name`,`tenant_mobile`,`tenant_email`,`tenant_gender`,`tenant_image`,`id_name`,`id_number`,`id_image`,`no_of_family`,`tenant_agrement_copy`,`effective_date`,`end_date` FROM `tenant_details` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $arr_catagory = $query->fetchAll();
                foreach($arr_catagory as $val)
                {
                    $park['tenant_id'] =  $val['tenant_id'];
                    $park['tenant_name'] =  $val['tenant_name'];
                    $park['tenant_mobile'] =  $val['tenant_mobile'];
                    $park['tenant_email'] =  $val['tenant_email'];
                    $park['tenant_gender'] =  $val['tenant_gender'];
                    $park['tenant_image'] =  $val['tenant_image'];
                    $park['id_name'] =  $val['id_name'];
                    $park['id_number'] =  $val['id_number'];
                    $park['id_image'] =  $val['id_number'];
                    $park['no_of_family'] =  $val['no_of_family'];
                    $park['tenant_agrement_copy'] =  $val['tenant_agrement_copy'];
                    $park['effective_date'] =  $val['effective_date'];
                    $park['end_date'] =  $val['end_date'];
                    array_push($allplayerdata, $park);
                }
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['tenant_list'] = $allplayerdata;
            }else{
                $response['status'] = 0;
                $response['message'] = 'List Empty';
            }
            break;
        case 'D':
            $sql="UPDATE `tenant_details` SET `del_flag`='".FLAG_Y."',`updated_by`='$parking_admin_name',`updated_date`='".TIME."' WHERE `tenant_id`='$tenant_id' AND `prk_admin_id`='$prk_admin_id' AND `del_flag`='".FLAG_N."' AND `active_flag`='".FLAG_Y."'";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Delete Successful';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Delete Not Successful';
            }
            break;
        default:
            $response['status'] = 0;
            $response['message'] = 'Wrong Select';
            break;
    }
    return json_encode($response);  
}
?>