<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_emergency_notification($prk_admin_id,$user_admin_id,$tower_name,$flat_name,$oth11,$oth12,$oth13){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
    $sql = "SELECT pugld.prk_user_gate_login_id,
        pt.token_value,
        pt.device_token_id,
        pt.device_type 
        FROM prk_user_gate_login_dtl pugld, prk_token pt
        WHERE pt.prk_admin_id=pugld.prk_admin_id 
        AND pt.prk_area_user_id=pugld.prk_area_user_id
        AND pt.active_flag='Y'
        AND pugld.prk_admin_id='$prk_admin_id'
        AND pugld.end_date IS NULL";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $device_token_id =  $val['device_token_id'];
            $device_type =  $val['device_type'];
            if ($device_type=='ANDR') {
                $payload = array();
                // $payload['area_name'] = $prk_area_name;
                // $payload['name'] = $user_family_name;
                $payload['in_date'] = TIME;
                $payload['url'] = 'EMERGENCY ALERT';
                $title = 'OPPRSS';
                $message = 'EMERGENCY ALERT FROM '.$tower_name.','.$flat_name.'.';
                $push_type = 'individual';
                $include_image='';
                $clickaction='EMERGENCY ALERT';
                $oth1='EMP';
                $oth2='';
                $oth3='';
                $paylo = push_send($title,$message,$push_type,$include_image,$device_token_id,$payload,$clickaction,$oth1,$oth2,$oth3);
            }
        }
    }
    $sql = "SELECT amd.adm_mer_dtl_name, 
        amd.adm_mer_dtl_number,
        amd.mer_status,
        amd.inserted_status
        FROM admin_mer_dtl amd
        WHERE amd.active_flag='Y' 
        AND amd.del_flag='N'  
        AND IF(mer_status='I',(
           IF(amd.inserted_status='U',amd.user_admin_id,amd.prk_admin_id)
        ),amd.mer_status) IN ('D','$prk_admin_id','$user_admin_id') 
        AND LENGTH(amd.adm_mer_dtl_number)='10'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $adm_mer_dtl_number = $val['adm_mer_dtl_number'];
            $sql="SELECT `user_admin`.`user_admin_id`,`token`.`device_token_id`,`token`.`device_type`,`token`.`token_value` FROM `user_admin`,`token` WHERE `user_mobile`='$adm_mer_dtl_number' AND `token`.`active_flag`='".FLAG_Y."' AND `token`.`user_admin_id`=`user_admin`.`user_admin_id`";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $device_token_id =  $val['device_token_id'];
                $device_type =  $val['device_type'];
                if ($device_type=='ANDR') {
                    $payload = array();
                    // $payload['area_name'] = $prk_area_name;
                    // $payload['name'] = $veh_number;
                    $payload['in_date'] = TIME;
                    $payload['url'] = 'EMERGENCY ALERT';
                    $title = 'OPPRSS';
                    $message = 'EMERGENCY ALERT FROM '.$tower_name.','.$flat_name.'.';
                    $push_type = 'individual';
                    $include_image='';
                    $clickaction='EMERGENCY ALERT';
                    $oth1='USER';
                    $oth2='';
                    $oth3='';
                    $paylo = push_send($title,$message,$push_type,$include_image,$device_token_id,$payload,$clickaction,$oth1,$oth2,$oth3);
                }
            }
        }
    }
    $response['status'] = 1;
    $response['message'] = 'Successful';
    return json_encode($response);
}
?>