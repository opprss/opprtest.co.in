<!-- ################################################
  
Description: Apartment Setupe.
Developed by: Rakhal Raj 
Created Date: 12-03-2018

 ####################################################-->
<?php include "all_nav/header.php";
	$category_type='APT_CH_PAY_TI';
	// late_fine_payable_time
	$late_fine_type = common_drop_down_list($category_type);
	$late_fine_type_list = json_decode($late_fine_type, true);
	// $prk_user_emp_category = json_decode($prk_user_emp_cat, true);
?>
<style>
  .size{
      font-size: 13px;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .error{
      font-size: 11px;
      color: red;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .success{
      font-size: 11px;
      color: green;
    }
</style>

<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Apartment Setup<?//=$flat_master_id?></h5>
  </div>
  <div class="am-pagebody">
  	<div class="card pd-20 pd-sm-40">
  		<div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
  			<form id="apart_setup">
	  			<div class="row mg-b-25">
	  				<div class="col-md-2">
		                <div class="form-group">
		                  <label class="form-control-label size">Maintenance Due Date <span class="tx-danger"></span></label>
		                  <label class="ckbox mg-t-10">
		                    <input id="last_due_day" value="Y" type="checkbox">
		                    <span style="font-size: 11px;">Last Day of the Month</span>
		                  </label>
		                </div>
	              	</div>
					<div class="col-md-2">  
						<div class="form-group">
						  <label class="form-control-label size">Day of the Month<span class="tx-danger"></span></label>
						  <input type="text" class="form-control" placeholder="Choose Day" name="choose_due_day" id="choose_due_day" value="" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
						  <span style="position: absolute;display: none;" id="error_choose_due_day" class="error_size">This field is required.</span>
						</div>
					</div>
					<div class="col-md-2" style="display: none;">
	                    <div class="form-group">
	                      <label class="form-control-label size">Flat/Sq.Ft<span class="tx-danger">*</span></label>
	                      <input type="text" class="form-control" placeholder="Flat/Sq.Ft" name="flat_sq_ft" id="flat_sq_ft"  onkeydown="upperCase(this)">
	                      <span style="position: absolute;" id="error_flat_sq_ft" class="error_size"></span>
	                    </div>
	              	</div>
	              	<div class="col-md-2">
	                    <div class="form-group">
	                      <label class="form-control-label size">Flat/Sq.Ft Rate<span class="tx-danger">*</span></label>
	                      <input type="text" class="form-control" placeholder="Rate" name="rate_sq_ft" id="rate_sq_ft"  onkeydown="upperCase(this)">
	                      <span style="position: absolute;" id="error_rate_sq_ft" class="error_size"></span>
	                    </div>
	              	</div>
	              	<div class="col-lg-2">
	                  <div class="form-group">
	                    <label class="form-control-label size">Maintenance Start Date<span class="tx-danger">*</span></label>
	                    <input type="text" name="maint_start_date" id="maint_start_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
	                    <span style="position: absolute; display: none;" id="error_maint_start_date" class="error">This field is required.</span>
	                  </div>
	                </div>
	                <div class="col-md-2" style="display: none;">
	                    <div class="form-group">
	                      <label class="form-control-label size">Adv.. Payment Month<span class="tx-danger">*</span></label>
	                      <input type="text" class="form-control" placeholder="Advance Payment Month" name="adv_payment_month" id="adv_payment_month"  onkeydown="upperCase(this)">
	                      <span style="position: absolute;" id="error_adv_payment_month" class="error_size"></span>
	                    </div>
	              	</div>
	              	<div class="col-md-2" style="display: none;">
	                    <div class="form-group">
	                      <label class="form-control-label size">Due Payment Month<span class="tx-danger">*</span></label>
	                      <input type="text" class="form-control" placeholder="Due Payment Month" name="due_payment_month" id="due_payment_month"  onkeydown="upperCase(this)">
	                      <span style="position: absolute;" id="error_due_payment_month" class="error_size"></span>
	                    </div>
	              	</div>
	              	<div class="col-md-2">
	                    <div class="form-group">
	                      <label class="form-control-label size">State Mant Date<span class="tx-danger">*</span></label>
	                      <input type="text" class="form-control" placeholder="State Mant Date" name="state_mant_date" id="state_mant_date">
	                      <span style="position: absolute;" id="error_state_mant_date" class="error_size"></span>
	                    </div>
	              	</div>
	              	<div class="col-lg-2">
                  		<div class="form-group">
		                    <label class="form-control-label size">Late Fine Time line<span class="tx-danger">*</span></label>
		                    <div class="select">
		                      <select name="late_fine_payable_time" id="late_fine_payable_time" style="opacity: 0.8;font-size:14px">
		                        <option value="">Select Type</option>
		                          <?php
		                          foreach ($late_fine_type_list['drop_down'] as $val){
		                          	if ($val['description']=='1'||$val['description']=='3'){ 
		                           ?>  
		                            <option value="<?php echo $val['description']?>"><?php echo $val['full_name']?></option>
		                          <?php          
		                          }}
		                        ?>
		                      </select>
		                    </div>
		                  <span style="position: absolute; display: none;" id="error_late_fine_payable_time" class="error">This field is required.</span>
	                  	</div>
                	</div>
	              	<div class="col-lg-2">
                  		<div class="form-group">
		                    <label class="form-control-label size">Late Fine Type<span class="tx-danger">*</span></label>
		                    <div class="select">
		                      <select name="late_fine_rate_type" id="late_fine_rate_type" style="opacity: 0.8;font-size:14px">
		                        <option value="">Select Type</option>
		                        <option value="F">Fixed</option>
		                        <option value="P">Percentage</option>
		                      </select>
		                    </div>
		                  <span style="position: absolute; display: none;" id="error_late_fine_payable_time" class="error">This field is required.</span>
	                  	</div>
                	</div>
	              	<div class="col-md-2">
	                    <div class="form-group">
	                      <label class="form-control-label size">Late Fine Charge<span class="tx-danger">*</span></label>
	                      <input type="text" class="form-control" placeholder="Late Fine Charge" name="late_fine_charrge" id="late_fine_charrge">
	                      <span style="position: absolute;" id="error_late_fine_charrge" class="error_size"></span>
	                    </div>
	              	</div>
	              	<div class="col-lg-2">
	                  	<div class="form-layout-footer mg-t-30">
		                  	<input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
	                  	</div>
	                </div>
	  			</div>
	  		</form>
  		</div>
  	</div>
  </div>
</div>
<?php include"all_nav/footer.php"; ?>
<script type="text/javascript">
	$('#last_due_day').click(function(){
      // alert($('#last_due_day').val());
      choose_due_day1();
      console.log(this.checked);
      if(this.checked == true) {
        $('#choose_due_day').val('').prop('disabled', true);
      } else {
        $('#choose_due_day').prop('disabled', false);
      }
    });
</script>
<script type="text/javascript">
	function choose_due_day1()  {
	  	var checkBox = document.getElementById("last_due_day");
	  	var choose_due_day = $('#choose_due_day').val();
		if (checkBox.checked == true || choose_due_day!='' ){
			$('#error_choose_due_day').hide();
			return true;
		}else{
			$('#error_choose_due_day').show();
			return false;
		}
  	}
  	$( document ).ready( function () {
  		$("#choose_due_day").blur(function () {
	    //alert("ok");
	      choose_due_day1();
	    });
  	});
</script>
<script type="text/javascript">
	addEventListener('DOMContentLoaded', function () {
	    var now = new Date;
	    var now = now.setDate(now.getDate() - 1);
	    pickmeup('#maint_start_date', {
	      default_date   : false,
	      position       : 'bottom',
	      hide_on_select : true,
	      render : function (date) {
	        //if (date < now) {
	          return {
	            disabled : false,
	            class_name : 'date-in-past'
	          };
	        //}
	        return {};
	      } 
	    });
	});
</script>
<script type="text/javascript">
	$("#flat_sq_ft,#rate_sq_ft,#adv_payment_month,#due_payment_month,#late_fine_charrge,#state_mant_date").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0)){
        event.preventDefault();
      }
    });
</script>
<script type="text/javascript">
	$("#adv_payment_month").keyup(function(){
      var mm = $('#adv_payment_month').val()
      if(mm > 12){
        $('#adv_payment_month').val('12');
      }
    });
    $("#due_payment_month").keyup(function(){
      var mm1 = $('#due_payment_month').val()
      if(mm1 > 12){
        $('#due_payment_month').val('12');
      }
    });
    $("#choose_due_day").keyup(function(){
      var mm = $('#choose_due_day').val()
     if(mm > 28){
        $('#choose_due_day').val('28');
      }
    });
</script>
<script type="text/javascript">
	$( document ).ready( function () {
		$("#apart_setup").validate( {
			rules: {
				flat_sq_ft: "required",
				rate_sq_ft: "required",
				adv_payment_month: "required",
				due_payment_month: "required",
				maint_start_date: "valueNotEqualsEff",
				tower_name: "valueNotEqualstower",
				choose_due_day: "valueNotEqualsdue",
				late_fine_payable_time: "required",
				late_fine_rate_type: "required",
				late_fine_charrge: "required",
				state_mant_date: "required"
			},
			messages: {
			}
		});
	    $("#maint_start_date").blur(function () {
	      var maint_start_date = $('#maint_start_date').val();
	      if(maint_start_date == ''){
	        $('#error_maint_start_date').show();
	        return false;
	      }else{
	        $('#error_maint_start_date').hide();
	        return true;
	      }
	    });
	});
	$.validator.addMethod("valueNotEqualsEff", function(value){
		// alert('ok');
		arg = "";
		if (value != "") {
		return arg !== value;
		}else{
		$("#error_maint_start_date").show();
		return true;
		}
	});
	$.validator.addMethod("valueNotEqualsdue", function(value){
		// alert('ok');
		arg = "";
		if (value != "") {
			return arg !== value;
		}else{
			$("#error_choose_due_day").show();
			return true;
		}
	});
  	$('input[type=text]').keyup(function(){
	    // alert('ok');
	    this.value = this.value.toUpperCase();
  	});
</script>
<script type="text/javascript">
	var prkUrl = "<?php echo PRK_URL; ?>";
	var FUTURE_DATE = "<?php echo FUTURE_DATE ?>";
	var prk_admin_id="<?=$prk_admin_id?>";
	var parking_admin_name="<?=$parking_admin_name?>";
	var token="<?=$token?>";
	var urlPrkAreGateDtl = prkUrl+'apartment_setup.php';
	$('#save').click(function(){
		var checkBox = document.getElementById("last_due_day");
		if (checkBox.checked == true){
			var last_due_day='Y';
		}else{
			var last_due_day='N';

		}
		var choose_due_day=($("#choose_due_day").val());
		var flat_sq_ft=($("#flat_sq_ft").val());
		var rate_sq_ft=($("#rate_sq_ft").val());
		var maint_start_date=($("#maint_start_date").val());
		var adv_payment_month=($("#adv_payment_month").val());
		var due_payment_month=($("#due_payment_month").val());
		var late_fine_payable_time=($("#late_fine_payable_time").val());
		var late_fine_charrge=($("#late_fine_charrge").val());
		var late_fine_rate_type=($("#late_fine_rate_type").val());
		var state_mant_date=($("#state_mant_date").val());
	    var str_array = maint_start_date.split('-');
	    var dd = str_array[0];
	    var mm = str_array[1];
	    var yy = str_array[2];
	    var maint_start_dat = yy+'-'+mm+'-'+dd;
		if($("#apart_setup").valid()&choose_due_day1()){
			//alert("ok");
			$('#save').val('Wait ...').prop('disabled', true);
		    $.ajax({
		        url :urlPrkAreGateDtl,
		        type:'POST',//'prk_admin_id','tower_name','effective_date','end_date','parking_admin_name','token','tower_id','crud_type'
		        data :{
		          'prk_admin_id':prk_admin_id,
		          'last_due_day':last_due_day,
		          'choose_due_day':choose_due_day,
		          'flat_sq_ft':flat_sq_ft,
		          'rate_sq_ft':rate_sq_ft,
		          'maint_start_date':maint_start_dat,
		          'adv_payment_month':adv_payment_month,
		          'due_payment_month':due_payment_month,
		          'parking_admin_name':parking_admin_name,
		          'late_fine_payable_time':late_fine_payable_time,
		          'late_fine_charrge':late_fine_charrge,
		          'late_fine_rate_type':late_fine_rate_type,
		          'state_mant_date':state_mant_date,
		          'tower_id':'',
		          'crud_type':'I',
		          'token': token
		        },
		        dataType:'html',
		        success  :function(data){
		          $('#save').val('SAVE').prop('disabled', false);
		          // alert(data);
		          var json = $.parseJSON(data);
		          if (json.status){
		            $.alert({
		              icon: 'fa fa-smile-o',
		              theme: 'modern',
		              title: 'Success !',
		              content: "<p style='font-size:0.9em;'>Setup Successfully</p>",
		              type: 'green',
		              buttons: {
		                Ok: function () {
		                    location.reload(true);
		                }
		              }
		            });
		          }else{
		            if( typeof json.session !== 'undefined'){
		              if (!json.session) {
		                window.location.replace("logout.php");
		              }
		            }else{
		              $.alert({
		              icon: 'fa fa-frown-o',
		              theme: 'modern',
		              title: 'Error !',
		              content: "<p style='font-size:0.9em;'>Something went wrong</p>",
		              type: 'red'
		            });
		            }
		          }
		        }
		    });
		}
	});
</script>
<script type="text/javascript">
	function apartment_setup_show(){
		var prkUrl = "<?php echo PRK_URL; ?>";
		var FUTURE_DATE = "<?php echo FUTURE_DATE ?>";
		var prk_admin_id="<?=$prk_admin_id?>";
		var parking_admin_name="<?=$parking_admin_name?>";
		var token="<?=$token?>";
		var urlPrkAreGateDtl = prkUrl+'apartment_setup.php';

		$.ajax({
	        url :urlPrkAreGateDtl,
	        type:'POST',//'prk_admin_id','tower_name','effective_date','end_date','parking_admin_name','token','tower_id','crud_type'
	        data :{
	          'prk_admin_id':prk_admin_id,
	          'last_due_day':'',
	          'choose_due_day':'',
	          'flat_sq_ft':'',
	          'rate_sq_ft':'',
	          'maint_start_date':'',
	          'adv_payment_month':'',
	          'due_payment_month':'',
	          'parking_admin_name':parking_admin_name,
	          'late_fine_payable_time':'',
	          'late_fine_charrge':'',
		      'late_fine_rate_type':'',
		      'state_mant_date':'',
	          'tower_id':'',
	          'crud_type':'S',
	          'token': token
	        },
	        dataType:'html',
	        success  :function(data){
	          var json = $.parseJSON(data);
	          if (json.status){
	          	if(json.last_due_day=='Y'){
	          		$("#last_due_day").prop( "checked", true );
	          		$('#choose_due_day').val('').prop('disabled', true);
	          	}else{
	          		$("#last_due_day").prop( "checked", false );
	          	}
	            $('#choose_due_day').val(json.choose_due_day);
	            $('#flat_sq_ft').val(json.flat_sq_ft);
	            $('#rate_sq_ft').val(json.rate_sq_ft);
	            $('#maint_start_date').val(json.maint_start_date);
	            $('#adv_payment_month').val(json.adv_payment_month);
	            $('#due_payment_month').val(json.due_payment_month);
	            $('#late_fine_payable_time').val(json.late_fine_payable_time);
	            $('#late_fine_charrge').val(json.late_fine_charrge);
	            $('#late_fine_rate_type').val(json.late_fine_rate_type);
	            $('#state_mant_date').val(json.state_mant_date);
	          }
	        }
	    });
	}
</script>
<script type="text/javascript">
	$( document ).ready( function () {
		apartment_setup_show();
	});
</script>