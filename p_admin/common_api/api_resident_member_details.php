<?php
/*
Description: parking area employ create.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function resident_member_details($prk_admin_id,$res_mem_name,$res_mem_mobile,$res_mem_email,$res_mem_gender,$inserted_by,$crud_type,$res_mem_id,$eff_date,$end_date,$oth3,$oth4,$oth5){
    $response = array();
    $allplayerdata = array();
    global $pdoconn;
    if(!empty($eff_date) && !empty($end_date)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($eff_date)) == date($eff_date) && date($format, strtotime($end_date)) == date($end_date)) {
            $eff_date= $eff_date;
            $end_date=  $end_date ;
        } else {
            $eff_date = date("Y-m-d", strtotime($eff_date));
            $end_date = date("Y-m-d", strtotime($end_date));
        }
    }else{
        $eff_date= TIME_TRN;
        $end_date= FUTURE_DATE;
    }
    switch ($crud_type) {
        case 'I':
            $sql = "SELECT `res_mem_id` FROM `resident_member` WHERE `res_mem_mobile` ='$res_mem_mobile' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."' AND `prk_admin_id`='$prk_admin_id'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $res_mem_id =  $val['res_mem_id'];
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['res_mem_id'] = $res_mem_id;
            }else{
                $res_mem_id=uniqid();
                $sql="INSERT INTO `resident_member`(`res_mem_id`, `prk_admin_id`, `res_mem_name`, `res_mem_mobile`, `res_mem_email`, `res_mem_gender`, `eff_date`, `end_date`, `inserted_by`, `inserted_date`) VALUES ('$res_mem_id','$prk_admin_id','$res_mem_name','$res_mem_mobile','$res_mem_email','$res_mem_gender','$eff_date','$end_date','$inserted_by','".TIME."')";

                $query  = $pdoconn->prepare($sql);
                if($query->execute()){
                    $response['status'] = 1;
                    $response['message'] = 'Successful';
                    $response['res_mem_id'] = $res_mem_id;
                }else{
                    $response['status'] = 0;
                    $response['message'] = 'Unsuccessfully';
                }  
            }
            break;
        case 'U':
            $sql = "UPDATE `resident_member` SET `updated_by`='$inserted_by',`updated_date`='".TIME."',`active_flag`='".FLAG_N."' WHERE `res_mem_id`='$res_mem_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            #------------------------------------------#
            $sql="INSERT INTO `resident_member`(`res_mem_id`, `prk_admin_id`, `res_mem_name`, `res_mem_mobile`, `res_mem_email`, `res_mem_gender`, `eff_date`, `end_date`, `inserted_by`, `inserted_date`) VALUES ('$res_mem_id','$prk_admin_id','$res_mem_name','$res_mem_mobile','$res_mem_email','$res_mem_gender','$eff_date','$end_date','$inserted_by','".TIME."')";

            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['res_mem_id'] = $res_mem_id;
            }else{
                $response['status'] = 0;
                $response['message'] = 'Unsuccessfully';
            }   
            break;
        case 'S':
            $sql="SELECT `res_mem_id`,
                `prk_admin_id`,
                `res_mem_name`,
                `res_mem_mobile`,
                `res_mem_email`,
                `res_mem_gender`,
                DATE_FORMAT(`eff_date`,'%d-%m-%Y') AS `eff_date`,
                DATE_FORMAT(`end_date`,'%d-%m-%Y') AS `end_date`
                FROM `resident_member` 
                WHERE `prk_admin_id`='$prk_admin_id' 
                AND `res_mem_id`='$res_mem_id'
                AND `active_flag`='".FLAG_Y."' 
                AND `del_flag`='".FLAG_N."' 
                ORDER BY `res_mem_unique_id` DESC";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $response['status'] = 1;
                $response['res_mem_id'] =  $val['res_mem_id'];
                $response['prk_admin_id'] =  $val['prk_admin_id'];
                $response['res_mem_name'] =  $val['res_mem_name'];
                $response['res_mem_mobile'] =  $val['res_mem_mobile'];
                $response['res_mem_email'] =  $val['res_mem_email'];
                $response['res_mem_gender'] =  $val['res_mem_gender'];
                $response['eff_date'] =  $val['eff_date'];
                $response['end_date'] =  $val['end_date'];
            }else{
                $response['status'] = 0;
                $response['message'] = 'Not Successful';
            }  
            break;
        case 'LS':
            $sql="SELECT `res_mem_id`,
                `prk_admin_id`,
                `res_mem_name`,
                `res_mem_mobile`,
                `res_mem_email`,
                `res_mem_gender`,
                DATE_FORMAT(`eff_date`,'%d-%b-%Y') AS `eff_date`,
                DATE_FORMAT(`end_date`,'%d-%b-%Y') AS `end_date`
                FROM `resident_member` 
                WHERE `prk_admin_id`='$prk_admin_id' 
                AND `active_flag`='".FLAG_Y."' 
                AND `del_flag`='".FLAG_N."' 
                ORDER BY `res_mem_unique_id` DESC";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $arr_catagory = $query->fetchAll();
                foreach($arr_catagory as $val){
                    $park['res_mem_id'] =  $val['res_mem_id'];
                    $park['prk_admin_id'] =  $val['prk_admin_id'];
                    $park['res_mem_name'] =  $val['res_mem_name'];
                    $park['res_mem_mobile'] =  $val['res_mem_mobile'];
                    $park['res_mem_email'] =  $val['res_mem_email'];
                    $park['res_mem_gender'] =  $val['res_mem_gender'];
                    $park['eff_date'] =  $val['eff_date'];
                    $park['end_date'] =  $val['end_date'];
                    array_push($allplayerdata, $park);
                }
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['resident_list'] = $allplayerdata;
            }else{
                $response['status'] = 0;
                $response['message'] = 'List Empty';
            }
            break;
        case 'D':
            $sql = "UPDATE `resident_member` SET `updated_by`='$inserted_by',`updated_date`='".TIME."',`del_flag`='".FLAG_Y."' WHERE `res_mem_id`='$res_mem_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Delete Successful';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Delete Not Successful';
            }
            break;
        default:
            $response['status'] = 0;
            $response['message'] = 'Wrong Select';
            break;
    }
    return json_encode($response);  
}
?>