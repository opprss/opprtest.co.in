<!-- ################################################
Description: Tower detalis.
Developed by: Rakhal Raj 
Created Date: 12-03-2018
 ####################################################-->
<?php include "all_nav/header.php";
  $flat_id=($_REQUEST['list2_id']);
  $flat_name=($_REQUEST['list2_name']);
  $tower_id=($_REQUEST['list1_id']);
  $tower_name=($_REQUEST['list1_name']);
  $oth1=$oth2=$oth3=$oth4=$oth5='';
  $crud_type='LS';
  $flat_master=flat_master($prk_admin_id,$tower_id,$flat_id,'','','','','','',$crud_type,'','','','','','','','');
  $flat_master_list = json_decode($flat_master, true);

  $tower_lis = tower_list($prk_admin_id);
  $tower_list = json_decode($tower_lis, true);

  $category_type='APT_FLT_HLD';
  $prk_user_emp_cat = common_drop_down_list($category_type);
  $prk_user_emp_category = json_decode($prk_user_emp_cat, true);
  $prk_inf = prk_inf($prk_admin_id);
  $prk_inf_dtl = json_decode($prk_inf, true);
  $resident_flag = $prk_inf_dtl['resident_flag'];
  // $resident_flag='R';
  $owner_details=owner_details($prk_admin_id,'','','','','','','','','',$parking_admin_name,$crud_type);
  $owner_details_list = json_decode($owner_details, true);
  $tenant_details=tenant_details($prk_admin_id,'','','','','','','','','','','','','',$parking_admin_name,$crud_type);
  $tenant_details_list = json_decode($tenant_details, true);
  $resident_details=resident_member_details($prk_admin_id,'','','','','',$crud_type,'','','','','','');
  $resident_details_list = json_decode($resident_details, true);

  $crud_type='S';
  $flat_master_sh=flat_master($prk_admin_id,$tower_id,$flat_id,'','','','','','',$crud_type,'','','','','','','','');
  $flat_master_show = json_decode($flat_master_sh, true);
  $flat_master_id='NA';
  $owner_id='';
  $owner_name='';
  $owner_mobile='';
  $living_status='';
  $living_name='';
  $tenant_id='';
  $tenant_name='';
  $tenant_mobile='';
  $res_mem_id='';
  $res_mem_name='';
  $res_mem_mobile='';
  $two_wh_spa_allw='N';
  $two_wh_spa='';
  $four_wh_spa_allw='N';
  $four_wh_spa='';
  $inter_com_mobile='';
  $app_use='Y';
  $club_membership='N';
  $effective_date=TIME_TRN_WEB;
  $end_date='';
  if($flat_master_show['status']){
    $flat_master_id=$flat_master_show['flat_master_id'];
    $owner_id=$flat_master_show['owner_id'];
    $owner_name=$flat_master_show['owner_name'];
    $owner_mobile=$flat_master_show['owner_mobile'];
    $living_status=$flat_master_show['living_status'];
    $living_name=$flat_master_show['living_name'];
    $tenant_id=$flat_master_show['tenant_id'];
    $tenant_name=$flat_master_show['tenant_name'];
    $tenant_mobile=$flat_master_show['tenant_mobile'];
    $res_mem_id=$flat_master_show['res_mem_id'];
    $res_mem_name=$flat_master_show['res_mem_name'];
    $res_mem_mobile=$flat_master_show['res_mem_mobile'];
    $two_wh_spa_allw=$flat_master_show['two_wh_spa_allw'];
    $two_wh_spa=$flat_master_show['two_wh_spa'];
    $four_wh_spa_allw=$flat_master_show['four_wh_spa_allw'];
    $four_wh_spa=$flat_master_show['four_wh_spa'];
    $inter_com_mobile=$flat_master_show['inter_com_mobile'];
    $app_use=$flat_master_show['app_use'];
    $club_membership=$flat_master_show['club_membership'];
    // $effective_date=$flat_master_show['effective_date'];
    $end_date=$flat_master_show['end_date'];
  }

  $drp_dw1='O';
  $drp_dw2='T';
  $drp_dw3='R';
  if($resident_flag=='R'){
    $drp_dw1='R';
    $drp_dw2='R';
    $drp_dw3='R';
  }
?>
<!-- header position -->
  <style type="text/css">
    .size{
      font-size: 11px;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .error{
      font-size: 11px;
      color: red;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .success{
      font-size: 11px;
      color: green;
    }
    div{
      font-size: 14px;
    }
    .select2-container .select2-selection--single{
      height:34px !important;
    }
  </style>
  <!-- links for datatable -->
  <link href="../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
    <div class="am-mainpanel">
      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Flat Details <?//=$flat_master_id?></h5>
      </div><!-- am-pagetitle -->
      <div class="am-pagebody">
        <div class="card pd-10 pd-sm-40" >
          <div class="editable tx-16 bd pd-30 tx-inverse">
            <form id="park_flat">
              <div class="row mg-b-5">
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Tower Name<span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="tower_name" id="tower_name" style="opacity: 0.8;font-size:14px">
                        <option value="">Select Tower</option>
                        <?php if($tower_list['status']){
                          foreach($tower_list['tower_list'] as $value){?>
                          <option value="<?=$value['tower_id'];?>"><?=$value['tower_name'];?></option>
                        <?php }}?>
                      </select>
                    </div>
                    <span style="position: absolute; display: none;" id="error_tower_name" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Flat Name<span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="flat_name" id="flat_name" style="opacity: 0.8;font-size:14px">
                        <option value="">Select Flat</option>
                      </select>
                    </div>
                  <span style="position: absolute; display: none;" id="error_flat_name" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-3 res_mem_name_d">
                  <div class="form-group">
                    <label class="form-control-label size">Member Name <span class="tx-danger">*</span></label>
                    <select name="res_mem_id" class="select2" id="res_mem_id">
                      <option value="">Select Member Name</option>
                      <?php if($resident_details_list['status']){  
                        foreach ($resident_details_list['resident_list'] as $val){?> 
                        <option value="<?php echo $val['res_mem_id']?>" <?php if($val['res_mem_id']==$res_mem_id){?> selected="selected" <?php }?>><?php echo $val['res_mem_name']?></option>
                      <?php }}?>
                    </select>
                    <span style="position: absolute; display: none;" id="error_res_mem_id" class="error">This field is required.</span>
                    <input type="hidden" id="res_mem_name" name="res_mem_name">
                    <input type="hidden" id="res_mem_mobile" name="res_mem_mobile">
                  </div>
                </div>
                <div class="col-lg-3 owner_name_d">
                  <div class="form-group">
                    <label class="form-control-label size">Owner Name<span class="tx-danger">*</span></label>
                    <select name="owner_id" class="select2" id="owner_id">
                      <option value="">Select Owner</option>
                        <?php if($owner_details_list['status']){  
                          foreach ($owner_details_list['owner_list'] as $val){?> 
                          <option value="<?=$val['owner_id']?>" <?php if($val['owner_id']==$owner_id){?> selected="selected" <?php }?>><?php echo $val['owner_name']?></option>
                        <?php }}?>
                    </select>
                    <span style="position: absolute; display: none;" id="error_owner_id" class="error">This field is required.</span>
                    <input type="hidden" id="owner_name" name="owner_name">
                    <input type="hidden" id="owner_mobile" name="owner_mobile">
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Living Status<span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="living_status" id="living_status" style="font-size:14px">
                        <option value="">Select Living</option>
                          <?php
                          foreach ($prk_user_emp_category['drop_down'] as $val){ 
                            if($val['sort_name']==$drp_dw1||$val['sort_name']==$drp_dw2||$val['sort_name']==$drp_dw3){?>  
                            <option value="<?php echo $val['sort_name']?>"><?php echo $val['full_name']?></option>
                          <?php          
                          }}
                        ?>
                      </select>
                    </div>
                  <span style="position: absolute; display: none;" id="error_living_status" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-3 tenant_name_d" style="display: ;" id="tenant_name_dis">
                  <div class="form-group">
                    <label class="form-control-label size">Tenant Name<span class="tx-danger"></span></label>
                    <select name="tenant_id" class="select2" id="tenant_id">
                      <option value="">Select Tenant</option>
                        <?php if($tenant_details_list['status']){  
                          foreach ($tenant_details_list['tenant_list'] as $val){?> 
                          <option value="<?=$val['tenant_id']?>" <?php if($val['tenant_id']==$tenant_id){?> selected="selected" <?php }?>><?php echo $val['tenant_name']?></option>
                        <?php }}?>
                    </select>
                    <span style="position: absolute; display: none;" id="error_tenant_id" class="error">This field is required.</span>
                    <input type="hidden" id="tenant_name" name="tenant_name">
                    <input type="hidden" id="tenant_mobile" name="tenant_mobile">
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Intercom Mobile No.<span class="tx-danger"></span></label>
                    <input type="text" name="contact_number" id="contact_number" placeholder="Mobile No" class="form-control" maxlength="10">
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Space Allow(2W)<span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="two_wh_spa_allw" id="two_wh_spa_allw" style="opacity: 0.8;font-size:14px">
                        <option value="">Select</option>
                        <option value="N" selected="selected">NO</option>
                        <option value="Y">YES</option>
                      </select>
                    </div>
                    <span style="position: absolute; display: none;" id="error_two_wh_spa_allw" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-2" style="display: none;" id="two_wh_spa_dis">
                  <div class="form-group">
                    <label class="form-control-label size">No of Space(2W)<span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="SPACE" name="two_wh_spa" id="two_wh_spa">
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Space Allow(4W)<span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="four_wh_spa_allw" id="four_wh_spa_allw" style="opacity: 0.8;font-size:14px">
                        <option value="">Select</option>
                        <option value="N" selected="selected">NO</option>
                        <option value="Y">YES</option>
                      </select>
                    </div>
                    <span style="position: absolute; display: none;" id="error_four_wh_spa_allw" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-2" style="display: none;" id="four_wh_spa_dis">
                  <div class="form-group">
                    <label class="form-control-label size">No of Space(4W)<span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="SPACE" name="four_wh_spa" id="four_wh_spa">
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">App Use <span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="app_use" id="app_use" style="opacity: 0.8;font-size:14px">
                        <option value="">Select</option>
                        <option value="N" selected="selected">NO</option>
                        <option value="Y">YES</option>
                      </select>
                    </div>
                    <span style="position: absolute; display: none;" id="error_app_use" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Club Membership <span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="club_membership" id="club_membership" style="opacity: 0.8;font-size:14px">
                        <option value="">Select</option>
                        <option value="N" selected="selected">NO</option>
                        <option value="Y">YES</option>
                      </select>
                    </div>
                    <span style="position: absolute; display: none;" id="error_club_membership" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Effective Date<span class="tx-danger">*</span></label>
                    <input type="text" name="effective_date" id="effective_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                    <span style="position: absolute; display: none;" id="error_effective_date" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">End Date</label>
                    <input type="text" name="end_date" id="end_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                   <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2 mg-t-25">
                  <div class="form-layout-footer">
                    <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button mg-b-10" name="save" id="save">
                  </div>
                </div>
                <div class="col-lg-2 mg-t-25">
                  <div class="form-layout-footer">
                    <a href="flat-master"><button class="btn btn-block btn-primary prk_button mg-b-10" type="button">BACK</button></a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- SMALL MODAL -->
      <div id="modaldemo2" class="modal fade">
        <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content bd-0 tx-14">
            <div class="modal-header pd-x-20">
              <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Four Wheeler Parking Lot Name</h6>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form method="post" enctype="multipart/form-data" id="park_lot" name="park_lot" data-parsley-validate>
              <div class="wd-300">
                <div class="d-md-flex mg-b-30 row">
                  <table id="loat_list">
                    
                  </table>
                </div>
                <div class="modal-footer justify-content-center">
                  <!-- <button type="button" class="btn btn-info pd-x-20">Save changes</button> -->
                  <button type="submit" class="btn btn-info pd-x-20">Save</button>
                  <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-secondary pd-x-20">Close</button>
                </div>
              </div>
            </form>
          </div>
        </div><!-- modal-dialog -->
      </div><!-- modal -->
      <!-- data table -->
      <div class="am-pagebody">
        <div class="card pd-10 pd-sm-40">
          <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
                <th >Sl No.</th>
                <th >Owner Name</th>
                <th >Living Status</th>
                <th >Intercome Mobile No.</th>
                <th >2W Space Allow</th>
                <th >4W Space Allow</th>
                <th >App Use</th>
                <th >Tenant Name</th>
                <th >Effective Date</th>
                <th >End Date</th>
              </thead>
              <?php 
                $list_is='N';
                $c=0;
                if($flat_master_list['status']){
                  foreach($flat_master_list['flat_mas_list'] as $value){ $list_is='Y';
                  $c+=1;
                  $liv_name=($value['living_status']=='R')?$value['res_mem_name']:$value['owner_name'];
                  ?>
                    <tr style="<?php if($value['activ_status']==FLAG_N){ ?>background-color: bisque;<?php }?>">
                    <td ><?=$c;?></td>
                    <!-- <td ><?//=$value['owner_name'];?></td> -->
                    <td ><?=$liv_name;?></td>
                    <td ><?=$value['living_status_full'];?></td>
                    <td ><?=$value['inter_com_mobile'];?></td>
                    <td ><?=$value['two_wh_spa_allw_show'];?></td>
                    <td ><?=$value['four_wh_spa_allw_show'];?></td>
                    <td ><?=$value['app_use_show'];?></td>
                    <td ><?=$value['tenant_name'];?></td>
                    <td ><?=$value['effective_date'];?></td>
                    <td ><?=$value['end_date'];?></td>
                    </tr>
                  <?php }
                }
              ?>
            </table>
          </div>
        </div>
      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
  <!-- footer part -->
<script type="text/javascript">
  // $("#tower_name").val("<?$tower_id?>");
  var prk_lot_l= new Array();
  var tower_id="<?=$tower_id?>";
  var tower_name="<?=$tower_name?>";
  var flat_id="<?=$flat_id?>";
  var flat_name="<?=$flat_name?>";
  var list_is="<?=$list_is?>";
  // alert(list_is);
  $("#tower_name").val(tower_id);
  var areaOption ='<option value='+flat_id+'>'+flat_name+'</option>';
  $("#flat_name").html(areaOption);
  $('#tower_name').attr("disabled", true);
  $('#flat_name').attr("disabled", true);
</script>
<script type="text/javascript">
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#effective_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
        if (date < now) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        // $('#error_effective_date').text('');
        return {};
      } 
    });
  });
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#effective_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
        if (date < now_end) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
        }
        return {};
      } 
    });
  });
  $('#effective_date').focus(function(){
    $('#end_date').val('');
  });
</script>
<!-- scripts for datatable -->
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  // alert(prkUrl);
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }/*,
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]*/
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
</script>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE ?>";
  var prk_admin_id="<?=$prk_admin_id?>"
  var parking_admin_name="<?=$parking_admin_name?>"
  var token="<?=$token?>"
  var resident_flag="<?=$resident_flag?>"
  // var resident_flag="R"
  if(resident_flag=='R'){  
    $(".res_mem_name_d").css("display", "");
    $(".owner_name_d").css("display", "none");
    $(".tenant_name_d").css("display", "none");
  }else{    
    $(".res_mem_name_d").css("display", "none");
    $(".owner_name_d").css("display", "");
    $(".tenant_name_d").css("display", "");         
  }
  $('#tenant_id').prop('disabled', true);
  var urlPrkAreGateDtl = prkUrl+'flat_master_add.php';
  $('#save').click(function(){
    var effective_date=($("#effective_date").val());
    if($("#park_flat").valid()){
      var four_wh_spa_allw = $('#four_wh_spa_allw').val();
      if (four_wh_spa_allw=='Y') {
        var four_wh_spa=($("#four_wh_spa").val());
        var prk_lot_html='';
        var i=0;
        // alert(prk_lot_l.length);
        // for(i=1; i<=four_wh_spa; ++i) {
        $.each(prk_lot_l, function(val, key) {
          i++;
          var prk_lot_dtl_id=key['prk_lot_dtl_id'];
          var prk_lot_number=key['prk_lot_number'];
          prk_lot_html+='<tr id="row'+i+'"><td><div class="form-group mg-b-0 mg-md-l-20 cl-md-12">\
            <label>Parking Lot '+i+'<span class="tx-danger">*</span></label>\
            <input type="hidden" value="OLD" name="prk_lot_dtl[]" class="prk_lot_dtl'+i+'">\
            <input type="hidden" value="'+prk_lot_dtl_id+'" name="prk_lot_dtl_id[]" class="prk_lot_dtl_id'+i+'">\
            <input type="text" value="'+prk_lot_number+'" name="prk_lot_number[]" class="form-control wd-200 wd-sm-250 prk_lot_number'+i+'" placeholder="Parking Lot Name" required></td>\
            <td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td>\
          </div></tr>';
        });
        // }
        i=(i+1);
        for(i; i<=four_wh_spa; ++i) {
          prk_lot_html+='<tr id="row'+i+'"><td><div class="form-group mg-b-0 mg-md-l-20 cl-md-12">\
            <label>Parking Lot '+i+'<span class="tx-danger">*</span></label>\
            <input type="hidden" value="NEW" name="prk_lot_dtl[]" class="prk_lot_dtl'+i+'">\
            <input type="hidden" value="" name="prk_lot_dtl_id[]" class="prk_lot_dtl_id'+i+'">\
            <input type="text" name="prk_lot_number[]" class="form-control wd-200 wd-sm-250 prk_lot_number'+i+'" placeholder="Parking Lot Name" required></td>\
            <td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td>\
          </div></tr>';
        }
        $(document).on('click', '.btn_remove', function(){
          var button_id = $(this).attr("id"); 
          $('#row'+button_id+'').remove();
        });
        // alert(i);
        $('#loat_list').html(prk_lot_html);
        $("#modaldemo2").modal();
      }else{

        flat_updat_confirm();
      }
    }
  });
  $('form[name="park_lot"]').submit(function (event) {
    event.preventDefault();
    flat_updat_confirm();
  });
  function flat_updat_confirm(){
    if (list_is=='Y') {
      $.confirm({
        title: 'Are You Sure?',
        content: "<p style='font-size:0.8em;'>Do You Want To Updated Current Flat Details??</p>",
        theme: 'modern',
        type: 'red',
        buttons: {
          cancel: function () {
          },
          somethingElse: {
            text: 'Confirm',
            btnClass: 'btn-red',
            keys: ['Y', 'shift'],
            action: function(){
              Flat_master_ad();
            }
          }
        }
      });
    }else{
      Flat_master_ad();
    }
    // prk_lot_dtl_add(prk_admin_id,'na',parking_admin_name);
  }
  function Flat_master_ad(){
    var effective_date=($("#effective_date").val());
    var str_array = effective_date.split('-');
    var dd = str_array[0];
    var mm = str_array[1];
    var yy = str_array[2];
    var effective_dat = yy+'-'+mm+'-'+dd;
    var end_date=($("#end_date").val());
    var str_array = end_date.split('-');
    var dd = str_array[0];
    var mm = str_array[1];
    var yy = str_array[2];
    var end_dat = yy+'-'+mm+'-'+dd;
    end_date = (!end_date == '') ? end_dat : FUTURE_DATE;
    // alert(end_date);
    var tower_name=($("#tower_name").val());
    var flat_name=($("#flat_name").val());
    var owner_id=($("#owner_id").val());
    var living_status=($("#living_status").val());
    var tenant_id=($("#tenant_id").val());
    var two_wh_spa_allw=($("#two_wh_spa_allw").val());
    var two_wh_spa=($("#two_wh_spa").val());
    var four_wh_spa_allw=($("#four_wh_spa_allw").val());
    var four_wh_spa=($("#four_wh_spa").val());
    var living_status=($("#living_status").val());
    var contact_number=($("#contact_number").val());
    var app_use=($("#app_use").val());
    var club_membership=($("#club_membership").val());
    var res_mem_id=($("#res_mem_id").val());
    var living_name='';
    if (living_status=='O') {
      living_name=($("#owner_name").val());
    }else if (living_status=='T') {
      living_name=($("#tenant_name").val());
    }else if (living_status=='R') {
      living_name=($("#res_mem_name").val());
    }else{
      living_name='';
    }
    // alert(living_name);
    $('#save').val('Wait ...').prop('disabled', true);
    //'prk_admin_id','tower_name','flat_name','owner_name','living_status','tenant_name','effective_date','end_date','parking_admin_name','token','crud_type','flat_master_id'
    $.ajax({
      url :urlPrkAreGateDtl,
      type:'POST',
      data :{
        'prk_admin_id':prk_admin_id,
        'tower_name':tower_name,
        'flat_name':flat_name,
        'owner_name':owner_id,
        'tenant_name':tenant_id,
        'living_name':living_name,
        'two_wh_spa_allw':two_wh_spa_allw,
        'two_wh_spa':two_wh_spa,
        'four_wh_spa_allw':four_wh_spa_allw,
        'four_wh_spa':four_wh_spa,
        'living_status':living_status,
        'effective_date':effective_dat,
        'end_date':end_date,
        'parking_admin_name':parking_admin_name,
        'flat_master_id':'',
        'crud_type':'I',
        'contact_number': contact_number,
        'app_use': app_use,
        'club_membership': club_membership,
        'res_mem_id': res_mem_id,
        'token': token
      },
      dataType:'html',
      success  :function(data){
        $('#save').val('SAVE').prop('disabled', false);
        // alert(data);
        var json = $.parseJSON(data);
        if (json.status){
          var flat_master_id = json.flat_master_id;
          prk_lot_dtl_add(prk_admin_id,flat_master_id,parking_admin_name);
          $.alert({
            icon: 'fa fa-smile-o',
            theme: 'modern',
            title: 'Success !',
            content: "<p style='font-size:0.9em;'>"+json.message+"</p>",
            type: 'green',
            buttons: {
              Ok: function () {
                location.reload(true);
              }
            }
          });
        }else{
          if( typeof json.session !== 'undefined'){
            if (!json.session) {
              window.location.replace("logout.php");
            }
          }else{
            $.alert({
            icon: 'fa fa-frown-o',
            theme: 'modern',
            title: 'Error !',
            content: "<p style='font-size:0.9em;'>"+json.message+"</p>",
            type: 'red'
          });
          }
        }
      }
    });
  }
  function prk_lot_dtl_add(prk_admin_id,flat_master_id,parking_admin_name) {
    var four_wh_spa_allww = $('#four_wh_spa_allw').val();
    var total_space=($("#four_wh_spa").val());
    // if (four_wh_spa_allww=='Y') {
      var lot_dtl_add = prkUrl+'prk_lot_dtl_add.php';
      var full_data=$('#park_lot').serialize()+"&prk_admin_id="+prk_admin_id+"&flat_master_id="+flat_master_id+"&parking_admin_name="+parking_admin_name+"&total_space="+total_space;
      // alert(full_data);
      $.ajax({
        url :lot_dtl_add,
        type:'POST',
        data : full_data,
        cache : false,
        dataType:'html',
        success  :function(data){
          // alert('ok');
          // alert(data);
        }
      });
    // }
  }
</script>
<script type="text/javascript">
  $( document ).ready( function () {
    $( "#park_flat" ).validate( {
      rules: {
        tower_name: "required",
        flat_name: "required",
        owner_id: "valueNotEqualsOwner",
        res_mem_id: "valueNotEqualsResMember",
        living_status: "required",
        tenant_id: "valueNotEqualsTenant",
        contact_number: {
          required: true,
          number: true,
          minlength: 10,
          maxlength: 10
        },
        two_wh_spa_allw: "required",
        two_wh_spa: {
          required: true,
          number: true,
          min: 1
        },
        four_wh_spa_allw: "required",
        four_wh_spa: {
          required: true,
          number: true,
          min: 1
        },
        effective_date: "required",
        app_use: "required",
        club_membership: "required"
      },
      messages: {
        owner_id:'',
        res_mem_id:'',
        tenant_id:'',
        contact_number: {
          required: 'This field is required.',
          number: 'Enter valid Number.',
          minlength: 'Minimum Length 10th Digit.',
          maxlength: 'Maximum  Length 10th Digit.'
        },
        two_wh_spa: {
          required: 'This field is required.',
          number: 'Enter valid Number.',
          min: "Minimum Space 1."
        },
        four_wh_spa: {
          required: 'This field is required.',
          number: 'Enter valid Number.',
          min: "Minimum Space 1."
        }
      }
    });
    $("#owner_id").on('change', function(){
      var v_type = $('#owner_id').val();
      search_owner(v_type)
      if(v_type == ""){
        $('#error_owner_id').show();
        return false;
      }else{
        $('#error_owner_id').hide();
        return true;
      }
    });
    $("#res_mem_id").on('change', function(){
      var v_type = $('#res_mem_id').val();
      search_resmember(v_type);
      if(v_type == ""){
        $('#error_res_mem_id').show();
        return false;
      }else{
        $('#error_res_mem_id').hide();
        return true;
      }
    });
    $("#tenant_id").on('change', function(){
      var v_type = $('#tenant_id').val();
      search_tenant(v_type);
      if(v_type == ""){
        $('#error_tenant_id').show();
        return false;
      }else{
        $('#error_tenant_id').hide();
        return true;
      }
    });
    $("#living_status").on('change', function(){
      // var v_type = $('#living_status').val();
      living_status_dis();
      /*if(v_type == "")
      {
        $('#error_living_status').show();
        return false;
      }else{
        $('#error_living_status').hide();
        return true;
      }*/
    });
    $("#two_wh_spa_allw").on('change', function(){
      var two_wh_spa_allw = $('#two_wh_spa_allw').val();
      two_wh_spa_allw_dis(two_wh_spa_allw);
      /*if(two_wh_spa_allw == "")
      {
        $('#error_two_wh_spa_allw').show();
        return false;
      }else{
          $('#error_two_wh_spa_allw').hide();
          return true;
      }*/
    });
    $("#four_wh_spa_allw").on('change', function(){
      var four_wh_spa_allw = $('#four_wh_spa_allw').val();
      four_wh_spa_allw_dis(four_wh_spa_allw);
      /*if(four_wh_spa_allw == "")
      {
        $('#error_four_wh_spa_allw').show();
        return false;
      }else{
          $('#error_four_wh_spa_allw').hide();
          return true;
      }*/
    });
  });
  $.validator.addMethod("valueNotEqualsOwner", function(value){
    // alert(value);
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_owner_id").show();
      return false;
    }
  });
  $.validator.addMethod("valueNotEqualsResMember", function(value){
    // alert(value);
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_res_mem_id").show();
      return false;
    }
  });
  $.validator.addMethod("valueNotEqualsTenant", function(value){
    // alert(value);
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_tenant_id").show();
      return false;
    }
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
  function living_status_dis() {
    var liv_status = $('#living_status').val();
    if(liv_status=='T'){  
      // $("#tenant_name_dis").css("display", "");
      $('#tenant_id').prop('disabled', false);
    }else{            
      // $("#tenant_name_dis").css("display", "none");
      $('#error_tenant_id').hide();
      $('#tenant_id').val('');
      $('#tenant_id').prop('disabled', true);
      // $("#tenant_id option:selected").value('');
    }
    //--------------------//
    if(liv_status=='R'){
      var con_mob=($("#res_mem_mobile").val());
      $("#contact_number").val(con_mob);
    }else if(liv_status=='O'){
      var con_mob=($("#owner_mobile").val());
      $("#contact_number").val(con_mob);
    }else if(liv_status=='T'){
      var con_mob=($("#tenant_mobile").val());
      $("#contact_number").val(con_mob);
    }else{
      $("#contact_number").val('');
    } 
  }
  function two_wh_spa_allw_dis(liv_status) {
    // alert(liv_status);
    if(liv_status=='Y'){  
      $("#two_wh_spa_dis").css("display", "");
    }else{            
      $("#two_wh_spa_dis").css("display", "none");
      $('#two_wh_spa').val('0');
    }        
  }
  function four_wh_spa_allw_dis(liv_status) {
    // alert(liv_status);
    if(liv_status=='Y'){  
      $("#four_wh_spa_dis").css("display", "");
    }else{            
      $("#four_wh_spa_dis").css("display", "none");
      $('#four_wh_spa').val('0');
    }        
  }
</script>
<!-- <script type="text/javascript">
  $('#tower_name').on("change", function() {
    var id=($("#tower_name").val());
    var urlCity = prkUrl+'flat_list.php';
    $.ajax ({
      type: 'POST',
      url: urlCity,
      data: {
        tower_id:id,
        prk_admin_id:prk_admin_id,
        parking_admin_name:parking_admin_name,
        token:token
      },
      success : function(data) {
        // alert(data);
        var obj=JSON.parse(data);
        var areaOption = "<option value=''>Select Flat</option>";
        // alert(obj['status']);
        if (obj['status']) {
          $.each(obj['flat_list'], function(val, key) {
            areaOption += '<option value="' + key['flat_id'] + '">' + key['flat_name'] + '</option>'
          });
        }
        $("#flat_name").html(areaOption);
      }
    });
  });
</script> -->
<script>
  $('.select2').select2();
  function search_owner(str) {
    var xhttp;
    if (str.length == 0) {
      return;
    }
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var json = $.parseJSON(this.responseText);
        if (json.status) {
          // alert(json.emp_name);
          $('#owner_name').val(json.emp_name);
          $('#owner_mobile').val(json.owner_mobile);
          living_status_dis();
        }else{
          alert(json.message);
        }
      }
    };
    xhttp.open("GET", "p_admin_middle_tire/owner_list.php?q="+str+"&pid="+prk_admin_id+"&type=SE", true);
    xhttp.send();
  }
  function search_tenant(str) {
    var xhttp;
    if (str.length == 0) {
      return;
    }
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var json = $.parseJSON(this.responseText);
        if (json.status) {
          // $('#tenant_id').val(json.emp_id);
          // alert(json.emp_name);
          $('#tenant_name').val(json.emp_name);
          $('#tenant_mobile').val(json.tenant_mobile);
          living_status_dis();
        }else{
          alert(json.message);
        }
      }
    };
    xhttp.open("GET", "p_admin_middle_tire/tenant_list.php?q="+str+"&pid="+prk_admin_id+"&type=SE", true);
    xhttp.send();
  }
  function search_resmember(str) {
    var xhttp;
    if (str.length == 0) {
      return;
    }
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var json = $.parseJSON(this.responseText);
        if (json.status) {
          // $('#res_mem_id').val(json.res_mem_id);
          // alert(json.res_mem_name);
          $('#res_mem_name').val(json.res_mem_name);
          $('#res_mem_mobile').val(json.res_mem_mobile);
          living_status_dis();
        }else{
          alert(json.message);
        }
      }
    }; 
    xhttp.open("GET", "p_admin_middle_tire/res_mem_name_search.php?q="+str+"&pid="+prk_admin_id+"&type=SE", true);
    xhttp.send();
  }
</script>
<script type="text/javascript">
  var flat_master_id="<?=$flat_master_id?>"
  // var owner_id="<?=$owner_id?>"
  var owner_name="<?=$owner_name?>";
  var owner_mobile="<?=$owner_mobile?>";
  var living_status="<?=$living_status?>";
  // var living_name="<?=$living_name?>";
  // var tenant_id="<?=$tenant_id?>";
  var tenant_name="<?=$tenant_name?>";
  var tenant_mobile="<?=$tenant_mobile?>";
  // var res_mem_id="<?=$res_mem_id?>";
  var res_mem_name="<?=$res_mem_name?>";
  var res_mem_mobile="<?=$res_mem_mobile?>";
  var inter_com_mobile="<?=$inter_com_mobile?>";
  var two_wh_spa_allw="<?=$two_wh_spa_allw?>";
  var two_wh_spa="<?=$two_wh_spa?>";
  var four_wh_spa_allw="<?=$four_wh_spa_allw?>";
  var four_wh_spa="<?=$four_wh_spa?>";
  var app_use="<?=$app_use?>";
  var club_membership="<?=$club_membership?>";
  var effective_date="<?=$effective_date?>";
  var end_date="<?=($end_date==FUTURE_DATE_WEB)?'':$end_date?>";

  $('#owner_name').val(owner_name);
  $('#owner_mobile').val(owner_mobile);

  $('#res_mem_name').val(res_mem_name);
  $('#res_mem_mobile').val(res_mem_mobile);

  $('#living_status').val(living_status);

  $('#tenant_name').val(tenant_name);
  $('#tenant_mobile').val(tenant_mobile);

  $('#contact_number').val(inter_com_mobile);

  two_wh_spa_allw_dis(two_wh_spa_allw);
  $('#two_wh_spa_allw').val(two_wh_spa_allw);
  $('#two_wh_spa').val(two_wh_spa);

  four_wh_spa_allw_dis(four_wh_spa_allw);
  $('#four_wh_spa_allw').val(four_wh_spa_allw);
  $('#four_wh_spa').val(four_wh_spa);

  $('#app_use').val(app_use);
  $('#club_membership').val(club_membership);

  $('#effective_date').val(effective_date);
  $('#end_date').val(end_date);
  if(tenant_name!=''){
    $('#tenant_id').prop('disabled', false);
  }
  var flat_prk_lot_list = prkUrl+'flat_prk_lot_list.php';
  $.ajax ({
    type: 'POST',
    url: flat_prk_lot_list,
    data: {
      'prk_admin_id':prk_admin_id,
      'flat_master_id':flat_master_id,
      'token':token
    },
    success : function(data) {
      // alert(data);
      var obj=JSON.parse(data);
      var areaOption = "<option value=''>Select Lot No</option>";
      if (obj['status']) {
        prk_lot_l=obj['prk_lot_list'];
        // $.each(obj['prk_lot_list'], function(val, key) {
        //   areaOption += '<option value="' + key['prk_lot_number'] + '">' + key['prk_lot_number'] + '</option>';
        // });
      }
      // $("#park_lot_no").html(areaOption);
    }
  });
</script>

