<!-- ################################################
  
Description: Parking area can edit gate pass.
Developed by: Rakhal Raj Mandal
Created Date: 11-01-2019

 ####################################################--><?php 
include "all_nav/header.php";
if (isset($_SESSION['prk_admin_id'])) {
  $user_name=$_SESSION['prk_admin_id'];
  $prk_admin_id = $_SESSION['prk_admin_id'];
}
$user_incident_id=base64_decode($_REQUEST['list_id']);
$incident_show1=incident_show($user_incident_id);
$incident_show = json_decode($incident_show1, true);
$prk_sub_user_list =prk_sub_user_list($prk_admin_id);
?>
<style>
  .size{
    font-size: 11px;
  }
 .error_size{
    font-size: 11px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
<!-- header position -->
    <div class="am-mainpanel"><!-- cloding in footer -->
      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Incident Verify<?php //echo($incident_show1); ?></h5>
      </div><!-- am-pagetitle -->
      <div class="am-pagebody">
        <div class="card single pd-20 pd-sm-40 col-md-12">

          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <div class="row">
              <div class="col-md-12">
                
                  <form id="addGate" method="get" action="som">
                    <div class="form-layout">
                      <div class="row mg-b-25">
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                              <label class="form-control-label size">Incident Type <span class="tx-danger">*</span></label>
                              <input type="text" class="form-control readonly" placeholder="Incident Type" name="user_incident_type_name" id="user_incident_type_name">
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                              <label class="form-control-label size">Incident Name <span class="tx-danger">*</span></label>
                              <input type="text" class="form-control readonly" placeholder="Incident Name" name="user_incident_name" id="user_incident_name">
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                              <label class="form-control-label size">Incident Number <span class="tx-danger">*</span></label>
                              <input type="text" class="form-control readonly" placeholder="Incident Number" name="user_incident_number" id="user_incident_number">
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                              <label class="form-control-label size">Issue Date<span class="tx-danger">*</span></label>
                              <input type="text" class="form-control readonly" placeholder="Incident Number" name="incident_in_date" id="incident_in_date">
                          </div>
                        </div>
                        <div class="col-lg-6 error_show">
                          <div class="form-group">
                              <label class="form-control-label size">Incident Dtl<span class="tx-danger">*</span></label>
                              <input type="text" class="form-control readonly" placeholder="Incident Dtl" name="user_incident_dtl" id="user_incident_dtl">
                          </div>
                        </div>
                        <div class="col-lg-6 error_show">
                          <div class="form-group">
                            <label class="form-control-label size">Remarks<span class="tx-danger"></span></label>
                            <input type="text" class="form-control" placeholder="Remarks" name="remarks" id="remarks">
                          </div>
                        </div>
                        <div class="col-lg-4 error_show">
                          <div class="form-group">
                            <label class="form-control-label size">Select Employee<span class="tx-danger">*</span></label>
                            <div class="select">
                              <select name="prk_area_user" id="prk_area_user" style="opacity: 0.8">
                                <option value="">SELECT EMPLOYEE</option>
                                <?php
                                  $count = count($prk_sub_user_list);
                                  if ($count == 0) {
                                ?>
                                    <option value="NA">NA</option>
                                <?php     
                                  }else{
                                    foreach ($prk_sub_user_list as $val){
                                ?>
                                      <option value="<?php echo $val['prk_area_user_id']?>"><?php echo $val['prk_user_name']?></option>
                                <?php
                                    }  
                                  } 
                                ?>
                              </select>
                            </div>
                            <span style="position: absolute; display: none;" id="error_prk_area_user" class="error_size">Please Select Employee</span>
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label size">Incident Status<span class="tx-danger">*</span></label>
                            <div class="select">
                              <select name="status_i" id="status_i" style="opacity: 0.8">
                                <option value="">SELECT EMPLOYEE</option>
                                <option value="D">DRAFT</option>
                                <option value="I">IN PROGRESS</option>
                                <option value="F">FIXED</option>
                                <option value="N">NOT FIXED</option>
                              </select>
                            </div>
                            <span style="position: absolute; display: none;" id="error_prk_area_user" class="error_size">Please Select Employee</span>
                          </div>
                        </div>
                        <div class="col-lg-4">
                          <div class="form-layout-footer mg-t-30">
                            <div class="row">
                              <div class="col-lg-6">
                                <input type="button" value="VERIFY" class="btn btn-block btn-primary prk_button" name="save" id="save">
                                <span style="position: absolute;" id="error_all" class="error_size"></span>
                              </div>
                              <div class="col-lg-6">
                                <button type="button" class="btn btn-block prk_button_skip" name="skip" id="skip" onclick="document.location.href='incident-dtl';">Back</button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div><!-- form-layout -->
                  </form>
              </div>
              
            </div>
          </div>
        </div><!-- card -->
      </div><!-- am-pagebody -->
      

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE_WEB ?>";
  var user_incident_id = "<?php echo $user_incident_id;?>";
  var prk_admin_id = "<?php echo $prk_admin_id;?>";
  var token = "<?php echo $token;?>";
  $( document ).ready( function () {
    $('#user_incident_type_name').val("<?php echo($incident_show['user_incident_type_name']);?>");
    $('#user_incident_name').val("<?php echo($incident_show['user_incident_name']);?>");
    $('#user_incident_number').val("<?php echo($incident_show['user_incident_number']);?>");
    $('#incident_in_date').val("<?php echo($incident_show['incident_in_date']);?>");
    $('#user_incident_dtl').val("<?php echo($incident_show['user_incident_dtl']);?>");
    $('#prk_area_user').val("<?php echo($incident_show['prk_area_user_id']);?>");
    $('#status_i').val("<?php echo($incident_show['u_status']);?>");
    // alert("<?php echo($incident_show['status']);?>");
        // alert('id');
    /*FORM SUBMIT*/
    $( "#prk_area_user" ).change(function() {
      var prk_area_user = $('#prk_area_user').val();
      prk_area_useraa(prk_area_user);
    });
    $('#save').click(function(){
      var prk_area_user = $('#prk_area_user').val();
      var remarks = $('#remarks').val();
      var status = $('#status_i').val();
      if (prk_area_useraa(prk_area_user)==true) {
        // alert('kkk');
        var incident_verify = prkUrl+'prk_admin_incident_verify.php';
        // alert(incident_verify)
        // var status='I';
        //'user_incident_id','prk_admin_id','prk_area_user_id','status','remarks','token'
        $('#save').val('Wait ...').prop('disabled', true);
        $.ajax({
          url :incident_verify,
          type:'POST',
          data :
          {
            'user_incident_id':user_incident_id,
            'prk_admin_id':prk_admin_id,
            'prk_area_user_id':prk_area_user,
            'status':status,
            'remarks':remarks,
            'token': token
          },
          dataType:'html',
          success  :function(data)
          {
            $('#save').val('VERIFY').prop('disabled', false);
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status){
                  $.alert({
                  icon: 'fa fa-smile-o',
                  theme: 'modern',
                  title: 'Success !',
                  content: "<p style='font-size:0.9em;'>Incident Verify successfully</p>",
                  type: 'green',
                  buttons: {
                    Ok: function () {
                        window.location='incident-dtl';
                    }
                  }
                });
              }else{
                  if( typeof json.session !== 'undefined'){
                    if (!json.session) {
                      window.location.replace("logout.php");
                    }
                  }else{
                    $.alert({
                    icon: 'fa fa-frown-o',
                    theme: 'modern',
                    title: 'Error !',
                    content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                    type: 'red'
                  });
                  }
                }
          }
        });
      }
     
    });
  });
  function prk_area_useraa(id){
    if(id!=''){
      $('#error_prk_area_user').hide();
      return true;
    }else{
      $('#error_prk_area_user').show();
      return false;
    }
  }
</script>