<?php 
/*
Description: Image compress. 
Developed by: Manoranjan
Created Date: 03-07-2018
Update date :
*/
function api_image_upload_compress($vis_img){
	$n_width=200;
	$n_height=200;
	$strc=$vis_img;
	$source_image=imagecreatefromjpeg($vis_img);
	$width=imagesx($source_image);
	$height=imagesy($source_image);
	$virtual_image=imagecreatetruecolor($n_width, $n_height);
	imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $n_width, $n_height, $width, $height);
	imagejpeg($virtual_image,$strc);

}
?>