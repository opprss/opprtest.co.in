<!-- ################################################
  
Description: user can add or modify address here. add and modify is difided with a reffer(from where page requist is comming that URL) variable in JS.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
  include "all_nav/header.php"; 
  if (isset($_SESSION['user_admin_id'])) {

    $user_admin_id = $_SESSION['user_admin_id'];
    $response = array();
    $response_address = array();
    $prk_area_list = prk_area_list();
    $prk_area_list = json_decode($prk_area_list, true);
    $state = state();
    $state = json_decode($state, true);
  }
?>
<style>
  .user-lebel{
  	text-transform: uppercase!importent;
  }
    .size{
      font-size: 11px;
    }
  .error_size{
    font-size: 11px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
<!-- header position -->
    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Address</h5>
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card single pd-20 pd-sm-40 col-md-12">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">

            <div class="row">
              <div class="col-md-12">
                <div id="check">
                  
                </div>
                  <form id="regForm2" method="post" action="">
                    <div class="form-layout">
                      <div class="row mg-b-0">
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Parking Area Name<?php //echo $prk_area_list;?></label>
                            <div class="select" style="font-size:14px">
                              <select name="prk_area_name" id="prk_area_name" value="null" style="color: skyblue">
                                <option value="">Select Parking Area</option>
                                <?php
                                  foreach ($prk_area_list['prk_area'] as $val){ ?>  
                                    <option value="<?php echo $val['prk_admin_id']?>"><?php echo $val['prk_area_name']?></option>
                                  <?php          
                                  }
                                ?> 
                              </select>
                            </div>
                            <span style="position: absolute;" id="error_prk_area" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Address <span class="tx-danger">*</span></label>

                            <input type="text" class="form-control" placeholder="Address" name="address" id="address" onkeyup="addressUppercase()" value="">
                            <span style="position: absolute;" id="error_address" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">landmark </label>

                            <input type="text" class="form-control" placeholder="landmark" name="landmark" id="landmark" onkeyup="landmarkUppercase()" value="">
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Area <span class="tx-danger">*</span></label>

                            <input type="text" class="form-control" placeholder="Area" name="area" id="area" onkeyup="areaUppercase()" value="">
                            <span style="position: absolute;" id="error_area" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Country <span class="tx-danger">*</span></label>

                            <input type="text" class="form-control readonly" value="INDIA" name="country" id="country" disabled="disabled" style="background:transparent;">
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Select state <?php //echo $state; ?> <span class="tx-danger">*</span></label>
                              <div class="select" style="font-size:14px">
                                <select name="state" id="state" value="null" style="color: skyblue">
                                  <option value="">SELECT STATE</option>
                                   <?php
                                    foreach ($state['state'] as $val){ ?>  
                                      <option value="<?php echo $val['sta_id']?>"><?php echo $val['state_name']?></option>
                                    <?php          
                                    }
                                  ?> 
                                </select>
                              </div>
                              <span style="position: absolute;" id="error_state" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Select City <span class="tx-danger">*</span></label>
                            <div class="select" style="font-size:14px">
                              <select name="city" id="city" value="null" style="color: skyblue">
                                <option value=''>SELECT CITY</option>
                              </select>
                            </div>
                            <span style="position: absolute;" id="error_city" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <label class="form-control-label user-lebel">Pin Code <span class="tx-danger">*</span></label>

                          <div class="form-group" id="staticParent">
                            <input type="text" class="form-control" placeholder="Pin Code" name="pin" id="pin" maxlength="6"  value="">
                            <span style="position: absolute;" id="error_pin" class="error_size"></span>
                          </div>
                        </div>
                        <!-- <div class="col-lg-3 error_show red_show">
                          <label class="form-control-label user-lebel">Tower Name <span class="tx-danger"></span></label>

                          <div class="form-group" id="staticParent">
                            <input type="text" class="form-control" placeholder="Tower Name" name="tower_name" id="tower_name"  value="">
                            <span style="position: absolute;" id="error_tower_name" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-3 error_show red_show">
                          <label class="form-control-label user-lebel">Flat Name <span class="tx-danger"></span></label>

                          <div class="form-group" id="staticParent">
                            <input type="text" class="form-control" placeholder="Flat Name" name="falt_name" id="falt_name"  value="">
                            <span style="position: absolute;" id="error_falt_name" class="error_size"></span>
                          </div>
                        </div> -->
                        <div class="col-lg-3 error_show red_show">
                          <div class="form-group">
                            <label class="form-control-label size">TOWER NAME <span class="tx-danger"></span></label>
                            <div class="select">
                              <select name="tower_name" id="tower_name" style="opacity: 0.8;font-size:14px">
                                <option value="">SELECT TOWER</option>
                              </select>
                            </div>
                          <span style="position: absolute;" id="error_tower_name" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-3 error_show red_show">
                          <div class="form-group">
                            <label class="form-control-label size">FLAT NAME <span class="tx-danger"></span></label>
                            <div class="select">
                              <select name="flat_name" id="flat_name" style="opacity: 0.8;font-size:14px">
                                <option value="">SELECT FLAT</option>
                              </select>
                            </div>
                          <span style="position: absolute;" id="error_falt_name" class="error_size"></span>
                          </div>
                        </div>
                        <?php
                          if (isset($_SESSION['error'])) {
                            echo "<p style=color:red>".$_SESSION['error']."</p>";
                            unset($_SESSION['error']);
                          }
                        ?>
                        <div class="col-lg-2 error_show">
                          <div class="form-group">
                            <input type="button" value="SAVE" class="btn btn-block btn-primary wl_user_button" name="save" id="save">
                          </div>
                        </div>
                        <div class="col-lg-2 error_show">
                          <div class="form-group">
                            <button type="button" class="btn btn-block btn-primary wl_user_button" name="skip" id="skip">BACK</button>
                          </div>
                        </div>
                    </div><!-- form-layout -->
                  </form>
              </div>
              
            </div>
          </div>
        </div><!-- card -->
      </div><!-- am-pagebody -->
      

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">

  $('#prk_area_name').on("change", function() {
    var userUrl = "<?php echo USER_URL; ?>";
    var id=($("#prk_area_name").val());
    var user_admin_id = "<?php echo $_SESSION["user_admin_id"];?>";
    var urlCity = userUrl+'tower_list.php';
    $.ajax ({
      type: 'POST',
      url: urlCity,
      data: {
        prk_admin_id:id,
        user_admin_id:user_admin_id,
        token:"<?php echo $token?>"
      },
      success : function(data) {
        //alert(data);
        var obj=JSON.parse(data);
        var areaOption = "<option value=''>SELECT TOWER</option>";
        // alert(obj['status']);
        if (obj['status']) {
          $.each(obj['tower_list'], function(val, key) {
            areaOption += '<option value="' + key['tower_id'] + '">' + key['tower_name'] + '</option>'
          });
        }
        $("#tower_name").html(areaOption);
      }
    });
  });

  $('#tower_name').on("change", function() {
    var userUrl = "<?php echo USER_URL; ?>";
    var id=($("#tower_name").val());
    var prk_admin_id=($("#prk_area_name").val());
    var user_admin_id = "<?php echo $_SESSION["user_admin_id"];?>";
    var urlCity = userUrl+'flat_list.php';
    $.ajax ({
      type: 'POST',
      url: urlCity,
      data: {
        tower_id:id,
        prk_admin_id:prk_admin_id,
        user_admin_id:user_admin_id,
        token:"<?php echo $token?>"
      },
      success : function(data) {
        //alert(data);
        var obj=JSON.parse(data);
        var areaOption = "<option value=''>SELECT FLAT</option>";
        // alert(obj['status']);
        if (obj['status']) {
          $.each(obj['flat_list'], function(val, key) {
            areaOption += '<option value="' + key['flat_id'] + '">' + key['flat_name'] + '</option>'
          });
        }
        $("#flat_name").html(areaOption);
      }
    });
  });
</script>
<script type="text/javascript">
  // alert('ok');
  var userUrl = "<?php echo USER_URL; ?>";
  var userBaseUrl = "<?php echo USER_BASE_URL; ?>";
  var referrer =  document.referrer;
  var FromProfile = userBaseUrl+'profile';
  var firstTime = userBaseUrl+"signup";
  $( document ).ready( function () {
    $(".red_show").css("display", "none");
    // $(".red_show").css("display", "");
    $('#address').attr("disabled", true);
    $('#landmark').attr("disabled", true);
    $('#area').attr("disabled", true);
    $('#pin').attr("disabled", true);
    $('#state').attr("disabled", true);
    $('#city').attr("disabled", true);
    // alert('ok');
    $('#state').on("change", function() {
      var id=($("#state").val());
      // alert(id);
      var urlCity = userUrl+'city.php';
      $.ajax ({
        type: 'POST',
        url: urlCity,
        data: {sta_id:id},
        success : function(data) {
          //alert(data);
          var obj=JSON.parse(data);
          //console.log(obj);
          var areaOption = "<option value=''>SELECT CITY</option>";
          $.each(obj['city'], function(val, key) {
            areaOption += '<option value="' + key['city_id'] + '">' + key['city_name'] + '</option>'
          });
          $("#city").html(areaOption);
        }
      });
    });
    $('#prk_area_name').on("change", function(){
      var prk_admin_id = $('#prk_area_name').val();           
      // alert(prk_admin_id);
      if(prk_admin_id==000){
        $(".red_show").css("display", "none");
        $('#address').val('');
        $('#landmark').val('');
        $('#area').val('');
        $('#pin').val('');
        $('#state').val('');
        $('#falt_name').val('');
        $('#tower_name').val('');
        $("#city").val('');
        $('#address').attr("disabled", false);
        $('#landmark').attr("disabled", false);
        $('#area').attr("disabled", false);
        $('#pin').attr("disabled", false);
        $('#state').attr("disabled", false);
        $('#city').attr("disabled", false);
      }else{
        $(".red_show").css("display", "");
        var falt_name='';
        var tower_name='';
        var demoLines = '';
        // alert(prk_admin_id);
        var urlVehNoCheck = userUrl+'prk_area_address_show.php';
        $.ajax({
          url :urlVehNoCheck,
          type:'POST',
          data :
          {
            'prk_admin_id':prk_admin_id
          },
          dataType:'html',
          success  :function(data)
          {
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status) {
              demoLines +='<option value='+json.prk_add_city+'>'+json.city_name+'</option>';            
              $('#address').val(json.prk_address);
              $('#landmark').val(json.prk_land_mark);
              $('#area').val(json.prk_add_area);
              $('#pin').val(json.prk_add_pin);
              $('#state').val(json.prk_add_state);
              $('#falt_name').val(falt_name);
              $('#tower_name').val(tower_name);
              $("#city").html(demoLines);

          
              $('#address').attr("disabled", true);
              $('#landmark').attr("disabled", true);
              $('#area').attr("disabled", true);
              $('#pin').attr("disabled", true);
              $('#state').attr("disabled", true);
              $('#city').attr("disabled", true);
            }else{
              location.reload();
            }
          }
        });
      }
    });
    $('#save').click(function(){
      // alert('ok');

      var address = $('#address').val();
      var landmark = $('#landmark').val();
      var area = $('#area').val();
      var country = $('#country').val();
      var state = $('#state :selected').val();
      var city = $("#city :selected").val();
      var pin = $('#pin').val();
      var user_admin_id = "<?php echo $_SESSION["user_admin_id"];?>";
      var mobile = "<?php echo $user_mobile; ?>";
      if(address!='' && area!='' && state!='' && city!='' && pin!=''){
        $('#error_address').text('');
        $('#error_area').text('');
        $('#error_state').text('');
        $('#error_city').text('');
        $('#error_pin').text('');
        // alert('ok');
        ajaxCall();
      }else{

        if (address == '') {
          $('#error_address').text('Address is required');
        }
        if (area == '') {
           $('#error_area').text('Area is required');
        }
        if (state == '') {
          $('#error_state').text('State is required');
        }
        if (city == '') {
          $('#error_city').text('City is required');
        }
        if (pin == '') {
          $('#error_pin').text('Pin is required');
        }
      }
      return false;
    });
    function ajaxCall(){
      var address = $('#address').val();
      var landmark = $('#landmark').val();
      var area = $('#area').val();
      var country = $('#country').val();
      var state = $('#state :selected').val();
      var city = $("#city :selected").val();
      var pin = $('#pin').val();
      var prk_area_name = $('#prk_area_name').val();
      //var address_name = $('#address_name').val();
      var tower_name = $('#tower_name').val();
      var falt_name = $('#flat_name').val();
      var user_admin_id = "<?php echo $_SESSION["user_admin_id"];?>";
      var mobile = "<?php echo $user_mobile; ?>";
      // alert('ok');
      $('#save').val('Wait ...').prop('disabled', true);
      var urlUserEditAddress = userUrl+'user_address.php';
      $.ajax({
        url :urlUserEditAddress,
        type:'POST',
        data :
        {
          'user_admin_id':user_admin_id,
          'user_address':address,
          'landmark':landmark,
          'user_add_area':area,
          'user_add_country':country,
          'user_add_state':state,
          'user_add_city':city,
          'user_add_pin':pin,
          'prk_admin_id':prk_area_name,
          //'address_name':address_name,
          'tower_name':tower_name,
          'falt_name':falt_name,
          'mobile':mobile,
          'token':"<?php echo $token?>"
        },
        dataType:'html',
        success  :function(data)
        {
          // alert(data);
          $('#save').val('SAVE').prop('disabled', false);
          if (data = true) {
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>Address Add successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                  // window.location=redirect;
                  window.location='address-list';
                }
              }
            });
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.8em;'>Something went wrong</p>",
                type: 'red'
              });
            }
          }
        }
      });
    }
    /*--------blar-------*/
    $("#address").blur(function () {
      var address=$('#address').val();
      if(address==''){
      $('#error_address').text('Address is required');

      }else{
        $('#error_address').text('');
      }
    });
    $("#area").blur(function () {
      var area=$('#area').val();
      if(area==''){
      $('#error_area').text('Area is required');

      }else{
        $('#error_area').text('');
      }
    });
    $("#state").blur(function () {
      var state=$('#state').val();
      if(state==''){
      $('#error_state').text('State is required');
      }else{
        $('#error_state').text('');
      }
    });
    $("#city").blur(function () {
      var city =$("#city").find("option:selected").text();
      if(city==''){
      $('#error_city').text('City is required');

      }else{
        $('#error_city').text('');
      }
    });
    $('#pin').blur(function(){
      var prk_add_pin = $('#pin').val();
      if(prk_add_pin == ''){
        $('#error_pin').text("Pin is required");
        return false;
      }else if(prk_add_pin.length != 6){
        $('#error_pin').text("Must be 6 characters long");
        return false;
      }else{
        $('#error_pin').text('');
        return true;
      }
    });
    $("#pin").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0)){
        event.preventDefault();
      }
    });
    $('#skip').click(function(){

      window.location = 'address-list';
    });
  });
  // uppercase
  function addressUppercase() {
      var x = document.getElementById("address");
      x.value = x.value.toUpperCase();
  }
  function landmarkUppercase() {
      var x = document.getElementById("landmark");
      x.value = x.value.toUpperCase();
  }
  function areaUppercase() {
      var x = document.getElementById("area");
      x.value = x.value.toUpperCase();
  }
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>