<?php
  include('global_asset/config.php');
  session_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>OPPRSS | FORGATE PASSWORD</title>
    <link rel="shortcut icon" href="<?php echo FAVICON ?>" type="image/x-icon">
    <!-- vendor css -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">
    <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <script src="lib/jquery/jquery.js"></script>
    <!-- <link rel="stylesheet" type="text/css" href="../css/style.css"> -->
    <!-- Amanda CSS -->
    <link rel="stylesheet" href="css/amanda.css">
    <link rel="stylesheet" href="css/style-up.css">
    <!-- popup -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <style>
      .size{
        font-size: 11px;
      }
      .error_size{
        font-size: 11px;
        color: red;
      }
      .success{
        font-size: 11px;
        color: green;
      }
    </style>
  </head>
  <body>
    <div class="am-signin-wrapper">
      <div class="am-signin-box">
        <div class="row no-gutters">
          <div class="col-lg-7" style="background:url('img/index-banner.jpg');padding-bottom: 0px;">
            <div class="signin-logo">
              <div style="text-align: left;">
              <a href="http://opprss.com" target="_blank"><img style="width: 210px; text-align: left;" src="<?php echo LOGO ?>"></a></div>
              <div class="content">
                <h1>Welcome to OPPRSS</h1>
                <h5 class="d-none d-sm-block pd-b-10">We provide an innovative and executive parking & security solution that improves parking & security experience more than ever before.</h5>  
              </div>
              <div class="row features-icon">
                <div class="col-lg-4">
                  <i class="fa fa-tasks fa-3x"></i>
                  <h6 class="pd-t-5">Digitally Tracked</h6>
                </div> 
                <div class="col-lg-4">
                  <i class="fa fa-american-sign-language-interpreting fa-3x"></i>
                  <h6 class="pd-t-5">Better Communication</h6>
                </div> 
                <div class="col-lg-4">
                  <i class="fa fa-android fa-3x "></i>
                  <h6 class="pd-t-5">App Based</h6>
                </div>                
              </div>                           
              <div class="content-footer features-icon mg-t-50" style="text-align: right;">
                <button class="btn-tab" onclick="window.open('https://www.opprss.com/contact-us', '_blank');" id="con_us">Contact Us</button>
                <button class="btn-tab" id="user_tab">User</button>
                <button class="btn-tab" id="admin_tab">Parking Area</button>
                <button class="btn-tab" id="super_admin_tab">Super Adnin</button>
                 <hr>
                <p class="tx-center tx-white-5 tx-12 mg-t-0 mg-b-0"><?php echo COPYRIGHT_EMAIL?></p>
              </div>
            </div>
          </div>
          <div class="col-lg-5 custom-width">
            <!-- <h5 class="tx-gray-800 mg-b-25">Forget Password</h5> -->
            <form id="forgatePassForm" method="post" action="">

              <h2 class="tx-gray-800 mg-b-50 text-center">Forget Password</h2>
              <div class="form-group tx-center mg-b-30" id="tab">
                <!-- <div class="col-lg-12"> -->
                  <div class="input-group">
                    <div id="radioBtn" class="btn-group">
                    <div class="row">
                      <div class="col-lg-6" style="margin-left: -18px;"><a class="btn btn-custom active radio" data-toggle="login_for" data-title="P" name="login_for" id="parking" style="width: 150px">Parking Area</a></div>

                      <div class="col-lg-6" style="margin-left: -18px;"><a class="btn btn-custom notActive radio" data-toggle="login_for" data-title="U" name="login_for" value="u" id="user" style="width: 150px;">User</a></div>
                    </div>
                    </div>
                    <input type="hidden" name="login_for" id="login_for" value="P">
                  </div>
                <!-- </div> -->
              </div>
              <div id="for_user" style="display: none;">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                      <input type="text" name="mobile" id="mobile" class="form-control inputText"  maxlength="10" value="" required>
                      <span class="floating-label mobile-level" id="both">MOBILE</span>
                      <span style="position: absolute;" id="error_mobile" class="error_size"></span>
                    </div>
                  </div>
                </div>
              </div>   

              <div id="for_parking">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                      <input type="text" name="username" id="username" class="form-control inputText" onkeyup="caps();" value="" required>
                      <span class="floating-label username1" id="both">USERNAME</span>
                      <span style="position: absolute;" id="error_username" class="error_size"></span>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group error_show">
                      <input type="text" name="email" id="email" class="form-control inputText" style="background-color: transparent;" required>
                      <span class="floating-label email1" id="both">EMAIL</span>
                      <span style="position: absolute;" id="error_email" class="error_size"></span>
                    </div>
                  </div>                  
                </div>
              </div>

              <!-- <input type="hidden" name="email" id="hidden" value="NULL" class="form-control"> -->

              <div class="row">
                <div class="col">
                  <div class="form-group error_show">
                    <input type="text" name="otp" id="otp" class="form-control inputText" disabled="disabled" style="background-color: transparent;" maxlength="6">
                    <span class="floating-label otpp" id="both">OTP</span>
                    <span style="position: absolute;" id="msg_otp" class="error_size"></span>
                  </div>
                </div>
                <div class="col">
                  <button type="button" class="btn btn-block" name="send_otp" id="send_otp">Send OTP</button>
                </div>
              </div>
              <div id="chagePasswordDiv" style="display: none;">
                <div class="form-group error_show">
                  <input type="password" name="password" id="password" class="form-control" placeholder="Enter a New Password">
                  <span style="position: absolute;" id="msg_password" class="error_size"></span>
                </div>
                <div class="form-group error_show">
                  <input type="password" name="repassword" id="repassword" class="form-control" placeholder="Confirm Password">
                  <span style="position: absolute;" id="msg_repassword" class="error_size"></span>
                </div>
                <br/> 
                <button type="button" class="btn btn-block" name="chagePassword" id="chagePassword">Change My Password</button>
                <span style="position: absolute;" id="msg_chagePassword" class="error_size"></span>
              </div>

              <button type="button" class="btn btn-block mg-t-10" name="resetPassword" id="resetPassword" disabled="disabled" style="width: 215px">Reset Your Password</button>
              
            </form>

            <div class="mg-t-10">
              <div id="items_container">
                
              </div>
              <?php
                if (isset($_SESSION['error'])) {
                  echo "<p style=color:red>".$_SESSION['error']."</p>";
                  /*unset($_SESSION['login_error_msg'];*/
                  unset($_SESSION['error']);
                }
              ?>
            </div>
            <div class="mg-t-10 pull-right" style=""><a href="signin">Sign In</a></div>
          </div>
        </div>
      </div>
    <script src="lib/popper.js/popper.js"></script>
    <script src="lib/bootstrap/bootstrap.js"></script>
    <script src="lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
    <script src="js/amanda.js"></script>
    <script src="lib/validation/jquery.validate.js"></script>
  </body>
  <script type="text/javascript">
    var userUrl = "<?php echo USER_URL; ?>";
    var prkUrl = "<?php echo PRK_URL; ?>";
    $.validator.addMethod("v_number", function(value) {
      /*^[A-Z]{2}\s[0-9]{2}\s[A-Z]{2}\s[0-9]{4}$*/
      var patt = / /g;
      patt.compile(patt);
      var str = value.replace(patt,"");
      var login_for = $("#login_for").val();
      if (login_for == "U") {
        return /^[A-Za-z\d$@$!%*#?&]{5,}$/.test(str);
        //var ms='Must have least 5 characters';
      }else{
        return /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/.test(str);
        //var ms='Password is not strong enough';
      }
    }, 'Minimum 8 characters, atleast 1 number, 1 alphabet & 1 spcial character' );
    /*radio button*/
    $('#radioBtn a').on('click', function(){
      var login_for = $(this).data('title');
      //alert(login_for);
      var tog = $(this).data('toggle');
      $('#'+tog).prop('value', login_for);

      //alert(hid);            
      $('a[data-toggle="'+tog+'"]').not('[data-title="'+login_for+'"]').removeClass('active').addClass('notActive');
      $('a[data-toggle="'+tog+'"][data-title="'+login_for+'"]').removeClass('notActive').addClass('active');
      if(login_for == 'P'){
        $('#forgatePassForm').trigger("reset");
        $("#for_parking").show();
        $("#for_user").hide();
        $("#login_for").attr("value","P");
      }else if(login_for == 'U'){
        //alert("u");
        //$("#forgatePassForm").attr("action","user_admin/web/login.php");
        $('#forgatePassForm').trigger("reset");
        $("#for_user").show();
        $("#for_parking").hide();
        $("#login_for").attr("value","U");
      }else{
        //alert("chose somthing");
      }
    });
    /*validation*/
    $( document ).ready( function () {
      var urlMobCheck = userUrl+'mob_check.php';
      //alert(urlMobCheck);
      $( "#forgatePassForm" ).validate( {
        rules: {
          username: "required",
          mobile: {
            required: true,
            number: true,
            minlength: 10
          },
          password: {
            required: true,
            v_number: "v_number",
          },
          repassword: {
            required: true,
            equalTo: "#password"
          },
          email: {
            required: true,
            email: true
          }
        },
        messages: {
          username: "Please enter your username",
          mobile: {
            required: "Please enter a your mobile number",
            number: "Please enter a valid mobile number",
            minlength: "Please enter a valid mobile number",
            remote: "Mobile is not register with us"
          },
          password: {
            required: "Please provide a password",
            minlength: "Must have least 8 characters"
          },
          confirm_password: {
            required: "Please provide a password",
            equalTo: "Please enter the same as password"
          },
          email: {
            required: "Please enter your email",
            email: "Enter a valid email"
          }
        }
      });
      /*SEND OTP*/
      $('#send_otp').click(function () {
        //alert("otp send call");
        //alert($("#forgatePassForm").valid());
        if($("#forgatePassForm").valid()){
          //alert("CHECKED AND CLICK OTP BTN");
          var mobile = $('#mobile').val();
          var name = $("#mobile").val();
          var email = $('#email').val();
          var login_for = $("#login_for").val();
          if (login_for == "P") {
            var name = $("#username").val();
            var mobile = "NULL";
            var urlEmailCheck = prkUrl+'prk_user_and_email_check.php';
            $.ajax({
              url :urlEmailCheck,
              type:'POST',
              data :{
                'user_name':name,
                'email':email
              },
              dataType:'html',
              success  :function(data){
                //alert(data);
                var json = $.parseJSON(data);
                if (json.status) {
                  var apiUrl = prkUrl+'otpSend.php';
                  otpCall(mobile, name, email, apiUrl);                           
                }else{
                  //alert("problem U");
                  $.alert({
                    icon: 'fa fa-frown-o',
                    theme: 'modern',
                    title: 'Error !',
                    content: "<p style='font-size:0.8em;'>"+json.message+"</p>",
                    type: 'red'
                  });
                }
              }
            }).responseText;
          }
          if (login_for == "U") {
            //alert("U");
            /*start*/
            var urlMobCheck = userUrl+'check_mobile_number_exists.php';
            $.ajax({
              url :urlMobCheck,
              type:'POST',
              data :{
                'mobile':mobile
              },
              dataType:'html',
              success  :function(data){
                //alert(data);
                var json = $.parseJSON(data);
                if (json.status) {
                  //alert("ok");
                  var apiUrl = userUrl+'otpSend.php';
                  otpCall(mobile, name, email, apiUrl);
                }else{
                  $.alert({
                    icon: 'fa fa-frown-o',
                    theme: 'modern',
                    title: 'Error !',
                    content: "<p style='font-size:0.8em;'>Mobile number is not register with us</p>",
                    type: 'red'
                  });
                }
              }
            }).responseText;
            /*end*/
            //otpCall(mobile, name, email);
          }
        }
      });
      $('#otp').keyup(function(){
        var ch = $('#otp').val().length;
        var i = 0;
        if (ch == 6) {
          $('#resetPassword').prop("disabled",false);
          $('#emp_signin').removeClass("disabled");
          $('#resetPassword').show();
        }else{
          $('#resetPassword').prop("disabled",true);
        }
      });
      $('#resetPassword').click(function () {
        //alert("reset password call");
        var mobile = $('#mobile').val();
        var email = $('#email').val();
        var otp = $('#otp').val();
        var login_for = $("#login_for").val();
        if (login_for == "P") {
          //alert("P");
          var mobile = "";
        }
        var urlOtpVal = userUrl+'otpVal.php';
        $.ajax({
          url :urlOtpVal,
          type:'POST',
          data :{
            'mobile':mobile,
            'otp':otp,
            'email':email
          },
          dataType:'html',
          success  :function(data){
            //alert(data);
            var json = $.parseJSON(data);
            if (json.status) {
              // alert("redirect");
              //window.location.replace("reset_password.php");
              $('#radioBtn').hide();
              $("#mobile").attr("disabled","disabled");
              $("#mob_name").attr("disabled","disabled");
              $("#email").attr("disabled","disabled");
              $("#username").attr("disabled","disabled");
              $("#mob_name").css("background-color","transparent");
              $("#resetPassword").hide();
              $("#otp").hide();
              $(".otpp").hide();
              $(".username1").hide();
              $(".email1").hide();
              $(".mobile-level").hide();
              $("#send_otp").hide();
              $("#msg_otp").hide();
              $("#chagePasswordDiv").show();
            }else{
              $('#msg_otp').html('OTP Not Matched');
            }
          }
        });
      });
      /*user_admin_id','mobile','password','repassword'*/
      $('#chagePassword').click(function () {
        //alert("chage password call");
        if($("#forgatePassForm").valid()){
          //alert("change password call");
          var login_for = $("#login_for").val();
          var password = $('#password').val();
          var repassword = $('#repassword').val();
          /*'user_admin_id','mobile','password','repassword'*/
          if (login_for == "U") {
            var user_admin_id = "";
            var mobile = $('#mobile').val();
            var urlForgetPassword = userUrl+'forgetPassword.php';
            $.ajax({
              url :urlForgetPassword,
              type:'POST',
              data :{
                'user_admin_id':user_admin_id,
                'mobile':mobile,
                'password':password,
                'repassword':repassword
              },
              dataType:'html',
              success  :function(data){
                //alert(data);
                var json = $.parseJSON(data);
                if (json.status) {
                  $.alert({
                    icon: 'fa fa-smile-o',
                    theme: 'modern',
                    title: 'Success !',
                    content: "<p style='font-size:0.7em;'>Password Changed Successfully</p>",
                    type: 'green',
                    buttons: {
                      Ok: function () {
                          window.location='signin';
                      }
                    }
                  });
                }else{
                  //alert("problem U");
                  $.alert({
                    icon: 'fa fa-frown-o',
                    theme: 'modern',
                    title: 'Error !',
                    content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
                    type: 'red'
                  });
                }
              }
            });
          }
          if (login_for == "P") {
            var username = $('#username').val();
            var email = $('#email').val();
            /*'user_name','email','password','repassword'*/
            var urlForgetPassword = prkUrl+'forgetPasswordPrkAdmin.php';
            $.ajax({
              url :urlForgetPassword,
              type:'POST',
              data :{
                'user_name':username,
                'email':email,
                'password':password,
                'repassword':repassword
              },
              dataType:'html',
              success  :function(data){
                //alert(data);
                var json = $.parseJSON(data);
                if (json.status) {
                  $.alert({
                    icon: 'fa fa-smile-o',
                    theme: 'modern',
                    title: 'Success !',
                    content: "<p style='font-size:0.7em;'>Password Changed Successfully</p>",
                    type: 'green',
                    buttons: {
                      Ok: function () {
                          window.location='signin';
                      }
                    }
                  });
                }else{
                  //alert("problem p");
                  $.alert({
                    icon: 'fa fa-frown-o',
                    theme: 'modern',
                    title: 'Error !',
                    content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
                    type: 'red'
                  });
                }
              }
            });
          }
        }
      });
    });
    //functions
    function myTimer() {
      var d = new Date();
      $('#btn_otp').removeAttr('disabled');
      //$('#msg1').hide();
    }
    function caps() {
      var x = document.getElementById("username");
      x.value = x.value.toUpperCase();
    }
    function otpCall(mobile, name, email, apiUrl){
      //alert(apiUrl);
      $.ajax({
        url :apiUrl,
        type:'POST',
        data :{
          'mobile':mobile,
          'name':name,
          'email':email
        },
        dataType:'html',
        success  :function(data){
          //alert(data);
          $('#otp').prop("disabled",false);
          $('#otp').focus();
          $('#msg_otp').html('OTP sent, please check');
          var fewSeconds = 20;
          $('#send_otp').prop('disabled', true);
          setTimeout(function(){
            $('#send_otp').prop('disabled', false);
            $('#send_otp').html("Resend");
          }, fewSeconds*1000);
        }
      }).responseText; 
    }
  </script>
</html>
