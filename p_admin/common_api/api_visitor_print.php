<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function visitor_print($prk_visit_dtl_id){
    global $pdoconn;
    $sql = "SELECT `prk_visit_dtl_id`,
        `visitor_type`,
        `visit_name`,
        `visit_gender`,
        `visit_mobile`,
        `visit_purpose`,
        `visit_meet_to`,
        (CASE `visitor_type` WHEN 'O' THEN IF(`visit_img` IS NULL or `visit_img` = '', 
                (CASE WHEN `visit_gender`= '".FLAG_M."' then '".MALE_IMG_D."' WHEN `visit_gender`= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".PRK_BASE_URL."',`visit_img`))
    
            WHEN 'V' THEN (SELECT IF(vgp.visitor_img IS NULL or vgp.visitor_img = '', (CASE WHEN `visit_gender`= '".FLAG_M."' then '".MALE_IMG_D."' WHEN `visit_gender`= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".PRK_BASE_URL."',vgp.visitor_img)) FROM visitor_gate_pass_dtl vgp WHERE visitor_gate_pass_dtl_id=`unique_id`)

            WHEN 'C' THEN (SELECT IF(cgp.visitor_img IS NULL or cgp.visitor_img = '', (CASE WHEN visit_gender= '".FLAG_M."' then '".MALE_IMG_D."' WHEN visit_gender= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".PRK_BASE_URL."',cgp.visitor_img)) FROM contractual_gate_pass cgp WHERE cgp.contractual_gate_pass_dtl_id=unique_id)
 
            WHEN 'G' THEN IF(`visit_img` IS NULL or `visit_img` = '', (CASE WHEN `visit_gender`= '".FLAG_M."' then '".MALE_IMG_D."' WHEN `visit_gender`= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".PRK_BASE_URL."',`visit_img`))
 
            WHEN 'U' THEN (SELECT IF(ud.user_img IS NULL or ud.user_img = '', (CASE WHEN visit_gender= '".FLAG_M."' then '".MALE_IMG_D."' WHEN visit_gender= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".USER_BASE_URL."',ud.user_img)) FROM user_detail ud where ud.user_admin_id= unique_id AND ud.active_flag='".FLAG_Y."') 
        end) as 'visit_img',
        `visit_with_veh`,
        if(`visitor_meet_to_name` IS NULL OR `visitor_meet_to_name`='','NA',`visitor_meet_to_name`) as `visitor_meet_to_name`,
        `visitor_meet_to_address`,
        `prk_admin_id`,
        `no_of_guest`,
        DATE_FORMAT(`start_date`,'%d-%b-%Y') AS `start_date`,
        DATE_FORMAT(`start_date`,'%I:%i %p') AS `start_time`,
        `end_date`,
        DATE_FORMAT(`end_date`,'%d-%b-%Y') AS `end_date`,
        DATE_FORMAT(`end_date`,'%I:%i %p') AS `end_time`,
        `veh_type`,
        if(`vehicle_type_dec` IS NULL OR `vehicle_type_dec`='','NA',`vehicle_type_dec`) as `vehicle_type_dec`,
        if(`veh_number` IS NULL OR `veh_number`='','NA',`veh_number`) as `veh_number`,
        DATE_FORMAT('".TIME."','%d-%b-%Y') AS `current_date`,
        DATE_FORMAT('".TIME."','%I:%i %p') AS `current_time`
        FROM `prk_visit_dtl` LEFT JOIN `vehicle_type` ON `vehicle_sort_nm`=`veh_type`
        WHERE `prk_visit_dtl_id`='$prk_visit_dtl_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['visit_name'] = $val['visit_name'];
        $response['visit_mobile'] = $val['visit_mobile'];
        $response['prk_admin_id'] = $val['prk_admin_id'];
        $response['visitor_meet_to_name'] = $val['visitor_meet_to_name'];
        $response['visit_purpose'] = $val['visit_purpose'];
        $response['vehicle_type_dec'] = $val['vehicle_type_dec'];
        $response['veh_number'] = $val['veh_number'];
        $response['veh_type'] = $val['veh_type'];
        $response['start_date'] = $val['start_date'];
        $response['start_time'] = $val['start_time'];
        $response['end_date'] = $val['end_date'];
        $response['end_time'] = $val['end_time'];
        $response['current_date'] = $val['current_date'];
        $response['current_time'] = $val['current_time'];
        $response['visit_img'] = $val['visit_img'];
        $response['qrcode_value'] = $val['visit_mobile'].'-OPPR';
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>