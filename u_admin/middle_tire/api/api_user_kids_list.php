<?php
/*
Description: user payment history.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_kids_list($user_admin_id,$mobile,$start_date,$end_date){
    if(!empty($start_date)&& !empty($end_date)){
       $format = "Y-m-d";
        if(date($format, strtotime($start_date)) == date($start_date) && date($format, strtotime($end_date)) == date($end_date)) {
            $start_date= $start_date;
            $end_date=  $end_date ;
        }else{
            $start_date = date("Y-m-d", strtotime($start_date));
            $end_date = date("Y-m-d", strtotime($end_date));
        }
    }else{
        $start_date = '2017-08-02';
        $end_date = FUTURE_DATE;
    }
    global $pdoconn;
    $allplayerdata = array();
    $sql = "SELECT `prk_area_dtl`.`prk_area_name`,
        ka.`kids_allow_id`,
        ka.`prk_admin_id`,
        ka.`user_admin_id`,
        ka.`user_family_id`,
        ufg.`user_family_name`,
        ufg.`user_family_gender`,
        ufg.`user_family_relation`,
        IF(ufg.`user_family_img` IS NULL OR ufg.`user_family_img`='',IF(ufg.`user_family_gender`='M','".MALE_IMG_D."','".FEMALE_IMG_D."'),CONCAT('".USER_BASE_URL."',ufg.`user_family_img`)) as 'user_family_img',
        ka.`alone_flag`,
        ka.`rep_name`,
        IF(ka.`rep_image` IS NULL OR ka.`rep_image`='','',CONCAT('".USER_BASE_URL."',ka.`rep_image`)) as 'rep_image',
        DATE_FORMAT(ka.`date`,'%d-%m-%Y') AS `date`,
        ka.`from_time`,
        ka.`to_time`,
        ka.`status`
        FROM `kids_allow` ka JOIN `user_family_group` ufg join `prk_area_dtl` 
        ON ka.`prk_admin_id` = `prk_area_dtl`.`prk_admin_id` 
        WHERE ufg.`user_family_id`=ka.`user_family_id`
        AND ufg.`active_flag`='".FLAG_Y."'
        AND ufg.`del_flag`='".FLAG_N."'
        AND ka.`user_admin_id`='$user_admin_id'
        AND ka.`active_flag`='".FLAG_Y."'
        AND ka.`del_flag`='".FLAG_N."'
        AND ka.`status` IN('I','O')
        AND `prk_area_dtl`.`active_flag` = '".FLAG_Y."'
        AND DATE_FORMAT(ka.`date`,'%Y-%m-%d') BETWEEN '$start_date' AND '$end_date'
        ORDER BY ka.`kids_allow_unique_id` DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    $group = array();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['prk_area_name'] = $val['prk_area_name'];
            $park['kids_allow_id'] = $val['kids_allow_id'];
            $park['prk_admin_id'] = $val['prk_admin_id'];
            $park['user_admin_id'] = $val['user_admin_id'];
            $park['user_family_id'] = $val['user_family_id'];
            $park['user_family_name'] = $val['user_family_name'];
            $park['user_family_gender'] = $val['user_family_gender'];
            $park['user_family_relation'] = $val['user_family_relation'];
            $park['user_family_img'] = $val['user_family_img'];
            $park['alone_flag'] = $val['alone_flag'];
            $park['rep_name'] = $val['rep_name'];
            $park['rep_image'] = $val['rep_image'];
            $park['date'] = $val['date'];
            $park['from_time'] = $val['from_time'];
            $park['to_time'] = $val['to_time'];
            $park['status'] = $val['status'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['kids_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>