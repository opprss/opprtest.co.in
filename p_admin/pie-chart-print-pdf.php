<?php 

include('fpdf/fpdf.php');
include '../global_asset/config.php';
$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',16);
date_default_timezone_set('Asia/Kolkata'); 
$date= date("Y-m-d");
$pdf->setXY(160,20);
  $pdf->Cell(160,10 ,$date);
$pdf->Image(prk_LOGO_pdf,80,20,-200);
$pdf->setXY(100,60);
//$pdf->Cell(200,50,0,1);
$pdf->Line(10,60,200,60);
$pdf->Line(10,63,200,63);
$pdf->setXY(40,60);
$pdf->Cell(10,70,'VEHICLE PIE CHART FOR CURRENT DATE',0);
$pdf->setXY(50,100);
$pdf->Image('web/images/pie_chart_dashboard/imagefile.png',30,100,150,0,'PNG');



$pdf->SetXY(30,270);
$pdf->SetFont('Arial','',9);
//$pdf->Cell(10,5,'COPYRIGHT'.chr(169).'2018 BY OPPR SOFTWARE SOLUTION PRIVATE LIMITED ALL RIGHTS RESERVED.','C');
$pdf->Cell(10,5,COPYRIGHT_PDF,'C');  
$pdf->Output();
 ?>
