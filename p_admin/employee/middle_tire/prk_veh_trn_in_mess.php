<?php 
/*
Description: parking employ logOut
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date : 07-04-2018
*/  
require_once 'api/parkAreaReg_api.php';
$response = array();
 if(isAvailable(array('prk_veh_trc_dtl_id'))){

 	if(isEmpty(array('prk_veh_trc_dtl_id'))){

	 	$prk_veh_trc_dtl_id = trim($_POST['prk_veh_trc_dtl_id']);

	 	$sql ="select prk_veh_trc_dtl.user_mobile,
			DATE_FORMAT(prk_veh_trc_dtl.prk_veh_in_time,'%D-%b-%Y') AS `veh_in_date`,
			DATE_FORMAT(prk_veh_trc_dtl.prk_veh_in_time,'%I:%i %p') AS `veh_in_time`,
			prk_veh_trc_dtl.prk_veh_type,
			prk_veh_trc_dtl.veh_number,
			prk_veh_trc_dtl.prk_admin_id,
			prk_veh_trc_dtl.veh_out_otp,
			prk_area_dtl.prk_area_name 
			from prk_veh_trc_dtl,prk_area_dtl 
			where prk_veh_trc_dtl.prk_veh_trc_dtl_id = '$prk_veh_trc_dtl_id'
			and prk_area_dtl.prk_admin_id=prk_veh_trc_dtl.prk_admin_id
			and prk_area_dtl.active_flag='".FLAG_Y."'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $count=$query->rowCount();
    	if($count>0){
	        $val = $query->fetch();
	        $user_mobile = $val['user_mobile'];
	        $veh_in_date = $val['veh_in_date'];
	        $veh_in_time = $val['veh_in_time'];
	        $prk_veh_type = $val['prk_veh_type'];
	        $veh_number = $val['veh_number'];
	        $prk_area_name = $val['prk_area_name'];
	        $veh_out_otp = $val['veh_out_otp'];
	        $otp_mes =", Vehicle Out OTP :".$veh_out_otp;
	 		$response = prk_veh_trn_in_mess($user_mobile,$veh_in_date,$veh_in_time,$prk_veh_type,$veh_number,$prk_area_name,$otp_mes); 	
	 	}else{
			$response['status'] = 0;
	 		$response['message'] = 'Wrong Mobile Number!';
	        $response = json_encode($response); 
		}	
 	}else{
		$response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response); 
	 }
}else{
	$response['status'] = 0; 
	$response['message'] = 'Invalid API Call';
	$response = json_encode($response); 
}
echo $response
?>