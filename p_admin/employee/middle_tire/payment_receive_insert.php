<?php 
/*
Description: parking employ payment resived.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/  
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('payment_receive_number','prk_admin_id','prk_veh_trc_dtl_id','payment_amount','payment_statas','prk_user_username','in_out_flag'))){
 	if(isEmpty(array('payment_receive_number','prk_admin_id','prk_veh_trc_dtl_id','payment_amount','payment_statas','prk_user_username','in_out_flag'))){

        $payment_receive_number = trim($_POST['payment_receive_number']);
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $prk_veh_trc_dtl_id = trim($_POST['prk_veh_trc_dtl_id']);
        $payment_amount = trim($_POST['payment_amount']);
        $payment_statas = trim($_POST['payment_statas']);
        $prk_user_username = trim($_POST['prk_user_username']);
        $in_out_flag = trim($_POST['in_out_flag']);
        
        $response = payment_receive_insert($payment_receive_number,$prk_admin_id,$prk_veh_trc_dtl_id,$payment_amount,$payment_statas,$prk_user_username,$in_out_flag);
        //echo ($respon);
 	}else{
        $response['status'] = 0;
	    $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo ($response);
?> 