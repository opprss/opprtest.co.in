<?php
/*
Description: user licence list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_lic_dtl_list($user_admin_id){
    $response = array();
    $allplayerdata = array();
    $gate_pass = array();
    global $pdoconn;
    $sql = "SELECT `user_lic_id`, `user_admin_id`, `user_lic_number`, `user_lic_name`, `user_lic_veh_class`, `user_lic_issue_date`, `user_lic_issued_by`, `user_lic_valid_till`, `s_d_w_of`, `blood_gr`, `date_of_birth`, `address`,IF(`user_lic_img` IS NULL or `user_lic_img` = '', '', CONCAT('".USER_BASE_URL."',`user_lic_img`)) as 'user_lic_img', `user_lic_img_status` FROM `user_lic_dtl` WHERE `user_admin_id`='$user_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."' ORDER BY `user_lic_id` DESC ";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val)
        {            
            $response['status'] = 1;
            $response['message'] = 'Successful';
            $gate_pass['user_lic_id'] = $val['user_lic_id'];
            $gate_pass['user_lic_number'] = $val['user_lic_number'];
            $gate_pass['user_lic_name'] = $val['user_lic_name'];
            $gate_pass['user_lic_veh_class'] = $val['user_lic_veh_class'];
            $gate_pass['user_lic_issue_date'] = $val['user_lic_issue_date'];
            $gate_pass['user_lic_issued_by'] = $val['user_lic_issued_by'];
            $gate_pass['user_lic_valid_till'] = $val['user_lic_valid_till'];
            $gate_pass['s_d_w_of'] = $val['s_d_w_of'];
            $gate_pass['blood_gr'] = $val['blood_gr'];
            $gate_pass['date_of_birth'] = $val['date_of_birth'];
            $gate_pass['address'] = $val['address'];
            $gate_pass['user_lic_img'] = $val['user_lic_img'];
            $gate_pass['user_lic_img_status'] = $val['user_lic_img_status'];
            array_push($allplayerdata, $gate_pass);
            $response['user_lic'] = $allplayerdata;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }

        
    return json_encode($response);
}
?>