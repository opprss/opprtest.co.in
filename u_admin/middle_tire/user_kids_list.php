<?php
/*
Description: user payment history.
Developed by: Rakhal Raj Mandal
Created Date: 30-03-2018
Update date :08-05-2018
*/
include_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_admin_id','mobile','start_date','end_date','token'))){
    if(isEmpty(array('user_admin_id','mobile','token'))){
        $user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
        $start_date = trim($_POST['start_date']);
        $end_date = trim($_POST['end_date']);
        $token = ($_POST['token']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $response= user_kids_list($user_admin_id,$mobile,$start_date,$end_date);
        }else{
            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
         $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response); 
}
echo $response 
?> 