<?php
/*
Description: Parking adimn user name send your meil.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function accountStatusEmail($email){
    $response = array();
    global $pdoconn;
    if(emailChecker($email)=='true'){
        $sql ="SELECT `prk_area_admin`.`park_ac_status`, `prk_area_admin`.`prk_admin_name`,`prk_area_admin`.`prk_admin_id` FROM `prk_area_dtl`,`prk_area_admin` WHERE `prk_area_dtl`.`prk_admin_id`= `prk_area_admin`.`prk_admin_id` AND `prk_area_dtl`.`prk_area_email`= '$email' AND active_flag ='".FLAG_Y."'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $count=$query->rowCount();
        if($count>0){
            $row = $query->fetch();
            $ac_sta =  $row['park_ac_status'];
            $prk_admin_id =  $row['prk_admin_id'];
            $prk_admin_name =  $row['prk_admin_name'];
            if($ac_sta=='A'){
                $response['status'] = 0;
                $response['prk_admin_id'] = $prk_admin_id;
                $response['prk_admin_name'] = $prk_admin_name;
                $response['message'] = 'User Already Exists';
            }
            elseif($ac_sta=='T'){
                $response['status'] = 0;
                $response['prk_admin_id'] = '';
                $response['message'] = 'User Temporary Locked';
            }
            elseif($ac_sta=='L'){
                $response['status'] = 0;
                $response['prk_admin_id'] = '';
                $response['message'] = 'User Locked';
            }
            elseif($ac_sta=='P'){
                $response['status'] = 0;
                $response['prk_admin_id'] = '';
                $response['message'] = 'User Payment due Please Contact Customer Care';
            }
            elseif($ac_sta=='D'){
                $response['status'] = 0;
                $response['prk_admin_id'] = '';
                $response['message'] = 'User Does Not Exist';
            }else{
                $response['status'] = 0;
                $response['prk_admin_id'] = '';
                $response['message'] = 'Please Contact to Customer Care';
            }
            return ($response);
        }else{
            $response['status'] = 1;
            $response['prk_admin_id'] = '';
            $response['message'] = 'Email Not Exist';
            return ($response);
        }
    }else{
            $response= emailChecker($email);
            return ($response);
    }
}
?>