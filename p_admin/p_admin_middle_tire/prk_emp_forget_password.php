<?php 
/*
Description: parking employ forget password.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
 require_once 'api/parkAreaReg_api.php';
// include_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','prk_area_user_id','prk_admin_name','password','repassword','token'))){
 	if(isEmpty(array('prk_admin_id','prk_area_user_id','prk_admin_name','password','repassword','token'))){

	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_area_user_id = trim($_POST['prk_area_user_id']);
        $prk_admin_name = trim($_POST['prk_admin_name']);
	 	$password = trim($_POST['password']);
        $repassword = trim($_POST['repassword']);
        $token = ($_POST['token']);
            $resp=prk_token_check($prk_admin_id,$token);
            $json = json_decode($resp);
             if($json->status){
        $response= prk_emp_forget_password($prk_admin_id,$prk_area_user_id,$prk_admin_name,$password,$repassword);
        echo ($response);
         }else{
                $response = $resp;
                echo $response;
            }
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response);
 }
?>