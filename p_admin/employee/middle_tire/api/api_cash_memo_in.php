<?php
/*
Description: vehicle in advance payment resive dtl show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function cash_memo_in($prk_veh_trc_dtl_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
        $sql = "SELECT `prk_veh_trc_dtl`.`prk_veh_type`,
            `vehicle_type`.`vehicle_type_dec`,
            `prk_veh_trc_dtl`.`veh_number`,
            `prk_veh_trc_dtl`.`user_mobile`,
            DATE_FORMAT('".TIME."','%d-%m-%Y %H:%i') AS `current_date`,
            DATE_FORMAT('".TIME."','%I:%i %p') AS `current_time`,
            DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%D-%b-%Y') AS `veh_in_date`,
            DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%I:%i %p') AS `veh_in_time`,
            `prk_veh_trc_dtl`.`payment_type` as payment_type_srt,
            (CASE `prk_veh_trc_dtl`.`payment_type` WHEN '".CASH."' ThEN '".CASH."' WHEN '".WALLET."' THEN '".WALLET."'
                WHEN '".TDGP."' THEN '".TDGP_F."' 
                WHEN '".DGP."' THEN '".DGP_F."' end) as 'payment_type',
            `prk_veh_trc_dtl`.`advance_pay`,
            `prk_veh_trc_dtl`.`advance_pay`,
            ifnull(`payment_receive`.`payment_receive_number`, `prk_veh_trc_dtl`.`prk_veh_trc_dtl_number`) AS `payment_receive_number`,
            `prk_area_dtl`.`prk_area_name`,
            `prk_area_dtl`.`prk_area_pro_img`,
            CONCAT(`prk_veh_trc_dtl`.`prk_veh_trc_dtl_id`, '-OPPR') as prk_veh_trc_dtl_id,
            `prk_area_rate`.`prk_ins_hr_rate`,
            `prk_area_rate`.`prk_ins_no_of_hr`,
            `prk_area_rate`.`veh_rate`,
            `prk_area_rate`.`veh_rate_hour`        
        FROM `prk_veh_trc_dtl` 
        join `vehicle_type`     on `prk_veh_trc_dtl`.`prk_veh_type` = `vehicle_type`.`vehicle_sort_nm`
                                AND `vehicle_type`.`active_flag` ='".FLAG_Y."' 
        join `prk_area_dtl`     on `prk_veh_trc_dtl`.`prk_admin_id` = `prk_area_dtl`.`prk_admin_id` 
                                AND `prk_area_dtl`.`active_flag` ='".FLAG_Y."'
        left join `prk_area_rate` on `prk_veh_trc_dtl`.`prk_admin_id` = `prk_area_rate`.`prk_admin_id`
                                AND `prk_veh_trc_dtl`.`prk_veh_type` = `prk_area_rate`.`prk_vehicle_type`
                                AND `prk_area_rate`.`active_flag` ='".FLAG_Y."'
                                AND `prk_area_rate`.`del_flag` ='".FLAG_N."'
                                AND  '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(`prk_area_rate`.`prk_rate_eff_date`, '%d-%m-%Y'), '%Y-%m-%d')    
                                AND  DATE_FORMAT(STR_TO_DATE(IFNULL(`prk_area_rate`.`prk_rate_end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')
        left join `payment_receive` on `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id` = `payment_receive`.`prk_veh_trc_dtl_id`
                                AND `payment_receive`.`in_out_flag` = '".FLAG_I."'
        WHERE `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id` = '$prk_veh_trc_dtl_id'";
        $query  = $pdoconn->prepare($sql);
        $query->execute(); 
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['vehicle_type'] = $val['prk_veh_type'];
        $response['vehicle_type_dec'] = $val['vehicle_type_dec'];
        $response['veh_number'] = $val['veh_number'];
        $response['user_mobile'] = $val['user_mobile'];
        $response['current_date'] = $val['current_date'];
        $response['current_time'] = $val['current_time'];
        $response['invoice_no'] = $val['payment_receive_number'];
        $response['veh_in_date'] = $val['veh_in_date'];
        $response['veh_in_time'] = $val['veh_in_time'];
        $response['payment_type'] = $val['payment_type'];
        $response['payment_type_srt'] = $val['payment_type_srt'];
        $response['advance_pay'] = $val['advance_pay'];
        $response['prk_area_name'] = $val['prk_area_name'];
        $response['prk_area_pro_img'] = $val['prk_area_pro_img'];
        $response['barcode_valu'] = $val['prk_veh_trc_dtl_id'];
        $response['prk_ins_hr_rate'] = $val['prk_ins_hr_rate'];
        $response['prk_ins_no_of_hr'] = $val['prk_ins_no_of_hr'];
        $response['veh_rate'] = $val['veh_rate'];
        $response['veh_rate_hour'] = $val['veh_rate_hour'];

    return json_encode($response); 
}
?>