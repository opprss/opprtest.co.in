<?php
/*
Description: Parking adimn gate pass genaret for user.
Developed by: Rakhal Raj Mandal
Created Date: 31-02-2018
Update date : 25-05-2018
*/ 
function unique_key_generate($unique_prk_short_name,$unique_type,$prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $unique_month = date("nY");
    $invoice_no = $unique_type.'/'.$unique_prk_short_name.'/'.$unique_month.'/00001';
    $unique_id = '';
    $sql = "select unique_id,unique_type,
        unique_prk_short_name,
        unique_month,
        CONCAT(unique_type,'/',unique_prk_short_name,'/',unique_month,'/',lpad(unique_seq,5,0)) as invoice_no,
        (unique_seq + 1) as new_seq 
        FROM unique_key 
        WHERE unique_type = '$unique_type'
        AND prk_admin_id = '$prk_admin_id'
        AND unique_prk_short_name = '$unique_prk_short_name'
        AND unique_month = date_format(current_date(), '%c%Y')";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $unique_id= $val['unique_id'];
        $unique_month= $val['unique_month'];
        $invoice_no= $val['invoice_no'];
        $new_seq= $val['new_seq'];
        $sql = "UPDATE `unique_key` SET `unique_seq`= '$new_seq' WHERE `unique_id`='$unique_id'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
    } else {
        $sql = "INSERT INTO `unique_key`(`unique_type`, `prk_admin_id`, `unique_prk_short_name`, `unique_month`, `unique_seq`) VALUES ('$unique_type','$prk_admin_id','$unique_prk_short_name',$unique_month,'2')";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $unique_id = $pdoconn->lastInsertId();
    }
    $response['status'] = 1;
    $response['message'] = 'Sucessfull';
    $response['unique_id'] = $unique_id;
    $response['unique_month'] = $unique_month;
    $response['invoice_no'] = $invoice_no;
    return json_encode($response);
}
?>