<?php
	include 'api/parkAreaReg_api.php';
	$dd=($_REQUEST['list_id']);
	// $list_id=base64_decode($_REQUEST['list_id']);
	// $dd="OTY0NzQ1MTM3OC0yVy1XQjg4NDQ3N3x8V0I4ODQ0Nzd8fDJXfHxUd28gV2hlZWxlcnM=,OTY0NzQ1MTM3OC00Vy1ERDEyMzQ1Nnx8REQxMjM0NTZ8fDRXfHxGb3VyIFdoZWVsZXJz,OTY0NzQ1MTM3OC0yVy1XQjEyREQxMjM0fHxXQjEyREQxMjM0fHwyV3x8VHdvIFdoZWVsZXJz,OTY0NzQ1MTM3OC00Vy1XQjA3UzE1MDd8fFdCMDdTMTUwN3x8NFd8fEZvdXIgV2hlZWxlcnM=";
    $pieces12 = explode(",", $dd);//$pieces[0]
    // echo(count($pieces12)).'</br>';
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Generate QR Code</title>
    <style type="text/css">
		.circle {
			width: 342px;
			height: 342px;
			border-radius: 50%;
			font-size: 50px;
			line-height: 500px;
			text-align: center;
			background: #fff;
			border: 2px solid black;
		}
		.logo_under_brdr {
		    width: 284px;
	    	margin-left: 27px
		}
		.vl {
			border-left: 3px solid #000;
			height: 157px;
			left: 36px;
			display: block;
			top: -41px
		}
		@media print{
			@page {
			   margin-top: 0px;
			   margin-bottom: 0px;
			   size: A4;
			   height: auto;
			   width: 100%;
			   margin-left: 2px;
			   margin-right: 2px;
			}
			body  {
			   padding-top: 10px;
			   padding-bottom: 10px ;
			   margin-left: 0px;
			}
			 #Header, #Footer { display: none !important; }
			.col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
		        float: left;
		   	}
		   	.col-sm-12 {
		        width: 100%;
		   	}
		   	.col-sm-11 {
		        width: 91.66666667%;
		   	}
		   	.col-sm-10 {
		        width: 83.33333333%;
		   	}
		   	.col-sm-9 {
		        width: 75%;
		   	}
		   	.col-sm-8 {
		        width: 66.66666667%;
		   	}
		   	.col-sm-7 {
		        width: 58.33333333%;
		   	}
		   	.col-sm-6 {
		        width: 50%;
		   	}
		   	.col-sm-5 {
	   	     	width: 41.66666667%;
		   	}
		   	.col-sm-4 {
		        width: 33.33333333%;
		   	}
		   	.col-sm-3 {
		        width: 25%;
		   	}
		   	.col-sm-2 {
		        width: 16.66666667%;
		   	}
		   	.col-sm-1 {
		        width: 8.33333333%;
		   	}
		   	.logo_under_brdr {
			    width: 284px;
		    	margin-left: 27px
			}
			img {
				max-width: none !important;
			}
		}
	</style>
</head>
<body class="bg">
    	<?php  
    	$count_list=0;
    	$html_show ='';
    	foreach($pieces12 as $i => $item) {
	    	$list_id=base64_decode($pieces12[$i]);
	    	// $list_id="9647451378-4W-WB07S1507||WB07S1507||4W||Four Wheelers";
		    $pieces = explode("||", $list_id);//$pieces[0]
		    // $veh_qr_code.'||'.$user_veh_number.'||'.$user_veh_type.'||'.$vehicle_type_dec
		    $veh_qr_code=$pieces[0];
		    $user_veh_number=$pieces[1];
		    $user_veh_type=$pieces[2];
		    $vehicle_type_dec=$pieces[3];
		    $veh_owner_name=$pieces[4];
		    $tower_name=$pieces[5];
		    $flat_name=$pieces[6];
		    $prk_area_name=$pieces[7];
		    $prk_area_logo_img=$pieces[8];
		    if ($count_list==0) {
	    		$html_show.='<div style="display: inline-block; page-break-after: auto;margin-bottom:0px;">
		    		<div class="col-sm-4" style="height: 356px;">
			    		<div class="full_div" style="display: block;">
							<div class="circle">
								<center><img style="height: 60px;width:auto;margin-top: 13px;margin-bottom: 2px;display: block;" src="'.LOGO_PDF.'"></center>
								<div class="col-sm-12 logo_under_brdr" style="border: 1px solid black;display: block;"></div>
							</div>
							<div class="col-sm-12" style="width: 263px;height:auto;top: -223px;margin-left: 18px;">
								<div class="row" >
									<div class="col-sm-5" style="display: block;">
										<div class="row">
											<div class="col-sm-12" style="top: -25px;font-size: 14px;display: block;"><b>'.$prk_area_name.'</b></div>
											<div class="col-sm-12" style="display: block;"><img style="height: 68px;width:95px;margin-top: -12px;" src="'.PRK_BASE_URL.$prk_area_logo_img.'"></div>
										</div>
									</div>
									<div class="col-sm-2 vl"></div>
									<div class="col-sm-5" style="top: -34px;right: 15px;display: block;">
										<img class="qrcode" style="height: 144px !important;width: 144px !important;" src="./../../library/QRCodeGenerate/generate.php?text='.$veh_qr_code.'" alt="">
									</div>
								</div>
								<div class="col-sm-12" style="border: 1px solid black;width: 314px;left: -20px;top: -41px;display: block;"></div>
								<div class="col-sm-12" style="top: -32px;display: block;">
									<div class="row">
										<div class="col-sm-12" style="margin-left: 0px;font-size: 14px;"><b>Name:</b> '.ucwords(strtolower($veh_owner_name)).'</div>
										<div class="col-sm-12" style="top: 5px;margin-left: 12px;"><b>Tower:</b> '.$tower_name.' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Flat:</b> '.$flat_name.'</div>
										<div class="col-sm-12" style="top: 11px;margin-left: 43px;"><b>Vehicle No:</b> '.$user_veh_number.'</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>';
		    }else{
		    	$html_show.='<div style="display: inline-block; page-break-after: auto;margin-bottom:0px;">
			    	<div class="col-sm-4" style="height: 356px;">
			    		<div class="full_div" style="display: block;">
							<div class="circle">
								<center><img style="height: 60px;width:auto;margin-top: 13px;margin-bottom: 2px;display: block;" src="'.LOGO_PDF.'"></center>
								<div class="col-sm-12 logo_under_brdr" style="border: 1px solid black;display: block;"></div>
							</div>
							<div class="col-sm-12" style="width: 263px;height:auto;top: -223px;margin-left: 18px;">
								<div class="row" >
									<div class="col-sm-5" style="display: block;">
										<div class="row">
											<div class="col-sm-12" style="top: -25px;font-size: 14px;display: block;"><b>'.$prk_area_name.'</b></div>
											<div class="col-sm-12" style="display: block;"><img style="height: 68px;width:95px;margin-top: -12px;" src="'.PRK_BASE_URL.$prk_area_logo_img.'"></div>
										</div>
									</div>
									<div class="col-sm-2 vl"></div>
									<div class="col-sm-5" style="top: -34px;right: 15px;display: block;">
										<img class="qrcode" style="height: 144px !important;width: 144px !important;" src="./../../library/QRCodeGenerate/generate.php?text='.$veh_qr_code.'" alt="">
									</div>
								</div>
								<div class="col-sm-12" style="border: 1px solid black;width: 314px;left: -20px;top: -41px;display: block;"></div>
								<div class="col-sm-12" style="top: -32px;display: block;">
									<div class="row">
										<div class="col-sm-12" style="margin-left: 0px;font-size: 14px;"><b>Name:</b> '.ucwords(strtolower($veh_owner_name)).'</div>
										<div class="col-sm-12" style="top: 5px;margin-left: 12px;"><b>Tower:</b> '.$tower_name.' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Flat:</b> '.$flat_name.'</div>
										<div class="col-sm-12" style="top: 11px;margin-left: 43px;"><b>Vehicle No:</b> '.$user_veh_number.'</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>';
		    }
    		$count_list+=1;
    		$count_list=($count_list==2)?0:$count_list;
    	}
    		echo($html_show);
    	?>
</body>
</html>
<script  src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="  crossorigin="anonymous"></script>
<script type="text/javascript">
	$(window).on('load', function() {
	 	window.print();
		setTimeout(function(){window.close();}, 1);
	});
</script>
