<!-- ################################################
  
Description: Parking area can show and download qr code for each gate
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
  include_once "all_nav/header.php"; 
  @session_start();
  if (isset($_SESSION['prk_admin_id'])) {
    $user_name=$_SESSION['prk_admin_id'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
    $response = array();
    $response = prk_area_profile_show($prk_admin_id);
    $response = json_decode($response, true);
    if (!empty($response)) {
      $prk_area_pro_img = $response['prk_area_pro_img'];
    }
  }
?>
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">QR Code</h5>
  </div>

  <div class="am-pagebody">
    <div class="card single pd-20 pd-sm-40">  
      <div class="row">
        <div class="col-lg-12">
          <div class="row">
            <div class="col-md-12">
              <table>
                <?php  
                $prk_add_id=$_SESSION['prk_admin_id'];
                $all_data=select_prk_dtl($prk_add_id);

                foreach($all_data as $name)
                ?> 
                  <tr>
                    <td>
                      <h3> <?php if(!empty($name['prk_area_name'])) echo $name['prk_area_name']; else echo "N/A" ?></h3>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      <hr>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h6>
                        <?php 
                        	if(!empty($name['prk_address'])){
                         		echo $name['prk_address'].', '.$name['prk_add_area'].', '.$name['prk_add_area'].', '.$name['state_name'].', '.$name['prk_add_country'].', '.$name['city_name'].'-'.$name['prk_add_pin'];
                         	}
                         ?>
                        
                      </h6>
                    </td>
                  </tr>
              </table>
            </div>
          </div>
          <div class="row text-center">
            <?php 
              $result=prk_area_gate_dtl_list($prk_add_id);
              // print_r($result);
              foreach($result as $value){
                $gate_type='';
                if($value['prk_area_gate_type']=='B'){
                  $gate_type='(BOTH)';
                }elseif ($value['prk_area_gate_type']=='I') {
                  $gate_type='(IN)';
                }elseif ($value['prk_area_gate_type']=='O') {
                  $gate_type='(OUT)';
                }elseif ($value['prk_area_gate_type']=='S') {
                  $gate_type='(SKIP)';
                }else{
                  $gate_type='(OTHER)';
                }
                ?>
                <div class="col-lg-4 pd-t-40"> 
                  <table>
                    <tr>
                      <td class="pd-b-20">
                        <label><?php echo $value['prk_area_gate_name'] ." ".$gate_type?></label>
                        <div class="editable tx-16  pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px; font-size: 3px;"></div>
                            <?php 
                              $qrcode=$value['prk_qr_code'];
                              // $code = '<center><img src="https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl='.$qrcode.'" title="Link to Google.com"></center>';
                              $code = '<center><img style="height: 70px;" src="./../library/QRCodeGenerate/generate.php?text='.$qrcode.'" alt=""></center>';
                              echo $code;
                             ?>
                      </td>
                    </tr>
                  </table>
                </div>
              <?php } ?>
          </div>
          <div class="row">
            <div class="col-lg-12 text-center pd-t-30 pdf-print">
              <a class="btn  prk_button" target="_BLANK" href="park-pdf-page?prk_add_id=<?php echo base64_encode($prk_add_id); ?>"><i class="fa fa-print"></i> PRINT QR CODE</a>
            </div>
          </div>                   
        </div> 
        <div class="col-lg-1"></div>
      </div>
    </div>
  </div>  
</div>
<?php include"all_nav/footer.php"; ?>


