<!-- ################################################
Description: Tower detalis.
Developed by: Rakhal Raj 
Created Date: 12-03-2018
####################################################-->
<?php include "all_nav/header.php";
  $tower_name='';
  $flat_name='';
  $living_status='';
  $effective_date='';
  $end_date='';
  $tower_id='';
  $flat_id='';
  $crud_type='LS';
  $flat_master=flat_master_list($prk_admin_id,$tower_id,$flat_id);
  $flat_master_list = json_decode($flat_master, true);
  // $flat_details=flat_details($prk_admin_id,$tower_name,$flat_name,$effective_date,$end_date,$parking_admin_name,$flat_id,$crud_type);
  // $flat_details_list = json_decode($flat_details, true);
  $tower_lis = tower_list($prk_admin_id);
  $tower_list = json_decode($tower_lis, true);
?>
<style type="text/css">
  .size{
    font-size: 11px;
  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
  .error_size{
    font-size: 11px;
    color: red;
  }
  .error{
    font-size: 11px;
    color: red;
  }
  .error_size{
    font-size: 11px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
<link href="../lib/highlightjs/github.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../lib/select2/css/select2.min.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">

    <h5 class="am-title">Flat Details<//?=$flat_master?></h5>
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-lg-3">
          <div class="form-group mg-t-6">
            <select name="tower_name" id="tower_name" style="opacity: 0.8;font-size:14px">
              <option value="">All Tower</option>
              <?php if($tower_list['status']){
                foreach($tower_list['tower_list'] as $value){?>
                <option value="<?=$value['tower_id'];?>"><?=$value['tower_name'];?></option>
              <?php }}?>
            </select>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="form-group mg-t-6">
            <select name="flat_name" id="flat_name" style="opacity: 0.8;font-size:14px">
              <option value="">All Flat</option>
            </select>
          </div>
        </div>
        <div class="col-md-2">

          <input type="button" value="Search" class="btn btn-block btn-primary prk_button mg-b-10" name="search" id="search">
        </div>
      </div>
      <div class="table-wrapper">
        <table id="datatable1" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th style="text-align: center;">Sl No.</th>
              <th >Tower Name</th>
              <th >Flat Name</th>
              <th >Owner Name</th>
              <th >Living Status</th>
              <th >Tenant Name</th>
              <th style="text-align: center;">Charge</th>
              <th style="text-align: center;">ADD</th>
            </tr>
          </thead>
          <tbody id="trn_show"></tbody>
        </table>
      </div>
    </div>
  </div>
<?php include"all_nav/footer.php"; ?>
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="../lib/datatables-responsive/jquery.dataTables.min.js"></script>
<script src="../lib/datatables-responsive/dataTables.buttons.min.js"></script>
<script src="../lib/datatables-responsive/jszip.min.js"></script>
<script src="../lib/datatables-responsive/pdfmake.min.js"></script>
<script src="../lib/datatables-responsive/vfs_fonts.js"></script>
<script src="../lib/datatables-responsive/buttons.html5.min.js"></script>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var prk_admin_id = "<?php echo $prk_admin_id; ?>";
  var parking_admin_name="<?=$parking_admin_name?>";
  var token="<?=$token?>";
  $("#tower_name").select2();
  $("#flat_name").select2();
  all_tower_flat_list();
  $('#tower_name').on("change", function() {
    var id=($("#tower_name").val());
    var urlCity = prkUrl+'flat_list.php';
    $.ajax ({
      type: 'POST',
      url: urlCity,
      data: {
        tower_id:id,
        prk_admin_id:prk_admin_id,
        parking_admin_name:parking_admin_name,
        token:token
      },
      success : function(data) {
        // alert(data);
        var obj=JSON.parse(data);
        var areaOption = "<option value=''>All Flat</option>";
        // alert(obj['status']);
        if (obj['status']) {
          $.each(obj['flat_list'], function(val, key) {
            areaOption += '<option value="' + key['flat_id'] + '">' + key['flat_name'] + '</option>'
          });
        }
        $("#flat_name").html(areaOption);
        $("#flat_name").select2();
      }
    });
  });
  $('#search').click(function(){

    all_tower_flat_list();
  });
  function all_tower_flat_list(){
    $('#refresh_button').show();
    var urlPrkAreGateDtl = prkUrl+'flat_master_list.php';
    var t_id=($("#tower_name").val());
    var f_id=($("#flat_name").val());
    $.ajax({
      url :urlPrkAreGateDtl,
      type:'POST',
      data :{
        'prk_admin_id':prk_admin_id,
        'token': token,
        'tower_id':t_id,
        'flat_id':f_id
      },
      dataType:'html',
      success  :function(data){
        // alert(data);
        var json = $.parseJSON(data);
        var demoLines="";
        if (json.status){
          var c = 0;
          for (var key in json.flat_master_list) {
            var keyy = json.flat_master_list[key];
            var tower_id=keyy['tower_id'];
            var tower_name=keyy['tower_name'];
            var flat_id=keyy['flat_id'];
            var flat_name=keyy['flat_name'];
            var living_status=keyy['living_status'];
            var living_status_full=keyy['living_status_full'];
            var res_mem_name=keyy['res_mem_name'];
            var owner_name=keyy['owner_name'];
            var tenant_name=keyy['tenant_name'];
            // var tenant_name=keyy['tenant_name'];
            // var tenant_name=keyy['tenant_name'];
            // $liv_name=($value['living_status']=='R')?$value['res_mem_name']:$value['owner_name'];
            var liv_name=(living_status=='R')?res_mem_name:owner_name;
            var payment_url='maintenance-transections?list1_id='+tower_id+'&list1_name='+tower_name+'&list2_id='+flat_id+'&list2_name='+flat_name;
            var flat_mas_add_url='flat-master-delails?list1_id='+tower_id+'&list1_name='+tower_name+'&list2_id='+flat_id+'&list2_name='+flat_name;
            // alert(keyy['flat_name']);
            c +=1;
            demoLines += '<tr>\
            <td style="text-align: center;"> '+c+'</td>\
            <td>'+keyy['tower_name']+'</td>\
            <td>'+keyy['flat_name']+'</td>\
            <td>'+liv_name+'</td>\
            <td>'+keyy['living_status_full']+'</td>\
            <td>'+keyy['tenant_name']+'</td>\
            ';
            if (living_status=='T'|| living_status=='O'|| living_status=='R') {
            // if (keyy['tran_flag']=='CR') {
              demoLines += '<td style="text-align: center;"><a href="'+payment_url+'" data-toggle="tooltip" data-placement="top" title="Charge"><i class="fa fa-rupee" style="font-size:37px;"></i></a></td>';
            }else{
              demoLines += '<td style="text-align: center;"><a href="#" data-toggle="tooltip" data-placement="top" title="Charge"><i class="fa fa-rupee" style="font-size:37px; color: #9bafc4;"></i></a></td>';
            }
            demoLines += '<td style="text-align: center;"><a href="'+flat_mas_add_url+'" data-toggle="tooltip" data-placement="top" title="Add"><i class="fa fa-plus-square" style="font-size:30px"></i></a></td>';
            demoLines +='</tr>';
          }
        }
        $('#datatable1').dataTable().fnDestroy();
        $("#trn_show").html(demoLines);
        datatable_show();
      }
    });
  }
  function datatable_show(){
    $('#datatable1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      language: {
      searchPlaceholder: 'Search Here',
      sSearch: '',
      lengthMenu: '_MENU_ Page',
      },
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      buttons: [
        {
          text: '<span id="refresh_button"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
          action: function ( e, dt, node, config ) {
              // alert( 'Button activated' );
              all_tower_flat_list();
          }
        },
        { 
          extend: 'copyHtml5', 
          //footer: true,
          title: 'TOWER & FLAT DETAILS SHEET' 
        },
        { 
          extend: 'excelHtml5', 
          //footer: true,
          title: 'TOWER & FLAT DETAILS SHEET'  
        },
        {
            extend: 'pdfHtml5',
            //footer: true,
            title: 'TOWER & FLAT DETAILS SHEET',
            customize: function(doc) {
              doc.content[1].margin = [ 0, 0, 0, 0 ] //left, top, right, bottom
            }
        },
      ]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    $('#refresh_button').hide();
  }
</script>