<?php
/*
Description: Parking area all gate dtl list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_area_gate_dtl_list($prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
   
   $sql = "SELECT * FROM `prk_area_gate_dtl` 
    WHERE `prk_admin_id`='$prk_admin_id' 
    AND `active_flag`='".FLAG_Y."' 
    AND `del_flag`='".FLAG_N."'
    AND  '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(`prk_area_gate_dtl`.`effective_date`, '%d-%m-%Y'), '%Y-%m-%d')    
    AND  DATE_FORMAT(STR_TO_DATE(IFNULL(`prk_area_gate_dtl`.`end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')  
    ORDER BY `prk_area_gate_id` DESC";
    // ORDER BY `products`.`product_category_id` DESC ,`naam` ASC
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val) 
    {
        $response['status'] = 1;
        $response['message'] = 'data ssucessfull';
        $park['prk_area_gate_id'] = $val['prk_area_gate_id'];
        $park['prk_area_gate_name'] = $val['prk_area_gate_name'];
        $park['prk_area_gate_dec'] = $val['prk_area_gate_dec'];
        $park['prk_area_gate_landmark'] = $val['prk_area_gate_landmark'];
        $park['prk_area_gate_type'] = $val['prk_area_gate_type'];
        $park['effective_date'] = $val['effective_date'];
        $park['end_date'] = $val['end_date'];
        $park['prk_qr_code'] = $val['prk_qr_code'];
        
        //$allplayerdata = array_merge($allplayerdata, $response);
        //echo json_encode($response);
        array_push($allplayerdata, $park);
        //$response['park'] = $allplayerdata;
    }
    return $allplayerdata;
    // return json_encode($allplayerdata);
}
?>