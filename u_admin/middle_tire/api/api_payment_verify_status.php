<?php 
/*
Description: user payment status show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function payment_verify_status($payment_dtl_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT `veh_number`,`payment_rec_status` FROM `payment_dtl` WHERE `payment_dtl_id`='$payment_dtl_id' AND `payment_dtl`.`pay_dtl_del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $response['status'] = 1;
    $response['message'] = 'Successful';    
    $pay_rec_sta = $val['payment_rec_status'];
    if(empty($pay_rec_sta)){
        $pay_rec_sta = 'R';
    }
    $response['payment_rec_status'] = $pay_rec_sta;
    // $response['payment_rec_status'] = $val['payment_rec_status'];
    $response['veh_number'] = $val['veh_number'];
    return json_encode($response); 
}
?>