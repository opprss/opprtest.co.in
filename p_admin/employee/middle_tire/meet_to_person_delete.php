<?php 
/*
Description: Parking visiter in list
Developed by: Rakhal Raj Mandal
Created Date: 31-05-2018
Update date : 
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','prk_user_username','token','me_to_per_id'))){
    if(isEmpty(array('prk_admin_id','prk_area_user_id','prk_user_username','token','me_to_per_id'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $prk_user_username = trim($_POST['prk_user_username']);
        $me_to_per_id = trim($_POST['me_to_per_id']);
        $token = trim($_POST['token']);
        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            //call Common api
            $response = meet_to_person_delete($prk_admin_id,$me_to_per_id,$prk_user_username);
        }else{

            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response); 
} 
echo $response;
?> 