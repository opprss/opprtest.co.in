<?php
/*
Description: check list empty. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function isEmpty($params){
	foreach ($params as $param) {
		if(empty($_POST[$param])){
			return false; 
		}
	}
	return true; 
}

/*is numiric*/

function isNumeric($params){
	foreach ($params as $param) {
		if(!is_numeric($_POST[$param])){
			return false; 
		}
	}
	return true; 
}
?>