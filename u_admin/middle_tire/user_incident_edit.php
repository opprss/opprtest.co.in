<?php
/*
Description: new user vehicle parking parking area 
Developed by: Rakhal Raj Mandal
Created Date: 10-04-2018
Update date : 17-05-2018,25-05-2018,
*/
require_once 'api/reg_api.php';
// include_once 'api/global_api.php';
$response = array();
$re = array();
if(isAvailable(array('user_incident_id','user_incident_number','user_incident_name','user_incident_type','user_incident_dtl','prk_admin_id','user_admin_id','mobile','token'))){
    if(isEmpty(array('user_incident_id','user_incident_number','user_incident_name','user_incident_type','user_incident_dtl','prk_admin_id','user_admin_id','mobile','token'))){
        
        $user_incident_id = trim($_POST['user_incident_id']);
        $user_incident_number = trim($_POST['user_incident_number']);
        $user_incident_name = trim($_POST['user_incident_name']);
        $user_incident_type = trim($_POST['user_incident_type']);
        $user_incident_dtl = trim($_POST['user_incident_dtl']);
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
        $token = trim($_POST['token']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){ 
            /*$result="";
                $char="987654321001234567899876543100123456789";
                $chararray=str_split($char);
                for ($i=0; $i < 6; $i++) { 
                    $randItem=array_rand($chararray);
                    $result.="".$chararray[$randItem];
                }
            $user_incident_number='INC-'.$result;*/
            $response = user_incident_edit($user_incident_id,$user_incident_name,$user_incident_type,$user_incident_dtl,$user_incident_number,$prk_admin_id,$user_admin_id,$mobile);
        }else{
            $response = $resp; 
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>