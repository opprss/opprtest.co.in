<?php 
/*
Description: parkin area gate list show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function emp_page_permissions_list($prk_admin_id,$prk_area_user_id,$prk_user_username,$prk_area_gate_type){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    if ($prk_area_gate_type==FLAG_S) {
    	
    	$oth1='3';
    }else{

    	$oth1='7';
    }
   	$sql="SELECT pup.form_id,
		pup.permission_id,
		pup.prk_admin_id,
		apup.prk_area_user_id,
		fd.form_name,
		fd.form_display_name,
		fd.form_img,
		fd.oth2,
		fd.form_php_name
		FROM prk_user_permission pup,form_dtl fd,admin_prk_user_permission apup
		WHERE fd.form_id=pup.form_id
		AND pup.active_flag='".FLAG_Y."'
		AND fd.active_flag='".FLAG_Y."'
		AND fd.oth1 BETWEEN '1' AND '$oth1'
		AND pup.del_flag='".FLAG_N."'
		AND pup.prk_admin_id='$prk_admin_id'
		AND apup.del_flag='".FLAG_N."'
		AND apup.active_flag='".FLAG_Y."'
		AND apup.form_id=pup.form_id
		AND apup.prk_admin_id=pup.prk_admin_id
		AND apup.prk_area_user_id='$prk_area_user_id'
		ORDER BY fd.form_order ASC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
	    $arr_catagory = $query->fetchAll();
	    foreach($arr_catagory as $val){
	        $park['permission_id'] = $val['permission_id'];
	        // $park['prk_admin_id'] = $val['prk_admin_id'];
	        // $park['prk_area_user_id'] = $val['prk_area_user_id'];
	        $park['form_name'] = $val['form_name'];
	        $park['form_display_name'] = $val['form_display_name'];
	        $park['form_php_name'] = $val['form_php_name'];
	        $park['form_img'] = $val['form_img'];
	        $park['oth2'] = $val['oth2'];
	        array_push($allplayerdata, $park);
	    }
        $response['status'] = 1;
        $response['message'] = 'Successful';
	    $response['permission_list'] = $allplayerdata;
	}else{
		$response['status'] = 0;
		// $response['oth1'] = $oth1;
	    $response['message'] = 'List Empty';
	}    
    return json_encode($response);
}
?>