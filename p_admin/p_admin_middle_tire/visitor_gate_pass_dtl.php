<?php
/*
Description: Parking visitor gate pass issue.
Developed by: Rakhal Raj Mandal
Created Date: 31-10-2018
Update date :31-10-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();

if(isAvailable(array('prk_admin_id','visitor_mobile','visitor_name','visitor_gender','visitor_add1','visitor_add2','visitor_state','visitor_city','visitor_landmark','visitor_country','visitor_pin_cod','id_name','id_number','visitor_gate_pass_num','effective_date','end_date','inserted_by','visitor_vehicle_type','visitor_vehicle_number'))){

    if(isEmpty(array('prk_admin_id','visitor_mobile','visitor_name','visitor_gender','visitor_add1','visitor_state','visitor_city','visitor_country','visitor_pin_cod','id_name','id_number','visitor_gate_pass_num','effective_date','end_date','inserted_by'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $visitor_mobile = trim($_POST['visitor_mobile']);
        $visitor_name = trim($_POST['visitor_name']);
        $visitor_gender = trim($_POST['visitor_gender']);
        $visitor_add1 = trim($_POST['visitor_add1']);
        $visitor_add2 = trim($_POST['visitor_add2']);
        $visitor_state = trim($_POST['visitor_state']);
        $visitor_city = trim($_POST['visitor_city']);
        $visitor_landmark = trim($_POST['visitor_landmark']);
        $visitor_country = trim($_POST['visitor_country']);
        $visitor_pin_cod = trim($_POST['visitor_pin_cod']);
        $id_name = trim($_POST['id_name']);
        $id_number = trim($_POST['id_number']);
        $visitor_gate_pass_num = trim($_POST['visitor_gate_pass_num']);
        $effective_date = $_POST['effective_date'];
        $end_date = $_POST['end_date'];
        $inserted_by = trim($_POST['inserted_by']);
        $visitor_vehicle_type = trim($_POST['visitor_vehicle_type']);
        $visitor_vehicle_number = trim($_POST['visitor_vehicle_number']);
        $visitor_img = trim($_POST['visitor_img']);
        $id_proof_img = trim($_POST['id_proof_img']);
        // $number = count($_POST["tower_name"]); 
        $time=time(); 
        if (!empty($visitor_img)) {      
            $PNG_TEMP_DIR ='./../uploades/'.$inserted_by.'/visitor_pro_img/';       
            if (!file_exists($PNG_TEMP_DIR)) {
                $old_mask = umask(0);
                mkdir($PNG_TEMP_DIR, 0777, TRUE);
                umask($old_mask);
            }
            $image_parts = explode(";base64,", $visitor_img);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
          
            $image_base64 = base64_decode($image_parts[1]);
            $fileName = uniqid() . '.png';
            $file = $PNG_TEMP_DIR . $fileName;
            file_put_contents($file, $image_base64);
            $visitor_img = "uploades/".$inserted_by."/visitor_pro_img/".$fileName;
            // $visitor_img1 = $PNG_TEMP_DIR."".$time."_".$_FILES['visitor_img']['name'];
            // @copy($_FILES['visitor_img']['tmp_name'],$visitor_img1);
        }else{

            $visitor_img = '';
        }
        if (!empty($id_proof_img)) {
            $PNG_TEMP_DIR ='./../uploades/'.$inserted_by.'/vis_id_prooof_img/';       
            if (!file_exists($PNG_TEMP_DIR)) {
                $old_mask = umask(0);
                mkdir($PNG_TEMP_DIR, 0777, TRUE);
                umask($old_mask);
            }
            $image_parts = explode(";base64,", $id_proof_img);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
          
            $image_base64 = base64_decode($image_parts[1]);
            $fileName = uniqid() . '.png';
            $file = $PNG_TEMP_DIR . $fileName;
            file_put_contents($file, $image_base64);
            $vis_id_prooof_img = "uploades/".$inserted_by."/vis_id_prooof_img/".$fileName;
            // $vis_id_prooof_img1 = $PNG_TEMP_DIR."".$time."_".$_FILES['id_proof_img']['name'];
            // @copy($_FILES['id_proof_img']['tmp_name'],$vis_id_prooof_img1);
        }else{
            
            $vis_id_prooof_img = '';
        }
        $response = visitor_gate_pass_dtl($prk_admin_id,$visitor_mobile,$visitor_name,$visitor_gender,$vis_id_prooof_img,$visitor_add1,$visitor_add2,$visitor_state,$visitor_city,$visitor_landmark,$visitor_country,$visitor_pin_cod,$visitor_img,$id_name,$id_number,$visitor_gate_pass_num,$effective_date,$end_date,$inserted_by,$visitor_vehicle_type,$visitor_vehicle_number);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>