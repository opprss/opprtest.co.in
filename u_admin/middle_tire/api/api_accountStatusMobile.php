<?php
/*
Description: user Account status check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function accountStatusMobile($mobile){
    $response = array();
    global $pdoconn;
    if(mobileChecker($mobile)=='true'){
        $sql ="SELECT `user_admin_id`,`user_ac_status` FROM `user_admin` WHERE `user_mobile`=:user_mobile";
        $query  = $pdoconn->prepare($sql);
        $query->execute(array('user_mobile'=>$mobile));
        $count=$query->rowCount();
        if($count>0){
            $row = $query->fetch();
            $ac_sta =  $row['user_ac_status'];
            if($ac_sta=='A'){
                $response['status'] = 0;
                $response['message'] = 'User Already Exists';
            }
            if($ac_sta=='T'){
                $response['status'] = 0;
                $response['message'] = 'User Temporary Locked';
            }
            if($ac_sta=='L'){
                $response['status'] = 0;
                $response['message'] = 'User Locked';
            }
            if($ac_sta=='D'){
                $response['status'] = 0;
                $response['message'] = 'User Does Not Exist';
            }
            if($ac_sta=='P'){
                $response['status'] = 0;
                $response['message'] = 'User Payment Due Please Contact Customer Care';
            }
            return ($response);
        }else{
            $response['status'] = 1;
            $response['message'] = 'Right User Mobile Number'; 
            return ($response);
        }
    }else{
            $response= mobileChecker($mobile); 
            return ($response);
    }
    
}
?>