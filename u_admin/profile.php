<!-- ################################################
  
Description: user full profile show.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php
include "all_nav/header.php"; 

if (isset($_SESSION['user_admin_id'])) {
  $user_admin_id = $_SESSION['user_admin_id'];
  $response = array();
  $response_address = array();
  $response = user_profile_show($user_admin_id);
  $response = json_decode($response, true);
  if (!empty($response)) {
    $user_name = $response['user_name'];
    $user_nick_name = $response['user_nick_name'];
    $user_gender = $response['user_gender'];
    $user_dob = $response['user_dob'];
    $user_email = $response['user_email'];
    $user_email_verify = $response['user_email_verify'];
    $user_aadhar_id = $response['user_aadhar_id'];  
    $user_img = $response['user_img'];  
    $visitor_verify_flag = $response['visitor_verify_flag'];  
  }
}
?>

<!-- header position -->

    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Profile<?php //echo $visitor_verify_flag; ?></h5>
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card single pd-20 pd-sm-40 col-md-12">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">

            <div class="row">
                <div class="col-md-2 text-center mg-b-20 mg-t-20">
                  
                  <?php
                    if (!empty($user_img)) {
                  ?>
                    <img src="<?php echo USER_BASE_URL.$user_img;?>" alt="" style="width: 100px;">                      
                  <?php
                    }else{
                  ?>
                    <i class="fa fa-user-circle-o tx-info tx-100 tx"></i>
                  <?php
                    }
                  ?>
                </div>
                <div class="col-md-2 text-center mg-b-20 mg-t-20">
                  
                  <?php 
                    $qrcode=$user_mobile.'-OPPR';
                      $code = '<center><img style="width: 127px;" src="https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl='.$qrcode.'" title="Link to Google.com"></center>';
                      echo $code;
                  ?>
                </div>
                <div class="col-md-8 pd-l-40">
                  <h5><?php echo $user_name ?></h5>
                  <h6><?php echo $user_nick_name ?></h6>
                  <hr>
                  <div class="tx-15">
                    <a href="edit-profile" class="btn btn-info btn-icon mg-b-20 pull-right" data-toggle="tooltip" data-placement="top" title="Modify basic">
                      <div style=""><i class="fa fa-pencil"></i></div>
                    </a>
                    <p class="mg-b-0">Mobile - <?php echo $user_mobile ?> <font color="green"><i class="fa fa-check" aria-hidden="true"></i></font></p>
                    <p class="mg-b-0">Email - <?php echo $user_email ?> 
                      <?php
                        if ($user_email_verify == "T") {
                         ?>
                         <font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>
                         <?php 
                        }else{
                        ?>
                        <a class="verify" href="verify-email" data-toggle="tooltip" data-placement="top" title="Verify">
                        <i class="fa fa-times"></i>
                      </a>
                        <?php
                        }
                       ?>
                    </p>
                    <p class="mg-b-0">DOB - <?php echo $user_dob ?></p>
                    <p class="mg-b-0">Gender - <?php
                     		//echo $user_gender
                     		if($user_gender == "M")
                     			echo "Male";
                     		elseif($user_gender == "F")
                     			echo "Female";
                     		else
                     			echo "Other";
                     ?></p>
                    <p class="mg-b-0">Aadhar ID - <?php if (isset($user_aadhar_id) && !empty($user_aadhar_id)) echo $user_aadhar_id; else echo "N/A"; ?></p>
                    <p class="mg-b-0">Visitor Verify -
                      <select style="width:70px;" id="visitor_verify">
                        <option value="Y">Yes</option>
                        <option value="N">NO</option>
                      </select><i class="fa fa-caret-down" aria-hidden="true"></i>
                    </p>
                  </div>
                </div>
            </div>
          </div>
        </div><!-- card -->
      </div><!-- am-pagebody -->
      

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">
  var usrUrl = "<?php echo USER_URL; ?>"; 
  var user_admin_id = "<?php echo $user_admin_id; ?>"; 
  var user_mobile = "<?php echo $user_mobile; ?>"; 
  var token = "<?php echo $token; ?>"; 
  var visitor_verify_flag = "<?php echo $visitor_verify_flag; ?>"; 
  var visitor_verif = usrUrl+'user_visitor_verify.php';

  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
    $('#visitor_verify').val(visitor_verify_flag);

    $('#visitor_verify').on("change", function() {
      var visitor_verify=($("#visitor_verify").val());
      // alert(visitor_verify);
      // alert(visitor_verif);
      $.ajax({
        url :visitor_verif,
        type:'POST',
        data :
        {
          'visitor_verify_flag':visitor_verify,
          'user_admin_id':user_admin_id,
          'mobile':user_mobile,
          'token':token
        },
        dataType:'html',
        success  :function(data)
        {
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success',
              content: "<p style='font-size:0.9em;'>Successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                    location.reload(true);
                }
              }
            });
          }else{                
            if (json.session==0) {
              window.location.replace("logout.php");
            }else{
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                type: 'red',
                buttons: {
                  Ok: function () {
                      location.reload(true);
                  }
                }
              });
            }
          }
        }
      });
    });
  });
</script>