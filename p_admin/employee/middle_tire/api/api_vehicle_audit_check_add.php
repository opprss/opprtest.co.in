<?php 
/*
Description: vehicle out all payment detailas data save. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/  
function vehicle_audit_check_add($prk_admin_id,$prk_gate_pass_id,$user_admin_id,$tower_id,$tower_name,$flat_id,$flat_name,$owner_name,$mobile,$veh_type,$veh_number,$allocated_park_space,$park_space,$remarks,$park_veh_for,$inserted_by,$vehicle_type_dec){
	$response = array();
  global $pdoconn;
  $unique_id=uniqid();
  $sql = "INSERT INTO `vehicle_audit_checklist`(`vehicle_audit_check_id`, `prk_admin_id`, `prk_gate_pass_id`, `user_admin_id`, `tower_id`, `tower_name`, `flat_id`, `flat_name`, `owner_name`, `mobile`, `veh_type`, `veh_number`, `allocated_park_space`, `park_space`, `remarks`, `audit_date`, `park_veh_for`, `inserted_by`, `inserted_date`) VALUES ('$unique_id','$prk_admin_id','$prk_gate_pass_id','$user_admin_id','$tower_id','$tower_name','$flat_id','$flat_name','$owner_name','$mobile','$veh_type','$veh_number','$allocated_park_space','$park_space','$remarks','".TIME."','$park_veh_for','$inserted_by','".TIME."')";
  $query  = $pdoconn->prepare($sql);
  if($query->execute()){
    vehicle_audit_check_add_mail_send($user_admin_id,$prk_admin_id,$prk_gate_pass_id,$allocated_park_space,$park_space,$vehicle_type_dec,$veh_number,$flat_name,$tower_name);
    $sql="SELECT `token`.`device_token_id`,`token`.`device_type`,`token`.`token_value` FROM `token` WHERE `token`.`active_flag`='".FLAG_Y."' AND `token`.`user_admin_id`='$user_admin_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $device_token_id =  $val['device_token_id'];
        $device_type =  $val['device_type'];
        if ($device_type=='ANDR') {
            $payload = array();
            // $payload['area_name'] = $prk_area_name;
            // $payload['name'] = $veh_number;
            $payload['in_date'] = TIME;
            $payload['url'] = 'PARKING CHECK';
            $title = 'OPPRSS';
            $message = $veh_number.' PARKED AT WRONG PLACE ('.$park_space.').';
            $push_type = 'individual';
            $include_image='';
            $clickaction='PARKING CHECK';
            $oth1='';
            $oth2='';
            $oth3='';
            $paylo = push_send($title,$message,$push_type,$include_image,$device_token_id,$payload,$clickaction,$oth1,$oth2,$oth3);
        }
    }
	  $response['status'] = 1;
    $response['message'] = 'Successful';
  }else{
    $response['status'] = 0;
    $response['message'] = 'Not Successful ';
  }  
  return json_encode($response); 
}
function vehicle_audit_check_add_mail_send($user_admin_id,$prk_admin_id,$prk_gate_pass_id,$allocated_park_space,$park_space,$vehicle_type_dec,$veh_number,$flat_name,$tower_name){
    global $pdoconn;
    $sql = "SELECT `user_detail`.`user_detail_id`,
        `user_detail`.`user_name`,
        `user_detail`.`user_nick_name`,
        `user_detail`.`user_gender`,
        `user_detail`.`user_dob`,
        `user_detail`.`user_email`,
        `user_detail`.`user_email_verify`,
        `user_detail`.`user_aadhar_id`,
        `user_detail`.`user_img`,
        `user_admin`.`user_mobile`,
        `user_admin`.`ad_flag`,
        `user_admin`.`sms_flag`,
        `user_admin`.`visitor_verify_flag`
        FROM `user_detail`,`user_admin` 
        WHERE `user_detail`.`user_admin_id`='$user_admin_id' 
        AND `user_detail`.`active_flag`='".FLAG_Y."'
        AND `user_admin`.`user_admin_id`=`user_detail`.`user_admin_id`";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $user_name = $val['user_name'];
    $user_email = $val['user_email'];
    // $user_mobile = $val['user_mobile'];
    $prk_in =prk_inf($prk_admin_id);
    $json = json_decode($prk_in);
    $prk_area_name=$json->prk_area_name;
    // $prk_admin_name=$json->prk_admin_name;
    $prk_area_address = prk_area_address_show($prk_admin_id);
    $area_address = json_decode($prk_area_address, true); 
    $prk_address = $area_address['prk_address'];
    $prk_land_mark = $area_address['prk_land_mark'];
    $prk_add_area = $area_address['prk_add_area'];
    $state_name = $area_address['state_name'];
    $city_name = $area_address['city_name'];
    $prk_add_country = $area_address['prk_add_country'];
    $prk_add_pin = $area_address['prk_add_pin'];
    // $gate_pass_dtl = prk_gate_pass_dtl_show($prk_gate_pass_id);
    // $vehicle_type_dec = $gate_pass_dtl['vehicle_type_dec'];
    // $veh_number = $gate_pass_dtl['veh_number'];
    // $flat_name = $gate_pass_dtl['flat_name'];
    // $tower_name = $gate_pass_dtl['tower_name'];
    // $living_status = $gate_pass_dtl['living_status'];
    // $veh_owner_name = $gate_pass_dtl['veh_owner_name'];
    $email_logo = EMAIL_LOGO."oppr-logo.png";
    $email_logo_fb=EMAIL_LOGO_fb."fb.jpg";
    $email_logo_gmail=EMAIL_LOGO_gmail."google.png";
    $email_logo_indeed=EMAIL_LOGO_indeed."inidied.png";
    
    $html_mail_con='<html>
        <body>
          <div style="border: 1px solid #00b8d4; width: 600px;">
            <div style="background-color: #00b8d4; height: 80px; width: 600px;">
              <span style="float: left;">
             <img src="'.$email_logo.'" height="50" width="100">
              </span>
              <br><div style="margin-top: 5px">
                <a href="#" style="margin-left: 350px;width: 20px;height: 20px;">
                  
                    <img src="'.$email_logo_fb.'" alt="FaceBook" height="30" width="30" / >
                </a>

               <span class="google">
                <a href="#" >

                  <img src="'.$email_logo_gmail.'" alt="Gmail" height="30" width="30" />
                </a>
              </span>
               <span class="linkedin">
                <a href="#">
                 <img src="'.$email_logo_indeed.'" height="30" width="30" alt="Indeed" />
                </a>
              </span>
            </div>
              <div style="margin-top: 80px; margin-left: 20px;">
                <div style="margin-top: 60px; text-align: center;">
                  <span>
                    <b>Vehicle Audit check</b>
                  </span>
                </div>
                <div style="margin-top: 20px; margin-left: 15px;">
                  <span>
                    <b>Dear Sir/Madam, '.$user_name.'</b>
                  </span>
                </div><br>
                <div style="margin-left: 15px;">
                  <span>'.$prk_area_name.'</span><br>
                  <span>Tower '.$tower_name.',Flat '.$flat_name.'</span><br>
                </div>
              </div>
              <div style="margin-top: 20px; margin-left: 30px;width: 555px">
               <p>The vehicle number is '.$veh_number.', Allocated park space is <b>'.$allocated_park_space.'</b> but you are the park the vehicle <b>'.$park_space.'</b> place.</p>

               <p>In case you need any further clarification for the same, please do get in touch immediately with care@opprss.com or your nearest branch. </p>

               <p> This E-Notification was automatically generated. Please do not reply to this mail. For any suggestions / feedback, please click on Feedback link provided on<a href="http://opprss.com"> www.opprss.com</a></p>
              </div>
              <div style="margin-top: 10px; margin-left: 20px;">

                  <b>Regards,</b><br>
                  <p>OPPR Team.<br>
                  A Smart Parking & Security Solution.
                </p><div style="width: 555px">
                   &emsp;"The information contained in this electronic message and any attachments to this message are intended for exclusive use of the addressee(s) and may contain confidential or privileged information. If you are not the intended recipient, please notify the sender at OPPR Software Solution Pvt Ltd or care@opprss.com immediately and
                destroy all copies of this message and any attachments. The views expressed in this E-mail message / Attachments, are those of the individual sender."
                </div>
              </div>

              </div>
              <div style="margin-top: 650px; text-align: center;">
              <span><a href="http://opprss.com/about-us">About Us</a> |</span> <span><a href="http://opprss.com/contact-us">Contect Us </a></span>
              </div>
              <div style="text-align: center; margin-top: 20px;padding-bottom: 20px; font-size: 11px"> &copy; 2017 BY OPPR SOFTWARE SOLUTION. ALL RIGHTS RESERVED.<br><br>
              </div>
            </div>
        </div>
      </body>
    </html>';
    email_massage($user_email,$html_mail_con);
}
?> 