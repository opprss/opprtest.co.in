<?php
/*
Description: vehicle update.
Developed by: Rakhal Raj Mandal
Created Date: 15-01-2018
Update date : 06-05-2018
*/ 
function user_visitor_verify($user_admin_id,$visitor_verify_flag,$mobile){
    global $pdoconn;

    $sql = "UPDATE `user_admin` SET `visitor_verify_flag`='$visitor_verify_flag',`updated_by`='$mobile',`updated_date`='".TIME."' WHERE `user_admin_id`='$user_admin_id' AND `user_ac_status`='".FLAG_A."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Update Successful';
        $response = json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Update Not Successful';
        $response = json_encode($response);
    }
    return $response;
}
?>