<?php
  include('../global_asset/config.php');
  session_start();
  if(!isset($_SESSION['user_admin_id'])){
    header('location:../signin');
  }
  $user_id = $_SESSION["user_admin_id"];
  $mobile = $_SESSION["mobile"];
  //echo $user_id;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>OPPRSS | Complete Your Registration</title>
    <link rel="shortcut icon" href="<?php echo FAVICON ?>" type="image/x-icon">
    <!-- vendor css -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">
    <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">  

    <!-- calender -->  
    <link rel="stylesheet" href="../library/date/css/pickmeup.css" type="text/css" />
    <script type="text/javascript" src="../library/date/js/pickmeup.js"></script>
    <script
      src="https://code.jquery.com/jquery-3.2.1.js"
      integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
      crossorigin="anonymous"></script>
    <script src="https://kendo.cdn.telerik.com/2017.3.1026/js/kendo.all.min.js"></script>

    <!-- Amanda CSS -->
    <link rel="stylesheet" href="../css/amanda.css">
    <link rel="stylesheet" type="text/css" href="../css/style-up.css">


    <!-- <style type="text/css">
      .ui-datepicker{
       font-size:10px;
      }
    </style> -->
  </head>

  <body>

    <div class="am-signup-wrapper">
      <div class="am-signup-box">
        <div class="row no-gutters">
          <div class="col-lg-5">
            <div>
              <img src="<?php echo LOGO ?>" width="300px">
              <p>WELCOME TO OPPR SOFTWARE SOLUTION</p>
              <p class="d-none d-sm-block">A private, dynamic young, non-government company making India digital. </p>

              <p>Already have an account? <br> <a href="../signin">Sign In</a></p>
            </div>
          </div>
          <div class="col-lg-7" style="padding: 35px;">
            <h5 class="tx-gray-800 mg-b-25">Complete Your Registration</h5>
            <form id="regCompleteForm">
              <div class="form-group error_show">
                  <input type="text" name="mobile" id="mobile" maxlength="10" class="form-control readonly" placeholder="Mobile" value="<?php echo $mobile?>" readonly="readonly" style="background-color: transparent;">
                  <span id="user_staus_error"></span>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group error_show">
                    <input type="password" name="password" id="password" class="form-control" placeholder="Type password">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group error_show">
                    <input type="password" class="form-control" placeholder="Confirm Password" name="repassword" id="repassword">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group error_show">
                    <input type="text" name="name" id="name" class="form-control" placeholder="Name" onkeyup="nameUppercase()">
                  </div>
                </div>
                <div class="col-md-6">
                  <p style="margin-top: 18px;">
                    <input type="checkbox" class="checkbox" id="agree" name="agree" required>
                    <label for="agree">Terms & Condition <a href="http://opprss.com/legal" target="_blank">more information..</a></label>
                    <br>
                    <label for="agree" class="error block"></label>
                  </p>
                </div>
              </div>
              
              <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <button type="button" class="btn btn-block" id="CompleteRegistration" style="margin-top: 5px">COMPLETE YOUR REGISTER</button>
                    </div><!-- form-group -->
                  </div><!-- col -->
                  <div class="col-md-6">
                     <div class="form-group">
                        <input type="reset" class="btn btn-block" id="configreset" value="RESET" style="margin-top: 5px;">
                    </div>
                  </div>
              </div>              
            </form>
          </div><!-- col-7 -->
        </div><!-- row -->
        <p class="tx-center tx-white-10 tx-12 mg-t-15"><?php echo COPYRIGHT_EMAIL?></p>
      </div><!-- signin-box -->
    </div><!-- am-signin-wrapper -->
    <script src="../lib/popper.js/popper.js"></script>
    <script src="../lib/bootstrap/bootstrap.js"></script>
    <script src="../lib/jquery-ui/jquery-ui.js"></script>
    <script src="../lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
    <script src="../lib/jquery-toggles/toggles.min.js"></script>
    <script src="../lib/highlightjs/highlight.pack.js"></script>
    <script src="../lib/select2/js/select2.min.js"></script>
    <script src="../js/amanda.js"></script>
    <script src="../lib/validation/jquery.validate.js"></script>
  </body>
  <script type="text/javascript">
    var userUrl = "<?php echo USER_URL; ?>";        
    /*Min Age restriction*/
    $.validator.addMethod("minAge", function(value, element, min) {
      var today = new Date();
      var birthDate = new Date(value);
      var age = today.getFullYear() - birthDate.getFullYear();
   
      if (age > min+1) {
          return true;
      }
      var m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
          age--;
      }
   
      return age >= min;
     }, "You are not old enough!");
    /*validation*/
    $( document ).ready( function () {
      $( "#regCompleteForm" ).validate( {
        rules: {
          name: "required",
          agree: "required",
          password: {
            required: true,
            minlength: 5
          },
          repassword: {
            required: true,
            equalTo: "#password"
          }
        },
        messages: {
          name: "Please enter your name",
          agree: "Please Select Terms & Condition",
          password: {
            required: "Please provide a password",
            minlength: "Must have least 5 characters"
          },
          repassword: {
            required: "Please provide confirm password",
            equalTo: "Please enter the same as password"
          }
        }
      });

      /*FORM SUBMIT*/
      $('#CompleteRegistration').click(function () {
        if($("#regCompleteForm").valid()){
          var user_admin_id = "<?php echo $user_id ?>";
          var mobile = $('#mobile').val();
          var password = $('#password').val();
          var name = $('#name').val();
          var t_c_flag= "Y";
          var urlRegistration = userUrl+'compliteRegistration.php';
          // alert(user_admin_id);
          // alert(email);
          $.ajax({
            url :urlRegistration,
            type:'POST',
            data :
            {//user_admin_id','mobile','password','name','t_c_flag'
              'user_admin_id':user_admin_id,
              'mobile':mobile,
              'password':password,
              'name':name,
              't_c_flag': t_c_flag
            },              
            dataType:'html',
            success  :function(data)
            {
              // alert(data);
              var json = $.parseJSON(data);

              if (json.status) {
                window.location='address';
              }else{
                //alert("Somthing went wrong");
                $.alert({
                  icon: 'fa fa-frown-o',
                  theme: 'modern',
                  title: 'Error !',
                  content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
                  type: 'red'
                });
              }
            }
          }).responseText;
        }//if valid end
      });
    });
    /*uppercase*/
    function nameUppercase() {
        var x = document.getElementById("name");
        x.value = x.value.toUpperCase();
    }
  </script>
</html>
