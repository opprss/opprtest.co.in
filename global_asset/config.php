<?php
define('FLAG_A', "A", true);//".FLAG_A."
define('FLAG_B', "B", true);//".FLAG_B."
define('FLAG_C', "C", true);//".FLAG_C."
define('FLAG_D', "D", true);//".FLAG_D."
define('FLAG_E', "E", true);//".FLAG_E."
define('FLAG_F', "F", true);//".FLAG_F."
define('FLAG_G', "G", true);//".FLAG_G."
define('FLAG_H', "H", true);//".FLAG_H."
define('FLAG_I', "I", true);//".FLAG_I."
define('FLAG_J', "J", true);//".FLAG_J."
define('FLAG_K', "K", true);//".FLAG_K."
define('FLAG_L', "L", true);//".FLAG_L."
define('FLAG_M', "M", true);//".FLAG_M."
define('FLAG_N', "N", true);//".FLAG_N."
define('FLAG_O', "O", true);//".FLAG_O."
define('FLAG_P', "P", true);//".FLAG_P."
define('FLAG_Q', "Q", true);//".FLAG_Q."
define('FLAG_R', "R", true);//".FLAG_R."
define('FLAG_S', "S", true);//".FLAG_S."
define('FLAG_T', "T", true);//".FLAG_T."
define('FLAG_U', "U", true);//".FLAG_U."
define('FLAG_V', "V", true);//".FLAG_V."
define('FLAG_W', "W", true);//".FLAG_W."
define('FLAG_X', "X", true);//".FLAG_X."
define('FLAG_Y', "Y", true);//".FLAG_Y."
define('FLAG_Z', "Z", true);//".FLAG_Z."

#--------------------------#
define('FLAG_TR', "TR", true);//".FLAG_TR."
define('FLAG_CR', "CR", true);//".FLAG_CR."
// define('FLAG_Z', "Z", true);//".FLAG_Z."
// define('FLAG_Z', "Z", true);//".FLAG_Z."

/*===================================================================*/
define('OPPR_BASE_URL', 'https://opprtest.co.in/', true);  

define('OPPR_BASE_URL1', OPPR_BASE_URL.'signin', true);
define('P_ADMIN', "p_admin/", true);//".P_ADMIN."
define('U_ADMIN', 'u_admin', true);//".U_ADMIN."
define('USER_BASE_URL', OPPR_BASE_URL.'u_admin/', true);
define('USER_URL', OPPR_BASE_URL.'u_admin/middle_tire/', true);
define('USER_PRO_IMG_URL', OPPR_BASE_URL.'u_admin/uploades/', true);


define('PRK_URL', OPPR_BASE_URL.'p_admin/p_admin_middle_tire/', true);
define('PRK_BASE_URL', OPPR_BASE_URL.'p_admin/', true);
define('PRK_COM_URL', OPPR_BASE_URL.'p_admin/common_api/', true);
define('PRK_PRO_IMG_URL', OPPR_BASE_URL.'p_admin/uploades/', true);
//http://localhost/rakhal/opprsscoin-test/p_admin/common_api/api_prk_user_payment_type_trn_pdf.php
define('IMAGE_URL', OPPR_BASE_URL.'img/', true);

define('QR_CODE_URL', OPPR_BASE_URL.'p_admin/', true);
// define('COPYRIGHT_EMAIL', '&copy; 2018 BY OPPR SOFTWARE SOLUTION PRIVATE LIMITED ALL RIGHTS RESERVED.', true);
define('COPYRIGHT_EMAIL', 'All Rights Reserved By OPPR Software Solution Pvt. Ltd', true);
// define('COPYRIGHT_PDF', 'COPYRIGHT'.' '.chr(169).' '.'2018 BY OPPR SOFTWARE SOLUTION PRIVATE LIMITED ALL RIGHTS RESERVED.', true);
define('COPYRIGHT_PDF', 'All Rights Reserved By OPPR Software Solution Pvt. Ltd', true);
// define('COPYRIGHT', '&copy; 2018 by OPPR Software Solution Pvt. Ltd', true);
define('COPYRIGHT', 'All Rights Reserved By OPPR Software Solution Pvt. Ltd', true);
define('SET_F', 'F', true);
define('SET_T', 'T', true);

// define('LOGO', OPPR_BASE_URL.'img/logonew.png');
// define('FAVICON', OPPR_BASE_URL.'img/favicon.png');
define('LOGO', OPPR_BASE_URL.'img/logo/logonew.png');
define('FAVICON', OPPR_BASE_URL.'img/logo/favicon.png');
define('EMAIL_LOGO', OPPR_BASE_URL.'img/');
define('EMAIL_LOGO_fb', OPPR_BASE_URL.'img/');
define('EMAIL_LOGO_gmail', OPPR_BASE_URL.'img/');
define('EMAIL_LOGO_indeed', OPPR_BASE_URL.'img/');

define('ACTIVE_FLAG_Y', "Y", true);//".ACTIVE_FLAG_Y."

define('ACTIVE_FLAG_N', "N", true);//".ACTIVE_FLAG_N."

define('DEL_FLAG_Y', "Y", true);//".DEL_FLAG_Y."

define('DEL_FLAG_N', "N", true);//".DEL_FLAG_N."

define('SMS_FLAG_N', "N", true);//".SMS_FLAG_N."

define('SMS_FLAG_Y', "Y", true);//".SMS_FLAG_Y."

define('AD_FLAG_N', "N", true);//".AD_FLAG_N."

define('AD_FLAG_Y', "Y", true);//".AD_FLAG_Y."

define('T_C_FLAG_Y', "Y", true);//".T_C_FLAG_Y."
#-----------------------------#
define('VISIT_WITH_VEH_Y', "Y", true);//".VISIT_WITH_VEH_Y."
define('VISIT_WITH_VEH_N', "N", true);//".VISIT_WITH_VEH_N."

define('DGP', "DGP", true);//".DGP."
define('TDGP', "TDGP", true);//".TDGP."
define('PP', "PAID_P", true);//".PP."
define('CASH', "CASH", true);//".CASH."
define('WALLET', "WALLET", true);//".WALLET."

define('DGP_F', "Digital Gatepass", true);//".DGP_F."
define('PP_F', "Paid Parking", true);//".PP_F."
define('TDGP_F', "Temporary Gatepass", true);//".TDGP_F."
#----------------------------------#
define('PARK_AC_STATUS_A', "A", true);//".PARK_AC_STATUS_A."
define('PARK_AC_STATUS_T', "T", true);//".PARK_AC_STATUS_T."
define('PARK_AC_STATUS_L', "L", true);//".PARK_AC_STATUS_L."
define('PARK_AC_STATUS_P', "P", true);//".PARK_AC_STATUS_P."
define('PARK_AC_STATUS_D', "D", true);//".PARK_AC_STATUS_D."


/*--------------------*/
define('DATE', date_default_timezone_set('Asia/Kolkata'), true);
date_default_timezone_set('Asia/Kolkata');

define('TIME', date("Y-m-d H:i:s"), true);//".TIME."

define('TIME_TRN', date("Y-m-d"), true);//".TIME_TRN."
define('TIME_TRN_WEB', date("d-m-Y"), true);//".TIME_TRN_WEB."
define('FUTURE_DATE', "2999-12-31", true);//".FUTURE_DATE."
define('FUTURE_DATE_WEB', "31-12-2999", true);//".FUTURE_DATE_WEB."
/*--------------------*/

define('EMAIL_VERI_T', "T", true);//".EMAIL_VERI_T."

define('EMAIL_VERI_Y', "Y", true);//".EMAIL_VERI_Y."

define('user_prk_veh_verify_status_T', "T", true);//".user_prk_veh_verify_status_T."

define('user_prk_veh_verify_status_F', "F", true);//".user_prk_veh_verify_status_F."
/*-----------------------*/
define('payment_rec_status_I', "I", true);//".payment_rec_status_I."

define('payment_rec_status_P', "P", true);//".payment_rec_status_P."

define('payment_status_F', "F", true);//".payment_status_F."

define('payment_status_T', "T", true);//".payment_status_T."


define('USER_PRK_VEH_VERITY_STATUS_T', "T", true);//".USER_PRK_VEH_VERITY_STATUS_T."

define('USER_PRK_VEH_VERITY_STATUS_F', "F", true);//".USER_PRK_VEH_VERITY_STATUS_F."

define('USER_PRK_VEH_VERITY_STATUS_R', "R", true);//".USER_PRK_VEH_VERITY_STATUS_R."

define('USER_LIC_IMG_STATUS_F', "F", true);//".USER_LIC_IMG_STATUS_F."

define('USER_LIC_IMG_STATUS_T', "T", true);//".USER_LIC_IMG_STATUS_T."

/*29-03-2018-somen*/

define('TYPE_A', "A", true);//".ALL."
define('TYPE_U', "U", true);//".USER."
define('TYPE_P', "P", true);//".PRK."

define('OTP_EXP_TIME', 20, true);//".OTP_EXP_TIME."
/*vatiable*/
define('NEW_REG_SUB_FOR_PRK', 'Welcome to OPPR-Digital Parking Solution', true);
define('NEW_REG_MSG_FOR_PRK', 'Our Sales team will contact you soon', true);
define('NEW_REG_SUB_FOR_ADMIN', 'New Registration', true);
define('NEW_REG_SUB_FOR_AMMIN', 'New Registration details', true);

define('OPPRS_ADMIN_EMAIL', 'admin@opprss.com', true);
define('OPPRS_SALES_EMAIL', 'sales@opprss.com', true);

define('OPPRS_CARE_EMAIL', 'care@opprss.com', true);
define('OPPRS_THANK_YOU_MOBILE', '+91-9073522555', true);
define('THANK_YOU_MSG', "We have send an acknowledge mail for any urgent need and query feel free to reach out our customer care.");

define('LOGO_PDF', OPPR_BASE_URL.'img/logo_pdf.png', true);//".LOGO_pdf."
define('pdf_LOGO', OPPR_BASE_URL.'img/oppr-logo.png', true);//".pdf_LOGO."
define('prk_LOGO_pdf', OPPR_BASE_URL.'img/logo.png');


#-----------------e_admin---------------#
define('PRK_EMP_URL', OPPR_BASE_URL.'p_admin/employee/middle_tire/', true);
define('PRK_EMP_BASE_URL', OPPR_BASE_URL.'p_admin/employee/', true);
define('SUP_EMP_URL', OPPR_BASE_URL.'p_admin/s_admin/middle_tire/', true);
define('SUP_EMP_BASE_URL', OPPR_BASE_URL.'p_admin/s_admin/', true);
define('UNIQUE_STATUS_S', "S", true);//".UNIQUE_STATUS_S."
define('UNIQUE_STATUS_F', "F", true);//".UNIQUE_STATUS_F."
define('UNIQUE_TYPE_CR', "CR", true);//".UNIQUE_TYPE_CR."
define('UNIQUE_TYPE_INV', "INV", true);//".UNIQUE_TYPE_INV."
define('ADVANCE_FLAG_Y', "Y", true);//".ADVANCE_FLAG_Y."
define('ADVANCE_FLAG_N', "N", true);//".ADVANCE_FLAG_N."
define('IN_OUT_FLAG_I', "I", true);//".IN_OUT_FLAG_I."
define('IN_OUT_FLAG_O', "O", true);//".IN_OUT_FLAG_O."
define('HDA_SHORT', "HDA", true);
define('HDA_FULL', "HALDIA DEVELOPMENT AUTHORITY", true);

/*--------------------*/
define('DUPLICATE', "DUPLICATE", true);
define('PRK_NAME', "Parking Solution", true);
define('PRK_GOV', "(A Statutory Authority Under Govt. of West Bengal)", true);
define('SMS_URL', "www.opprss.com", true);

define('MALE_IMG_D', OPPR_BASE_URL.'img/maleimage.png', true);//".MALE_IMG_D."
define('FEMALE_IMG_D', OPPR_BASE_URL.'img/femaleimage.png', true);//".FEMALE_IMG_D."
?>