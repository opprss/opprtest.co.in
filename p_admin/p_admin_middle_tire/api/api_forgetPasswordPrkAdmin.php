<?php
/*
Description: forget password
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function forgetPasswordPrkAdmin($user_name,$email,$password,$repassword){
    global $pdoconn;
    $response = array();
    if(passwordChecker($password,$repassword)=='true'){
        $password=md5($password);
        //date_default_timezone_set('Asia/Kolkata');

        $sql="SELECT `prk_area_admin`.`prk_admin_id` FROM `prk_area_admin`,`prk_area_dtl` where `prk_area_admin`.`prk_admin_id` = `prk_area_dtl`.`prk_admin_id` AND `prk_area_dtl`.`active_flag`='".FLAG_Y."' AND `prk_area_admin`.`prk_admin_name`='$user_name' AND `prk_area_dtl`.`prk_admin_id` IN (SELECT `prk_admin_id` FROM `prk_area_dtl` WHERE `prk_area_email` = '$email')";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $count=$query->rowCount();
        if($count>0){
            //$time=date("Y-m-d h:i:sa");
            $sql ="UPDATE `prk_area_admin`,`prk_area_dtl` SET `prk_area_admin`.`prk_admin_password`='$password', `prk_area_admin`.`updated_by`='$user_name', `prk_area_admin`.`updated_date`='".TIME."' WHERE `prk_area_admin`.`prk_admin_id` = `prk_area_dtl`.`prk_admin_id` AND `prk_area_dtl`.`active_flag`='".FLAG_Y."' AND `prk_area_admin`.`prk_admin_name`='$user_name'AND `prk_area_dtl`.`prk_admin_id` IN (SELECT `prk_admin_id` FROM `prk_area_dtl` WHERE `prk_area_email` = '$email')";
            $query = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Update sucessfull';
                return json_encode($response);
                //return true;
            }else{
                $response['status'] = 0;
                $response['message'] = 'Update Not Save';
                return json_encode($response);
            }
        }else{
            $response['status'] = 0;
            $response['message'] = 'Email Or User Name Wrong';
            return json_encode($response);
        }
    }else{
        $response=passwordChecker($password,$repassword);
        return json_encode($response);
    }
}
?>