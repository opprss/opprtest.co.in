<?php
/*
Description: Parking area account status check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function p_admin_dtl_show($prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    // $sql = "SELECT * FROM `permission` WHERE `form_id` = '$value['form_id']' AND `user_name` = '$prk_admin_name'";
    $sql = "SELECT prk_area_admin.prk_admin_id,
        prk_area_admin.prk_admin_name,
        prk_area_admin.park_ac_status,
        prk_area_admin.account_number,
        prk_area_admin.park_pay,
        prk_area_admin.payment_dtl_img,
        prk_area_admin.park_detail_2,
        prk_area_admin.park_detail_3,
        prk_area_admin.park_detail_4,
        prk_area_admin.prient_in_flag,
        prk_area_admin.prient_out_flag,
        prk_area_admin.advance_flag,
        prk_area_admin.advance_pay,
        prk_area_admin.ad_flag,
        prk_area_admin.sms_flag,
        prk_area_admin.veh_out_otp_flag,
        prk_area_admin.veh_token_flag,
        prk_area_admin.visitor_token_flag,
        prk_area_admin.veh_valid_flag,
        prk_area_admin.parking_type,
        prk_area_admin.visitor_out_verify_flag,
        prk_area_admin.veh_out_verify_flag,
        prk_area_admin.visitor_otp_flag,
        prk_area_admin.visitor_delay_time,
        prk_area_admin.alarm_time,
        prk_area_admin.bb_flag,
        prk_area_admin.visitor_park_flag,
        prk_area_admin.visitor_image_flag,
        prk_area_admin.resident_flag,
        prk_area_admin.vis_prient_in_flag,
        prk_area_admin.vis_prient_out_flag,
        prk_area_admin.office_flag,
        prk_area_admin.printer_name,
        prk_area_dtl.prk_area_name,
        prk_area_dtl.prk_area_short_name,
        prk_area_dtl.prk_area_email,
        prk_area_dtl.prk_area_rep_name,
        prk_area_dtl.prk_area_rep_mobile,
        prk_area_dtl.prk_area_pro_img,
        prk_area_dtl.prk_area_logo_img 
        FROM prk_area_admin,prk_area_dtl
        WHERE prk_area_admin.prk_admin_id='$prk_admin_id'
        AND prk_area_dtl.active_flag='".FLAG_Y."'
        AND prk_area_dtl.prk_admin_id=prk_area_admin.prk_admin_id";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Sucessfull';
        // $response['prk_admin_id'] = $val['prk_admin_id'];
        $response['prk_admin_name'] = $val['prk_admin_name'];
        $response['park_ac_status'] = $val['park_ac_status'];
        $response['account_number'] = $val['account_number'];
        // $response['park_pay'] = $val['park_pay'];
        // $response['payment_dtl_img'] = $val['payment_dtl_img'];
        // $response['park_detail_2'] = $val['park_detail_2'];
        // $response['park_detail_3'] = $val['park_detail_3'];
        // $response['park_detail_4'] = $val['park_detail_4'];
        $response['prient_in_flag'] = $val['prient_in_flag'];
        $response['prient_out_flag'] = $val['prient_out_flag'];
        $response['advance_flag'] = $val['advance_flag'];
        $response['advance_pay'] = $val['advance_pay'];
        $response['ad_flag'] = $val['ad_flag'];
        $response['sms_flag'] = $val['sms_flag'];
        $response['veh_out_otp_flag'] = $val['veh_out_otp_flag'];
        $response['veh_token_flag'] = $val['veh_token_flag'];
        $response['visitor_token_flag'] = $val['visitor_token_flag'];
        $response['veh_valid_flag'] = $val['veh_valid_flag'];
        $response['parking_type'] = $val['parking_type'];
        $response['visitor_out_verify_flag'] = $val['visitor_out_verify_flag'];
        $response['veh_out_verify_flag'] = $val['veh_out_verify_flag'];
        $response['prk_area_name'] = $val['prk_area_name'];
        // $response['prk_area_short_name'] = $val['prk_area_short_name'];
        $response['prk_area_email'] = $val['prk_area_email'];
        $response['prk_area_rep_name'] = $val['prk_area_rep_name'];
        $response['prk_area_rep_mobile'] = $val['prk_area_rep_mobile'];
        // $response['prk_area_pro_img'] = $val['prk_area_pro_img'];
        // $response['prk_area_logo_img'] = $val['prk_area_logo_img'];
        $response['visitor_otp_flag'] = $val['visitor_otp_flag'];
        $response['visitor_delay_time'] = $val['visitor_delay_time'];
        $response['alarm_time'] = $val['alarm_time'];
        $response['bb_flag'] = $val['bb_flag'];
        $response['visitor_park_flag'] = $val['visitor_park_flag'];
        $response['visitor_image_flag'] = $val['visitor_image_flag'];
        $response['resident_flag'] = $val['resident_flag'];
        $response['vis_prient_in_flag'] = $val['vis_prient_in_flag'];
        $response['vis_prient_out_flag'] = $val['vis_prient_out_flag'];
        $response['office_flag'] = $val['office_flag'];
        $response['printer_name'] = $val['printer_name'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>