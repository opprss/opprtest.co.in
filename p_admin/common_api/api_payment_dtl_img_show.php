<?php 
/*
Description: parkin area wallet QR code img show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/  
function payment_dtl_img_show($prk_admin_id){
    $response = array();
    global $pdoconn;

    $sql = "SELECT `payment_dtl_img` FROM `prk_area_admin` WHERE `prk_admin_id`='$prk_admin_id' AND `park_ac_status`='A'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $payment_dtl_img = $val['payment_dtl_img'];
    if(!empty($payment_dtl_img)){
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['payment_dtl_img'] = $val['payment_dtl_img'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'Image Empty';
    }
    return json_encode($response);
}
?>