<?php 
/*
Description: user vehicle out QR code scan and notify show parking employ .
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function prk_area_advance_status($prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();

    $sql="SELECT prk_admin_id, prk_admin_name, advance_flag, advance_pay ,prient_in_flag , prient_out_flag FROM prk_area_admin where prk_admin_id = '$prk_admin_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
    	$val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        //$response['prient_out_flag'] = $val['prient_out_flag'];
        $response['prk_admin_name'] = $val['prk_admin_name'];
        $response['advance_flag'] = $val['advance_flag'];
        $response['advance_pay'] = $val['advance_pay'];
        $response['print_in_flag'] = $val['prient_in_flag'];
        $response['print_out_flag'] = $val['prient_out_flag'];
	}else{
		$response['status'] = 0;
	    $response['message'] = 'List Empty';
	}
    return json_encode($response); 
}
?>