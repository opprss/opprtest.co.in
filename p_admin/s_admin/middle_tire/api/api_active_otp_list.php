<?php
/*
Description: Parking area account status check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function active_otp_list(){
    global $pdoconn;
    $allplayerdata = array();
    $sql="SELECT `otp_id`,
        `otp`,
        `admin_id`,
        `admin_email`,
        `mobile_no`,
        DATE_FORMAT(`otp_start_time`,'%d-%b-%Y') AS `send_date`,
        DATE_FORMAT(`otp_start_time`,'%I:%i %p') AS `send_time`
        FROM `otp_details` 
        WHERE `active_flag`='Y' 
        ORDER BY `otp_id` DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $resp['otp'] = $val['otp'];
            $resp['name'] = $val['admin_id'];
            $resp['email'] = $val['admin_email'];
            $resp['mobile'] = $val['mobile_no'];
            $resp['send_date'] = $val['send_date'];
            $resp['send_time'] = $val['send_time'];
            array_push($allplayerdata, $resp);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['otp_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>