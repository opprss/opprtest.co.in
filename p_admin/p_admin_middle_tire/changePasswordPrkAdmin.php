<?php 
/*
Description: Parking adimn password change.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// include_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('user_name','password','repassword','prk_admin_id','token'))){
 	if(isEmpty(array('user_name','password','repassword','prk_admin_id','token'))){

	 	$user_name = trim($_POST['user_name']);
	 	$password = trim($_POST['password']);
        $repassword = trim($_POST['repassword']);
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $token = trim($_POST['token']);
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            $response= changePasswordPrkAdmin($user_name,$password,$repassword,$prk_admin_id);
            echo ($response);
        }else{
            $response = $resp;
            echo $response;
        }
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response);
 }
?>