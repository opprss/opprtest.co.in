<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <!-- <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels"> -->


    <title>Oppr Software solution</title>

    <!-- vendor css -->
    <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="../lib/jquery-toggles/toggles-full.css" rel="stylesheet">
    <link href="../lib/rickshaw/rickshaw.min.css" rel="stylesheet">

    <!-- Amanda CSS -->
    <link rel="stylesheet" href="../css/amanda.css">
    <link rel="stylesheet" href="../css/style.css">
  </head>

  <body>

    <div class="am-header"  id="header_1">
      <div class="am-header-left">
        <a id="naviconLeft" href="" class="am-navicon d-none d-lg-flex"><i class="icon ion-navicon-round"></i></a>
        <a id="naviconLeftMobile" href="" class="am-navicon d-lg-none"><i class="icon ion-navicon-round"></i></a>
        <a href="index.html" class="am-logo">
          <img src="../img/logo.png">
        </a>
      </div><!-- am-header-left -->

      <div class="am-header-right">
        <!-- dropdown -->
        <div class="dropdown dropdown-profile">
          <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
            <img src="../img/img3.jpg" class="wd-32 rounded-circle" alt="">
            <span class="logged-name"><span class="hidden-xs-down">Welcome Guest</span> <!-- <i class="fa fa-angle-down mg-l-3"></i> --></span>
          </a>
        </div><!-- dropdown -->
      </div><!-- am-header-right -->
    </div><!-- am-header -->

    <div class="am-sideleft">
      <div class="nav am-sideleft-tab" id="nav">
      </div>

      <div class="tab-content" style="" id="sidemenu">
        <div id="mainMenu" class="tab-pane active">
          <ul class="nav am-sideleft-menu">
            <li class="nav-item">
              <a href="index.php" class="nav-link active">
                <i class="icon ion-ios-home-outline"></i>
                <span>Home Page</span>
              </a>
            </li><!-- nav-item -->
            <span style="display: none;">
            <li class="nav-item">
              <!-- <a href="" class="nav-link with-sub"> -->
                <a href="" class="nav-link">
                <i class="icon ion-ios-personadd-outline"></i>
                <span>Add Vehical</span>
              </a>
            </li><!-- nav-item -->
            <li class="nav-item">
              <!-- <a href="" class="nav-link with-sub"> -->
                <a href="" class="nav-link">
                <i class="icon ion-ios-medkit-outline"></i>
                <span>Add Licence</span>
              </a>
            </li><!-- nav-item -->
            <li class="nav-item">
              <!-- <a href="" class="nav-link with-sub"> -->
                <a href="" class="nav-link">
                <i class="icon ion-ios-photos-outline"></i>
                <span>Add Token</span>
              </a>
            </li><!-- nav-item -->
            <li class="nav-item">
              <!-- <a href="" class="nav-link with-sub"> -->
                <a href="" class="nav-link">
                <i class="icon ion-ios-calculator-outline"></i>
                <span>Add Payment</span>
              </a>
            </li><!-- nav-item -->
            <li class="nav-item">
              <!-- <a href="" class="nav-link with-sub"> -->
                <a href="" class="nav-link">
                <i class="icon ion-ios-paper-outline"></i>
                <span>My Transtion</span>
              </a>
            </li><!-- nav-item -->
            <li class="nav-item">
              <a href="widgets.html" class="nav-link">
                <i class="icon ion-ios-briefcase-outline"></i>
                <span>Feed Back</span>
              </a>
            </li><!-- nav-item -->
            </span>
            <li class="nav-item">
              <a href="" class="nav-link with-sub">
                
                <i class="icon ion-ios-bookmarks-outline"></i>
                <span>Customer Sevices</span>
              </a>
              <ul class="nav-sub">
                <li class="nav-item"><a href="blank.html" class="nav-link">Contectus</a></li>
                <li class="nav-item"><a href="page-signin.html" class="nav-link">About</a></li>
                <li class="nav-item"><a href="page-signup.html" class="nav-link">FAQ</a></li>
                <li class="nav-item"><a href="page-notfound.html" class="nav-link">Legal</a></li>
              </ul>
            </li><!-- nav-item -->
          </ul>
        </div><!-- #mainMenu -->
      </div><!-- tab-content -->
    </div><!-- am-sideleft -->

