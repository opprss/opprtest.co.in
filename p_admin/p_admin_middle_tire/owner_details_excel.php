<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
 $response = array();
 $allplayerdata = array();
 if(isAvailable(array('prk_admin_id','owner_name','owner_mobile','owner_email','owner_gender','parking_admin_name','effective_date','end_date','owner_id','token','crud_type'))){
    if(isEmpty(array('prk_admin_id','parking_admin_name','token','crud_type'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $tower_id = '';
        $flat_id = '';
        // $owner_name = trim($_POST['owner_name']);
        // $owner_mobile = trim($_POST['owner_mobile']);
        // $owner_email = trim($_POST['owner_email']);
        // $owner_gender = trim($_POST['owner_gender']);
        $parking_admin_name = trim($_POST['parking_admin_name']);
        $effective_date = trim($_POST['effective_date']);
        $end_date = trim($_POST['end_date']);
        $owner_id = trim($_POST['owner_id']);
        $crud_type = trim($_POST['crud_type']);
        $token = trim($_POST['token']);
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            if($_FILES['file']['name']){
                $PNG_TEMP_DIR ='./../uploades/'.$parking_admin_name.'/excel/';
                if (!file_exists($PNG_TEMP_DIR)) {
                    $old_mask = umask(0);
                    mkdir($PNG_TEMP_DIR, 0777, TRUE);
                    umask($old_mask);
                }
                $file_post = $PNG_TEMP_DIR.''.time()."_".$_FILES['file']['name'];
                @copy($_FILES['file']['tmp_name'],$file_post);
                if($handle = fopen($file_post, "r")){
                    $indexing = 1;     
                    while (($Row = fgetcsv($handle, 10000, ",")) !== FALSE) {
                        if($indexing == 1){
                        $indexing++;
                        continue;
                        }
                        $owner_name = isset($Row[0]) ? strtoupper(trim($Row[0])) : '';
                        $owner_mobile = isset($Row[1]) ? trim($Row[1]) : '';
                        $owner_email = isset($Row[2]) ? strtolower(trim($Row[2])) : '';
                        $owner_gender = isset($Row[3]) ? strtoupper(trim($Row[3])) : '';
                        // array_push($allplayerdata, $owner_name);
                        owner_details($prk_admin_id,$tower_id,$flat_id,$owner_name,$owner_mobile,$owner_email,$owner_gender,$owner_id,$effective_date,$end_date,$parking_admin_name,$crud_type);
                    }
                }
                $response['status'] = 1;
                $response['message'] = 'Successfully';
                // $response['file'] = $file_post;
                // $response['flat_name_list'] = $owner_name;
                $response = json_encode($response);
            }else{
                $response['status'] = 0;
                $response['message'] = 'File Missing';
                $response = json_encode($response);
            }
        }else{
            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>