<?php
/*
Description: user address edit.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_edit_address($user_add_id,$user_admin_id,$user_address,$landmark,$user_add_area,$user_add_city,$user_add_state,$user_add_country,$user_add_pin,$mobile,$prk_admin_id,$tower_name,$falt_name){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $sql = "UPDATE `user_address` SET `user_address`='$user_address',`landmark`='$landmark',`user_add_area`='$user_add_area',`user_add_city`='$user_add_city',`user_add_state`='$user_add_state',`user_add_country`='$user_add_country',`user_add_pin`='$user_add_pin',`user_admin_id`='$user_admin_id',`prk_admin_id`='$prk_admin_id',`tower_name`='$tower_name',`falt_name`='$falt_name',`updated_by`='$mobile',`updated_date`='".TIME."' WHERE `user_admin_id`='$user_admin_id' AND `user_add_id`='$user_add_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";


    /*$sql = "SELECT `default_flag`,`user_add_verify_flag` FROM `user_address` WHERE `user_add_id`='$user_add_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $default_flag = $val['default_flag'];
    $user_add_verify_flag = $val['user_add_verify_flag'];
    //----------------------------------
    $sql ="UPDATE `user_address` SET `active_flag`='".FLAG_N."', `updated_date`='".TIME."', `updated_by`='$mobile' WHERE `user_admin_id`='$user_admin_id' AND `user_add_id`='$user_add_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
        $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $sql = "INSERT INTO `user_address`(`user_admin_id`,`user_address`,`landmark`,`user_add_area`,`user_add_city`,`user_add_state`,`user_add_country`,`user_add_pin`,`inserted_by`,`inserted_date`,`prk_admin_id`,`tower_name`,`falt_name`,`default_flag`,`user_add_verify_flag`) VALUE ('$user_admin_id','$user_address','$landmark','$user_add_area','$user_add_city','$user_add_state','$user_add_country','$user_add_pin','$mobile','".TIME."','$prk_admin_id','$tower_name','$falt_name','$default_flag','$user_add_verify_flag')";*/
        $query = $pdoconn->prepare($sql);
        if($query->execute()){
            $response['status'] = 1;
            $response['message'] = 'Update Successful';
            $response = json_encode($response);
        }else{
            $response['status'] = 0;
            $response['message'] = 'Update Not Successful';
            $response = json_encode($response);
        }
    /*}else{ 
        $response['status'] = 0;
        $response['message'] = 'Update Not Successful';
        $response = json_encode($response);
    }*/
    return $response;
}
?>