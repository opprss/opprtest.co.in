<!-- ################################################
  
Description: user can see and delete all the gate pass issued for him/her
Developed by: Soemen Banerjee
Created Date: 17-03-2018
updated Date: 18-05-2018

 ####################################################-->
<?php include "all_nav/header.php"; ?>
    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Digital Gate Pass List</h5>
        <!-- <form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card single pd-20 pd-sm-40 col-md-12">
          <!-- <h6 class="card-body-title">Sign Up Here</h6>
          <p class="mg-b-20 mg-sm-b-30">A form with a label on top of each form control.</p> -->
          
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          
            <div class="row">
              <!-- <dir class="col-md-1">
                
              </dir> -->
              <div class="col-md-12">
                  <div class="list-group widget-list-group">

                    <!-- PHP CODE -->
                    <?php
                      $user_id = $_SESSION['user_admin_id'];
                      
                      if(isset($user_id) ){
                        $response = array();
                        $response = user_gete_pass_dtl_list($user_id);
                        $response = json_decode($response, true);
                        //print_r($response);
                        if($response['status']){
                          $count = count($response['gate_pass']);
                          for ($i=0; $i < $count ; $i++) { 
                            $prk_gate_pass_id= $response['gate_pass'][$i]['prk_gate_pass_id'];
                            $prk_gate_pass_num= $response['gate_pass'][$i]['prk_gate_pass_num'];
                            $veh_number= $response['gate_pass'][$i]['veh_number'];
                            $effective_date = $response['gate_pass'][$i]['effective_date'];
                            $end_date= $response['gate_pass'][$i]['end_date'];
                            $vehicle_typ_img = $response['gate_pass'][$i]['vehicle_typ_img'];
                        ?>
                        <div class="list-group-item rounded-top-0">
                          <div class="media pd--b-20">
                            
                            <div class="d-flex mg-l-0 mg-r-20 wd-40">
                              <!--<i class="fa fa-id-card  tx-info tx-40 tx"></i>-->
                              <img src='<?php echo OPPR_BASE_URL.$vehicle_typ_img; ?>' style='width: 50px; height: 50px;'>
                            </div><!-- d-flex -->
                            <div class="media-body">
                              <div class="row">
                                <div class="col-lg-12 tx-18 pd-b-10">
                                    <?php echo $veh_number; ?>
                                </div>
                                <div class="col-lg-4">
                                   <p class="mg-b-0 tx-12">
                                    Gate Pass :
                                    <?php
                                      if (!empty($prk_gate_pass_num))
                                         echo $prk_gate_pass_num;
                                       else
                                        echo "N/A";
                                    ?>
                                    <br>Issue Date :
                                    <?php
                                      if (!empty($effective_date))
                                         echo $effective_date;
                                       else
                                        echo "N/A";
                                     ?>
                                     <br>Expiry Date : 
                                    <?php
                                      if (!empty($end_date))
                                         echo $end_date;
                                       else
                                        echo "N/A";
                                     ?>
                                   </p>
                                </div>
                              </div>                              
                            </div><!-- media-body -->
                            <div class="d-flex">
                              
                              <button type="button" class="clean-button" name="delete" id="delete<%=count++%>" value="<?php echo $prk_gate_pass_id; ?>" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
                            </div>
                           
                          </div>
                        </div><!-- list-group-item -->
                    <?php
                        }
                      }else{
                    ?>
                    <h5 class="text-center">No gate pass issued for you</h5>
                    <?php
                      }
                    }else{
                         $response['status'] = 0; 
                         $response['message'] = 'Invalid API Call';
                         echo json_encode($response);
                         //header('location: signin.php');
                      }
                    ?>
                  </div>
              </div>
              
            </div>
          </div>
        </div><!-- card -->
      </div><!-- am-pagebody -->
      

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">

  $(document).ready(function(){
    var userUrl = "<?php echo USER_URL; ?>";
    /*delete popup*/ 
    $('button[id^="delete"]').on('click', function() {
      var user_admin_id = "<?php echo $_SESSION['user_admin_id'];?>";
      var prk_gate_pass_id = this.value;
      $.confirm({
        title: 'Are You Sure?',
        content: "<p style='font-size:0.8em;'>It will delete the item permanently</p>",
        theme: 'modern',
        type: 'red',
        buttons: {
          cancel: function () {
          },
          somethingElse: {
            text: 'Delete',
            btnClass: 'btn-red',
            keys: ['Y', 'shift'],
            action: function(){
              if (prk_gate_pass_id != '') {
                //alert(prk_rate_id);
                var urlDtl = userUrl+'user_gate_pass_dtl_delete.php';
                //alert(urlDtl);
                $.ajax({
                  url :urlDtl,
                  type:'POST',
                  data :
                  {
                    'prk_gate_pass_id':prk_gate_pass_id,
                    'user_admin_id':user_admin_id,
                    'mobile':"<?php echo $_SESSION['mobile'] ;?>",
                    'token': '<?php echo $token;?>'
                  },
                  dataType:'html',
                  success  :function(data)
                  {
                    //alert(data);
                    var json = $.parseJSON(data);
                    if (json.status){
                      location.reload(true);
                    }else{
                      if( typeof json.session !== 'undefined'){
                        if (!json.session) {
                          window.location.replace("logout.php");
                        }
                      }else{
                        $.alert({
                        icon: 'fa fa-frown-o',
                        theme: 'modern',
                        title: 'Error !',
                        content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                        type: 'red'
                      });
                      }
                    }
                  }
                });
              }else{
                $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
                type: 'red'
                });
              }
            }
          }
        }
      });
    });
   /*/delete popup*/
    $('[data-toggle="tooltip"]').tooltip();   
  });

// add mouseover link on delete button
    let d=document.getElementsByClassName('clean-button');
for(let i=0;i<d.length;i++){
d[i].addEventListener("mouseover",function(){
                    d[i].style.cursor="pointer";
                    }
                );
}    
// end
    </script>
 