<!-- ################################################
  
Description: Parking area admin can add/delete parking rate by this web page.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
 <?php 
include "all_nav/header.php"; 
@session_start();
if (isset($_SESSION['prk_admin_id']) && isset($_SESSION['prk_user_username']) && isset($_SESSION['prk_area_user_id']) && isset($_SESSION['token']) && isset($_SESSION['prk_area_gate_type']) && isset($_SESSION['prk_area_gate_id'])) {
  //$insert_by=$_SESSION['parking_admin_name'];
  $prk_admin_id = $_SESSION['prk_admin_id'];
  $prk_user_username = $_SESSION["prk_user_username"];
  $prk_area_user_id = $_SESSION["prk_area_user_id"];
  $token = $_SESSION["token"];
  $prk_area_gate_type = $_SESSION["prk_area_gate_type"]; 
  $prk_area_gate_id = $_SESSION["prk_area_gate_id"];
  $today = date("d-m-Y");
  $prk_inf = prk_inf($prk_admin_id);
  $prk_inf = json_decode($prk_inf, true);
  $prk_area_name = $prk_inf['prk_area_name'];
  //Vehicle{"status":1,"message":"Successful","prk_admin_name":"GARAIA_5684","advance_flag":"N","advance_pay":"0","prient_in_flag":"N","prient_out_flag":"N"}

}
?>
<style>
  .size{
    font-size: 11px;
  }
.success{
  font-size: 11px;
  color: green;
}
</style>
<!-- vendor css -->
   
    <link href="../lib/highlightjs/github.css" rel="stylesheet">
    <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">

    <div class="am-mainpanel">
      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">History Transaction</h5>
        <!--<form id="searchBar" class="search-bar" action="">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>-->
      </div>

      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40" >
          <!-- <h6 class="card-body-title">Add Employee</h6> -->
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form id="downloadForm" method="post" action="<?php echo PRK_EMP_URL;?>prk_user_payment_type_trn_web_pdf">
            <div class="row mg-b-25">
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">By Date</label>
                  <input type="text" class="form-control s_date prevent_readonly" placeholder="start date" name="start_date" id="start_date" readonly="readonly">
                  <span id="start_date_error" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">&nbsp;</label>
                  <input type="text" class="form-control e_date prevent_readonly" placeholder="end date" name="end_date" id="end_date" readonly="readonly">
                  <span id="end_date_error" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-2">

   <!--  $prk_admin_id= $_POST['prk_admin_id'];
    $payment_rec_emp_name= $_POST['payment_rec_emp_name'];
    $payment_type= $_POST['payment_type'];
    $insert_by=$_POST['insert_by'];
    $start_date=$_POST['start_date'];
    $end_date=$_POST['end_date']; -->
                <div class="form-group">
                  <label class="form-control-label size">Payment Type</label>
                  <div class="select" style="">
                    <select name="payment_type" id="payment_type" style="opacity: 0.8">
                      <option value="all">ALL</option>
                      <option value="<?php echo CASH ?>">Cash</option>
                      <option value="<?php echo WALLET ?>">Wallet</option>
                      <option value="<?php echo DGP ?>"><?php echo DGP_F ?></option>
                      <option value="<?php echo TDGP ?>"><?php echo TDGP_F ?></option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-layout-footer mg-t-30">
                  <div class="row">
                    <div class="col-lg-12">
                      <button type="button" class="btn btn-block prk_button" name="ok" id="ok" style="padding-bottom: 9px; padding-top: 9px">OK</button>
                      <div style="position: absolute;" id="success_submit" class="success"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-layout-footer mg-t-30">
                  <div class="row">
                    <div class="col-lg-12">
                      <input type="hidden" name="prk_admin_id" value="<?php echo $prk_admin_id; ?>">
                      <input type="hidden" name="prk_area_name" value="<?php echo $prk_area_name; ?>">
                      <input type="hidden" name="payment_rec_emp_name" value="<?php echo $prk_user_username ?>">
                      <input type="hidden" name="insert_by" value="<?php echo $prk_area_name; ?>">
                      <button type="submit" class="btn btn-block prk_button" name="download" id="download" style="padding-bottom: 9px; padding-top: 9px" disabled="disabled">Download</button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">Filter By</label>

                  <div class="select" style="">
                    <select name="pay_type" id="pay_type" style="opacity: 0.8">
                      <option value="all">ALL</option>
                      <option value="<?php echo CASH ?>"><?php echo CASH ?></option>
                      <option value="<?php echo WALLET ?>"><?php echo WALLET ?></option>
                      <option value="<?php echo DGP ?>"><?php echo DGP_F ?></option>
                      <option value="<?php echo TDGP ?>"><?php echo TDGP_F ?></option>
                    </select>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        </div>
      </div>   

      <div class="am-pagebody">

        <div class="card pd-20 pd-sm-40">
          <div class="col-md-12">                  
              <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap table-responsive table-bordered">
              <thead>
                <tr>
                  <th class="wd-5p" style="background-color: #00b8d4;">Type</th>
                  <th class="wd-15p" style="background-color: #00b8d4;">Vehicle Number</th>
                  <th class="wd-20p" style="background-color: #00b8d4;">Payment Receive Date & Time</th>
                  <th class="wd-20p" style="background-color: #00b8d4;">Payment type</th>
                  <th class="wd-15p" style="background-color: #00b8d4;">Payment Amount</th>
                </tr>
              </thead>
              <tbody id="list">
              	
              </tbody>
            </table>
          </div><!-- table-wrapper -->
          </div>
        </div><!-- card -->
<!-- footer part -->
<?php include"all_nav/footer.php"; ?>
<style type="text/css">
  .error_size{
  font-size:11px;
  color: red;

}
</style>
<script type="text/javascript">

 
  /*for Tool Tip*/
  $(document).ready(function(){

    var pkrEmpUrl = "<?php echo PRK_EMP_URL; ?>";
    var prkUrl = "<?php echo PRK_EMP_URL; ?>";
    var imagUrl = "<?php echo OPPR_BASE_URL?>";
     var prk_admin_id = "<?php echo $_SESSION['prk_admin_id'] ?>";
     var payment_rec_emp_name = "<?php echo $prk_user_username; ?>";
     //alert(payment_rec_emp_name);
     //alert(prk_admin_id );
     //var prk_admin_id = "100";
     /*var download = 0;
     var payment_type = 'CASH';*/
      //var month = $('#month :selected').val();
      //alert(month);
      var start_date = '';
      var end_date = '';
      // var payment_type ='CASH';
      var payment_type = 'all';
      // alert(payment_type);
      var download = 0;
      callAjax(prk_admin_id,payment_rec_emp_name,start_date,end_date,payment_type,download);

      $('#ok').click(function(){
        var payment_type = $('#payment_type :selected').val();
        // alert(payment_type);
        start_date = $('#start_date').val();
        end_date = $('#end_date').val();
        if (start_date != '' && end_date != '' ) {
          var download = 1;
          callAjax(prk_admin_id,payment_rec_emp_name,start_date,end_date,payment_type,download);
        }else{
          if (end_date == '') {
            $('#end_date_error').text('End date is required');
          }
          if (start_date == '') {
            $('#start_date_error').text('Start date is required');
          }
        }
      });
      
  	$("#start_date").blur(function () {
	    var start_date = $('#start_date').val();
	    if(start_date==''){
	    $('#start_date_error').text('Start date is required');
	    }
	    else{
	      $('#start_date_error').text('');;
	    }
  	});
	$("#end_date").blur(function () {
	    var end_date = $('#end_date').val();
	    if(end_date==''){
	    $('#end_date_error').text('End date is required');
	    }
	    else{
	      $('#end_date_error').text('');;
	    }
	});


    $('#pay_type').change(function(){
        var payment_type = $('#pay_type :selected').val();
        // alert(payment_type);
        if (start_date != '' && end_date != '' ) {
          var download = 1;
        }else{
          var download = 0;
        }
        callAjax(prk_admin_id,payment_rec_emp_name,start_date,end_date,payment_type,download);
    });

    $('#download').click(function(){
        $("#downloadForm").submit(function(){
            //alert("Submitted");
            
        });
        
        //alert("hello");
    });


function callAjax(prk_admin_id,payment_rec_emp_name,start_date,end_date,payment_type,download){
        var urluserPaymentHistory = prkUrl+'prk_user_payment_type_trn.php';
        // alert(urluserPaymentHistory);
        //alert(urluserPaymentHistory);
        //prk_user_payment_type_trn($prk_admin_id,$payment_rec_emp_name,$payment_type,$today,$today);
        /*$.ajax({
            url :urluserPaymentHistory,
            type:'POST',
            beforeSend: function(){
              $('.ajax-loader').css("visibility", "visible");
            },
            data :
            {
              'prk_admin_id':prk_admin_id,
              'payment_rec_emp_name':payment_rec_emp_name,
              'payment_type':payment_type,
              'start_date':start_date,
              'end_date':end_date
            },
            dataType:'html',
            success  :function(data)
            {
              alert(data);
              	var demoLines = '';
              	if (data.status) {
              		if (data.payment_history.length === 0) {
	                  $('#download').prop('disabled', true);
	                  demoLines +='<tr class="text-center">\
			                  <td style="font-weight: bold; font-size: 1.2em;" colspan="6">List is empty</td>\
			                </tr>';
	                }else{
	                	if (download) {
	                   		$('#download').prop('disabled', false);
	                    }
	                	for (var key in data.payment_history) {
			                //alert(key);
			                demoLines += '<tr class="text-center">\
				                  <td style="font-weight: bold; font-size: 1.2em; background-color: #C0C0C0;" colspan="6">'+key+'</td>\
				                </tr>';
			                $.each(data.payment_history[key], function(key, value) {
			                	//alert(key);
			                	//alert(value['veh_number']);
							    //alert(data.payment_history[key]['veh_number']);
							    //<img src="'+imagUrl+this["vehicle_typ_img"]+'" style="width: 60px; height: 60px; border-radius:20px;">
							    demoLines += '<tr>\
		                          <td><img src="'+imagUrl+value['vehicle_typ_img']+'" style="width: 60px; height: 60px;"></td>\
		                          <td>'+value['veh_number']+'</td>\
		                          <td>'+value['payment_rec_date']+'</td>\
		                          <td>'+value['payment_type']+'</td>\
		                          <td>'+value['payment_amount']+'</td>\
		                        </tr>';
							});
							demoLines += '<tr style="text-align: right;">\
			                  <td colspan="4" style="background-color: #00b8d4; color: #FFF;">\
			                    <b>Total Vehicle - '+data.total_veh+'</b>\
			                  </td>\
			                  <td  style="background-color: #00b8d4; color: #FFF;">\
			                    <b>Total Amount - Rs. '+data.total_pay+'</b>\
			                  </td>\
			                </tr>';
		            	} 
	                }
              	}else{
              		demoLines +='<tr class="text-center">\
			                  <td style="font-weight: bold; font-size: 1.2em;" colspan="6">No Data Found</td>\
			                </tr>';
              	}       
              //alert(demoLines);
              $("#list").html(demoLines);
            },
            complete: function(){
              $('.ajax-loader').css("visibility", "hidden");
            }
          });*/
        $.ajax({
            url :urluserPaymentHistory,
            type:'POST',
            beforeSend: function(){
              $('.ajax-loader').css("visibility", "visible");
            },
            data :
            {
              'prk_admin_id':prk_admin_id,
              'payment_rec_emp_name':payment_rec_emp_name,
              'payment_type':payment_type,
              'start_date':start_date,
              'end_date':end_date
            },
            dataType:'json',
            success  :function(data)
            {
              // alert(data);
                var demoLines = '';
                if (data.status) {
                  if (data.payment_history.length === 0) {
                    $('#download').prop('disabled', true);
                    demoLines +='<tr class="text-center">\
                        <td style="font-weight: bold; font-size: 1.2em;" colspan="6">List is empty</td>\
                      </tr>';
                  }else{
                    if (download) {
                        $('#download').prop('disabled', false);
                      }
                    for (var key in data.payment_history) {
                      //alert(key);
                      var keye =key;
                      if ( keye == "<?php echo TDGP; ?>") {
                          keye ="<?php echo TDGP_F; ?>";
                        }
                        if ( keye == "<?php echo DGP; ?>") {
                          keye ="<?php echo DGP_F; ?>";
                        }
                      demoLines += '<tr class="text-center">\
                          <td style="font-weight: bold; font-size: 1.2em; background-color: #C0C0C0;" colspan="6">'+keye+'</td>\
                        </tr>';
                      $.each(data.payment_history[key], function(key, value) {
                        //alert(key);
                        //alert(value['veh_number']);
                        var payment_ty = value['payment_type'];
                        if ( payment_ty == "<?php echo TDGP; ?>") {
                          payment_ty ="<?php echo TDGP_F; ?>";
                        }
                        if ( payment_ty == "<?php echo DGP; ?>") {
                          payment_ty ="<?php echo DGP_F; ?>";
                        }
                  //alert(data.payment_history[key]['veh_number']);
                  //<img src="'+imagUrl+this["vehicle_typ_img"]+'" style="width: 60px; height: 60px; border-radius:20px;">
                  demoLines += '<tr>\
                              <td><img src="'+imagUrl+value['vehicle_typ_img']+'" style="width: 60px; height: 60px;"></td>\
                              <td>'+value['veh_number']+'</td>\
                              <td>'+value['payment_rec_date']+'</td>\
                              <td>'+payment_ty+'</td>\
                              <td>'+value['payment_amount']+'</td>\
                            </tr>';
              });
              
                  } 
                  demoLines += '<tr style="text-align: right;">\
                        <td colspan="4" style="background-color: #00b8d4; color: #FFF;">\
                          <b>Total Vehicle - '+data.total_veh+'</b>\
                        </td>\
                        <td  style="background-color: #00b8d4; color: #FFF;">\
                          <b>Total Amount - Rs. '+data.total_pay+'</b>\
                        </td>\
                      </tr>';
                  }
                }else{
                  demoLines +='<tr class="text-center">\
                        <td style="font-weight: bold; font-size: 1.2em;" colspan="6">No Data Found</td>\
                      </tr>';
                }       
              //alert(demoLines);
              $("#list").html(demoLines);
            },
            complete: function(){
              $('.ajax-loader').css("visibility", "hidden");
            }
          });
    }

    $('[data-toggle="tooltip"]').tooltip();

  });
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#start_date', {
      default_date   : false,
          /*format  : 'Y-m-d',*/
          /*position       : 'top',*/
          hide_on_select : true,
      render : function (date) {
            if (date < now) {
                return {
                  disabled : false,
                  class_name : 'date-in-past'
                };
            }
            $('#start_date_error').text('');
            return {};
        } 
    });
  });

    $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#start_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
            if (date < now_end) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            $('#error_end_date').text('');

            return {};
        } 
    });
  });

  $('#start_date').focus(function(){
    $('#end_date').val('');
  });
  /*$('#end_date').click(function(){
    if ($('#start_date').val('') == '') {
      
    }
  });*/
  
  
  
  
</script>