<?php
/*
Description: Parking visitor gate pass issue.
Developed by: Rakhal Raj Mandal
Created Date: 31-10-2018
Update date :31-10-2018
*/ 
    require_once 'api/parkAreaReg_api.php';
    $response = array();
 
    if(isAvailable(array('visitor_gate_pass_dtl_id','prk_admin_id','visitor_mobile','visitor_name','visitor_gender','visitor_add1','visitor_add2','visitor_state','visitor_city','visitor_landmark','visitor_country','visitor_pin_cod','id_name','id_number','visitor_gate_pass_num','effective_date','end_date','parking_admin_name','visitor_img','vis_id_prooof_img'))){

        if(isEmpty(array('visitor_gate_pass_dtl_id','prk_admin_id','visitor_mobile','visitor_name','visitor_gender','visitor_add1','visitor_state','visitor_city','visitor_landmark','visitor_country','visitor_pin_cod','id_name','id_number','visitor_gate_pass_num','effective_date','end_date','parking_admin_name'))){
            $prk_admin_id = trim($_POST['prk_admin_id']);
            $visitor_mobile = trim($_POST['visitor_mobile']);
            $visitor_name = trim($_POST['visitor_name']);
            $visitor_gender = trim($_POST['visitor_gender']);
            $visitor_add1 = trim($_POST['visitor_add1']);
            $visitor_add2 = trim($_POST['visitor_add2']);
            $visitor_state = trim($_POST['visitor_state']);
            $visitor_city = trim($_POST['visitor_city']);
            $visitor_landmark = trim($_POST['visitor_landmark']);
            $visitor_country = trim($_POST['visitor_country']);
            $visitor_pin_cod = trim($_POST['visitor_pin_cod']);
            $id_name = trim($_POST['id_name']);
            $id_number = trim($_POST['id_number']);
            $visitor_gate_pass_num = trim($_POST['visitor_gate_pass_num']);
            $effective_date = $_POST['effective_date'];
            $end_date = $_POST['end_date'];
            $user_name = trim($_POST['parking_admin_name']);
            $visitor_gate_pass_dtl_id = trim($_POST['visitor_gate_pass_dtl_id']);
            $visitor_img = trim($_POST['visitor_img']);
            $vis_id_prooof_img = trim($_POST['vis_id_prooof_img']);

            $number = count($_POST["tower_name"]); 

             $sql1 = "UPDATE `visitor_gate_pass_dtl` SET `updated_by`= '$user_name',`updated_date`= '".TIME."',`end_date`= '$end_date' WHERE `visitor_gate_pass_dtl_id`='$visitor_gate_pass_dtl_id'";
            $query = $pdoconn->prepare($sql1);
            $query->execute();
            $sql1 = "UPDATE `visitor_service_dtl` SET `updated_by`= '$user_name',`updated_date`= '".TIME."',`active_flag`= '".FLAG_N."' WHERE `visitor_gate_pass_dtl_id`='$visitor_gate_pass_dtl_id'";
            $query = $pdoconn->prepare($sql1);
            if($query->execute()){

            // $visitor_gate = visitor_gate_pass_dtl($prk_admin_id,$visitor_mobile,$visitor_name,$visitor_gender,$vis_id_prooof_img,$visitor_add1,$visitor_add2,$visitor_state,$visitor_city,$visitor_landmark,$visitor_country,$visitor_pin_cod,$visitor_img,$id_name,$id_number,$visitor_gate_pass_num,$effective_date,$end_date,$user_name);

            // $json = json_decode($visitor_gate);
            // if($json->status=='1'){
                // $visitor_gate_pass_dtl_id = $json->visitor_gate_pass_dtl_id;

                if($number > 0){  
                    for($i=0; $i<$number; $i++) {  
                        $falt_name=$_POST["falt_name"][$i];
                        $tower_name=$_POST["tower_name"][$i];
                        $start_date=$_POST["start_date"][$i];
                        $end_date1=$_POST["end_date1"][$i];
                        $remark=$_POST["remark"][$i];
                        $sql = "INSERT INTO `visitor_service_dtl` (`visitor_gate_pass_dtl_id`,`prk_admin_id`,`tower_name`,`falt_name`,`in_time`,`out_time`,`visitor_remark`,`inserted_by`,`inserted_date`) VALUES ('$visitor_gate_pass_dtl_id','$prk_admin_id', '$tower_name','$falt_name','$start_date','$end_date1','$remark','$user_name','".TIME."')";
                        $query = $pdoconn->prepare($sql);
                        $query->execute();
                    }                    
                    $response['status'] = 1;
                    $response['message'] = 'Updated Successful';
                    $response = json_encode($response);  
                }else{                    
                    $response['status'] = 0;
                    $response['message'] = 'Updated Not Successful';
                    $response = json_encode($response);
                }
            }else{
                $response['status'] = 0;
                $response['message'] = 'Updated Not Successful';
                $response = json_encode($response);
            }  
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response['state'] = json_encode($response);
            $response = json_encode($response);
        }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        $response = json_encode($response);
    }
    echo $response;
?>