<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function visitor_vehicle_parking_space_list($prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park= array();
    $sql = "SELECT ra.prk_admin_id,
        ra.alocate_space as 'total_space',
        ifnull(rj.Use_space,'0') as 'used_space',
        (ra.alocate_space-ifnull(rj.Use_space,'0')) as 'avl_space'
        FROM
        (SELECT vpd.prk_admin_id,
        count(vpd.vis_prk_id) as alocate_space
        FROM visitor_parking_dtl vpd
        WHERE vpd.active_flag='Y'
        AND vpd.del_flag='N'
        GROUP BY vpd.prk_admin_id) ra LEFT JOIN
        (SELECT 
        count(pvtd.gate_pass_num) as Use_space,
        pvd.prk_admin_id
        FROM prk_visit_dtl pvd,prk_veh_trc_dtl pvtd, visitor_parking_dtl vpd
        WHERE vpd.prk_admin_id=pvd.prk_admin_id
        AND vpd.vis_prk_id=pvtd.gate_pass_num
        AND vpd.active_flag='Y'
        AND vpd.del_flag='N'
        AND pvtd.prk_admin_id=pvd.prk_admin_id
        AND pvtd.prk_veh_trc_dtl_id=pvd.prk_veh_trc_dtl_id
        AND pvtd.prk_veh_out_time IS NULL
        AND pvd.end_date is NULL 
        AND pvd.visit_with_veh='Y'
        GROUP BY pvd.prk_admin_id
        ) rj ON rj.prk_admin_id=ra.prk_admin_id
        WHERE ra.prk_admin_id='$prk_admin_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['veh_type'] = 'Visitor Parking';
            $park['total_space'] = $val['total_space'];
            $park['used_space'] = $val['used_space'];
            $park['avl_space'] = $val['avl_space'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['visitor_vehicle_space'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
/*function visitor_vehicle_parking_space_list($prk_admin_id){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
    $sql = "SELECT  rm.prk_admin_id,
        rm.veh_type,
        rm.veh_space as total_space,
        ifnull(ra.v_count,0) as used_space,
        (rm.veh_space - IFnull(ra.v_count, 0)) avl_space
        FROM
        (SELECT pvd.prk_admin_id,
                pvd.veh_type,
                COUNT(pvd.prk_visit_dtl_id) as v_count
        FROM prk_visit_dtl pvd 
        WHERE pvd.visit_with_veh='Y'
        AND pvd.end_date is NULL
        GROUP BY pvd.veh_type, pvd.prk_admin_id) ra RIGHT JOIN
        (SELECT psu.prk_admin_id,
                psuvs.veh_type,
                psuvs.veh_space 
        FROM prk_sub_unit_veh_space psuvs, prk_sub_unit psu
        WHERE psuvs.prk_admin_id=psu.prk_admin_id
        AND   psuvs.prk_sub_unit_id=psu.prk_sub_unit_id
        AND   psuvs.active_flag='Y' 
        AND   psuvs.del_flag='N'
        AND   psu.prk_sub_unit_short_name='VP' 
        AND   psu.active_flag='Y' 
        AND   psu.del_flag='N') rm 
        ON rm.prk_admin_id=ra.prk_admin_id AND rm.veh_type=ra.veh_type
        WHERE rm.prk_admin_id='$prk_admin_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['veh_type'] = $val['veh_type'];
            $park['total_space'] = $val['total_space'];
            $park['used_space'] = $val['used_space'];
            $park['avl_space'] = $val['avl_space'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['visitor_vehicle_space'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}*/
?>