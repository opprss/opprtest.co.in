<!-- ################################################
  
Description: Parking area admin add address once. after submitting 1st time all fileds will be disabled.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
 <?php
include "all_nav/header.php";  

@session_start();

if (isset($_SESSION['prk_admin_id'])) {
  $parking_admin_name=$_SESSION['parking_admin_name'];
  $prk_admin_id = $_SESSION['prk_admin_id'];
  $_SESSION["prk_admin_id"]=$prk_admin_id;
  $response_address = prk_area_address_show($prk_admin_id);
  $response_address = json_decode($response_address, true);
  if (!empty($response_address)) {
    $prk_address = $response_address['prk_address'];
    $prk_land_mark = $response_address['prk_land_mark'];
    $prk_add_area = $response_address['prk_add_area'];
    $prk_add_city = $response_address['prk_add_city'];
    $prk_add_state = $response_address['prk_add_state'];
    $prk_add_country = $response_address['prk_add_country'];
    $prk_add_pin = $response_address['prk_add_pin'];
    $city_name = $response_address['city_name'];
    $state_name = $response_address['state_name'];
  }

}

?>

<!-- header position -->
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
</style> 

<div class="am-mainpanel">

  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Address</h5>
    <!-- <form id="searchBar" class="search-bar" action="#">
      <div class="form-control-wrapper">
        <input type="search" class="form-control bd-0" placeholder="Search...">
      </div>
      <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
    </form>search-bar-->
  </div><!-- am-pagetitle -->
  
  <div class="am-pagebody">
      <div class="card single pd-20 pd-sm-40" >
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <form name="addressForm">
            <div class="form-layout">
              <div class="row mg-b-25">
                  <div class="col-lg-4 error_show">
                    <div class="form-group">
                      <label class="form-control-label size">ADDRESS <span class="tx-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="Address" name="prk_address" id="address" autofocus="autofocus" value="<?php if(isset($prk_address)) echo $prk_address; else echo ''; ?>" onkeydown="upperCase(this)">
                     <span id="error_msg" class="color"></span>
                    </div>
                  </div>
                  <div class="col-lg-4 error_show">
                    <div class="form-group">
                      <label class="form-control-label size">LANDMARK</label>
                      <input type="text" class="form-control" placeholder="landmark" name="prk_land_mark" id="landmark" value="<?php if(isset($prk_land_mark)) echo $prk_land_mark; else echo ''; ?>" onkeydown="upperCase(this)">
                          <span id="error_landmark" class="color"></span>
                    </div>
                  </div>
                  <div class="col-lg-4 error_show">
                    <div class="form-group">
                      <label class="form-control-label size">AREA <span class="tx-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="Area" name="prk_add_area" id="prk_add_area" value="<?php if(isset($prk_add_area)) echo $prk_add_area; else echo ''; ?>" onkeydown="upperCase(this)">
                      <span id="error_area" class="color"></span>
                    </div>
                  </div>
                  <div class="col-lg-4 error_show">
                    <div class="form-group">
                      <label class="form-control-label size">COUNTRY <span class="tx-danger">*</span></label>
                      <input type="text" class="form-control disabled" value="INDIA" name="prk_add_country" id="country" readonly="readonly" style="background-color: #fafafa; color: gray;">
                    </div>
                  </div>
                  <div class="col-lg-4 error_show">
                    <label class="form-control-label size">STATE <span class="tx-danger">*</span></label>
                    <div class="form-group" style="font-size:14px">
                        <?php 
                        if (isset($state_name)) {
                        ?>
                          <div class="select" style="margin-top: -5px;">
                          <select style="background-color: #fafafa; color: gray;">
                            <option><?php echo $state_name; ?></option>
                          </select>
                        </div>
                        <?php
                        }else{
                        ?>
                        <div class="select" style="margin-top: -5px;">
                          <select name="prk_add_state" id="state" value="" style="color: skyblue;">
                          </select>
                        </div>
                        <span id="error_state" class="color"></span>
                        <?php
                        }
                        ?>
                    </div>
                  </div>
                  <div class="col-lg-4 error_show">
                    <div class="form-group">
                      <label class="form-control-label size">CITY <span class="tx-danger">*</span></label>
                      <?php 
                        if (isset($city_name)) {
                        ?>
                          <div class="select" style="margin-top: -5px;font-size:14px" >
                          <select style="background-color: #fafafa; color: gray;">
                            <option><?php echo $city_name; ?></option>
                          </select>
                        </div>
                        <?php
                        }else{
                        ?>
                        <div class="select" style="margin-top: -5px;">
                          <select name="prk_add_city" id="city" value="" style="color: skyblue">
                        </select>
                        </div>
                        <?php
                        }
                        ?>
                        <span id="error_city" class="color"></span>
                    </div>
                  </div>
                  <div class="col-lg-4 error_show">
                    <div class="form-group" id="staticParent">
                      <label class="form-control-label size">PIN <span class="tx-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="Pin Code" name="prk_add_pin" id="pin" maxlength="6" value="<?php if(isset($prk_add_pin)) echo $prk_add_pin; else echo ''; ?>">
                      <span id="error_pin" class="color"></span>
                    </div>
                  </div>

                  <div class="col-lg-4">
                    <div class="form-layout-footer mg-t-30">
                      
                      <div class="row">
                        <div class="col-lg-6">
                          <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
                        </div>
                        <div class="col-lg-6">
                          <button type="button" class="btn btn-block prk_button_skip" name="skip" id="skip">SKIP</button>
                        </div>
                      </div>
                    </div>
                  </div>
              </div><!-- form-layout -->
            </div>
          </form>
          
    </div>
  </div>  

<!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
    
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
        /*var prk_admin_id=' <?php echo $prk_admin_id; ?>';
        alert(prk_admin_id);*/
  var address = '<?php echo $prk_address; ?>';
  if (address != '') {
    $("input").prop("disabled", true);
    $("input").css("background-color", "#fafafa");
    $("input").css("color", "gray");
    $("#save").prop("disabled", true);
  }
  $('#save').click(function(){
          
        var prk_address = $('#address').val();
        var prk_land_mark = $('#landmark').val();
        var prk_add_area = $('#prk_add_area').val();
        var prk_add_country = $('#country').val();
        var prk_add_state = $('#state :selected').val();
        var prk_add_city =$("#city :selected").val();
        var prk_add_pin = $('#pin').val();
        var prk_admin_id=' <?php echo $prk_admin_id; ?>';
        var inserted_by=' <?php echo $parking_admin_name; ?>';
        if(prk_address!='' && prk_add_area!='' && prk_add_state!='' && prk_add_city!='' && prk_add_pin!='' && prk_add_pin.length == 6){
           var urlPrkAdd = prkUrl+'prk_add.php';
          $('#save').val('Wait ...').prop('disabled', true);
          $.ajax({
              url :urlPrkAdd,
              type:'POST',
              data :
              {
                'prk_address':prk_address,
                'prk_land_mark':prk_land_mark,
                'prk_add_area':prk_add_area,
                'prk_add_country':prk_add_country,
                'prk_add_state':prk_add_state,
                'prk_add_city':prk_add_city,
                'prk_add_pin':prk_add_pin,
                'prk_admin_id':prk_admin_id,
                'inserted_by':inserted_by,
                'token': '<?php echo $token;?>'
              },
              dataType:'html',
              success  :function(data)
              {
                $('#save').val('SAVE').prop('disabled', false);
                var json = $.parseJSON(data);

                if (json.status) {
                  $.alert({
                    icon: 'fa fa-smile-o',
                    theme: 'modern',
                    title: 'Success !',
                    content: "<p style='font-size:0.9em;'>Address saved successfully</p>",
                    type: 'green',
                    buttons: {
                      Ok: function () {
                        window.location='dashboard';
                      }
                    }
                  });
                }else{
                  if( typeof json.session !== 'undefined'){
                    if (!json.session) {
                      window.location.replace("logout.php");
                    }
                  }else{
                    $.alert({
                    icon: 'fa fa-frown-o',
                    theme: 'modern',
                    title: 'Error !',
                    content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                    type: 'red'
                  });
                  }
                }
              }
          });
      }else{
        if (prk_address == '') {
        $('#error_msg').text("Address is required");
        }if (prk_add_area == '') {
        $('#error_area').text("Area is required");
        }if (prk_add_state == '') {
          $('#error_state').text('State  is required');
        }if (prk_add_city == '') {
          $('#error_city').text('City is required');
        }if (prk_add_pin == '') {
          $('#error_pin').text("Pin number is required & must be 6 characters long");
        }
        return false;
      }

      });


  $('#address').blur(function(){
    var prk_address = $('#address').val();
    if(prk_address!=''){
    $('#error_msg').text('');
    return false;
    }else{
    $('#error_msg').text('Enter your address');
    }
  })

     $('#prk_add_area').blur(function(){
    var prk_add_area = $('#prk_add_area').val();
    if(prk_add_area!=''){
      $('#error_area').text('');
      return false;
    }else{
    $('#error_area').text('Enter your area');
    }
  })
  $('#state').change(function(){
    var prk_add_state = $('#state :selected').text();
    if(prk_add_state!=''){
    $('#error_state').text('');
    }
  });

  $('#city').change(function(){
    var prk_add_city =$("#city :selected").val();
    if(prk_add_city!=''){
    $('#error_city').text('');
    }
  });


    $('#pin').blur(function(){
      var prk_add_pin = $('#pin').val();
      if(prk_add_pin == ''){
        $('#error_pin').text("Pin number is required & must be 6 characters long");
        return false;
      }else if(prk_add_pin.length != 6){
        $('#error_pin').text("Pin number is required & must be 6 characters long");
        return false;
      }else{
        $('#error_pin').hide();
        return true;
      }
    });
    

        /*validation*/
        $( document ).ready( function () {

            /*custom validation*/

            $("#state").blur(function(){
                var state = $('#state').val();
                if(state == "")
                {
                  $('#error_state').show();
                  return false;
                }else{
                    $('#error_state').hide();
                    return true;
                }
            });
        } );
    </script>


    <script>

      //select state
      $(document).ready(function(){

      var urlState = prkUrl+'state.php';

       $.ajax ({
        type: 'POST',
        url: urlState,
        data: "",
        success : function(data) {
          // alert(data);
          var obj=JSON.parse(data);
           var areaOption = "<option value=''>SELECT STATE</option>";
           $.each(obj['state'], function(val, key) {
          // alert(key);
            areaOption += '<option value="' + key['sta_id'] + '">' + key['state_name'] + '</option>'
           });

          $("#state").html(areaOption);
          


        }
  });
//select city 
 
 $('#state').on("change", function() {
    var id=($("#state").val());
    var urlCity = prkUrl+'city.php';

     $.ajax ({
        type: 'POST',
        url: urlCity,
        data: {id:id},
        success : function(data) {
          // alert(data);
            var obj=JSON.parse(data);
             
           //console.log(obj);
           var areaOption = "<option value=''>SELECT CITY</option>";
           $.each(obj['city'], function(val, key) {
            areaOption += '<option value="' + key['city_id'] + '">' + key['city_name'] + '</option>'
           });

        $("#city").html(areaOption);



        }
    });

  });
// });


//pin number validation only accept numeric value
/*$(function() {
  $('#staticParent').on('keydown', '#pin', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
})*/
$("#pin").keypress(function(event){
  var inputValue = event.charCode;
  if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0)){
    event.preventDefault();
  }
});
$('#skip').click(function(){
 window.location.href='dashboard';
});

     });

      /*uppercase*/
  function upperCase(a){
      setTimeout(function(){
          a.value = a.value.toUpperCase();
      }, 1);
  }

    </script>
    <style type="text/css">
      .color{
        color: red;
        font-size: 13px;
        position: absolute;
      }
    </style>