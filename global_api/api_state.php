<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function state(){
    $allplayerdata = array();
    $response = array();
    $vehicle = array();
    global $pdoconn;
    $sql = "SELECT * FROM `state` WHERE `active_flag`='".FLAG_Y."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val) 
    {
        $state_name=$val['state_name'];
        $sta_id=$val['e'];
        $response['status'] = 1;
        $response['message'] = 'Successfully';
        $vehicle['sta_id'] = $sta_id;
        $vehicle['state_name'] = $state_name;
        array_push($allplayerdata, $vehicle);
        $response['state'] = $allplayerdata;
    }
    return json_encode($response);
}
?>