<?php
@session_start();
include('../../global_asset/emp_user_include.php');
if(!isset($_SESSION['employee_user_name'])){
  header('location:logout');
}
$employee_user_id=$_SESSION["employee_user_id"];
$employee_user_name=$_SESSION["employee_user_name"];
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <title>OPPR | Digital Parking Solution</title>

    <link rel="shortcut icon" href="../../img/favicon.png" type="image/x-icon">
    <!--     <link rel="icon" href="http://indicoderz.in/img/favicon.ico" type="image/x-icon"> -->
    <!-- vendor css -->
    <link href="../../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="../../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="../../lib/jquery-toggles/toggles-full.css" rel="stylesheet">
    <link href="../../lib/rickshaw/rickshaw.min.css" rel="stylesheet">

    <!-- calender -->
    <link rel="stylesheet" href="../../library/date/css/pickmeup.css" type="text/css" />
    <script type="text/javascript" src="../../library/date/js/pickmeup.js"></script>

    <!-- icon -->
    <script src="https://use.fontawesome.com/f9a581fe37.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
    <script src="../../js/jquery.min.js"></script>

    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css"> -->
    <link rel="stylesheet" href="../../css/jquery-confirm.min.css">

    <!-- Amanda CSS -->
    <link rel="stylesheet" href="../../css/amanda.css">
    <link rel="stylesheet" href="../../css/style.css">
    <!-- Data Dable -->
    <link href="../../datatable/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <link href="../../datatable/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Data Dable -->
      <script src="https://kit.fontawesome.com/f97f7deb0b.js"></script>
  </head>

  <body>
    <div class="am-header"  id="header_1">
      <div class="am-header-left">
        <a id="naviconLeft" href="" class="am-navicon d-none d-lg-flex"><i class="icon ion-navicon-round"></i></a>
        <a id="naviconLeftMobile" href="" class="am-navicon d-lg-none"><i class="icon ion-navicon-round"></i></a>
        <a href="dashboard" class="am-logo">
          <img src="<?php echo LOGO ?>" style="height:auto; width:160px !important;">
        </a>
      </div><!-- am-header-left -->
      <div class="am-header-right">
        <div class="heaing pd-t-10" style="text-align: right;">
           <a href="logout" style="color: #FFF"><i class="fa fa-power-off"></i> Logout</a>
        </div>
      </div><!-- am-header-right -->
      <!-- am-header-right -->
    </div><!-- am-header -->

    <div class="am-sideleft" >
      <div class="nav am-sideleft-tab" id="nav" style=" background-color: #00b8d4;">
       
      </div>

      <div class="tab-content" style="" id="sidemenu">
        <div id="mainMenu" class="tab-pane active">
          <ul class="nav am-sideleft-menu">
            <li class="nav-item">
              <a href="dashboard" class="nav-link">
                <i class="icon ion-ios-home"></i>&nbsp;&nbsp;
                <span>Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="prk-draft-list" class="nav-link">
                <i class="fa fa-list-alt"></i>&nbsp;&nbsp;
                <span>Premises List</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="super-admin-dashboard" class="nav-link">
                <i class="fa fa-bars"></i>&nbsp;&nbsp; 
                <span>Form Permission</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="prk-list" class="nav-link">
                <!--<i class="fas fa-users-cog"></i>-->
                <i class="fa fa-cogs"></i>&nbsp;&nbsp;
                <span>Premises Setting</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="user-from-permission" class="nav-link">
                <i class="fa fa-users"></i>&nbsp;&nbsp;
                <span>Employee Permission</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="otp-list" class="nav-link">
                <i class="fa fa-commenting-o"></i>&nbsp;&nbsp; 
                <span>OTP LIST</span>
              </a>
            </li>
            <!-- nav-item -->
          </ul>
        </div><!-- #mainMenu -->
      </div><!-- tab-content -->
      <nav class="navbar fixed-bottom navbar-light bg-faded">
        <div class="">
          <span style="color: #00b8d4; font-size: 0.8em;"><?php echo COPYRIGHT; ?></span>
        </div><!-- am-footer -->
      </nav>
    </div><!-- am-sideleft -->


<style type="text/css">
  .fa-plus:before {
    font-family: 'FontAwesome';
    content: "\f067";
    display: none;
  }
  .fa-minus:before {
    font-family: 'FontAwesome';
    content: "\f068";
    display: none;
  }
</style>