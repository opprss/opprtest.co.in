<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2019
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
$allplayerdata = array();
$prk = array();
if(isAvailable(array('prk_admin_id','parking_admin_name','token','tower_id','flat_id','owner_id','living_status','payment_type','bank_name','cheque_number','tenant_id','main_tran_by','owner_name','flat_name','tower_name','flat_size','flat_master_id'))){

    if(isEmpty(array('prk_admin_id','parking_admin_name','token','tower_id','flat_id','owner_id','living_status','payment_type','owner_name','flat_name','tower_name','flat_master_id'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $parking_admin_name = trim($_POST['parking_admin_name']);
        $token = trim($_POST['token']);
        $to_year = '';
        $to_month = '';
        $tower_id = trim($_POST['tower_id']);
        $flat_id = trim($_POST['flat_id']);
        $flat_name = trim($_POST['flat_name']);
        $flat_size = trim($_POST['flat_size']);
        $tower_name = trim($_POST['tower_name']);
        $owner_id = trim($_POST['owner_id']);
        $owner_name = trim($_POST['owner_name']);
        $living_status = trim($_POST['living_status']);
        $tenant_id = trim($_POST['tenant_id']);
        $payment_type = trim($_POST["payment_type"]); 
        $bank_name = trim($_POST["bank_name"]); 
        $cheque_number = trim($_POST["cheque_number"]); 
        $main_tran_by = trim($_POST["main_tran_by"]); 
        $manTar = $_POST['manTar'];
        $flat_master_id = trim($_POST['flat_master_id']);
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            foreach ($manTar as $value){ 
                $total_amount=0;
                $year=$value['year'];
                $from_month=$value['from_month'];
                // $total_amount=$value['total_amount'];
                $full_name=$value['full_name'];
                $mant_tr=$value['mant_tr'];
                $sql="SELECT `main_tran_id` FROM `maintenance_transition` WHERE `prk_admin_id`='$prk_admin_id' AND `main_tran_from_year`='$year' AND `main_tran_from_month`='$from_month' AND `tower_id`='$tower_id' AND `flat_id`='$flat_id'";
                $query  = $pdoconn->prepare($sql);
                $query->execute();
                $count=$query->rowCount();
                if($count<=0){
                    $prk_in =prk_inf($prk_admin_id);
                    $json = json_decode($prk_in);
                    if($json->status=='1'){
                        $prk_admin_name = $json->prk_admin_name;
                        $prk_area_name = $json->prk_area_name;
                        $prk_area_short_name = $json->prk_area_short_name;
                        $last_due_day = $json->last_due_day;
                        $choose_due_day = $json->choose_due_day;
                        $prk_area_logo_img = $json->prk_area_logo_img;
                    }
                    $prk_area_address =prk_area_address_show($prk_admin_id);
                    $prk_area_address_show = json_decode($prk_area_address);
                    $prk_address=$prk_area_address_show->prk_address;
                    $prk_land_mark=$prk_area_address_show->prk_land_mark;
                    $prk_add_area=$prk_area_address_show->prk_add_area;
                    $state_name=$prk_area_address_show->state_name;
                    $city_name=$prk_area_address_show->city_name;
                    $prk_add_country=$prk_area_address_show->prk_add_country;
                    $prk_add_pin=$prk_area_address_show->prk_add_pin;
                    $address=$prk_address.', '.$prk_add_area.', PIN: '.$prk_add_pin;
                    $a_date = $year.'-'.$from_month.'-23';
                    if ($last_due_day=='Y') {
                        $date = new DateTime($a_date);
                        $date->modify('last day of this month');
                        $payment_due_date= $date->format('d-m-Y');
                    }else{
                        $a_date = $year.'-'.$from_month.'-'.$choose_due_day;
                        $time = strtotime($a_date);
                        $payment_due_date = date("d-m-Y", strtotime("+1 month", $time));
                    }
                    #-------------------------#
                    $main_tran_id=uniqid();
                    $maintenance_tran=maintenance_tran_show($prk_admin_id,$parking_admin_name,$year,$from_month,$tower_id,$flat_id,$owner_id,$living_status,$tenant_id,$flat_master_id);
                    $maintenance_tran = json_decode($maintenance_tran, true);
                    foreach ( $maintenance_tran['mani_trn_list'] as $value ) {
                        $maint_id=$value['maint_id'];
                        $maint_type=$value['maint_type'];
                        $maint_charge=empty($value['maint_charge'])?'0':$value['maint_charge'];
                        $payable_time=$value['payable_time'];
                        $payable_time_full=$value['payable_time_full'];
                        $main_tran_all_dtl_id=uniqid();
                        $sql="INSERT INTO `maintenance_transition_all_dtl`(`main_tran_all_dtl_id`, `prk_admin_id`, `main_tran_id`, `maint_id`, `maint_charge`, `inserted_by`, `inserted_date`,`maint_type`) VALUES ('$main_tran_all_dtl_id','$prk_admin_id','$main_tran_id','$maint_id','$maint_charge','$parking_admin_name','".TIME."','$maint_type')";
                        $query  = $pdoconn->prepare($sql);
                        $query->execute();
                        $total_amount +=$maint_charge;
                    }
                    #-------------------------#
                    $bill_date=TIME_TRN_WEB;
                    $words=api_number_to_word($total_amount);
                    $bill_month = date("M-Y", strtotime($a_date));
                        # this function add maintenance_tran_reseave_mail() function #
                    $order_id= maintenance_tran_bill_generate($prk_admin_id,$prk_area_short_name,$bill_month);
                    $maintenance_bill_p=maintenance_bill_pdf_generate($prk_admin_name,$prk_area_name,$address,$order_id,$bill_date,$payment_due_date,$owner_name,$flat_name,$total_amount,$words,$tower_name,$flat_size,$bill_month,$prk_area_logo_img,$prk_admin_id,$year,$from_month,$tower_id,$flat_id,$owner_id,$living_status,$tenant_id,$flat_master_id);
                    $maintenance_bill_pd = json_decode($maintenance_bill_p);
                    $maintenance_bill_pdf=$maintenance_bill_pd->url;
                    // $maintenance_bill_pdf='abcf.pdf';
                    $tran_date=TIME;
                    $tran_flag='CR';
                    $sql="INSERT INTO `maintenance_transition`(`main_tran_id`, `prk_admin_id`, `tower_id`, `flat_id`, `owner_id`, `tenant_id`, `order_id`, `main_tran_from_year`, `main_tran_from_month`, `main_tran_to_year`, `main_tran_to_month`, `total_amount`, `main_tran_date`, `inserted_by`, `inserted_date`, `payment_type`, `bank_name`, `cheque_number`, `main_tran_by`, `main_tran_rece_by`,`maintenance_bill_pdf`) VALUES ('$main_tran_id','$prk_admin_id','$tower_id','$flat_id','$owner_id','$tenant_id','$order_id','$year','$from_month','$to_year','$to_month','$total_amount','$tran_date','$parking_admin_name','".TIME."','$payment_type','$bank_name','$cheque_number','$main_tran_by','$parking_admin_name','$maintenance_bill_pdf')";
                    $query  = $pdoconn->prepare($sql);
                    if($query->execute()){
                        apartment_maintenance_transition($prk_admin_id,$main_tran_id,$total_amount,$payment_type,$tran_date,$tran_flag,$parking_admin_name,'A',$parking_admin_name);
                        maintenance_tran_reseave_mail($main_tran_id,$prk_admin_id,$living_status,$prk_area_name,$address,$bill_date,$payment_due_date,$owner_name,$tower_name,$flat_name,$words,$flat_size,$bill_month,$prk_area_logo_img,$parking_admin_name,$year,$from_month,$tower_id,$flat_id,$owner_id,$tenant_id,$flat_master_id);
                    }else{
                        $response['status'] = 0; 
                        $response['message'] = 'Payment Unsuccessful';
                        $response = json_encode($response);
                    }
                    $prk[$full_name] =PRK_BASE_URL.$maintenance_bill_pdf;
                    array_push($allplayerdata, $prk);
                }else{
                    $response['status'] = 0; 
                    $response['message'] = $full_name.' Month Allready Paid';
                    echo json_encode($response);
                    die();
                }
            }
            $response['status'] = 1; 
            $response['maintenance_bill_pdf'] = $prk; 
            $response['message'] = 'Payment Successful';
            $response = json_encode($response);
        }else{
            $response = $resp;
            // print_r($manTar);
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>