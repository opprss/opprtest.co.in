<?php
@session_start();
if(!isset($_SESSION['prk_admin_id'])){
  header('location:../signin.php');
}
include('../global_asset/parking_include.php');

$sessionDetails = array();
$sessionDetails = getSessionDetails();
//print_r($sessionDetails);
// var_dump($sessionDetails);
$parking_admin_name = $sessionDetails['parking_admin_name'];
$prk_area_name = $sessionDetails['prk_area_name'];
$prk_area_pro_img = $sessionDetails['prk_area_pro_img'];
//$response['prk_area_pro_img'] = $prk_area_pro_img;
if (isset($_SESSION['token'])) {
  $token = $_SESSION['token'];
  $prk_admin_id = $_SESSION['prk_admin_id'];
}

$page_name = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <title>OPPRSS | A Smart parking & Security Solution</title>

    <link rel="shortcut icon" href="<?php echo FAVICON ?>" type="image/x-icon">
    <!--     <link rel="icon" href="http://indicoderz.in/img/favicon.ico" type="image/x-icon"> -->
    <!-- vendor css -->
    <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="../lib/jquery-toggles/toggles-full.css" rel="stylesheet">
    <link href="../lib/rickshaw/rickshaw.min.css" rel="stylesheet">

    <!-- calender -->
    <link rel="stylesheet" href="../library/date/css/pickmeup.css" type="text/css" />
    <script type="text/javascript" src="../library/date//js/pickmeup.js"></script>

    <!-- icon -->
    <script src="https://use.fontawesome.com/f9a581fe37.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

    <!-- Amanda CSS -->
    <link rel="stylesheet" href="../css/amanda.css">
    <link rel="stylesheet" href="../css/style.css">
    <!-- Data Dable -->
    <link href="../datatable/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <link href="../datatable/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Data Dable -->

    <script type="text/javascript">
      prk_user_token_check();
      setInterval(function(){
        prk_user_token_check();
      }, 5000);
      function prk_user_token_check(){
        // alert('prk_token_check');
        var pkrUrl = "<?php echo PRK_URL; ?>";
        var user_token_check = pkrUrl+'prk_token_check.php';
        var prk_admin_id = "<?php echo $_SESSION['prk_admin_id']; ?>";
        var token = "<?php echo $token; ?>";
        // alert(prk_admin_id);
        $.ajax({
          url :user_token_check,
          type:'POST',
          data :
          {
            'prk_admin_id':prk_admin_id,
            'token':token
          },
          dataType:'html',
          success  :function(data)
          {
            var json = $.parseJSON(data);
            if (json.session) {
            }else{
             window.location.replace("logout.php");
            }
          }
        });
      }
    </script>
  </head>

  <body>
    <div class="am-header"  id="header_1">
      <div class="am-header-left">
        <a id="naviconLeft" href="" class="am-navicon d-none d-lg-flex"><i class="icon ion-navicon-round"></i></a>
        <a id="naviconLeftMobile" href="" class="am-navicon d-lg-none"><i class="icon ion-navicon-round"></i></a>
        <a href="dashboard" class="am-logo">
          <img src="<?php echo LOGO ?>" style="height:auto; width:160px !important;">
        </a>
      </div><!-- am-header-left -->

      <div class="am-header-right">
        <div class="dropdown dropdown-profile">
          <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
          <!-- <img src="uploades/CENTRAL_4233/profile/6471518685559.jpg" class="wd-32 rounded-circle" alt=""> -->
            <?php
              if (isset($sessionDetails)){
                if (!empty($prk_area_pro_img)) {
            ?>
                  <img src="<?php echo PRK_BASE_URL.$prk_area_pro_img;?>" class="wd-32 rounded-circle" alt="">
            <?php
                }else{
            ?>
                <img src="<?php echo IMAGE_URL ?>user.png" class="wd-32 rounded-circle" alt="">
            <?php
                }
              }else{
                echo "hello";
              }
            ?>
            <span class="logged-name"><span class="hidden-xs-down">
              <?php
                  if (isset($sessionDetails))
                    //echo session_id();
                    echo $prk_area_name;
                  else
                    echo "jane Doe";
                ?>
            </span> <i class="fa fa-angle-down mg-l-3"></i></span>
          </a>
          <div class="dropdown-menu wd-200">
            <ul class="list-unstyled user-profile-nav">
              <li><a href="parking-area-profle"><i class="icon ion-ios-person-outline"></i> Edit Profile</a></li>
              <!-- <li><a href=""><i class="icon ion-ios-gear-outline"></i> Settings</a></li> -->
              <!-- <li><a href="download"><i class="icon ion-ios-download-outline"></i> Downloads</a></li> -->
              <!-- <li><a href=""><i class="icon ion-ios-folder-outline"></i> Collections</a></li> -->
              <li><a href="change-password"><i class="fa fa-key" style="margin-right: 10px;"></i>Change Password</a></li>
              <li><a href="logout.php"><i class="icon ion-power"></i> Sign Out</a></li>
            </ul>
          </div><!-- dropdown-menu -->
        </div><!-- dropdown -->
      </div><!-- am-header-right -->
    </div><!-- am-header -->

    <div class="am-sideleft" >
      <div class="nav am-sideleft-tab" id="nav" style=" background-color: #00b8d4;">
      </div>

      <div class="tab-content" style="" id="sidemenu">
        <div id="mainMenu" class="tab-pane active">
          <ul class="nav am-sideleft-menu">
            <li class="nav-item">
              <a class="nav-link" href="dashboard"><h6 style="font-size: 13px;">Dashboard</h6></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="customer-bill-list"><h6 style="font-size: 13px;">Bill Payment</h6></a>
            </li>
            <?php
              $form_seq=1;
              $group_form_id=0;
              // $respon=prk_admin_form_list_show1($form_seq,$group_form_id);
              // $respon = json_decode($respon, true);
              $form_list_show=prk_admin_form_list_show2($form_seq,$group_form_id,$prk_admin_id);
              $form_list = json_decode($form_list_show, true);
              if($form_list['status']){
              foreach($form_list['form_dtl'] as $key => $item){
            ?>
              <li class="nav-item js_slider">
                <a class="nav-link with-sub" href="#"><h6 style="font-size: 13px;"><?=$key?></h6></a>
                <ul class="nav-sub">
                <?php foreach($form_list['form_dtl'][$key] as $value){?>
                  <li class="nav-item" style="margin-bottom: 10px;">
                    <?php if($value['form_php_name']=='parking-space-details'){?>
                      <h7><span><img style="width:20px; height:20px; margin-bottom: 5px;" src="<?php echo OPPR_BASE_URL.$value['form_img']; ?>"></span>&nbsp;&nbsp;<a  target="_blank" style="color:#343a40;" href="<?php echo $value['form_php_name'] ?>"><?php echo $value['form_display_name'] ?></a></h7>
                    <?php }else{?>
                   <h7><span><img style="width:20px; height:20px; margin-bottom: 5px;" src="<?php echo OPPR_BASE_URL.$value['form_img']; ?>"></span>&nbsp;&nbsp;<a  style="color:#343a40;" href="<?php echo $value['form_php_name'] ?>"><?php echo $value['form_display_name'] ?></a></h7>
                  <?php }?>
                  </li>
                <?php }?>
                </ul>
              </li>
            <?php  }} ?>
          </ul>
        </div><!-- #mainMenu -->
      </div><!-- tab-content -->
      <nav class="navbar fixed-bottom navbar-light bg-faded">
        <div class="">
          <span style="color: #00b8d4; font-size: 0.8em;"><?php echo COPYRIGHT; ?></span>
        </div><!-- am-footer -->
      </nav>
    </div><!-- am-sideleft -->

<script type="text/javascript">
  $(document).ready(function() {
    // hide submenu (works well)
    $('.js_slider ul').hide();
    // accordion multilevel menu (works well)
    $('.js_slider > a').on('click', function(event){
      var jsPro = $(this).parent('li');
      if (jsPro.hasClass('fa-minus')) {
          jsPro.removeClass('fa-minus');
          jsPro.addClass('fa-plus');
          jsPro.children().find('li.fa-minus').addClass('fa-plus');
          jsPro.children().find('li').removeClass('fa-minus');
          jsPro.find('ul').slideUp();
      }else {
        jsPro.addClass('fa-minus');
        jsPro.removeClass('fa-plus');
        jsPro.children('ul').slideDown();
        jsPro.siblings('li').children('ul').slideUp();
        jsPro.siblings('li.fa-minus').addClass('fa-plus');
        jsPro.siblings('li').removeClass('fa-minus');
        jsPro.siblings('li').find('li.fa-minus').addClass('fa-plus');
        jsPro.siblings('li').find('li').removeClass('fa-minus');
        jsPro.siblings('li').find('ul').slideUp();
      }
      event.preventDefault();
    });
    // add font-awesome icon
    // $('.js_slider').addClass("fa fa-plus");
  });// and ready
</script>
<script type="text/javascript">    
  $(document).bind("contextmenu",function(e) {        
    e.preventDefault();    
  });
  document.onkeydown = function(e) {
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
      return false;
    }    
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
      return false;    
    }    
    if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
      return false;   
    }    
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)){ 
      return false;    
    } 
  };
</script>
<style type="text/css">
  .fa-plus:before {
    font-family: 'FontAwesome';
    content: "\f067";
    display: none;
  }
  .fa-minus:before {
    font-family: 'FontAwesome';
    content: "\f068";
    display: none;
  }
</style>