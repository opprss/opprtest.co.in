<?php 
/*
Description: parking employ gate login
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date : 07-04-2018
*/ 
// require_once 'api/global_api.php';
    require_once 'api/parkAreaReg_api.php';
    $response = array();
    
    if(isAvailable(array('prk_admin_id','prk_area_gate_id','prk_user_username','prk_area_gate_name','prk_area_gate_type','prk_area_user_id','token'))){

        if(isEmpty(array('prk_admin_id','prk_area_gate_id','prk_user_username','prk_area_gate_name','prk_area_gate_type','prk_area_user_id','token'))){
            $prk_admin_id = trim($_POST['prk_admin_id']);
            $prk_area_gate_id = trim($_POST['prk_area_gate_id']);
            $prk_user_username = trim($_POST['prk_user_username']);
            $prk_area_gate_name = trim($_POST['prk_area_gate_name']);
            $prk_area_gate_type = trim($_POST['prk_area_gate_type']);
            $prk_area_user_id = trim($_POST['prk_area_user_id']);
            $token = trim($_POST['token']);

            $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
            $json = json_decode($resp);
            if($json->status=='1'){
                //prk_user_gate_logout($prk_admin_id,$prk_user_username);
                
                /*$respon = prk_user_gate_login($prk_admin_id,$prk_user_username,$prk_area_gate_id,$prk_area_gate_name,$prk_area_gate_type);
                echo ($respon);*/
                $respon = prk_user_gate_login($prk_admin_id,$prk_user_username,$prk_area_gate_id,$prk_area_gate_name,$prk_area_gate_type,$prk_area_user_id);
                $json = json_decode($respon);
                if($json->status=='1'){ 
                    $_SESSION["prk_area_gate_id"]= $prk_area_gate_id;
                    $_SESSION["prk_area_gate_type"]= $prk_area_gate_type;
                    $_SESSION["prk_qr_code"]= $json->prk_qr_code;
                    $_SESSION["prk_user_gate_login_id"]= $json->prk_user_gate_login_id;
                    // header('location: ../dashboard');
                    header('location: ../home');
                }
            }else{
                header('location: ../logout');
                $response['status'] = 0; 
                $response['session'] = 0;
                $response['message'] = 'Session Expired Please Login Again';
                echo json_encode($response);
            }            
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
        }
    }else{
         $response['status'] = 0; 
         $response['message'] = 'Invalid API Call';
         echo json_encode($response);
    }
 
?>