<?php 
/*
Description: vehicle out all payment detailas data save. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/  
function payment_dtl($prk_veh_trc_dtl_id,$user_admin_id,$prk_admin_id,$user_mobile_number,$user_nick_name,$veh_number,$total_hr, $round_hr,$total_pay,$payment_type,$payment_rec_status,$vehicle_type,$inserted_by,$prk_area_gate_id,$who_veh_out){
	$response = array();
    global $pdoconn;
    $sql = "INSERT INTO `payment_dtl`(`prk_veh_trc_dtl_id`, `user_admin_id`, `prk_admin_id`, `user_mobile_number`, `user_nick_name`,`veh_number`, `total_hr`, `round_hr`,`total_pay`, `payment_type`,`inserted_by`,`payment_rec_status`,`vehicle_type`,`prk_area_gate_id`,`inserted_date`,`who_veh_out`) VALUES ('$prk_veh_trc_dtl_id','$user_admin_id','$prk_admin_id','$user_mobile_number','$user_nick_name','$veh_number','$total_hr','$round_hr','$total_pay','$payment_type','$inserted_by','$payment_rec_status','$vehicle_type','$prk_area_gate_id','".TIME."','$who_veh_out')";
    $query  = $pdoconn->prepare($sql);
    if($query->execute()){
    	$id=$pdoconn->lastInsertId();
    	$response['status'] = 1;
        $response['payment_dtl_id'] = $id;
        $response['message'] = 'Successful';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful ';
    }  
    return json_encode($response); 
}
?> 