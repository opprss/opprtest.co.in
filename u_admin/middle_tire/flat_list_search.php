<?php 
/*
Description: found the parking area list.
Developed by: Rakhal Raj Mandal
Created Date: 30-05-2018
Update date : ----------
*/
require_once 'api/reg_api.php';
// require_once 'api/global_api.php';
$response = array();
if(isAvailable(array('user_admin_id','token','prk_admin_id','tower_id','flat_name'))){
    if(isEmpty(array('user_admin_id','token','prk_admin_id','tower_id'))){
        $user_admin_id = trim($_POST['user_admin_id']);
        $token = trim($_POST['token']);
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $tower_id = trim($_POST['tower_id']);
        $flat_name = trim($_POST['flat_name']);

            $response = flat_list_search($prk_admin_id,$tower_id,$flat_name);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response); 
} 
echo $response;
?>