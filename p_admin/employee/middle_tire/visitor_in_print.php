<?php
	@session_start();
	include 'api/parkAreaReg_api.php';
	$prk_visit_dtl_id = isset($_REQUEST['id']) ? trim($_REQUEST['id']) : ''; 
	$duplicate = isset($_REQUEST['duplicate']) ? trim($_REQUEST['duplicate']) : 'N'; 
	
	$vis_prt1 = visitor_print($prk_visit_dtl_id);
	$vis_prt = json_decode($vis_prt1, true);
	$visit_name = $vis_prt['visit_name'];
	$visit_mobile = $vis_prt['visit_mobile'];
	$prk_admin_id = $vis_prt['prk_admin_id'];
	$visitor_meet_to_name = $vis_prt['visitor_meet_to_name'];
	$visit_purpose = $vis_prt['visit_purpose'];
	$vehicle_type_dec = $vis_prt['vehicle_type_dec'];
	$veh_number = $vis_prt['veh_number'];
	$veh_type = $vis_prt['veh_type'];
	$start_date = $vis_prt['start_date'];
	$start_time = $vis_prt['start_time'];
	$end_date = $vis_prt['end_date'];
	$end_time = $vis_prt['end_time'];
	$current_date = $vis_prt['current_date'];
	$current_time = $vis_prt['current_time'];
	$visit_img = $vis_prt['visit_img'];
	$qrcode_value = $vis_prt['qrcode_value'];

	$prk_inf = prk_inf($prk_admin_id);
	$prk_inf = json_decode($prk_inf, true);
	$prk_area_name = $prk_inf['prk_area_name'];
	$prk_area_short_name = $prk_inf['prk_area_short_name'];
	$prk_area_display_name = $prk_inf['prk_area_display_name'];
	$prk_area_logo_img = $prk_inf['prk_area_logo_img'];

	$area_address = prk_area_address_show($prk_admin_id);
	$area_address = json_decode($area_address, true);
	$prk_address = $area_address['prk_address'];
	$prk_land_mark = $area_address['prk_land_mark'];
	$prk_add_area = $area_address['prk_add_area'];
	$city_name = $area_address['city_name'];
	$state_name = $area_address['state_name'];
	$prk_add_pin = $area_address['prk_add_pin'];
	$prk_add_country = $area_address['prk_add_country'];
	$address =$prk_land_mark.', ADD-'.$prk_address.', CITY-'.$city_name.', STATE-'.$state_name.', PIN-'.$prk_add_pin;

	$add_len=strlen($visit_name);
	$visit_name =($add_len>20)?mb_substr($visit_name, 0, 18).'..':$visit_name;

	$add_len=strlen($visit_purpose);
	$visit_purpose =($add_len>20)?mb_substr($visit_purpose, 0, 18).'..':$visit_purpose;
	
	$add_len=strlen($visitor_meet_to_name);
	$visitor_meet_to_name =($add_len>20)?mb_substr($visitor_meet_to_name, 0, 18).'..':$visitor_meet_to_name;


	$vhe_name_show=($veh_number=='NA')?$veh_number:$veh_number.'('.$veh_type.')';
	$in_date_show=$start_date.' '.$start_time;
	$out_date_show=$end_date.' '.$end_time;
	$qrcode_value=$visit_mobile.'-OPPR';
?>
<STYLE>
	td {text-align:left; font-size:14px; padding: 0px}
	td #cash{margin-left: 0px; font-size: 15px;padding: 0px;}
	td #Truck b{font-size: 15px; font-weight: bolder; text-align: center;}
	td #Haldia {font-size: 20px; font-style: bold;text-align: center;}
	/*td #Stationary{margin-left: 50px}*/
	td #barcode{text-align: center;}
	img{text-align: center;}
	td #estimation{font-size: 15px}
	@media print{
		@page {
		   margin-top: 0px;
		   margin-bottom: 0;
		}
		body  {
		   padding-top: 85px;
		   padding-bottom: 72px ;
		}
	}
	tr.border_bottom td{
		border-bottom: 1px solid black;
	}
	tr.border_bottom_futtor td{
		border-top: 1px solid black;
	}
</STYLE>
<table style="height: 204px; width: 328px; border: 1px solid">
	<tbody>
		<tr style="height: 5px;border: 1px solid black;" class="border_bottom">
			<td style="width: 60px; height: 5px;"><img src="<?=LOGO_PDF ?>" alt="" width="60" height="40" /></td>
			<td style="width: 294px; height: 5px;" colspan="4"><center><b><?=$prk_area_name?><b></center></td>
			<td style="width: 60px; height: 5px;"><img src="<?=PRK_BASE_URL.''.$prk_area_logo_img; ?>" alt="" width="60" height="40" /></td>
		</tr>
		<tr style="height: 18px;">
			<td style="width: 63px; height: 18px;" colspan="2">&nbsp;Name&nbsp;</td>
			<td style="width: 40px; height: 18px;" colspan="2">:<?=$visit_name?></td>
			<td style="width: 70px; height: 18px;" colspan="2" rowspan="3"><img src="<?=$visit_img?>" alt="" width="70" height="70" /></td>
		</tr>
		<tr style="height: 3px;">
			<td style="width: 63px; height: 3px;" colspan="2">&nbsp;Mobile</td>
			<td style="width: 40px; height: 3px;" colspan="2">:<?=$visit_mobile?></td>
		</tr>
		<tr style="height: 9px;">
			<td style="width: 63px; height: 9px;" colspan="2">&nbsp;Purpose</td>
			<td style="width: 40px; height: 9px;" colspan="2">:<?=$visit_purpose?></td>
		</tr>
		<tr style="height: 1px;">
			<td style="width: 63px; height: 1px;" colspan="2">&nbsp;Meet to</td>
			<td style="width: 40px; height: 1px;" colspan="2">:<?=$visitor_meet_to_name?></td>
			<td style="width: 10px; height: 1px;" colspan="2" rowspan="3"><img src="./../../../library/QRCodeGenerate/generate.php?text=<?=$qrcode_value?>" alt="" width="70" height="70" /></td>
		</tr>
		<tr style="height: 6px;">
			<td style="width: 63px; height: 6px;" colspan="2">&nbsp;Vehicle</td>
			<td style="width: 40px; height: 6px;" colspan="2">:<?=$vhe_name_show?>&nbsp;</td>
		</tr>
		<tr style="height: 18px;">
			<td style="width: 63px; height: 18px;" colspan="2">&nbsp;In Date</td>
			<td style="width: 40px; height: 18px;" colspan="2">:<?=$in_date_show?></td>
		</tr>
		</tr>
		<tr style="height: 18px;">
			<td style="width: 63px; height: 18px;" colspan="2">&nbsp;Out Date</td>
			<td style="width: 40px; height: 18px;" colspan="8">:........................................</td>
		</tr>
		<tr style="height: 25px;" class="border_bottom_futtor">
			<td style="width: 63px; height: 25px;" colspan="3">&nbsp;Date:<?=$current_date?><br />&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<?=$current_time?></td>
			<td style="width: 217px; height: 25px;" colspan="3">Authorized Signatory<br />By Order <?=$prk_area_display_name?></td>
		</tr>
	</tbody>
</table>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script type="text/javascript">
	$(window).on('load', function() {
	 	window.print();
		setTimeout(function(){window.close();}, 1);
	});
</script>
