<?php
/*
Description: show the all vehicle type.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function vehicleType(){
    $allplayerdata = array();
    global $pdoconn;
    $sql = "SELECT vehicle_type_Dec,vehicle_typ_img,vehicle_Sort_nm FROM `vehicle_type` WHERE `active_flag`='".ACTIVE_FLAG_Y."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val)
    {
        $vehicle['vehicle_type'] = $val['vehicle_type_Dec'];
        $vehicle['vehicle_Sort_nm'] = $val['vehicle_Sort_nm'];
        $vehicle['vehicle_typ_img'] = $val['vehicle_typ_img'];
        array_push($allplayerdata, $vehicle);
    }
    return ($allplayerdata);
}
?>