<?php
/*
Description: Parking area vehicle all rate dtl list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function current_attendance_list($prk_admin_id,$prk_area_user_id,$date_list){

    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    if (!empty($date_list)) {
      
        $date=$date_list;
    }else{

        $date=TIME_TRN;
    }
    $sql="SELECT `employee_attendance_list`.`employee_attendance_id`,
        `employee_attendance_list`.`prk_admin_id`, 
        `employee_attendance_list`.`prk_area_user_id`, 
        `employee_attendance_list`.`attendance_date`, 
        DATE_FORMAT(`employee_attendance_list`.`attendance_date`,'%b-%Y') AS `this_month`,
        DATE_FORMAT(`employee_attendance_list`.`in_time`,'%d-%m-%Y') AS `in_date`,
        DATE_FORMAT(`employee_attendance_list`.`in_time`,'%I:%i:%p') AS `in_time`,
        IF(`employee_attendance_list`.`out_time` IS NULL or `employee_attendance_list`.`out_time` = '', 'NA', DATE_FORMAT(`employee_attendance_list`.`out_time`,'%d-%m-%Y')) as out_date,
        IF(`employee_attendance_list`.`out_time` IS NULL or `employee_attendance_list`.`out_time` = '', 'NA', DATE_FORMAT(`employee_attendance_list`.`out_time`,'%I:%i:%p')) as out_time,
        `prk_area_user`.`prk_user_username`,
        `prk_area_user`.`prk_user_name`
        FROM `employee_attendance_list`,`prk_area_user` 
        WHERE `prk_area_user`.`del_flag`='".FLAG_N."'
        AND `prk_area_user`.`prk_area_user_id`=`employee_attendance_list`.`prk_area_user_id`
        AND `prk_area_user`.`prk_admin_id`=`employee_attendance_list`.`prk_admin_id`
        AND `employee_attendance_list`.`active_flag`='".FLAG_Y."'
        AND `employee_attendance_list`.`del_flag`='".FLAG_N."'
        AND `employee_attendance_list`.`prk_admin_id`='$prk_admin_id'
        AND if(`employee_attendance_list`.`out_time` IS NULL OR `employee_attendance_list`.`out_time`='','','YES')='' 
        AND `employee_attendance_list`.`attendance_date`='$date'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val)
        {
            $park['employee_attendance_id'] = $val['employee_attendance_id'];
            $park['prk_area_user_id'] = $val['prk_area_user_id'];
            $park['prk_user_username'] = $val['prk_user_username'];
            $park['attendance_date'] = $val['attendance_date'];
            $park['this_month'] = $val['this_month'];
            $park['in_date'] = $val['in_date'];
            $park['in_time'] = $val['in_time'];
            $park['out_date'] = $val['out_date'];
            $park['out_time'] = $val['out_time'];
            $park['prk_user_name'] = $val['prk_user_name'];
            // $park['prk_rate_eff_date'] = $val['prk_rate_eff_date'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Sucessfull';
        $response['atten_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
    // return $allplayerdata;
}
?>