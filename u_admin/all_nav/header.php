<?php
@session_start();
if(!isset($_SESSION['user_admin_id'])){
header('location:../signin');
}
if (isset($_SESSION['token'])) {
  $token = $_SESSION['token'];
  $user_admin_id = $_SESSION['user_admin_id'];
}
include('../global_asset/user_include.php');
$sessionDetails = array( );
$sessionDetails = getSessionDetails();

$user_name = $sessionDetails['user_name'];
$user_mobile = $sessionDetails['user_mobile'];
$user_img = $sessionDetails['user_img'];

/*token check in every page load*/
$json = json_decode(user_token_check($_SESSION['user_admin_id'],$token));
if (! $json->session) {
  header('location: logout');
}

$page_name = basename($_SERVER['PHP_SELF']);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>OPPRSS | A Smart parking & Security Solution</title>
    <link rel="shortcut icon" href="<?php echo FAVICON ?>" type="image/x-icon">
    <!-- vendor css -->
    <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="../lib/jquery-toggles/toggles-full.css" rel="stylesheet">
    <link href="../lib/rickshaw/rickshaw.min.css" rel="stylesheet">
    <!-- calender -->  
    <link rel="stylesheet" href="../library/date/css/pickmeup.css" type="text/css" />
    <script type="text/javascript" src="../library/date/js/pickmeup.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- icon -->
    <script src="https://use.fontawesome.com/f9a581fe37.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <!-- Amanda CSS -->
    <link rel="stylesheet" href="../css/amanda.css">
    <link rel="stylesheet" href="../css/style-user.css">
    <!-- Data Dable -->
    <link href="../datatable/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <link href="../datatable/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Data Dable -->
  </head>
  <body>
    <script type="text/javascript">
      user_token_check();
      setInterval(function(){
        user_token_check();
      }, 2000);
      function user_token_check(){
        // alert('prk_token_check');
        var pkrUrl = "<?php echo USER_URL; ?>";
        var user_token_check = pkrUrl+'user_token_check.php';
        var user_admin_id = "<?php echo $_SESSION['user_admin_id']; ?>";
        var token = "<?php echo $token; ?>";
        // alert(prk_admin_id);
        $.ajax({
          url :user_token_check,
          type:'POST',
          data :
          {
            'user_admin_id':user_admin_id,
            'token':token
          },
          dataType:'html',
          success  :function(data)
          {
            // alert(data);
            var json = $.parseJSON(data);
            if (json.session) {
            }else{
             window.location.replace("logout.php");
            }
          }
        });
      }
    </script>
    <div class="am-header"  id="header_1">
      <div class="am-header-left">
        <a id="naviconLeft" href="" class="am-navicon d-none d-lg-flex"><i class="icon ion-navicon-round"></i></a>
        <a id="naviconLeftMobile" href="" class="am-navicon d-lg-none"><i class="icon ion-navicon-round"></i></a>
        <a href="vehicle-list" class="am-logo">
          <img src="<?php echo LOGO ?>" style="height:auto; width:160px !important;">
        </a>
      </div><!-- am-header-left -->

      <div class="am-header-right">
        <div class="dropdown dropdown-profile">
          <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
            <?php
              if (isset($sessionDetails)){
                if (!empty($user_img)) {
            ?>
                  <img src="<?php echo USER_BASE_URL.$user_img;?>" class="wd-32 rounded-circle" alt="">
            <?php
                }else{
            ?>
                <img src="img/user.png" class="wd-32 rounded-circle" alt="">
            <?php 
                }
              }else{
                echo "hello";
              }             
            ?>
            <span class="logged-name">
              <span class="hidden-xs-down">
                <?php
                  if (isset($sessionDetails))
                    echo $user_name;
                  else
                    echo "jane Doe";
                ?>
              </span>
               <i class="fa fa-angle-down mg-l-3"></i></span>
          </a>
          <div class="dropdown-menu wd-200">
            <ul class="list-unstyled user-profile-nav">
              <li><a href="profile"><i class="icon ion-ios-person-outline"></i> Edit Profile</a></li>
              <li><a href="change-password"><i class="icon ion-ios-folder-outline"></i> Change Password</a></li>
              <li><a href="logout.php"><i class="icon ion-power"></i> Sign Out</a></li>
            </ul>
          </div><!-- dropdown-menu -->
        </div><!-- dropdown -->
      </div><!-- am-header-right -->
    </div><!-- am-header -->

    <div class="am-sideleft">
      <div class="nav am-sideleft-tab" id="nav" style=" background-color: #00b8d4;">
      </div>

      <div class="tab-content" style="" id="sidemenu">
        <div id="mainMenu" class="tab-pane active">
          <ul class="nav am-sideleft-menu">
            <li class="nav-item">
              <a href="vehicle-list" class="nav-link <?php if($page_name == 'vehicle-list.php') echo 'active' ?>">
                <i class="icon ion-ios-home"></i>
                <span>Home Page</span>
              </a>
            </li><!-- nav-item -->
            <li class="nav-item">
              <a href="vehicle-details" class="nav-link <?php if($page_name == 'vehicle-details.php') echo 'active' ?>">
                <i class="icon ion-model-s"  style="font-size: 1.3em;"></i>
                <span>Vehicle Details</span>
              </a>
            </li>
            <li class="nav-item">
              <!-- <a href="" class="nav-link with-sub"> -->
              <a href="user-list" class="nav-link <?php if($page_name == 'user-list.php') echo 'active' ?>">
                <i class="icon ion-person"></i>
                <span>Visitor List</span>
              </a>
            </li><!-- nav-item -->
            <li class="nav-item">
              <!-- <a href="" class="nav-link with-sub"> -->
              <a href="user-guest-list" class="nav-link <?php if($page_name == 'user-guest-list.php') echo 'active' ?>">
                <i class="icon ion-person"></i>
                <span>Guest List</span>
              </a>
            </li><!-- nav-item -->
            <li class="nav-item">
              <!-- <a href="" class="nav-link with-sub"> -->
              <a href="allow-kid" class="nav-link <?php if($page_name == 'allow-kid.php') echo 'active' ?>">
                <i class="icon ion-person"></i>
                <span>Allow Kid</span>
              </a>
            </li>
            <li class="nav-item">
              <!-- <a href="" class="nav-link with-sub"> -->
              <a href="parking-space-avail" class="nav-link <?php if($page_name == 'parking-space-avail.php') echo 'active' ?>">
                <i class="fa fa-list-ol" style="font-size: 1.3em;"></i>
                <span>Parking Space</span>
              </a>
            </li><!-- nav-item -->
            <li class="nav-item">
              <!-- <a href="" class="nav-link with-sub"> -->
              <a href="user-incident-in" class="nav-link <?php if($page_name == 'user-incident-in.php') echo 'active' ?>">
                <i class="fas fa-indent"></i>
                <span>Incident</span>
              </a>
            </li><!-- nav-item -->
            <li class="nav-item">
              <a href="licence" class="nav-link <?php if($page_name == 'licence.php') echo 'active' ?>">
                <i class="fa fa-id-card" style="font-size: 1.3em;"></i>
                <span>Licence</span>
              </a>
            </li><!-- nav-item -->
            <li class="nav-item">
              <a href="gate-pass" class="nav-link <?php if($page_name == 'gate-pass.php') echo 'active' ?>">
                <i class="fa fa-sticky-note" style="font-size: 1.3em;"></i>
                <span>Gate Pass</span>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a href="add-payments" class="nav-link <?php if($page_name == 'add-payments.php') echo 'active' ?>">
                <i class="fa fa-credit-card" style="font-size: 1.3em;"></i>
                <span>Payment</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="maintenance-area" class="nav-link <?php if($page_name == 'maintenance-area.php'||$page_name == 'maintenance-pay.php') echo 'active' ?>">
                <i class="fa fa-credit-card" style="font-size: 1.3em;"></i>
                <span>Maintenance Pay</span>
              </a>
            </li> -->
            <li class="nav-item">
              <a href="my-transaction" class="nav-link <?php if($page_name == 'my-transaction.php') echo 'active' ?>">
                <i class="fa fa-list-ol" style="font-size: 1.3em;"></i>
                <span>My Transactions</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="address-list" class="nav-link <?php if($page_name == 'address-list.php') echo 'active' ?>">
                <i class="icon ion-ios-navigate"></i>
                <span>Address</span>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a href="user-apartment-list" class="nav-link <?php if($page_name == 'user-apartment-list.php') echo 'active' ?>">
                <i class="icon ion-ios-navigate"></i>
                <span>Property list</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="relation-list" class="nav-link <?php if($page_name == 'relation-list.php') echo 'active' ?>">
                <i class="icon ion-ios-navigate"></i>
                <span>Property Relation</span>
              </a>
            </li> -->
            <li class="nav-item">
              <!-- <a href="" class="nav-link with-sub"> -->
                <a href="customer-services" class="nav-link <?php if($page_name == 'customer-services.php') echo 'active' ?>">
                <i class="icon ion-happy"></i>
                <span>Customer Services</span>
              </a>
            </li>
            <!-- nav-item -->
          </ul>
        </div><!-- #mainMenu -->
      </div><!-- tab-content -->

      <nav class="navbar fixed-bottom navbar-light bg-faded">
        <div class="">
          <span style="color: #00b8d4; font-size: 0.8em;"><?php echo COPYRIGHT ?></span>
        </div><!-- am-footer -->
      </nav>
    </div>
    </div><!-- am-sideleft -->
<!-- <script type="text/javascript">    
  $(document).bind("contextmenu",function(e) {        
    e.preventDefault();    
  });
  document.onkeydown = function(e) {
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
      return false;
    }    
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
      return false;    
    }    
    if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
      return false;   
    }    
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)){ 
      return false;    
    } 
  };
</script> -->


