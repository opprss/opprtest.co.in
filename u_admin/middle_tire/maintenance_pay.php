<?php 
/*
Description: user address show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
require_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_admin_id','mobile','token','prk_admin_id','m_account_number','tower_id','flat_id','owner_id','tenant_id','amount','payment_type','bank_name','cheque_number'))){
 	if(isEmpty(array('user_admin_id','mobile','token','prk_admin_id','m_account_number','tower_id','flat_id','amount','payment_type'))){
        $user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
        $token = trim($_POST['token']);
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $m_account_number = trim($_POST['m_account_number']);
        $tower_id = trim($_POST['tower_id']);
        $flat_id = trim($_POST['flat_id']);
        $owner_id = trim($_POST['owner_id']);
        $tenant_id = trim($_POST['tenant_id']);
        $amount = trim($_POST['amount']);
        $payment_type = trim($_POST['payment_type']);
        $bank_name = trim($_POST['bank_name']);
        $cheque_number = trim($_POST['cheque_number']);
        $online_payment_dtl_id=isset($_POST["online_payment_dtl_id"]) ? trim($_POST['online_payment_dtl_id']) : ''; 
        $orderid=isset($_POST["orderid"]) ? trim($_POST['orderid']) : ''; 
        $main_tran_by = 'U';
        $main_tran_rece_by = $mobile;
        $inserted_by = $mobile;
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){ 
            // $response = emergency_delete($adm_mer_dtl_id,$updated_by);
            $oth1=$oth2=$oth3=$oth4=$oth5='';
            $response = maintenance_pay($prk_admin_id,$m_account_number,$tower_id,$flat_id,$owner_id,$tenant_id,$amount,$payment_type,$bank_name,$cheque_number,$main_tran_by,$main_tran_rece_by,$inserted_by,$online_payment_dtl_id,$orderid,$oth1,$oth2,$oth3,$oth4,$oth5);
        }else{
            $response = $resp; 
        }
 	}else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
 		// $response['POST'] = $paramList = $_POST;;
        $response =  json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>