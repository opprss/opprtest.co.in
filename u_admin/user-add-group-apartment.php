<!-- ################################################
Description: List of user's vechile | user can delete vehicle from this page.
Developed by: Soemen Banerjee
Created Date: 17-03-2018
 ####################################################-->
<?php  
  include "all_nav/header.php";
  $list_id = isset($_REQUEST['list_id']) ? trim($_REQUEST['list_id']) : '';
  $list_name = isset($_REQUEST['list_name']) ? trim($_REQUEST['list_name']) : '';
  $user_apar_id =base64_decode($list_id);
  $user_apar_name =base64_decode($list_name);
  $relation_list1=user_to_user_relation_list($user_admin_id,$user_apar_id);
  $relation_list = json_decode($relation_list1, true);
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
  .error{
    font-size: 11px;
    color: red;
  }
  th,td{
    text-align: center;
  }
</style>
<script src="../lib/validation/jquery.validate.js"></script>
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
  <div class="am-mainpanel">
    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Property : (<?=$user_apar_name?>) Add Tenant</h5>
    </div>
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40" >
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <form role="form" id="addGroup" method="post" enctype="multipart/form-data">
          <div class="row mg-b-5">
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">User Name<span class="tx-danger">*</span></label>
                <input type="text" name="user_relation_name" id="user_relation_name" class="form-control" placeholder="Name">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">User Mobile Number<span class="tx-danger">*</span></label>
                <input type="text" name="user_relation_mobile" id="user_relation_mobile" class="form-control" placeholder="Mobile Number" maxlength="10">
              </div>
            </div>
            <div class="col-lg-2" style="display: none;">
              <div class="form-group">
                <label class="form-control-label size">Willing-to-rent-out-to<span class="tx-danger">*</span></label>
                <div class="select" style="font-size:14px">
                  <select name="willing_to_rent_out" id="willing_to_rent_out">
                    <option value="">Select Type</option>
                    <option value="Family">Family</option>
                    <option value="Single Men">Single Men</option>
                    <option value="Single Women">Single Women</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Agreement Month(0-36)<span class="tx-danger">*</span></label>
                <input type="text" name="rent_argument_month" id="rent_argument_month" class="form-control" placeholder="Rent Agreement Month">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Electricty & Water Charge<span class="tx-danger">*</span></label>
                <div class="select" style="font-size:14px">
                  <select name="elect_and_water_charge" id="elect_and_water_charge">
                    <option value="">Select Type</option>
                    <option value="Y">Yes</option>
                    <option value="N Men">No</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Security Deposit Amount<span class="tx-danger">*</span></label>
                <input type="text" name="security_deposit_amount" id="security_deposit_amount" class="form-control" placeholder="Amount">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Rent Agreement Amount<span class="tx-danger">*</span></label>
                <input type="text" name="user_relation_agreement_amount" id="user_relation_agreement_amount" class="form-control" placeholder="Amount">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                  <label class="form-control-label size">Agreement Start Date<span class="tx-danger">*</span></label>
                  <input type="text" class="form-control prevent_readonly" placeholder="start date" name="user_relation_agreement_start_date" id="user_relation_agreement_start_date" readonly="">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="form-control-label size">Agreement End Date</label>
                  <input type="text" class="form-control prevent_readonly" placeholder="end date" name="user_relation_agreement_end_date" id="user_relation_agreement_end_date" readonly="">
              </div>
            </div>
            <div class="col-lg-5">
              <div class="row mg-t-35">
                <div class="col-lg-4">
                  <div class="form-group">
                    <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="add_group" id="add_group">
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <button type="reset" class="btn btn-block prk_button btn-primary">Reset</button>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <button type="button" class="btn btn-block prk_button btn-primary" onclick="document.location.href='user-apartment-list';">Back</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40">
      <div class="table-wrapper">
        <table id="datatable1" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="wd-2p">Sl No.</th>
              <th class="wd-15p">Name</th>
              <th class="wd-10p">Mobile Number</th>
              <th class="wd-10p">Security Amount</th>
              <th class="wd-10p">Agreement Amount</th>
              <th class="wd-10p">Agreement Date </th>
              <th class="wd-5p">Agreement Month </th>
              <!-- <th class="wd-10p">Send Date & Time</th> -->
              <th class="wd-10p">Approved Date</th>
              <th class="wd-10p">Status</th>
              <th class="wd-10p">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $x=1;
            if ($relation_list['status']){
            foreach($relation_list['relation_list'] as $value){
              $status_full_name='';
              if ($value['user_relation_status']=='D') {
                # code...
                $status_full_name='Draft';
              }elseif ($value['user_relation_status']=='R') {
                # code...
                $status_full_name='Reject';
              }elseif ($value['user_relation_status']=='A') {
                # code...
                $status_full_name='Approved';
              }else{
                # code...
                $status_full_name='No Status';
              }
            $relation_trn_val="relation-transaction-history?list_id=".base64_encode($value['user_relation_id']);
            ?>
            <tr>
              <td><?=$x; ?></td>
              <td><?=$value['user_relation_name']; ?></td>
              <td><?=$value['user_relation_mobile']; ?></td>
              <td><?=$value['security_deposit_amount']; ?></td>
              <td><?=$value['user_relation_agreement_amount']; ?></td>
              <td><?=$value['agreement_start_date']; ?></td>
              <td><?=$value['rent_argument_month']; ?></td>
              <!-- <td><?//=$value['send_date'].' '.$value['send_time']; ?></td> -->
              <td><?=$value['approved_date'].' '.$value['approved_time']; ?></td>
              <td><?=$status_full_name; ?></td>
              <td>
                <a href="<?=$relation_trn_val?>" class="" data-toggle="tooltip" data-placement="top" title="Transaction History"><i class="fa fa-money" style="font-size:18px"></i></a>&nbsp;&nbsp;
                <button class="clean-button" onclick="relation_group_dalete(this.id)" id="<?php echo $value['user_relation_id']; ?>"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
              </td>
            </tr>
            <?php $x++; }} ?>
          </tbody>
        </table>
      </div><!-- table-wrapper -->
    </div>
  </div>
  <?php include"all_nav/footer.php"; ?>
<script type="text/javascript">
  var userUrl = "<?php echo USER_URL; ?>";
  var user_admin_id = "<?php echo $user_admin_id; ?>";
  var token = "<?php echo $token; ?>";
  var mobile = "<?php echo $user_mobile; ?>";
  var user_apar_id = "<?php echo $user_apar_id; ?>";
  $( document ).ready( function () {  
    $('#add_group').click(function () {
      var user_relation_name=$('#user_relation_name').val();
      var user_relation_mobile=$('#user_relation_mobile').val();
      var willing_to_rent_out=$('#willing_to_rent_out').val();
      var rent_argument_month=$('#rent_argument_month').val();
      var elect_and_water_charge=$('#elect_and_water_charge').val();
      var user_relation_agreement_amount=$('#user_relation_agreement_amount').val();
      var security_deposit_amount=$('#security_deposit_amount').val();
      var user_relation_agreement_start_date=$('#user_relation_agreement_start_date').val();
      var user_relation_agreement_end_date=$('#user_relation_agreement_end_date').val();
      if($("#addGroup").valid()){
        // alert('ok');
        $(this).val('Wait ...').prop('disabled', true);
        var user_relation = userUrl+'user_to_user_relation_add.php';
        //'user_admin_id','mobile','token','user_relation_mobile','user_relation_name','user_relation_agreement_start_date','user_relation_agreement_end_date'
        $.ajax({
          url :user_relation,
          type:'POST',
          data :{
            'user_admin_id':user_admin_id,
            'mobile':mobile,
            'token':token,
            'user_apar_id':user_apar_id,
            'user_relation_mobile':user_relation_mobile,
            'user_relation_name':user_relation_name,
            'willing_to_rent_out':willing_to_rent_out,
            'rent_argument_month':rent_argument_month,
            'elect_and_water_charge':elect_and_water_charge,
            'user_relation_agreement_amount':user_relation_agreement_amount,
            'user_relation_agreement_start_date':user_relation_agreement_start_date,
            'user_relation_agreement_end_date':user_relation_agreement_end_date,
            'security_deposit_amount':security_deposit_amount
          },
          dataType:'html',
          success  :function(data){
            // alert(data);
            $('#add_group').val('SAVE').prop('disabled', false);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.9em;'>Successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                      location.reload();

                  }
                }
              });
            }else{
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
                type: 'red'
              });
              if (json.session==0) {
                window.location.replace("logout.php");
              }
            }
          }
        });
      }
    });
    $( "#addGroup" ).validate( {
      rules: {
        user_relation_name: "required",
        user_relation_mobile: {
          required: true,
          number: true,
          minlength: 10,
          maxlength: 10
        },
        willing_to_rent_out: "required",
        rent_argument_month: {
          required: true,
          number: true,
          min: 1,
          max: 36
        },
        elect_and_water_charge: "required",
        security_deposit_amount: {
          required: true,
          number: true
        },
        user_relation_agreement_amount: {
          required: true,
          number: true
        },
        user_relation_agreement_start_date: "required"
      },
      messages: {
        user_relation_mobile: {
          required: 'This field is required.',
          number: 'Enter valid Number.',
          minlength: 'Minimum Length 10th Digit.',
          maxlength: 'Maximum  Length 10th Digit.'
        },
        rent_argument_month: {
          required: 'This field is required.',
          number: 'Enter valid Amount.',
          min:"Minimum month 1.",
          max:"Maximum month 36."
        },
        security_deposit_amount: {
          required: 'This field is required.',
          number: 'Enter valid Amount.'
        }
      }
    });
  });
  function relation_group_dalete(user_relation_id){
    // alert(user_incident_id);
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.9em;'>It will delete the item permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            var user_relation_delete = userUrl+'user_to_user_relation_delete.php';
            // 'user_admin_id','mobile','token','user_apar_id','user_relation_id'
            $.ajax({
              url :user_relation_delete,
              type:'POST',
              data :{
                'user_admin_id':user_admin_id,
                'mobile':mobile,
                'token':token,
                'user_apar_id':user_apar_id,
                'user_relation_id':user_relation_id
              },
              dataType:'html',
              success  :function(data){
                // alert(data);
                var json = $.parseJSON(data);
                if (json.status){
                  $.alert({
                    icon: 'fa fa-smile-o',
                    theme: 'modern',
                    title: 'Success !',
                    content: "<p style='font-size:0.9em;'>Delete Successfully</p>",
                    type: 'green',
                    buttons: {
                      Ok: function () {
                          location.reload();
                      }
                    }
                  });
                }else{
                  $.alert({
                    icon: 'fa fa-frown-o',
                    theme: 'modern',
                    title: 'Error !',
                    content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
                    type: 'red'
                  });
                  if(json.session==0) {
                    window.location.replace("logout.php");
                  }
                }
              }
            });
          }
        }
      }
    });
  }
</script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#user_relation_agreement_start_date', {
      default_date   : false,
      // format  : 'Y-m-d',
      // position       : 'top',
      hide_on_select : true,
      render : function (date) {
        // if (date < now) {
            return {
              disabled : false,
              class_name : 'date-in-past'
            };
        // }
        return {};
      } 
    });
  });
  $('#user_relation_agreement_end_date').focus(function () {
    var start = new Date;
    var end = $('#user_relation_agreement_start_date').val();
    //alert(end);
    var d=new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var newdate=yy+"/"+mm+"/"+dd;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24);
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    //delete days;
    pickmeup('#user_relation_agreement_end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
        if (date < now_end) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        return {};
      } 
    });
  });
  $('#user_relation_agreement_start_date').focus(function(){
    $('#user_relation_agreement_end_date').val('');
  });
  // ------------------Date---------------
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>
