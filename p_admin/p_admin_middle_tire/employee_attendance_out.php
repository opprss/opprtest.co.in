<?php 
/*
Description: parking employ out vehicle
Developed by: Rakhal Raj Mandal
Created Date: 06-04-2018
Update date : 25-05-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
$re = array();
$resp = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','parking_admin_name','token'))){
 	if(isEmpty(array('prk_admin_id','prk_area_user_id','parking_admin_name','token'))){
 		
	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_area_user_id = base64_encode(trim($_POST['prk_area_user_id']));
	 	$parking_admin_name = trim($_POST['parking_admin_name']);
        $token = ($_POST['token']);
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){	
        	
	        $response = employee_attendance_out($prk_admin_id,$prk_area_user_id,$parking_admin_name);
        }else{
            $response = $resp;
        }
 	}else{
        $response['status'] = 0;
    	$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>