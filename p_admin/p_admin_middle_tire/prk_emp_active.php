<?php 
/*
Description: parking area  employ active and diactive.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// include_once 'api/global_api.php';

$prk_admin_id = trim($_GET['prk_admin_id']);
$prk_area_user_id = trim($_GET['prk_area_user_id']);
$prk_admin_name = trim($_GET['prk_admin_name']);
$active = trim($_GET['active']);

 $response = array();

 if(isset($prk_admin_id) && isset($prk_area_user_id) && isset($prk_admin_name)&& isset($active)){
 
        $response= prk_emp_active($prk_admin_id,$prk_area_user_id,$prk_admin_name,$active);
        header('Location: ' . $_SERVER['HTTP_REFERER']);
 	
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response);
 }
?>