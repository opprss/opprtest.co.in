<?php
/*
Description: Parking area account status check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_employee_from_permition($prk_admin_id,$user_name,$form_id,$inserted_by){
    global $pdoconn;
    $sql ="SELECT * FROM `prk_user_permission` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag` = '".FLAG_Y."' AND `del_flag` = '".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        // $sql = "DELETE FROM permission WHERE `user_name`='".$user_hide_id."'";   
        $sql = "DELETE FROM prk_user_permission WHERE `prk_admin_id`='$prk_admin_id'";   
        $pdoconn->exec($sql);
        $response = permission_emp($prk_admin_id,$user_name,$form_id,$inserted_by);
    }else{

        $response = permission_emp($prk_admin_id,$user_name,$form_id,$inserted_by);
    }
    return $response; 
}
function permission_emp($prk_admin_id,$user_name,$form_id,$inserted_by){
    $response = array();
    global $pdoconn;
    $prk_area_user_id=NULL;
    $user_name=NULL;
    foreach($form_id as $val){
        $sql="INSERT INTO `prk_user_permission`(`prk_admin_id`, `prk_area_user_id`, `user_name`, `form_id`, `inserted_by`, `inserted_date`) VALUES ('$prk_admin_id','$prk_area_user_id','$user_name','$val','$inserted_by','".TIME."')";

        // $sql = "INSERT INTO `prk_admin_permission`(`prk_admin_id`,`user_name`,`form_id`,`inserted_by`,`inserted_date`) VALUE('$prk_admin_id','$user_name','$val','$inserted_by','".TIME."')";
        $sqls=$pdoconn->prepare($sql);
       if($sqls->execute())
       {
            $response['status'] = 1;
            $response['message'] = 'Permission Save';
       }else{
            $response['status'] = 0;
            $response['message'] = 'Permission Not Save ';
        }
    }
    return json_encode($response); 
}
?>