<?php 
/*
Description: Parking gate update.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','token','booking_date'))){
 	if(isEmpty(array('prk_admin_id','token'))){
	 	$prk_admin_id = trim($_POST['prk_admin_id']);
        $token = trim($_POST['token']);
        $booking_date = trim($_POST['booking_date']);
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            $response= vehicle_booking_dashboard($prk_admin_id,$booking_date);
        }else{
            $response = $resp;
        }
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo ($response); 
?>