<?php
/*
Description: Parking area vehicle all rate dtl list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
// parking_admin_name, owner_id & tenant_id not use.
function maintenance_tran_late_fine($prk_admin_id,$year,$form_month,$previous_balance,$current_due){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $allplayerda = array();
    $park = array();
    $resp = array();
    $current_date=TIME_TRN;
    $prk_in =prk_inf($prk_admin_id);
    $json = json_decode($prk_in);
    if($json->status=='1'){
        $date = $year.'-'.$form_month;
        $last_due_day=$json->last_due_day;
        $last_due_day = sprintf("%'.02d", $last_due_day);
        $choose_due_day=$json->choose_due_day;
        $late_fine_payable_time=$json->late_fine_payable_time;
        $late_fine_charrge=$json->late_fine_charrge;
        $late_fine_rate_type=$json->late_fine_rate_type;
        $prk_area_name=$json->prk_area_name;
        $state_mant_date=$json->state_mant_date;
        $state_mant_date = sprintf("%'.02d", $state_mant_date);
        $previous_bal = ($previous_balance>0)?$previous_balance:0;
        if ($late_fine_rate_type=='P') {
            $fine_charges=$previous_bal*($late_fine_charrge/100);
            $total_pay_before_due_date=($previous_balance+$current_due+$fine_charges);
            $after_fine_charges=($total_pay_before_due_date*($late_fine_charrge/100));
            $total_pay_after_due_date=$total_pay_before_due_date+$after_fine_charges;
        }else{
            $fine_charges=0;
            $total_pay_before_due_date=($previous_balance+$current_due+$fine_charges);
            $after_fine_charges=$late_fine_charrge;
            $total_pay_after_due_date=$total_pay_before_due_date+$after_fine_charges;
        }
        if ($last_due_day!=FLAG_Y) {

            $bill_due_date = $date.'-'.$choose_due_day;
        }else{
            $a_date = $date.'-1';
            $bill_due_date = date("Y-m-t", strtotime($a_date));
        }
        $state_mant_date = $date.'-'.$state_mant_date;
        $response['status'] = 1;
        $response['message'] = 'Successfully';
        $response['prk_area_name'] = $prk_area_name; 
        $response['fine_charges'] = $fine_charges; 
        $response['statement_date'] = $state_mant_date; 
        $response['bill_due_date'] = $bill_due_date; 
        $response['total_pay_before_due_date'] = $total_pay_before_due_date; 
        $response['after_fine_charges'] = $after_fine_charges; 
        $response['total_pay_after_due_date'] = $total_pay_after_due_date; 
        $response['late_fine_charrge'] = $late_fine_charrge; 
        $response['late_fine_rate_type'] = $late_fine_rate_type; 
    }else{
        $response['status'] = 0;
        $response['message'] = 'Setup Not Completed';
    }
    return json_encode($response);
}
?>