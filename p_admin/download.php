<!-- ################################################
  
Description: parking area admin can contact with oppr team with the following contact form.
Developed by: Manranjan sarkar & Soemen Banerjee
Created Date: 29-03-2018

 ####################################################-->
<?php include "all_nav/header.php";  ?>
<style type="text/css">
  .bg-info{
    background-color: #2F4F4F !important;
  }
  .card{
    min-height: 180px !important;
  }
</style>
 <div class="am-mainpanel"><!-- closing in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Download</h5>
        <!--<form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
  


      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">We have following reports for you.</h6>
          <!-- <p class="mg-b-20 mg-sm-b-30">Add an optional header within a card.</p> -->

          <div class="row">
            
            <div class="col-md-4 mg-t-20">
              <div class="card bd-0">
                <div class="card-header card-header-default bg-info">
                  QR Code
                </div><!-- card-header -->
                <div class="card-body bd bd-t-0">
                  <p class="mg-b-0">OPPR is a fully digitize parking solution. Whether you are a vehicle owner or parking space provider... <a href="<?php echo PRK_BASE_URL.'qr-code-page'?>" target="_BLANK">View</a></p>
                </div><!-- card-body -->
              </div><!-- card -->
            </div><!-- col -->

            <div class="col-md-4 mg-t-20">
              <div class="card bd-0">
                <div class="card-header card-header-default bg-info">
                  Vehicle Transaction
                </div><!-- card-header -->
                <div class="card-body bd bd-t-0">
                  <p class="mg-b-0">We will love to provide you with more information, answers any questions you may have arise and lead you into the feature... <a href="<?php echo PRK_BASE_URL.'transections'?>" target="_BLANK">View</a></p>
                </div><!-- card-body -->
              </div><!-- card -->
            </div><!-- col -->
            <div class="col-md-4 mg-t-20">
              <div class="card bd-0">
                <div class="card-header card-header-default bg-info">
                  Visitor Transaction
                </div><!-- card-header -->
                <div class="card-body bd bd-t-0">
                  <p class="mg-b-0">We will love to provide you with more information, answers any questions you may have arise and lead you into the feature... <a href="<?php echo PRK_BASE_URL.'visitor-transections'?>" target="_BLANK">View</a></p>
                </div><!-- card-body -->
              </div><!-- card -->
            </div><!-- col -->
            <div class="col-md-4 mg-t-20">
              <div class="card bd-0">
                <div class="card-header card-header-default bg-info">
                  Employee Transaction
                </div><!-- card-header -->
                <div class="card-body bd bd-t-0">
                  <p class="mg-b-0">We try to group all the questions you may have arise about our product in one place. Please explore... <a href="<?php echo PRK_BASE_URL.'employee-transections'?>" target="_BLANK">View</a></p>
                </div><!-- card-body -->
              </div><!-- card -->
            </div><!-- col -->
            <div class="col-md-4 mg-t-20">
              <div class="card bd-0">
                <div class="card-header card-header-default bg-info">
                  Parking Space
                </div><!-- card-header -->
                <div class="card-body bd bd-t-0">
                  <p class="mg-b-0">Comming Soon... <a href="<?php echo PRK_BASE_URL.'parking-space-details'?>" target="_BLANK">View</a></p>
                </div><!-- card-body -->
              </div><!-- card -->
            </div><!-- col -->   
          </div><!-- row -->
        </div><!-- card -->
      </div>

<?php include"all_nav/footer.php"; ?>
</style>




