<?php
/*
Description: Parking area gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function visitor_gate_pass_work_dtl($number,$prk_admin_id,$visitor_gate_pass_dtl_id,$tower_name,$falt_name,$start_date,$end_date1,$remark,$inserted_by){
    global $pdoconn;
    $response = array();

    for($i=0; $i<$number; $i++) {   
        $falt_name=$falt_name[$i];
        $tower_name=$tower_name[$i];
        $start_date=$start_date[$i];
        $end_date1=$end_date1[$i];
        $remark=$remark[$i];
        $sql = "INSERT INTO `visitor_service_dtl` (`visitor_gate_pass_dtl_id`,`prk_admin_id`,`tower_name`,`falt_name`,`in_time`,`out_time`,`visitor_remark`,`inserted_by`,`inserted_date`) VALUES ('$visitor_gate_pass_dtl_id','$prk_admin_id', '$tower_name','$falt_name','$start_date','$end_date1','$remark','$inserted_by','".TIME."')";
        $query = $pdoconn->prepare($sql);
        if($query->execute()){
            // $response['status'] = 1;
            // $response['message'] = 'Gate Pass Issue Successful';
            // $response = json_encode($response);

        }else{
            // $response['status'] = 0;
            // $response['message'] = 'Gate Pass Issue Not Successful';
            // $response = json_encode($response);
        }
    }  
    $response['status'] = 1;
    $response['message'] = 'Gate Pass Issue Successful';
    $response = json_encode($response);
    return json_encode($response);
}
?>
