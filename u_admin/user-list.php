<!-- ################################################
Description: this is user home page after login user redirectd to this web page, here will show all the list of user's vehicle with all details.
Developed by: Rakhal Raj Mandal
Created Date: 17-03-2018/25-Feb-2020
#################################################### -->
<?php 
  include "all_nav/header.php";
  $user_mobile = $_SESSION['mobile'];
  $token = $_SESSION["token"];
  $user_id = $_SESSION['user_admin_id'];
?>
<link href="../lib/highlightjs/github.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../lib/select2/css/select2.min.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Visitor List</h5>
  </div>
  <div class="am-pagebody">
    <div class="card single pd-10 pd-sm-20">          
      <div class="table-wrapper">
        <table id="datatable1" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="wd-10p">Image</th>
              <th class="wd-15p">Name</th>
              <th class="wd-15p">Address</th>
              <th class="wd-10p">Purpase</th>
              <th class="wd-10p">Premises Name</th>
              <!-- <th class="wd-10p">No. OF Guest</th> -->
              <th class="wd-10p">IN Date</th>
              <th class="wd-10p">IN Time</th>
              <th class="wd-10p">In Verify</th>
              <th class="wd-10p">Out Verify</th>
            </tr>
          </thead>
          <tbody id="visitor_in_data"></tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include "all_nav/footer.php"; ?>
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="../lib/datatables-responsive/jquery.dataTables.min.js"></script>
<script src="../lib/datatables-responsive/dataTables.buttons.min.js"></script>
<script src="../lib/datatables-responsive/jszip.min.js"></script>
<script src="../lib/datatables-responsive/pdfmake.min.js"></script>
<script src="../lib/datatables-responsive/vfs_fonts.js"></script>
<script src="../lib/datatables-responsive/buttons.html5.min.js"></script>
<script type="text/javascript">
  var user_mobile = "<?php echo $user_mobile; ?>";
  var user_id = "<?php echo $user_id; ?>";
  var token = "<?php echo $token; ?>";
  var userUrl = "<?php echo USER_URL; ?>";
  $(document).ready(function(){
    visitorVisitList();
  });
  function out_verify_user(str) {
    var prk_visit_dtl_id = str;
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>Allow this user</p>",
      theme: 'modern',
      type: 'green',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Allow',
          btnClass: 'btn-green',
          keys: ['Y', 'shift'],
          action: function(){
            if (prk_visit_dtl_id != '') {
              var urlDtl = userUrl+'visitor_visit_out_verify.php';
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :{
                  'prk_visit_dtl_id':prk_visit_dtl_id,
                  'user_mobile':user_mobile,
                  'user_admin_id':user_id,
                  'token':token
                },
                dataType:'html',
                success  :function(data){
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success !',
                      content: "<p style='font-size:0.9em;'>Visitor out successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                          visitorVisitList();
                          // location.reload();
                        }
                      }
                    });
                  }else{
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
  function in_verify_user(str) {
    var prk_visit_dtl_id = str;
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>Allow this user</p>",
      theme: 'modern',
      type: 'green',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Allow',
          btnClass: 'btn-green',
          keys: ['Y', 'shift'],
          action: function(){
            if (prk_visit_dtl_id != '') {
              var urlDtl = userUrl+'visitor_visit_in_verify.php';
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :{
                  'prk_visit_dtl_id':prk_visit_dtl_id,
                  'user_mobile':user_mobile,
                  'user_admin_id':user_id,
                  'token':token
                },
                dataType:'html',
                success  :function(data){
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success !',
                      content: "<p style='font-size:0.9em;'>Visitor in successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                          visitorVisitList();
                          // location.reload();
                        }
                      }
                    });
                  }else{
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
  function visitorVisitList(){
    $('#refresh_button').show();
    var visitor_in_list = userUrl+'user_visitor_in_list.php';
    // 'user_mobile','user_admin_id','token'
    $.ajax({
      url :visitor_in_list,
      type:'POST',
      data : {
        'user_mobile':user_mobile,
        'user_admin_id':user_id,
        'token':token
      },
      dataType:'html',
      success  :function(data){
        //alert(data);
        var demoLines = '';
        var json = $.parseJSON(data);
        if (json.status){
          for (var key in json.visitor_list) {
            var keyy = json.visitor_list[key];
            if(keyy['visit_img']==''){
              if(keyy['visit_gender']=='M'){
                var img_url= "<?php echo MALE_IMG_D; ?>";
              }else{
                var img_url= "<?php echo FEMALE_IMG_D; ?>";
              }
            }else{

              var img_url= keyy['visit_img'];
            }
            demoLines += '<tr>\
            <td><img src="'+img_url+'" style="height: 40px; border-radius: 50%;"></td>\
            <td>'+keyy['visit_name']+'</td>\
            <td>'+keyy['visit_address']+'</td>\
            <td>'+keyy['visit_purpose']+'</td>\
            <td>'+keyy['prk_area_name']+'</td>\
            <td>'+keyy['visit_in_date']+'</td>\
            <td>'+keyy['visit_in_time']+'</td><td>';

            if(keyy['visitor_in_verify_flag']=='D'){
              demoLines +='<button type="button" class="activate pd-l-10 btn btn-link" id="'+keyy['prk_visit_dtl_id']+'" onclick="in_verify_user(this.id)" data-toggle="tooltip" data-placement="top"><i class="fa fa-check" id="in_verify_logo" style="font-size:32px;color:green;margin-top:-14px;"></i></button>'; 
            }else{
              demoLines +='<button style="border: none;" type="button" class="activate pd-l-10 btn btn-link" id="'+keyy['prk_visit_dtl_id']+'" onclick="in_verify_user(this.id)" data-toggle="tooltip" data-placement="top" disabled><i class="fa fa-check" id="in_verify_logo" style="font-size:32px;color:grey;margin-top:-14px;"></i></button>'; 
            }
            demoLines +='</td><td>';
            if(keyy['visitor_in_verify_flag']=='D' || keyy['visitor_out_verify_flag']=='Y'){
              demoLines +='<button style="border: none;" type="button" class="activate pd-l-10 btn btn-link" id="'+keyy['prk_visit_dtl_id']+'" onclick="out_verify_user(this.id)" data-toggle="tooltip" data-placement="top" disabled><i class="fa fa-check" id="in_verify_logo" style="font-size:32px;color:grey;margin-top:-14px;"></i></button>'; 
            }else{
              demoLines +='<button type="button" class="activate pd-l-10 btn btn-link" id="'+keyy['prk_visit_dtl_id']+'" onclick="out_verify_user(this.id)" data-toggle="tooltip" data-placement="top"><i class="fa fa-check" id="in_verify_logo" style="font-size:32px;color:green;margin-top:-14px;"></i></button>'; 
            }
            demoLines +='</td></tr>';
          }
        }
        $('#datatable1').dataTable().fnDestroy();
        $("#visitor_in_data").html(demoLines);
        datatable_show();
      }
    });
  }
  function datatable_show(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      language: {
      searchPlaceholder: 'Search Here',
      sSearch: '',
      lengthMenu: '_MENU_ Page',
      },
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      buttons: [
        {
          text: '<span id="refresh_button"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
          action: function ( e, dt, node, config ) {
              // alert( 'Button activated' );
              visitorVisitList();
          }
        },
        { 
          extend: 'copyHtml5', 
          //footer: true,
          title: 'VISITOR SHEET' 
        },
        { 
          extend: 'excelHtml5', 
          //footer: true,
          title: 'VISITOR SHEET'  
        },
        {
          extend: 'pdfHtml5',
          //footer: true,
          title: 'VISITOR SHEET',
          customize: function(doc) {
            doc.content[1].margin = [ 60, 0, 0, 0 ]
          }
        },
      ]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    $('#refresh_button').hide();
  }
</script>
      