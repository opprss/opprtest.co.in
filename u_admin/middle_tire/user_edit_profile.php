<?php 
/*
Description: user edit profile.
Developed by: Rakhal Raj Mandal
Created Date: 15-01-2018
Update date : 06-05-2018
*/
 require_once 'api/reg_api.php';
// include_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('user_admin_id','user_name','user_nick_name','user_gender','user_dob','user_email','user_email_verify','user_aadhar_id','mobile'))){
 	if(isEmpty(array('user_admin_id','user_name','mobile'))){

	 	$user_admin_id = trim($_POST['user_admin_id']);
	 	$user_name = trim($_POST['user_name']);
	        $user_nick_name = trim($_POST['user_nick_name']);
	        $user_gender = trim($_POST['user_gender']);
	        $user_dob = trim($_POST['user_dob']);
        	$user_email = strtolower(trim($_POST['user_email']));
	        //$user_email = trim($_POST['user_email']);
	        $user_email_verify = trim($_POST['user_email_verify']);
	        $user_aadhar_id = trim($_POST['user_aadhar_id']);
	        $mobile = trim($_POST['mobile']);
	        
	        if(!empty($user_email)){
	             if(emailChecker($user_email)=='true'){
	                $response=user_edit_profile($user_admin_id,$user_name,$user_nick_name,$user_gender,$user_dob,$user_email,$user_email_verify,$user_aadhar_id,$mobile);
	             echo ($response);
	             }else{
	                $response['status'] = 0;
	                $response['message'] = 'Please enter a valid email';
	                echo json_encode($response);  
	             }
	        }else{
	            $response=user_edit_profile($user_admin_id,$user_name,$user_nick_name,$user_gender,$user_dob,$user_email,$user_email_verify,$user_aadhar_id,$mobile);
	             echo ($response);
	        }
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?>