<!-- ################################################
Description: QR Code
Developed by: Rakhal Raj Mandal
Created Date: 27-08-2019
####################################################-->
<?php include "all_nav/header.php";?>
<style type="text/css">
  .size{
    font-size: 11px;
    text-transform: uppercase;
  }
  .error_size{
    font-size: 11px;
    color: red;
  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
</style> 
<div class="am-mainpanel"><!-- cloding in footer -->
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">QR Code<?//=$prk_area_name?></h5>
  </div><!-- am-pagetitle -->
</div>
<div class="am-mainpanel">
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40" style="padding: 10px;">
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <form id="duplicatePayRecForm" action="" method="post">
          <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
              <p style="text-align: center;"><b><?=$prk_area_name?></br>
                <?=$city_name.', '.$state_name?></br>
                Show this QR code for <?=$gate_type?> Gate
              </b></p>
            </div>
            <div class="col-lg-2"></div>
          </div>
          <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
              <div class="form-group">
                <center>
                <?php 
                  $qrcode=$prk_qr_code;
                  // $code = '<img src="https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl='.$qrcode.'" title="Link to Google.com" style="height: 350px;">';
                  $code = '<img src="./../../library/QRCodeGenerate/generate.php?text='.$qrcode.'" title="OPPRSS" style="height: 350px;">';
                  echo $code;
                ?>
                </center>
            </div>
            </div>
            <div class="col-lg-2"></div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php include"all_nav/footer.php"; ?>
  