<?php
/*
Description: Parking adimn contect us.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
    // require_once '../../connection-pdo.php';
    require_once 'api/parkAreaReg_api.php';
    // require_once 'api/global_api.php';
    $response = array();
    if(isAvailable(array('name', 'email', 'phone', 'subject', 'message', 'inserted_by'))){
        if(isEmpty(array('name', 'email', 'phone', 'subject', 'message', 'inserted_by'))){

            $name = trim($_POST['name']);
            $email = trim($_POST['email']);
            $phone = trim($_POST['phone']);
            $subject = trim($_POST['subject']);
            $message = trim($_POST['message']);
            $inserted_by = trim($_POST['inserted_by']);

            $response = contect_us_prk($name, $email, $phone, $subject, $message, $inserted_by);
            echo $response;

        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response); 
         }

     }else{
         $response['status'] = 0; 
         $response['message'] = 'Invalid API Call';
         echo json_encode($response); 
     } 
?>