<!-- ################################################
  
Description: It will dispaly list of all current vehicle availabe in the parking area
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
  include "all_nav/header.php"; 
  if (isset($_SESSION['prk_admin_id'])) {
    $insert_by=$_SESSION['parking_admin_name'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
  }
  $response = prk_inf($prk_admin_id);
  $asas = $response;
  $response = json_decode($response, true);
  $advance_flag = $response['advance_flag'];
  $advance_pay = $response['advance_pay'];
  $print_in_flag = $response['prient_in_flag'];
  $print_out_flag = $response['prient_out_flag'];
  $veh_out_otp_flag = $response['veh_out_otp_flag'];
  $veh_token_flag = $response['veh_token_flag'];
?>
<link href="../lib/highlightjs/github.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../lib/select2/css/select2.min.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Current Vehicle</h5>
  </div>
</div>
<div class="am-mainpanel">
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40" style="padding: 10px;">
      <div class="table-wrapper mg-t-15">
        <table id="datatable1" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="wd-5p">Type</th>
              <th class="wd-10p">VEHICLE NUMBER</th>
              <th class="wd-10p">Token</th>
              <th class="wd-10p">In date</th>
              <th class="wd-10p">In Time</th>
              <th class="wd-10p text-center">Action</th>
            </tr>
          </thead>
          <tbody id="list_ve">
          </tbody>
        </table>
      </div>
    </div>
<?php include"all_nav/footer.php"; ?>
<!-- scripts for datatable -->
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="../lib/datatables-responsive/jquery.dataTables.min.js"></script>
<script src="../lib/datatables-responsive/dataTables.buttons.min.js"></script>
<script src="../lib/datatables-responsive/jszip.min.js"></script>
<script src="../lib/datatables-responsive/pdfmake.min.js"></script>
<script src="../lib/datatables-responsive/vfs_fonts.js"></script>
<script src="../lib/datatables-responsive/buttons.html5.min.js"></script>
<script>
  var prkUrl = "<?php echo PRK_URL; ?>";
  var prk_admin_id = "<?php echo $prk_admin_id; ?>";
  $(document).ready(function(){
    // alert(prkUrl);
    current_veh_loadAjax();
    /*setInterval(function(){
      var input = $('#myInput').val();
      if(input==''){
        // searching();
        current_veh_loadAjax();
      }
    }, 1000);*/
  });
  function current_veh_loadAjax(){
    $('#refresh_button').show();
    var current_veh = prkUrl+'prk_veh_trc_dtl_list.php';
    $.ajax({
      url :current_veh,
      type:'POST',
      data :
      {
        'prk_admin_id':prk_admin_id
      },
      dataType:'html',
      success  :function(data){
        // alert(data);
        var demoLines = '';
        var json = $.parseJSON(data);
        if (json.status){
          $('#datatable1').dataTable().fnDestroy();
          var c = 0;
          for (var key in json.parking) {
            c +=1;
            var keyy = json.parking[key];
            var img_url= "<?php echo OPPR_BASE_URL; ?>"+keyy['vehicle_typ_img'];
            demoLines += '<tr>\
              <td> <img src="'+img_url+'" style="height: 40px;"></td>\
              <td>'+keyy['veh_number']+'</td>\
              <td>'+keyy['veh_token']+'</td>\
              <td>'+keyy['prk_veh_in_date']+'</td>\
              <td>'+keyy['prk_veh_in_time']+'</td>\
              <td class="text-center">';
            if(keyy['veh_out_verify_flag'] == 'Y'){
              var dis_val = 'Y';
            }else{
              var dis_val = 'N';
            }
            if(dis_val == 'N'){
              demoLines +='<button style="border: none;" type="button" class="activate pd-l-10 btn btn-link " id="'+keyy['prk_veh_trc_dtl_id']+'" onclick="cu_veh_out(this.id)" data-toggle="tooltip" data-placement="top" ><i class="fa fa-check" id="in_verify_logo" style="font-size:32px;color:green;margin-top:-14px;"></i></button>';
            }else{
              demoLines +='<button style="border: none;" type="button" class="activate pd-l-10 btn btn-link" id="'+keyy['prk_veh_trc_dtl_id']+'" onclick="cu_veh_out(this.id)" data-toggle="tooltip" data-placement="top" disabled><i class="fa fa-check" id="in_verify_logo" style="font-size:32px;color:grey;margin-top:-14px;"></i></button>'; 
            }
            demoLines += '</td>\
              </tr>';
          }
        }
        $("#list_ve").html(demoLines);
        datatable_show();
      }
    });
  }
  function cu_veh_out(prk_veh_trc_dtl_id){
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will Out Verify Vehicle</p>",
      theme: 'modern',
      type: 'green',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Verify',
          btnClass: 'btn-green',
          keys: ['Y', 'shift'],
          action: function(){
            // alert(prk_veh_trc_dtl_id);
            if (prk_veh_trc_dtl_id != '') {
              var urlDtl = prkUrl+'veh_out_verify.php';
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :
                {
                  'prk_veh_trc_dtl_id':prk_veh_trc_dtl_id,
                  'prk_admin_id':prk_admin_id,
                },
                dataType:'html',
                success  :function(data)
                {
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    // location.reload(true);
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Vehicle Out Verify successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                            // location.reload(true);
                          current_veh_loadAjax();
                        }
                      }
                    });
                  }else{
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
  function datatable_show(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      language: {
      searchPlaceholder: 'Search Here',
      sSearch: '',
      lengthMenu: '_MENU_ Page',
      },
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      buttons: [
        {
          text: '<span id="refresh_button"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
          action: function ( e, dt, node, config ) {
              // alert( 'Button activated' );
              current_veh_loadAjax();
          }
        },
        { 
          extend: 'copyHtml5', 
          //footer: true,
          title: 'CURRENT VEHICLE SHEET' 
        },
        { 
          extend: 'excelHtml5', 
          //footer: true,
          title: 'CURRENT VEHICLE SHEET'  
        },
        {
          extend: 'pdfHtml5',
          //footer: true,
          title: 'CURRENT VEHICLE SHEET',
          customize: function(doc) {
            doc.content[1].margin = [ 60, 0, 0, 0 ]
          }
        },
      ]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    $('#refresh_button').hide();
  }
</script>
