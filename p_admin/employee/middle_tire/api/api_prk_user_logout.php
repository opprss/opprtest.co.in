<?php 
/*
Description: Parking employ logout.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function prk_user_logout($token){
    global $pdoconn;
    $sql ="UPDATE `prk_token` SET `active_flag`='".FLAG_N."', `end_date`='".TIME."' WHERE `token_value`='$token' and `active_flag`='".FLAG_Y."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Logout Sucessful';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Logout Unsucessful';
    }
   return json_encode($response); 
}
?>