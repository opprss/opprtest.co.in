<?php 
/*
Description: vehicle verify and data insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :26-04-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','prk_user_username','token'))){
    if(isEmpty(array('prk_admin_id','prk_area_user_id','prk_user_username','token'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);      
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $prk_user_username = trim($_POST['prk_user_username']);
        $token = trim($_POST['token']);
        $user_admin_id=$oth1=$oth2='';
        $token_check = prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($token_check);
        if($json->status=='1'){
            
            $response = emergency_list($prk_admin_id,$user_admin_id,$prk_area_user_id,$oth1,$oth2);
        }else{
            $response = $token_check;
        }   
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>