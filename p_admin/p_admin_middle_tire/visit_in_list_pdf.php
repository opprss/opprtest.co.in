<?php 
/*
Description: Parking visiter in list pdf
Developed by: Rakhal Raj Mandal
Created Date: 03-02-2019
Update date : 
*/ 
require_once 'api/parkAreaReg_api.php';
$previous=$_SERVER['HTTP_REFERER'];
$response = array();
if(isAvailable(array('prk_admin_id','start_date','end_date','token'))){
    if(isEmpty(array('prk_admin_id','token'))){            
        $prk_admin_id = trim($_POST['prk_admin_id']); 
        $start_date = trim($_POST['start_date']);
        $end_date = trim($_POST['end_date']);
        $visit_name = isset($_REQUEST['visit_name']) ? trim($_REQUEST['visit_name']) : ''; 
        $visit_mobile = isset($_REQUEST['visit_mobile']) ? trim($_REQUEST['visit_mobile']) : ''; 
        $tower_id = isset($_REQUEST['tower_id']) ? trim($_REQUEST['tower_id']) : ''; 
        $flat_id = isset($_REQUEST['flat_id']) ? trim($_REQUEST['flat_id']) : ''; 
        // $prk_admin_id = 6; 
        // $start_date = '';
        // $end_date = '';
        $token = ($_POST['token']);
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
          $response = visit_in_list($prk_admin_id,$start_date,$end_date,$visit_name,$visit_mobile,$tower_id,$flat_id);
          // echo $response;
          $res=json_decode($response,true);

            $html="<html>
                <head>
                <style type='text/css'>
                        @media print
                        {
                        @page {
                        margin-top: 0px;
                        margin-bottom: 0px;
                        font-size:10px;
                        margin-left:90px;
                        margin-right:94px;
                        size:landscape;
                        margin: 0;
                        }
                        body  {
                        padding-top: 3px;
                        padding-bottom: 3px ;
                        font-size:10px;
                        margin-left:3px;
                        margin-right:3px;
                        }
                        }
                        .tg  {border-spacing:0;width: 104%;margin-top: 5px;border:1px solid #fff;}
                        .tg1  {border-collapse:collapse;border-spacing:0;width: 100%;}
                        .tg td{font-family:Arial, sans-serif;font-size:10px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;border-color:black;}
                        .tg th{font-family:Arial, sans-serif;font-size:10px;font-weight:normal;padding:10px 20px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
                        .tg .tg-xldj{border-color:inherit;text-align:left;padding-left: 5px;}
                        .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top;}
                    </style>
                </head>
                <body>
                <table class='tg1'>
                  <tr>
                    <th class='tg-s268' colspan='1' style='width: 28%;'><img style='height: 70px;' src='./../../img/logo_pdf.png'></th>
                    <th class='tg-s268' colspan='5' style='font-weight: 100;font-size: 12px;width: 30%;'>".$res['prk_area_name']."<br>Parking Visitor Transaction</th>
                    <th class='tg-0lax' colspan='4' style='font-weight: 100;font-size: 12px;width: 30%;'>Start Date: ".$res['start_date']."<br>End Date: ".$res['end_date']."</th>
                  </tr>
                </table>
                <table class='tg'>
                    <tr style='border-bottom:1px solid #e4e0e0'>
                        <th class='tg-xldj'><b>Visitor Name</b></th>
                        <th class='tg-xldj'><b>Mobile Number</b></th>
                        <th class='tg-xldj'><b>Address</b></th>
                        <th class='tg-xldj'><b>Id Proof</b></th>
                        <th class='tg-xldj'><b>Purpose</b></th>
                        <th class='tg-0pky' style='padding-left: 5px;'><b>In Date</b></th>
                        <th class='tg-0pky' style='padding-left: 5px;'><b>Out Date</b></th>
                        <th class='tg-xldj'><b>Meet To Name</b></th>
                        <th class='tg-xldj'><b>Flat/Tower Name</b></th>
                        <th class='tg-xldj'><b>Vehicle Number</b></th>
                    </tr>";
                    $html3="";
                    foreach ($res['visiter'] as $key => $item){
                      foreach ($item as $key_p => $values) {
                            // echo $item['veh_number'];
                        $html1="<tr style='border-bottom:0px solid #e4e0e0'>
                                  <td class='tg-0pky' style='width:12%'>".$values['visit_name']."</td>
                                  <td class='tg-0pky' style='width:6%'>".$values['visit_mobile']."</td>
                                  <td class='tg-0pky' style='width:26%'>".$values['visit_address']."</td>
                                  <td class='tg-0pky' style='width:7%'>".$values['visit_card_id']."</td>
                                  <td class='tg-0pky' style='width:7%'>".$values['visit_purpose']."</td>
                                  <td class='tg-0pky' style='width:7%'>".$values['visiter_in_date']."<br>".$values['visiter_in_time']."</td>
                                  <td class='tg-0pky' style='width:8%'>".$values['visiter_out_date']."<br>".$values['visiter_out_time']."</td>
                                  <td class='tg-0pky' style='width:7%'>".$values['visitor_meet_to_name']."</td>
                                  <td class='tg-0pky' style='width:7%'>".$values['visitor_meet_to_address']."</td>
                                  <td class='tg-0pky' style='width:17%'>".$values['veh_number']."</td>
                                </tr>";
                        $html3.=$html1;
                      }
                    }
                    $html2="</table>
                </body>
                </html>";
            // echo ($mess);
            $html=$html."".$html3."".$html2;
            echo ($html);
        }
        else{
          echo $response=$resp;
        }
    }else{
      $response['status'] = 0;
      $response['message'] = 'All Fields Are Mandatory';
      echo json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response); 
} 
?> 
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous">
</script>
<script type="text/javascript">
    $(window).on('load', function() {
        window.print();
        setTimeout(function(){
          window.close();
          window.location.href='<?php echo $previous; ?>';
        }, 1);
    });
</script>