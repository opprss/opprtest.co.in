<?php 
/*
Description: parking area employ delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_area_user_delete($prk_admin_id,$prk_user_username){
    global $pdoconn;
    $response = array();
    $sql ="UPDATE `prk_area_user` SET `del_flag`='".FLAG_Y."', `updated_date`='".TIME."',`updated_by`='$prk_user_username' WHERE `prk_admin_id`='$prk_admin_id' AND `prk_user_username`='$prk_user_username' AND `del_flag`='".FLAG_N."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
         $response['status'] = 1;
        $response['message'] = 'Delete Sucessfull';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Delete Not Sucessfull';
        return json_encode($response);
    }
}
?>