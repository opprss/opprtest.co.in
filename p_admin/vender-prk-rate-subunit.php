<!-- ################################################
  
Description: Parking subunit can add/delete parking rate by this web page.
Developed by: Manoranjan
Created Date: 26-05-2018

 ####################################################-->
 <?php 
include "all_nav/header.php"; 
@session_start();
if (isset($_SESSION['prk_admin_id'])) {
  $parking_admin_name=$_SESSION['parking_admin_name'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
    }

?>
<style>
  .size{
    font-size: 11px;
  }
.success{
  font-size: 11px;
  color: green;
}
</style>
<!-- vendor css -->
   
    <link href="../lib/highlightjs/github.css" rel="stylesheet">
    <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">

    <div class="am-mainpanel">
      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Subunit Allocated</h5>
        <!--<form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>-->
      </div>

      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40" >
          <!-- <h6 class="card-body-title">Add Employee</h6> -->
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form>
            <div class="row mg-b-25">
              <div class="col-lg-4">
               <div class="form-group">
                <label class="form-control-label size">SUBUNIT NAME<span class="tx-danger">*</span>
                  </label>
                    <div class="select">
                      <select name="vehicle_type" id="subunit_name" value="" style="opacity: 0.8; font-size:14px">
                      </select>
                    </div>
                    <span id="err_subunit_name" style="position: absolute;" class="error_size"></span>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                
                
                 <div class="form-group">
                  <label class="form-control-label size">VEHICLE TYPE <span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="vehicle_type" id="vehicle_type" value="" style="opacity: 0.8; font-size:14px">
                      </select>
                    </div>
                    <span id="error_vehicle_type" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <!-- col-4 -->
              <div class="col-lg-4">
           

                <div class="form-group error_show">
                  <label class="form-control-label size">NO OF ALLOCATED SPACE<span class="tx-danger">*</span></label>
                    <!-- <div class="select"> -->
                      
                    <input type="text" name="representative" id="tot_allocated_space" class="form-control inputText" required placeholder="Enter No Of Allocated Space">
                    
                    <span id="error_allocated_space" style="position: absolute;" class="error_size"></span>
                </div>
              </div><!-- col-4 -->

             
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">EFFECTIVE DATE <span class="tx-danger">*</span></label>
                  <input type="text" class="form-control prevent_readonly" placeholder="DD/MM/YYYY (Effective Date)" name="dob" maxlength="10" id="start_date" readonly="readonly" style="background-color:transparent">
                  <!-- readonly="" style="background-color: white" -->
                  <span id="error_date" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">END DATE</label>
                  <input type="text"  name="end_date"  class="form-control e_date prevent_readonly" placeholder="DD/MM/YYYY (End Date)" name="prk_add_pin" id="end_date" readonly="readonly" style="background-color:transparent">
                  <span id="error_end_date" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
             
              <div class="col-lg-4">
                <div class="form-layout-footer mg-t-30">
                  <div class="row">
                    <div class="col-lg-6">
                      <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save" style="padding-bottom: 9px; padding-top: 9px">
                      <div style="position: absolute;" id="success_submit" class="success"></div>
                    </div>
                    <div class="col-lg-6">
                      <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        </div>
      </div>   

      <div class="am-pagebody">

        <div class="card pd-20 pd-sm-40">
          <!-- <h6 class="card-body-title">List of Sub Unit</h6> -->
          <!-- <p class="mg-b-20 mg-sm-b-30"></p> -->

          <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
                <tr>
                 
                  <th class="">SubUnit Name</th>
                  <th class="">Vehicle Type</th>
                  <th class="">Allocated Space</th>
                  <th class="">Effective Date</th>
                  <th class="">End Date</th>
                   <th class="">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                // $prk_admin_id='402';
           $respon=prk_sub_unit_veh_space_list($prk_admin_id);
            // print_r($respon);
           // echo  $prk_vehicle_type=$respon['prk_sub_unit_veh_space_id'];
           //$x=1;
              foreach($respon as $value){
               $prk_sub_unit_veh_space_id=$value['prk_sub_unit_veh_space_id'];
                 ?>

               <tr>
                 
                  <td><?php echo $value['prk_sub_unit_short_name']; ?></td>
                  <td><?php echo $value['veh_type']; ?></td>
                  <td><?php echo $value['veh_space']; ?></td>
                  <td><?php echo $value['prk_sub_unit_veh_space_eff_date']; ?></td>
                  <td><?php echo $value['prk_sub_unit_veh_space_end_date']; ?></td>
                  
                  <td>

            <a href="vender-prk-subunit-update?list_id=<?php echo base64_encode($value['prk_sub_unit_veh_space_id']); ?>" data-toggle="tooltip" data-placement="top" title="Modify"><!-- <button type="button" class="btn btn-info btn-md"> --><i class="fa fa-edit" style="font-size:18px"></i><!-- </button> --></a>

<!-- <a class="delete pd-l-10" href="<?php //echo PRK_URL ?>prk_sub_unit_veh_space_delete?prk_sub_unit_veh_space_id=<?php //echo $value['prk_sub_unit_veh_space_id'];?>&prk_admin_id=<?php //echo $value['prk_admin_id']; ?>&user_name=<?php //echo $parking_admin_name;?>" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></a> -->

  <button type="button" class="clean-button" name="delete" id="delete<%=count++%>" value="<?php echo $prk_sub_unit_veh_space_id; ?>" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
                 </td>
              </tr>
                <?php } ?>
         
                </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->

       
<!-- footer part -->
<?php include"all_nav/footer.php"; ?>
<style type="text/css">
  .error_size{
  font-size:11px;
  color: red;

}
</style>

<script type="text/javascript">
  $(document).ready(function(){
    var prkUrl = "<?php echo PRK_URL; ?>";
    var FUTURE_DATE = "<?php echo FUTURE_DATE_WEB ?>";

    /*reset button*/
    $('#reset').click(function(){
      $('.error_size').text('');
        $('input[name="initial_hr"]').val('');
        $('input[name="initial_hr"]').attr('readonly', false);
        $('input[name="per_hr_rate"]').val('');
        $('input[name="per_hr_rate"]').attr('readonly', false);
        $('input[name="prk_no_of_mt_dis"]').attr('readonly', false);
        $('#error_second_h').show();
        $('input[name="initial_hr"]').removeClass("readonly");
        $('input[name="per_hr_rate"]').removeClass("readonly");
        $('input[name="prk_no_of_mt_dis"]').removeClass("readonly");
      });
 

$('button[id^="delete"]').on('click', function() {
    //alert("hh");
    var prk_admin_id = "<?php echo $prk_admin_id;?>";
    var user_name='<?php echo $parking_admin_name; ?>';
    // var prk_sub_unit_veh_space_id='<?php //echo $prk_sub_unit_veh_space_id; ?>';
    var prk_sub_unit_veh_space_id = this.value;
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the item permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            if (prk_sub_unit_veh_space_id != '') {
              //alert(prk_rate_id);
              var urlDtl = prkUrl+'prk_sub_unit_veh_space_delete.php';
              //alert(urlDtl);
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :
                {
                  'prk_admin_id':prk_admin_id,
                  'prk_sub_unit_veh_space_id':prk_sub_unit_veh_space_id,
                  'user_name':user_name,
                  'token': '<?php echo $token;?>'
                },
                dataType:'html',
                success  :function(data)
                {
                  //alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    location.reload(true);
                  }/*else if(!json.session){
                    window.location.replace("logout.php");
                  }*/else{
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  });



    $('#save').click(function(){
      // var mm = $('#mm').val();
      // var length = mm.length;  
    
      var prk_sub_unit_veh_space_end_date=$('#end_date').val();
      // if(prk_sub_unit_veh_space_end_date==''){
        
      // }
      prk_sub_unit_veh_space_end_date = (!prk_sub_unit_veh_space_end_date == '') ? prk_sub_unit_veh_space_end_date : FUTURE_DATE;
      var veh_type =$("#vehicle_type").find("option:selected").val();          
      var prk_sub_unit_id =$("#subunit_name").find("option:selected").val();
      var veh_space= $('#tot_allocated_space').val();
      var prk_sub_unit_veh_space_eff_date = $('#start_date').val();
      var prk_admin_id=' <?php echo $prk_admin_id; ?>';
     var user_name=' <?php echo $parking_admin_name; ?>';
     // alert(prk_sub_unit_veh_space_end_date);prk_sub_unit_id
     // alert(prk_sub_unit_veh_space_end_date);
      if(veh_type!='' && prk_sub_unit_id!='' && tot_allocated_space!='' && prk_sub_unit_veh_space_eff_date!=''){

        var urlPrkAreRate = prkUrl+'prk_sub_unit_veh_space.php';
        $('#save').val('Wait ...').prop('disabled', true);
        $.ajax({
          url :urlPrkAreRate,
          type:'POST',
          data :
          {
            'prk_sub_unit_id':prk_sub_unit_id,
            'prk_admin_id':prk_admin_id,
            'veh_type':veh_type,
            'veh_space':veh_space,
            'prk_sub_unit_veh_space_eff_date':prk_sub_unit_veh_space_eff_date,
            'prk_sub_unit_veh_space_end_date':prk_sub_unit_veh_space_end_date,
            'user_name':user_name,
             'token': '<?php echo $token;?>'     
          },
          dataType:'html',
          success  :function(data)
          {
            $('#save').val('SAVE').prop('disabled', false);
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status){
                $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success',
                content: "<p style='font-size:0.9em;'>Subunit space allocated successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                      location.reload(true);
                  }
                }
              });
            }
            else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) { 
                     window.location.replace("logout.php");                    
                             }                  
                            }else{                    
                              $.alert({                    
                                icon: 'fa fa-frown-o',                    
                                theme: 'modern',                    
                                title: 'Error !',                    
                                content: "Somting went wrong",                    
                                type: 'red'                  
                              });                  
                            }                
                          }
          }
        });
      }
      else{if(prk_sub_unit_id==''){
      $('#err_subunit_name').text('Sub-unit name is required');
      }if(veh_type==''){
      $('#error_vehicle_type').text('Vehicle type is required');
      }if(veh_space==''){
      $('#error_allocated_space').text('Allocated space is required');
      }if(prk_sub_unit_veh_space_eff_date==''){
      $('#error_date').text('Effective date is required');
      }
      // $('#error_tot_prk_space').text('Totat parking space is required');
      }
    });
    // Initial no fo HR
    $("#initial_hr").blur(function () {
        var first_hour = $('#initial_hr').val();
        if(first_hour==''){
        $('#error_initial_hr').text('Initial no of hour is required');
        //$('#initial_hr').focus();
        return false
        }else{
          $('#error_initial_hr').text('');
        }
    });

    // validation date
    $("#start_date").blur(function () {
      var start_date = $('#start_date').val();
      if(start_date==''){
        $('#error_date').text('Effective date is required');
        //$('#start_date').focus();
        return false;
      }
      else{
        $('#error_date').text('');
      }
    });

    /*veh type*/
    $("#subunit_name").change(function () {
      var subunit_name =$("#subunit_name").find("option:selected").val(); 
      // alert(subunit_name);
      if(subunit_name==''){
        $('#err_subunit_name').text('Sub-unit name is required');
        return false;
      }else{
        $('#err_subunit_name').text('');
      }
    });

    $("#vehicle_type").change(function () {
        var vehicle_type =$("#vehicle_type").find("option:selected").val(); 
        // alert(subunit_name);
        if(vehicle_type==''){
          $('#error_vehicle_type').text('Vehicle type is required');
          return false;
        }else{
          $('#error_vehicle_type').text('');
        }
    });


    $("#tot_allocated_space").blur(function () {
        var tot_allocated_space = $('#tot_allocated_space').val();
        if(tot_allocated_space==''){
        $('#error_allocated_space').text('Vehicle allocated is required');
        //$('#tot_allocated_space').focus();
        return false;
        }else{
          $('#error_allocated_space').text('');
        }
    });
    
    // allow only numeric value for rat
    $("#tot_allocated_space").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0)){
        event.preventDefault();
      }
    });
    
    $("#state").blur(function(){
            var state = $('#state').val();
            //alert(state);
            /*var age=getAge(date)*/
            if(state == "")
            {
              $('#error_state').show();
              return false;
            }else{
                /*alert("yes");*/
                $('#error_state').hide();
                return true;
            }
    });

  /*});
      //add manoranjan
$(document).ready(function(){*/
      var url = prkUrl+'sub-unit-name.php';
      var prk_admin_id='<?php echo $prk_admin_id; ?>';
       $.ajax ({
          type: 'POST',
          url: url,
          data: {prk_admin_id:prk_admin_id},
          success : function(data) {
            // alert(data);
              var obj=JSON.parse(data);
             var areaOption = "<option value='' style='color:gray;' selected='selected'>SELECT SUBUNIT NAME</option>";
             $.each(obj, function(val, key) {
              
              areaOption += '<option value="' + key['prk_sub_unit_id'] + '">' + key['prk_sub_unit_name'] + '</option>'
             });

            $("#subunit_name").html(areaOption);

          }
        });
  });
 
  
       //ended manoranjan

      //select state
      $(document).ready(function(){
         var prkUrl = "<?php echo PRK_URL; ?>";
        var urlVehicleType = prkUrl+'vehicle_type.php';
// alert('mano');
       $.ajax ({
          type: 'POST',
          url: urlVehicleType,
          data: "",
          success : function(data) {
            // alert(data);
              var obj=JSON.parse(data);
             var areaOption = "<option value='' style='color:gray;' selected='selected'>SELECT VEHICLE TYPE</option>";
             $.each(obj, function(val, key) {
              // alert("Value is :" + key['e']);
              areaOption += '<option value="' + key['vehicle_Sort_nm'] + '">' + key['vehicle_type'] + '</option>'
             });

            $("#vehicle_type").html(areaOption);

          }
      });
    

       
//pin number validation only accept numeric value
$(function() {
  $('#staticParent').on('keydown', '#pin', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
})


     // });

/*date*/
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#start_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
            if (date < now) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            $('#error_date').text('');
            return {};
        } 
    });
  });

  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#start_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
            if (date < now_end) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            return {};
        } 
    });
  });

  $('#start_date').focus(function(){
    $('#end_date').val('');
  });
 });
    </script>

    <script src="../lib/highlightjs/highlight.pack.js"></script>
    <script src="../lib/datatables/jquery.dataTables.js"></script>
    <script src="../lib/datatables-responsive/dataTables.responsive.js"></script>

    <script>
      $(function(){
        'use strict';

        $('#datatable1').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search Here',
            sSearch: '',
            lengthMenu: '_MENU_ Page',
          }
        });

        $('#datatable2').DataTable({
          bLengthChange: false,
          searching: false,
           "scrollX": true
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

      });
    </script>
    <!-- for tool tipe -->
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });

  function upperCase(a){
      setTimeout(function(){
          a.value = a.value.toUpperCase();
      }, 1);
  }
</script>

<style type="text/css">
  .date{background-color: white}
</style>