<?php
session_start();
include "all_nav/header.php";
 $insert_by=$employee_user_name;
// $products = $_SESSION["products"];
?>
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="../../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
<div class="am-mainpanel"><!-- cloding in footer -->
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">ACTIVE OTP LIST</h5>
  </div>
</div>
<div class="am-mainpanel">
	<div class="am-pagebody">
        <div class="card pd-20 pd-sm-40">
          <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="wd-5p">Sl No.</th>
                  <th class="wd-5p">OTP</th>
                  <th class="wd-10p">Mobile</th>
                  <th class="wd-15p">Email</th>
                  <th class="wd-15p">Name</th>
                  <th class="wd-10p">Send Date</th>
                  <th class="wd-10p">Send Time</th>
                </tr>
              </thead>
              <tbody id="otp_list_table">
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->
    </div>
</div>   
<!-- datatable -->
<?php include"all_nav/footer.php"; ?>
<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="../../lib/datatables-responsive/jquery.dataTables.min.js"></script>
<script src="../../lib/datatables-responsive/dataTables.buttons.min.js"></script>
<script src="../../lib/datatables-responsive/jszip.min.js"></script>
<script src="../../lib/datatables-responsive/pdfmake.min.js"></script>
<script src="../../lib/datatables-responsive/vfs_fonts.js"></script>
<script src="../../lib/datatables-responsive/buttons.html5.min.js"></script>
<script type="text/javascript">
  var SUP_EMP_URL = "<?php echo SUP_EMP_URL ;?>";
  apart_balance_sheet();
  function apart_balance_sheet(){
    $('#refresh_button_table').show();
    var urlActiveOptList = SUP_EMP_URL+'active_otp_list.php';
    $.ajax({
      url :urlActiveOptList,
      type:'POST',
      data :{},
      dataType:'html',
      success  :function(data){
        // alert(data);
        var json = $.parseJSON(data);
        if (json.status){
          $('#datatable1').dataTable().fnDestroy();
          var demoLines="";
          var c = 0;
          for (var key in json.otp_list) {
            var keyy = json.otp_list[key];
            c +=1;
             demoLines += '<tr>\
              <td> '+c+'</td>\
              <td>'+keyy['otp']+'</td>\
              <td>'+keyy['mobile']+'</td>\
              <td>'+keyy['email']+'</td>\
              <td>'+keyy['name']+'</td>\
              <td>'+keyy['send_date']+'</td>\
              <td>'+keyy['send_time']+'</td></tr>';
          }
        }else{
         demoLines = '';
        }
        $("#otp_list_table").html(demoLines);
        datatable_show();
      }
    });
  }
  function datatable_show(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      language: {
      searchPlaceholder: 'Search Here',
      sSearch: '',
      lengthMenu: '_MENU_ Page',
      },
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      buttons: [
        {
          text: '<span id="refresh_button_table"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
          action: function ( e, dt, node, config ) {
              // alert( 'Button activated' );
              apart_balance_sheet();
          }
        },
        { 
          extend: 'copyHtml5', 
          //footer: true,
          title: 'OTP List' 
        },
        { 
          extend: 'excelHtml5', 
          //footer: true,
          title: 'OTP List'  
        },
        {
          extend: 'pdfHtml5',
          //footer: true,
          title: 'OTP List',
          customize: function(doc) {
            doc.content[1].margin = [ 60, 0, 0, 0 ]
          }
        },
      ]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    $('#refresh_button_table').hide();
  }
</script>