<?php
session_start();
include "all_nav/header.php";
 $insert_by=$employee_user_name;
// $products = $_SESSION["products"];
?>
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">

<div class="am-mainpanel"><!-- cloding in footer -->
    <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Premises Employee From Permission</h5>
    </div>
</div>
<div class="am-mainpanel">
	<div class="am-pagebody">
        <div class="card pd-20 pd-sm-40">
          <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
              	
                <tr>
                  <th class="wd-5p">Sl NO.</th>
                  <th class="wd-15p">Premises Name</th>
                  <th class="wd-10p">User Name</th>
                  <th class="wd-10p">Representative Name</th>
                  <th class="wd-10p">Email</th>
                  <th class="wd-10p">Mobile</th>
                  <th class="wd-5p">Action</th>
                  
                </tr>
              </thead>
              <tbody>
              	<?php 
  	              $respon=prk_super_admin_list();
                  // echo $respon;
                  $respon = json_decode($respon, true);
                  $c=0;
  	              foreach($respon['parking_area'] as $value){
                    $c+=1;
  	              	$prk_admin_name = $value['prk_admin_name'];
  	              	$prk_admin_id = $value['prk_admin_id'];  
                  ?>
                 	<tr>
                   <td><?php echo $c; ?></td>
                   <td><?php echo $value['prk_area_name']; ?></td>
                   <td><?php echo $value['prk_admin_name']; ?></td>
                   <td><?php echo $value['prk_area_rep_name']; ?></td>
                   <td><?php echo $value['prk_area_email']; ?></td>
                   <td><?php echo $value['prk_area_rep_mobile']; ?></td>
                   <td style="text-align: center;">
                   	<button type="button" name="permission" id="permission" data-target="#user_parmission<?=$prk_admin_id?>" value="<?php echo $prk_admin_id; ?>" data-toggle="modal" data-placement="top" title="Get Permission" style="background: transparent; border: 0px;"><i class="fa fa-database" style="font-size:15px"></i></button>
                   </td>
                  </tr>
                  <div id="user_parmission<?=$prk_admin_id?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                      <!-- Modal content-->
                      <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title" style="font-size: 18px;">Get Permission</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                          <div class="modal-body">
                              <div class="row">
                                <?php
                                  $form_dtl=p_employee_form_list();
                                  // echo $form_dtl;
                                  $form_dtl = json_decode($form_dtl, true);
                                  foreach($form_dtl['from_list'] as $value)
                                  {
                                    $select_list =p_employee_form_select_list($value['form_id'],$prk_admin_id);
                                    $select_li = json_decode($select_list);
                                    // echo $select_list;
                                    // echo $value['form_id'];
                                    ?>
                                    <form method="post" id="form" action="middle_tire/prk_employee_from_permition.php">
                                      <div class="col-sm-2">
                                          <input type="hidden" name="user_name" id="user_name" value="<?php   echo $prk_admin_name; ?>">
                                          <input type="hidden" name="inserted_by" id="inserted_by" value="<?php echo $insert_by; ?>">
                                          <input type="hidden" name="prk_admin_id" id="prk_admin_id" value="<?php  echo $prk_admin_id; ?>">
                                      </div>
                                      <div class="col-sm-4">
                                          <?php echo $value['form_name'];   ?>
                                      </div>
                                      <div class="col-sm-6">
                                         <input id="checkbox" type="checkbox" name="checkbox[]" value="<?php echo $value['form_id'];?>" 
                                         <?php
                                            if($select_li->status=='1'){
                                                $select_list = json_decode($select_list, true);
                                                foreach ($select_list['from_list'] as  $val)
                                                {
                                                  if($value['form_id'] == $val['form_id']){ echo "checked"; }   
                                                } 
                                              }
                                          ?>>
                                      </div>
                                  <?php  } ?>
                                </div>
                                    <div class="row">
                                      <div class="col-sm-4"></div>
                                      <div class="col-sm-4"></div>
                                      <div class="col-sm-4">
                                          <input type="submit" name="submit_permission" id="" class="btn btn-success" onclick="validate_permission()">
                                      </div> 
                                    </div>
                                </form>
                          </div>
                      </div>
                    </div>
                  </div>
         		     <?php } ?>
                </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->
    </div>
</div>   
<!-- datatable -->
<?php include"all_nav/footer.php"; ?>

<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

  });
</script>