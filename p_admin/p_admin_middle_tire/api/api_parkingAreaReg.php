<?php
/*
Description: parking admin registration.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function parkingAreaReg($p_ad_name,$p_ad_password,$p_ad_repassword,$p_ar_name,$prk_ar_email,$p_ar_rep_name,$p_ar_rep_mobile,$t_c_flag,$parking_type){
    global $pdoconn;
    $response = array();
    if(accountStatus($p_ad_name)=='true'){
        if(passwordChecker($p_ad_password,$p_ad_repassword)=='true'){
            if(emailChecker($prk_ar_email)=='true'){
                    $password=md5($p_ad_password);
                    $response = array();
                    $account_number= account_number_genarate();
                        /*$sql = "INSERT INTO `prk_area_admin`(`prk_admin_name`,`prk_admin_password`,`account_number`,`inserted_date`,`t_c_flag`) VALUE ('$p_ad_name','$password','$account_number','".TIME."','$t_c_flag ')";*/
                        $sql = "INSERT INTO `prk_area_admin`(`prk_admin_name`,`prk_admin_password`,`account_number`,`inserted_by`,`inserted_date`,`t_c_flag`,`parking_type`) VALUE ('$p_ad_name','$password','$account_number','$p_ad_name','".TIME."','$t_c_flag ','$parking_type')";
                        $query = $pdoconn->prepare($sql);                            
                        if($query->execute()){
                            $prk_admin_id = $pdoconn->lastInsertId(); 
                            $sql = "INSERT INTO `prk_area_dtl`(`prk_area_name`,`prk_area_email`,`prk_area_rep_name`,`prk_area_rep_mobile`,`prk_admin_id`,`inserted_by`,`inserted_date`) VALUE ('$p_ar_name','$prk_ar_email','$p_ar_rep_name','$p_ar_rep_mobile','$prk_admin_id','$p_ad_name','".TIME."')";
                                $query = $pdoconn->prepare($sql);
                                if($query->execute()){
                                    $prk_user_emp_no = $p_ad_name.'-1';
                                    // prk_area_user($prk_admin_id,$p_ad_name,$prk_user_emp_no,$p_ar_rep_name,$p_ad_password,$p_ad_password,$p_ad_name);
                                    $prk_user_gender='M';
                                    $prk_user_address='';
                                    $prk_user_landmark='';
                                    $prk_user_state='';
                                    $prk_user_city='';
                                    $prk_user_pincode='';
                                    $prk_user_emp_category='';
                                    $prk_user_img='';
                                     prk_area_user($prk_admin_id,$p_ad_name,$prk_user_emp_no,$p_ar_rep_name,$p_ad_password,$p_ad_password,$p_ar_rep_mobile,$prk_user_gender,$prk_user_address,$prk_user_landmark,$prk_user_state,$prk_user_city,$prk_user_pincode,$p_ad_name,$prk_user_emp_category,$prk_user_img);

                                    $content = array('to' => $prk_ar_email,
                                                     'subject' => 'Welcome to OPPRS Software Solution',
                                                     'p_ar_name' => $p_ar_name,
                                                     'p_ar_rep_name' => $p_ar_rep_name,
                                                     'body' => 'Our Sales team will contact you soon');

                                    if (send_mail_to_parking_area($content)) {
                                        $body = 'Name: '.$p_ad_name.'Email: '.$prk_ar_email;
                                        $content = array('to' => 'sales@opprss.com',
                                                     'subject' => 'New Registration',
                                                     'p_ar_name' => $p_ar_name,
                                                     'p_ar_rep_name' => $p_ar_rep_name,
                                                     'p_ar_rep_mobile'=> $p_ar_rep_mobile,
                                                     'prk_ar_email'=> $prk_ar_email);
                                        if (send_mail_to_sales_team($content)) {

                                            $response['status'] = 1;
                                            $response['message'] = 'registration sucessfull';
                                            return json_encode($response);
                                        }else{
                                            $response['status'] = 0;
                                            $response['message'] = 'registration sucessfull without mail1';
                                            return json_encode($response);
                                        }
                                        
                                    }else{
                                        $response['status'] = 0;
                                        $response['message'] = 'registration sucessfull without mail2';
                                        return json_encode($response);
                                    }
                                }else{
                        }
                        }
            }else{
                $response=emailChecker($prk_ar_email);
                return json_encode($response);
           }
        }else{
            $response=passwordChecker($p_ad_password,$p_ad_repassword);
            return json_encode($response);
        }
    }else{
        $response=accountStatus($p_ad_name);
        return json_encode($response);
    }
}
?>