<?php 
/*
Description: user vehicle out QR code scan and notify show parking employ .
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function payment_dtl_show($prk_admin_id,$payment_rec_emp_name,$prk_area_gate_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();

    $sql="SELECT `payment_dtl`.`payment_dtl_id`,
        `payment_dtl`.`veh_number`, 
        `payment_dtl`.`vehicle_type`,
        `vehicle_type`.`vehicle_typ_img` 
        FROM `payment_dtl`,`vehicle_type` 
        WHERE `payment_dtl`.`prk_admin_id`='$prk_admin_id'
        AND `payment_dtl`.`vehicle_type`=`vehicle_type`.`vehicle_sort_nm`
        AND `payment_dtl`.`prk_area_gate_id`='$prk_area_gate_id'  
        AND `payment_dtl`.`pay_dtl_del_flag`='".FLAG_N."' 
        AND `payment_dtl`.`payment_rec_status`='".FLAG_I."' 
        AND `payment_dtl`.`who_veh_out`='".FLAG_U."' 
        ORDER BY `payment_dtl_id` ASC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
    	$val = $query->fetch();
        $response['status'] = 1;
        $response['session'] = 1;
        $response['message'] = 'Successful';
        $response['payment_dtl_id'] = $val['payment_dtl_id'];
        $response['veh_number'] = $val['veh_number'];
        $response['vehicle_type'] = $val['vehicle_type'];
        $response['vehicle_typ_img'] = $val['vehicle_typ_img'];
	}else{
		$response['status'] = 0;
        $response['session'] = 1;
	    $response['message'] = 'List Empty';
	}
    return json_encode($response); 
}
?>