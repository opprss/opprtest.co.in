<?php
/*
Description: Parking area vehicle all rate dtl list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function apartment_bulk_mail_send($prk_admin_id,$parking_admin_name,$id,$subject,$message){

    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $pieces = explode("-", $id);
    $case=$pieces[0];
    switch ($case) {
        case 'OT': #OTHER
            // $sql="";
            break;
        case 'AM': #All Member
            $sql="SELECT CONCAT('O-',od.owner_id ) as 'id',
                od.owner_name as 'name',
                od.owner_mobile as 'mobile',
                od.owner_email as 'email' 
                FROM owner_details od 
                WHERE od.prk_admin_id='$prk_admin_id' 
                AND od.active_flag='".FLAG_Y."' 
                AND od.del_flag='".FLAG_N."' UNION
                SELECT CONCAT('T-',td.tenant_id) as 'id',
                td.tenant_name as 'name',
                td.tenant_mobile as 'mobile',
                td.tenant_email as 'email' 
                FROM tenant_details td 
                WHERE td.prk_admin_id='$prk_admin_id' 
                AND td.active_flag='".FLAG_Y."' 
                AND td.del_flag='".FLAG_N."'";
            break;
        case 'AF': #All Facility Member
            $sql="SELECT CONCAT('O-',od.owner_id ) as 'id',
            od.owner_name as 'name',
            od.owner_mobile as 'mobile',
            od.owner_email as 'email'
            FROM `board_members` bm, `designation` des,`owner_details` od  
            WHERE bm.`prk_admin_id`='$prk_admin_id' 
            AND bm.`active_flag`='".FLAG_Y."' 
            AND bm.`del_flag`='".FLAG_N."'
            AND des.`designation_id`=bm.`designation_id` 
            AND des.`prk_admin_id`=bm.`prk_admin_id` 
            AND des.`active_flag`='".FLAG_Y."' 
            AND des.`del_flag`='".FLAG_N."'
            AND od.`owner_id`=bm.`owner_id` 
            AND od.`prk_admin_id`=bm.`prk_admin_id` 
            AND od.`active_flag`='".FLAG_Y."' 
            AND od.`del_flag`='".FLAG_N."'";
            break;
        case 'AO': #All OWNER
            $sql="SELECT CONCAT('O-',od.owner_id ) as 'id',
            od.owner_name as 'name',
            od.owner_mobile as 'mobile',
            od.owner_email as 'email' 
            FROM owner_details od 
            WHERE od.prk_admin_id='$prk_admin_id' 
            AND od.active_flag='".FLAG_Y."' 
            AND od.del_flag='".FLAG_N."'";
            break;
        case 'AT': #All TENANT
            $sql="SELECT CONCAT('T-',td.tenant_id) as 'id',
            td.tenant_name as 'name',
            td.tenant_mobile as 'mobile',
            td.tenant_email as 'email' 
            FROM tenant_details td 
            WHERE td.prk_admin_id='$prk_admin_id' 
            AND td.active_flag='".FLAG_Y."' 
            AND td.del_flag='".FLAG_N."'";
            break;
        case 'TO': #All OWNER THIS TOWER
            $sql="SELECT fm.flat_master_id,
            CONCAT('O-',od.owner_id ) as 'id',
            od.`owner_name` as 'name',
            od.owner_mobile as 'mobile',
            od.owner_email as 'email'
            FROM flat_master fm,owner_details od  
            WHERE fm.tower_id='$pieces[1]'      
            AND fm.prk_admin_id='$prk_admin_id' 
            AND fm.active_flag='".FLAG_Y."' 
            AND fm.del_flag='".FLAG_N."'
            AND '".TIME_TRN."' BETWEEN fm.`effective_date` AND fm.`end_date`
            AND od.prk_admin_id=fm.prk_admin_id
            AND od.owner_id=fm.owner_id 
            AND od.`active_flag`='".FLAG_Y."' 
            AND od.`del_flag`='".FLAG_N."'
            UNION 
            SELECT fm.flat_master_id,
            CONCAT('T-',td.tenant_id) as 'id',
            td.tenant_name as 'name',
            td.tenant_mobile as 'mobile',
            td.tenant_email as 'email' 
            FROM flat_master fm,tenant_details td 
            WHERE fm.tower_id='$pieces[1]'   
            AND fm.prk_admin_id='$prk_admin_id' 
            AND fm.active_flag='".FLAG_Y."' 
            AND fm.del_flag='".FLAG_N."'
            AND fm.living_status='".FLAG_T."'
            AND '".TIME_TRN."' BETWEEN fm.`effective_date` AND fm.`end_date`
            AND td.prk_admin_id=fm.prk_admin_id
            AND td.tenant_id=fm.tenant_id 
            AND td.`active_flag`='".FLAG_Y."' 
            AND td.`del_flag`='".FLAG_N."'";
            break;
        case 'TT': #All TENANT THIS TOWER
            $sql="SELECT fm.flat_master_id,
            CONCAT('T-',td.tenant_id) as 'id',
            td.tenant_name as 'name',
            td.tenant_mobile as 'mobile',
            td.tenant_email as 'email' 
            FROM flat_master fm,tenant_details td 
            WHERE fm.tower_id='$pieces[1]'      
            AND fm.prk_admin_id='$prk_admin_id' 
            AND fm.active_flag='".FLAG_Y."' 
            AND fm.del_flag='".FLAG_N."'
            AND fm.living_status='".FLAG_T."'
            AND '".TIME_TRN."' BETWEEN fm.`effective_date` AND fm.`end_date`
            AND td.prk_admin_id=fm.prk_admin_id
            AND td.tenant_id=fm.tenant_id 
            AND td.`active_flag`='".FLAG_Y."' 
            AND td.`del_flag`='".FLAG_N."'";
            break;
        default:
            $response['status'] = 0;
            $response['message'] = 'Wrong Select';
            return json_encode($response);
            break;
            exit();
    }
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['id'] = $val['id'];
            $park['name'] = $val['name'];
            $park['mobile'] = $val['mobile'];
            $park['email'] = $val['email'];
            // email_massage($val['email'],$message);
            email_massage_new($val['email'],$message,$subject);
            array_push($allplayerdata, $park);
        }
        $compose_mail_id=uniqid();
        $sql="INSERT INTO `apartment_compose_mail`(`compose_mail_id`, `prk_admin_id`, `send_to`, `subject`, `message`, `send_date`, `inserted_by`, `inserted_date`) VALUES ('$compose_mail_id','$prk_admin_id','$id','$subject','$message','".TIME."','$parking_admin_name','".TIME."')";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['flat_master_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>