<?php
/*
Description: email checker. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function emailChecker($email){
    $response = array();
    if (filter_var($email, FILTER_VALIDATE_EMAIL)){
        return true;
    }
    else
    {
        $response['status'] = 0;
        $response['message'] = 'Email Not Valide.';
        return ($response);
    }
}
?>