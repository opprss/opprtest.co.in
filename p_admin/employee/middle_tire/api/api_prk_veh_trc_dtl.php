<?php 
/*
Description: Parking employ vehicle verify and data insert trc table.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function prk_veh_trc_dtl($prk_admin_id,$user_admin_id,$prk_veh_type,$veh_number,$prk_user_username,$prk_in_rep_name,$payment_type,$gate_pass_num,$prk_sub_unit_id,$advance_pay,$user_mobile,$prk_area_short_name,$veh_out_otp,$veh_token){
    global $pdoconn;
    $response = array();
    $sql = "SELECT `prk_admin_id` FROM `prk_veh_trc_dtl` WHERE `prk_admin_id`='$prk_admin_id' AND `prk_veh_type`='$prk_veh_type' AND `veh_number`='$veh_number' AND `prk_veh_out_time` is NULL";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $response['status'] = 0;
        $response['message'] = 'This vehicle is already inside the parking area';
    }else{
        $prk_area_name='';
        $prk_in =prk_inf($prk_admin_id);
        $json = json_decode($prk_in);
        if($json->status=='1'){
            $veh_out_verify_flag = $json->veh_out_verify_flag;
            $prk_area_name=$json->prk_area_name;
            if($veh_out_verify_flag==FLAG_Y){
                $veh_out_verify_flag=FLAG_N;
            }else{
                $veh_out_verify_flag=FLAG_Y;
            }
        }
        $unique_prk_short_name = $prk_area_short_name;
        $unique_type = FLAG_TR;
        $unique_ke = unique_key_generate($unique_prk_short_name,$unique_type,$prk_admin_id);
        $json = json_decode($unique_ke);
        if($json->status=='1'){
            $prk_veh_trc_dtl_number = $json->invoice_no;
            $unique_id = $json->unique_id;
        }
        $sql = "INSERT INTO `prk_veh_trc_dtl`(`prk_admin_id`,`user_admin_id`,`prk_veh_type`,`veh_number`,`transition_date`,`prk_in_rep_name`,`inserted_by`,`payment_type`,`gate_pass_num`,`prk_veh_in_time`,`inserted_date`,`prk_sub_unit_id`,`advance_pay`,`user_mobile`,`prk_veh_trc_dtl_number`,`veh_out_otp`,`veh_token`,`veh_out_verify_flag`) VALUE ('$prk_admin_id','$user_admin_id','$prk_veh_type','$veh_number','".TIME_TRN."','$prk_in_rep_name','$prk_user_username','$payment_type','$gate_pass_num','".TIME."','".TIME."','$prk_sub_unit_id','$advance_pay','$user_mobile','$prk_veh_trc_dtl_number','$veh_out_otp','$veh_token','$veh_out_verify_flag')";
        $query = $pdoconn->prepare($sql); 
        if($query->execute()){
            $id = $pdoconn->lastInsertId();

            $sql="SELECT `user_admin`.`user_admin_id`,`token`.`device_token_id`,`token`.`device_type`,`token`.`token_value` FROM `user_admin`,`token` WHERE `user_mobile`='$user_mobile' AND `token`.`active_flag`='".FLAG_Y."' AND `token`.`user_admin_id`=`user_admin`.`user_admin_id`";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $device_token_id =  $val['device_token_id'];
                $device_type =  $val['device_type'];
                if ($device_type=='ANDR') {
                    $payload = array();
                    // $payload['area_name'] = $prk_area_name;
                    // $payload['name'] = $veh_number;
                    $payload['in_date'] = TIME;
                    $payload['url'] = 'VEHICLE IN';
                    $title = 'OPPRSS';
                    $message = $veh_number.' SUCCESSFULLY PARKED AT '.$prk_area_name.'.';
                    $push_type = 'individual';
                    $include_image='';
                    $clickaction='VEHICLE IN';
                    $oth1='';
                    $oth2='';
                    $oth3='';
                    $paylo = push_send($title,$message,$push_type,$include_image,$device_token_id,$payload,$clickaction,$oth1,$oth2,$oth3);
                }
            }

            if($advance_pay>0){
                $unique_prk_short_name = $prk_area_short_name;
                $unique_type = FLAG_CR;
                $payment_statas = FLAG_S;
                $in_out_flag = FLAG_I;
                $unique_ke = unique_key_generate($unique_prk_short_name,$unique_type,$prk_admin_id);
                $json = json_decode($unique_ke);
                if($json->status=='1'){
                    $payment_receive_number = $json->invoice_no;
                    $unique_id = $json->unique_id;
                }
                $payment_rec = payment_receive_insert($payment_receive_number,$prk_admin_id,$id,$payment_type,$advance_pay,$payment_statas,$prk_in_rep_name,$in_out_flag);
            }
            $response['status'] = 1;
            $response['prk_veh_trc_dtl_id'] = $id;
            $response['veh_in_date'] = date('d-M-Y',strtotime(TIME));
            $response['veh_in_time'] = date('h:i A',strtotime(TIME));
            $response['message'] = 'Sucessful';
        }else{
            $response['status'] = 0;
            $response['message'] = 'Not Sucessful';
        }  
    } 
    return json_encode($response); 
}
?>