<!-- ################################################
  
Description: It will dispaly list of all current vehicle availabe in the parking area
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
  include "all_nav/header.php"; 
  if (isset($_SESSION['prk_admin_id'])) {
    $insert_by=$_SESSION['parking_admin_name'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
  }
?>
<link href="../lib/highlightjs/github.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../lib/select2/css/select2.min.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
<style>
  .size{
    font-size: 11px;
  }
  .error{
    font-size: 11px;
    color: red;
  }
  th,td{
    text-align: center;
  }
  .button_print{
    border: none;
    background: none;
  }
</style>
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Bill Payment</h5>
  </div>
</div>
<div class="am-mainpanel">
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40" style="padding: 10px;">
      <div class="table-wrapper mg-t-15">
        <table id="datatable1" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="wd-5p">Sl No.</th>
              <th class="wd-10p">Invoice No.</th>
              <th class="wd-10p">Bill Month</th>
              <th class="wd-15p">Bill Period</th>
              <th class="wd-10p">Bill Due Date</th>
              <th class="wd-10p">Total Amount</th>
              <th class="wd-10p">Payment Receive Date</th>
              <th class="wd-10p">Action</th>
            </tr>
          </thead>
          <tbody id="cus_bill_list_table"></tbody>
        </table>
      </div><!-- table-wrapper -->
    </div><!-- card -->
<?php include"all_nav/footer.php"; ?>
<!-- scripts for datatable -->
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="../lib/datatables-responsive/jquery.dataTables.min.js"></script>
<script src="../lib/datatables-responsive/dataTables.buttons.min.js"></script>
<script src="../lib/datatables-responsive/jszip.min.js"></script>
<script src="../lib/datatables-responsive/pdfmake.min.js"></script>
<script src="../lib/datatables-responsive/vfs_fonts.js"></script>
<script src="../lib/datatables-responsive/buttons.html5.min.js"></script>
<script>
  var prkUrl = "<?php echo PRK_URL; ?>";
  var SUP_EMP_URL = "<?php echo SUP_EMP_URL; ?>";
  var prk_admin_id = "<?php echo $prk_admin_id; ?>";
  var insert_by = "<?php echo $insert_by; ?>";
  var token = "<?php echo $token; ?>";
  $(document).ready(function(){

    cus_bill_list();
  });
  function cus_bill_list() {
    $('#refresh_button_table').show();
    var cusBillGenerateList = prkUrl+'cus_bill_generate_list.php';
    $.ajax({
      url :cusBillGenerateList,
      type:'POST',
      data :{
        'prk_admin_id':prk_admin_id
      },
      dataType:'html',
      success  :function(data){
        // alert(data);
        var demoLines = '';
        var json = $.parseJSON(data);
        if (json.status){
            var c = 0;
          for (var key in json.cus_bill_gen_list) {
            var keyy = json.cus_bill_gen_list[key];
            c +=1;
            demoLines += '<tr>\
              <td> '+c+'</td>\
              <td>'+keyy['invoice_number']+'</td>\
              <td>'+keyy['billing_month']+'</td>\
              <td>'+keyy['bill_period_start_date']+' To '+keyy['bill_period_end_date']+'</td>\
              <td>'+keyy['bill_due_date']+'</td>\
              <td>'+keyy['total_invoice_value']+'</td>\
              <td>'+keyy['payment_receive_date']+'</td>\
              <td>';
              if(keyy['payment_receive_flag']=='N'){

                demoLines += '<button style="background-color:green;" class="out btn prk_button" title="Pay Bill"><a href="customer-bill-receive?list_id='+keyy['cus_bill_dtl_id']+'" style="color:white;"><i class="fa fa-sign-out"></i> Pay</a></button>&nbsp;&nbsp;';
              }else{

                demoLines += '<button style="background-color:darkgray;" class="out btn prk_button" title="Pay Bill"><i class="fa fa-sign-out"></i> Pay</button>&nbsp;&nbsp;';
              }
              demoLines += '<button id="'+keyy['cus_bill_dtl_id']+'" class="button_print" onclick="customer_bill_print(this.id)"><a href="#" data-toggle="tooltip" data-placement="top" title="Download Bill"><i class="fa fa-print" style="font-size:27px"></i></a></button>';
            demoLines += '</td></tr>';
          }
        }
        $('#datatable1').dataTable().fnDestroy();
        $("#cus_bill_list_table").html(demoLines);
        datatable_show();
      }
    });
  }
  function datatable_show(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      language: {
      searchPlaceholder: 'Search Here',
      sSearch: '',
      lengthMenu: '_MENU_ Page',
      },
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      buttons: [
        {
          text: '<span id="refresh_button_table"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
          action: function ( e, dt, node, config ) {
              // alert( 'Button activated' );
              cus_bill_list();
          }
        },
        { 
          extend: 'copyHtml5', 
          //footer: true,
          title: 'Customer Bill List' 
        },
        { 
          extend: 'excelHtml5', 
          //footer: true,
          title: 'Customer Bill List'  
        },
        {
          extend: 'pdfHtml5',
          //footer: true,
          title: 'Customer Bill List',
          customize: function(doc) {
            doc.content[1].margin = [ 60, 0, 0, 0 ]
          }
        },
      ]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    $('#refresh_button_table').hide();
  }
  function customer_bill_print(str) {
    // alert(str);
    urlPrint = SUP_EMP_URL+"customer_bill_print?list_id="+str;
    window.open(urlPrint,'mywindow','width=850,height=600');
    location.reload();
  }
</script>
