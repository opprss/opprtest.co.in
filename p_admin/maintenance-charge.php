<!-- ################################################
  
Description: Tower detalis.
Developed by: Rakhal Raj 
Created Date: 12-03-2019

 ####################################################-->
<?php include "all_nav/header.php";
$crud_type='LS';

$maintenance_charge=maintenance_charge($prk_admin_id,'','','','','','','',$parking_admin_name,$crud_type,'');
$maintenance_charge_list = json_decode($maintenance_charge, true);

$category_type='APT_FLT_HLD';
$prk_user_emp_cat = common_drop_down_list($category_type);
$prk_user_emp_category = json_decode($prk_user_emp_cat, true);
$category_type='APT_CH_PAY_TI';
$payable_time = common_drop_down_list($category_type);
$payable_time_list = json_decode($payable_time, true);
?>

<!-- header position -->
  <style type="text/css">
    .size{
      font-size: 11px;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .error{
      font-size: 11px;
      color: red;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .success{
      font-size: 11px;
      color: green;
    }
  </style>
  <!-- links for datatable -->
  <link href="../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
  
    <div class="am-mainpanel">

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Manitenance Charge Details<//?=$maintenance_charge?></h5>
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40" >
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form id="park_flat">
              <!-- <form role="form" id="park_flat" method="post"  enctype="multipart/form-data"> -->
              <div class="row">
                <div class="col-lg-8">
                  <div class="row">
                    <div class="col-lg-5">
                      <div class="form-group">
                        <label class="form-control-label size">Type of Maintenance <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control" placeholder="Name" name="maint_type" id="maint_type">
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label size">Maintenance Short Name <span class="tx-danger"></span></label>
                        <input type="text" class="form-control" placeholder="Short Name" name="maint_type_short" id="maint_type_short">
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label class="form-control-label size">Maintenance Charge <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control" placeholder="Charge" name="maint_charge" id="maint_charge">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label size">Payable Time <span class="tx-danger">*</span></label>
                        <div class="select">
                          <select name="payable_time" id="payable_time" style="opacity: 0.8;font-size:14px">
                            <option value="">Select Type</option>
                              <?php
                              foreach ($payable_time_list['drop_down'] as $val){ if($val['sort_name']=='M'){?>  
                                <option value="<?php echo $val['sort_name']?>"><?php echo $val['full_name']?></option>
                              <?php }} ?>
                          </select>
                        </div>
                        <span style="position: absolute; display: none;" id="error_payable_time" class="error">This field is required.</span>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label size">Applicable for <span class="tx-danger">*</span></label>
                        <div class="select">
                          <select name="holder_type" id="holder_type" style="opacity: 0.8;font-size:14px">
                            <option value="">Select Type</option>
                              <?php
                              foreach ($prk_user_emp_category['drop_down'] as $val){ 
                                if($val['sort_name']!='V'){?>  
                                <option value="<?php echo $val['sort_name']?>"><?php echo $val['full_name']?></option>
                              <?php          
                              }}
                            ?>
                          </select>
                        </div>
                        <span style="position: absolute; display: none;" id="error_holder_type" class="error">This field is required.</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Effective Date <span class="tx-danger">*</span></label>
                    <input type="text" name="effective_date" id="effective_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                    <span style="position: absolute; display: none;" id="error_effective_date" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">End Date </label>
                    <input type="text" name="end_date" id="end_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                   <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2 mg-t-25">

                  <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
                </div>
                <div class="col-lg-2 mg-t-25">

                  <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- data table -->
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40">
          <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th >SL No.</th>
                  <th >Type of Maintenance</th>
                  <th >Maintenance Charge</th>
                  <th >Payable Time</th>
                  <th >Applicable for</th>
                  <th >Effective Date</th>
                  <th >End Date</th>
                  <th >Action</th>
                </tr>
              </thead>
                <?php 
                if($maintenance_charge_list['status']){
                  $c=0;
                  foreach($maintenance_charge_list['maint_list'] as $value){ $c+=1;?>
                    <tr>
                    <td ><?=$c;?></td>
                    <td ><?=$value['maint_type'];?></td>
                    <td ><?=$value['maint_charge'];?></td>
                    <td ><?=$value['payable_time_full'];?></td>
                    <td ><?=$value['holder_type_full'];?></td>
                    <td ><?=$value['effective_date'];?></td>
                    <td ><?=$value['end_date'];?></td>
                    <td >
                      <a href="<?php echo 'maintenance-charge-modify?list_id='.base64_encode($value['maint_id']);?>" class="pd-l-10" data-toggle="tooltip" data-placement="top" title="Modify"><i class="fa fa-pencil" style="font-size:18px; font-size:18px;"></i></a>
                      <button type="button" class="clean-button" name="delete" id="<?php echo $value['maint_id'];?>" data-toggle="tooltip" onclick="flat_delete(this.id)" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
                    </td>
                    </tr>
                  <?php }}?>
            </table>
          </div>
        </div>
      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#effective_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
        if (date < now) {
          return {
            // disabled : true,
            class_name : 'date-in-past'
          };
        }
        return {};
      } 
    });
  });
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#effective_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
        if (date < now_end) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
        }
        return {};
      } 
    });
  });
  $('#effective_date').focus(function(){
    $('#end_date').val('');
  });
</script>
<!-- scripts for datatable -->
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>

<script>
  // alert(prkUrl);
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      },
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
</script>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE ?>";
  var prk_admin_id="<?=$prk_admin_id?>"
  var parking_admin_name="<?=$parking_admin_name?>"
  var token="<?=$token?>"
  var urlPrkAreGateDtl = prkUrl+'maintenance_charge.php';
  // alert(token);
  // $( document ).ready( function () {
  $('#save').click(function(){
    var effective_date=($("#effective_date").val());
    var end_date=($("#end_date").val());

    var str_array = end_date.split('-');
    var dd = str_array[0];
    var mm = str_array[1];
    var yy = str_array[2];
    var end_dat = yy+'-'+mm+'-'+dd;
    end_date = (!end_date == '') ? end_dat : FUTURE_DATE;
    var maint_type=($("#maint_type").val());
    var maint_type_short=($("#maint_type_short").val());
    var maint_charge=($("#maint_charge").val());
    var payable_time=($("#payable_time").val());
    var holder_type=($("#holder_type").val());
    // alert(end_date);
    var str_array = effective_date.split('-');
    var dd = str_array[0];
    var mm = str_array[1];
    var yy = str_array[2];
    var effective_dat = yy+'-'+mm+'-'+dd;
    if($("#park_flat").valid()&effective_date!=''){
      // alert('ok');
       $('#save').val('Wait ...').prop('disabled', true);
      $.ajax({
        url :urlPrkAreGateDtl,
        type:'POST',//'prk_admin_id','maint_type','maint_charge','payable_time','holder_type','effective_date','end_date','parking_admin_name','token','crud_type','maint_id'
        data :{
          'prk_admin_id':prk_admin_id,
          'maint_type':maint_type,
          'maint_type_short':maint_type_short,
          'maint_charge':maint_charge,
          'payable_time':payable_time,
          'holder_type':holder_type,
          'effective_date':effective_dat,
          'end_date':end_date,
          'parking_admin_name':parking_admin_name,
          'token': token,
          'crud_type':'I',
          'maint_id':''
        },
        dataType:'html',
        success  :function(data){
          $('#save').val('SAVE').prop('disabled', false);
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>Maintenance Charge added successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                    location.reload(true);
                }
              }
            });
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
              type: 'red'
            });
            }
          }
        }
      });
    }
  });
  function flat_delete(flat_id){
    // alert(tower_id);
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            if (flat_id != '') {
                $.ajax({
                  url :urlPrkAreGateDtl,
                  type:'POST',
                  data :{
                    'prk_admin_id':prk_admin_id,
                    'maint_type':'',
                    'maint_charge':'',
                    'payable_time':'',
                    'holder_type':'',
                    'effective_date':'',
                    'end_date':'',
                    'parking_admin_name':parking_admin_name,
                    'token': token,
                    'crud_type':'D',
                    'maint_id':flat_id

                  },
                  dataType:'html',
                  success  :function(data)
                  {
                    // alert(data);
                    var json = $.parseJSON(data);
                    if (json.status){
                      $.alert({
                        icon: 'fa fa-smile-o',
                        theme: 'modern',
                        title: 'Success',
                        content: "<p style='font-size:0.9em;'>Delete successfully</p>",
                        type: 'green',
                        buttons: {
                          Ok: function () {
                            location.reload(true);
                          }
                        }
                      });
                    }else{
                       $.alert({
                        icon: 'fa fa-frown-o',
                        theme: 'modern',
                        title: 'Error !',
                        content: "<p style='font-size:0.7em;'>Somting went wrong 1</p>",
                        type: 'red'
                      });
                    }
                  }
                });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
</script>
<script type="text/javascript">
  $( document ).ready( function () {
    $( "#park_flat" ).validate( {
      rules: {
        maint_type: "required",
        maint_charge: "required",
        payable_time: "valueNotEqualspay",
        holder_type: "valueNotEqualshol",
        effective_date: "valueNotEqualsEff"
      },
      messages: {
      }
    });
    $("#effective_date").blur(function () {
      var effective_date = $('#effective_date').val();
      if(effective_date == ''){
        $('#error_effective_date').show();
        return false;
      }else{
        $('#error_effective_date').hide();
        return true;
      }
    });
    $("#payable_time").on('change', function(){
      var v_type = $('#payable_time').val();
      if(v_type == "")
      {
        $('#error_payable_time').show();
        return false;
      }else{
          $('#error_payable_time').hide();
          return true;
      }
    });
    $("#holder_type").on('change', function(){
      var v_type = $('#holder_type').val();
      if(v_type == "")
      {
        $('#error_holder_type').show();
        return false;
      }else{
          $('#error_holder_type').hide();
          return true;
      }
    });
  });
  $.validator.addMethod("valueNotEqualsEff", function(value){
    // alert('ok');
    arg = "";
    if (value != "") {
      return arg !== value;
    }else{
      $("#error_effective_date").show();
      return true;
    }
  });
  $.validator.addMethod("valueNotEqualspay", function(value){
    // alert(value);
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_payable_time").show();
      return false;
    }
  });
  $.validator.addMethod("valueNotEqualshol", function(value){
    // alert(value);
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_holder_type").show();
      return false;
    }
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
  $("#maint_charge").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0)){
      event.preventDefault();
    }
  });
</script>