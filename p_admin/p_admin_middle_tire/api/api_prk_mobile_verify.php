<?php
/*
Description: Parking gaarea mobile verify.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_mobile_verify($prk_admin_id, $prk_area_rep_mobile){
    global $pdoconn;
    $response = array();
    $sql ="UPDATE `prk_area_dtl` SET `prk_mobile_verify`='T' WHERE `prk_admin_id`='$prk_admin_id' AND `prk_area_rep_mobile`='$prk_area_rep_mobile' AND `active_flag`='".FLAG_Y."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Mobile Number Verified';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Mobile Number Not Update';
    }
    return json_encode($response);
}
?>