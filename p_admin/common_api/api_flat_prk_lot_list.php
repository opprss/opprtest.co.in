<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function flat_prk_lot_list($prk_admin_id,$flat_master_id){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
    $sql = "SELECT `prk_lot_dtl_id`,`prk_lot_number` FROM `prk_lot_dtl` WHERE `flat_master_id`='$flat_master_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['prk_lot_dtl_id'] = $val['prk_lot_dtl_id'];
            $park['prk_lot_number'] = $val['prk_lot_number'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['prk_lot_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>