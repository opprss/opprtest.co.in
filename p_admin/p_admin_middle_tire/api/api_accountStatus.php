<?php
/*
Description: Parking area account status check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function accountStatus($p_ad_name){
    $response = array();
    global $pdoconn;
    $sql ="SELECT `prk_admin_id`,`park_ac_status` FROM `prk_area_admin` WHERE `prk_admin_name`=:prk_admin_name";
    $query  = $pdoconn->prepare($sql);
    $query->execute(array('prk_admin_name'=>$p_ad_name));
    $count=$query->rowCount();
    if($count>0){
        $row = $query->fetch();
        $ac_sta =  $row['park_ac_status'];
        if($ac_sta=='A'){
            $response['status'] = 0;
            $response['message'] = 'User Already Exists';
        }
        if($ac_sta=='T'){
            $response['status'] = 0;
            $response['message'] = 'User Temporary Locked';
        }
        if($ac_sta=='L'){
            $response['status'] = 0;
            $response['message'] = 'User Locked';
        }
        if($ac_sta=='D'){
            $response['status'] = 0;
            $response['message'] = 'Your Account is not verified yet.';
        }
        if($ac_sta=='P'){
            $response['status'] = 0;
            $response['message'] = 'User Payment due Please Contact Customer Care';
        }
        return ($response);
    }else{
        return true; 
    }
}
?>