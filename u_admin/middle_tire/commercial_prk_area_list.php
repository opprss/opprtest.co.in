<?php 
/*
Description: found the parking area list.
Developed by: Rakhal Raj Mandal
Created Date: 30-05-2018
Update date : ----------
*/
require_once 'api/reg_api.php';
// require_once 'api/global_api.php';
$response = array();
if(isAvailable(array('user_admin_id','token','state','city'))){
    if(isEmpty(array('user_admin_id','token'))){
        $user_admin_id = trim($_POST['user_admin_id']);
        $token = trim($_POST['token']);
        $city = trim($_POST['city']);

            $response = commercial_prk_area_list();
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response); 
} 
echo $response;
?>