<?php
/*
Description: city name show. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function city_name($city_id){
    $allplayerdata = array();
    $response = array();
    $vehicle = array();
    global $pdoconn;
    $sql = "SELECT `city_name` FROM `city` WHERE `e_id`='$city_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $response['status'] = 1;
    $response['message'] = 'Successfully';
    $response['city_name'] = $val['city_name'];
    return json_encode($response);
}
?>