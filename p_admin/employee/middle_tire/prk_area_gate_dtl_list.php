<?php 
/*
Description: parking area all gate dtl list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
 require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
 $response = array();
 $resp= array();
 if(isAvailable(array('prk_admin_id','prk_area_user_id','token'))){
 	if(isEmpty(array('prk_admin_id','prk_area_user_id','token'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_area_user_id = trim($_POST['prk_area_user_id']);
	 	$token = ($_POST['token']);
        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $respon=prk_area_gate_dtl_list($prk_admin_id);
        	echo ($respon);
        }else{
            $response['status'] = 0; 
            $response['session'] = 0;
            $response['message'] = 'Session expired please login again';
            echo json_encode($response);
        }

        
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?> 