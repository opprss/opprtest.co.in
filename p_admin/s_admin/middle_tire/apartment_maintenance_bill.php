<?php
/*
Description: Parking area login.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
    require_once 'api/parkAreaReg_api.php';
    $response = array();
    $allplayerdata = array();
    // $allplayerdataa = array();
    $current_date=TIME_TRN;
    $this_month = date('m-Y', strtotime($current_date));
    if(isAvailable(array('prk_admin_id','inserted_by'))){
        if(isEmpty(array('prk_admin_id','inserted_by'))){
            $prk_admin_id = trim($_POST['prk_admin_id']);
            $inserted_by = trim($_POST['inserted_by']);
            // $response = prk_employee_from_permition($prk_admin_id,$user_name,$form_id,$inserted_by);
            $admin_dtl_show = flat_master_list($prk_admin_id,'','');
            $flat_master_list = json_decode($admin_dtl_show,true);
            if($flat_master_list['status']){
                $c=0;
                foreach($flat_master_list['flat_master_list'] as $value){
                    // $allplayerdataa = array();
                    $c+=1;
                    $flat_id = $value['flat_id'];
                    $flat_name = $value['flat_name'];
                    $tower_id = $value['tower_id'];
                    $tower_name = $value['tower_name'];
                    // $prk_admin_id = $value['prk_admin_id'];
                    // $eff_date = $value['eff_date'];
                    // $end_date = $value['end_date'];
                    $flat_master_id = $value['flat_master_id'];
                    $owner_id = $value['owner_id'];
                    $owner_name = $value['owner_name'];
                    $owner_email = $value['owner_email'];
                    $tenant_id = $value['tenant_id'];
                    $tenant_name = $value['tenant_name'];
                    $tenant_email = $value['tenant_email'];
                    $living_status = $value['living_status'];
                    // $living_status_full = $value['living_status_full'];
                    $m_account_number = $value['m_account_number'];
                    if ($living_status!='V') {
                        $tran_date=TIME_TRN;
                        // $tran_date='2019-08-01';
                        $bill_list = apartment_maintenance_bill($prk_admin_id,$tower_id,$tower_name,$flat_id,$flat_name,$flat_master_id,$owner_id,$owner_name,$owner_email,$tenant_id,$tenant_name,$tenant_email,$living_status,$tran_date, $m_account_number,$inserted_by);

                        $bill_d[$value['flat_name']] = $bill_list;
                    }
                }
                $response['status'] = 1;
                $response['sort_name'] = $bill_d;
                $response['message'] = 'Successful';
                $response = json_encode($response);
            }else{
                $response['status'] = 0;
                $response['message'] = 'All Fields Are Mandator';
                $response = json_encode($response);
            }
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandator';
            $response = json_encode($response);
        }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        $response = json_encode($response);
    }
    echo $response;
?>