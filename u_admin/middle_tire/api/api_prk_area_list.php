<?php 
/*
Description: finde the parking area lisst.
Developed by: Rakhal Raj Mandal
Created Date: 02-06-2018
Update date :---------
*/
function prk_area_list(){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
	$sql="SELECT 'OTHER AREA'  AS `prk_area_name`,'000' AS `prk_admin_id`
		UNION
		SELECT `prk_area_dtl`.`prk_area_name`,
		`prk_area_admin`.`prk_admin_id`		 
		FROM `prk_area_admin` ,`prk_area_dtl`
		WHERE `prk_area_admin`.`prk_admin_id` = `prk_area_dtl`.`prk_admin_id`
		AND  `prk_area_admin`.`park_ac_status` ='".FLAG_A."'
		AND  `prk_area_dtl`.`active_flag` ='".FLAG_Y."'
		ORDER BY 2 ASC ";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
	    $arr_catagory = $query->fetchAll();
	    foreach($arr_catagory as $val)
	    {
	        $response['status'] = 1;
	        $response['message'] = 'Successful';
	        $park['prk_admin_id'] = $val['prk_admin_id'];
	        $park['prk_area_name'] = $val['prk_area_name'];
	        array_push($allplayerdata, $park);
	        $response['prk_area'] = $allplayerdata;
	    }
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
function prk_area_list_search($prk_area_name){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
	$sql="SELECT `prk_area_dtl`.`prk_area_name`,
		`prk_area_admin`.`prk_admin_id`		 
		FROM `prk_area_admin` ,`prk_area_dtl`
		WHERE `prk_area_admin`.`prk_admin_id` = `prk_area_dtl`.`prk_admin_id`
        AND `prk_area_dtl`.`prk_area_name` LIKE '%".$prk_area_name."%'
		AND  `prk_area_admin`.`park_ac_status` ='".FLAG_A."'
		AND  `prk_area_dtl`.`active_flag` ='".FLAG_Y."'
		ORDER BY 1 ASC 
		LIMIT 10";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
	    $arr_catagory = $query->fetchAll();
	    foreach($arr_catagory as $val){
	        $response['status'] = 1;
	        $response['message'] = 'Successful';
	        $park['prk_admin_id'] = $val['prk_admin_id'];
	        $park['prk_area_name'] = $val['prk_area_name'];
	        array_push($allplayerdata, $park);
	        $response['prk_area'] = $allplayerdata;
	    }
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>