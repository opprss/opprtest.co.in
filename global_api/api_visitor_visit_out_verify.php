<?php
/*
Description: OTP Validate. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function visitor_visit_out_verify($prk_visit_dtl_id,$user_name,$verify_status){
    $response = array();
    global $pdoconn;

    $sql = "UPDATE `prk_visit_dtl` SET `visitor_out_verify_date`='".TIME."',`visitor_out_verify_flag`='$verify_status',`visitor_out_verify_name`='$user_name' WHERE `prk_visit_dtl_id`='$prk_visit_dtl_id'";
    $query  = $pdoconn->prepare($sql);
    if($query->execute()){ 
        if ($verify_status=='Y') {
            $response['status']=1;
            $response['message'] = 'Visitor is Verified for OUT';
            $response = json_encode($response);
        }else{
            $response['status']=1;
            $response['message'] = 'Visitor Blocked';
            $response = json_encode($response);
        }
    }else{
        $response['status']=0;
        $response['message'] = 'Visitor is not Verified for OUT';
        $response = json_encode($response);
    }   
    return $response;         
}
?>