<?php
/*
Description: Aparment maintenance bill mail send.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function apartment_maintenance_bill($prk_admin_id,$tower_id,$tower_name,$flat_id,$flat_name,$flat_master_id,$owner_id,$owner_name,$owner_email,$tenant_id,$tenant_name,$tenant_email,$living_status,$tran_date, $m_account_number,$inserted_by){
    global $pdoconn;
    $response = array();
    $piec_sh_new = explode("-", $tran_date);//$pieces[0]
    $month=$piec_sh_new[1];
    $year=$piec_sh_new[0];
    $a_previous_balance = account_previous_balance($prk_admin_id,$m_account_number);
    $acc_previous_balance = json_decode($a_previous_balance, true); 
    $previous_balance = $acc_previous_balance['account_due'];

    $maintenance_tran=maintenance_tran_show($prk_admin_id,$year,$month,$tower_id,$flat_id,$living_status,$flat_master_id);
    $maintenance_tran = json_decode($maintenance_tran, true); 
    
    $current_due = $maintenance_tran['total_pay_m'];

    $tran_late_fine = maintenance_tran_late_fine($prk_admin_id,$year,$month,$previous_balance,$current_due);
    $m_tran_late_fine = json_decode($tran_late_fine, true); 
    $prk_area_name = $m_tran_late_fine['prk_area_name'];
    $fine_charges = $m_tran_late_fine['fine_charges'];
    $statement_date = $m_tran_late_fine['statement_date'];
    $bill_due_date = $m_tran_late_fine['bill_due_date'];
    $total_pay_before_due_date = $m_tran_late_fine['total_pay_before_due_date'];
    $after_fine_charges = $m_tran_late_fine['after_fine_charges'];
    $total_pay_after_due_date = $m_tran_late_fine['total_pay_after_due_date'];
    $late_fine_charrge = $m_tran_late_fine['late_fine_charrge'];
    $late_fine_rate_type = $m_tran_late_fine['late_fine_rate_type'];

    $sql = "SELECT `main_mas_id` FROM `maintenance_master` WHERE `prk_admin_id`='$prk_admin_id' AND `tower_id`='$tower_id' AND `flat_id`='$flat_id' AND `m_account_number`='$m_account_number' AND `main_tran_year`='$year' AND `main_tran_month`='$month' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    $vall = $query->fetch();
    $path='';
    if($count<=0){
        $prin_t='YES';
        $maintenance_mas = maintenance_master($prk_admin_id,$tower_id,$flat_id,$m_account_number,$year,$month,$previous_balance,$current_due,$fine_charges,$statement_date,$bill_due_date,$total_pay_before_due_date,$total_pay_after_due_date,$after_fine_charges,$inserted_by);
        $maintenance_master = json_decode($maintenance_mas, true); 
        $main_mas_id = $maintenance_master['main_mas_id'];
        foreach ( $maintenance_tran['mani_trn_list'] as $value ) { 
          $maint_id=$value['maint_id'];
          $maint_type=$value['maint_type'];
          $maint_charge=$value['maint_charge'];
          $payable_time_full=$value['payable_time_full'];

          maintenance_child($prk_admin_id,$main_mas_id,$tower_id,$flat_id,$year,$month,$maint_id,$maint_type,$maint_charge,$inserted_by);
        }
        $account_due=$current_due;
        $crud_type='U';
        $account_detail = flat_account_details($prk_admin_id,$tower_id,$flat_id,$m_account_number,$account_due,$inserted_by,$crud_type);
        $account_details_1 = json_decode($account_detail, true); 
        $payment_head = 'Maintance';
        $amount = $total_pay_before_due_date;
        $payment_type = 'DE';
        $main_tran_by = 'O';
        $main_tran_rece_by = '';
        $previous_balance = $total_pay_before_due_date;
        flat_account_transaction($prk_admin_id,$m_account_number,$payment_head,$payment_type,$amount,$main_tran_by,$main_tran_rece_by,$inserted_by);
        if($account_details_1['status']){
            $add_late=add_late_fine_maintenance($prk_admin_id,$tower_id,$flat_id,$year,$month,$previous_balance,$main_mas_id,$m_account_number,$inserted_by,$tran_date,$bill_due_date);
        }
        $path=main_mas_bill_pdf_mail($prk_admin_id,$main_mas_id,$living_status,$owner_id,$owner_name,$owner_email,$tenant_id,$tenant_name,$tenant_email,$prk_area_name,$tower_name,$flat_name,$late_fine_charrge,$late_fine_rate_type);
    }else{
        $main_mas_id = $vall['main_mas_id'];
        $add_late=add_late_fine_maintenance($prk_admin_id,$tower_id,$flat_id,$year,$month,$previous_balance,$main_mas_id,$m_account_number,$inserted_by,$tran_date,$bill_due_date);
        $prin_t='NO';
    }
    return $prin_t.'||'.$bill_due_date.'||'.$add_late.'||'.$tran_date;
    // return $prin_t.'||'.$path;
}
function add_late_fine_maintenance($prk_admin_id,$tower_id,$flat_id,$year,$month,$previous_balance,$main_mas_id,$m_account_number,$inserted_by,$tran_date,$bill_due_date){
    global $pdoconn;
    $sql = "SELECT `main_child_id` FROM `maintenance_child` WHERE `main_mas_id`='$main_mas_id'AND  `prk_admin_id`='$prk_admin_id' AND `maint_id`='L_F' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count<=0){
        $days = (strtotime($bill_due_date) - strtotime($tran_date)) / (60 * 60 * 24);
        if ($days<0) {
            $current_due = 0;
            $tran_late_fine = maintenance_tran_late_fine($prk_admin_id,$year,$month,$previous_balance,$current_due);
            $m_tran_late_fine = json_decode($tran_late_fine, true); 
            $fine_charges = $m_tran_late_fine['fine_charges'];
            $maint_id = 'L_F';
            $maint_type = 'LATE FINE';
            $maint_charge = $fine_charges;
            maintenance_child($prk_admin_id,$main_mas_id,$tower_id,$flat_id,$year,$month,$maint_id,$maint_type,$maint_charge,$inserted_by);

            $crud_type='U';
            $account_due=$fine_charges;
            flat_account_details($prk_admin_id,$tower_id,$flat_id,$m_account_number,$account_due,$inserted_by,$crud_type);
            $payment_head = 'Late Fine';
            $amount = $fine_charges;
            $payment_type = 'DE';
            $main_tran_by = 'O';
            $main_tran_rece_by = '';
            flat_account_transaction($prk_admin_id,$m_account_number,$payment_head,$payment_type,$amount,$main_tran_by,$main_tran_rece_by,$inserted_by);
            return 'Y';
        }else{

            return 'N';
        }
    }else{

        return 'NN';
    }
}
?>