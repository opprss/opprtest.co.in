<?php 
/*
Description: parking area logout
Developed by: somen banerjee
Created Date: -------
Update date :22-06-2018
*/
// require_once 'api/add_api.php';
// require_once 'api/global_api.php';
require_once 'api/parkAreaReg_api.php';

 
$response = array();
 
if(isAvailable(array('token'))){
 	if(isEmpty(array('token'))){
	 	$token = trim($_POST['token']);
	 	$response= prk_area_logout($token);
	 	echo ($response);
 	}else{
		$response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        echo json_encode($response); 
	}

}else{
	$response['status'] = 0; 
	$response['message'] = 'Invalid API Call';
	echo json_encode($response); 
} 
?>