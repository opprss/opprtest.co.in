<?php 
/*
Description: Parking employ vehicle verify and data insert trc table.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
 // 'prk_admin_id','prk_area_user_id','in_prk_area_user_id','prk_user_username','token'
function employee_attendance_in($prk_admin_id,$prk_user_user,$prk_user_username){
    global $pdoconn;
    $response = array();
    $pieces = explode("-", $prk_user_user);
    $prk_user_user=$pieces[0];
    $prk_user_user=trim(base64_decode($prk_user_user));
    $sql ="SELECT prk_area_user_id,prk_admin_id FROM prk_area_user where prk_user_username='$prk_user_user' AND `active_flag`='".FLAG_Y."' AND del_flag='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $prk_area_user_id = $val['prk_area_user_id'];
        $user_prk_admin_id = $val['prk_admin_id'];
        if ($user_prk_admin_id==$prk_admin_id) {
            $sql ="SELECT * FROM `employee_attendance_list` WHERE `prk_admin_id`= '$prk_admin_id' AND `prk_area_user_id`= '$prk_area_user_id' AND IF(`out_time` IS NULL or `out_time` = '','Y','N')='Y'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $response['status'] = 0;
                $response['message'] = 'Employee Already IN';
            }else{
                $sql = "INSERT INTO `employee_attendance_list`(`prk_admin_id`,`prk_area_user_id`,`attendance_date`, `in_time`, `inserted_by`, `inserted_date`) VALUES ('$prk_admin_id','$prk_area_user_id','".TIME_TRN."','".TIME."','$prk_user_username','".TIME."')";
                $query = $pdoconn->prepare($sql); 
                if($query->execute()){
                    $id = $pdoconn->lastInsertId();
                    $response['status'] = 1;
                    $response['message'] = 'Employee IN Sucessful';
                }else{
                    $response['status'] = 0;
                    $response['message'] = 'Employee IN not Sucessful';
                }   
            }
        }else{
            $response['status'] = 0;
            $response['message'] = 'Wrong Area';
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'User ID Locked';
    }
    return json_encode($response); 
}
?>