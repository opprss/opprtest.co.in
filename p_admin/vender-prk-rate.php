<!-- ################################################
  
Description: Parking area admin can add/delete parking rate by this web page.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
 <?php 
include "all_nav/header.php"; 
@session_start();
if (isset($_SESSION['prk_admin_id'])) {
  $parking_admin_name=$_SESSION['parking_admin_name'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
    }

?>
<style>
  .size{
    font-size: 11px;
  }
.success{
  font-size: 11px;
  color: green;
}
</style>
<!-- vendor css -->
   
    <link href="../lib/highlightjs/github.css" rel="stylesheet">
    <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">

    <div class="am-mainpanel">
      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Parking Rate</h5>
        <!--<form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>-->
      </div>

      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40" >
          <!-- <h6 class="card-body-title">Add Employee</h6> -->
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form>
            <div class="row mg-b-25">
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">VEHICLE TYPE <span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="vehicle_type" id="vehicle_type" value="" style="opacity: 0.8; font-size:14px">
                      </select>
                    </div>
                    <span id="error_vehicle_type" style="position: absolute;" class="error_size"></span>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">RATE TYPE <span class="tx-danger">*</span></label>
                    <label class="rdiobox">
                      <input name="radio" id="fixed" value="F" type="radio">
                      <span>FIXED </span>
                    </label>

                    <label class="rdiobox">
                      <input name="radio" id="dynamic" value="D" type="radio" checked>
                      <span>DYANAMIC </span>
                    </label>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">INITIAL NO OF HOUR <span class="tx-danger">*</span></label>
                  <input type="text"  name="initial_hr" class="form-control" placeholder="Enter Initial No of HR" id="initial_hr">
                  <span id="error_initial_hr" style="position: absolute;" class="error_size"></span>
                </div>
              </div><!-- col-4 -->

              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">INITIAL HOUR(S) RATE <span class="tx-danger">*</span></label>
                  <input type="text" class="form-control" placeholder="Enter Initial HR rate" name="first_hour" id="first_hour">
                  <span id="error_first_h" style="position: absolute;" class="error_size"></span>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size"> ADDITINAL HOUR <span class="tx-danger">*</span></label>
                  <input type="text" name="rate_hour" class="form-control" placeholder="Enter ADDITINAL HOUR"  id="rate_hour">
                  <span id="error_rate_hour" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">ADDITINAL RATE <span class="tx-danger">*</span></label>
                  <input type="text" name="per_hr_rate" class="form-control" placeholder="Enter ADDITINAL RATE"  id="second_hour">
                  <span id="error_second_h" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <!-- HH -->
              <div class="col-lg-3 time">
                <div class="form-group">
                  <label class="form-control-label size">ROUND UP TIME (IN MINUTES)</label>
                  <div class="row">
                    <div class="col-lg-3 pd-r-0">
                      <input type="text"  class="form-control readonly" placeholder="HH" name="hh" id="hh" maxlength="2" value="00" readonly="readonly">
                    </div>
                    <div class="pd-t-9 mg-0">
                      <b>:</b>
                    </div>
                    <div class="col-lg-3 pd-l-0">
                      <input type="text"  class="form-control" placeholder="MM" name="mm" id="mm" maxlength="2">
                    </div>
                  </div>
                  <span id="error_prk_no_of_mt_dis" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">EFFECTIVE DATE <span class="tx-danger">*</span></label>
                  <input type="text" class="form-control prevent_readonly" placeholder="DD/MM/YYYY (Effective Date)"  id="start_date" readonly="readonly" style="background-color:transparent">
                  <!-- readonly="" style="background-color: white" -->
                  <span id="error_date" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">END DATE</label>
                  <input type="text"  name="end_date"  class="form-control prevent_readonly" placeholder="DD/MM/YYYY (End Date)" name="prk_add_pin" id="end_date" readonly="readonly" style="background-color:transparent">
                  <span id="error_end_date" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">NO OF PARKING SPACE <span class="tx-danger">*</span></label>
                  <input type="text" name="tot_prk_space" class="form-control" placeholder="total number of parking space"  id="tot_prk_space">
                  <span id="error_tot_prk_space" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-layout-footer mg-t-30">
                  <div class="row">
                    <div class="col-lg-6">
                      <input type="button" value="SAVE" style="padding-bottom: 9px; padding-top: 9px" class="btn btn-block btn-primary prk_button" name="save" id="save">
                      <div style="position: absolute;" id="success_submit" class="success"></div>
                    </div>
                    <div class="col-lg-6">
                      <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        </div>
      </div>   

      <div class="am-pagebody">

        <div class="card pd-20 pd-sm-40">
          <!-- <p class="mg-b-20 mg-sm-b-30"></p> -->

          <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Image</th>
                  <th class="">Rate Type</th>
                  <th class="">Initial No of HR</th>
                  <th class="">Initial rate</th>
                  <th class="">Rate hour</th>
                  <th class="">Rate Value</th>
                  <th class="">Parking Space</th>
                  <th class="">Effective Date</th>
                  <th class="">End Date</th>
                  <th class="">Round Up time</th>
                  <th class="">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
               $respon=park_area_rate_list($prk_admin_id);
               //print_r($respon); 
               $respon = json_decode($respon, true);
              foreach($respon as $value){
                $prk_vehicle_type=$value['prk_vehicle_type'];
                $prk_rate_id = $value['prk_rate_id'];
                 ?>

               <tr>
                  <td><img src="<?php echo OPPR_BASE_URL.$value['vehicle_typ_img']; ?>" style="height: 40px;"> </td>
                  <td><?php echo $value['prk_rate_type']; ?></td>
                  <td><?php echo $value['prk_ins_no_of_hr']; ?></td>
                  <td><?php echo $value['prk_ins_hr_rate']; ?></td>
                  <td><?php echo $value['veh_rate_hour']; ?></td>
                  <td><?php echo $value['veh_rate']; ?></td>
                  <td><?php echo $value['tot_prk_space']; ?></td>
                  <td><?php echo $value['prk_rate_eff_date']; ?></td>
                  <td><?php echo $value['prk_rate_end_date']; ?></td>
                  <td><?php echo $value['prk_no_of_mt_dis']; ?></td>
                  <td>

                    <a href="vender-prk-update?list_id=<?php echo base64_encode($value['prk_rate_id']); ?>" data-toggle="tooltip" data-placement="top" title="Modify"><!-- <button type="button" class="btn btn-info btn-md"> --><i class="fa fa-edit" style="font-size:18px"></i><!-- </button> --></a>

                    <button type="button" class="clean-button" name="delete" id="delete<%=count++%>" value="<?php echo $prk_rate_id; ?>" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
                 </td>
              </tr>
                <?php } ?>
         
                </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->
<!-- footer part -->
<?php include"all_nav/footer.php"; ?>
<style type="text/css">
  .error_size{
    font-size:11px;
    color: red;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){

    /*veh-type*/
    var urlVehicleType = prkUrl+'vehicle_type.php';
        // alert(urlVehicleType);
        $.ajax ({
            type: 'POST',
            url: urlVehicleType,
            data: "",
            success : function(data) {
                var obj=JSON.parse(data);
               var areaOption = "<option value='' style='color:gray;' selected='selected'>SELECT VEHICLE TYPE</option>";
               $.each(obj, function(val, key) {
                // alert("Value is :" + key['e']);
                areaOption += '<option value="' + key['vehicle_Sort_nm'] + '">' + key['vehicle_type'] + '</option>'
               });

              $("#vehicle_type").html(areaOption);

            }
        });



    $('input[type="radio"]').click(function(){
      if ($(this).is(':checked'))
      {
        var value=$(this).val();
        if(value=='F'){
          //$('input[name="initial_hr"]').val('24');
          $('#initial_hr').val('24');
          $('input[name="initial_hr"]').attr('readonly', true);
          $('input[name="per_hr_rate"]').val('0');
          $('input[name="per_hr_rate"]').attr('readonly', true);
          $('input[name="rate_hour"]').attr('readonly', true);
          $('input[name="rate_hour"]').val('0');

          //$('input[name="hh"]').attr('readonly', true);
          $('input[name="mm"]').attr('readonly', true);
          $('#error_second_h').hide();
          $('#error_initial_hr').hide();

          $('input[name="initial_hr"]').addClass("readonly");
          $('input[name="per_hr_rate"]').addClass("readonly");
          $('input[name="rate_hour"]').addClass("readonly");

          //$('input[name="hh"]').addClass("readonly");
          $('input[name="mm"]').addClass("readonly");
        }else{
          $('input[name="initial_hr"]').val('');
          $('input[name="initial_hr"]').attr('readonly', false);
          $('input[name="per_hr_rate"]').val('');
          $('input[name="per_hr_rate"]').attr('readonly', false);
          $('input[name="rate_hour"]').attr('readonly', false);
          $('input[name="rate_hour"]').val('');
          //$('input[name="hh"]').attr('readonly', false);
          $('input[name="mm"]').attr('readonly', false);
          $('#error_second_h').show();
          $('input[name="initial_hr"]').removeClass("readonly");
          $('input[name="per_hr_rate"]').removeClass("readonly");
          $('input[name="rate_hour"]').removeClass("readonly");
          $('input[name="mm"]').removeClass("readonly");
        }
        
      }
  });

    /*reset button*/

    $('#reset').click(function(){
      $('input[name="initial_hr"]').val('');
      $('input[name="initial_hr"]').attr('readonly', false);
      $('input[name="per_hr_rate"]').val('');
      $('input[name="per_hr_rate"]').attr('readonly', false);
      $('input[name="prk_no_of_mt_dis"]').attr('readonly', false);
      $('#error_second_h').show();
      $('input[name="initial_hr"]').removeClass("readonly");
      $('input[name="per_hr_rate"]').removeClass("readonly");
      $('input[name="prk_no_of_mt_dis"]').removeClass("readonly");
    });
   });
</script>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE_WEB ?>"
  /*delete popup*/  
  $('button[id^="delete"]').on('click', function() {
    var prk_admin_id = "<?php echo $prk_admin_id;?>";
    var user_name=' <?php echo $parking_admin_name; ?>';
    var prk_rate_id = this.value;
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the item permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            if (prk_rate_id != '') {
              //alert(prk_rate_id);
              var urlDtl = prkUrl+'prk_area_rate_delete.php';
              //alert(urlDtl);
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :
                {
                  'prk_rate_id':prk_rate_id,
                  'user_name':user_name,
                  'prk_admin_id':prk_admin_id,
                  'token': '<?php echo $token;?>'
                },
                dataType:'html',
                success  :function(data)
                {
                  //alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    location.reload(true);
                  }else{
                    if( typeof json.session !== 'undefined'){
                      if (!json.session) {
                        window.location.replace("logout.php");
                      }
                    }else{
                      $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                    }
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  });
  /*/delete popup*/
  $(document).ready(function(){
	
    $('#reset').click(function(){
      $('.error_size').text('');
    });
    
    
    $('#save').click(function(){
      var mm = $('#mm').val();
      var length = mm.length;
      /*if (mm == '') {
         mm = '00';
      }
      if(length < 2){
        mm = '0'+mm;
      }*/
      if (mm == '') {
         mm = '00';
      }else if(mm== 0){
        mm = '00';
      }else if(length<2){
         mm = '0'+mm;
      }
      var prk_rate_end_date=$('#end_date').val();
      prk_rate_end_date = (!prk_rate_end_date == '') ? prk_rate_end_date : FUTURE_DATE;
    
      var prk_no_of_mt_dis = '00:'+mm;
      var prk_vehicle_type =$("#vehicle_type").find("option:selected").val();          
      var prk_rate_type= document.querySelector('input[name="radio"]:checked').value;
      var prk_ins_no_of_hr=$('#initial_hr').val();
      var prk_ins_hr_rate = $('#first_hour').val();
      var veh_rate = $('#second_hour').val();
      var rate_hour = $('#rate_hour').val();
      var tot_prk_space = $('#tot_prk_space').val();
      var prk_rate_eff_date = $('#start_date').val();
      var prk_admin_id=' <?php echo $prk_admin_id; ?>';
      var user_name=' <?php echo $parking_admin_name; ?>';
      //alert(prk_ins_hr_rate);
      if(prk_ins_no_of_hr!='' && veh_rate!='' && rate_hour!='' && prk_vehicle_type!='' && prk_rate_eff_date!='' && tot_prk_space!='' && prk_ins_hr_rate !=''){
        //'prk_vehicle_type','prk_admin_id','prk_rate_type','prk_ins_no_of_hr','prk_ins_hr_rate','prk_no_of_mt_dis','prk_rate_eff_date','prk_rate_end_date','user_name','tot_prk_space','veh_rate_hour','veh_rate'
        $('#save').val('Wait ...').prop('disabled', true);
        var urlPrkAreRate = prkUrl+'prk_area_rate.php';
        $.ajax({
          url :urlPrkAreRate,
          type:'POST',
          data :
          {
            'prk_vehicle_type':prk_vehicle_type,
            'prk_admin_id':prk_admin_id,
            'prk_rate_type':prk_rate_type,
            'prk_ins_no_of_hr':prk_ins_no_of_hr,
            'prk_ins_hr_rate':prk_ins_hr_rate,
            'tot_prk_space':tot_prk_space,
            'prk_rate_eff_date':prk_rate_eff_date,
            'prk_rate_end_date':prk_rate_end_date,
            'user_name':user_name,
            'prk_no_of_mt_dis':prk_no_of_mt_dis,
            'veh_rate_hour':rate_hour,
            'veh_rate':veh_rate,
            'token': '<?php echo $token;?>'
          },
          dataType:'html',
          success  :function(data)
          {
            $('#save').val('SAVE').prop('disabled', false);
            //alert(data);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success',
                content: "<p style='font-size:0.9em;'>Rate added successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                      location.reload(true);
                  }
                }
              });
            }else{
              if( typeof json.session !== 'undefined'){
                if (!json.session) {
                  window.location.replace("logout.php");
                }
              }else{
                $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                type: 'red'
              });
              }
            }
          }
        });
      }
      else{

      if (prk_ins_hr_rate== '') {
        $('#error_first_h').text('Initial hour rate is required');         
          //return false;
      }if (veh_rate == '') {
      $('#error_second_h').text('Rate value is required');
        //return false;
      }if (rate_hour == '') {
        $('#error_rate_hour').text('Rate hour is required');
      } if (prk_vehicle_type == '') {
      $('#error_vehicle_type').text('Vehicle type is required');
        //return false;
      }if (prk_rate_eff_date == '') {
      $('#error_date').text('Effective date is required');
        //return false;
      }if (tot_prk_space == '') {
      $('#error_tot_prk_space').text('Totat parking space is required');
        //return false;
      }if (prk_ins_no_of_hr == '') {
      $('#error_initial_hr').text('Initial no of hour is required');
        //return false;
      }

    }
  });
  // Initial no fo HR
  $("#initial_hr").blur(function () {
      var first_hour = $('#initial_hr').val();
      if(first_hour==''){
      $('#error_initial_hr').text('Initial no of hour is required');
      //$('#initial_hr').focus();

      }else{
        $('#error_initial_hr').text('');
      }
  });
  // validation 1st hour
  $("#first_hour").blur(function () {
      var first_hour = $('#first_hour').val();
      if(first_hour==''){
      $('#error_first_h').text('Initial no of rate is required');
      //$('#first_hour').focus();

      }else{
        $('#error_first_h').text('');;
      }
  });
  // validation 2nd hour
  $("#second_hour").blur(function () {
    var second_hour = $('#second_hour').val();
    if(second_hour==''){
    $('#error_second_h').text('Rate value is required');
    //$('#second_hour').focus();

    }
    else{
      $('#error_second_h').text('');
    }
  });
  $("#rate_hour").blur(function () {
    var rate_hour = $('#rate_hour').val();
    if(rate_hour==''){
      $('#error_rate_hour').text('Rate hour is required');
      return false;
    }
    else{
      $('#error_rate_hour').text('');
      return true;
    }
  });
  // validation date
  $("#start_date").blur(function () {
    var start_date = $('#start_date').val();
    if(start_date==''){
    $('#error_date').text('Effective date is required');
    //$('#start_date').focus();

    }
    else{
      $('#error_date').text('');;
    }
  });
  /*veh type*/
  $("#vehicle_type").blur(function () {
    var prk_rate_type= document.querySelector('input[name="radio"]:checked').value;
    if(prk_rate_type==''){
      $('#error_vehicle_type').text('Vehicle type required');
    }else{
      $('#error_vehicle_type').text('');;
    }
  });
  $("#tot_prk_space").blur(function () {
      var tot_prk_space = $('#tot_prk_space').val();
      if(tot_prk_space==''){
      $('#error_tot_prk_space').text('Totat parking space is required');
      //$('#tot_prk_space').focus();

      }else{
        $('#error_tot_prk_space').text('');;
      }
  });
  /*round up time*/
  $("#mm").keyup(function(){
      var mm = $('#mm').val()
      if (mm > 15 && mm < 59){
        $.confirm({
          icon: 'fa fa-exclamation-circle',
          theme: 'modern',
          title: 'Hold on!',
          content: "<p style='font-size:0.9em;'>Round up time is more then <b><span style='color:red'>'"+mm+"'</span></b> minute. Are you sure? </p>",
          type: 'red',
          buttons: {
            Yes: function () {
              //ok no job
            },
            No: function () {
              $('#mm').val('');
            }
          }
        });
      }else if(mm > 59){
        $('#mm').val('59');
      }
  });
  // allow only numeric value for rat
  $("#first_hour,#second_hour,#initial_hr,#tot_prk_space,#mm,#rate_hour").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0)){
      event.preventDefault();
    }
  });
});
</script>
<script>
  //select state
  $(document).ready(function(){
    //pin number validation only accept numeric value
    $(function() {
      $('#staticParent').on('keydown', '#pin', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
    })
  });
  /*date*/
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#start_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
            if (date < now) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            $('#error_date').text('');
            return {};
        } 
    });
  });
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#start_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
            if (date < now_end) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            return {};
        } 
    });
  });
  $('#start_date').focus(function(){
    $('#end_date').val('');
  });
</script>
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>

<script>
  $(function(){
    'use strict';

    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    $('#datatable2').DataTable({
      bLengthChange: false,
      searching: false,
       "scrollX": true
    });
    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
</script>
<!-- for tool tipe -->
<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
  });
  function upperCase(a){
    setTimeout(function(){
      a.value = a.value.toUpperCase();
    }, 1);
  }
</script>
<style type="text/css">
  .date{background-color: white}
</style>