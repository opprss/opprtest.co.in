<?php
/*
Description: Parking adimn register meil check.
Developed by: Rakhal Raj Mandal
Created Date: 19-05-2018
Update date : ---------
*/ 
require_once 'api/parkAreaReg_api.php';
// include_once 'api/global_api.php';
$respon = array();
$response = array();
$responsen = array();
if(isAvailable(array('email'))){
     if(isEmpty(array('email'))){

        $email = trim($_POST['email']);
        $response = accountStatusEmail($email);
        $response = json_encode($response);  
     }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
}else{
    $response['status'] = 0;
    $response['message'] = 'Invalid API Call';
     $response = json_encode($response);
}
echo $response;
?> 