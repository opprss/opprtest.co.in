<?php 
/*
Description: User QR code scan and out parking area.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_veh_trc_dtl_id','quercode_value','user_mobile','name'))){
 	if(isEmpty(array('prk_veh_trc_dtl_id','quercode_value','user_mobile','name'))){	
 		$prk_veh_trc_dtl_id = trim($_POST['prk_veh_trc_dtl_id']);
	 	$quercode_value = trim($_POST['quercode_value']);
	 	$user_mobile = trim($_POST['user_mobile']);
	 	$user_nick_name = trim($_POST['name']);
        $who_veh_out = FLAG_U;
 		global $pdoconn;
	    $response = array();
	    $allplayerdata = array();
	    $park = array();
	   
	    $sql="SELECT `user_admin_id`,`prk_admin_id`,`prk_veh_type`,`veh_number`,`prk_veh_in_time`,`payment_type`,`gate_pass_num` FROM `prk_veh_trc_dtl` WHERE `prk_veh_trc_dtl_id`='$prk_veh_trc_dtl_id'";
	    $query  = $pdoconn->prepare($sql);
	    $query->execute();
	    $val = $query->fetch();
	    $prk_admin_id= $val['prk_admin_id'];
	    $user_admin_id = $val['user_admin_id'];
	    $prk_veh_type = $val['prk_veh_type'];
	    $veh_number = $val['veh_number'];
	    $prk_veh_in_time = $val['prk_veh_in_time'];
	    $gate_pass_num = $val['gate_pass_num'];

	    $prk_user_fi=prk_user_find($quercode_value);
    	$json = json_decode($prk_user_fi);
	 	if($json->status=='1'){
	 		$prk_area_gate_type=$json->prk_area_gate_type;
		 	$user_name=$json->user_name;
		 	$prk_admin_id1=$json->prk_admin_id;
		 	$prk_area_gate_id=$json->prk_area_gate_id;
		 	if($prk_admin_id==$prk_admin_id1){
			 	if($prk_area_gate_type == FLAG_O || $prk_area_gate_type == FLAG_B){ 

				 	$rate_cal = rate_calculate($prk_veh_trc_dtl_id,$user_mobile,$prk_admin_id,$prk_area_gate_id,$who_veh_out);
					$json = json_decode($rate_cal);
				 	if($json->status=='1'){
				 		$payment_dtl_id = $json->payment_dtl_id;
		            	$response = prk_veh_out_dtl($payment_dtl_id);
		            	vehicle_out_scan_push_notification_employee($prk_admin_id,$prk_area_gate_id,$veh_number);
		            }else{
					    $response['status'] = 0; 
					    $response['message'] = 'Vehicle Out Unsuccessful';
					 	$response = json_encode($response);
		            }
		        }else{
					$response['status'] = 0;
			        $response['message'] = 'This is Wrong Gate';
			        $response = json_encode($response); 
				}
			}else{
				$response['status'] = 0;
		        $response['message'] = 'This is Wrong Parking Area';
		        $response = json_encode($response); 
			}
		}else{
	    	$response=$prk_user_fi;
	    	// echo ($response);
	    }
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 	$response = json_encode($response);
}
echo ($response);
?>