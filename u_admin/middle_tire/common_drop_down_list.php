<?php 
/*
Description: all city show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
require_once 'api/reg_api.php';
$respon = array();
if(isAvailable(array('category_type'))){
    if(isEmpty(array('category_type'))){
        $category_type = trim($_REQUEST['category_type']);
        $respon = common_drop_down_list($category_type);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $respon = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $respon = json_encode($response); 
} 
echo $respon;
?>