<?php
/*
Description: email send your email. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function email_massage($email,$message){
	$to  = $email; 
	$headers='';
	$subject = 'Welcome to OPPR Software Solution';
	$headers .= 'X-Mailer: PHP/' . phpversion();
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= 'From: OPPR Software Solution <noreply@opprss.co.in>' . "\r\n";
	$headers .= "X-Priority: 3\r\n";
	$headers .= "X-Mailer: PHP". phpversion() ."\r\n";
	mail($to, $subject, $message, $headers);  
	return true;
	/*if(mail($to, $subject, $message, $headers))
		return true;
	else
	    return false; */
}
function email_massage_new($to_email,$message,$subject){
	$to  = $to_email; 
	$headers='';
	// $subject = 'Welcome to OPPR Software Solution';
	$subject = 'Welcome to OPPR Software Solution';
	$headers .= 'X-Mailer: PHP/' . phpversion();
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= 'From: OPPR Software Solution <noreply@opprss.co.in>' . "\r\n";
	$headers .= "X-Priority: 3\r\n";
	$headers .= "X-Mailer: PHP". phpversion() ."\r\n";
	mail($to, $subject, $message, $headers);  
	return true;
	/*if(mail($to, $subject, $message, $headers))
		return true;
	else
	    return false;*/  
}
function mail_attachment($filename, $file, $mailto, $from_name, $replyto, $subject, $message) {
	$from_mail='noreply@opprss.co.in';
	$file_size = filesize($file);
	$handle = fopen($file, "r");
	$content = fread($handle, $file_size);
	fclose($handle);
	$content = chunk_split(base64_encode($content));
	$uid = md5(uniqid(time()));
	$header = "From: ".$from_name." <".$from_mail.">\r\n";
	$header .= "Reply-To: ".$replyto."\r\n";
	$header .= "MIME-Version: 1.0\r\n";
	$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
	$header .= "This is a multi-part message in MIME format.\r\n";
	$header .= "--".$uid."\r\n";
	$header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
	$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
	$header .= $message."\r\n\r\n";
	$header .= "--".$uid."\r\n";
	$header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
	$header .= "Content-Transfer-Encoding: base64\r\n";
	$header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
	$header .= $content."\r\n\r\n";
	$header .= "--".$uid."--";
	if (mail($mailto, $subject, "", $header)) {
		return true;
		// echo "mail send ... OK"; // or use booleans here
	} else {
		return false;
		// echo "mail send ... ERROR!";
	}
}
?>