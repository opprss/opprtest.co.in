<?php 
/*
Description: user vehicle delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
 require_once 'api/reg_api.php';
// include_once 'api/global_api.php'; 
$response = array();
if(isAvailable(array('user_veh_dtl_id','user_admin_id','mobile','token'))){
 	if(isEmpty(array('user_veh_dtl_id','user_admin_id','mobile','token'))){
	 	$user_veh_dtl_id = trim($_POST['user_veh_dtl_id']);
	 	$user_admin_id = trim($_POST['user_admin_id']);
	 	$mobile = trim($_POST['mobile']);
        $token = ($_POST['token']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $respon=add_vehicle_delete($user_veh_dtl_id,$user_admin_id,$mobile);
            echo ($respon);
        }else{
            $response['status'] = 0; 
            $response['session'] = 0;
            $response['message'] = 'Session Expired Please Login Again';
            echo json_encode($response);
        }
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?>