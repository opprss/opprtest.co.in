<!-- ################################################
  
Description: Parking area can add/delete parking gate with description.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php include "all_nav/header.php";?>
<?php
$tower_id=base64_decode($_REQUEST['list_id']);
$tower_name='';
$tower_short_name='';
$effective_date='';
$end_date='';
$crud_type='S';
$tower_details=tower_details($prk_admin_id,$tower_name,$tower_short_name,$effective_date,$end_date,$parking_admin_name,$tower_id,$crud_type);
$tower_details_show = json_decode($tower_details, true);
if ($tower_details_show['end_date']==FUTURE_DATE) {
  # code...
  $end_date='';
}else{

  $end_date=$tower_details_show['end_date'];
}
?>
<!-- header position -->
  <style type="text/css">
    .size{
      font-size: 11px;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .error{
      font-size: 11px;
      color: red;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .success{
      font-size: 11px;
      color: green;
    }
  </style>
    <!-- links for datatable -->
    <link href="../lib/highlightjs/github.css" rel="stylesheet">
    <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
  
    <div class="am-mainpanel">

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Tower Info Modify</h5>
      </div>
      
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40" >
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form id="park_tower">
              <!-- <form role="form" id="park_tower" method="post"  enctype="multipart/form-data"> -->
              <div class="row mg-b-25">
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">TOWER NAME <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Tower Name" name="tower_name" id="tower_name">
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">TOWER SHORT NAME <span class="tx-danger"></span></label>
                    <input type="text" class="form-control" placeholder="Short Name" name="tower_short_name" id="tower_short_name">
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">EFFECTIVE DATE<span class="tx-danger">*</span></label>
                    <input type="text" name="effective_date" id="effective_date" placeholder="DD-MM-YYYY" class="form-control">
                    <span style="position: absolute; display: none;" id="error_effective_date" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">END DATE</label>
                    <input type="text" name="end_date" id="end_date" placeholder="DD-MM-YYYY" class="form-control prevent_readonly" readonly="readonly">
                   <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-layout-footer mg-t-30">
                    <div class="row">
                      <div class="col-lg-6">
                        <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
                      </div>
                      <div class="col-lg-6">
                        <a href="tower-details"><button class="btn btn-block btn-primary prk_button" type="button">BACK</button></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
<?php include"all_nav/footer.php"; ?>
<script type="text/javascript">
  /*date*/
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#end_date', {
      default_date   : false,
      position       : 'bottom',
      format  : 'Y-m-d',
      hide_on_select : true,
      render : function (date) {
        if (date < now) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
        }
        $('#error_effective_date').text('');
        return {};
        } 
    });
  });
</script>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE ?>";
  $("#tower_name").val("<?=$tower_details_show['tower_name']?>");
  $("#tower_short_name").val("<?=$tower_details_show['tower_short_name']?>");
  $("#effective_date").val("<?=$tower_details_show['eff_date']?>");
  if ("<?=$tower_details_show['end_date']?>"==FUTURE_DATE) {
    var end_date_o='';
  }else{

    var end_date_o="<?=$tower_details_show['end_date']?>";
  }
  $("#end_date").val(end_date_o);
  $('#tower_name').prop('disabled', true);
  $('#effective_date').prop('disabled', true);
  var prk_admin_id="<?=$prk_admin_id?>"
  var parking_admin_name="<?=$parking_admin_name?>"
  var token="<?=$token?>"
  var tower_id="<?=$tower_id?>"
  var urlPrkAreGateDtl = prkUrl+'tower_details.php';
  // alert(token);
  // $( document ).ready( function () {
  $('#save').click(function(){
    var effective_date=($("#effective_date").val());
    var end_date=($("#end_date").val());

    /*var str_array = end_date.split('-');
    var dd = str_array[0];
    var mm = str_array[1];
    var yy = str_array[2];
    var end_dat = yy+'-'+mm+'-'+dd;*/
    end_date = (!end_date == '') ? end_date : FUTURE_DATE;
    var tower_name=($("#tower_name").val());
    var tower_short_name=($("#tower_short_name").val());
    // alert(end_date);
    /*var str_array = effective_date.split('-');
    var dd = str_array[0];
    var mm = str_array[1];
    var yy = str_array[2];
    var effective_dat = yy+'-'+mm+'-'+dd;*/

    if($("#park_tower").valid()&effective_date!=''){
      // alert('ok');
       $('#save').val('Wait ...').prop('disabled', true);
      $.ajax({
        url :urlPrkAreGateDtl,
        type:'POST',//'prk_admin_id','tower_name','effective_date','end_date','parking_admin_name','token','tower_id','crud_type'
        data :{
          'prk_admin_id':prk_admin_id,
          'tower_name':tower_name,
          'tower_short_name':tower_short_name,
          'effective_date':effective_date,
          'end_date':end_date,
          'parking_admin_name':parking_admin_name,
          'tower_id':tower_id,
          'crud_type':'U',
          'token': token
        },
        dataType:'html',
        success  :function(data){
          $('#save').val('SAVE').prop('disabled', false);
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>Tower Details Updated Successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                    window.location.href='tower-details';
                }
              }
            });
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>Something went wrong</p>",
              type: 'red'
            });
            }
          }
        }
      });
    }
  });
</script>
<script type="text/javascript">
  $( document ).ready( function () {
    $( "#park_tower" ).validate( {
      rules: {
        tower_name: "required",
        effective_date: "valueNotEqualsEff"
      },
      messages: {
      }
    });
    $("#effective_date").blur(function () {
      var effective_date = $('#effective_date').val();
      if(effective_date == ''){
        $('#error_effective_date').show();
        return false;
      }else{
        $('#error_effective_date').hide();
        return true;
      }
    });
  });
  $.validator.addMethod("valueNotEqualsEff", function(value){
    // alert('ok');
    arg = "";
    if (value != "") {
      return arg !== value;
    }else{
      $("#error_effective_date").show();
      return true;
    }
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>