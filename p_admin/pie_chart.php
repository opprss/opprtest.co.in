<?php
require_once ('jpgraph/src/jpgraph.php');
require_once ('jpgraph/src/jpgraph_pie.php');
require_once ('jpgraph/src/jpgraph_pie3d.php');
// include('web/vehicle_chart_by_date.php');
 
include('web/api/graph.php'); 
$end_date = date("Y-m-d");
$start_date= date('Y-m-d', strtotime('-7 days'));

$response = vehicle_chart_by_date($start_date,$end_date);
$json = json_decode($response,true);
  $two = $json['2W']; 
 $three = $json['3W'];
 $four = $json['4W'];
 $eight = $json['8W'];
 $data=array($two,$three,$four,$eight);

 
// $data = array(40,60,21,33);
 
$graph = new PieGraph(300,200);
$graph->SetShadow();
 
$graph->title->Set("All Vehicle");
$graph->title->SetFont(FF_FONT1,FS_BOLD);
 
$p1 = new PiePlot3D($data);
$p1->SetAngle(20);
$p1->SetSize(0.5);
$p1->SetCenter(0.45);
// $p1->SetLegends($gDateLocale->GetShortMonth('aa'));
$p1->SetLegends(array('2W','3W','4W','8W'));
$graph->Add($p1);
$graph->Stroke();
?>