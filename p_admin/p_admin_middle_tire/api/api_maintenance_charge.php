<?php
/*
Description: parking area employ create.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function maintenance_charge($prk_admin_id,$maint_type,$maint_charge,$maint_type_short,$payable_time,$holder_type,$effective_date,$end_date,$parking_admin_name,$crud_type,$maint_id){
   $response = array();
   $allplayerdata = array();
    global $pdoconn;
    switch ($crud_type) {
        case 'I':
            $id=uniqid();
            $sql="INSERT INTO `maintenance_charge`(`maint_id`, `prk_admin_id`, `maint_type`, `maint_charge`, `maint_type_short`, `payable_time`, `holder_type`, `effective_date`, `end_date`, `inserted_by`, `inserted_date`) VALUES ('$id','$prk_admin_id','$maint_type','$maint_type_short','$maint_charge','$payable_time','$holder_type','$effective_date','$end_date','$parking_admin_name','".TIME."')";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Maintenance Add';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Maintenance Not Add';
            } 
            break;
        case 'U':
            $sql="UPDATE `maintenance_charge` SET `active_flag`='".FLAG_N."',`updated_by`='$parking_admin_name',`updated_date`='".TIME."' WHERE `maint_id`='$maint_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();

            $sql="INSERT INTO `maintenance_charge`(`maint_id`, `prk_admin_id`, `maint_type`, `maint_type_short`, `maint_charge`, `payable_time`, `holder_type`, `effective_date`, `end_date`, `inserted_by`, `inserted_date`) VALUES ('$maint_id','$prk_admin_id','$maint_type','$maint_charge','$maint_type_short','$payable_time','$holder_type','$effective_date','$end_date','$parking_admin_name','".TIME."')";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Flat Modify Save';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Flat Modify Not Save ';
            }  
            break;
        case 'S':
            $sql="SELECT mc.`maint_id`,
            mc.`prk_admin_id`,
            mc.`maint_type`,
            mc.`maint_type_short`,
            mc.`maint_charge`,
            mc.`payable_time`,
            (SELECT `full_name` FROM `all_drop_down` WHERE `sort_name`=mc.`payable_time` AND `category`='APT_CH_PAY_TI') as payable_time_full,
            mc.`holder_type`,
            (SELECT `full_name` FROM `all_drop_down` WHERE `sort_name`=mc.`holder_type` AND `category`='APT_FLT_HLD') as holder_type_full,
            mc.`effective_date`,
            mc.`end_date`,
            IF('".TIME_TRN."' BETWEEN mc.`effective_date` AND mc.`end_date`, '".FLAG_Y."', '".FLAG_N."') as activ_status 
            FROM `maintenance_charge` mc WHERE 
            mc.`prk_admin_id`='$prk_admin_id' 
            AND mc.`active_flag`='".FLAG_Y."' 
            AND mc.`del_flag`='".FLAG_N."'
            AND mc.`maint_id`='$maint_id'";

            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['maint_id'] =  $val['maint_id'];
                $response['prk_admin_id'] =  $val['prk_admin_id'];
                $response['maint_type'] =  $val['maint_type'];
                $response['maint_type_short'] =  $val['maint_type_short'];
                $response['maint_charge'] =  $val['maint_charge'];
                $response['payable_time'] =  $val['payable_time'];
                $response['payable_time_full'] =  $val['payable_time_full'];
                $response['holder_type'] =  $val['holder_type'];
                $response['holder_type_full'] =  $val['holder_type_full'];
                $response['effective_date'] =  $val['effective_date'];
                $response['end_date'] =  $val['end_date'];
                $response['activ_status'] =  $val['activ_status'];
            }else{
                $response['status'] = 0;
                $response['message'] = 'Not Successful';
            }  
            break;
        case 'LS':
            $sql="SELECT mc.`maint_id`,
            mc.`prk_admin_id`,
            mc.`maint_type`,
            mc.`maint_type_short`,
            mc.`maint_charge`,
            mc.`payable_time`,
            (SELECT `full_name` FROM `all_drop_down` WHERE `sort_name`=mc.`payable_time` AND `category`='APT_CH_PAY_TI') as payable_time_full,
            mc.`holder_type`,
            (SELECT `full_name` FROM `all_drop_down` WHERE `sort_name`=mc.`holder_type` AND `category`='APT_FLT_HLD') as holder_type_full,
            mc.`effective_date`,
            mc.`end_date`,
            IF('".TIME_TRN."' BETWEEN mc.`effective_date` AND mc.`end_date`, '".FLAG_Y."', '".FLAG_N."') as activ_status 
            FROM `maintenance_charge` mc WHERE 
            mc.`prk_admin_id`='$prk_admin_id' 
            AND mc.`active_flag`='".FLAG_Y."' 
            AND mc.`del_flag`='".FLAG_N."'
            ORDER BY mc.`mant_unique_id` DESC";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $arr_catagory = $query->fetchAll();
                foreach($arr_catagory as $val){
                    $park['maint_id'] =  $val['maint_id'];
                    $park['prk_admin_id'] =  $val['prk_admin_id'];
                    $park['maint_type'] =  $val['maint_type'];
                    $park['maint_type_short'] =  $val['maint_type_short'];
                    $park['maint_charge'] =  $val['maint_charge'];
                    $park['payable_time'] =  $val['payable_time'];
                    $park['payable_time_full'] =  $val['payable_time_full'];
                    $park['holder_type'] =  $val['holder_type'];
                    $park['holder_type_full'] =  $val['holder_type_full'];
                    $park['effective_date'] =  $val['effective_date'];
                    $park['end_date'] =  $val['end_date'];
                    $park['activ_status'] =  $val['activ_status'];
                    array_push($allplayerdata, $park);
                }
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['maint_list'] = $allplayerdata;
            }else{
                $response['status'] = 0;
                $response['message'] = 'List Empty';
            }
            break;
        case 'D':
            $sql="UPDATE `maintenance_charge` SET `del_flag`='".FLAG_Y."',`updated_by`='$parking_admin_name',`updated_date`='".TIME."' WHERE `maint_id`='$maint_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Delete Successful';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Delete Not Successful';
            }
            break;
        default:
            $response['status'] = 0;
            $response['message'] = 'Wrong Select';
            break;
    }
    return json_encode($response);  
}
?>