<?php 
/*
Description: QR code scan and parking area gate and user found.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/

require_once 'api/reg_api.php';
// require_once 'api/global_api.php';
 
$response = array();
 
if(isAvailable(array('quercode_value'))){

 	if(isEmpty(array('quercode_value'))){

	 	$quercode_value = trim($_POST['quercode_value']);
	 	$response= prk_user_find($quercode_value);
	 	echo ($response);

 	}else{
 			$response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response); 
	 }

 }else{
	 $response['status'] = 0; 
	 $response['message'] = 'Invalid API Call';
     echo json_encode($response); 
 } 
?>