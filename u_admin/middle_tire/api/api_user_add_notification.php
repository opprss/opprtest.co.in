<?php
/*
Description: user vehicle list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_add_notification($user_mobile,$user_name,$tower_name,$falt_name,$prk_area_email,$prk_area_rep_name){
    
    $email_logo = EMAIL_LOGO."oppr-logo.png";
    $email_logo_fb=EMAIL_LOGO_fb."fb.jpg";
    $email_logo_gmail=EMAIL_LOGO_gmail."google.png";
    $email_logo_indeed=EMAIL_LOGO_indeed."inidied.png";
    $message = '
        <html>
          <body>
              <div style="border: 1px solid #00b8d4; width: 600px;">
                <div style="background-color: #00b8d4; height: 80px; width: 600px;">
                  <span style="float: left;">
                 <img src="'.$email_logo.'" height="50" width="100">
                  </span>
                  <br><div style="margin-top: 5px">
                    <a href="#" style="margin-left: 350px;width: 20px;height: 20px;">
                      
                        <img src="'.$email_logo_fb.'" alt="FaceBook" height="30" width="30" / >
                    </a>

                   <span class="google">
                    <a href="#" >

                      <img src="'.$email_logo_gmail.'" alt="Gmail" height="30" width="30" />
                    </a>
                  </span>
                   <span class="linkedin">
                    <a href="#">
                     <img src="'.$email_logo_indeed.'" height="30" width="30" alt="Indeed" />
                    </a>
                  </span>
                </div>

                  <div style="margin-top: 80px; margin-left: 20px;">
                    <span>
                      <b>Hi, '.$prk_area_rep_name.'</b>
                    </span>
                  </div>
                  <div style="margin-top: 20px; margin-left: 30px;width: 555px">
                   <p>In your residential complex new member adding as address details give me <br> below:-      <br>

                   <p>Name : '.$user_mobile.' </p>
                   <p>Mobile : '.$user_name.' </p>
                   <p>Tower : '.$tower_name.' </p>
                   <p>Flat : '.$falt_name.' </p>
                   </p>
                  </div>
                  <div style="margin-top: 10px; margin-left: 20px;">

                      <b>Regards,</b><br>
                      <p>OPPR Team.<br>
                      Digital Parking Solution.
                    </p><div style="width: 555px">
                       &emsp;"The information contained in this electronic message and any attachments to this message are intended for exclusive use of the addressee(s) and may contain confidential or privileged information. If you are not the intended recipient, please notify the sender at OPPR Software Solution Pvt Ltd or care@opprss.com immediately and
                    destroy all copies of this message and any attachments. The views expressed in this E-mail message / Attachments, are those of the individual sender."
                    </div>
                  </div>

                  </div>
                  <div style="margin-top: 520px; text-align: center;">
                  <span><a href="http://opprss.com/about-us">About Us</a> |</span> <span><a href="http://opprss.com/contact-us">Contect Us </a></span>
                  </div>
                  <div style="text-align: center; margin-top: 20px;padding-bottom: 20px; font-size: 11px"> &copy; 2017 BY OPPR SOFTWARE SOLUTION. ALL RIGHTS RESERVED.<br><br>
                  </div>
                </div>
              </div>
          </body>
        </html>
        ';
    // return ($message);
    email_massage($prk_area_email,$message); 
    $response['status'] = 1;
    $response['message'] = 'Successful';
    return json_encode($response);
}
?>