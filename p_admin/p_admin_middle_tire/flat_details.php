<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','tower_name','flat_name','effective_date','end_date','parking_admin_name','token','flat_id','crud_type','flat_size','two_wh_spa','four_wh_spa'))){
    if(isEmpty(array('prk_admin_id','parking_admin_name','token','crud_type'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
        $tower_name = trim($_POST['tower_name']);
        $flat_name = trim($_POST['flat_name']);
        $effective_date = trim($_POST['effective_date']);
        $end_date = trim($_POST['end_date']);
        $parking_admin_name = trim($_POST['parking_admin_name']);
        $flat_id = trim($_POST['flat_id']);
        $crud_type = trim($_POST['crud_type']);
        $flat_size = trim($_POST['flat_size']);
        $two_wh_spa = trim($_POST['two_wh_spa']);
        $four_wh_spa = trim($_POST['four_wh_spa']);
        $token = trim($_POST['token']);
        $resp=prk_token_check($prk_admin_id,$token);
            $json = json_decode($resp);
            if($json->status){
                $response = flat_details($prk_admin_id,$tower_name,$flat_name,$effective_date,$end_date,$parking_admin_name,$flat_id,$crud_type,$flat_size,$two_wh_spa,$four_wh_spa);
            }else{
                $response = $resp;
            }
    }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
 }
echo $response;
?>