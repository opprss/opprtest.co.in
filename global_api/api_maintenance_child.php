<?php
    /*
    Description: State select.
    Developed by: Rakhal Raj Mandal
    Created Date: -------
    Update date :30-03-2018
    */
    function maintenance_child($prk_admin_id,$main_mas_id,$tower_id,$flat_id,$main_tran_year,$main_tran_month,$maint_id,$main_type,$main_charge,$inserted_by){
        $response = array();
        global $pdoconn;
        $id=uniqid();
        $sql = "INSERT INTO `maintenance_child`(`main_child_id`, `prk_admin_id`, `main_mas_id`, `tower_id`, `flat_id`, `main_tran_year`, `main_tran_month`, `maint_id`, `main_type`, `main_charge`, `generate_date`, `inserted_by`, `inserted_date`) VALUES ('$id','$prk_admin_id','$main_mas_id','$tower_id','$flat_id','$main_tran_year','$main_tran_month','$maint_id','$main_type','$main_charge','".TIME."','$inserted_by','".TIME."')";
        $query  = $pdoconn->prepare($sql);
        if($query->execute()){
            $response['status'] = 1;
            $response['message'] = 'Successful';
        }else{
            $response['status'] = 0;
            $response['message'] = 'Not Successful';
        }
        return json_encode($response);
    }
?>