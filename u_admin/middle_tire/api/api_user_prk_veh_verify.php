<?php
/*
Description: vehicle QR code scan parking area gate.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-05-2018
*/
function user_prk_veh_verify($quercode_value,$user_veh_type,$user_veh_number,$user_mobile,$user_admin_id,$user_nick_name,$payment_type,$gate_pass_num){
    global $pdoconn;
    $response = array();
    $resp = array();
    $prk_user_fi=prk_user_find($quercode_value); 
    $json = json_decode($prk_user_fi);
	if($json->status=='1'){		
	    $prk_area_gate_type=$json->prk_area_gate_type;
	    $prk_admin_id=$json->prk_admin_id;
	    $prk_area_gate_id=$json->prk_area_gate_id;
	    if($prk_area_gate_type == FLAG_I || $prk_area_gate_type == FLAG_B){
	    	$wheeler_count= prking_wheeler_count($prk_admin_id,$user_veh_type,$user_veh_number);
	        $json = json_decode($wheeler_count);
	        if($json->status=='1'){
		        $wheeler_cou= wheeler_count($prk_admin_id,$user_veh_type,$user_veh_number,$payment_type);
		        $json = json_decode($wheeler_cou);
		        if($json->status=='1'){
		        	//echo $wheeler_cou;
		        	if($payment_type== DGP){
		        		$resp=gate_pass_match($prk_admin_id,$user_veh_type,$user_veh_number);
		               	$json = json_decode($resp);
		               	if($json->status=='1'){
		               		$gate_pass_num=$json->prk_gate_pass_num;	               		
		               		$payment_type = DGP;
		               	}else{
		                    $response['status'] = 0;
		                    $response['stat'] = $prk_area_gate_type;
		                   // $response['message'] = 'Your Gate Pass Not Match';
		                    $return = json_encode($response);
		                    die();
		                }
		        	}

		        	$sql = "INSERT INTO `user_prk_veh_verify`(`user_veh_type`,`user_veh_number`,`user_mobile`,`user_admin_id`,`user_name`,`prk_admin_id`,`payment_type`,`gate_pass_num`,`prk_area_gate_id`) VALUE('$user_veh_type','$user_veh_number','$user_mobile','$user_admin_id','$user_nick_name','$prk_admin_id','$payment_type','$gate_pass_num','$prk_area_gate_id')";

	                $query  = $pdoconn->prepare($sql);
	                if($query->execute()){
	                    $user_prk_veh_varify_id=$pdoconn->lastInsertId();
	                    $response['status'] = 1;
	                    $response['user_prk_veh_varify_id'] = $user_prk_veh_varify_id;
	                    $response['message'] = 'Successful';
	                    $return = json_encode($response);  
	                    vehicle_in_scan_push_notification_employee($prk_admin_id,$prk_area_gate_id,$user_veh_number);
	                }else{
	                    $response['status'] = 0;
	                    $response['message'] = 'Not Successful';
	                    $return = json_encode($response);  
	                }
		        }else{
		        	$return = $wheeler_cou;
		        }
		  	}else{
		  		$return = $wheeler_count;
		  	}
		}else{
			$response['status'] = 0;
	        $response['message'] = 'This is Wrong Gate';
	        $return = json_encode($response); 
		}
	}else{
        $return = $prk_user_fi;
    }
    return $return;
}
?>