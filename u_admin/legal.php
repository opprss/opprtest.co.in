<?php include 'all_nav/header1.php';
 include 'contect/api.php'; 
 $question_array=leagal_page('questions.txt');
 ?>

    </header>
    <div class="page-ttl">
        <div class="layer-stretch">
            <div class="page-ttl-container">
                <div class="layer-ttl"><h4><span class="text-primary font-header_head">Legal</span></h4></div>
                <p><a href="#">Home</a> &#8594; <span>Legal</span></p>
            </div>
        </div>
    </div><!-- End Page Title Section -->
    <!-- Start Button Section -->
    <div class="light-background">
        <div class="layer-stretch">
            <div class="layer-wrapper pb-0">
                <div class="row pt-4">
                    <div class="col-12">
                        <div class="panel panel-default">
                            <div class="panel-head">
                                <div class="panel-title font-header">The Legal Document For Your Business.</div>

                            </div>
                            <div class="panel-wrapper">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <p>
                                                <span class="dropcap bg-dark text-white"><?php echo $question_array[0]; ?></span> 
                                                <div class="font_body">  
                                                    <?php    
                                                    // print_r($question_array);
                                                    echo  $question_array[1];
                                                    ?>
                                            </p>
                                                </div>
                                        </div>
                                      
                                        <div class="col-sm-4">
                                            <p><span class="dropcap">M</span>
                                                <div class="font_body">auris viverra, dui at cursus suscipit, lectus hendrerit anute, nec iaculis felis nulla quils tellus. In rhoncus pharetra ailm. Quisque tortor odio, feugiat vitae velitndle atncuion, feuiat euismod.
                                                </div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="panel panel-default">
                            <div class="panel-head">
                                <div class="panel-title">Icon Dropcap Example</div>
                            </div>
                            <div class="panel-wrapper">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <p><span class="dropcap" style="margin-top: 10px"><i class="icon-layers panel-head-icon text-primary" style="font-size: 50px;"></i></span>
                                                <h3 class="font-header"><?php echo  $question_array[2];?></h3>
                                            </p>
                                            <p><?php echo $question_array[3]; ?></p>
                                            <p class="font_body"><b><?php echo $question_array[4].' '.$question_array[5]; ?></b></p>
                                        </div>
                                     
                                    </div>
                                </div>
                            </div>
                            <!-- 2nd legal -->
                              <div class="panel-wrapper">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <p><span class="dropcap" style="margin-top: 10px"><i class="icon-layers panel-head-icon text-primary" style="font-size: 50px;"></i></span>
                                                <h3 class="font-header"><?php echo  $question_array[6];?></h3>
                                            </p>
                                            <p class="font_body"><?php echo $question_array[7]; ?></p>
                                        </div>
                                     
                                    </div>
                                </div>
                            </div>
                            <!--//  -->
                            <!-- 3rd legal -->
                              <div class="panel-wrapper">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <p><span class="dropcap" style="margin-top: 10px"><i class="icon-layers panel-head-icon text-primary" style="font-size: 50px;"></i></span>
                                                <h3 class="font-header"><?php echo  $question_array[8];?></h3>
                                            </p>
                                            <p class="font_body"><?php echo $question_array[9]; ?></p>
                                        </div>
                                     
                                    </div>
                                </div>
                            </div>
                            <!--//  -->
                            <!-- 4th legal -->
                              <div class="panel-wrapper">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <p><span class="dropcap" style="margin-top: 10px"><i class="icon-layers panel-head-icon text-primary" style="font-size: 50px;"></i></span>
                                                <h3 class="font-header"><?php echo  $question_array[10];?></h3>
                                            </p>
                                            <p class="font_body"><?php echo $question_array[11]; ?></p>
                                            <p class="font_body"><b><?php echo $question_array[12].' '.$question_array[13]; ?></b></p>
                                        </div>
                                     
                                    </div>
                                </div>
                            </div>
                            <!--//  -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End Button Section -->
<!--     <div class="action">
        <div class="layer-stretch">
            <div class="layer-wrapper text-center">
                <div class="layer-ttl"><h4>We design <span class="text-primary">delightful</span> digital experiences</h4></div>
                <div class="p-5 color-light">Read more about what we do and our philosophy of design. Judge for yourself The work and results we’ve achieved for other clients, and meet our highly experienced Team who just love to design, develop and deploy. Tell Us Your Story</div>
                <a href="#" class="btn btn-outline btn-primary btn-pill btn-outline-2x btn-lg mt-3">Tell Us Your Story</a>
            </div>
        </div>
    </div>
 -->    <!-- End Action Section -->
    <!-- Start Footer Section -->
    <!-- End main Footer Section -->
        <!-- Start Copyright Section -->
        <div style="margin-top: 50px">
       
       <?php include"all_nav/footer.php" ?>
   </div>