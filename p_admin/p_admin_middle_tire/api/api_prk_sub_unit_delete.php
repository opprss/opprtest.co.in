<?php
/*
Description: Parking area sub unit delete.
Developed by: Rakhal Raj Mandal
Created Date: 31-03-2018
Update date : 27-05-2018
*/ 
function prk_sub_unit_delete($prk_admin_id,$prk_sub_unit_id,$user_name){
    global $pdoconn;
    $response = array();

    $sql ="UPDATE `prk_gate_pass_dtl` SET `updated_by`='$user_name',`updated_date`='".TIME."',`del_flag`= '".FLAG_Y."' WHERE `prk_sub_unit_id` ='$prk_sub_unit_id' AND `prk_admin_id` = '$prk_admin_id' AND `del_flag` = '".FLAG_N."' AND `active_flag`='".FLAG_Y."'";
    $query = $pdoconn->prepare($sql);
    $query->execute();

    $sql ="UPDATE `prk_sub_unit_veh_space` SET `updated_by`='$user_name',`updated_date`='".TIME."',`del_flag`= '".FLAG_Y."' WHERE `prk_sub_unit_id` = '$prk_sub_unit_id' AND `prk_admin_id` = '$prk_admin_id' AND `active_flag` = '".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query = $pdoconn->prepare($sql);
    $query->execute();


    $sql ="UPDATE `prk_sub_unit` SET `del_flag`='".FLAG_Y."', `updated_date`='".TIME."',`updated_by`='$user_name' WHERE `prk_admin_id`='$prk_admin_id' AND `prk_sub_unit_id`='$prk_sub_unit_id' AND `del_flag`='".FLAG_N."' AND `active_flag` = '".FLAG_Y."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Delete Sucessfull';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Delete Not Sucessfull';
        return json_encode($response);
    }
}
/*
Description: Parking area sub unit delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018

function prk_sub_unit_delete($prk_admin_id,$prk_sub_unit_id,$user_name){
    global $pdoconn;
    $response = array();
    $sql ="UPDATE `prk_sub_unit` SET `del_flag`='".DEL_FLAG_Y."', `updated_date`='".TIME."',`updated_by`='$user_name' WHERE `prk_admin_id`='$prk_admin_id' AND `prk_sub_unit_id`='$prk_sub_unit_id' AND `del_flag`='".DEL_FLAG_N."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Delete Sucessfull';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Delete Not Sucessfull';
        return json_encode($response);
    }
}*/
?>