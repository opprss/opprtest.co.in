<?php 
/*
Description: Admi  User Name Generate.
Developed by: Rakhal Raj Mandal
Created Date: 13-Nov-2019
Update date : -------
*/ 
require_once 'api/parkAreaReg_api.php';
if(isAvailable(array('parking_area_name'))){
    if(isEmpty(array('parking_area_name'))){
        $parking_area_name = trim($_POST['parking_area_name']);
        $response = admin_username_generate($parking_area_name);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo ($response);
?>