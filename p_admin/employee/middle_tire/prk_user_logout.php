<?php 
/*
Description: parking employ logOut
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date : 07-04-2018
*/  
require_once 'api/parkAreaReg_api.php';
$response = array();
 
 if(isAvailable(array('token','prk_admin_id','out_prk_user_username','prk_user_gate_login_id'))){

 	if(isEmpty(array('token','prk_admin_id','out_prk_user_username'))){

	 	$token = trim($_POST['token']);
	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$out_prk_user_username = trim($_POST['out_prk_user_username']);
	 	$prk_user_gate_login_id = trim($_POST['prk_user_gate_login_id']);
	 	//prk_user_gate_logout($prk_admin_id,$out_prk_user_username);
	 	if (empty($prk_user_gate_login_id) || $prk_user_gate_login_id =='' || $prk_user_gate_login_id == NULL) {
 			echo $response= prk_user_logout($token); 		
	 	}else{
	 		prk_user_gate_logout($prk_admin_id,$out_prk_user_username,$prk_user_gate_login_id);
 			echo $response= prk_user_logout($token);
	 	}
 	}else{
		$response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        echo json_encode($response); 
	 }
}else{
	$response['status'] = 0; 
	$response['message'] = 'Invalid API Call';
	echo json_encode($response); 
}
?>