<?php 
/*
Description: parking area visit the people all list. 
Developed by: Rakhal Raj Mandal
Created Date: 18-06-2018
Update date : -------
*/
function prk_user_gate_login_check($prk_user_gate_login_id){
    global $pdoconn;
    $response = array();

    $sql="SELECT * FROM prk_user_gate_login_dtl where prk_user_gate_login_id ='$prk_user_gate_login_id' and end_date is null";
    $query = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $return = json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Unsuccessful';
        $return = json_encode($response);
    }
    return $return;          
}
?>