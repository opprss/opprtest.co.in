<?php
/*
Description: Parking all gate pass dtl list.
Developed by: Rakhal Raj Mandal
Created Date: 31-02-2018
Update date : 27-05-2018
*/ 
function prk_gate_pass_dtl_list($prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql="SELECT pgpd.`prk_gate_pass_id`,
    pgpd.`prk_admin_id`,
    pgpd.`user_admin_id`,
    pgpd.`prk_sub_unit_id`,
    pgpd.`tower_id`,
    pgpd.`flat_id`,
    pgpd.`living_status`,
    pgpd.`park_lot_no`,
    pgpd.`veh_type`,
    pgpd.`veh_number`,
    pgpd.`sub_unit_name`,
    pgpd.`veh_owner_name`,
    pgpd.`mobile`,
    pgpd.`effective_date`,
    pgpd.`end_date`,
    pgpd.`prk_gate_pass_num`,
    `vehicle_type`.`vehicle_type_dec`,
    `vehicle_type`.`vehicle_typ_img`,
    `prk_sub_unit`.`prk_sub_unit_name`,
    `prk_sub_unit`.`prk_sub_unit_short_name`,
    `tower_details`.`tower_name`,
    `flat_details`.`flat_name`
    FROM `prk_gate_pass_dtl` pgpd JOIN `vehicle_type` JOIN `prk_sub_unit`
    LEFT JOIN `tower_details` ON `tower_details`.`tower_id`=`pgpd`.`tower_id`
    AND `tower_details`.`active_flag`='".FLAG_Y."'
    AND `tower_details`.`del_flag`='".FLAG_N."'
    LEFT JOIN `flat_details` ON `flat_details`.`flat_id`=`pgpd`.`flat_id`
    AND `flat_details`.`active_flag`='".FLAG_Y."'
    AND `flat_details`.`del_flag`='".FLAG_N."' 
    WHERE pgpd.`prk_admin_id`='$prk_admin_id'
    AND pgpd.`active_flag`='".FLAG_Y."' 
    AND pgpd.`del_flag`='".FLAG_N."'
    AND `prk_sub_unit`.`prk_sub_unit_id` = pgpd.`prk_sub_unit_id`
    AND `vehicle_type`.`active_flag`='".FLAG_Y."'
    AND '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(pgpd.`effective_date`, '%d-%m-%Y'), '%Y-%m-%d') 
    AND DATE_FORMAT(STR_TO_DATE(IFNULL(pgpd.`end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')
    AND `vehicle_type`.`vehicle_sort_nm`=pgpd.`veh_type`
    ORDER BY  pgpd.`prk_gate_pass_id` DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val){
        $response['status'] = 1;
        $response['message'] = 'data ssucessfull';
        $park['prk_gate_pass_id'] = $val['prk_gate_pass_id'];
        $park['prk_admin_id'] = $val['prk_admin_id'];
        $park['user_admin_id'] = $val['user_admin_id'];
        $park['prk_sub_unit_id'] = $val['prk_sub_unit_id'];
        $park['veh_type'] = $val['veh_type'];
        $park['vehicle_type_dec'] = $val['vehicle_type_dec'];
        $park['vehicle_typ_img'] = $val['vehicle_typ_img'];
        $park['veh_number'] = $val['veh_number'];
        $park['tower_id'] = $val['tower_id'];
        $park['tower_name'] = $val['tower_name'];
        $park['flat_id'] = $val['flat_id'];
        $park['flat_name'] = $val['flat_name'];
        $park['living_status'] = $val['living_status'];
        $park['park_lot_no'] = $val['park_lot_no'];
        $park['sub_unit_name_f'] = $val['sub_unit_name'];
        $park['veh_owner_name'] = $val['veh_owner_name'];
        $park['mobile'] = $val['mobile'];
        $park['effective_date'] = $val['effective_date'];
        $park['end_date'] = $val['end_date'];
        $park['prk_gate_pass_num'] = $val['prk_gate_pass_num'];
        $park['prk_sub_unit_name'] = $val['prk_sub_unit_name'];
        $park['sub_unit_name'] = $val['prk_sub_unit_short_name'];
        array_push($allplayerdata, $park);
    }
    return json_encode($allplayerdata);
}
?>