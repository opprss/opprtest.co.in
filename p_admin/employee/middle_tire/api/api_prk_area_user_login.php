<?php 
/*
Description: Parking employ login.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function prk_area_user_login($prk_user_username,$prk_user_password,$device_token_id,$device_type){
    global $pdoconn;
    $response = array();
    $sql ="SELECT `prk_area_user_id`,`prk_admin_id`,`prk_user_name` FROM `prk_area_user` WHERE `prk_user_username`=:prk_user_username AND `prk_user_password`=:prk_user_password AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute(array('prk_user_username'=>$prk_user_username,'prk_user_password'=>md5($prk_user_password)));
    $count=$query->rowCount();
    if($count>0){
        $row = $query->fetch();
        $prk_area_user_id =  $row['prk_area_user_id'];
        $prk_admin_id =  $row['prk_admin_id'];
        $prk_user_name =  $row['prk_user_name'];
        #-----------------------#
        $sql ="UPDATE `prk_token` SET `end_date`='".TIME."',`active_flag`='".FLAG_N."' WHERE `prk_admin_id` = '$prk_admin_id' and `prk_area_user_id` ='$prk_area_user_id' and `active_flag`='".FLAG_Y."'";
        $query = $pdoconn->prepare($sql);
        $query->execute(); 
        #-----------------------#
        $token = sha1(uniqid());
        $sql = "INSERT INTO `prk_token` (`prk_admin_id`, `prk_area_user_id`, `token_value`, `device_token_id`, `device_type`,`token_start_date`) VALUES ('$prk_admin_id', '$prk_area_user_id', '$token', '$device_token_id', '$device_type','".TIME."')";
        $query = $pdoconn->prepare($sql);
        $query->execute();  
        if ($query){
            $response['status'] = 1;
            $response['message'] = 'login Successful';
            $response['prk_user_username'] = $prk_user_username;
            $response['prk_area_user_id'] = $prk_area_user_id;
            $response['prk_admin_id'] = $prk_admin_id;
            $response['prk_user_name'] = $prk_user_name;
            $response['token'] = $token;
            return json_encode($response); 
        }else{
            $response['status'] = 0;
            $response['token'] = $token;
            $response['message'] = 'token not found';
        }     
    }else{
        $response['status'] = 0;
        $response['message'] = 'username or password is wrong';
        return json_encode($response);
    }
}
?>