<?php
  include 'api/parkAreaReg_api.php';
	$contractual_gate_pass_dtl_id = isset($_REQUEST['list_id']) ? trim($_REQUEST['list_id']) : '2';
	$prk_admin_id = isset($_SESSION['prk_admin_id']) ? trim($_SESSION['prk_admin_id']) : '7';
  	$gate_pass_show1 = contractual_gate_pass_show($prk_admin_id,$contractual_gate_pass_dtl_id);
  	$gate_pass_show = json_decode($gate_pass_show1, true);

  	$visitor_name=$gate_pass_show['visitor_name'];
  	$visitor_mobile=$gate_pass_show['visitor_mobile'];
  	$emergency_no=$gate_pass_show['emergency_no'];
  	$visitor_date_of_birth=$gate_pass_show['visitor_date_of_birth'];
  	$visitor_blood_group=$gate_pass_show['visitor_blood_group'];
  	$visitor_contractor=$gate_pass_show['visitor_contractor'];
  	$visitor_add=$gate_pass_show['visitor_add'];
  	$visitor_department=$gate_pass_show['visitor_department'];
  	$visitor_pf_number=$gate_pass_show['visitor_pf_number'];
  	$visitor_esi_number=$gate_pass_show['visitor_esi_number'];
  	$effective_date=$gate_pass_show['effective_date'];
  	$end_date=$gate_pass_show['end_date'];
  	$visitor_img=$gate_pass_show['visitor_img'];


	$add_len=strlen($visitor_name);
	$visitor_name =($add_len>21)?mb_substr($visitor_name, 0, 20).'..':$visitor_name;

	$add_len=strlen($visitor_contractor);
	$visitor_contractor =($add_len>21)?mb_substr($visitor_contractor, 0, 20).'..':$visitor_contractor;

	$add_len=strlen($visitor_department);
	$visitor_department =($add_len>21)?mb_substr($visitor_department, 0, 20).'..':$visitor_department;

	$add_len=strlen($visitor_add);
	$visitor_add =($add_len>32)?mb_substr($visitor_add, 0, 30).'..':$visitor_add;

  	$qrCodeValue=$visitor_mobile.'-OPPR';
  	$logoUrl=IMAGE_URL.'centur_industry_logo.jpg';
 ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Contractual ID Card Print</title>
	    <link rel="shortcut icon" href="<?php echo FAVICON ?>" type="image/x-icon">
	</head>
	<body>
		<!-- <div style="display: inline-block; page-break-after: auto;"> -->
		<center>
		<table>
			<tr>
				<td>
					<table style="height: 280px; width: 454px; border: 1px solid;">
						<tbody>
							<tr style="height: 19px;">
								<td style="width: 130px; height: 19px;">&nbsp;<img src="<?=$logoUrl ?>" alt="" width="81" height="51" /></td>
								<td style="width: 112px; height: 19px;" colspan="2">
									<center><p>
										<span style="font-size: 14px;"><b>Century Industrial Products Pvt. Ltd.</b></span><br>
										<span style="font-size: 11px;">Chakundi, Dankuni Coal Complex, Hooghly</span>
									</p></center>
								</td>
								<td style="width: 96px; height: 19px;" rowspan="3">&nbsp;<img src="<?=$visitor_img ?>" alt="" width="104" height="104" /></td>
							</tr>
							<tr style="height: 19px;">
								<td style="width: 130px; height: 19px;">&nbsp;NAME</td>
								<td style="width: 112px; height: 19px;" colspan="2">:&nbsp;<?=$visitor_name ?></td>
							</tr>
							<tr style="height: 19px;">
								<td style="width: 130px; height: 19px;">&nbsp;Ph. No.</td>
								<td style="width: 112px; height: 19px;" colspan="2">:&nbsp;<?=$visitor_mobile ?></td>
							</tr>
							<tr style="height: 19px;">
								<td style="width: 130px; height: 19px;">&nbsp;Date of Birth</td>
								<td style="width: 325px; height: 19px;" colspan="2">:&nbsp;<?=$visitor_date_of_birth ?></td>
								<td style="width: 96px; height: 78px;" rowspan="4">&nbsp;<img src="<?=OPPR_BASE_URL?>library/QRCodeGenerate/generate.php?text=<?=$qrCodeValue ?>" alt="" width="92" height="92" /></td>
							</tr>
							<tr style="height: 20px;">
								<td style="width: 130px; height: 20px;">&nbsp;Blood Group</td>
								<td style="width: 325px; height: 20px;" colspan="2">:&nbsp;<?=$visitor_blood_group ?></td>
							</tr>
							<tr style="height: 19px;">
								<td style="width: 130px; height: 19px;">&nbsp;Contractor</td>
								<td style="width: 325px; height: 19px;" colspan="2">:&nbsp;<?=$visitor_contractor ?></td>
							</tr>
							<tr style="height: 20px;">
								<td style="width: 130px; height: 20px;">&nbsp;Department</td>
								<td style="width: 325px; height: 20px;" colspan="2">:&nbsp;<?=$visitor_department ?></td>
							</tr>
							<tr style="height: 18px;">
								<td style="width: 130px; height: 18px;">&nbsp;Emergency No</td>
								<td style="width: 421px; height: 18px;" colspan="3">:&nbsp;<?=$emergency_no ?></td>
							</tr>
							<tr style="height: 18px;">
								<td style="width: 130px; height: 18px;">&nbsp;Valid From</td>
								<td style="width: 112px; height: 18px;">:&nbsp;<?=$effective_date ?></td>
								<td style="width: 213px; height: 18px;"colspan="2">&nbsp;To : <?=$end_date ?></td>
							</tr>
							<tr style="height: 18px;">
								<td style="width: 130px; height: 18px;">&nbsp;P.F Number</td>
								<td style="width: 112px; height: 18px;">:&nbsp;<?=$visitor_pf_number ?></td>
								<td style="width: 213px; height: 18px;"colspan="2">&nbsp;E.S.I Number : <?=$visitor_esi_number ?></td>
							</tr>
							<tr style="height: 18px;">
								<td style="width: 130px; height: 18px;">&nbsp;Holder Signature</td>
								<td style="width: 112px; height: 18px;">&nbsp;</td>
								<td style="width: 213px; height: 18px;">&nbsp;Issuing <br> Authority</td>
								<td style="width: 96px; height: 18px;">&nbsp;</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table style="height: 340px; width: 454px; border: 1px solid;">
						<tbody>
							<tr style="text-align: center;">
								<td style="width: 291px;" colspan="3">
								<p><span style="font-size: 20px;"><strong>Century Industrial Products Pvt. Ltd.</strong></span><br> Chakundi, Dankuni Coal Complex, Hooghly</p>
								</td>
							</tr>
							<tr>
								<td style="width: 291px;" colspan="3">Note: Please follow the rules:-</td>
							</tr>
							<tr>
								<td style="width: 291px;" colspan="3">1. This photo ID card is non-transferable.</td>
							</tr>
							<tr>
								<td style="width: 291px;" colspan="3">2. The cardholder must submit this card at the Gate before leaving.</td>
							</tr>
							<tr>
								<td style="width: 291px;" colspan="3">3. This card is the property of CIPPL.</td>
							</tr>
							<tr>
								<td style="width: 291px;" colspan="3">4. The loss of the card must be reported immediately to the security department of CIPPL.</td>
							</tr>
							<tr>
								<td style="width: 291px;" colspan="3">5. The cardholder must obey all the safety rules demonstrated by the security department during issuance.</td>
							</tr>
							<tr>
								<td style="width: 291px;" colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td style="width: 97px;">&nbsp;</td>
								<td style="width: 59px;">&nbsp;</td>
								<td style="width: 135px;">Authorized Signature</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</table>
		</center>
		<!-- </div> -->
	</body>
</html>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous">
</script>
<script type="text/javascript">
	$(window).on('load', function() {
	 	window.print();
		setTimeout(function(){window.close();}, 1);
	});
</script>