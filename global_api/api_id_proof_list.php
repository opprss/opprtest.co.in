<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function id_proof_list(){
    $allplayerdata = array();
    $response = array();
    $vehicle = array();
    global $pdoconn;
    // $sql = "SELECT * FROM `state` WHERE `active_flag`='".FLAG_Y."'";
    $sql = "SELECT `all_drop_down_id`,`sort_name`,`full_name` FROM `all_drop_down` WHERE `del_flag`='".FLAG_N."' AND `category`='ID_P'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val) 
    {
        $response['status'] = 1;
        $response['message'] = 'Successfully';
        $vehicle['all_drop_down_id'] =$val['all_drop_down_id'];
        $vehicle['sort_name'] =$val['sort_name'];
        $vehicle['full_name'] =$val['full_name'];
        array_push($allplayerdata, $vehicle);
        $response['state'] = $allplayerdata;
    }
    return json_encode($response);
}
?>