<?php 
/*
Description: user complite registration only for draft user.
Developed by: Rakhal Raj Mandal
Created Date: 10-02-2018
Update date : 10-05-2018
*/ 
include_once 'api/reg_api.php';
// include_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('user_admin_id','mobile','password','name','t_c_flag'))){
 	if(isEmpty(array('user_admin_id','mobile','password','name','t_c_flag'))){
	 	$user_admin_id = trim($_POST['user_admin_id']);
	 	$mobile = trim($_POST['mobile']);
	 	$password = trim($_POST['password']);
        $name = trim($_POST['name']);
	 	$t_c_flag = trim($_POST['t_c_flag']);
        $response= compliteRegistration($user_admin_id,$mobile,$password,$name,$t_c_flag); 
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 	$response = json_encode($response);
}
echo $response;
/*include_once 'api/reg_api.php';
include_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('user_admin_id','mobile','password','name'))){
 	if(isEmpty(array('user_admin_id','mobile','password','name'))){
	 	$user_admin_id = trim($_POST['user_admin_id']);
	 	$mobile = trim($_POST['mobile']);
	 	$password = trim($_POST['password']);
	 	$name = trim($_POST['name']);
        $response= compliteRegistration($user_admin_id,$mobile,$password,$name); 
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 	$response = json_encode($response);
}
echo $response;
/*require_once 'api/reg_api.php';
include_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('user_admin_id','mobile','password','repassword','name','email','dob','gender'))){
 	if(isEmpty(array('user_admin_id','mobile','password','repassword','name','email','dob','gender'))){
	 	$mobile = trim($_POST['mobile']);
	 	$password = trim($_POST['password']);
	 	$repassword = trim($_POST['repassword']);
	 	$name = trim($_POST['name']);
	 	$email = trim($_POST['email']);
	 	$dob = trim($_POST['dob']);
	 	$gender = trim($_POST['gender']);
	 	$user_admin_id = trim($_POST['user_admin_id']);
        $response=compliteRegistration($user_admin_id,$mobile,$password,$repassword,$email,$name,$dob,$gender);
        echo ($response);
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        echo json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 	echo json_encode($response);
}*/
?>