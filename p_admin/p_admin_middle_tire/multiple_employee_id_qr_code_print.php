<?php
  include 'api/parkAreaReg_api.php';
  @session_start();
  $prk_area_user_idd=json_decode($_REQUEST['prk_area_user_id']);
  $prk_admin_id = $_SESSION['prk_admin_id'];
  $prk_inf = prk_inf($prk_admin_id);
  $prk_inf = json_decode($prk_inf, true);
  $prk_area_name = $prk_inf['prk_area_name'];
  $prk_area_short_name = $prk_inf['prk_area_short_name'];
  $prk_area_logo_img = $prk_inf['prk_area_logo_img'];

  $area_address = prk_area_address_show($prk_admin_id);
  $area_address = json_decode($area_address, true);
  $prk_address = $area_address['prk_address'];
  $prk_land_mark = $area_address['prk_land_mark'];
  $prk_add_area = $area_address['prk_add_area'];
  $city_name = $area_address['city_name'];
  $state_name = $area_address['state_name'];
  $prk_add_pin = $area_address['prk_add_pin'];
  $prk_add_country = $area_address['prk_add_country'];
  $address =$prk_land_mark.', ADD-'.$prk_address.', CITY-'.$city_name.', STATE-'.$state_name.', PIN-'.$prk_add_pin;  
?>
<head>
  <title>Multiple User Id Card</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<style type="text/css">
  .tg  {border-collapse:collapse;border-spacing:0;}
  .tg td{font-family:Arial, sans-serif;font-size:11px;padding:10px 5px;overflow:hidden;word-break:normal;}
  .tg th{font-family:Arial, sans-serif;font-size:11px;font-weight:normal;padding:10px 5px;overflow:hidden;word-break:normal;}
  .tg .tg-xldj{border-color:inherit;text-align:left;line-height: 0px}
  .tg .tg-quj4{border-color:inherit;text-align:right;}
  .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top;line-height: 0px}
  .tg .tg-0pky1{border-color:inherit;text-align:left;vertical-align:top;line-height: 16px;}
  .tg .tg-0lax{text-align:left;vertical-align:top;line-height: 0px}
  @media print{
  @page {
     margin-top: 30px;
     margin-bottom: 5px;
     size: A4;
  }
  body  {
     padding-top: 10px;
     padding-bottom: 0px ;
     margin-left: 0px;
  }
  #Header, #Footer { display: none !important; }
  .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
        float: left;
   }
   .col-sm-12 {
        width: 100%;
   }
   .col-sm-11 {
        width: 91.66666667%;
   }
   .col-sm-10 {
        width: 83.33333333%;
   }
   .col-sm-9 {
        width: 75%;
   }
   .col-sm-8 {
        width: 66.66666667%;
   }
   .col-sm-7 {
        width: 58.33333333%;
   }
   .col-sm-6 {
        width: 50%;
   }
   .col-sm-5 {
        width: 41.66666667%;
   }
   .col-sm-4 {
        width: 33.33333333%;
   }
   .col-sm-3 {
        width: 25%;
   }
   .col-sm-2 {
        width: 16.66666667%;
   }
   .col-sm-1 {
        width: 8.33333333%;
   }
  }
</style>
<body>
  <div class="row mg-b-5">
    <div class="col-sm-12">
      <div class="row">
        <?php
          foreach ($prk_area_user_idd as  $prk_area_user_id) {
            $sql ="SELECT  `prk_area_user`.`prk_admin_id`,
              `prk_area_user`.`prk_user_username`,
              `prk_area_user`.`prk_user_name`,
              `prk_area_user`.`prk_user_mobile`,
              `prk_area_user`.`prk_user_gender`,
              `prk_area_user`.`prk_user_img`,
              `prk_area_user`.`prk_user_address`,
              `prk_area_user`.`prk_user_landmark`,
              `prk_area_user`.`prk_user_country`,
              `prk_area_user`.`prk_user_state`,
              `prk_area_user`.`prk_user_city`,
              `prk_area_user`.`prk_user_pincode`
              FROM `prk_area_user`
              WHERE `prk_area_user`.`prk_area_user_id`='$prk_area_user_id'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $val = $query->fetch();
            $prk_user_name = $val['prk_user_name'];
            $prk_user_username = $val['prk_user_username'];
            $prk_user_address = strtoupper($val['prk_user_address']);
            $prk_user_gender = $val['prk_user_gender'];
            $prk_user_img = $val['prk_user_img'];
            if ($prk_user_gender =='M') {
              $gender='MALE';
              $prk_user_imga=MALE_IMG_D;
            }else{
              $gender='FEMALE';
              $prk_user_imga=FEMALE_IMG_D;
            }
            if (!empty($prk_user_img)) {

              $prk_user_img = PRK_BASE_URL.$val['prk_user_img'];
            }else{

              $prk_user_img = $prk_user_imga;
            }
            $prk_user_mobile = $val['prk_user_mobile'];
            $add_len_vis=strlen($prk_user_address);
            $prk_user_address =($add_len_vis>23)?mb_substr($prk_user_address, 0, 23).'..':$prk_user_address;
            $prk_area_len_vis=strlen($prk_area_name);
            $prk_area_name_list = ($prk_area_len_vis>22)?mb_substr($prk_area_name, 0, 22).'..':$prk_area_name;
        ?>
        <div style="display: inline-block; page-break-after: auto;">
          <div class="col-sm-6">
            <div style="width: 340px;height: auto;margin-top: 5px;margin-left: 5px;">
              <div class="col-sm-12" style="border: 2px solid black;">
                <div class="row">
                  <div class="col-sm-3" style="margin-top: 5px;"><center><img style="height: 40px;width:60px;" src="<?php echo LOGO_PDF;?>"></center></div>
                  <div class="col-sm-6" style="padding-top: 8px;"><center><b><?php echo $prk_area_name; ?></b></center></div>
                  <div class="col-sm-3" style="margin-top: 5px;"><center><img style="height: 40px;width:60px;margin-top: 2px;margin-bottom: 2px;" src="<?php echo PRK_BASE_URL.$prk_area_logo_img;?>"></center></div>
                </div>
                <div class="row" style="border-bottom:1px solid black">
                  <div class="col-sm-3"></div>
                  <div class="col-sm-6"><p style="margin: 0 18px 0px;">( Employee ID )</p></div>
                  <div class="col-sm-3"></div>
                </div>
                <div class="row" style="border-bottom:1px solid black">
                  <div class="col-sm-12" style="font-size: 10px;text-align: center;"><?php echo $address; ?></div>
                </div>
                <div class="row">
                  <div class="col-sm-9" style="margin-top: 5px;">
                    <div class="row">
                      <div class="col-sm-12" style="font-size: 12px;margin-top: 2px;">ID No : <?php echo $prk_user_username; ?></div>
                      <div class="col-sm-12" style="font-size: 12px;margin-top: 2px;">Name: <?php echo ucwords(strtolower($prk_user_name)); ?></div>
                      <div class="col-sm-12" style="font-size: 12px;margin-top: 2px;">Gender: <?php echo ucwords(strtolower($gender)); ?></div>
                      <div class="col-sm-12" style="font-size: 12px;margin-top: 2px;">Address: <?php echo ucwords(strtolower($prk_user_address))?></div>
                      <div class="col-sm-12" style="font-size: 12px;margin-top: 2px;">Mobile: <?php echo $prk_user_mobile; ?></div>
                      <div class="col-sm-12" style="font-size: 12px;margin-top: 2px;">By-Order: <?php echo $prk_area_name_list; ?></div>
                    </div>
                  </div>
                  <div class="col-sm-3" style="margin-top: 5px;">
                    <div class="row">
                      <div class="col-sm-12">
                        <img src="<?php echo $prk_user_img; ?>" style='' width="60" height="60">
                      </div>
                      <div class="col-sm-12">
                        <img style="height: 60px; margin-top: 5px; margin-bottom: 3px;" src="./../../library/QRCodeGenerate/generate.php?text=<?=base64_encode($prk_user_username).'-OPPR'?>" alt="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php 
          }
        ?>
      </div>
    </div>
  </div>
</body>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous">
</script>
<script type="text/javascript">
  $(window).on('load', function() {
    window.print();
    setTimeout(function(){window.close();}, 1);
  });
</script>
