<?php 
/*
Description: Parking visiter in list
Developed by: Rakhal Raj Mandal
Created Date: 31-05-2018
Update date : 
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','prk_user_username','token','prk_gate_pass_id','end_date','veh_owner_name'))){
    if(isEmpty(array('prk_admin_id','prk_area_user_id','prk_user_username','token','prk_gate_pass_id','veh_owner_name'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $user_name = trim($_POST['prk_user_username']);
        $token = trim($_POST['token']);
        $prk_gate_pass_id = trim($_POST['prk_gate_pass_id']);
        $user_admin_id = isset($_POST['user_admin_id']) ? trim($_POST['user_admin_id']) : '';
        $veh_type = isset($_POST['veh_type']) ? trim($_POST['veh_type']) : '';
        $veh_number = isset($_POST['veh_number']) ? trim($_POST['veh_number']) : '';
        $prk_sub_unit_id = isset($_POST['prk_sub_unit_id']) ? trim($_POST['prk_sub_unit_id']) : '';
        $mobile = isset($_POST['mobile']) ? trim($_POST['mobile']) : '';
        $end_date = trim($_POST['end_date']);
        $veh_owner_name = trim($_POST['veh_owner_name']);
        $effective_date = isset($_POST['effective_date']) ? trim($_POST['effective_date']) : '';
        $prk_gate_pass_num = isset($_POST['prk_gate_pass_num']) ? trim($_POST['prk_gate_pass_num']) : '';
        $end_date=(empty($end_date))?FUTURE_DATE_WEB:$end_date;

        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            //call global
            $response=prk_gate_pass_dtl_update($prk_gate_pass_id,$prk_admin_id,$user_admin_id,$veh_type,$veh_number,$prk_sub_unit_id,$mobile,$end_date,$veh_owner_name,$effective_date,$prk_gate_pass_num,$user_name);
        }else{

            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response); 
} 
echo $response;
?> 