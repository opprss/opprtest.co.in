<?php
/*
Description: vehicle in parking area the show in details.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
// function user_apartment_add($user_admin_id,$mobile,$user_apar_name,$user_apar_address,$user_apar_id)

function user_apartment_add($user_admin_id,$mobile,$user_apar_name,$user_apar_type,$user_apar_configuration,$user_apar_house_no,$user_apar_built_up_area,$user_apar_bedrooms,$user_apar_bathrooms,$user_apar_balconies,$user_apar_furnished,$user_apar_furnished_name,$user_apar_reserved_parking,$user_apar_address,$user_apar_id,$user_apar_wardrobe,$user_apar_fans,$user_apar_light,$user_apar_modular_kitchen){
    global $pdoconn;
    // $user_apar_id=uniqid();
    $sql = "INSERT INTO `user_apartment_list`(`user_apar_id`, `user_admin_id`, `user_apar_type`, `user_apar_name`, `user_apar_configuration`, `user_apar_house_no`, `user_apar_built_up_area`, `user_apar_bedrooms`, `user_apar_bathrooms`, `user_apar_balconies`, `user_apar_furnished`, `user_apar_wardrobe`, `user_apar_fans`, `user_apar_light`, `user_apar_modular_kitchen`, `user_apar_furnished_name`, `user_apar_reserved_parking`, `user_apar_address`, `inserted_by`, `inserted_date`) VALUES ('$user_apar_id','$user_admin_id','$user_apar_type','$user_apar_name','$user_apar_configuration','$user_apar_house_no','$user_apar_built_up_area','$user_apar_bedrooms','$user_apar_bathrooms','$user_apar_balconies','$user_apar_furnished','$user_apar_wardrobe','$user_apar_fans','$user_apar_light','$user_apar_modular_kitchen','$user_apar_furnished_name','$user_apar_reserved_parking','$user_apar_address','$mobile','".TIME."')";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Successful';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
    }
    return json_encode($response);
}
function user_apartment_list($user_admin_id){
    global $pdoconn;
    $allplayerdata = array();
    $sql="SELECT `user_apar_id`, `user_apar_type`,`user_apar_name`, `user_apar_configuration`, `user_apar_house_no`, `user_apar_built_up_area`, `user_apar_bedrooms`, `user_apar_bathrooms`, `user_apar_balconies`, `user_apar_furnished`, `user_apar_wardrobe`, `user_apar_fans`, `user_apar_light`, `user_apar_modular_kitchen`, `user_apar_furnished_name`, `user_apar_reserved_parking`, `user_apar_address` FROM `user_apartment_list` WHERE `user_admin_id`='$user_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."' ORDER BY `user_apar_unique_id` DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['user_apar_id'] =  $val['user_apar_id'];
            $park['user_apar_type'] =  $val['user_apar_type'];
            $park['user_apar_name'] =  $val['user_apar_name'];
            $park['user_apar_configuration'] =  $val['user_apar_configuration'];
            $park['user_apar_house_no'] =  $val['user_apar_house_no'];
            $park['user_apar_built_up_area'] =  $val['user_apar_built_up_area'];
            $park['user_apar_bedrooms'] =  $val['user_apar_bedrooms'];
            $park['user_apar_bathrooms'] =  $val['user_apar_bathrooms'];
            $park['user_apar_balconies'] =  $val['user_apar_balconies'];
            $park['user_apar_furnished'] =  $val['user_apar_furnished'];
            $park['user_apar_wardrobe'] =  $val['user_apar_wardrobe'];
            $park['user_apar_fans'] =  $val['user_apar_fans'];
            $park['user_apar_light'] =  $val['user_apar_light'];
            $park['user_apar_modular_kitchen'] =  $val['user_apar_modular_kitchen'];
            $park['user_apar_furnished_name'] =  $val['user_apar_furnished_name'];
            $park['user_apar_reserved_parking'] =  $val['user_apar_reserved_parking'];
            $park['user_apar_address'] =  $val['user_apar_address'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['apartment_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
function user_apartment_delete($user_admin_id,$mobile,$user_apar_id){
    global $pdoconn;
    $sql="UPDATE `user_apartment_list` SET `updated_by`='$mobile',`updated_date`='".TIME."',`del_flag`='".FLAG_Y."' WHERE `user_apar_id`='$user_apar_id' AND `user_admin_id`='$user_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Successful';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
    }
    return json_encode($response);
}
function user_apartment_show($user_admin_id,$user_apar_id){
    global $pdoconn;
    $allplayerdata = array();
    $sql="SELECT `user_apar_id`, `user_apar_type`,`user_apar_name`, `user_apar_configuration`, `user_apar_house_no`, `user_apar_built_up_area`, `user_apar_bedrooms`, `user_apar_bathrooms`, `user_apar_balconies`, `user_apar_furnished`, `user_apar_wardrobe`, `user_apar_fans`, `user_apar_light`, `user_apar_modular_kitchen`, `user_apar_furnished_name`, `user_apar_reserved_parking`, `user_apar_address` FROM `user_apartment_list` WHERE `user_admin_id`='$user_admin_id' AND `user_apar_id`='$user_apar_id' AND`active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."' ORDER BY `user_apar_unique_id` DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['user_apar_id'] =  $val['user_apar_id'];
        $response['user_apar_type'] =  $val['user_apar_type'];
        $response['user_apar_name'] =  $val['user_apar_name'];
        $response['user_apar_configuration'] =  $val['user_apar_configuration'];
        $response['user_apar_house_no'] =  $val['user_apar_house_no'];
        $response['user_apar_built_up_area'] =  $val['user_apar_built_up_area'];
        $response['user_apar_bedrooms'] =  $val['user_apar_bedrooms'];
        $response['user_apar_bathrooms'] =  $val['user_apar_bathrooms'];
        $response['user_apar_balconies'] =  $val['user_apar_balconies'];
        $response['user_apar_furnished'] =  $val['user_apar_furnished'];
        $response['user_apar_wardrobe'] =  $val['user_apar_wardrobe'];
        $response['user_apar_fans'] =  $val['user_apar_fans'];
        $response['user_apar_light'] =  $val['user_apar_light'];
        $response['user_apar_modular_kitchen'] =  $val['user_apar_modular_kitchen'];
        $response['user_apar_furnished_name'] =  $val['user_apar_furnished_name'];
        $response['user_apar_reserved_parking'] =  $val['user_apar_reserved_parking'];
        $response['user_apar_address'] =  $val['user_apar_address'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
function user_apartment_modify($user_admin_id,$mobile,$user_apar_name,$user_apar_type,$user_apar_configuration,$user_apar_house_no,$user_apar_built_up_area,$user_apar_bedrooms,$user_apar_bathrooms,$user_apar_balconies,$user_apar_furnished,$user_apar_furnished_name,$user_apar_reserved_parking,$user_apar_address,$user_apar_id,$user_apar_wardrobe,$user_apar_fans,$user_apar_light,$user_apar_modular_kitchen){
    global $pdoconn;
    $sql="UPDATE `user_apartment_list` SET `updated_by`='$mobile',`updated_date`='".TIME."',`active_flag`='".FLAG_N."' WHERE `user_apar_id`='$user_apar_id' AND `user_admin_id`='$user_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        #..
        $response = user_apartment_add($user_admin_id,$mobile,$user_apar_name,$user_apar_type,$user_apar_configuration,$user_apar_house_no,$user_apar_built_up_area,$user_apar_bedrooms,$user_apar_bathrooms,$user_apar_balconies,$user_apar_furnished,$user_apar_furnished_name,$user_apar_reserved_parking,$user_apar_address,$user_apar_id,$user_apar_wardrobe,$user_apar_fans,$user_apar_light,$user_apar_modular_kitchen);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
        $response = json_encode($response);
    }
    return $response;
}
?>