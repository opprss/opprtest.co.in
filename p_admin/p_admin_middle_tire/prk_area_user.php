<?php
/*
Description: parking area employ create.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
    // require_once 'api/global_api.php';
    require_once 'api/parkAreaReg_api.php';
    $response = array();
 
    if(isAvailable(array('prk_admin_id','prk_user_username','prk_user_emp_no','prk_user_name','prk_user_password','prk_user_repassword','prk_user_mobile','prk_user_gender','prk_user_address','prk_user_landmark','prk_user_state','prk_user_city','prk_user_pincode','inserted_by','token','prk_user_emp_category'))){

        if(isEmpty(array('prk_admin_id','prk_user_username','prk_user_emp_no','prk_user_name','prk_user_password','prk_user_repassword','prk_user_mobile','prk_user_gender','prk_user_address','prk_user_state','prk_user_city','prk_user_pincode','inserted_by','token','prk_user_emp_category'))){
            $prk_admin_id = trim($_POST['prk_admin_id']);
            $prk_user_username = trim($_POST['prk_user_username']);
            $prk_user_emp_no = trim($_POST['prk_user_emp_no']);
            $prk_user_name = trim($_POST['prk_user_name']);
            $prk_user_password = trim($_POST['prk_user_password']);
            $prk_user_repassword = trim($_POST['prk_user_repassword']);
            $prk_user_mobile = ($_POST['prk_user_mobile']);
            $prk_user_gender = ($_POST['prk_user_gender']);
            $prk_user_address = ($_POST['prk_user_address']);
            $prk_user_landmark = ($_POST['prk_user_landmark']);
            $prk_user_state = ($_POST['prk_user_state']);
            $prk_user_city = ($_POST['prk_user_city']);
            $prk_user_pincode = ($_POST['prk_user_pincode']);
            $inserted_by = trim($_POST['inserted_by']);
            $prk_user_emp_category = trim($_POST['prk_user_emp_category']);
            $token = trim($_POST['token']);
            $prk_user_img = isset($_POST['prk_user_img']) ? trim($_POST['prk_user_img']) : '';
            $resp=prk_token_check($prk_admin_id,$token);
            $json = json_decode($resp);
            // echo $resp;
             if($json->status){
                if (!empty($prk_user_img)) {
                    $PNG_TEMP_DIR ='./../uploades/'.$inserted_by.'/prk_user_img/';
                    if (!file_exists($PNG_TEMP_DIR)) {
                        $old_mask = umask(0);
                        mkdir($PNG_TEMP_DIR, 0777, TRUE);
                        umask($old_mask);
                    }
                    $image_parts = explode(";base64,", $prk_user_img);
                    $image_type_aux = explode("image/", $image_parts[0]);
                    $image_type = $image_type_aux[1];
                  
                    $image_base64 = base64_decode($image_parts[1]);
                    $fileName = uniqid() . '.png';
                    $file = $PNG_TEMP_DIR . $fileName;
                    file_put_contents($file, $image_base64);
                    $prk_user_img='uploades/'.$inserted_by.'/prk_user_img/'.$fileName;
                }else{

                    $prk_user_img=NULL;
                }
                $response =prk_area_user($prk_admin_id,$prk_user_username,$prk_user_emp_no,$prk_user_name,$prk_user_password,$prk_user_repassword,$prk_user_mobile,$prk_user_gender,$prk_user_address,$prk_user_landmark,$prk_user_state,$prk_user_city,$prk_user_pincode,$inserted_by,$prk_user_emp_category,$prk_user_img);
            }else{
                $response = $resp;
            }
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
        }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        $response = json_encode($response);
    }
    echo $response;
?>