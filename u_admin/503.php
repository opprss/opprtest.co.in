<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Site Title -->
    <title>OPPR Software Solution</title>
    <!-- Meta Description Tag -->
    <meta name="Description" content="OPPRS Software Solution">
    <!-- Favicon Icon -->
    <link rel="icon" type="image/x-icon" href="images/logo13.png" />
    <!-- Google web Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dosis:500,600" rel="stylesheet">
    <!-- Font Awesoeme Stylesheet CSS -->
    <link rel="stylesheet" href="../oppr-extra/font-awesome/css/font-awesome.min.css" />
    <!-- Themify Stylesheet CSS -->
    <link rel="stylesheet" href="../oppr-extra/css/themify-icons.min.css">
    <!-- Simple Line Icons Stylesheet CSS -->
    <link rel="stylesheet" href="../oppr-extra/css/simple-line-icons.min.css" />
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="../oppr-extra/css/bootstrap.min.css">
    <!-- Material Design Lite Stylesheet CSS -->
    <link rel="stylesheet" href="../oppr-extra/css/material.min.css" />
    <!-- Material Design Select Field Stylesheet CSS -->
    <link rel="stylesheet" href="../oppr-extra/css/mdl-selectfield.min.css">
    <!-- Animteheading Stylesheet CSS -->
    <link rel="stylesheet" href="../oppr-extra/css/animteheading.min.css" />
    <!-- Owl Carousel Stylesheet CSS -->
    <link rel="stylesheet" href="../oppr-extra/css/owl.carousel.min.css" />
    <!-- Animate Stylesheet CSS -->
    <link rel="stylesheet" href="../oppr-extra/css/animate.min.css" />
    <!-- Magnific Popup Stylesheet CSS -->
    <link rel="stylesheet" href="../oppr-extra/css/magnific-popup.min.css" />
    <!-- Flex Slider Stylesheet CSS -->
    <link rel="stylesheet" href="../oppr-extra/css/flexslider.min.css" />
    <!-- Custom Main Stylesheet CSS -->
    <link rel="stylesheet" href="../oppr-extra/css/style.css">
     <link rel="stylesheet" href="../oppr-extra/css/custom_css.css">

<!-- added Manoranjan for fbicon -->
    <meta name="description" content="OPPRSS provides best quality digital parking services and digital parking solutions."/>
<meta name="keywords" content="digital parikig solution , smart parking solution, app based parking , parking solution , automatic carparking system , parking solution in kolkata , parking ticket solutions/oppr parking solution , oppr software , parking management , autopay parking system , digital ticketing system , mobile parking solution , vehicle parking system" />
<meta property="og:title" content="oppr parking solution in kolkata" />
<meta property="og:type" content="website design" />
<meta property="og:url" content="http://opprss.com/oppr_old/opprss.com/index.php" />
<meta property="og:image" content="http://opprss.com/oppr_old/opprss.com/images/logo13.png" />
<!-- ended -->
<!-- for loading images -->
</head>
<body>
    <!-- Start Not Found Section -->
    <div class="notfound-wrapper">
        <h1 class="legal_font">Legal</h1>
        <p class="notfound-tag-1">Temporarily unavailable</p>
        <span class="notfound-tag-2 Comming_soon">Comming Soon...</span>
        <!-- <span class="notfound-tag-2">Check back after some time.</span> -->
        <div class="notfound-link">
            <a href="vehicle-list.php" class="btn btn-danger"><span>Home Page</span><i class="fa fa-star-half"></i></a>
        </div>
       <!--  <ul class="social-list notfound-social">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-google"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="#"><i class="fa fa-flickr"></i></a></li>
            <li><a href="#"><i class="fa fa-rss"></i></a></li>
        </ul> -->
    </div><!-- End Not Found Section -->
    
</body>
</html>