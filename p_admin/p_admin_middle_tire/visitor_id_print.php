<?php
	include 'api/parkAreaReg_api.php';
	@session_start();
	$visitor_gate_pass_dtl_id=base64_decode($_REQUEST['list_id']);
	$prk_admin_id = $_SESSION['prk_admin_id'];
	$prk_inf = prk_inf($prk_admin_id);
	$prk_inf = json_decode($prk_inf, true);
	$prk_area_name = $prk_inf['prk_area_name'];
	$prk_area_short_name = $prk_inf['prk_area_short_name'];
	$prk_area_logo_img = $prk_inf['prk_area_logo_img'];
	$area_address = prk_area_address_show($prk_admin_id);
	$area_address = json_decode($area_address, true);
	$prk_address = $area_address['prk_address'];
	$prk_land_mark = $area_address['prk_land_mark'];
	$prk_add_area = $area_address['prk_add_area'];
	$city_name = $area_address['city_name'];
	$state_name = $area_address['state_name'];
	$prk_add_pin = $area_address['prk_add_pin'];
	$prk_add_country = $area_address['prk_add_country'];
	$address =$prk_land_mark.', ADD-'.$prk_address.', CITY-'.$city_name.', STATE-'.$state_name.', PIN-'.$prk_add_pin;
	$visitor_gate_pass_dtl_id=visitor_gate_pass_dtl_show($visitor_gate_pass_dtl_id);
  	$vi_g_p = json_decode($visitor_gate_pass_dtl_id, true);
  	$visitor_gender = $vi_g_p['visitor_gender'];
  	if($visitor_gender=='M'){
  		$visitor_gender='MALE';
  	}else{
  		$visitor_gender='FEMALE';
  	}
  	$visitor_gate_pass_num = $vi_g_p['visitor_gate_pass_num'];
  	$visitor_name = $vi_g_p['visitor_name'];
  	$visitor_add1 = $vi_g_p['visitor_add1'];
  	$visitor_state_name = $vi_g_p['state_name'];
  	$visitor_city_name = $vi_g_p['city_name'];
  	$visitor_mobile = $vi_g_p['visitor_mobile'];
  	$effective_date = $vi_g_p['effective_date'];
  	$end_date = $vi_g_p['end_date'];
  	$visitor_img = $vi_g_p['visitor_img'];
  	$address_visitor=$visitor_add1.','.$visitor_state_name;
  	$add_len_vis=strlen($address_visitor);
  	$address_visitor =($add_len_vis>20)?mb_substr($address_visitor, 0, 20).'..':$address_visitor;
  	$prk_area_len_vis=strlen($prk_area_name);
  	$prk_area_name_list = ($prk_area_len_vis>20)?mb_substr($prk_area_name, 0, 20).'as..':$prk_area_name;
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style type="text/css">
	.tg  {border-collapse:collapse;border-spacing:0;}
	.tg td{font-family:Arial, sans-serif;font-size:11px;padding:10px 5px;overflow:hidden;word-break:normal;}
	.tg th{font-family:Arial, sans-serif;font-size:11px;font-weight:normal;padding:10px 5px;overflow:hidden;word-break:normal;}
	.tg .tg-xldj{border-color:inherit;text-align:left;line-height: 0px}
	.tg .tg-quj4{border-color:inherit;text-align:right;}
	.tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top;line-height: 0px}
	.tg .tg-0pky1{border-color:inherit;text-align:left;vertical-align:top;line-height: 16px;}
	.tg .tg-0lax{text-align:left;vertical-align:top;line-height: 0px}
	@media print{
		@page {
		   margin-top: 0px;
		   margin-bottom: 0;
		}
		body  {
		   padding-top: 85px;
		   padding-bottom: 72px ;
		   margin-left: 150px;
		}
		 #Header, #Footer { display: none !important; }
		.col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
	        float: left;
	   }
	   .col-sm-12 {
	        width: 100%;
	   }
	   .col-sm-11 {
	        width: 91.66666667%;
	   }
	   .col-sm-10 {
	        width: 83.33333333%;
	   }
	   .col-sm-9 {
	        width: 75%;
	   }
	   .col-sm-8 {
	        width: 66.66666667%;
	   }
	   .col-sm-7 {
	        width: 58.33333333%;
	   }
	   .col-sm-6 {
	        width: 50%;
	   }
	   .col-sm-5 {
	        width: 41.66666667%;
	   }
	   .col-sm-4 {
	        width: 33.33333333%;
	   }
	   .col-sm-3 {
	        width: 25%;
	   }
	   .col-sm-2 {
	        width: 16.66666667%;
	   }
	   .col-sm-1 {
	        width: 8.33333333%;
	   }
	}
</style>
<div style="width: 340px; height: auto;margin-left: 20px;margin-top: 10px;">
	<div class="col-sm-12" style="border: 2px solid black;">
		<div class="row" >
			<div class="col-sm-3" style="margin-top: 5px;"><center><img style="height: 40px;width:60px;margin-top: 2px;margin-bottom: 2px;" src="<?php echo LOGO_PDF;?>"></center></div>
			<div class="col-sm-6" style="padding-top: 8px;"><center><b><?php echo $prk_area_name; ?></b></center></div>
			<div class="col-sm-3" style="margin-top: 5px;"><center><img style="height: 40px;width:60px;margin-top: 2px;margin-bottom: 2px;" src="<?php echo PRK_BASE_URL.$prk_area_logo_img;?>"></center></div>
		</div>
		<div class="row" style="border-bottom:1px solid black">
			<div class="col-sm-3"></div>
			<div class="col-sm-6"><p style="margin: 0 9px 0px;">( Visitor Gatepass )</p></div>
			<div class="col-sm-3"></div>
		</div>
		<div class="row" style="border-bottom:1px solid black">
	      <div class="col-sm-12" style="font-size: 10px;text-align: center;"><?php echo $address; ?></div>
	    </div>
		<div class="row">
			<div class="col-sm-9" style="margin-top: 5px;">
				<div class="row">
					<div class="col-sm-12" style="font-size: 12px;margin-top: 3px;">ID No : <?php echo $visitor_gate_pass_num; ?></div>
					<div class="col-sm-12" style="font-size: 12px;margin-top: 2px;">Name: <?php echo ucwords(strtolower($visitor_name)); ?></div>
					<div class="col-sm-12" style="font-size: 12px;margin-top: 2px;">Gender: <?php echo ucwords(strtolower($visitor_gender)); ?></div>
					<div class="col-sm-12" style="font-size: 12px;margin-top: 2px;">Mobile: <?php echo $visitor_mobile; ?></div>
					<div class="col-sm-12" style="font-size: 12px;margin-top: 2px;">Address: <?=ucwords(strtolower($address_visitor));?></div>
					<div class="col-sm-12" style="font-size: 12px;margin-top: 2px;">Issue Date: <?php echo $effective_date; ?></div>
					<div class="col-sm-12" style="font-size: 12px;margin-top: 2px;">By-Order: <?php echo $prk_area_name_list; ?></div>
				</div>
			</div>
			<div class="col-sm-3" style="margin-top: 5px;">
				<div class="row">
					<div class="col-sm-12">
						<img src="<?php echo PRK_BASE_URL.''.$visitor_img; ?>" style='' width="60" height="60">
					</div>
					<div class="col-sm-12">
						<img style="height: 60px; margin-top: 5px;" src="./../../library/QRCodeGenerate/generate.php?text=<?=$visitor_mobile.'-OPPR'?>" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous">
</script>
<script type="text/javascript">
	$(window).on('load', function() {
	 	window.print();
		setTimeout(function(){window.close();}, 1);
	});
</script>
