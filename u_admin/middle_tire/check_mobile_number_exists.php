<?php 
/*
Description: mobile number check mobile allready registor  yes or no.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
    // require_once 'api/global_api.php';
    require_once 'api/reg_api.php';
    $response = array();
    if(isAvailable(array('mobile'))){
        if(isEmpty(array('mobile'))){
            $mobile = trim($_POST['mobile']);
            $response=check_mobile_number_exists($mobile);
            echo $response;
        }else{
                $response['status'] = 0;
                $response['message'] = 'All Fields Are Mandatory';
                echo json_encode($response); 
        }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        echo json_encode($response); 
    } 
?>