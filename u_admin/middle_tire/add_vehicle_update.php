<?php 
/*
Description: user vehicle update.
Developed by: Rakhal Raj Mandal
Created Date: 15-01-2018
Update date : 06-05-2018
*/ 
/*include_once 'api/reg_api.php';
// include_once 'api/global_api.php';
$response = array();
if(isAvailable(array('user_veh_dtl_id','user_veh_type','user_veh_number','user_veh_reg','user_veh_reg_exp_dt','user_admin_id','mobile','token'))){
    if(isEmpty(array('user_veh_dtl_id','user_veh_type','user_veh_number','user_admin_id','mobile','token'))){
        $user_veh_dtl_id = trim($_POST['user_veh_dtl_id']);
        $user_veh_type = trim($_POST['user_veh_type']);
        $user_veh_number = trim($_POST['user_veh_number']);
        $user_veh_reg = trim($_POST['user_veh_reg']);
        $user_veh_reg_exp_dt = trim($_POST['user_veh_reg_exp_dt']);
        $user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
        $token = ($_POST['token']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $respon=add_vehicle_update($user_veh_dtl_id,$user_veh_type,$user_veh_number,$user_veh_reg,$user_veh_reg_exp_dt,$user_admin_id,$mobile);
            echo ($respon);
        }else{
            echo ($resp);
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        echo json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response);
}*/
include_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_veh_dtl_id','user_veh_type','user_veh_number','user_veh_reg','user_veh_reg_exp_dt','user_admin_id','mobile','token'))){
 	if(isEmpty(array('user_veh_dtl_id','user_veh_type','user_veh_number','user_admin_id','mobile','token'))){
 		$user_veh_dtl_id = trim($_POST['user_veh_dtl_id']);
	 	$user_veh_type = trim($_POST['user_veh_type']);
	 	$user_veh_number = trim($_POST['user_veh_number']);
	 	$user_veh_reg = trim($_POST['user_veh_reg']);
	 	$user_veh_reg_exp_dt = trim($_POST['user_veh_reg_exp_dt']);
        $user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
        $token = ($_POST['token']);

        $user_ins_number = isset($_POST['user_ins_number']) ? trim($_POST['user_ins_number']) : '';
        $user_ins_exp_dt = isset($_POST['user_ins_exp_dt']) ? trim($_POST['user_ins_exp_dt']) : '';
        $user_emu_number = isset($_POST['user_emu_number']) ? trim($_POST['user_emu_number']) : '';
        $user_emu_exp_dt = isset($_POST['user_emu_exp_dt']) ? trim($_POST['user_emu_exp_dt']) : '';

        $rc_img = isset($_POST['rc_img']) ? trim($_POST['rc_img']) : '';
        $ins_img = isset($_POST['ins_img']) ? trim($_POST['ins_img']) : '';
        $emu_img = isset($_POST['emu_img']) ? trim($_POST['emu_img']) : '';
        $oth_doc_no1_img = isset($_POST['oth_doc_no1_img']) ? trim($_POST['oth_doc_no1_img']) : '';
        $oth_doc_no2_img = isset($_POST['oth_doc_no2_img']) ? trim($_POST['oth_doc_no2_img']) : '';

        $web_rc_img = isset($_POST['web_rc_img']) ? trim($_POST['web_rc_img']) : '';
        $web_ins_img = isset($_POST['web_ins_img']) ? trim($_POST['web_ins_img']) : '';
        $web_emu_img = isset($_POST['web_emu_img']) ? trim($_POST['web_emu_img']) : '';
        $web_oth_doc_no1_img = isset($_POST['web_oth_doc_no1_img']) ? trim($_POST['web_oth_doc_no1_img']) : '';
        $web_oth_doc_no2_img = isset($_POST['web_oth_doc_no2_img']) ? trim($_POST['web_oth_doc_no2_img']) : '';

        $oth_doc_no1 = isset($_POST['oth_doc_no1']) ? trim($_POST['oth_doc_no1']) : '';
        $oth_doc_no1_exp_dt = isset($_POST['oth_doc_no1_exp_dt']) ? trim($_POST['oth_doc_no1_exp_dt']) : '';
        $oth_doc_no2 = isset($_POST['oth_doc_no2']) ? trim($_POST['oth_doc_no2']) : '';
        $oth_doc_no2_exp_dt = isset($_POST['oth_doc_no2_exp_dt']) ? trim($_POST['oth_doc_no2_exp_dt']) : '';

        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $respon=add_vehicle_update($user_veh_dtl_id,$user_veh_type,$user_veh_number,$user_veh_reg,$user_veh_reg_exp_dt,$user_ins_number,$user_ins_exp_dt,$user_emu_number,$user_emu_exp_dt,$user_admin_id,$mobile,$rc_img,$ins_img,$emu_img,$oth_doc_no1,$oth_doc_no1_exp_dt,$oth_doc_no1_img,$oth_doc_no2,$oth_doc_no2_exp_dt,$oth_doc_no2_img,$web_rc_img,$web_ins_img,$web_emu_img,$web_oth_doc_no1_img,$web_oth_doc_no2_img);
        	echo ($respon);
        }else{
            $response['status'] = 0; 
            $response['session'] = 0;
            $response['message'] = 'Session Expired Please Login Again';
            echo json_encode($response);
        }
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response);
}
?>