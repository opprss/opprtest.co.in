<?php
/*
Description: user status check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
 //require_once 'api/api.php';
 include_once 'api/parkAreaReg_api.php';

 $response = array();
 
if(isAvailable(array('mobile'))){

 	if(isEmpty(array('mobile'))){

	 	$mobile = trim($_POST['mobile']);
	 	$json = user_status_check($mobile);

	 	$json = json_decode($json);
	 	 if ($json->status == 0) {
	 	 	 echo "false";
	 	 }else{
	 	 	echo "true";
		}

	}else{
 			$response['status'] = 0;
	 		$response['message'] = 'Mobile or password is empty';
	 		echo json_encode($response);
 	}
}else{
		 $response['status'] = 0; 
		 $response['message'] = 'Invalid API Call';
		 echo json_encode($response);
 }
 
?>