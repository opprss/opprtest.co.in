<?php
/*
Description: parking area employ create.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function flat_master($prk_admin_id,$tower_name,$flat_name,$owner_name,$living_status,$tenant_name,$effective_date,$end_date,$parking_admin_name,$crud_type,$flat_master_id,$two_wh_spa_allw,$four_wh_spa_allw,$two_wh_spa,$four_wh_spa,$contact_number,$living_name,$app_use){
   $response = array();
   $allplayerdata = array();
    global $pdoconn;
    switch ($crud_type) {
        case 'I':
            // $end_date_lod= date($effective_date,strtotime("-1 day"));
            /*$end_date_lod=date("Y-m-d",strtotime($effective_date." -1 day"));
            $sql="UPDATE `flat_master` SET `end_date`='$end_date_lod',`updated_by`='$parking_admin_name',`updated_date`='".TIME."' WHERE `tower_id`='$tower_name' AND `flat_id`='$flat_name' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."' AND '".TIME_TRN."' BETWEEN `effective_date` AND `end_date`";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $id=uniqid();
            $sql="INSERT INTO `flat_master`(`flat_master_id`, `prk_admin_id`, `flat_id`, `tower_id`, `owner_id`, `living_status`, `tenant_id`, `effective_date`, `end_date`, `inserted_by`, `inserted_date`, `two_wh_spa_allw`, `four_wh_spa_allw`, `contact_number`, `two_wh_spa`, `four_wh_spa`, `living_name`, `app_use`) VALUES ('$id','$prk_admin_id','$flat_name','$tower_name','$owner_name','$living_status','$tenant_name','$effective_date','$end_date','$parking_admin_name','".TIME."','$two_wh_spa_allw','$four_wh_spa_allw','$contact_number','$two_wh_spa','$four_wh_spa','$living_name','$app_use')";

            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Flat Details Save';
                $response['flat_master_id'] = $id;
            }else{
                $response['status'] = 0;
                $response['message'] = 'Flat Details Not Save ';
            } */
            $res_mem_id=$flat_master_id=$oth1=$oth2=$oth3=$oth4=$oth5='';
            $club_membership='N';
            $flat_mas_add=flat_master_add($prk_admin_id,$tower_name,$flat_name,$owner_name,$living_status,$tenant_name,$effective_date,$end_date,$parking_admin_name,$flat_master_id,$two_wh_spa_allw,$four_wh_spa_allw,$two_wh_spa,$four_wh_spa,$contact_number,$living_name,$app_use,$res_mem_id,$club_membership,$oth1,$oth2,$oth3,$oth4,$oth5);
            $json = json_decode($flat_mas_add);
            if($json->status){
                $response['status'] = 1;
                $response['message'] = 'Flat Details Save';
                $response['flat_master_id'] = $json->flat_master_id;
            }else{
                $response['status'] = 0;
                $response['message'] = 'Flat Details Not Save ';
            }
            break;
        case 'U':
            /*$sql="UPDATE `flat_master` SET `end_date`='$end_date',`updated_by`='$parking_admin_name',`updated_date`='".TIME."' WHERE `tower_id`='$tower_name' AND `flat_id`='$flat_name' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."' AND '".TIME_TRN."' BETWEEN `effective_date` AND `end_date`";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Flat Modify Save';
            }else{*/
                $response['status'] = 0;
                $response['message'] = 'Flat Modify Not Save ';
            // }  
            break;
        case 'S':
            $sql="SELECT `flat_master`.`flat_master_id`,
            `flat_master`.`prk_admin_id`,
            `flat_master`.`flat_id`,
            `flat_details` .`flat_name`,
            `flat_details` .`flat_size`,
            `flat_details` .`m_account_number`,
            `flat_master`.`tower_id`,
            `tower_details`.`tower_name`,
            `flat_master`.`owner_id`,
            `owner_details`.`owner_name`,
            `owner_details`.`owner_mobile`,
            `owner_details`.`owner_email`,
            `flat_master`.`living_status`,
            `flat_master`.`living_name`,
            `all_drop_down`.`full_name` as 'living_status_full',
            `flat_master`.`tenant_id`,
            `tenant_details`.`tenant_name`,
            `tenant_details`.`tenant_mobile`,
            `tenant_details`.`tenant_email`,
            `resident_member`.`res_mem_id`,
            `resident_member`.`res_mem_name`,
            `resident_member`.`res_mem_mobile`,
            `resident_member`.`res_mem_email`,
            `flat_master`.`two_wh_spa_allw`,
            `flat_master`.`four_wh_spa_allw`,
            `flat_master`.`two_wh_spa`,
            `flat_master`.`four_wh_spa`,
            `flat_master`.`contact_number`,
            `flat_master`.`app_use`,
            `flat_master`.`club_membership`,
            DATE_FORMAT(`flat_master`.`effective_date`,'%d-%m-%Y') AS `effective_date`,
            DATE_FORMAT(`flat_master`.`end_date`,'%d-%m-%Y') AS `end_date`,
            IF('".TIME_TRN."' BETWEEN `flat_master`.`effective_date` AND `flat_master`.`end_date`, '".FLAG_Y."', '".FLAG_N."') as activ_status
            FROM `flat_master` JOIN `flat_details` JOIN `tower_details` JOIN `all_drop_down` 
            LEFT JOIN `owner_details` ON `owner_details`.`owner_id`= `flat_master`.`owner_id`
            AND `owner_details`.`active_flag`='".FLAG_Y."'
            AND `owner_details`.`del_flag`='".FLAG_N."' 
            LEFT JOIN `tenant_details` ON `tenant_details`.`tenant_id`=`flat_master`.`tenant_id`
            AND `tenant_details`.`active_flag`='".FLAG_Y."'
            AND `tenant_details`.`del_flag`='".FLAG_N."' 
            LEFT JOIN `resident_member` ON `resident_member`.`res_mem_id`=`flat_master`.`res_mem_id`
            AND `resident_member`.`active_flag`='".FLAG_Y."'
            AND `resident_member`.`del_flag`='".FLAG_N."' 
            WHERE `flat_master`.`prk_admin_id`='$prk_admin_id' 
            AND `flat_master`.`flat_id`='$flat_name'
            AND `flat_master`.`tower_id`='$tower_name'
            AND `flat_master`.`flat_id`=`flat_details`.`flat_id`
            AND `flat_details`.`active_flag`='".FLAG_Y."'
            AND `flat_details`.`del_flag`='".FLAG_N."'
            AND `tower_details`.`tower_id`=`flat_master`.`tower_id`
            AND `tower_details`.`active_flag`='".FLAG_Y."'
            AND `tower_details`.`del_flag`='".FLAG_N."'
            AND `flat_master`.`active_flag`='".FLAG_Y."'
            AND `flat_master`.`del_flag`='".FLAG_N."'
            AND `all_drop_down`.`category`='APT_FLT_HLD'
            AND `all_drop_down`.`sort_name`=`flat_master`.`living_status`
            AND `all_drop_down`.`del_flag`='".FLAG_N."'
            AND '".TIME_TRN."' BETWEEN `flat_master`.`effective_date` AND `flat_master`.`end_date`";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['flat_master_id'] =  $val['flat_master_id'];
                $response['prk_admin_id'] =  $val['prk_admin_id'];
                $response['flat_id'] =  $val['flat_id'];
                $response['flat_name'] =  $val['flat_name'];
                $response['flat_size'] =  $val['flat_size'];
                $response['m_account_number'] =  $val['m_account_number'];
                $response['tower_id'] =  $val['tower_id'];
                $response['tower_name'] =  $val['tower_name'];
                $response['owner_id'] =  $val['owner_id'];
                $response['owner_name'] =  $val['owner_name'];
                $response['owner_mobile'] =  $val['owner_mobile'];
                $response['owner_email'] =  $val['owner_email'];
                $response['living_status'] =  $val['living_status'];
                $response['living_status_full'] =  $val['living_status_full'];
                $response['living_name'] =  $val['living_name'];
                $response['tenant_id'] =  $val['tenant_id'];
                $response['tenant_name'] =  $val['tenant_name'];
                $response['tenant_mobile'] =  $val['tenant_mobile'];
                $response['tenant_email'] =  $val['tenant_email'];
                $response['res_mem_id'] = $val['res_mem_id'];
                $response['res_mem_name'] = $val['res_mem_name'];
                $response['res_mem_mobile'] = $val['res_mem_mobile'];
                $response['res_mem_email'] = $val['res_mem_email'];
                $response['two_wh_spa_allw'] =  $val['two_wh_spa_allw'];
                $response['two_wh_spa'] =  $val['two_wh_spa'];
                $response['four_wh_spa_allw'] =  $val['four_wh_spa_allw'];
                $response['four_wh_spa'] =  $val['four_wh_spa'];
                $response['inter_com_mobile'] =  $val['contact_number'];
                $response['app_use'] =  $val['app_use'];
                $response['club_membership'] =  $val['club_membership'];
                $response['effective_date'] =  $val['effective_date'];
                $response['end_date'] =  $val['end_date'];
                $response['activ_status'] =  $val['activ_status'];
                $response['park_lot_no'] = park_lot_no_list_combo($prk_admin_id,$val['tower_id'],$val['flat_id']);
            }else{
                $response['status'] = 0;
                $response['message'] = 'Not Successful';
            }  
            break;
        case 'LS':
            $sql="SELECT `flat_master`.`flat_master_id`,
            `flat_master`.`prk_admin_id`,
            `flat_master`.`flat_id`,
            `flat_details` .`flat_name`,
            `flat_details` .`flat_size`,
            `flat_details` .`m_account_number`,
            `flat_master`.`tower_id`,
            `tower_details`.`tower_name`,
            `flat_master`.`owner_id`,
            `owner_details`.`owner_name`,
            `owner_details`.`owner_email`,
            `flat_master`.`living_status`,
            `flat_master`.`living_name`,
            `all_drop_down`.`full_name` as 'living_status_full',
            `flat_master`.`tenant_id`,
            `tenant_details`.`tenant_name`,
            `tenant_details`.`tenant_email`,
            `resident_member`.`res_mem_id`,
            `resident_member`.`res_mem_name`,
            `resident_member`.`res_mem_email`,
            `flat_master`.`two_wh_spa_allw`,
            if(`flat_master`.`two_wh_spa_allw`='".FLAG_Y."','YES','NO') as 'two_wh_spa_allw_show',
            if(`flat_master`.`four_wh_spa_allw`='".FLAG_Y."','YES','NO') as 'four_wh_spa_allw_show',
            `flat_master`.`four_wh_spa_allw`,
            `flat_master`.`two_wh_spa`,
            `flat_master`.`four_wh_spa`,
            `flat_master`.`contact_number`,
            if(`flat_master`.`app_use`='".FLAG_Y."','YES','NO') as 'app_use_show',
            `flat_master`.`app_use`,
            `flat_master`.`club_membership`,
            `flat_master`.`effective_date`,
            `flat_master`.`end_date`,
            IF('".TIME_TRN."' BETWEEN `flat_master`.`effective_date` AND `flat_master`.`end_date`, '".FLAG_Y."', '".FLAG_N."') as activ_status
            FROM `flat_master` JOIN `flat_details` JOIN `tower_details` JOIN `all_drop_down` 
            LEFT JOIN `owner_details` ON `owner_details`.`owner_id`= `flat_master`.`owner_id`
            AND `owner_details`.`active_flag`='".FLAG_Y."'
            AND `owner_details`.`del_flag`='".FLAG_N."' 
            LEFT JOIN `tenant_details` ON `tenant_details`.`tenant_id`=`flat_master`.`tenant_id`
            AND `tenant_details`.`active_flag`='".FLAG_Y."'
            AND `tenant_details`.`del_flag`='".FLAG_N."' 
            LEFT JOIN `resident_member` ON `resident_member`.`res_mem_id`=`flat_master`.`res_mem_id`
            AND `resident_member`.`active_flag`='".FLAG_Y."'
            AND `resident_member`.`del_flag`='".FLAG_N."' 
            WHERE `flat_master`.`prk_admin_id`='$prk_admin_id' 
            AND `flat_master`.`flat_id`='$flat_name'
            AND `flat_master`.`tower_id`='$tower_name'
            AND `flat_master`.`flat_id`=`flat_details`.`flat_id`
            AND `flat_details`.`active_flag`='".FLAG_Y."'
            AND `flat_details`.`del_flag`='".FLAG_N."'
            AND `tower_details`.`tower_id`=`flat_master`.`tower_id`
            AND `tower_details`.`active_flag`='".FLAG_Y."'
            AND `tower_details`.`del_flag`='".FLAG_N."'
            -- AND `flat_master`.`active_flag`='".FLAG_Y."'
            AND `flat_master`.`del_flag`='".FLAG_N."'
            AND `all_drop_down`.`category`='APT_FLT_HLD'
            AND `all_drop_down`.`sort_name`=`flat_master`.`living_status`
            AND `all_drop_down`.`del_flag`='".FLAG_N."'
            ORDER BY `flat_master`.`flat_master_unique_id` DESC";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $arr_catagory = $query->fetchAll();
                foreach($arr_catagory as $val){
                    $park['flat_master_id'] =  $val['flat_master_id'];
                    $park['prk_admin_id'] =  $val['prk_admin_id'];
                    $park['flat_id'] =  $val['flat_id'];
                    $park['flat_name'] =  $val['flat_name'];
                    $park['flat_size'] =  $val['flat_size'];
                    $park['m_account_number'] =  $val['m_account_number'];
                    $park['tower_id'] =  $val['tower_id'];
                    $park['tower_name'] =  $val['tower_name'];
                    $park['owner_id'] =  $val['owner_id'];
                    $park['owner_name'] =  $val['owner_name'];
                    $park['living_status'] =  $val['living_status'];
                    $park['living_status_full'] =  $val['living_status_full'];
                    $park['living_name'] =  $val['living_name'];
                    $park['tenant_id'] =  $val['tenant_id'];
                    $park['tenant_name'] =  $val['tenant_name'];
                    $park['res_mem_id'] = $val['res_mem_id'];
                    $park['res_mem_name'] = $val['res_mem_name'];
                    $park['res_mem_email'] = $val['res_mem_email'];
                    $park['two_wh_spa_allw'] =  $val['two_wh_spa_allw'];
                    $park['two_wh_spa_allw_show'] =  $val['two_wh_spa_allw_show'];
                    $park['two_wh_spa'] =  $val['two_wh_spa'];
                    $park['four_wh_spa_allw'] =  $val['four_wh_spa_allw'];
                    $park['four_wh_spa_allw_show'] =  $val['four_wh_spa_allw_show'];
                    $park['four_wh_spa'] =  $val['four_wh_spa'];
                    $park['effective_date'] =  $val['effective_date'];
                    $park['end_date'] =  $val['end_date'];
                    $park['inter_com_mobile'] =  $val['contact_number'];
                    $park['app_use'] =  $val['app_use'];
                    $park['club_membership'] =  $val['club_membership'];
                    $park['app_use_show'] =  $val['app_use_show'];
                    $park['activ_status'] =  $val['activ_status'];
                    $park['park_lot_no'] = park_lot_no_list_combo($prk_admin_id,$val['tower_id'],$val['flat_id']);
                    array_push($allplayerdata, $park);
                }
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['flat_mas_list'] = $allplayerdata;
            }else{
                $response['status'] = 0;
                $response['message'] = 'List Empty';
            }
            break;
        case 'D':
            $sql="UPDATE `flat_master` SET `del_flag`='".FLAG_Y."',`updated_by`='$parking_admin_name',`updated_date`='".TIME."' WHERE `flat_master_id`='$flat_master_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Delete Successful';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Delete Not Successful';
            }
            break;
        default:
            $response['status'] = 0;
            $response['message'] = 'Wrong Select';
            break;
    }
    return json_encode($response);  
}
function prk_lot_dtl_add($prk_admin_id,$flat_master_id,$prk_lot_dtl,$prk_lot_dtl_id,$prk_lot_number,$inserted_by){
    global $pdoconn;
    if ($prk_lot_dtl=='NEW') {
        $id=uniqid();
        $sql = "INSERT INTO `prk_lot_dtl`(`prk_lot_dtl_id`, `prk_admin_id`, `flat_master_id`, `prk_lot_number`, `inserted_by`, `inserted_date`) VALUES ('$id','$prk_admin_id','$flat_master_id','$prk_lot_number','$inserted_by','".TIME."')";
    }else{
        # code...
        $sql = "UPDATE `prk_lot_dtl` SET `prk_lot_number`='$prk_lot_number',`updated_by`='$inserted_by',`updated_date`='".TIME."',`active_flag`='".FLAG_Y."' WHERE `del_flag`='".FLAG_N."' AND `prk_lot_dtl_id`='$prk_lot_dtl_id' AND `prk_admin_id`='$prk_admin_id'";
    }
    $query  = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Successfully';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successfully';
    }  
    return json_encode($response);  
}
?>