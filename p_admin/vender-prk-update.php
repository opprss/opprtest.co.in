<!-- ################################################
  
Description: Parking area admin update parking rate.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
include "all_nav/header.php";
if (isset($_SESSION['prk_admin_id'])) {
  $parking_admin_name=$_SESSION['parking_admin_name'];
  $prk_admin_id = $_SESSION['prk_admin_id'];
}
$prk_rate_id=base64_decode($_REQUEST['list_id']);
$respose=park_area_rate_list_update($prk_rate_id);

$prk_rate_id = $respose[0]['prk_rate_id'];
$prk_vehicle_type = $respose[0]['prk_vehicle_type'];
$prk_rate_type = $respose[0]['prk_rate_type'];
$prk_vehicle_type = $respose[0]['prk_vehicle_type'];
$vehicle_type_dec = $respose[0]['vehicle_type_dec'];
$vehicle_typ_img = $respose[0]['vehicle_typ_img'];
$prk_ins_no_of_hr = $respose[0]['prk_ins_no_of_hr'];
$prk_ins_hr_rate = $respose[0]['prk_ins_hr_rate'];
$rate_hour = $respose[0]['veh_rate_hour'];
$veh_rate = $respose[0]['veh_rate'];
$prk_rate_eff_date = $respose[0]['prk_rate_eff_date'];
$prk_rate_end_date = $respose[0]['prk_rate_end_date'];
$tot_prk_space = $respose[0]['tot_prk_space'];
$prk_no_of_mt_dis = $respose[0]['prk_no_of_mt_dis'];
?>

<style type="text/css">
  .date{background-color: white}

  .size{
    font-size: 11px;
  }
.success{
  font-size: 11px;
  color: green;
}
</style>

    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Parking Rate Update</h5>
        <!--<form id="searchBar" class="search-bar" action="">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">

        <div class="card single pd-20 pd-sm-40">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <div class="row mg-b-25">
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">VEHICLE TYPE <span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="vehicle_type" id="vehicle_type" value="" style="opacity: 0.8; font-size:14px; background-color: #fafafa; color: gray;" disabled>
                        <option><?php echo $vehicle_type_dec; ?></option>
                      </select>
                    </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <?php //print_r($respose); ?>
                  <label class="form-control-label size">RATE TYPE</label>
                  <input type="text" class="form-control" name="rate" id="prk_rate_type" value="<?php echo $prk_rate_type ?>" disabled>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">INITIAL NO OF HOUR</label>
                  <input type="text" class="form-control" name="prk_ins_no_of_hr" id="prk_ins_no_of_hr" value="<?php echo  $prk_ins_no_of_hr; ?>" disabled>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">INITIAL HOUR RATE</label>
                  <input type="text" class="form-control" name="prk_ins_hr_rate" id="prk_ins_hr_rate" value="<?php echo $prk_ins_hr_rate; ?>" disabled>
                </div>
              </div><!-- col-4-->
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">RATE HOUR</label>
                  <input type="text" class="form-control"  name="veh_rate_hour" id="veh_rate_hour" value="<?php echo $rate_hour; ?>" disabled>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">RATE VALUE</label>
                  <input type="text" class="form-control"  name="veh_rate" id="veh_rate" value="<?php echo $veh_rate; ?>" disabled>
                </div>
              </div>
              <div class="col-lg-3 time">
                <div class="form-group">
                  <label class="form-control-label size">ROUND UP TIME (IN MUNITE)</label>
                  <div class="row">
                    <?php
                      $time = explode(":", $prk_no_of_mt_dis);
                    ?>
                    <div class="col-lg-3 pd-r-0">
                      <input type="text" class="form-control readonly" placeholder="HH" name="hh" id="hh" maxlength="2" value="<?php echo $time[0]; ?>" readonly="readonly">
                    </div>
                    <div class="pd-t-9 mg-0">
                      <b>:</b>
                    </div>
                    <div class="col-lg-3 pd-l-0">
                      <input type="text" class="form-control" placeholder="MM" name="mm" id="mm" maxlength="2" value="<?php echo $time[1]; ?>">
                    </div>
                  </div>
                  <span id="error_prk_no_of_mt_dis" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">EFFECTIVE DATE</label>
                  <input type="text" class="form-control readonly"  name="" id="Effective_Date" value="<?php echo $prk_rate_eff_date; ?>" disabled readonly="readonly">
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">END DATE</label>
                    <input type="text" class="form-control prevent_readonly" placeholder="MM/DD/YYYY"  name="end_date" id="end_date" value="<?php if($prk_rate_end_date == FUTURE_DATE_WEB) echo ''; else echo $prk_rate_end_date; ?>" readonly="readonly" style="background-color:transparent">
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">NO. OF PARKING SPACE <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control"  name="tot_prk_space" id="tot_prk_space" value="<?php echo $tot_prk_space; ?>">
                    <span id="error_tot_prk_space" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-layout-footer mg-t-30">
                  <div class="row">
                    <div class="col-lg-6">
                      <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" id="save">
                    </div>
                    <div class="col-lg-6">
                      <button type="button" class="btn btn-block prk_button_skip" onclick="document.location.href='vender-prk-rate';">BACK</button>
                    </div>
                    
                  </div>
                </div>  
              </div>
            </div>  
            </div>
          </div>
        </div>
      </div>     

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
<style type="text/css">
  .error_size{
  font-size:11px;
  color: red;

}
</style>
      <!-- footer part -->
    
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE_WEB ?>"
  $(document).ready(function(){
    /*type check*/

    var prk_rate_type = $('#prk_rate_type').val();
    if (prk_rate_type == 'F') {
      $('input[name="prk_no_of_mt_dis"]').attr('readonly', true);
    }else{
      $('input[name="prk_no_of_mt_dis"]').attr('readonly', false);
    }

    $("#prk_no_of_mt_dis").keyup(function(){
        var roundUp = $('#prk_no_of_mt_dis').val();
        if (roundUp > 15 && roundUp < 59){
          $.confirm({
            icon: 'fa fa-exclamation-circle',
            theme: 'modern',
            title: 'Warning',
            content: "<p style='font-size:0.9em;'>Round up time is more then 15 minute. Are you sure?</p>",
            type: 'red',
            buttons: {
              Yes: function () {
                //ok no job
              },
              No: function () {
                $('#prk_no_of_mt_dis').val('00');
              }
            }
          });
        }else if(roundUp > 59){
          $('#prk_no_of_mt_dis').val('59');
        }
    });
    $('#save').click(function(){
          var mm = $('#mm').val();
          var length = mm.length;
          /*if (mm == '') {
             mm = '00';
          }
          if(length < 2){
            mm = '0'+mm;
          }*/
          if (mm == '') {
             mm = '00';
          }else if(mm== 0){
            mm = '00';
          }else if(length<2){
             mm = '0'+mm;
          }

          var prk_no_of_mt_dis = '00:'+mm;
          var prk_vehicle_type ='<?php echo $prk_vehicle_type; ?>';
          var prk_rate_type = $('#prk_rate_type').val();
          var user_name = '<?php echo $parking_admin_name; ?>';
          var prk_admin_id ='<?php echo $prk_admin_id; ?>';
          var prk_rate_id = '<?php echo $prk_rate_id; ?>';
          
          var prk_ins_no_of_hr = $('#prk_ins_no_of_hr').val();
          var prk_ins_hr_rate = $('#prk_ins_hr_rate').val();
          var prk_per_hr_rate=$('#Initial_per_rate').val();
          var tot_prk_space = $('#tot_prk_space').val();
          var prk_rate_eff_date=$('#Effective_Date').val();
          var prk_rate_end_date=$('#end_date').val();
          var veh_rate_hour=$('#veh_rate_hour').val();
          var veh_rate=$('#veh_rate').val();
          prk_rate_end_date = (!prk_rate_end_date == '') ? prk_rate_end_date : FUTURE_DATE;

          if(tot_prk_space != ''){
          
          var urlPrkAreRateUpdate = prkUrl+'prk_area_rate_update.php';
          $('#save').val('Wait ...').prop('disabled', true);
          $.ajax({
              url :urlPrkAreRateUpdate,
              type:'POST',
              data :
              {
                'prk_rate_id':prk_rate_id,
                'prk_vehicle_type':prk_vehicle_type,
                'prk_rate_type':prk_rate_type,
                'prk_ins_no_of_hr':prk_ins_no_of_hr,
                'prk_ins_hr_rate':prk_ins_hr_rate,
                'tot_prk_space':tot_prk_space,
                'prk_rate_eff_date':prk_rate_eff_date,
                'prk_admin_id':prk_admin_id,
                'prk_rate_end_date':prk_rate_end_date,
                'user_name':user_name,
                'prk_no_of_mt_dis':prk_no_of_mt_dis,                     
                'veh_rate_hour':veh_rate_hour,                     
                'veh_rate':veh_rate,
                'token': '<?php echo $token;?>'                      
              },
              dataType:'html',
              success  :function(data)
              {
                $('#save').val('SAVE').prop('disabled', false);
                var json = $.parseJSON(data);
                  if (json.status){
                      $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success !',
                      content: "<p style='font-size:0.9em;'>Rate updated successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                            window.location='vender-prk-rate';
                        }
                      }
                    });
                  }else{
                    if( typeof json.session !== 'undefined'){
                      if (!json.session) {
                        window.location.replace("logout.php");
                      }
                    }else{
                      $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                    }
                  }
              }
            });
        }else{
        if (tot_prk_space == '') {
	      $('#error_tot_prk_space').text('Totat parking space is required');
	        //return false;
	      }
        }
    });
    
    $("#tot_prk_space").blur(function () {
      var tot_prk_space = $('#tot_prk_space').val();
      if(tot_prk_space==''){
      $('#error_tot_prk_space').text('Totat parking space is required');
      //$('#tot_prk_space').focus();

      }else{
        $('#error_tot_prk_space').text('');;
      }
  });
  // allow only numeric value for rat
  $("#first_hour,#second_hour,#initial_hr,#tot_prk_space,#mm").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0)){
      event.preventDefault();
    }
  });
  });
  $("#mm").keyup(function(){
    var mm = $('#mm').val()
    if (mm > 15 && mm < 59){
      $.confirm({
        icon: 'fa fa-exclamation-circle',
        theme: 'modern',
        title: 'Hold on!',
        content: "<p style='font-size:0.9em;'>Round up time is more then <b><span style='color:red'>'"+mm+"'</span></b> minute. Are you sure? </p>",
        type: 'red',
        buttons: {
          Yes: function () {
            //ok no job
          },
          No: function () {
            $('#mm').val('');
          }
        }
      });
    }else if(mm > 59){
      $('#mm').val('59');
    }
  });

  $('#end_date').focus(function ()  {
    var start = new Date;
    var end = "<?php echo $prk_rate_eff_date; ?>";
    var d=new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var newdate=yy+"/"+mm+"/"+dd;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24);
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
            if (date < now_end) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            return {};
        } 
    });
  });
</script>