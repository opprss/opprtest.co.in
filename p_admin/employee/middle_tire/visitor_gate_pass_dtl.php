<?php
/*
Description: Parking visitor gate pass issue.
Developed by: Rakhal Raj Mandal
Created Date: 31-10-2018
Update date :31-10-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();

if(isAvailable(array('prk_admin_id','prk_area_user_id','prk_user_username','token','visitor_mobile','visitor_name','visitor_gender','visitor_add1','visitor_add2','visitor_state','visitor_city','visitor_landmark','visitor_country','visitor_pin_cod','id_name','id_number','visitor_gate_pass_num','effective_date','end_date','visitor_vehicle_type','visitor_vehicle_number'))){
    if(isEmpty(array('prk_admin_id','prk_area_user_id','prk_user_username','token','visitor_mobile','visitor_name','visitor_gender','visitor_add1','visitor_state','visitor_city','visitor_country','visitor_pin_cod','id_name','id_number','visitor_gate_pass_num','effective_date'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $inserted_by = trim($_POST['prk_user_username']);
        $token = trim($_POST['token']);
        $visitor_mobile = trim($_POST['visitor_mobile']);
        $visitor_name = trim($_POST['visitor_name']);
        $visitor_gender = trim($_POST['visitor_gender']);
        $visitor_add1 = trim($_POST['visitor_add1']);
        $visitor_add2 = trim($_POST['visitor_add2']);
        $visitor_state = trim($_POST['visitor_state']);
        $visitor_city = trim($_POST['visitor_city']);
        $visitor_landmark = trim($_POST['visitor_landmark']);
        $visitor_country = trim($_POST['visitor_country']);
        $visitor_pin_cod = trim($_POST['visitor_pin_cod']);
        $id_name = trim($_POST['id_name']);
        $id_number = trim($_POST['id_number']);
        $visitor_gate_pass_num = trim($_POST['visitor_gate_pass_num']);
        $effective_date = $_POST['effective_date'];
        $end_date = empty($_POST['end_date'])?FUTURE_DATE_WEB:trim($_POST['end_date']);
        $visitor_vehicle_type = trim($_POST['visitor_vehicle_type']);
        $visitor_vehicle_number = trim($_POST['visitor_vehicle_number']);
        $visitor_img = isset($_REQUEST['visitor_img']) ? trim($_REQUEST['visitor_img']) : ''; 
        $id_proof_img = isset($_REQUEST['id_proof_img']) ? trim($_REQUEST['id_proof_img']) : ''; 
        $call_type = isset($_REQUEST['call_type']) ? trim($_REQUEST['call_type']) : 'ANDR'; 
        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $vis_id_prooof_img = '';
            if ($call_type=='WEB') {
                if (!empty($visitor_img)) {      
                    $PNG_TEMP_DIR ='./../../uploades/'.$inserted_by.'/visitor_pro_img/';       
                    if (!file_exists($PNG_TEMP_DIR)) {
                        $old_mask = umask(0);
                        mkdir($PNG_TEMP_DIR, 0777, TRUE);
                        umask($old_mask);
                    }
                    $image_parts = explode(";base64,", $visitor_img);
                    $image_type_aux = explode("image/", $image_parts[0]);
                    $image_type = $image_type_aux[1];
                  
                    $image_base64 = base64_decode($image_parts[1]);
                    $fileName = uniqid() . '.png';
                    $file = $PNG_TEMP_DIR . $fileName;
                    file_put_contents($file, $image_base64);
                    $visitor_img = "uploades/".$inserted_by."/visitor_pro_img/".$fileName;
                }else{

                    $visitor_img = '';
                }
                if (!empty($id_proof_img)) {
                    $PNG_TEMP_DIR ='./../../uploades/'.$inserted_by.'/vis_id_prooof_img/';       
                    if (!file_exists($PNG_TEMP_DIR)) {
                        $old_mask = umask(0);
                        mkdir($PNG_TEMP_DIR, 0777, TRUE);
                        umask($old_mask);
                    }
                    $image_parts = explode(";base64,", $id_proof_img);
                    $image_type_aux = explode("image/", $image_parts[0]);
                    $image_type = $image_type_aux[1];
                  
                    $image_base64 = base64_decode($image_parts[1]);
                    $fileName = uniqid() . '.png';
                    $file = $PNG_TEMP_DIR . $fileName;
                    file_put_contents($file, $image_base64);
                    $vis_id_prooof_img = "uploades/".$inserted_by."/vis_id_prooof_img/".$fileName;
                }else{
                    
                    $vis_id_prooof_img = '';
                }
            }else{
                if (!empty($visitor_img)) {      
                    $PNG_TEMP_DIR ='./../../uploades/'.$inserted_by.'/visitor_pro_img/';       
                    if (!file_exists($PNG_TEMP_DIR)) {
                        $old_mask = umask(0);
                        mkdir($PNG_TEMP_DIR, 0777, TRUE);
                        umask($old_mask);
                    }
                    $decode_file = base64_decode($visitor_img);
                    $file= uniqid().'.png';
                    $file_dir=$PNG_TEMP_DIR.$file;
                    if(file_put_contents($file_dir, $decode_file)){

                        $visitor_img = "uploades/".$inserted_by."/visitor_pro_img/".$file;
                    }else{

                        $visitor_img = '';
                    }
                }else{

                    $visitor_img = '';
                }
                if (!empty($id_proof_img)) {
                    $PNG_TEMP_DIR ='./../../uploades/'.$inserted_by.'/vis_id_prooof_img/';       
                    if (!file_exists($PNG_TEMP_DIR)) {
                        $old_mask = umask(0);
                        mkdir($PNG_TEMP_DIR, 0777, TRUE);
                        umask($old_mask);
                    }
                    $decode_file = base64_decode($id_proof_img);
                    $file= uniqid().'.png';
                    $file_dir=$PNG_TEMP_DIR.$file;
                    if(file_put_contents($file_dir, $decode_file)){

                        $vis_id_prooof_img = "uploades/".$inserted_by."/vis_id_prooof_img/".$file;
                    }else{

                        $vis_id_prooof_img = '';
                    }
                }else{
                    
                    $vis_id_prooof_img = '';
                }
            }
            $response = visitor_gate_pass_dtl($prk_admin_id,$visitor_mobile,$visitor_name,$visitor_gender,$vis_id_prooof_img,$visitor_add1,$visitor_add2,$visitor_state,$visitor_city,$visitor_landmark,$visitor_country,$visitor_pin_cod,$visitor_img,$id_name,$id_number,$visitor_gate_pass_num,$effective_date,$end_date,$inserted_by,$visitor_vehicle_type,$visitor_vehicle_number);
        }else{

            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>