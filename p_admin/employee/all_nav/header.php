<?php
  @session_start();
  if(!isset($_SESSION["prk_area_gate_id"])){
    header('location:gate-list');
  }
  include('../../global_asset/parking_emp_include.php');
  $prk_user_username = $_SESSION["prk_user_username"];
  $prk_user_name = $_SESSION["prk_user_name"];
  $prk_area_user_id = $_SESSION["prk_area_user_id"];
  $prk_admin_id = $_SESSION["prk_admin_id"];
  $token = $_SESSION["token"];
  $prk_area_gate_id = $_SESSION["prk_area_gate_id"];
  $prk_area_gate_type = $_SESSION["prk_area_gate_type"];
  $prk_qr_code = $_SESSION["prk_qr_code"];
  $prk_user_gate_login_id = $_SESSION["prk_user_gate_login_id"];
  $page_name = basename($_SERVER['PHP_SELF']);
  $page_permissions=emp_page_permissions_list($prk_admin_id,$prk_area_user_id,$prk_user_username,$prk_area_gate_type);
  $page_permissions_list = json_decode($page_permissions, true);

  $super_user_dtl=prk_area_super_user_dtl($prk_user_username);
  $admin_dtl = json_decode($super_user_dtl, true);
  $prk_area_name = $admin_dtl['prk_area_name'];
  $prk_add_area = $admin_dtl['prk_add_area'];
  $prk_add_city = $admin_dtl['prk_add_city'];
  $prk_add_state = $admin_dtl['prk_add_state'];
  $prk_add_country = $admin_dtl['prk_add_country'];
  $city_name = $admin_dtl['city_name'];
  $state_name = $admin_dtl['state_name'];
  $prk_area_pro_img = $admin_dtl['prk_area_pro_img'];

  $response = prk_inf($prk_admin_id);
  $asas = $response;
  $response = json_decode($response, true);
  $advance_flag = $response['advance_flag'];
  $advance_pay = $response['advance_pay'];
  $print_in_flag = $response['prient_in_flag'];
  $print_out_flag = $response['prient_out_flag'];
  $veh_out_otp_flag = $response['veh_out_otp_flag'];
  $veh_token_flag = $response['veh_token_flag'];
  $visitor_token_flag = $response['visitor_token_flag'];
  $parking_type = $response['parking_type'];
  $vis_prient_in_flag = $response['vis_prient_in_flag'];
  $vis_prient_out_flag = $response['vis_prient_out_flag'];

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>OPPRSS | A Smart parking & Security Solution</title>
    <link rel="shortcut icon" href="<?php echo FAVICON ?>" type="image/x-icon">
    <!--     <link rel="icon" href="http://indicoderz.in/img/favicon.ico" type="image/x-icon"> -->
    <!-- vendor css -->
    <link href="../../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="../../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="../../lib/jquery-toggles/toggles-full.css" rel="stylesheet">
    <link href="../../lib/rickshaw/rickshaw.min.css" rel="stylesheet">

    <!-- calender -->  
    <link rel="stylesheet" href="../../library/date//css/pickmeup.css" type="text/css" />
    <script type="text/javascript" src="../../library/date//js/pickmeup.js"></script>
  
    <!-- icon -->
    <script src="https://use.fontawesome.com/f9a581fe37.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Data Dable -->
    <link href="../../datatable/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <link href="../../datatable/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Data Dable -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

    <!-- Amanda CSS -->
    <link rel="stylesheet" href="../../css/amanda.css">
    <link rel="stylesheet" href="../../css/style.css">
  </head>
  <body>
    <script type="text/javascript">
      prk_user_token_check();
      setInterval(function(){
        prk_user_token_check();
      }, 5000);
      function prk_user_token_check(){
        var pkrEmpUrl = "<?php echo PRK_EMP_URL; ?>";
        var user_token_check = pkrEmpUrl+'prk_user_token_check.php';
        var prk_area_user_id = "<?php echo $prk_area_user_id; ?>";
        var prk_admin_id = "<?php echo $prk_admin_id; ?>";
        var token = "<?php echo $token; ?>";
        $.ajax({
          url :user_token_check,
          type:'POST',
          data :
          {
            'prk_admin_id':prk_admin_id,
            'prk_area_user_id':prk_area_user_id,
            'token':token
          },
          dataType:'html',
          success  :function(data)
          {
            var json = $.parseJSON(data);
            if (json.session) {
            }else{
             window.location.replace("../logout.php");
            }
          }
        });
      }
    </script>
    <div class="am-header"  id="header_1">
      <div class="am-header-left">
        <a id="naviconLeft" href="" class="am-navicon d-none d-lg-flex"><i class="icon ion-navicon-round"></i></a>
        <a id="naviconLeftMobile" href="" class="am-navicon d-lg-none"><i class="icon ion-navicon-round"></i></a>
        <a href="dashboard" class="am-logo">
          <img src="<?php echo LOGO ?>" style="height:auto; width:160px !important;">
        </a>
      </div><!-- am-header-left -->
      <div>
        <?php 
          $prk_area_gate_type = $_SESSION["prk_area_gate_type"]; 
          if ($prk_area_gate_type == "B") {
            $gate_type = "BOTH";
          }else if ($prk_area_gate_type == "O") {
            $gate_type = "OUT";
          }else if ($prk_area_gate_type == "I") {
            $gate_type = "IN";
          }else if ($prk_area_gate_type == "S") {
            $gate_type = "SKIP";
          }else{
            $gate_type = "";
          }
          echo "<h1 style='color:#FFF; padding-top: 20px;'>".$gate_type."</h1>";
        ?>
      </div>
      <div class="am-header-right">
        <!--/ notification -->
        <div class="dropdown dropdown-profile">
          <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
          <!--<img src="uploades/CENTRAL_4233/profile/6471518685559.jpg" class="wd-32 rounded-circle" alt=""> -->
            <span class="logged-name"><span class="hidden-xs-down">
              <?php
                  if (isset($prk_user_name))
                    //echo session_id();
                    echo $prk_user_name;
                  else
                    echo "Guest";
                ?>
            </span> <i class="fa fa-angle-down mg-l-3"></i></span>
          </a>
          <div class="dropdown-menu wd-200">
            <ul class="list-unstyled user-profile-nav">
              <li><a id="check-out" href="gate-list"><i class="icon ion-log-out"></i> Check Out</a></li>
              <li><a href="logout.php"><i class="icon ion-power"></i> Sign Out</a></li>
            </ul>
          </div><!-- dropdown-menu -->
        </div><!-- dropdown -->
      </div><!-- am-header-right -->
    </div><!-- am-header -->
    <div class="am-sideleft" >
      <div class="nav am-sideleft-tab" id="nav" style=" background-color: #00b8d4;">
      </div>
      <div class="tab-content" style="" id="sidemenu">
        <div id="mainMenu" class="tab-pane active">
          <ul class="nav am-sideleft-menu">
            <?php if ($page_permissions_list['status']){
              foreach($page_permissions_list['permission_list'] as $vall){
              if ($vall['oth2']=='M'){

              }else{
              ?>
              <li class="nav-item pd-t-8 pd-b-8">
                <h7><span><img style="width:20px; height:20px;" src="<?php echo OPPR_BASE_URL.$vall['form_img']; ?>"></span>&nbsp;&nbsp;<a  style="color:#343a40;" href="<?php echo $vall['form_php_name'] ?>"><?php echo $vall['form_display_name'] ?></a></h7>
              </li>
            <?php  }}}?>
          </ul>
        </div><!-- #mainMenu -->
      </div><!-- tab-content -->
      <nav class="navbar fixed-bottom navbar-light bg-faded">
        <div class="">
          <span style="color: #00b8d4; font-size: 0.8em;"><?php echo COPYRIGHT; ?></span>
        </div><!-- am-footer -->
      </nav>
    </div><!-- am-sideleft -->
<script type="text/javascript">    
  $(document).bind("contextmenu",function(e) {        
    e.preventDefault();    
  });
  document.onkeydown = function(e) {
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
      return false;
    }    
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
      return false;    
    }    
    if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
      return false;   
    }    
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)){ 
      return false;    
    } 
  };
</script>
<script type="text/javascript">
  var prk_user_username='<?=$prk_user_username?>';
  var prk_user_name='<?=$prk_user_name?>';
  var prk_area_user_id='<?=$prk_area_user_id?>';
  var prk_admin_id='<?=$prk_admin_id?>';
  var token='<?=$token?>';
  var prk_area_gate_id='<?=$prk_area_gate_id?>';
  var prk_area_gate_type='<?=$prk_area_gate_type?>';
  var prk_qr_code='<?=$prk_qr_code?>';
  var prk_user_gate_login_id='<?=$prk_user_gate_login_id?>';
  var pkrEmpUrl = "<?php echo PRK_EMP_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE_WEB ?>";
  var prkUrl = "<?php echo PRK_URL; ?>";
  var parking_type='<?=$parking_type?>';

</script>

