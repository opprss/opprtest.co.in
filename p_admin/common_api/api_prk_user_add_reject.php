<?php
/*
Description: Parking area vehicle rate update.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_user_add_reject($prk_admin_id,$user_name,$user_add_id){
    global $pdoconn;
    $response = array();
    // $sql = "UPDATE `user_address` SET `user_add_verify_flag`='".FLAG_Y."',`user_add_verify_by`='$user_name',`user_add_verify_date`='".TIME."' WHERE `user_add_id`='$user_add_id'";
    $sql = "UPDATE `user_address` SET `updated_by`='$user_name',`updated_date`='".TIME."',`del_flag`='".FLAG_Y."' WHERE `user_add_id`='$user_add_id' AND `prk_admin_id`='$prk_admin_id'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Sucessfull';
        $response = json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Sucessfull';
        $response = json_encode($response);
    }
    echo($response);
}
?> 