<?php
/*
Description: parking area employ create.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function board_of_members($prk_admin_id,$designation_id,$owner_id,$effective_date,$end_date,$parking_admin_name,$crud_type,$board_members_id){
   $response = array();
   $allplayerdata = array();
    global $pdoconn;
    switch ($crud_type) {
        case 'I':
            $id=uniqid();
            $sql="INSERT INTO `board_members`(`board_members_id`, `prk_admin_id`, `designation_id`, `owner_id`, `effective_date`, `end_date`, `inserted_by`, `inserted_date`) VALUES ('$id','$prk_admin_id','$designation_id','$owner_id','$effective_date','$end_date','$parking_admin_name','".TIME."')";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Successful';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Unsuccessful';
            } 
            break;
        case 'U':
            $sql="UPDATE `board_members` SET `end_date`='$end_date',`updated_by`='$parking_admin_name',`updated_date`='".TIME."' WHERE `board_members_id`='$board_members_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";

            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Designation Modify Save';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Designation Modify Not Save ';
            }  
            break;
        case 'S':
            $sql="SELECT bm.`board_members_id`,
                bm.`prk_admin_id`,
                bm.`designation_id`,
                des.`desig_name`,
                des.`desig_short_name`,
                bm.`owner_id`,
                od.`owner_name`,
                bm.`effective_date`,
                bm.`end_date`,
                IF('".TIME_TRN."' BETWEEN bm.`effective_date` AND bm.`end_date`, '".FLAG_Y."', '".FLAG_N."') as activ_status
                FROM `board_members` bm, `designation` des,`owner_details` od  
                WHERE bm.`prk_admin_id`='$prk_admin_id' 
                AND bm.`active_flag`='".FLAG_Y."' 
                AND bm.`del_flag`='".FLAG_N."'
                AND des.`designation_id`=bm.`designation_id` 
                AND des.`active_flag`='".FLAG_Y."' 
                AND des.`del_flag`='".FLAG_N."'
                AND od.`owner_id`=bm.`owner_id` 
                AND od.`active_flag`='".FLAG_Y."' 
                AND od.`del_flag`='".FLAG_N."'
                AND bm.`board_members_id`='$board_members_id'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['board_members_id'] =  $val['board_members_id'];
                $response['prk_admin_id'] =  $val['prk_admin_id'];
                $response['designation_id'] =  $val['designation_id'];
                $response['desig_name'] =  $val['desig_name'];
                $response['desig_short_name'] =  $val['desig_short_name'];
                $response['owner_id'] =  $val['owner_id'];
                $response['owner_name'] =  $val['owner_name'];
                $response['effective_date'] =  $val['effective_date'];
                $response['end_date'] =  $val['end_date'];
                $response['activ_status'] =  $val['activ_status'];
            }else{
                $response['status'] = 0;
                $response['message'] = 'Not Successful';
            }  
            break;
        case 'LS':
            $sql="SELECT bm.`board_members_id`,
                bm.`prk_admin_id`,
                bm.`designation_id`,
                des.`desig_name`,
                des.`desig_short_name`,
                bm.`owner_id`,
                od.`owner_name`,
                bm.`effective_date`,
                bm.`end_date`,
                IF('".TIME_TRN."' BETWEEN bm.`effective_date` AND bm.`end_date`, '".FLAG_Y."', '".FLAG_N."') as activ_status
                FROM `board_members` bm, `designation` des,`owner_details` od  
                WHERE bm.`prk_admin_id`='$prk_admin_id' 
                AND bm.`active_flag`='".FLAG_Y."' 
                AND bm.`del_flag`='".FLAG_N."'
                AND des.`designation_id`=bm.`designation_id` 
                AND des.`active_flag`='".FLAG_Y."' 
                AND des.`del_flag`='".FLAG_N."'
                AND od.`owner_id`=bm.`owner_id` 
                AND od.`active_flag`='".FLAG_Y."' 
                AND od.`del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $arr_catagory = $query->fetchAll();
                foreach($arr_catagory as $val){
                    $park['board_members_id'] =  $val['board_members_id'];
                    $park['prk_admin_id'] =  $val['prk_admin_id'];
                    $park['designation_id'] =  $val['designation_id'];
                    $park['desig_name'] =  $val['desig_name'];
                    $park['desig_short_name'] =  $val['desig_short_name'];
                    $park['owner_id'] =  $val['owner_id'];
                    $park['owner_name'] =  $val['owner_name'];
                    $park['effective_date'] =  $val['effective_date'];
                    $park['end_date'] =  $val['end_date'];
                    $park['activ_status'] =  $val['activ_status'];
                    array_push($allplayerdata, $park);
                }
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['members_list'] = $allplayerdata;
            }else{
                $response['status'] = 0;
                $response['message'] = 'List Empty';
            }
            break;
        case 'D':
            $sql="UPDATE `board_members` SET `del_flag`='".FLAG_Y."',`updated_by`='$parking_admin_name',`updated_date`='".TIME."' WHERE `board_members_id`='$board_members_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Delete Successful';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Delete Not Successful';
            }
            break;
        default:
            $response['status'] = 0;
            $response['message'] = 'Wrong Select';
            break;
    }
    return json_encode($response);  
}
?>