<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function prk_admin_form_list_show1($form_seq,$group_form_id){
    $allplayerdata = array();
    $response = array();
    $vehicle = array();
    global $pdoconn;
    $sql="SELECT form_id,form_name,form_display_name,form_php_name 
        FROM form_dtl 
        where group_form_id='$group_form_id' 
        AND active_flag='".FLAG_Y."' 
        AND del_flag='".FLAG_N."' 
        AND form_seq='$form_seq' 
        ORDER BY `form_order`";
    $query  = $pdoconn->prepare($sql);
    $query->execute(); 
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val) 
        {
            $vehicle['form_id'] = $val['form_id'];
            $vehicle['form_name'] = $val['form_name'];
            $vehicle['form_display_name'] = $val['form_display_name'];
            $vehicle['form_php_name'] = $val['form_php_name'];
            array_push($allplayerdata, $vehicle);
        }
        $response['status'] = 1;
        $response['message'] = 'Successfully';
        $response['form_dtl'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>