<?php
/*
Description: parking area employ delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';

$response = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','token'))){
 	if(isEmpty(array('prk_admin_id','prk_area_user_id','token'))){ 
	 	$prk_admin_id = $_POST['prk_admin_id'];
	 	$prk_area_user_id = trim($_POST['prk_area_user_id']);
	 	$token = trim($_POST['token']);
        $resp=prk_token_check($prk_admin_id,$token);
		$json = json_decode($resp);
		// echo $resp;
		if($json->status){
		    $response = prk_area_user_show($prk_admin_id,$prk_area_user_id);
		    
		}else{
		    $response = $resp;
		    //echo $response;
		}
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 	$response = json_encode($response);
}
echo $response;
?>