<?php 
/*
Description: vehicle verify and data insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','user_prk_veh_varify_id','prk_area_user_id','token','prk_user_username'))){
    if(isEmpty(array('prk_admin_id','user_prk_veh_varify_id','prk_area_user_id','token','prk_user_username'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);      
        $user_prk_veh_varify_id = trim($_POST['user_prk_veh_varify_id']);
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $prk_user_username = trim($_POST['prk_user_username']);
        $token = trim($_POST['token']);
        $veh_out_otp=NULL;
        $veh_token=NULL;
        $token_check=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($token_check);
        if($json->status=='1'){
            $sql = "SELECT * FROM `user_prk_veh_verify` WHERE `user_prk_veh_varify_id`='$user_prk_veh_varify_id' AND `user_prk_veh_verify_status`='".FLAG_F."' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $user_admin_id = $val['user_admin_id'];
                $prk_veh_type = $val['user_veh_type'];
                $veh_number = strtoupper($val['user_veh_number']);
                $user_mobile = $val['user_mobile'];
                $prk_in_rep_name = $prk_user_username;
                $prk_user_username = $prk_user_username;
                $payment_type= $val['payment_type'];
                $gate_pass_num= $val['gate_pass_num'];
                $advance_pay = 0;
                $prk_sub_unit_id = NULL;
                if($payment_type == DGP){
                    $sql = "SELECT `prk_sub_unit_id` FROM `prk_gate_pass_dtl` WHERE `prk_gate_pass_num` ='$gate_pass_num'";
                    $query  = $pdoconn->prepare($sql);
                    $query->execute();
                    $val = $query->fetch();
                    $prk_sub_unit_id = $val['prk_sub_unit_id'];
                }     
                $prk_in =prk_inf($prk_admin_id);
                $json = json_decode($prk_in);
                if($json->status=='1'){
                    $prk_admin_name = $json->prk_admin_name;
                    $advance_flag = $json->advance_flag;
                    $prient_in_flag = $json->prient_in_flag;
                    $prient_out_flag = $json->prient_out_flag;
                    $sms_flag = $json->sms_flag;
                    $prk_area_name = $json->prk_area_name;
                    $prk_area_short_name = $json->prk_area_short_name;                   
                    $veh_out_otp_flag = $json->veh_out_otp_flag;                   
                    // $veh_token_flag = $json->veh_token_flag;                   
                    $veh_out_otp_flag = $json->veh_out_otp_flag; 
                    if ($veh_out_otp_flag==FLAG_Y) {
                        $veh_out_otp = mt_rand(100000,999999);
                        $otp_mes =", Vehicle Out OTP :".$veh_out_otp;
                    }  

                   $respon = prk_veh_trc_dtl($prk_admin_id,$user_admin_id,$prk_veh_type,$veh_number,$prk_user_username,$prk_in_rep_name,$payment_type,$gate_pass_num,$prk_sub_unit_id,$advance_pay,$user_mobile,$prk_area_short_name,$veh_out_otp,$veh_token);
                   /*$respon = prk_veh_trc_dtl($prk_admin_id,$user_admin_id,$user_veh_type,$user_veh_number,$prk_in_rep_name,$prk_in_rep_name,$payment_type,$gate_pass_num,$prk_sub_unit_id,$advance_pay,$user_mobile,$prk_area_short_name,$veh_out_otp,$veh_token);*/
                    $json = json_decode($respon);
                    $status=$json->status;
                    if($status=='1')
                    {
                        $prk_veh_trc_dtl_id=$json->prk_veh_trc_dtl_id; 
                        $active_flag = FLAG_N;
                        $user_prk_veh_verify_status= FLAG_T;
                        $user_prk_remark='Message Successful';
                        $response = user_prk_veh_verify_out($user_prk_veh_varify_id,$user_prk_veh_verify_status,$user_prk_remark,$prk_veh_trc_dtl_id,$active_flag);
                    }
                }else{
                    $response = $prk_in;
                }
            }else{
                $response['status'] = 0;
                $response['message'] = 'Vehicle Already Verified';
                $response = json_encode($response);
            }
        }else{
            $response = $token_check;
        }   
    }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>