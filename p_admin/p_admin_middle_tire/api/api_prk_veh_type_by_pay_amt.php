<?php
/*
Description: prk_veh_type_by_pay_amt.
Developed by: Rakhal Raj Mandal
Created Date: 15-06-2018
Update date : ----------
*/ 
@include_once '../../connection-pdo.php';
function prk_veh_type_by_pay_amt($start_date,$end_date,$prk_admin_id){
    $allplayerdata = array();
    global $pdoconn;
    if(!empty($start_date) && !empty($end_date)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($start_date)) == date($start_date) && date($format, strtotime($end_date)) == date($end_date)) {
            $start_date= $start_date;
            $end_date=  $end_date ;
        } else {
            $start_date = date("Y-m-d", strtotime($start_date));
            $end_date = date("Y-m-d", strtotime($end_date));
        }
    }else{
        DATE;
        $end_date= TIME_TRN;
        $start_date= TIME_TRN;
    }
    $sql ="SELECT vt.vehicle_sort_nm as vehicle_type,
        count(pr.payment_amount) as veh_num,
        vt.vehicle_type_dec,
        ifnull(sum(pr.payment_amount),0) as payment_amount
        from payment_receive pr join prk_veh_trc_dtl pt
        on pt.prk_veh_trc_dtl_id = pr.prk_veh_trc_dtl_id and pr.del_flag ='".FLAG_N."'
        AND DATE_FORMAT(pr.payment_rec_date,'%Y-%m-%d')  BETWEEN '$start_date' AND '$end_date'
        AND pr.prk_admin_id = '$prk_admin_id'
        right join vehicle_type vt on  pt.prk_veh_type = vt.vehicle_sort_nm
        WHERE vt.active_flag= 'Y'
        group by vt.vehicle_sort_nm";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $response['status'] = 1;
            $response['message'] = 'Ssucessfull';
            $vehicle['vehicle_type'] = $val['vehicle_type'];
            $vehicle['veh_num'] = $val['veh_num'];
            $vehicle['payment_amount'] = $val['payment_amount'];
            array_push($allplayerdata, $vehicle);
            $response['parking_amu'] = $allplayerdata;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }    
    return json_encode($response);
}
?>