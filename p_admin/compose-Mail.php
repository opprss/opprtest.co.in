<!-- ################################################
  
Description: Tower detalis.
Developed by: Rakhal Raj 
Created Date: 12-03-2018

 ####################################################-->
<?php include "all_nav/header.php";
$mail_details=drop_down_mail_list($prk_admin_id);
$mail_details_list = json_decode($mail_details, true);
$set_val=isset($_REQUEST['domain'])?$_REQUEST['domain']:'COM';

$apartment_bulk_mail_send=apartment_bulk_mail_send_list($prk_admin_id,$parking_admin_name);
$apartment_bulk_mail_send_list = json_decode($apartment_bulk_mail_send, true);
?>

  
<style type="text/css">
  .panel-body.no-padding {
    box-shadow: none;
  }
  .hpanel .panel-body {
      background: #fff;
      border-radius: 2px;
      padding: 15px;
      position: relative;
      -webkit-box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.1);
      box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.1);
  }
  input[type=text], input[type="password"], input[type="email"], input[type="date"] {
      background: transparent;
       border-right-style: solid; 
       border-left-style: solid; 
       border-top-style: solid; 
       border-color: #94e8f5; 
      border-width: 1px;
      outline: none;
      outline: none;
      
  }
  .note-popover .note-popover-content, .note-toolbar {
    padding: 0 0 5px 0px;
    margin: 0;
    background-color: #fff;
    }
    .note-group-select-from-files {
      display: none;
    }
    .size{
      font-size: 11px;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .error{
      font-size: 11px;
      color: red;
      padding-left: 15px;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .success{
      font-size: 11px;
      color: green;
    }
</style>
<!-- links for datatable -->
  <link href="../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">

<body>
    <div class="am-mainpanel">
        <div class="am-pagetitle" id="nav_1">
            <h5 class="am-title">Mail</h5>
        </div>
        <div class="am-pagebody">
            <div class="card pd-20 pd-sm-40">
                <div class="row mg-b-25">
                    <div class="col-md-12" style="background-color: white;" id="compose_mail">
                        <div class="hpanel email-compose mg-b-15">
                            <div class="panel-heading hbuilt">
                                <div class="p-xs">
                                    <form method="get" class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                              <div class="row">
                                                <div class="col-sm-1">
                                                  <label >From:</label>
                                                </div>
                                                <div class="col-sm-4">
                                                  <input type="text" class="form-control input-sm" placeholder="" name="from_mail" value="OPPR SOFTWARE SOLUTION PVT. LTD." disabled="" id="from_mail">
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-top: 30px;">
                                            <div class="col-md-12">
                                              <div class="row">
                                                <div class="col-md-1">
                                                  <label >To:</label>
                                                </div>
                                                <div class="col-md-4">
                                                  <div class="select">
                                                    <select name="to_mail_list" id="to_mail_list" style="opacity: 0.8;font-size:14px">
                                                      <option value="">Send To Mail</option>
                                                        <?php
                                                        foreach ($mail_details_list['drop_down_list'] as $val){ ?>  
                                                          <option value="<?php echo $val['id']?>"><?php echo $val['name']?></option>
                                                        <?php          
                                                        }
                                                      ?>
                                                    </select>
                                                  </div>
                                                  <span style="display: none;" id="error_to_mail_list" class="error">This field is required.</span>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-top: 30px;">
                                            <div class="col-md-12">
                                              <div class="row">
                                                <div class="col-sm-1">
                                                  <label>Subject:</label>
                                                </div>
                                                <div class="col-sm-4">
                                                  <input type="text" class="form-control input-sm" placeholder="Enter Email subject" name="subject_mail" id="subject_mail">
                                                <span style="display: none;" id="error_subject_mail" class="error">This field is required.</span>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-top: 30px;">
                                          <div class="col-md-12">
                                            <textarea id="content" style="width:100%;height: 200px;" placeholder="Enter Text Here" class="form-control prevent_readonly"></textarea>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <div class="col-md-12">
                                            <button class="btn btn-primary ft-compse" type="button" id="send_mail">Send email</button>
                                          </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="am-pagebody">
          <div class="card pd-10 pd-sm-40">
            <div class="table-wrapper">
              <table id="datatable1" class="table display responsive" style="width: 100%;">
                <thead>
                  <tr>
                      <th >SL No.</th>
                      <th >To Mail</th>
                      <th >Subject</th>
                      <th >Date</th>
                      <th >Action</th>
                  </tr>
                  </thead>
                  <?php 
                  $c = 0;
                  if($apartment_bulk_mail_send_list['status']){
                  foreach($apartment_bulk_mail_send_list['mail_list'] as $value){
                  $c +=1;
                  ?>
                    <tr style="">
                      <td><?=$c?></td>
                      <td ><?=$value['send_to_full'];?></td>
                      <td ><?=$value['subject'];?></td>
                      <td ><?=$value['send_date'];?></td>
                      <td><a data-toggle="modal" style="cursor:pointer;" data-target="#message_show<?=$value['compose_mail_id']?>" data-placement="top" title="View Message"><i class="fa fa-eye" style="font-size:18px" aria-hidden="true"></i></a></td>
                    </tr>
                    <div id="message_show<?=$value['compose_mail_id']?>" class="modal fade">
                      <div class="modal-dialog modal-dialog-vertical-center" role="document">
                        <div class="modal-content bd-0 tx-14">
                          <div class="modal-header pd-y-20 pd-x-25">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message Preview</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body pd-25">
                            <h4 class="lh-3 mg-b-20"><a href="" class="tx-inverse hover-primary"><?=$value['subject']?></a></h4>
                            <p class="mg-b-5"><?=$value['message']?></p>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div><!-- modal-dialog -->
                    </div>
                  <?php }}?>
              </table>
            </div>
          </div>
        </div>
    </div>
</body>

<?php include"all_nav/footer.php"; ?>
<script type="text/javascript">
  var set_val="<?=$set_val?>";
  if (set_val=='sent') {
      $('#compose_mail').hide();
      $('#sent_mail').show();
  }else{
      $('#compose_mail').show();
      $('#sent_mail').hide();
  }
  // alert(set_val);
  $(document).ready(function() {
      /*$('#to_mail_list').on('change', function() {
        if(this.value == "OT") {
          $('#hidden_to').show();
        }else{
          $('#hidden_to').hide();
        }
      });*/
      $("#compose").click(function () {
          $('#compose_mail').show();
          $('#sent_mail').hide();
      });
      $("#sent").click(function () {
          $('#compose_mail').hide();
          $('#sent_mail').show();
      });
      $("#send_mail").click(function () {
          if(to_mail_list()== true & subject_mail()== true){
              send_mail();
          }
      });
  });
  function to_mail_list() {
      var to_mail_list = $("#to_mail_list").val();
      if(to_mail_list== ''){ 
          $('#error_to_mail_list').show();           
          return false;          
      }else{            
          $("#error_to_mail_list").hide();            
          return true;          
      }        
  }
  $("#to_mail_list").on('change', function(){          
    to_mail_list();        
  });
  function subject_mail() {
      var subject_mail = $("#subject_mail").val();
      if(subject_mail== ''){           
          $("#error_subject_mail").show();            
          return false;          
      }else{            
          $("#error_subject_mail").hide();            
          return true;          
      }        
  }
  $("#subject_mail").blur(function(){          
    subject_mail();        
  });
</script>
<!-- scripts for datatable -->
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }/*,
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]*/
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
</script>
<script type="text/javascript">
    var prkUrl = "<?php echo PRK_URL; ?>";
    var FUTURE_DATE = "<?php echo FUTURE_DATE ?>";
    var prk_admin_id="<?=$prk_admin_id?>"
    var parking_admin_name="<?=$parking_admin_name?>"
    var token="<?=$token?>"
    var urlPrkAreGateDtl = prkUrl+'apartment_bulk_mail_send.php';
    function send_mail(){
      var id=($("#to_mail_list").val());
      var subject=($("#subject_mail").val());
      var message=($("#content").val());
      $.ajax({
          url :urlPrkAreGateDtl,
          type:'POST',
          data :{
            'prk_admin_id':prk_admin_id,
            'id':id,
            'subject':subject,
            'message':message,
            'parking_admin_name':parking_admin_name,
            'token': token
          },
          dataType:'html',
          success  :function(data){
            //alert(data);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.9em;'>Mail Send successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                      location.reload(true);
                  }
                }
              });
            }else{
              if( typeof json.session !== 'undefined'){
                if (!json.session) {
                  window.location.replace("logout.php");
                }
              }else{
                $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.9em;'>No Member Here</p>",
                type: 'red'
              });
              }
            }
          }
      });
    }
</script>




