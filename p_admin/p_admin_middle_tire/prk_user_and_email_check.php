 <?php 
/*
Description: Parking area registration email check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
    require_once 'api/parkAreaReg_api.php';
    // require_once 'api/global_api.php';
    $response = array();
 
    if(isAvailable(array('user_name','email'))){

        if(isEmpty(array('user_name','email'))){
            $user_name = trim($_POST['user_name']);
            $email = trim($_POST['email']);
            
            $response= prk_user_and_email_check($user_name,$email);
            echo ($response);
            
        }else{
                $response['status'] = 0;
                $response['message'] = 'All Fields Are Mandatory';
                echo json_encode($response);
        }
    }else{
             $response['status'] = 0; 
             $response['message'] = 'Invalid API Call';
             echo json_encode($response);
     }

?>