<!-- ################################################
  
Description: Tower detalis.
Developed by: Rakhal Raj 
Created Date: 12-03-2018

 ####################################################-->
<?php include "all_nav/header.php";?>
<?php 
$tenant_id=base64_decode($_REQUEST['list_id']);
$crud_type='S';
// $flat_details=flat_details($prk_admin_id,$tower_name,$effective_date,$end_date,$parking_admin_name,$tower_id,$crud_type);
$tenant_details=tenant_details($prk_admin_id,'','','','','','','',$tenant_id,'','','','','',$parking_admin_name,$crud_type);
$tenant_details_show = json_decode($tenant_details, true);
$Agreement_url='#';
if (!empty($tenant_details_show['tenant_agrement_copy'])) {
  # code...
  $Agreement_url=PRK_BASE_URL.$tenant_details_show['tenant_agrement_copy'];
}
//$tower_lis = tower_list($prk_admin_id);
//$tower_list = json_decode($tower_lis, true);
$category_type='ID_P';
$prk_user_emp_cat = common_drop_down_list($category_type);
$prk_user_emp_category = json_decode($prk_user_emp_cat, true);
?>

<!-- header position -->
  <style type="text/css">
    .size{
      font-size: 11px;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .error{
      font-size: 11px;
      color: red;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .success{
      font-size: 11px;
      color: green;
    }
  </style>
  <!-- links for datatable -->
  <link href="../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
    <script src="../js/webcam.min.js"></script>
  
    <div class="am-mainpanel">

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Tenant Info Modify<//?=$Agreement_url?></h5>
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40" >
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form id="park_tenant">
              <div class="row">
                  <div class="col-md-7">
                    <div class="row">
                      <div class="col-lg-3">
                        <div class="form-group">
                          <label class="form-control-label size">TENANT NAME<span class="tx-danger">*</span></label>
                          <input type="text" name="tenant_name" id="tenant_name" placeholder="Tenant Name" class="form-control prevent_readonly">
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <label class="form-control-label size">MOBILE NUMBER<span class="tx-danger">*</span></label>
                          <input type="text" name="tenant_mobile" id="tenant_mobile" placeholder="Mobile Number" class="form-control prevent_readonly" maxlength="10">
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label size">EMAIL ID<span class="tx-danger"></span></label>
                          <input type="text" name="tenant_email" id="tenant_email" placeholder="EMAIL ID" class="form-control prevent_readonly" style="text-transform: none;">
                        </div>
                      </div>
                      <div class="col-lg-2">
                        <div class="form-group">
                          <label class="form-control-label size">Gender<span class="tx-danger">*</span></label>
                            <label class="rdiobox">
                              <input type="radio" name="tenant_gender" id="tenant_gender_m" value="M" type="radio" checked>
                              <span>Male </span>
                            </label>

                            <label class="rdiobox">
                              <input type="radio" name="tenant_gender" id="tenant_gender_f" value="F" type="radio">
                              <span>Female </span>
                            </label>
                            <span style="position: absolute;" id="error_own_gender" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <label class="form-control-label size">TYPE OF ID PROOF<span class="tx-danger"></span></label>
                          <div class="select">
                            <select name="id_name" id="id_name" style="opacity: 0.8;font-size:14px">
                              <option value="">Select ID</option>
                                <?php
                                  foreach ($prk_user_emp_category['drop_down'] as $val){ ?>  
                                    <option value="<?php echo $val['sort_name']?>"><?php echo $val['full_name']?></option>
                                  <?php          
                                  }
                                ?>
                            </select>
                          </div>
                          <span style="position: absolute; display: none;" id="error_id_type" class="error">This field is required.</span>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label size">ID Number<span class="tx-danger"></span></label>
                          <input type="text" class="form-control" placeholder="Id Number" name="id_number" id="id_number">
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label size">Number of Family<span class="tx-danger">*</span></label>
                          <input type="text" class="form-control" placeholder="No of family" name="no_of_family" id="no_of_family">
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <label class="form-control-label size">EFFECTIVE DATE<span class="tx-danger">*</span></label>
                          <input type="text" name="effective_date" id="effective_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                          <span style="position: absolute; display: none;" id="error_effective_date" class="error">This field is required.</span>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <label class="form-control-label size">END DATE</label>
                          <input type="text" name="end_date" id="end_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                         <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-lg-2" style="font-size: 11px;">
                        <label class="form-control-label size">Download</label>
                        <div class="pd-t-15">
                          <center><a href="<?=$Agreement_url?>"><i class="fa fa-download" aria-hidden="true" style="font-size: 20px;"></i></a></center>
                        </div>
                      </div>
                      <div class="col-lg-4" style="font-size: 11px;">
                        <label class="form-control-label size">Agreement Copy</label>
                        <div class="pd-t-15">
                          
                          <input type="file" name="tenant_agrement_copy" id="tenant_agrement_copy">
                        </div>
                      </div>
                      <!-- <div class="col-lg-3"></div> -->
                      <div class="col-lg-4">
                        <div class="form-group">
                          <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <a href="tenant-details"><button class="btn btn-block btn-primary prk_button" type="button">BACK</button></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <div id="my_camera"></div><br/>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <button type="button" class="btn btn-block prk_button" onClick="take_snapshot()"><p style="margin-bottom: 0px;font-size: 11px;margin-left: -3px;">Capture<br>Image</p></button>
                          <button type="button" class="btn btn-block prk_button" id="my-button"><p style="margin-bottom: 0px;font-size: 11px;margin-left: -2px;">Upload<br>Image</p></button>
                          <input type="file" name="visitor_img" accept="image/*" id="visitor_img" style="visibility: hidden;">
                          <input type="hidden" name="tenant_image" class="tenant_image" id="tenant_image">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <?php if (empty($tenant_details_show['tenant_image'])) {?>
                          <div id="results">captured Tenant image will appear here...</div>
                          <?php }else{ ?>
                          <div id="results"><img style="height: 100px;width: 130px;" src="<?=PRK_BASE_URL.''.$tenant_details_show['tenant_image']?>"/></div>
                          <?php } ?>
                          <label style="position: absolute;" class="error_size" id="error_tenant_image"></label>
                        </div>
                      </div>
                      <div class="col-md-1"></div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <div id="my_camera2"></div><br/>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <button type="button" class="btn btn-block prk_button" onClick="take_snapshot2()"><p style="margin-bottom: 0px;font-size: 11px;margin-left: -3px;">Capture<br>Image</p></button>
                          <button type="button" class="btn btn-block prk_button" id="my-button2"><p style="margin-bottom: 0px;font-size: 11px;margin-left: -2px;">Upload<br>Image</p></button>
                          <input type="file" name="visitor_img2" accept="image/*" id="visitor_img2" style="visibility: hidden;">
                          <input type="hidden" name="id_image" class="id_image" id="id_image">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <?php if (empty($tenant_details_show['id_image'])) {?>
                          <div id="results2">captured choose Id image will appear here...</div>
                          <?php }else{ ?>
                          <div id="results2"><img style="height: 100px;width: 130px;" src="<?=PRK_BASE_URL.''.$tenant_details_show['id_image']?>"/></div>
                          <?php } ?>
                          <label style="position: absolute;" class="error_size" id="error_id_image"></label>
                        </div>
                      </div>
                      <div class="col-md-1"></div>
                      
                    </div>
                  </div>
              </div>
            </form>
          </div>
        </div>
      </div>
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#effective_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
        if (date < now) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        // $('#error_effective_date').text('');
        return {};
      } 
    });
  });
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#effective_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
        if (date < now_end) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
        }
        return {};
      } 
    });
  });
  $('#effective_date').focus(function(){
    $('#end_date').val('');
  });
</script>
<!-- scripts for datatable -->
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>

<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE ?>";
  var FUTURE_DATE_WEB = "<?php echo FUTURE_DATE_WEB ?>";

  $("#tenant_name").val("<?=$tenant_details_show['tenant_name']?>");
  $("#tenant_mobile").val("<?=$tenant_details_show['tenant_mobile']?>");
  $("#tenant_email").val("<?=$tenant_details_show['tenant_email']?>");
  $("#id_name").val("<?=$tenant_details_show['id_name']?>");
  $("#id_number").val("<?=$tenant_details_show['id_number']?>");
  $("#no_of_family").val("<?=$tenant_details_show['no_of_family']?>");
  $("#effective_date").val("<?=$tenant_details_show['effective_date']?>");
  if ("<?=$tenant_details_show['end_date']?>"==FUTURE_DATE_WEB) {
    var end_date_o='';
  }else{

    var end_date_o="<?=$tenant_details_show['end_date']?>";
  }
  $("#end_date").val(end_date_o);
  if ("<?=$tenant_details_show['tenant_gender']?>"=='M') {
    document.getElementById("tenant_gender_m").checked = true;
  }else{
    document.getElementById("tenant_gender_f").checked = true;
  }
  var tenant_id="<?=$tenant_details_show['tenant_id']?>"
  var prk_admin_id="<?=$prk_admin_id?>"
  var parking_admin_name="<?=$parking_admin_name?>"
  var token="<?=$token?>"
  var urlPrkAreGateDtl = prkUrl+'tenant_details.php';
  // alert(token);
  // $( document ).ready( function () {
  $('#save').click(function(){
    var effective_date=($("#effective_date").val());
    var end_date=($("#end_date").val());
    end_date = (!end_date == '') ? end_dat : FUTURE_DATE;
    var tenant_name=($("#tenant_name").val());
    var tenant_mobile=($("#tenant_mobile").val());
    var tenant_email=($("#tenant_email").val());
    var tenant_gender =  $('input[name=tenant_gender]:checked').val();
    var id_name=($("#id_name").val());
    var id_number=($("#id_number").val());
    var no_of_family=($("#no_of_family").val());
    var tenant_agrement_copy = $('#tenant_agrement_copy').prop('files')[0];
    var tenant_image=$('#tenant_image').val();
    var id_image=$('#id_image').val();

    var form_data = new FormData();  
    form_data.append('prk_admin_id', prk_admin_id);
    form_data.append('tenant_name', tenant_name);
    form_data.append('tenant_mobile', tenant_mobile);
    form_data.append('tenant_email', tenant_email);
    form_data.append('tenant_gender', tenant_gender);
    form_data.append('id_name', id_name);
    form_data.append('id_number', id_number);
    form_data.append('no_of_family', no_of_family);
    form_data.append('parking_admin_name', parking_admin_name);
    form_data.append('effective_date', effective_date);
    form_data.append('end_date', end_date);
    form_data.append('tenant_id', tenant_id);
    form_data.append('crud_type', 'U');
    form_data.append('tenant_agrement_copy', tenant_agrement_copy);
    form_data.append('token', token);
    form_data.append('tenant_image', tenant_image);
    form_data.append('id_image', id_image);

    if($("#park_tenant").valid()&effective_date!=''){
      // alert('ok');
       $('#save').val('Wait ...').prop('disabled', true);
      $.ajax({
        url :urlPrkAreGateDtl,
        type:'POST',//'prk_admin_id','tenant_name','tenant_mobile','tenant_email','tenant_gender','id_name','id_number','no_of_family','parking_admin_name','effective_date','end_date','tenant_id','token','crud_type'
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        dataType:'html',
        success  :function(data){
          $('#save').val('SAVE').prop('disabled', false);
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>Tenant Details Updated successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                  
                  window.location.href='tenant-details';
                }
              }
            });
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>Something went wrong</p>",
              type: 'red'
            });
            }
          }
        }
      });
    }
  });
</script>
<script type="text/javascript">
  $( document ).ready( function () {
    $( "#park_tenant" ).validate( {
      rules: {
        tenant_name: "required",
        no_of_family: "required",
        // id_number: "required",
        tenant_email: { 
          // required: true,
          email:true 
        },
        tenant_mobile: {
          required: true,
          number: true,
          minlength: 10,
        },
        // id_name: "valueNotEqualstower",
        effective_date: "valueNotEqualsEff"
      },
      messages: {
        owner_mobile: {
          required: "This field is required.",
          number: "Enter valid mobile Numberr.",
          minlength: "Enter valid mobile Number."
        },
        tenant_email: { 
          email:'Enter The Valid Email.' 

        }
      }
    });
    $("#owner_name").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 60) && (inputValue != 0 & inputValue !=32)){
        // if(!(inputValue >= 97) && (inputValue != 46)){
        event.preventDefault();
      }
    });
    $("#owner_mobile").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0)){
        event.preventDefault();
      }
    });
    $("#effective_date").blur(function () {
      var effective_date = $('#effective_date').val();
      if(effective_date == ''){
        $('#error_effective_date').show();
        return false;
      }else{
        $('#error_effective_date').hide();
        return true;
      }
    });
    /*$("#id_name").on('change', function(){
      var v_type = $('#id_name').val();
      if(v_type == "")
      {
        $('#error_id_type').show();
        return false;
      }else{
          $('#error_id_type').hide();
          return true;
      }
    });*/
    $("#tenant_mobile,#no_of_family").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0)){
        event.preventDefault();
      }
    });
  });
  $.validator.addMethod("valueNotEqualsEff", function(value){
    // alert('ok');
    arg = "";
    if (value != "") {
      return arg !== value;
    }else{
      $("#error_effective_date").show();
      return true;
    }
  });
  $.validator.addMethod("valueNotEqualstower", function(value){
    // alert(value);
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_id_type").show();
      return false;
    }
  });
  jQuery.validator.addMethod("email_accept", function(value, element, param) {
    return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
  },'please enter a valid email');
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
  $('#tenant_email').keyup(function(){
    // alert('ok');
    this.value = this.value.toLowerCase();
  });
</script>
<script language="JavaScript">
  Webcam.set({
      width: 130,
      height: 100,
      image_format: 'jpeg',
      jpeg_quality: 90
  });
  Webcam.attach( '#my_camera' );
  function take_snapshot() {
    Webcam.snap( function(data_uri) {
      $(".tenant_image").val(data_uri);
      document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
      tenant_image();
    });
  }
  $(function() {
    $("#visitor_img").change(function() {
      var file = this.files[0];
      var imagefile = file.type;
      var match= ["image/jpeg","image/png","image/jpg"];
      if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
      {
        $('#previewing').attr('src','../images/noimage.jpg');
        return false;
      }else{
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(this.files[0]);
      }
    });
  });
  function imageIsLoaded(e) {
    // alert('ok');
    $(".tenant_image").val(e.target.result);
    document.getElementById('results').innerHTML = '<img style="height: 100px;width: 130px;" src="'+e.target.result+'"/>';
    tenant_image();
  };
  $('#my-button').click(function(){
    $('#visitor_img').click();
  });
</script>
<script language="JavaScript">
  Webcam.set({
    width: 130,
    height: 100,
    image_format: 'jpeg',
    jpeg_quality: 90
  });
  Webcam.attach( '#my_camera2' );
  function take_snapshot2() {
    Webcam.snap( function(data_uri) {
      $(".id_image").val(data_uri);
      document.getElementById('results2').innerHTML = '<img src="'+data_uri+'"/>';
      id_image();
    });
  }
  $(function() {
    $("#visitor_img2").change(function() {
      var file = this.files[0];
      var imagefile = file.type;
      var match= ["image/jpeg","image/png","image/jpg"];
      if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
      {
        $('#previewing').attr('src','../images/noimage.jpg');
        return false;
      }else{
        var reader = new FileReader();
        reader.onload = imageIsLoadedaa;
        reader.readAsDataURL(this.files[0]);
      }
    });
  });
  function imageIsLoadedaa(e) {
    // alert('okk');
    $(".id_image").val(e.target.result);
    document.getElementById('results2').innerHTML = '<img style="height: 100px;width: 130px;" src="'+e.target.result+'"/>';
    id_image();
  };
  $('#my-button2').click(function(){
    $('#visitor_img2').click();
  });
</script>