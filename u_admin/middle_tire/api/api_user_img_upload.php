<?php 
/*
Description: user image upload.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_img_upload($user_img,$user_admin_id,$mobile){
	global $pdoconn;
    $response = array();
    $PNG_TEMP_DIR ='./../uploades/'.$mobile.'/profile/';
    include "phpqrcode/qrlib.php";
    if (!file_exists($PNG_TEMP_DIR)) {
        $old_mask = umask(0);
        mkdir($PNG_TEMP_DIR, 0777, TRUE);
        umask($old_mask);
    }
	$decode_file = base64_decode($user_img);
	$file= uniqid().'.png';
	$file_dir=$PNG_TEMP_DIR.$file;
    if(file_put_contents($file_dir, $decode_file)){
    	api_image_upload_compress($file_dir);
	    $file='uploades/'.$mobile.'/profile/'.$file;
	    $sql ="UPDATE `user_detail` SET `user_img`='$file' WHERE `user_admin_id`='$user_admin_id' AND `active_flag`='".FLAG_Y."'";
	    $query = $pdoconn->prepare($sql);
	    if($query->execute()){
	    	$response['status'] = 1;
	    	$response['user_img'] = $file;
	        $response['message'] = 'Upload Successfully';
	    }else{
	        $response['status'] = 0;
	        $response['message'] = 'Upload Not Successfully';
	    } 
	}else{
        $response['status'] = 0;
        $response['message'] = 'Not Image Upload';
	}
	return json_encode($response); 
}
?>