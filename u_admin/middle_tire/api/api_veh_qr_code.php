<?php
/*
Description: vehicle QR code img show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function veh_qr_code($user_veh_dtl_id){
    $response = array();
    global $pdoconn;
    $sql = "SELECT `veh_qr_code` FROM `user_vehicle_detail` WHERE `user_veh_dtl_id`='$user_veh_dtl_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['veh_qr_code'] = $val['veh_qr_code'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }  
    return json_encode($response);
}
?>