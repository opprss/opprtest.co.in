<!-- ################################################
  
Description: Tower detalis.
Developed by: Rakhal Raj 
Created Date: 12-03-2018

 ####################################################-->
<?php 
  include "all_nav/header.php";
  $res_mem_name=$res_mem_mobile=$res_mem_email=$res_mem_gender=$inserted_by=$res_mem_id=$eff_date=$end_date=$oth3=$oth4=$oth5='';
  $crud_type='LS';
  $resident_details=resident_member_details($prk_admin_id,$res_mem_name,$res_mem_mobile,$res_mem_email,$res_mem_gender,$inserted_by,$crud_type,$res_mem_id,$eff_date,$end_date,$oth3,$oth4,$oth5);
  $resident_details_list = json_decode($resident_details, true);
?>
<!-- header position -->
  <style type="text/css">
    .size{
      font-size: 11px;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .error{
      font-size: 11px;
      color: red;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .success{
      font-size: 11px;
      color: green;
    }
  </style>
  <!-- links for datatable -->
  <link href="../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
    <div class="am-mainpanel">
      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Resident Info<//?=$resident_details?></h5>
      </div><!-- am-pagetitle -->
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40" >
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form id="park_owner">
              <div class="row mg-b-25">
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Member Name<span class="tx-danger">*</span></label>
                    <input type="text" name="res_mem_name" id="res_mem_name" placeholder="Name" class="form-control prevent_readonly">
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Mobile Number<span class="tx-danger"></span></label>
                    <input type="text" name="res_mem_mobile" id="res_mem_mobile" placeholder="Mobile No." class="form-control prevent_readonly" maxlength="10">
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Email ID<span class="tx-danger"></span></label>
                    <input type="text" name="res_mem_email" id="res_mem_email" placeholder="EMAIL ID" class="form-control prevent_readonly" style="text-transform: none;">
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Gender<span class="tx-danger">*</span></label>
                      <label class="rdiobox">
                        <input type="radio" name="res_mem_gender" id="own_gender_m" value="M" type="radio" checked>
                        <span>Male </span>
                      </label>

                      <label class="rdiobox">
                        <input type="radio" name="res_mem_gender" id="own_gender_f" value="F" type="radio">
                        <span>Female </span>
                      </label>
                      <span style="position: absolute;" id="error_own_gender" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Effective Date<span class="tx-danger">*</span></label>
                    <input type="text" name="eff_date" id="eff_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">End Date</label>
                    <input type="text" name="end_date" id="end_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-layout-footer mg-t-30">
                    <div class="row">
                      <div class="col-lg-6">
                        <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
                      </div>
                      <div class="col-lg-6">
                        <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- data table -->
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40">
          <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th >Sl No.</th>
                  <th >Member Name</th>
                  <th >Mobile Number</th>
                  <th >Email Id</th>
                  <th >Effective Date</th>
                  <th >End Date</th>
                  <th >Action</th>
                </tr>
              </thead>
                <?php if($resident_details_list['status']){$c=0;
                foreach($resident_details_list['resident_list'] as $value){$c+=1;?>
                  <tr>
                    <td ><?=$c;?></td>
                    <td ><?=$value['res_mem_name'];?></td>
                    <td ><?=$value['res_mem_mobile'];?></td>
                    <td ><?=$value['res_mem_email'];?></td>
                    <td ><?=$value['eff_date'];?></td>
                    <td ><?=$value['end_date'];?></td>
                    <td >
                      <a href="<?php echo 'resident?list_id='.base64_encode($value['res_mem_id']);?>" class="pd-l-10" data-toggle="tooltip" data-placement="top" title="Modify"><i class="fa fa-pencil" style="font-size:18px; font-size:18px;"></i></a>
                      <button type="button" class="clean-button" name="delete" id="<?php echo $value['res_mem_id'];?>" data-toggle="tooltip" onclick="resident_delete(this.id)" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
                    </td>
                  </tr>
                <?php }}?>
            </table>
          </div>
        </div>
      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
<!-- footer part -->
<script type="text/javascript">
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#eff_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
        if (date < now) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        // $('#error_effective_date').text('');
        return {};
      } 
    });
  });
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#eff_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
        if (date < now_end) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
        }
        return {};
      } 
    });
  });
  $('#effective_date').focus(function(){
    $('#end_date').val('');
  });
</script>
<!-- scripts for datatable -->
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
</script>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE ?>";
  var prk_admin_id="<?=$prk_admin_id?>"
  var parking_admin_name="<?=$parking_admin_name?>"
  var token="<?=$token?>"
  var urlPrkAreGateDtl = prkUrl+'resident_member_details.php';
  // alert(token);
  // $( document ).ready( function () {
  $('#save').click(function(){
    var res_mem_name=($("#res_mem_name").val());
    var res_mem_mobile=($("#res_mem_mobile").val());
    var res_mem_email=($("#res_mem_email").val());
    var res_mem_gender =  $('input[name=res_mem_gender]:checked').val();
    var eff_date=($("#eff_date").val());
    var end_date=($("#end_date").val());
    if($("#park_owner").valid()){
       $('#save').val('Wait ...').prop('disabled', true);
       // 'prk_admin_id','res_mem_name','res_mem_mobile','res_mem_email','res_mem_gender','parking_admin_name','eff_date','end_date','token','crud_type'
      $.ajax({
        url :urlPrkAreGateDtl,
        type:'POST',
        data :{
          'prk_admin_id':prk_admin_id,
          'res_mem_name':res_mem_name,
          'res_mem_mobile':res_mem_mobile,
          'res_mem_email':res_mem_email,
          'res_mem_gender':res_mem_gender,
          'parking_admin_name':parking_admin_name,
          'eff_date':eff_date,
          'end_date':end_date,
          'crud_type':'I',
          'res_mem_id':'',
          'token': token
        },
        dataType:'html',
        success  :function(data){
          $('#save').val('SAVE').prop('disabled', false);
          var json = $.parseJSON(data);
          // alert(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>Added Successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                  location.reload(true);
                }
              }
            });
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>Something went wrong</p>",
              type: 'red'
            });
            }
          }
        }
      });
    }
  });
  function resident_delete(res_mem_id){
    // alert(tower_id);
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the Owner permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            if (res_mem_id != '') {
              $.ajax({
                url :urlPrkAreGateDtl,
                type:'POST',
                data :{
                  'prk_admin_id':prk_admin_id,
                  'res_mem_name':'',
                  'res_mem_mobile':'',
                  'res_mem_email':'',
                  'res_mem_gender':'',
                  'parking_admin_name':parking_admin_name,
                  'eff_date':'',
                  'end_date':'',
                  'crud_type':'D',
                  'res_mem_id':res_mem_id,
                  'token': token
                },
                dataType:'html',
                success  :function(data){
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Deleted Successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                          location.reload(true);
                        }
                      }
                    });
                  }else{
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.7em;'>Something went wrong 1</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
</script>
<script type="text/javascript">
  $( document ).ready( function () {
    $( "#park_owner" ).validate( {
      rules: {
        res_mem_name: "required",
        res_mem_email: { 
          // required: true,
          email:true 
        },
        res_mem_mobile: {
          // required: true,
          number: true,
          minlength: 10,
        },
        eff_date: "required"
      },
      messages: {
        res_mem_email: { 
          // required: "This field is required.",
          email:"Enter valid email."
        },
        res_mem_mobile: {
          // required: "This field is required.",
          number: "Enter valid mobile Numberr.",
          minlength: "Enter valid mobile Number."
        }
      }
    });
  });
  /*jQuery.validator.addMethod("accept", function(value, element, param) {

    return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
  },'please enter a valid email');*/
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
  $('#res_mem_email').keyup(function(){
    // alert('ok');
    this.value = this.value.toLowerCase();
  });
</script>