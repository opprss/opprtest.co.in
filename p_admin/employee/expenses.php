<!-- ################################################
Description: User Address verify
Developed by: Rakhal Raj Mandal
Created Date: 27-08-2019
####################################################-->
<?php 
  include "all_nav/header.php";
  $crud_type='LS';
  $apartment_transaction=apartment_transaction($prk_admin_id,'','','','','','','',$crud_type,'','','','');
  $apartment_transaction_list = json_decode($apartment_transaction, true);

  $category_type='PAY_TYPE';
  $payment_method = common_drop_down_list($category_type);
  $payment_method_list = json_decode($payment_method, true);
?>
<style type="text/css">
  .size{
    font-size: 11px;
    text-transform: uppercase;
  }
  .error,.error_size{
    font-size: 11px;
    color: red;
  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
  th,td{
    text-align: center;
  }
</style> 
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">

    <h5 class="am-title">Apartment Expense<?//=$apartment_transaction?></h5>
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40" >
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <form id="apart_tran">
          <div class="row mg-b-25">
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">CONCERNED DEPT/PERSON<span class="tx-danger">*</span></label>
                <input type="text" class="form-control" placeholder="Name" name="con_per_name" id="con_per_name">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">MOBILE NUMBER</label>
                <input type="text" name="con_per_mobile" id="con_per_mobile" placeholder="Mobile No" class="form-control prevent_readonly" maxlength="10">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">EMAIL ID</label>
                <input type="text" name="con_per_email" id="con_per_email" placeholder="EMAIL ID" class="form-control prevent_readonly" style="text-transform: none;">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">AMOUNT<span class="tx-danger">*</span></label>
                <input type="text" name="total_amount" id="total_amount" placeholder="Amount" class="form-control prevent_readonly" style="text-transform: none;">
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">PAYMENT METHOD<span class="tx-danger">*</span></label>
                <div class="select" style="opacity: 0.8;font-size: 14px;">
                  <select name="payment_type" id="payment_type" style="">
                    <option value="">SELECT PAYMENT MODE</option>
                    <?php  foreach ($payment_method_list['drop_down'] as $val){ ?>  
                      <option value="<?php echo $val['sort_name']?>"><?php echo $val['full_name']?></option>
                    <?php } ?> 
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-5" id="hidden_cheque" style="display: none;">
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label size">BANK NAME<span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Bank Name" name="bank_name" id="bank_name">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label size">CHEQUE NUMBER<span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Cheque Number" name="cheque_number" id="cheque_number" maxlength="6">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">REMARKS<span class="tx-danger">*</span></label>
                <textarea  name="apart_tran_remark" id="apart_tran_remark" placeholder="ENTER TEXT HERE" class="form-control prevent_readonly" style="text-transform: none;"></textarea>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="form-layout-footer mg-t-30">
                <div class="row">
                  <div class="col-lg-6">
                    <input type="hidden" name="apar_tran_by" id="apar_tran_by" value="E">
                    <input type="button" value="PAY" class="btn btn-block btn-primary prk_button" name="pay" id="pay">
                  </div>
                  <div class="col-lg-6">
                    <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form> 
      </div>
    </div>
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40">
      <div class="table-wrapper">
        <table id="datatable1" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th >SL No</th>
              <th >Concerned Dept/Person</th>
              <th >Mobile No</th>
              <th >Email Id</th>
              <th >Charge</th>
            </tr>
          </thead>
          <?php $c = 0;
            if($apartment_transaction_list['status']){
              foreach($apartment_transaction_list['apart_tran_list'] as $value){
                $c +=1; ?>
                  <tr>
                    <td ><?=$c?></td>
                    <td ><?=$value['con_per_name'];?></td>
                    <td ><?=$value['con_per_mobile'];?></td>
                    <td ><?=$value['con_per_email'];?></td>
                    <td >Rs.<?=$value['total_amount'];?>/-</td>
                  </tr>
          <?php }}?>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include"all_nav/footer.php"; ?>
<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
</script>
<script type="text/javascript">
  $(document).ready( function () {
    $( "#apart_tran" ).validate({
      rules: {
        con_per_name: "required",
        total_amount: {
          required: true,
          number: true
        },
        payment_type: "required",
        bank_name: "required",
        cheque_number: {
          required: true,
          number: true
        },
        con_per_email: { 
          email: true
        },
        con_per_mobile: {
          number: true,
          minlength: 10
        },
        apart_tran_remark: "required"
      },
      messages: {
        total_amount: {
          required: 'This field is required.',
          number: 'Enter valid Amount.'
        },
        con_per_mobile: {
          number: 'Enter valid mobile Number.',
          minlength: 'Minimum Length 10th Digit.'
        },
        con_per_email: { 

          email:'enter valid email'
        },
        cheque_number: {
          required: 'This field is required.',
          number: 'Enter valid cheque Number.'
        }
      }
    });
  });
</script>
<script type="text/javascript">
  $('#pay').click(function(){
    var con_per_name=($("#con_per_name").val());
    var con_per_mobile=($("#con_per_mobile").val());
    var con_per_email=($("#con_per_email").val());
    var bank_name=($("#bank_name").val());
    var cheque_number=($("#cheque_number").val());
    var total_amount=($("#total_amount").val());
    var payment_type=($("#payment_type").val());
    var apart_tran_remark=($("#apart_tran_remark").val());
    var apar_tran_by=($("#apar_tran_by").val());
    if($("#apart_tran").valid()){
      var urlPrkAreGateDtl = pkrEmpUrl+'expense.php';
      $('#pay').val('Wait ...').prop('disabled', true);
      // 'prk_admin_id','prk_area_user_id','prk_user_username','token','con_per_name','con_per_mobile','con_per_email','total_amount','payment_type','apart_tran_remark','bank_name','cheque_number'
      $.ajax({
        url :urlPrkAreGateDtl,
        type:'POST',
        data :{
          'prk_admin_id':prk_admin_id,
          'prk_area_user_id':prk_area_user_id,
          'prk_user_username':prk_user_username,
          'token': token,
          'con_per_name':con_per_name,
          'con_per_mobile':con_per_mobile,
          'con_per_email':con_per_email,
          'total_amount':total_amount,
          'payment_type':payment_type,
          'apart_tran_remark':apart_tran_remark,
          'bank_name':bank_name,
          'cheque_number':cheque_number,
          'apar_tran_by':apar_tran_by,
          'crud_type':'I'
        },
        dataType:'html',
        success  :function(data){
          $('#pay').val('PAY').prop('disabled', false);
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>Payment Pay successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                    location.reload(true);
                }
              }
            });
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>Somthing went wrong</p>",
              type: 'red'
            });
            }
          }
        }
      });
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#payment_type').on('change', function() {
      if(this.value == "CHEQUE") {
        $('#hidden_cheque').show(1000);
      }else{
        $('#hidden_cheque').hide(1000);
      }
    });
  });
</script>