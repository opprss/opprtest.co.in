<?php
/*
Description: parking area profile show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_area_user_show($prk_admin_id,$prk_area_user_id){
    $response = array();
    global $pdoconn;
    $sql ="SELECT `prk_user_username`,
        `prk_user_emp_category`,
        `prk_user_emp_no`,
        `prk_user_name`,
        `prk_user_password`,
        `prk_user_mobile`,
        `prk_user_gender`,
        `prk_user_img`,
        `prk_user_address`,
        `prk_user_landmark`,
        `prk_user_country`,
        `prk_user_state`,
        (SELECT `state_name` FROM `state` WHERE `e`=`prk_user_state`) as `state_name`,
        `prk_user_city`,
        (SELECT `city_name` FROM `city` WHERE `e_id`=`prk_user_city`) as `city_name`,
        `prk_user_pincode` 
        FROM `prk_area_user` WHERE `prk_admin_id`='$prk_admin_id' AND `prk_area_user_id`='$prk_area_user_id' AND `del_flag`='".FLAG_N."'";
        // IF(`prk_user_img` IS NULL or `prk_user_img` = '', (CASE WHEN `prk_user_gender`= '".FLAG_M."' then '".MALE_IMG_D."' WHEN `prk_user_gender`= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".PRK_BASE_URL."',`prk_user_img`)) as `prk_user_img`
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $response['status'] = 1;
    $response['message'] = 'Sucessful';
    $response['prk_user_username'] = $val['prk_user_username'];
    $response['prk_user_emp_category'] = $val['prk_user_emp_category'];
    $response['prk_user_emp_no'] = $val['prk_user_emp_no'];
    $response['prk_user_name'] = $val['prk_user_name'];
    $response['prk_user_password'] = $val['prk_user_password'];
    $response['prk_user_mobile'] = $val['prk_user_mobile'];
    $response['prk_user_gender'] = $val['prk_user_gender'];
    $response['prk_user_img'] = $val['prk_user_img'];
    $response['prk_user_address'] = $val['prk_user_address'];
    $response['prk_user_landmark'] = $val['prk_user_landmark'];
    $response['prk_user_country'] = $val['prk_user_country'];
    $response['prk_user_state'] = $val['prk_user_state'];
    $response['state_name'] = $val['state_name'];
    $response['prk_user_city'] = $val['prk_user_city'];
    $response['city_name'] = $val['city_name'];
    $response['prk_user_pincode'] = $val['prk_user_pincode'];

    return json_encode($response);
}
?>