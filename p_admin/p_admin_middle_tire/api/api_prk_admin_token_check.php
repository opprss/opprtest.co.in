<?php
/*
Description: user token check.
Developed by: Somen Banerjee
Created Date: -------
Update date :22-06-2018
*/
function prk_admin_token_check($prk_admin_id){
    global $pdoconn;
    $sql = "SELECT `prk_admin_id` FROM `prk_admin_token` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $sql = "UPDATE `prk_admin_token` SET `end_date` = '".TIME."', `active_flag` = '".FLAG_N."' WHERE `prk_admin_id` = '$prk_admin_id' AND `active_flag` = '".FLAG_Y."'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
    }
    return true;
}
?>