<?php 
/*
Description: parking area all details show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
    // require 'api/global_api.php';
    require 'api/parkAreaReg_api.php';
    $response = array();
 
    if(isAvailable(array('prk_user_username'))){

        if(isEmpty(array('prk_user_username'))){
            $prk_user_username = trim($_POST['prk_user_username']);
            $respon = prk_area_super_user_dtl($prk_user_username);
            echo ($respon);
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
        }
    }else{
         $response['status'] = 0; 
         $response['message'] = 'Invalid API Call';
         echo json_encode($response);
     }
?>