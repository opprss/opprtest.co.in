<!-- ################################################
  
Description: Report for each employee (gate man) in daily basis
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
  include "all_nav/header.php"; 
  if (isset($_SESSION['prk_admin_id']) && isset($_SESSION['prk_user_username']) && isset($_SESSION['prk_area_user_id']) && isset($_SESSION['token']) && isset($_SESSION['prk_area_gate_type']) && isset($_SESSION['prk_area_gate_id'])) {
   //$insert_by=$_SESSION['parking_admin_name'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
    $prk_user_username = $_SESSION["prk_user_username"];
    $prk_area_user_id = $_SESSION["prk_area_user_id"];
    $token = $_SESSION["token"];
    $prk_area_gate_type = $_SESSION["prk_area_gate_type"]; 
    $prk_area_gate_id = $_SESSION["prk_area_gate_id"];
    //Vehicle{"status":1,"message":"Successful","prk_admin_name":"GARAIA_5684","advance_flag":"N","advance_pay":"0","prient_in_flag":"N","prient_out_flag":"N"}
  }
  //if (condition) {
  $prk_admin_id= $_GET['p_id'];
  $payment_rec_emp_name= $_GET['emp_name'];
  $payment_type= $_GET['type'];
  if (isset($_GET['start_date'])) {
    $start_date= $_GET['start_date'];
  }else{
    $start_date='';
  }
  if (isset($_GET['end_date'])) {
    $end_date= $_GET['end_date'];
  }else{
    $end_date='';
  }
  // $start_date= $_GET['start_date'];
  // $end_date= $_GET['end_date'];
  $prk_inf = prk_inf($prk_admin_id);
  $prk_inf = json_decode($prk_inf, true);
  $prk_area_name = $prk_inf['prk_area_name'];
  //}
?>
<!-- vendor css -->
   
  <link href="../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
 <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Employee Payment Details</h5>
      </div><!-- am-pagetitle -->
    </div>
        <div class="am-mainpanel">

    <!-- datatable -->

 
    <!-- <div class="am-mainpanel"> -->
      <div class="am-pagebody">

        <div class="card single pd-20 pd-sm-40">
          <div class="row">
            <div class="col-md-2 pd-b-20">
              <button type="button" class="btn btn-block prk_button_skip" name="skip" id="skip" onclick="document.location.href='my-transaction';">BACK</button>
            </div>
            <div class="col-md-2 pd-b-20">
              <form action="<?php echo PRK_EMP_URL ?>prk_user_payment_type_trn_web_pdf" method="post">
                <input type="hidden" name="prk_admin_id" value="<?php echo $prk_admin_id;?>">
                <input type="hidden" name="payment_rec_emp_name" value="<?php echo $payment_rec_emp_name; ?>">
                <input type="hidden" name="payment_type" value="<?php echo $payment_type; ?>">
                <input type="hidden" name="start_date" value="<?php echo $start_date; ?>">
                <input type="hidden" name="end_date" value="<?php echo $end_date; ?>">
                <input type="hidden" name="insert_by" value="<?php echo $prk_area_name; ?>">
                <button type="submit" class="btn btn-block prk_button" name="skip" id="download">Download</button>
              </form>
              
            </div>
          </div>
          <!--test start-->
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap table-responsive table-bordered">
              <thead>
                <tr>
                  <th class="wd-5p" style="background-color: #00b8d4;">Type</th>
                  <th class="wd-15p" style="background-color: #00b8d4;">Vehicle Number</th>
                  <th class="wd-20p" style="background-color: #00b8d4;">Payment Receive Date & Time</th>
                  <th class="wd-20p" style="background-color: #00b8d4;">Payment type</th>
                  <th class="wd-15p" style="background-color: #00b8d4;">Payment Amount</th>
                </tr>
              </thead>
              <tbody id="list">
              	
              </tbody>
            </table>
          </div><!-- table-wrapper -->
          <!--/TEST end-->
        </div><!-- card -->
<!-- datatable -->
<?php include"all_nav/footer.php"; ?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
<script type="text/javascript">

  $(document).ready(function(){

    var prkUrl = "<?php echo PRK_EMP_URL; ?>";
    var imagUrl = "<?php echo OPPR_BASE_URL?>";
     var prk_admin_id = "<?php echo $prk_admin_id ;?>";
     var payment_rec_emp_name = "<?php echo $prk_user_username ;?>";
     var payment_type = "<?php echo $payment_type ;?>";

     callAjax(prk_admin_id,payment_rec_emp_name,payment_type);

      //$('#payment_rec_emp_name').change(function(){
       // var prk_area_user_id = $('#payment_rec_emp_name :selected').val();
       // var payment_rec_emp_name = $('#payment_rec_emp_name :selected').text();
        
      //});
      
    function callAjax(prk_admin_id,payment_rec_emp_name,payment_type){
      //alert("ajax call");
      var start_date = "<?php echo $start_date ;?>";
      var end_date = "<?php echo $end_date ;?>";
      var urluserPaymentHistory = prkUrl+'prk_user_payment_type_trn.php';
      // alert(urluserPaymentHistory);
      //alert(urluserPaymentHistory);
      //prk_user_payment_type_trn($prk_admin_id,$payment_rec_emp_name,$payment_type,$today,$today);
      $.ajax({
        url :urluserPaymentHistory,
        type:'POST',
        beforeSend: function(){
          $('.ajax-loader').css("visibility", "visible");
        },
        data :
        {
          'prk_admin_id':prk_admin_id,
          'payment_rec_emp_name':payment_rec_emp_name,
          'payment_type':payment_type,
          'start_date':start_date,
          'end_date':end_date
        },
        dataType:'json',
        success  :function(data)
        {
          // alert(data);
          	var demoLines = '';
          	if (data.status) {
          		if (data.payment_history.length === 0) {
                $('#download').prop('disabled', true);
                demoLines +='<tr class="text-center">\
	                  <td style="font-weight: bold; font-size: 1.2em;" colspan="6">List is empty</td>\
	                </tr>';
              }else{
              	if (download) {
                 		$('#download').prop('disabled', false);
                  }
              	for (var key in data.payment_history) {
	                //alert(key);
                  var keye =key;
                  if ( keye == "<?php echo TDGP; ?>") {
                      keye ="<?php echo TDGP_F; ?>";
                    }
                    if ( keye == "<?php echo DGP; ?>") {
                      keye ="<?php echo DGP_F; ?>";
                    }
	                demoLines += '<tr class="text-center">\
		                  <td style="font-weight: bold; font-size: 1.2em; background-color: #C0C0C0;" colspan="6">'+keye+'</td>\
		                </tr>';
	                $.each(data.payment_history[key], function(key, value) {
	                	//alert(key);
	                	//alert(value['veh_number']);
                    var payment_ty = value['payment_type'];
                    if ( payment_ty == "<?php echo TDGP; ?>") {
                      payment_ty ="<?php echo TDGP_F; ?>";
                    }
                    if ( payment_ty == "<?php echo DGP; ?>") {
                      payment_ty ="<?php echo DGP_F; ?>";
                    }
					           //alert(data.payment_history[key]['veh_number']);
					           //<img src="'+imagUrl+this["vehicle_typ_img"]+'" style="width: 60px; height: 60px; border-radius:20px;">
					           demoLines += '<tr>\
                        <td><img src="'+imagUrl+value['vehicle_typ_img']+'" style="width: 60px; height: 60px;"></td>\
                        <td>'+value['veh_number']+'</td>\
                        <td>'+value['payment_rec_date']+'</td>\
                        <td>'+payment_ty+'</td>\
                        <td>'+value['payment_amount']+'</td>\
                      </tr>';
					        });
            	  } 
                demoLines += '<tr style="text-align: right;">\
                  <td colspan="4" style="background-color: #00b8d4; color: #FFF;">\
                    <b>Total Vehicle - '+data.total_veh+'</b>\
                  </td>\
                  <td  style="background-color: #00b8d4; color: #FFF;">\
                    <b>Total Amount - Rs. '+data.total_pay+'</b>\
                  </td>\
                </tr>';
              }
          	}else{
          		demoLines +='<tr class="text-center">\
	                  <td style="font-weight: bold; font-size: 1.2em;" colspan="6">No Data Found</td>\
	                </tr>';
          	}       
          //alert(demoLines);
          $("#list").html(demoLines);
        },
        complete: function(){
          $('.ajax-loader').css("visibility", "hidden");
        }
      });
    }
    
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>
