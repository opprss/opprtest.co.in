<?php 
require_once ('../jpgraph/src/jpgraph.php');
require_once ('../jpgraph/src/jpgraph_pie.php');
require_once ('../jpgraph/src/jpgraph_pie3d.php');
function dashbord_paichat_all_zero_value($two_free,$tot_prk_space_2w_free,$title){
$data=array($two_free,$tot_prk_space_2w_free);
// print_r($data);
	// $Legends = array('Loss','Win');
    $labels  = array("Free\n(%.1f%%)","Used\n(%.1f%%)");
    $color   = array('red','red','red');
$graph = new PieGraph(550,350);
$graph->SetShadow();
	$graph->title->Set($title);
$graph->title->SetFont(FF_FONT1,FS_BOLD);
$p1 = new PiePlot3D($data);
	// $p1->SetAngle(20);
$p1->SetSize(0.5);
$p1->SetCenter(0.45);
$graph->legend->SetAbsPos(10,10,'right','top');
// $p1->SetLegends($Legends);
			$p1->SetLabels($labels);
		    $p1->SetLabelPos(1);
		    $p1->SetLabelType(PIE_VALUE_PER);
		    $p1->value->Show();
		    $p1->value->SetFont(FF_FONT1,FS_NORMAL,9);
		    $p1->value->SetColor('navy');
$graph->Add($p1);

	//add manoranjan
$gdImgHandler = $graph->Stroke(_IMG_HANDLER);
$fileName = "images/pie_chart_dashboard/imagefile.png";
$graph->img->Stream($fileName);
$graph->img->Headers();
	//$graph->img->Stream();
	//manoranjan
// $data    = array($two_free,$tot_prk_space_2w_free);
//     $Legends = array('Loss','Win');
//     $labels  = array("Loss\n(%.1f%%)","Win\n(%.1f%%)");
//     $color   = array('red','red','red');
//     $graph   = new PieGraph(550,350);
//     $graph->SetShadow();

//     $p1 = new PiePlot3D($data);
//     $p1->ExplodeSlice(1);
//     $p1->SetCenter(0.55);
//     $p1->SetLegends($Legends);
//     $graph->legend->Pos(0.5,0.1);


//     $p1->SetTheme("earth");
//     $p1->SetSliceColors($color);

//     // Setup the labels to be displayed
//     $p1->SetLabels($labels);
//     $p1->SetLabelPos(1);
//     $p1->SetLabelType(PIE_VALUE_PER);
//     $p1->value->Show();
//     $p1->value->SetFont(FF_FONT1,FS_NORMAL,9);
//     $p1->value->SetColor('navy');

//     $graph->Add($p1);
//     $gdImgHandler = $graph->Stroke(_IMG_HANDLER);
// $fileName = "images/pie_chart_dashboard/imagefile.png";
// $graph->img->Stream($fileName);
// $graph->img->Headers();
    // $graph->Stroke();



$img = $graph->Stroke(_IMG_HANDLER);
ob_start();
imagepng($img);
$img_data = ob_get_contents();
ob_end_clean();
echo '<img src="data:image/png;base64,';
echo base64_encode($img_data);
echo '"/>';
}
 ?>