<?php
/*
Description: user licence add.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
    require_once 'api/reg_api.php';
    // require_once 'api/global_api.php';
    $response = array();
if(isAvailable(array('user_admin_id','user_lic_number','user_lic_name','user_lic_veh_class','user_lic_issue_date','user_lic_issued_by','user_lic_valid_till','mobile','token','s_d_w_of','blood_gr','date_of_birth','address'))){
 	if(isEmpty(array('user_admin_id','user_lic_number','user_lic_name','user_lic_veh_class','user_lic_issue_date','user_lic_valid_till','mobile','token'))){
        
        $user_admin_id = trim($_POST['user_admin_id']);
        $user_lic_number = trim($_POST['user_lic_number']);
        $user_lic_name = trim($_POST['user_lic_name']);
        $user_lic_veh_class = trim($_POST['user_lic_veh_class']);
        $user_lic_issue_date = trim($_POST['user_lic_issue_date']);
        $user_lic_issued_by = trim($_POST['user_lic_issued_by']);
        $user_lic_valid_till = trim($_POST['user_lic_valid_till']);
        $mobile = trim($_POST['mobile']);
        $s_d_w_of = trim($_POST['s_d_w_of']);
        $blood_gr = trim($_POST['blood_gr']);
        $date_of_birth = trim($_POST['date_of_birth']);
        $address = trim($_POST['address']);
        $user_lic_img_status='D';
        $token = ($_POST['token']);
        $user_lic_img = isset($_POST['user_lic_img']) ? trim($_POST['user_lic_img']) : '';
        $user_lic_img_web = isset($_POST['user_lic_img_web']) ? trim($_POST['user_lic_img_web']) : '';
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            if (!empty($user_lic_img)) {
                $PNG_TEMP_DIR ='./../uploades/'.$mobile.'/licence/';
                if (!file_exists($PNG_TEMP_DIR)) {
                    $old_mask = umask(0);
                    mkdir($PNG_TEMP_DIR, 0777, TRUE);
                    umask($old_mask);
                }
                $decode_file = base64_decode($user_lic_img);
                $file= uniqid().'.png';
                $file_dir=$PNG_TEMP_DIR.$file;
                if(file_put_contents($file_dir, $decode_file)){
                    $user_lic_img='uploades/'.$mobile.'/licence/'.$file;
                }else{
                    $user_lic_img=NULL;
                }
            }else if (!empty($user_lic_img_web)) {
                $PNG_TEMP_DIR ='./../uploades/'.$mobile.'/licence/';
                if (!file_exists($PNG_TEMP_DIR)) {
                    $old_mask = umask(0);
                    mkdir($PNG_TEMP_DIR, 0777, TRUE);
                    umask($old_mask);
                }
                $image_parts = explode(";base64,", $user_lic_img_web);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];
              
                $image_base64 = base64_decode($image_parts[1]);
                $fileName = uniqid() . '.png';
                $file = $PNG_TEMP_DIR . $fileName;
                file_put_contents($file, $image_base64);
                $user_lic_img='uploades/'.$mobile.'/licence/'.$fileName;
            }else{

                $user_lic_img=NULL;
            }
            $response = user_lic_dtl($user_admin_id,$user_lic_number,$user_lic_name,$user_lic_veh_class,$user_lic_issue_date,$user_lic_issued_by,$user_lic_valid_till,$mobile,$s_d_w_of,$blood_gr,$date_of_birth,$address,$user_lic_img,$user_lic_img_status);
            echo ($response);
        }else{
            $response['status'] = 0; 
            $response['session'] = 0;
            $response['message'] =  'Session Expired Please Login Again';
            echo json_encode($response);
        }
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response);
 }
?>