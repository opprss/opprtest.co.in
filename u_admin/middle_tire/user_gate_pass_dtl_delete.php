<?php 
/*
Description: user gate pass delete.
Developed by: Rakhal Raj Mandal
Created Date: 18-02-2018
Update date : 18-05-2018
*/
 require_once 'api/reg_api.php';
// include_once 'api/global_api.php'; 
$response = array();
if(isAvailable(array('prk_gate_pass_id','user_admin_id','mobile','token'))){
 	if(isEmpty(array('prk_gate_pass_id','user_admin_id','mobile','token'))){
	 	$prk_gate_pass_id = trim($_POST['prk_gate_pass_id']);
	 	$user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
	 	$token = trim($_POST['token']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $response = user_gate_pass_dtl_delete($prk_gate_pass_id,$user_admin_id,$mobile);
            //echo ($response);
        }else{
            $response = $resp; 
        }
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            $response =  json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>