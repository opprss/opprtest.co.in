<!-- ################################################
  
Description: After successfull login, parking area admin redirect to this page.this is home page for parking area admin. 
Developed by: Manranjan sarkar & Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
include "all_nav/header.php"; 
/*today Earnings*/
$today = date("Y-m-d");
$response_today = prk_tot_pay_amt($today,$today,$_SESSION['prk_admin_id']);
$json_today = json_decode($response_today,true);
$total_pay_today = $json_today['total_pay']; 
/*END today earnings*/

/* START this week Earnings*/
$sunday = strtotime("last sunday");
$sunday = date('w', $sunday)==date('w') ? $sunday+7*86400 : $sunday; 
$this_week_sd = date("Y-m-d",$sunday);
$ed = strtotime(date("Y-m-d",$sunday)." +6 days");
$this_week_ed =  date("Y-m-d",$ed);
$response_week = prk_tot_pay_amt($this_week_sd,$this_week_ed,$_SESSION['prk_admin_id']);
$json_week = json_decode($response_week,true);
$total_pay_week = $json_week['total_pay']; 
/* ENDT this week Earnings*/

/*START current month*/
$first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
$last_day_this_month  = date('Y-m-t');
$response_month = prk_tot_pay_amt($first_day_this_month,$last_day_this_month,$_SESSION['prk_admin_id']);
$json_month = json_decode($response_month,true);
$total_pay_month = $json_month['total_pay'];
/*END current month*/
$response = prk_inf($_SESSION['prk_admin_id']);
  $asas = $response;
  $response = json_decode($response, true);
  $veh_out_verify_flag = $response['veh_out_verify_flag'];
  $advance_pay = $response['advance_pay'];
  $print_in_flag = $response['prient_in_flag'];
  $print_out_flag = $response['prient_out_flag'];
  $veh_out_otp_flag = $response['veh_out_otp_flag'];
  $veh_token_flag = $response['veh_token_flag'];
?>
<!-- header position -->
    <div class="am-mainpanel"><!-- closing in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Dashboard</h5>
      </div><!-- am-pagetitle -->
      <?php date_default_timezone_set('Asia/Kolkata');
        $date = date('F d/m/Y');
      ?>

      <div class="am-pagebody" style="max-width: 99%;">
        <div class="row row-sm">
          <div class="col-lg-6">
            <!-- <a href="current-veh-list"> -->
            <div class="card" style="height: 250px;">
              <div id="rs1" class="wd-100p ht-250"></div>
              <div class="overlay-body pd-x-20 pd-t-20">
                <div class="d-flex justify-content-between">
                  <?php if($veh_out_verify_flag =='Y'){ ?>
                  <div>
                    <a href="current-veh-list" class="tx-gray-600 hover-info"><h6 class="tx-12 tx-uppercase tx-inverse tx-bold mg-b-5">CURRENT VEHICLE LIST IN PARKING AREA</h6>
                    <p class="tx-12" style="color: grey"><?php echo $date; ?></p>
                  </div>
                  <i class="icon ion-more tx-16 lh-0"></i>
                </div><!-- d-flex -->
                <div class="dash-img" style="padding-top: 0px; margin-top: 0px;">
                  <div class="row" style="text-align: left; color: #333;">               
                  <?php
                    $response = array();
                    $response =vehicleType();
                    //print_r($response);
                    $count = count($response);

                    for ($i=0; $i < $count ; $i++) { 
                   ?>
                   <div class="col-lg-4 pd-b-10 tx-10">
                    <img src="<?php echo OPPR_BASE_URL.$response[$i]['vehicle_typ_img'] ?>" class="img-responsive" style="height: 35px;">
                    <?php echo $response[$i]['vehicle_type'].' (<b>'.$response[$i]['vehicle_Sort_nm'].'</b>)'?>
                    </div>
                    <?php
                    }
                  ?>
                  </div>
                </div></a>
              <?php }else{ ?>
                  <div>
                    <h6 class="tx-12 tx-uppercase tx-inverse tx-bold mg-b-5">CURRENT VEHICLE LIST IN PARKING AREA</h6>
                    <p class="tx-12" style="color: grey"><?php echo $date; ?></p>
                  </div><i class="icon ion-more tx-16 lh-0"></i>
                  </div><!-- d-flex -->
                  <div class="dash-img" style="padding-top: 0px; margin-top: 0px;">
                  <div class="row" style="text-align: left; color: #333;">               
                    <?php
                      $response = array();
                      $response =vehicleType();
                      //print_r($response);
                      $count = count($response);

                      for ($i=0; $i < $count ; $i++) { 
                    ?>
                    <div class="col-lg-4 pd-b-10 tx-10">
                      <img src="<?php echo OPPR_BASE_URL.$response[$i]['vehicle_typ_img'] ?>" class="img-responsive" style="height: 35px;">
                      <?php echo $response[$i]['vehicle_type'].' (<b>'.$response[$i]['vehicle_Sort_nm'].'</b>)'?>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              <?php } ?>
              </div>
            </div>
            <!-- </a> -->
          </div>

          <div class="col-lg-6 mg-t-15 mg-sm-t-20 mg-lg-t-0">
            <div class="card" style="height: 250px;">
              <div id="rs2" class="wd-100p ht-250"></div>
              <div class="overlay-body pd-x-20 pd-t-20">
                <div class="d-flex justify-content-between">
                  <div>
                    <h6 class="tx-12 tx-uppercase tx-inverse tx-bold mg-b-5">Earnings Overview</h6>
                    <p class="tx-12"><?php echo $date; ?></p>
                  </div>
                </div><!-- d-flex -->
                <div class="d-flex mg-b-10">
                  <div class="bd-r pd-r-10">
                    <label class="tx-12">Today</label>
                    <h1 class="tx-lato tx-inverse tx-bold"><?php echo $total_pay_today; ?></h1>
                  </div>
                  <div class="bd-r pd-x-10">
                    <label class="tx-12">This Week</label>
                    <h2 class="tx-lato tx-inverse tx-bold"><?php echo $total_pay_week; ?></h2>
                  </div>
                  <div class="pd-l-10">
                    <label class="tx-12">This Month</label>
                    <h3 class="tx-lato tx-inverse tx-bold"><?php echo $total_pay_month; ?></h3>
                  </div>
                </div>
              </div>
            </div><!-- card -->
          </div><!-- col-4 -->
        </div><!-- row -->

        <div class="row row-sm pd-t-10">
          

          <div class="col-lg-6 mg-t-15 mg-sm-t-20 mg-lg-t-0">
            <div class="card" style="height: 300px" id="graph">
              <div class="overlay-body pd-x-20 pd-t-20">
                <div class="d-flex justify-content-between">
                  <div>
                    <h6 class="tx-12 tx-uppercase tx-inverse tx-bold mg-b-5">Today's Earnings</h6>
                  </div>
                  <a href="earning-graph" class="tx-gray-600 hover-info"><i class="icon ion-more tx-16 lh-0"></i></a>
                </div><!-- d-flex -->
                <div class="tx-center">
                  <div id="container" style="width: 100%;">
                    <canvas id="canvas"></canvas>
                  </div>
                </div>
              </div>
            </div><!-- card -->
          </div><!-- col-4 -->

          <div class="col-lg-6 mg-t-15 mg-sm-t-20 mg-lg-t-0">
            <div class="card" style="height: 300px" id="graph">
              <div id="rs2" class="wd-100p ht-200"></div>
              <div class="overlay-body pd-x-20 pd-t-20">
                <div class="d-flex justify-content-between">
                  <div>
                    <h6 class="tx-12 tx-uppercase tx-inverse tx-bold mg-b-5">Parking Space Overview</h6>
                  </div>
                  <a href="space-representation" class="tx-gray-600 hover-info"><i class="icon ion-more tx-16 lh-0"></i></a>
                </div><!-- d-flex -->
                <div class="tx-center">
                  <div id="canvas-holder" style="width:100%">
                    <canvas id="chart-area"></canvas>
                    <div id="holder"></div>
                  </div>
                </div>
              </div>
            </div><!-- card -->
          </div><!-- col-4 -->
        </div>
      </div><!-- col-4 -->
    </div><!-- row -->
  </div><!-- am-pagebody -->
      
<!-- footer part -->
<?php include"all_nav/footer.php"; ?>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script src="../js/ajax.googleapis_jquery.min.js"></script>
<script src="../js/ResizeSensor.js"></script>
<script src="../js/dashboard.js"></script>
<script src="../lib/chart.js/Chart.bundle.js"></script>
<script src="../lib/chart.js/utils.js"></script>
<script>
    //var MONTHS = ['somen', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var prkUrl = "<?php echo PRK_URL; ?>"; 
    var today = "<?php echo TIME_TRN ?>"   

    $(document).ready(function(){
      window.history.pushState(null, "", window.location.href);        
      window.onpopstate = function() {
        window.history.pushState(null, "", window.location.href);
      };
      /*exmple*/
      var ctx_live = document.getElementById("canvas");
      var myChart = new Chart(ctx_live, {
        type: 'bar',
        data: {
          labels: [],
          datasets: [{
            data: [],
            borderWidth: 1,
            backgroundColor : '#00b8d4',
            borderColor:'#00b8d4',
            label: 'Total',
          }]
        },
        options: {
          responsive: true,
          title: {
            display: false,
            text: "This is somen banerjee",
          },
          legend: {
            display: false
          },
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true,
              }
            }],
              xAxes: [{
                maxBarThickness: 50
            }]
          },
            animation: {
              onComplete: function() {
                done();
              }
            }
        }
      });

      // logic to get new data
      var getData = function() {
        urlGraph = prkUrl+'prk_veh_type_by_pay_amt';

        $.ajax({
          url: urlGraph,
          type:'POST',
          data:{
            'prk_admin_id':"<?php echo $_SESSION['prk_admin_id']?>",
            'start_date':today,
            'end_date':today,
            'token': '<?php echo $token;?>'
          },
          success: function(data) {
            // process your data to pull out what you plan to use to update the chart
            // e.g. new label and a new data point
            
            // add new label and data point to chart's underlying data structures
            var json = $.parseJSON(data);
              $.each(json.parking_amu, function(val, key) {
              myChart.data.labels.push(key['vehicle_type']+" (Rs."+key['payment_amount']+")");
              myChart.data.datasets[0].data.push(key['payment_amount']);
              //myChart.data.datasets[0].backgroundColor.push(getRandomColor());

            });
             // re-render the chart
            myChart.update();
          }
        });
      };
      // get new data every 3 seconds
      //setInterval(getData, 3000);
      getData();
      /*/examle*/

    




      /*pie*/


        var ctx = document.getElementById('chart-area').getContext('2d');
        var myPie = new Chart(ctx,{
          type: 'pie',
          data: {
            datasets: [{
              data: [],
              backgroundColor: [],
              label: 'Dataset 1'
            }],
            labels: []
          },
          options: {
            responsive: true
          }
        });

        /*pie logic*/

          var getDataPie = function() {
          urlPie = prkUrl+'prk_area_rate_list';

          $.ajax({
            url: urlPie,
            type:'POST',
            data:{
              'prk_admin_id':"<?php echo $_SESSION['prk_admin_id']?>"
            },
            success: function(data) {
             
              var json = $.parseJSON(data);

                if(json!=''){
                  
                  $.each(json, function(val, key) {
                    //alert();
                    myPie.data.labels.push(key['prk_vehicle_type']+" ("+key['tot_prk_space']+")");
                    myPie.data.datasets[0].data.push(key['tot_prk_space']);
                    myPie.data.datasets[0].backgroundColor.push(getRandomColor());

                  });

                  // re-render the chart
                  myPie.update();
                }else{
                  $('#chart-area').hide();
                  $('#holder').html('<div style="margin-top: 150px">Parking Rate Yet Not Added </div>');  
                }
            }
          });
          };
        /*/pie logic*/
        getDataPie();
      /*/pie*/
    });
    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
          color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    function done() {
      // var url_base64 = document.getElementById("canvas").toDataURL("image/png");
      // var url_base64jp = document.getElementById("canvas").toDataURL("image/jpg");
      // //alert("done");
      // link1.href = url_base64;

    }
</script>
