<?php
/*
Description: Parking area gate delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function visitor_service_dtl_delete($prk_admin_id,$visitor_service_dtl_id,$parking_admin_name){
    global $pdoconn;
    $response = array();
    $sql ="UPDATE `visitor_service_dtl` SET `updated_by`='$parking_admin_name',`updated_date`='".TIME."',`del_flag`='".FLAG_Y."' WHERE `visitor_service_dtl_id`='$visitor_service_dtl_id'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Delete Sucessfull';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Delete Not Sucessfull';
        return json_encode($response);
    }
}
?>