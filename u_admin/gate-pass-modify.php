<!-- header position -->
<?php
include "all_nav/header.php";
//@session_start();

if (isset($_GET["g_id"])) {
  $user_gate_pass_dtl_id = $_GET['g_id'];
  $response = array();

  $response = getGatePassDetails($user_gate_pass_dtl_id);

  if (!empty($response)) {

    $user_vehicle_no = $response['user_vehicle_no']; 
    $user_gate_pass_num = $response['user_gate_pass_num'];
    $parking_area_id = $response['parking_area_id'];
    $effective_date = $response['effective_date'];
    $end_date = $response['end_date'];
  }

}
?>
<style>
  .size{
    font-size: 11px;
  }
.error_size{
  font-size: 11px;
  color: gray;

}
.success{
  font-size: 11px;
  color: green;
}
</style>

<!-- header position -->

    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Modify your Gate Pass</h5>
        <!-- <form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card pd-20 pd-sm-40 col-md-12">
          <!-- <h6 class="card-body-title">Sign Up Here</h6>
          <p class="mg-b-20 mg-sm-b-30">A form with a label on top of each form control.</p> -->
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            
            <div class="row">
              <!-- <dir class="col-md-1">
                
              </dir> -->
              <div class="col-md-12">
                
                  <form id="modifyVehicle" method="post" action="">
                    <div class="form-layout">
                      <div class="row mg-b-25">
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <div class="select">
                              <select name="user_vehicle_no" id="user_vehicle_no" style="opacity: 0.8" disabled>
                                <option value="<?php if(isset($user_vehicle_no)) echo $user_vehicle_no; else echo 'NULL'?>">
                                  <?php 
                                  if(isset($user_vehicle_no)) echo $user_vehicle_no; else echo 'Vehicle Number';
                                  ?>
                                </option>
                              </select>
                              </div>
                              <span style="position: absolute;" id="error_user_vehicle_no" class="error_size"></span>                          
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <div class="select">
                                <select name="parking_area_id" id="parking_area_id" value="null" style="opacity: 0.8" disabled>
                                <option value="<?php if(isset($parking_area_id)) echo $parking_area_id; else echo 'NULL'?>">
                                    <?php 
                                    if(isset($parking_area_id)) echo $parking_area_id; else echo 'Vehicle Number';
                                    ?>
                                  </option>
                              </select>
                            </div>
                              <span style="position: absolute;" id="error_parking_area_id" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="text" class="form-control" placeholder="Gate Pass Number" name="user_gate_pass_num" value="<?php if(isset($user_gate_pass_num)) echo $user_gate_pass_num;?>" id="user_gate_pass_num"   maxlength="10" style="background-color: transparent;" readonly="readonly">
                            <span style="position: absolute;" id="error_user_gate_pass_num" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <input type="text" name="effective_date" id="effective_date" placeholder="MM/DD/YYYY" class="form-control" value="<?php if(isset($effective_date)) echo $effective_date;?>" style="background-color: transparent;" disabled>
                            <span style="position: absolute;" id="error_effective_date" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <input type="text" name="end_date" id="end_date" placeholder="MM/DD/YYYY" class="form-control" value="<?php if(isset($end_date)) echo $end_date;?>">
                            <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <button type="button" class="btn btn-block btn-secondary" name="skip" id="skip" onclick="document.location.href='gate-pass';">Back</button>
                          </div>
                        </div>

                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <button type="button" class="btn btn-block btn-info" name="save" id="save">Save</button>
                            <span style="position: absolute;" id="error_all" class="error_size"></span>
                          </div>
                        </div>
                    </div><!-- form-layout -->
                  </form>
              </div>
              
            </div>
          </div>
        </div><!-- card -->
        <!-- /add employee form -->
        <!-- <span class="pd-20"></span> -->

      </div><!-- am-pagebody -->
      

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">

        /*validation*/
        $( document ).ready( function () {
          var userUrl = "<?php echo USER_URL; ?>";


          /*FORM SUBMIT*/
          $('#save').click(function(){

              //alert("save");
              //var user_veh_type = $('#user_veh_type').val();

              //'parking_area_id','user_vehicle_no','user_admin_id','effective_date','mobile'
              var user_gate_pass_dtl_id = "<?php echo $user_gate_pass_dtl_id ?>";
              var user_vehicle_no = $('#user_vehicle_no').val();
              var parking_area_id = $('#parking_area_id').val();
              var user_gate_pass_num = $('#user_gate_pass_num').val();
              var effective_date = $('#effective_date').val();
              var end_date = $('#end_date').val();
              var user_admin_id = "<?php echo $_SESSION['user_admin_id'] ?>";
              var mobile ="<?php echo $user_mobile ?>";
              //alert(user_vehicle_no+"--"+parking_area_id+"--"+user_gate_pass_num+"--"+effective_date+"--"+mobile+"--"+user_admin_id);
              if (user_vehicle_no!='' && parking_area_id!='' &&user_gate_pass_num!='' && effective_date!='' && end_date!='') {

                var urluserGatePassDtlUpdate = userUrl+'user_gate_pass_dtl_update.php';

                $.ajax({
                      url :urluserGatePassDtlUpdate,
                      type:'POST',
                      data :
                      {
                        'user_gate_pass_dtl_id':user_gate_pass_dtl_id,
                        'user_gate_pass_num':user_gate_pass_num,
                        'parking_area_id':parking_area_id,
                        'user_vehicle_no':user_vehicle_no,
                        'effective_date':effective_date,
                        'end_date':end_date,
                        'user_admin_id':user_admin_id,
                        'mobile':mobile
                      },
                      dataType:'html',
                      success  :function(data)
                      {
                        //alert(data);
                        //if (data = true) {
                          //window.location='gate_pass.php';
                        var json = $.parseJSON(data);

                        if (json.status) {
                          //window.location='signin.php';
                          $.alert({
                            icon: 'fa fa-smile-o',
                            theme: 'modern',
                            title: 'Success !',
                            content: "<p style='font-size:0.8em;'>Gate Pass Update Successfully</p>",
                            type: 'green',
                            buttons: {
                              Ok: function () {
                                  window.location='gate-pass';
                              }
                            }
                          });
                        }else{
                          //alert("problem p");
                          $.alert({
                            icon: 'fa fa-frown-o',
                            theme: 'modern',
                            title: 'Error !',
                            content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
                            type: 'red'
                          });
                        }
                      }
                  });
              
              }else{
                $('#error_all').text('ALL ARE REQUIRED');
              }
              
          });

           

      } );


      addEventListener('DOMContentLoaded', function () {

          var now = new Date;
          //alert(now);

          pickmeup('#end_date', {
                  default_date   : false,
                  position       : 'top',
                  hide_on_select : true,
                  render : function (date) {
                        if (date < now) {
                            return {
                              disabled : true,
                              class_name : 'date-in-past'
                            };
                        }
                        return {};
                    } 
                });
        });
        /* end velidation*/


        function gatePassUppercase() {
          var x = document.getElementById("user_gate_pass_num");
          x.value = x.value.toUpperCase();
        }

    </script>