<?php  
/*
Description: user licence image upload.
Developed by: Rakhal Raj Mandal
Created Date: 02-04-2018
Update date :02-04-2018
*/
// require_once '../../connection-pdo.php';
require_once 'api/reg_api.php';
// require_once 'api/global_api.php';
$response = array();
if(isAvailable(array('user_admin_id','mobile','user_ins_img','user_veh_dtl_id','token'))){
 	if(isEmpty(array('user_admin_id','mobile','user_ins_img','user_veh_dtl_id','token'))){
	 	$user_ins_img = trim($_POST['user_ins_img']);
	 	$user_veh_dtl_id = trim($_POST['user_veh_dtl_id']);
	 	$user_admin_id = trim($_POST['user_admin_id']);
	 	$mobile = trim($_POST['mobile']);
	 	$token = trim($_POST['token']);
	 	$resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
 			$respon= user_ins_img_upload($user_ins_img, $user_veh_dtl_id, $user_admin_id,$mobile);
        	echo ($respon);
        }else{
        	echo $resp;
        }
 	}else{
		$response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
 		echo json_encode($response);
    }
}else{
	$response['status'] = 0; 
	$response['message'] = 'Invalid API Call';
    echo json_encode($response); 
}
?>