<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','contractual_gate_pass_dtl_id'))){
    if(isEmpty(array('prk_admin_id','contractual_gate_pass_dtl_id'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $contractual_gate_pass_dtl_id = trim($_POST['contractual_gate_pass_dtl_id']);
        $response = contractual_gate_pass_show($prk_admin_id,$contractual_gate_pass_dtl_id);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
 }
echo $response;
?>