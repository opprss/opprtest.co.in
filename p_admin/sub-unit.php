<!-- ################################################
  
Description: Parking area can add/delete subunit.adding subunit is must for issueing digital gate pass.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
include "all_nav/header.php"; 
if (isset($_SESSION['prk_admin_id'])) {
    $insert_by=$_SESSION['parking_admin_name'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
    $user_name = $sessionDetails['parking_admin_name'];
    }
 ?>
<!-- vendor css -->
   
    <link href="../lib/highlightjs/github.css" rel="stylesheet">
    <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">



<style type="text/css">
  .size{
    font-size: 11px;
  }
</style>
 <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Subunit</h5>
        <!--<form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
    </div>
    <div class="am-mainpanel">
      <div class="am-pagebody">

        <div class="card pd-20 pd-sm-40" >
          <!-- <h6 class="card-body-title">Add Subunit</h6> -->
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form name="sub_unit" id="sub_unit" method="post" action="">
              <div class="row mg-b-25">
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label size">NAME <span class="tx-danger">*</span></label>
                    <input class="form-control error_msg" type="text" name="prk_sub_unit_name" placeholder="Subunit Name" id="prk_sub_unit_name" onkeydown="upperCase(this)">
                      <span style="position: absolute;" id="error_name" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label size">SHORT NAME <span class="tx-danger">*</span></label>
                    <input class="form-control error_msg" type="text" name="prk_sub_unit_short_name"  placeholder="Subunit Short Name" id="prk_sub_unit_short_name" onkeydown="upperCase(this)">
                      <span style="position: absolute;" id="error_short_name" class="error_size"></span>
                  </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label size">ADDRESS <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="prk_sub_unit_address"  placeholder="Address" id="prk_sub_unit_address" style="background-color: white" onkeydown="upperCase(this)">
                      <span style=" position: absolute;" id="error_address" class="error_size"></span>
                  </div>
                </div><!-- col-4-->
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label size">REPRESENTATIVE NAME <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="prk_sub_unit_rep_name"  placeholder="Reprentative Name" id="prk_sub_unit_rep_name" style="background-color: white" onkeydown="upperCase(this)">
                      <span style=" position: absolute;" id="error_rep_name" class="error_size"></span>
                  </div>
                </div>
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">REPRESENTATIVE MOBILE NUMBER <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="prk_sub_unit_rep_no"  placeholder="Representative MOBILE Number" id="prk_sub_unit_rep_no" style="background-color: white" maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                    <span style=" position: absolute;" id="error_rep_number" class="error_size"></span>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">REPRESENTATIVE EMAIL <span class="tx-danger">*</span></label>
                  <input class="form-control" type="email" name="prk_sub_unit_rep_email" placeholder="REPRESENTATIVE EMAIL" id="prk_sub_unit_rep_email" style="background-color: white">
                    <span style=" position: absolute;" id="error_rep_email" class="error_size"></span>
                </div>
              </div>

              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">EFFECTIVE DATE <span class="tx-danger">*</span></label>
                  <input class="form-control prevent_readonly" type="text" name="prk_sub_unit_eff_date" placeholder="Effective date" id="prk_sub_unit_eff_date" style="background-color: white" readonly="readonly">
                    <span style=" position: absolute;" id="error_eff_date" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">END DATE </label>
                  <input class="form-control prevent_readonly" type="text" name="end_date" placeholder="End date" id="end_date" style="background-color: white" readonly="readonly">
                    <span style=" position: absolute;" id="error_eff_date" class="error_size"></span>
                </div>
              </div>

              <div class="col-lg-4">
                <div class="form-layout-footer mg-t-30">
                  <div class="row">
                    <div class="col-lg-6">
                      <input type="button" value="ADD" class="btn btn-block btn-primary prk_button" id="submit">
                      <div style="position: absolute;" id="success_submit" class="success"></div>
                    </div>
                    <div class="col-lg-6">
                      <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
      </div>
    </div>


<!-- datatable -->

 
    <!-- <div class="am-mainpanel"> -->
      <div class="am-pagebody">

        <div class="card pd-20 pd-sm-40">
          <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="wd-15p">Name</th>
                  <th class="wd-15p">Short Name</th>
                  <th class="wd-15p">Rep Name</th>
                  <th class="wd-15p">Rep Number</th>
                  <th class="wd-15p">Rep Email</th>
                  <th class="wd-15p">Effective Date</th>
                  <th class="wd-15p">End Date</th>
                  <th class="wd-10p">Action</th>
                </tr>
              </thead>
              <tbody>
              <?php 
              $respon=prk_sub_unit_dtl_list($prk_admin_id);
              
              
           
              foreach($respon as $value){
                 ?>

               <tr>
                 <td><?php echo $value['prk_sub_unit_name']; ?></td>
                 <td><?php echo $value['prk_sub_unit_short_name']; ?></td>
                 <td><?php echo $value['prk_sub_unit_rep_name']; ?></td>
                 <td><?php echo $value['prk_sub_unit_rep_mob_no']; ?></td>
                 <td><?php echo $value['prk_sub_unit_rep_email']; ?></td>
                 <td><?php echo $value['prk_sub_unit_eff_date']; ?></td>
                 <td><?php echo $value['prk_sub_unit_end_date']; ?></td>
                 <td>
                    <a href="sub-unit-update?list_id=<?php echo base64_encode($value['prk_sub_unit_id']); ?>" data-toggle="tooltip" data-placement="top" title="Modify"><i class="fa fa-edit" style="font-size:18px"></i></a>

                  <button type="button" class="clean-button" name="delete" id="delete<%=count++%>" value="<?php echo $value['prk_sub_unit_id']; ?>" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
                 </td>
              </tr>
                <?php } ?>
         
                </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->

    
<!-- datatable -->
<?php include"all_nav/footer.php"; ?>

<style>
.error_size{
  font-size: 11px;
  color: red;

}
.success{
  font-size: 11px;
  color: green;
}
</style>



<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE_WEB ?>";

  /*delete popup*/  
  $('button[id^="delete"]').on('click', function() {
    var prk_admin_id = "<?php echo $prk_admin_id;?>";
    var user_name=' <?php echo $user_name; ?>';
    var prk_sub_unit_id = this.value;
    //alert(prk_sub_unit_id);
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the item permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            if (prk_sub_unit_id != '') {
              //alert(prk_rate_id);
              var urlDtl = prkUrl+'prk_sub_unit_delete.php';
              //alert(urlDtl);
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :
                {
                  'prk_sub_unit_id':prk_sub_unit_id,
                  'user_name':user_name,
                  'prk_admin_id':prk_admin_id,
                  'token': '<?php echo $token;?>'
                },
                dataType:'html',
                success  :function(data)
                {
                  var json = $.parseJSON(data);
                  if (json.status){
                    location.reload(true);
                  }else{
                    if( typeof json.session !== 'undefined'){
                      if (!json.session) {
                        window.location.replace("logout.php");
                      }
                    }else{
                      $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                    }
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  });
 /*/delete popup*/
  $(document).ready(function(){
  
    $('#reset').click(function(){
      $('.error_size').text('');
    });
    
    $('#submit').click(function(){
      //alert("click");
      var prk_sub_unit_name =$('#prk_sub_unit_name').val();
      var prk_sub_unit_short_name=$('#prk_sub_unit_short_name').val();
      var prk_sub_unit_address=$('#prk_sub_unit_address').val();
      var prk_sub_unit_rep_name=$('#prk_sub_unit_rep_name').val();
      var prk_sub_unit_rep_no=$('#prk_sub_unit_rep_no').val();
      var prk_sub_unit_rep_email=$('#prk_sub_unit_rep_email').val();
      var prk_sub_unit_eff_date = $('#prk_sub_unit_eff_date').val();
      var end_date = $('#end_date').val();
      end_date = (!end_date == '') ? end_date : FUTURE_DATE;

      var prk_admin_id = "<?php echo $prk_admin_id;?>";
      var user_name = "<?php echo $user_name;?>";


      if(prk_sub_unit_name!='' && prk_sub_unit_short_name!='' &&prk_sub_unit_address!='' && prk_sub_unit_rep_name!='' && prk_sub_unit_rep_no!='' && prk_sub_unit_rep_email!='' && prk_sub_unit_eff_date!=''){
        var urlPrkAreUser = prkUrl+'prk_sub_unit.php';
        $('#submit').val('Wait ...').prop('disabled', true);
        $.ajax({
          url :urlPrkAreUser,
          type:'POST',
          data :
          {
            'prk_sub_unit_name':prk_sub_unit_name,
            'prk_sub_unit_short_name':prk_sub_unit_short_name,
            'prk_sub_unit_address':prk_sub_unit_address,
            'prk_sub_unit_rep_name':prk_sub_unit_rep_name,
            'prk_sub_unit_rep_mob_no':prk_sub_unit_rep_no,
            'prk_sub_unit_eff_date':prk_sub_unit_eff_date,
            'prk_sub_unit_rep_email':prk_sub_unit_rep_email,
            'prk_admin_id':prk_admin_id,
            'user_name':user_name,
            'prk_sub_unit_end_date':end_date,
            'token': '<?php echo $token;?>'             
          },
          dataType:'html',
          success  :function(data)
          {
            $('#submit').val('ADD').prop('disabled', false);
            var json = $.parseJSON(data);
            if (json.status) {
              $.alert({
                  icon: 'fa fa-smile-o',
                  theme: 'modern',
                  title: 'Success !',
                  content: "<p style='font-size:0.9em;'>Subunit added successfully</p>",
                  type: 'green',
                  buttons: {
                    Ok: function () {
                        location.reload(true);
                    }
                  }
              });
            }else{
              if( typeof json.session !== 'undefined'){
                if (!json.session) {
                  window.location.replace("logout.php");
                }
              }else{
                $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                type: 'red'
              });
              }
            }
          }
        });     
      }else{
        if (prk_sub_unit_name == '') {
            $('#error_name').text('Required name');
            //return false;
        }if (prk_sub_unit_address == '') {
          $('#error_address').text('Required address');
          //return false;
        }if (prk_sub_unit_short_name == '') {
          $('#error_short_name').text('Required short name');
          //return false;
        }if (prk_sub_unit_rep_name == '') {
          $('#error_rep_name').text('Required representative name');
          //return false;
        }if (prk_sub_unit_rep_no == '') {
          $('#error_rep_number').text('Required representative number');
          //return false;
        }if (prk_sub_unit_rep_email == '') {
          $('#error_rep_email').text('Required representative email');
          //return false;
        }if (prk_sub_unit_eff_date == '') {
          $('#error_eff_date').text('Required effective date');
          //return false;
        }
        return false;
      }
    });

    $("#prk_sub_unit_name").blur(function () {
      var prk_sub_unit_name=$('#prk_sub_unit_name').val();
      if(prk_sub_unit_name==''){
        $('#error_name').text('Required name');
       // $('#prk_sub_unit_name').focus();
        return false;
      }else{
        $('#error_name').text('');
      }
    });

    $("#prk_sub_unit_short_name").blur(function () {
      var prk_sub_unit_short_name=$('#prk_sub_unit_short_name').val();
      if(prk_sub_unit_short_name==''){
        $('#error_short_name').text('Required short name');
       // $('#prk_sub_unit_short_name').focus();
       return false;
      }else{
        $('#error_short_name').text('');
      }
    });

    $("#prk_sub_unit_address").blur(function () {
      var prk_sub_unit_address=$('#prk_sub_unit_address').val();
      if(prk_sub_unit_address==''){
        $('#error_address').text('Required Address');
       // $('#prk_sub_unit_address').focus();
       return false;
      }else{
        $('#error_address').text('');
      }
    });

    $("#prk_sub_unit_rep_name").blur(function () {
      var prk_sub_unit_rep_name=$('#prk_sub_unit_rep_name').val();
      if(prk_sub_unit_rep_name==''){
        $('#error_rep_name').text('Required representative name');
        //$('#prk_sub_unit_rep_name').focus();
        return false;
      }else{
        $('#error_rep_name').text('');
      }
    });

    $("#prk_sub_unit_rep_no").blur(function () {
      var prk_sub_unit_rep_no=$('#prk_sub_unit_rep_no').val();
      if(prk_sub_unit_rep_no==''){
        $('#error_rep_number').text('Required representative number');
        //$('#prk_sub_unit_rep_no').focus();
        return false;
      }else if (!isMobile(prk_sub_unit_rep_no)){
        $('#error_rep_number').text('Mobile is not valid');
       // $('#prk_sub_unit_rep_no').focus();
      }else{
        $('#error_rep_number').text('');
      }
    });

    $("#prk_sub_unit_rep_email").blur(function () {
      var prk_sub_unit_rep_email=$('#prk_sub_unit_rep_email').val();
      if(prk_sub_unit_rep_email==''){
        $('#error_rep_email').text('Required representative email');
        $('#prk_sub_unit_rep_email').focus();
        return false;
      }else if(!isEmail(prk_sub_unit_rep_email)){
        $('#error_rep_email').text('Email is not valid');
        return false;

      }else{
        $('#error_rep_email').text('');
      }
    });

    $("#prk_sub_unit_eff_date").blur(function () {
      var prk_sub_unit_eff_date=$('#prk_sub_unit_eff_date').val();
      if(prk_sub_unit_eff_date==''){
        $('#error_eff_date').text('Required effective date');
        //$('#prk_sub_unit_eff_date').focus();
        return false;
      }else{
        $('#error_eff_date').text('');
      }
    });

    $('#end_date').focus(function () {
      var start = new Date;
      var end = $('#prk_sub_unit_eff_date').val();
      var d = new Date(end.split("-").reverse().join("-"));
      var dd=d.getDate();
      var mm=d.getMonth()+1;
      var yy=d.getFullYear();
      var hh = start.getHours();
      var min = start.getMinutes();
      var sec = start.getSeconds();
      var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
      var end = new Date(newdate);
      days = (end - start) / (1000 * 60 * 60 * 24)-1;
      days = Math.round(days);
      now_end = start.setDate(start.getDate() + days);
      pickmeup('#end_date', {
        default_date   : false,
        /*position       : 'top',*/
        hide_on_select : true,
        render : function (date) {
          if (date < now_end) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
          }
          return {};
        } 
      });
    });

    $('#prk_sub_unit_eff_date').focus(function(){

      $('#end_date').val('');
    });
  });

  /*alphabets only*/
  $("#prk_sub_unit_rep_name,#prk_sub_unit_name,#prk_sub_unit_short_name").keypress(function(event){
    var inputValue = event.charCode;
    // alert(inputValue);
    if(!(inputValue >= 60) && (inputValue != 0 & inputValue !=32)){
        event.preventDefault();
    }
  });


</script>
    <!-- datatable scripts -->
    <script src="../lib/highlightjs/highlight.pack.js"></script>
    <script src="../lib/datatables/jquery.dataTables.js"></script>
    <script src="../lib/datatables-responsive/dataTables.responsive.js"></script>

    <script>
      $(function(){
        'use strict';

        $('#datatable1').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search Here',
            sSearch: '',
            lengthMenu: '_MENU_ Page',
          }
        });

        $('#datatable2').DataTable({
          bLengthChange: false,
          searching: false,
          responsive: true
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

      });
    </script>
    <!-- for tool tipe -->
    <script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });

    addEventListener('DOMContentLoaded', function () {

    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    //alert(now);
    pickmeup('#prk_sub_unit_eff_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
            if (date < now) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
             $('#error_eff_date').text('');
            return {};
        } 
    });
  });
  function upperCase(a){
      setTimeout(function(){
          a.value = a.value.toUpperCase();
      }, 1);
  }
  function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }
  function isMobile(mobile){
    var regex = /^\d{10}$/;
    return regex.test(mobile);
  }
</script>
