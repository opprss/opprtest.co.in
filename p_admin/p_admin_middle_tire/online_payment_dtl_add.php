<?php 
/*
Description: user address show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('payment_company','payment_for','transaction_id_for','status','checksumhash','mid','orderid','bankname','txnamount','txndate','txnid','respcode','paymentmode','banktxnid','currency','gatewayname','respmsg','inserted_by'))){
 	if(isEmpty(array('payment_company','payment_for','inserted_by'))){
        $payment_company = trim($_POST['payment_company']);
        $payment_for = trim($_POST['payment_for']);
        $transaction_by = isset($_POST['transaction_by']) ? trim($_POST['transaction_by']) : 'U';
        $transaction_id_for = trim($_POST['transaction_id_for']);
        $status = trim($_POST['status']);
        $checksumhash = trim($_POST['checksumhash']);
        $mid = trim($_POST['mid']);
        $orderid = trim($_POST['orderid']);
        $bankname = trim($_POST['bankname']);
        $txnamount = trim($_POST['txnamount']);
        $txndate = trim($_POST['txndate']);
        $txnid = trim($_POST['txnid']);
        $respcode = trim($_POST['respcode']);
        $paymentmode = trim($_POST['paymentmode']);
        $banktxnid = trim($_POST['banktxnid']);
        $currency = trim($_POST['currency']);
        $gatewayname = trim($_POST['gatewayname']);
        $respmsg = trim($_POST['respmsg']);
        $inserted_by = trim($_POST['inserted_by']);

        $response = online_payment_dtl_add($payment_company,$payment_for,$transaction_by,$transaction_id_for,$status,$checksumhash,$mid,$orderid,$bankname,$txnamount,$txndate,$txnid,$respcode,$paymentmode,$banktxnid,$currency,$gatewayname,$respmsg,$inserted_by);
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response =  json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>