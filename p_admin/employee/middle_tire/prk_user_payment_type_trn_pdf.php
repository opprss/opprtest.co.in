<?php 
/*
Description: parking employ today payment type history
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :03-05-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
$resp = array();
if(isAvailable(array('prk_admin_id','payment_rec_emp_name','payment_type','start_date','end_date'))){
    if(isEmpty(array('prk_admin_id','payment_rec_emp_name','payment_type'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $payment_rec_emp_name = trim($_POST['payment_rec_emp_name']);
        $payment_type = trim($_POST['payment_type']);
        $start_date = trim($_POST['start_date']);
        $end_date = trim($_POST['end_date']);
       
            $prk_user= prk_user_payment_type_trn($prk_admin_id,$payment_rec_emp_name,$payment_type,$start_date,$end_date);
            //$response = $prk_user;
            $json = json_decode($prk_user);
            if($json->status=='1'){
                $response = user_pay_pdf_dwn($prk_user);
            }else{
                $response['status']=0;
                $response['message']='PDF Created Failed';
                $response = json_encode($response); 
            }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response); 
} 
echo ($response);
?>