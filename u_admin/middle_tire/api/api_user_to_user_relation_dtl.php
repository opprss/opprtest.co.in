<?php
/*
Description: vehicle in parking area the show in details.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-10-2019
*/
function user_to_user_relation_add($user_admin_id,$mobile,$user_apar_id,$user_relation_mobile,$user_relation_name,$willing_to_rent_out,$rent_argument_month,$elect_and_water_charge,$security_deposit_amount,$user_relation_agreement_amount,$user_relation_agreement_start_date,$user_relation_agreement_end_date,$user_relation_id){
    global $pdoconn;
    $format = "Y-m-d";
    if(!empty($user_relation_agreement_start_date)) { 
        if(date($format, strtotime($user_relation_agreement_start_date)) == date($user_relation_agreement_start_date)) {
            $user_relation_agreement_start_date= $user_relation_agreement_start_date;
        } else {
            $user_relation_agreement_start_date = date("Y-m-d", strtotime($user_relation_agreement_start_date));
        }
    }else{
        $user_relation_agreement_start_date = TIME_TRN;
    }
    if(!empty($user_relation_agreement_end_date)) { 
        if(date($format, strtotime($user_relation_agreement_end_date)) == date($user_relation_agreement_end_date)) {
            $user_relation_agreement_end_date= $user_relation_agreement_end_date;
        } else {
            $user_relation_agreement_end_date = date("Y-m-d", strtotime($user_relation_agreement_end_date));
        }
    }else{
        $user_relation_agreement_end_date = FUTURE_DATE;
    }
    // $id=uniqid();
    $sql = "INSERT INTO `user_to_user_relation`(`user_relation_id`, `user_admin_id`, `user_apar_id`, `user_relation_mobile`, `user_relation_name`, `willing_to_rent_out`, `rent_argument_month`, `elect_and_water_charge`, `user_relation_agreement_amount`, `user_relation_agreement_start_date`, `user_relation_agreement_end_date`, `security_deposit_amount`, `send_date`, `inserted_by`, `inserted_date`) VALUES ('$user_relation_id','$user_admin_id','$user_apar_id','$user_relation_mobile','$user_relation_name','$willing_to_rent_out','$rent_argument_month','$elect_and_water_charge','$user_relation_agreement_amount','$user_relation_agreement_start_date','$user_relation_agreement_end_date','$security_deposit_amount','".TIME."','$mobile','".TIME."')";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $apartment_show1 = user_apartment_show($user_admin_id,$user_apar_id);
        $apartment_show = json_decode($apartment_show1, true);
        if($apartment_show['status']){
          // $user_apar_id=$apartment_show['user_apar_id'];
          $user_apar_name=$apartment_show['user_apar_name'];
          $user_apar_address=$apartment_show['user_apar_address'];
            $sql="SELECT `user_admin`.`user_admin_id`,`token`.`device_token_id`,`token`.`device_type`,`token`.`token_value` FROM `user_admin`,`token` WHERE `user_mobile`='$user_relation_mobile' AND `token`.`active_flag`='".FLAG_Y."' AND `token`.`user_admin_id`=`user_admin`.`user_admin_id`";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $device_token_id =  $val['device_token_id'];
                $device_type =  $val['device_type'];
                if ($device_type=='ANDR') {
                    // $device_token_id='edrwl48HLoI:APA91bHeqhUyOOWBrnS525XLcH7rwDEkvIP53YdWrs7xN5F2gwoGXNcLM1BY9e7It11LdWJIfKUCc6N5REZeIVSU7WyIoOYftE8O-qJ_NHxfhV3UNaC9_HoH9lahTQK9zaCGeDtYjJHh';
                    $payload = array();
                    // $payload['area_name'] = $prk_area_name;
                    // $payload['name'] = $visit_name;
                    $payload['in_date'] = TIME;
                    $payload['url'] = 'APARTMENT ADD GROUP';
                    $title = 'OPPRSS';
                    $message = $user_apar_name.' ADDED FOR YOU.';
                    $push_type = 'individual';
                    $include_image='';
                    $clickaction='APARTMENT ADD GROUP';
                    $oth1='';
                    $oth2='';
                    $oth3='';
                    $paylo = push_send($title,$message,$push_type,$include_image,$device_token_id,$payload,$clickaction,$oth1,$oth2,$oth3);
                }
            }
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
    }
    return json_encode($response);
}
function user_to_user_relation_list($user_admin_id,$user_apar_id){
    global $pdoconn;
    $allplayerdata = array();
    $sql="SELECT `user_relation_id`, 
        `user_apar_id`, 
        `user_relation_mobile`, 
        `user_relation_name`, 
        `willing_to_rent_out`, 
        `rent_argument_month`, 
        `elect_and_water_charge`, 
        `user_relation_agreement_amount`, 
        `security_deposit_amount`, 
        `user_relation_user_admin_id`, 
        `user_relation_status`, 
        DATE_FORMAT(`send_date`,'%d-%b-%Y') AS `send_date`,
        DATE_FORMAT(`send_date`,'%I:%i %p') AS `send_time`,
        DATE_FORMAT(`approved_date`,'%d-%b-%Y') AS `approved_date`,
        DATE_FORMAT(`approved_date`,'%I:%i %p') AS `approved_time`,
        DATE_FORMAT(`user_relation_agreement_start_date`,'%d-%b-%Y') AS `agreement_start_date`,
        DATE_FORMAT(`user_relation_agreement_end_date`,'%d-%b-%Y') AS `agreement_end_date`
        FROM `user_to_user_relation` 
        WHERE `user_admin_id`='$user_admin_id'
        AND `user_apar_id`='$user_apar_id' 
        AND `active_flag`='".FLAG_Y."' 
        AND `del_flag`='".FLAG_N."' 
        ORDER BY `user_relation_unique_id` DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['user_relation_id'] =  $val['user_relation_id'];
            $park['user_relation_mobile'] =  $val['user_relation_mobile'];
            $park['user_apar_id'] =  $val['user_apar_id'];
            $park['user_relation_name'] =  $val['user_relation_name'];
            $park['willing_to_rent_out'] =  $val['willing_to_rent_out'];
            $park['rent_argument_month'] =  $val['rent_argument_month'];
            $park['elect_and_water_charge'] =  $val['elect_and_water_charge'];
            $park['user_relation_agreement_amount'] =  $val['user_relation_agreement_amount'];
            $park['security_deposit_amount'] =  $val['security_deposit_amount'];
            $park['user_relation_user_admin_id'] =  $val['user_relation_user_admin_id'];
            $park['user_relation_status'] =  $val['user_relation_status'];
            $park['send_date'] =  $val['send_date'];
            $park['send_time'] =  $val['send_time'];
            $park['approved_date'] =  $val['approved_date'];
            $park['approved_time'] =  $val['approved_time'];
            $park['agreement_start_date'] =  $val['agreement_start_date'];
            $park['agreement_end_date'] =  $val['agreement_end_date'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['relation_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
function user_to_user_relation_delete($user_admin_id,$mobile,$user_apar_id,$user_relation_id){
    global $pdoconn;
    $sql = "UPDATE `user_to_user_relation` SET `updated_by`='$mobile',`updated_date`='".TIME."',`del_flag`='".FLAG_Y."' WHERE `user_relation_id`='$user_relation_id' AND `user_admin_id`='$user_admin_id' AND `user_apar_id`='$user_apar_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Successful';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
    }
    return json_encode($response);
}
function to_user_relation_list($user_admin_id){
    global $pdoconn;
    $allplayerdata = array();
    $sql="SELECT to_user.`user_relation_id`,
        ual.`user_apar_name`,
        ual.`user_apar_address`,
        to_user.`user_relation_name`,
        to_user.`user_relation_mobile`, 
        to_user.`rent_argument_month`, 
        to_user.`user_relation_agreement_amount`,
        to_user.`security_deposit_amount`, 
        to_user.`user_relation_status`,
        ua.`user_admin_id`,
        udt.`user_name` as 'send_by_name',
        DATE_FORMAT(to_user.`send_date`,'%d-%b-%Y') AS `send_date`,
        DATE_FORMAT(to_user.`send_date`,'%I:%i %p') AS `send_time`,
        DATE_FORMAT(to_user.`approved_date`,'%d-%b-%Y') AS `approved_date`,
        DATE_FORMAT(to_user.`approved_date`,'%I:%i %p') AS `approved_time`,
        DATE_FORMAT(to_user.`user_relation_agreement_start_date`,'%d-%b-%Y') AS `agreement_start_date`,
        DATE_FORMAT(to_user.`user_relation_agreement_end_date`,'%d-%b-%Y') AS `agreement_end_date`
        FROM `user_to_user_relation` to_user JOIN `user_admin` ua JOIN `user_apartment_list` ual JOIN `user_detail` udt
        WHERE udt.`user_admin_id`=to_user.`user_admin_id`
        AND udt.`active_flag`='".FLAG_Y."'
        AND ual.`user_apar_id`=to_user.`user_apar_id`
        AND ual.`active_flag`='".FLAG_Y."'
        AND ual.`del_flag`='".FLAG_N."'
        AND ua.`user_mobile`=to_user.`user_relation_mobile` 
        AND ua.`user_ac_status`='".FLAG_A."' 
        AND to_user.`active_flag`='".FLAG_Y."'
        AND to_user.`del_flag`='".FLAG_N."'
        AND ua.`user_admin_id`='$user_admin_id'
        ORDER BY to_user.`send_date` DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['user_relation_id'] =  $val['user_relation_id'];
            $park['user_apar_name'] =  $val['user_apar_name'];
            $park['user_apar_address'] =  $val['user_apar_address'];
            $park['send_by_name'] =  $val['send_by_name'];
            $park['send_date'] =  $val['send_date'];
            $park['send_time'] =  $val['send_time'];
            $park['approved_date'] =  $val['approved_date'];
            $park['approved_time'] =  $val['approved_time'];
            $park['user_relation_name'] =  $val['user_relation_name'];
            $park['user_relation_mobile'] =  $val['user_relation_mobile'];
            $park['user_relation_agreement_amount'] =  $val['user_relation_agreement_amount'];
            $park['security_deposit_amount'] =  $val['security_deposit_amount'];
            $park['user_relation_status'] =  $val['user_relation_status'];
            $park['agreement_start_date'] =  $val['agreement_start_date'];
            $park['agreement_end_date'] =  $val['agreement_end_date'];
            $park['rent_argument_month'] =  $val['rent_argument_month'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['relation_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
function to_user_relation_approved($user_admin_id,$mobile,$user_relation_id,$user_relation_status){
    global $pdoconn;
    $sql = "UPDATE `user_to_user_relation` SET `updated_by`='$mobile',`updated_date`='".TIME."',`approved_date`='".TIME."',`user_relation_status`='$user_relation_status',`user_relation_user_admin_id`='$user_admin_id' WHERE `user_relation_id`='$user_relation_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Successful';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
    }
    return json_encode($response);
}
function user_to_user_relation_transaction($user_admin_id,$mobile,$user_relation_id,$trn_amount,$payment_type,$user_to_user_relation_trn_id,$online_payment_dtl_id,$orderid){
    global $pdoconn;
    // $id=uniqid();
    $sql = "INSERT INTO `user_to_user_relation_transaction`(`user_to_user_relation_trn_id`, `user_admin_id`, `user_relation_id`, `trn_amount`, `payment_type`, `transaction_date`, `online_payment_dtl_id`, `orderid`, `inserted_by`, `inserted_date`) VALUES ('$user_to_user_relation_trn_id','$user_admin_id','$user_relation_id','$trn_amount','$payment_type','".TIME."','$online_payment_dtl_id','$orderid','$mobile','".TIME."')";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['relation_trn_id'] = $user_to_user_relation_trn_id;
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
    }
    return json_encode($response);
}
function to_user_relation_transaction_list($user_admin_id,$user_relation_id){
    global $pdoconn;
    $allplayerdata = array();
    $sql = "SELECT uturt.`user_to_user_relation_trn_id`,
        uturt.`user_admin_id`,
        uturt.`user_relation_id`,
        uturt.`trn_amount`,
        uturt.`payment_type`, 
        DATE_FORMAT(uturt.`transaction_date`,'%d-%b-%Y') AS `transaction_date`,
        DATE_FORMAT(uturt.`transaction_date`,'%I:%i %p') AS `transaction_time`,
        utur.`user_apar_id`,
        ual.`user_apar_name`
        FROM `user_to_user_relation_transaction` uturt JOIN `user_to_user_relation` utur JOIN `user_apartment_list` ual
        WHERE ual.`user_apar_id`=utur.`user_apar_id`
        AND ual.`active_flag`='".FLAG_Y."' 
        AND ual.`del_flag`='".FLAG_N."'
        AND utur.`user_relation_id`=uturt.`user_relation_id`
        AND utur.`active_flag`='".FLAG_Y."' 
        AND utur.`del_flag`='".FLAG_N."'
        AND uturt.`active_flag`='".FLAG_Y."' 
        AND uturt.`del_flag`='".FLAG_N."'  
        AND uturt.`user_admin_id`='$user_admin_id'
        AND uturt.`user_relation_id`='$user_relation_id' 
        ORDER BY uturt.`transaction_date` DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['user_to_user_relation_trn_id'] =  $val['user_to_user_relation_trn_id'];
            $park['trn_amount'] =  $val['trn_amount'];
            $park['payment_type'] =  $val['payment_type'];
            $park['transaction_date'] =  $val['transaction_date'];
            $park['transaction_time'] =  $val['transaction_time'];
            $park['user_apar_name'] =  $val['user_apar_name'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['transaction_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
function user_to_user_relation_transaction_list($user_admin_id,$user_relation_id){
    global $pdoconn;
    $allplayerdata = array();
    $sql = "SELECT uturt.`user_to_user_relation_trn_id`,
        uturt.`user_relation_id`,
        uturt.`trn_amount`,
        uturt.`payment_type`, 
        DATE_FORMAT(uturt.`transaction_date`,'%d-%b-%Y') AS `transaction_date`,
        DATE_FORMAT(uturt.`transaction_date`,'%I:%i %p') AS `transaction_time`,
        utur.`user_apar_id`,
        utur.`user_relation_name`,
        ual.`user_apar_name`
        FROM `user_to_user_relation_transaction` uturt JOIN `user_to_user_relation` utur JOIN `user_apartment_list` ual
        WHERE ual.`user_apar_id`=utur.`user_apar_id`
        AND ual.`active_flag`='".FLAG_Y."' 
        AND ual.`del_flag`='".FLAG_N."'
        AND utur.`user_relation_id`=uturt.`user_relation_id`
        AND utur.`active_flag`='".FLAG_Y."' 
        AND utur.`del_flag`='".FLAG_N."'
        AND uturt.`active_flag`='".FLAG_Y."' 
        AND uturt.`del_flag`='".FLAG_N."'  
        AND ual.`user_admin_id`='$user_admin_id'
        AND uturt.`user_relation_id`='$user_relation_id' 
        ORDER BY uturt.`transaction_date` DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['user_to_user_relation_trn_id'] =  $val['user_to_user_relation_trn_id'];
            $park['trn_amount'] =  $val['trn_amount'];
            $park['payment_type'] =  $val['payment_type'];
            $park['transaction_date'] =  $val['transaction_date'];
            $park['transaction_time'] =  $val['transaction_time'];
            $park['user_apar_name'] =  $val['user_apar_name'];
            $park['user_relation_name'] =  $val['user_relation_name'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['transaction_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>