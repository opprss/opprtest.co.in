<?php 
/*
Description: Parking employ gate login.
Developed by: Rakhal Raj Mandal
Created Date: 30-02-2018
Update date : 29-04-2018
*/ 
function prk_user_gate_login($prk_admin_id,$prk_user_username,$prk_area_gate_id,$prk_area_gate_name,$prk_area_gate_type,$prk_area_user_id){
    global $pdoconn;
    $response = array();
    $sql ="UPDATE `prk_user_gate_login_dtl` SET
        `prk_user_gate_login_dtl`.`end_date` = '".TIME."',
        `prk_user_gate_login_dtl`.`updated_by` = '$prk_user_username',
        `prk_user_gate_login_dtl`.`updated_date` = '".TIME."'
        WHERE `prk_user_gate_login_dtl`.`end_date` is null 
        and `prk_user_gate_login_dtl`.`prk_admin_id` = '$prk_admin_id'  
        and `prk_user_gate_login_dtl`.`prk_user_username` = '$prk_user_username'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();

    $sql = "INSERT INTO `prk_user_gate_login_dtl`(`prk_admin_id`,`prk_area_user_id`,`prk_user_username`,`prk_area_gate_id`,`prk_area_gate_name`,`prk_area_gate_type`,`inserted_by`,`start_date`,`inserted_date`) VALUE ('$prk_admin_id','$prk_area_user_id','$prk_user_username','$prk_area_gate_id','$prk_area_gate_name','$prk_area_gate_type','$prk_user_username','".TIME."','".TIME."')";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){        
        $id=$pdoconn->lastInsertId();
        $sql ="SELECT `prk_qr_code` FROM `prk_area_gate_dtl` WHERE `active_flag`= '".FLAG_Y."' AND `del_flag`= '".FLAG_N."' AND `prk_area_gate_id`= '$prk_area_gate_id'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $val = $query->fetch();
        $response['status'] = 1;
        $response['session'] = 1;
        $response['prk_user_gate_login_id'] = $id;
        $response['prk_qr_code'] = $val['prk_qr_code'];
        $response['message'] = 'Sucessful';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Data Not Save';
        return json_encode($response);
    }  
}
?>