<!-- ################################################
Description: List of user's vechile | user can delete vehicle from this page.
Developed by: Soemen Banerjee
Created Date: 17-03-2018
 ####################################################-->
<?php  
  include "all_nav/header.php";
  $user_relation_id = isset($_REQUEST['list_id']) ? base64_decode(trim($_REQUEST['list_id'])) : '';
  $user_apar_name = isset($_REQUEST['list_apar_name']) ? base64_decode(trim($_REQUEST['list_apar_name'])) : '';
  $send_by_name = isset($_REQUEST['list_sender_name']) ? base64_decode(trim($_REQUEST['list_sender_name'])) : '';
  $user_relation_agreement_amount = isset($_REQUEST['list_agreement_amount']) ? base64_decode(trim($_REQUEST['list_agreement_amount'])) : '';
  // $user_apar_id = isset($_REQUEST['list_id']) ? base64_decode(trim($_REQUEST['list_id'])) : '';
  $relation_transaction_list=to_user_relation_transaction_list($user_admin_id,$user_relation_id);
  $tran_list = json_decode($relation_transaction_list, true);
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
  .error{
    font-size: 11px;
    color: red;
  }
  th,td{
    text-align: center;
  }
</style>
<script src="../lib/validation/jquery.validate.js"></script>
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
  <div class="am-mainpanel">
    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Property Transaction <?//=$user_relation_agreement_amount?></h5>
    </div>
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40" >
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <form role="form" id="relaTran" action="pgRedirect_rela_tran" method="post" enctype="multipart/form-data">
          <input type="hidden" name="nana" id="nana">
          <input type="hidden" name="user_admin_id" id="user_admin_id">
          <input type="hidden" name="mobile" id="mobile">
          <input type="hidden" name="token" id="token">
          <input type="hidden" name="user_relation_id" id="user_relation_id">
          <input type="hidden" name="payment_type" id="payment_type">
          <input type="hidden" name="callback_url" id="callback_url">
          <div class="row mg-b-5">
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Send by Name<span class="tx-danger"></span></label>
                <input type="text" name="send_by_name" id="send_by_name" class="form-control" placeholder="Name" readonly="">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Property Name<span class="tx-danger"></span></label>
                <input type="text" name="apartment_name" id="apartment_name" class="form-control" placeholder="Property Name" readonly="">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Amount<span class="tx-danger">*</span></label>
                <input type="text" name="trn_amount" id="trn_amount" class="form-control" placeholder="Amount" readonly="">
              </div>
            </div>
            <div class="col-lg-5">
              <div class="row mg-t-35">
                <div class="col-lg-4">
                  <div class="form-group">
                    <input type="button" value="Pay" class="btn btn-block btn-primary prk_button" name="rela_tran" id="rela_tran">
                  </div>
                </div>
                <!-- <div class="col-lg-4">
                  <div class="form-group">
                    <button type="reset" class="btn btn-block prk_button btn-primary">Reset</button>
                  </div>
                </div> -->
                <div class="col-lg-4">
                  <div class="form-group">
                    <button type="button" class="btn btn-block prk_button btn-primary" onclick="document.location.href='relation-list';">Back</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40">
      <div class="table-wrapper">
        <table id="datatable1" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="wd-5p">Sl No.</th>
              <th class="wd-20p">Property Name</th>
              <th class="wd-10p">Amount</th>
              <th class="wd-10p">Payment Type</th>
              <th class="wd-10p">Transaction Date</th>
              <th class="wd-10p">Transaction Time</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              $x=1;
              if ($tran_list['status']){
              foreach($tran_list['transaction_list'] as $value){
            ?>
            <tr>
              <td><?=$x; ?></td>
              <td><?=$value['user_apar_name']; ?></td>
              <td><?=$value['trn_amount']; ?></td>
              <td><?=$value['payment_type']; ?></td>
              <td><?=$value['transaction_date']; ?></td>
              <td><?=$value['transaction_time']; ?></td>
            </tr>
            <?php $x++; }} ?>
          </tbody>
        </table>
      </div><!-- table-wrapper -->
    </div>
  </div>
  <?php include"all_nav/footer.php"; ?>
<script type="text/javascript">
  $('#send_by_name').val("<?=$send_by_name?>");
  $('#apartment_name').val("<?=$user_apar_name?>");
  $('#trn_amount').val("<?=$user_relation_agreement_amount?>");
  $('#user_admin_id').val("<?=$user_admin_id?>");
  $('#token').val("<?=$token?>");
  $('#mobile').val("<?=$user_mobile?>");
  $('#user_relation_id').val("<?=$user_relation_id?>");

  var userBaseUrl = "<?php echo USER_BASE_URL; ?>";
  var callback_url=userBaseUrl+'pgResponse_rela_tran'
  $('#callback_url').val(callback_url);
  // alert(callback_url);

  // var user_admin_id = "<?php //echo $user_admin_id; ?>";
  // var token = "<?php //echo $token; ?>";
  // var mobile = "<?php //echo $user_mobile; ?>";
  // var user_relation_id = "<?php //echo $user_relation_id; ?>";
  $( document ).ready( function () {  
    $('#rela_tran').click(function () {
      if($("#relaTran").valid()){
        $('#relaTran').submit();
        /*$(this).val('Wait ...').prop('disabled', true);
        var trn_amount=$('#agreement_amount').val();
        // var payment_type=$('#payment_type').val();
        var payment_type='CASH';
        var user_relation = userUrl+'user_to_user_relation_transaction.php';
        //'user_admin_id','mobile','token','user_relation_id','trn_amount','payment_type'
        $.ajax({
          url :user_relation,
          type:'POST',
          data :{
            'user_admin_id':user_admin_id,
            'mobile':mobile,
            'token':token,
            'user_relation_id':user_relation_id,
            'trn_amount':trn_amount,
            'payment_type':payment_type
          },
          dataType:'html',
          success  :function(data){
            // alert(data);
            $('#add_group').val('Pay').prop('disabled', false);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.9em;'>Successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                    location.reload();
                  }
                }
              });
            }else{
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
                type: 'red'
              });
              if (json.session==0) {
                window.location.replace("logout.php");
              }
            }
          }
        });*/
      }
    });
    $( "#relaTran" ).validate( {
      rules: {
        trn_amount: {
          required: true,
          number: true
        }
      },
      messages: {
        trn_amount: {
          required: 'This field is required.',
          number: 'Enter valid Amount.'
        }
      }
    });
  });
</script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>
