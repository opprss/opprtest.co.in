<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function park_lot_no_list_combo($prk_admin_id,$tower_id,$flat_id){
    global $pdoconn;
    $prk_lot_number='';
     $sql ="SELECT pld.`prk_lot_dtl_id`,
        pld.`prk_lot_number`
        FROM `flat_master` fm,`prk_lot_dtl` pld
        WHERE pld.`flat_master_id`=fm.`flat_master_id`
        AND pld.`prk_admin_id`=fm.`prk_admin_id`
        AND pld.`active_flag`='".FLAG_Y."' 
        AND pld.`del_flag`='".FLAG_N."' 
        AND fm.`active_flag`='".FLAG_Y."' 
        AND fm.`del_flag`='".FLAG_N."' 
        AND fm.`prk_admin_id`='$prk_admin_id'
        AND fm.`flat_id`='$flat_id' 
        AND fm.`tower_id`='$tower_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $prk_lot_number.=$val['prk_lot_number'].',';
        }
    }
    return strtoupper(substr($prk_lot_number, 0, -1));
}
?>