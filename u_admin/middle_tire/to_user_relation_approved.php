<?php 
/*  Description:  Apartment Add.
    Developed by: Rakhal Raj Mandal
    Created Date: 06-10-2019
    Update date : 06-10-2019
*/ 
include_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_admin_id','mobile','token','user_relation_id','user_relation_status'))){
    if(isEmpty(array('user_admin_id','mobile','token','user_relation_id','user_relation_status'))){
        $user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
        $token = trim($_POST['token']);
        $user_relation_id = trim($_POST['user_relation_id']);
        $user_relation_status = trim($_POST['user_relation_status']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            // api_user_to_user_relation_dtl
            $response = to_user_relation_approved($user_admin_id,$mobile,$user_relation_id,$user_relation_status);
        }else{
            #..
            $response= $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response= json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response= json_encode($response);
}
echo ($response);
?>