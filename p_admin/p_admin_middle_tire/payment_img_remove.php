<?php
/*
Description: parking wallet payment QR code img show
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/  
require_once 'api/parkAreaReg_api.php';
$prk_admin_id = $_POST['prk_admin_id'];
$token = $_POST['token'];

$resp=prk_token_check($prk_admin_id,$token);
$json = json_decode($resp);
if($json->status){
    if (paaymentImageRemove($prk_admin_id)) {
	$response['status'] = 1;
	$response['msg'] = 'sucessfull';
	}else{
		$response['status'] = 0;
		$response['msg'] = 'Unsucessfull';
	}
}else{
    $response['session'] = 0;
	$response['msg'] = 'session expired';
}
echo json_encode($response);
?>