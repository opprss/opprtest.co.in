<?php
/*
Description: Parking adimn username and  password check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
    // require_once 'api/global_api.php';
    require_once 'api/parkAreaReg_api.php';
    $response = array();
 
    if(isAvailable(array('prk_admin_name','password'))){

        if(isEmpty(array('prk_admin_name','password'))){
            $prk_admin_name = trim($_POST['prk_admin_name']);
            $prk_admin_password = trim($_POST['password']);
            $response = prk_admin_old_pass_check($prk_admin_name,$prk_admin_password);
            echo $response;
        }else{
            $response['status']= 0;
            $response['message']="empty value";
            echo json_encode($response);
        }
    }else{
    	$response['status']= 0;
        $response['message']="invalid api call";
        echo json_encode($response);
     }
?>