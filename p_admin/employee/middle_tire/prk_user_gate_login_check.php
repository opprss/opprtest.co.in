<?php 
/*
Description: parking area visit the people all list. 
Developed by: Rakhal Raj Mandal
Created Date: 18-06-2018
Update date : -------
*/
include_once 'api/parkAreaReg_api.php';
// include_once 'api/global_api.php';
$response = array();
$resp = array();
if(isAvailable(array('prk_user_gate_login_id'))){
 	if(isEmpty(array('prk_user_gate_login_id'))){ 

	 	$prk_user_gate_login_id = trim($_POST['prk_user_gate_login_id']);       
        $response = prk_user_gate_login_check($prk_user_gate_login_id);

 	}else{
            $response['status'] = 0;
	    	$response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>