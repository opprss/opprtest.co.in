<?php 
/*
Description: parkin area gate list show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function vehicle_number_search($prk_admin_id,$veh_number){
    global $pdoconn;
    $allplayerdata = array();
    $sql = "SELECT pgpd.veh_type,
		vt.vehicle_type_dec, 
		vt.vehicle_typ_img, 
		pgpd.veh_number
		FROM prk_gate_pass_dtl pgpd,vehicle_type vt
		WHERE pgpd.veh_type=vt.vehicle_sort_nm
		AND vt.active_flag='".FLAG_Y."'
		AND pgpd.prk_admin_id='$prk_admin_id'
		AND pgpd.active_flag='".FLAG_Y."'
		AND pgpd.del_flag='".FLAG_N."'
		AND pgpd.veh_number LIKE '%".$veh_number."%'
		LIMIT 10";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
	    $arr_catagory = $query->fetchAll();
	    foreach($arr_catagory as $val){
	        $park['veh_type'] = $val['veh_type'];
	        $park['vehicle_type_dec'] = $val['vehicle_type_dec'];
	        $park['vehicle_typ_img'] = $val['vehicle_typ_img'];
	        $park['veh_number'] = $val['veh_number'];
	        array_push($allplayerdata, $park);
	    }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['vehicle_list'] = $allplayerdata;
	}else{
		$response['status'] = 0;
	    $response['message'] = 'List Empty';
	}    
    return json_encode($response);
}
?>