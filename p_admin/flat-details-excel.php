<!-- ################################################
Description: Tower detalis.
Developed by: Rakhal Raj 
Created Date: 12-03-2018
 ####################################################-->
<?php include "all_nav/header.php";
  $tower_name='';
  $flat_name='';
  $living_status='';
  $effective_date='';
  $end_date='';
  $tower_id='';
  $flat_id='';
  $crud_type='LS';
  // $flat_details=flat_details($prk_admin_id,$tower_name,$effective_date,$end_date,$parking_admin_name,$tower_id,$crud_type);
  $flat_details=flat_details($prk_admin_id,$tower_name,$flat_name,$effective_date,$end_date,$parking_admin_name,$flat_id,$crud_type,'','','');
  $flat_details_list = json_decode($flat_details, true);

  $tower_lis = tower_list($prk_admin_id);
  $tower_list = json_decode($tower_lis, true);
?>

<!-- header position -->
  <style type="text/css">
    .size{
      font-size: 11px;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .error{
      font-size: 11px;
      color: red;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .success{
      font-size: 11px;
      color: green;
    }
  </style>
  <!-- links for datatable -->
  <link href="../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
  
    <div class="am-mainpanel">

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Flat Info<//?=$flat_details?></h5>
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40" >
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form id="park_flat">
              <div class="row mg-b-5">
                <div class="col-lg-5">
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label size">TOWER NAME <span class="tx-danger">*</span></label>
                        <div class="select">
                          <select name="tower_name" id="tower_name" style="opacity: 0.8;font-size:14px">
                            <option value="">Select Tower</option>
                            <?php if($tower_list['status']){
                              foreach($tower_list['tower_list'] as $value){?>
                              <option value="<?=$value['tower_id'];?>"><?=$value['tower_name'];?></option>
                            <?php }}?>
                          </select>
                        </div>
                      <span style="position: absolute; display: none;" id="error_tower_name" class="error">This field is required.</span>
                      </div>
                    </div>
                    <div class="col-lg-8">
                      <div class="form-group mg-t-20">
                        <!-- <label class="form-control-label size">FLAT NAME <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control" placeholder="Flat Name" name="flat_name" id="flat_name"> -->
                        <input type="file" class="form-control"  id="file_path" name="file_path">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-7">
                  <div class="row">
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label class="form-control-label size">EFFECTIVE DATE<span class="tx-danger">*</span></label>
                        <input type="text" name="effective_date" id="effective_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                        <span style="position: absolute; display: none;" id="error_effective_date" class="error">This field is required.</span>
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label class="form-control-label size">END DATE</label>
                        <input type="text" name="end_date" id="end_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                       <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-layout-footer mg-t-30">
                        <div class="row">
                          <div class="col-lg-6">
                            <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
                          </div>
                          <div class="col-lg-6">
                            <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- data table -->
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40">
          <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th >Sl No.</th>
                  <th >Tower Name</th>
                  <th >Flat Name</th>
                  <th >Flat Size</th>
                  <th >Effective Date</th>
                  <th >End Date</th>
                  <th >Action</th>
                </tr>
              </thead>
                <?php 
                if($flat_details_list['status']){$c=0;
                  foreach($flat_details_list['flat_list'] as $value){$c+=1;?>
                    <tr>
                    <td ><?=$c;?></td>
                    <td ><?=$value['tower_name'];?></td>
                    <td ><?=$value['flat_name'];?></td>
                    <td ><?=$value['flat_size'];?></td>
                    <td ><?=$value['eff_date'];?></td>
                    <td ><?=$value['end_date'];?></td>
                    <td >
                      <a href="<?php echo 'flat?list_id='.base64_encode($value['flat_id']);?>" class="pd-l-10" data-toggle="tooltip" data-placement="top" title="Modify"><i class="fa fa-pencil" style="font-size:18px; font-size:18px;"></i></a>
                      <!-- <button type="button" class="clean-button" name="delete" id="<?php echo $value['flat_id'];?>" data-toggle="tooltip" onclick="flat_delete(this.id)" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button> -->
                    </td>
                    </tr>
                  <?php }}?>
            </table>
          </div>
        </div>
      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
  <!-- footer part -->
<script type="text/javascript">
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#effective_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
        if (date < now) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        return {};
      } 
    });
  });
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#effective_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      hide_on_select : true,
      render : function (date) {
        if (date < now_end) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
        }
        return {};
      } 
    });
  });
  $('#effective_date').focus(function(){

    $('#end_date').val('');
  });
</script>
<!-- scripts for datatable -->
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  // alert(prkUrl);
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
</script>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE ?>";
  var prk_admin_id="<?=$prk_admin_id?>"
  var parking_admin_name="<?=$parking_admin_name?>"
  var token="<?=$token?>"
  var urlPrkAreGateDtl = prkUrl+'flat_details_excel.php';
  // alert(token);
  // $( document ).ready( function () {
  $('#save').click(function(){
    var effective_date=($("#effective_date").val());
    var end_date=($("#end_date").val());

    var str_array = end_date.split('-');
    var dd = str_array[0];
    var mm = str_array[1];
    var yy = str_array[2];
    var end_dat = yy+'-'+mm+'-'+dd;
    end_date = (!end_date == '') ? end_dat : FUTURE_DATE;
    var tower_name=($("#tower_name").val());
    var flat_name='';
    var flat_size='';
    // var two_wh_spa=($("#two_wh_spa").val());
    // var four_wh_spa=($("#four_wh_spa").val());
    var two_wh_spa='';
    var four_wh_spa='';
    // alert(end_date);
    var str_array = effective_date.split('-');
    var dd = str_array[0];
    var mm = str_array[1];
    var yy = str_array[2];
    var effective_dat = yy+'-'+mm+'-'+dd;
    var file_data = $('#file_path').prop('files')[0];
    if($("#park_flat").valid()&effective_date!=''){
      var form_data = new FormData();
      form_data.append('prk_admin_id', prk_admin_id);
      form_data.append('tower_name', tower_name);
      form_data.append('flat_name', flat_name);
      form_data.append('effective_date', effective_dat);
      form_data.append('end_date', end_date);
      form_data.append('flat_size', flat_size);
      form_data.append('two_wh_spa', two_wh_spa);
      form_data.append('four_wh_spa', four_wh_spa);
      form_data.append('parking_admin_name', parking_admin_name);
      form_data.append('flat_id', '');
      form_data.append('crud_type', 'I');
      form_data.append('token', token);
      form_data.append('file', file_data);
      // alert('ok');
       $('#save').val('Wait ...').prop('disabled', true);
      $.ajax({
        url :urlPrkAreGateDtl,
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,   
        type:'POST',
        dataType:'html',
        success  :function(data){
          $('#save').val('SAVE').prop('disabled', false);
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>Flat Details Added Successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                    location.reload(true);
                }
              }
            });
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>Something went wrong</p>",
              type: 'red'
            });
            }
          }
        }
      });
    }
  });
  function flat_delete(flat_id){
    // alert(tower_id);
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the Flat permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            /*if (flat_id != '') {
              $.ajax({
                url :urlPrkAreGateDtl,
                type:'POST',
                data :{
                  'prk_admin_id':prk_admin_id,
                  'tower_name':'',
                  'flat_name':'',
                  'effective_date':'',
                  'end_date':'',
                  'flat_size':'',
                  'two_wh_spa':'',
                  'four_wh_spa':'',
                  'parking_admin_name':parking_admin_name,
                  'flat_id':flat_id,
                  'crud_type':'D',
                  'token': token
                },
                dataType:'html',
                success  :function(data){
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Flat Delete Successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                          location.reload(true);
                        }
                      }
                    });
                  }else{
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.7em;'>Something went wrong 1</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{*/
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.8em;'>Delete Not Allow this Page Allow</p>",
                type: 'red'
              });
            // }
          }
        }
      }
    });
  }
</script>
<script type="text/javascript">
  $( document ).ready( function () {
    $( "#park_flat" ).validate( {
      rules: {
        file_path: "required",
        // flat_size: "required",
        // two_wh_spa: "required",
        // four_wh_spa: "required",
        effective_date: "valueNotEqualsEff",
        tower_name: "valueNotEqualstower"
      },
      messages: {
      }
    });
    $("#effective_date").blur(function () {
      var effective_date = $('#effective_date').val();
      if(effective_date == ''){
        $('#error_effective_date').show();
        return false;
      }else{
        $('#error_effective_date').hide();
        return true;
      }
    });
    $("#tower_name").on('change', function(){
      var v_type = $('#tower_name').val();
      if(v_type == "")
      {
        $('#error_tower_name').show();
        return false;
      }else{
          $('#error_tower_name').hide();
          return true;
      }
    });
  });
  $.validator.addMethod("valueNotEqualsEff", function(value){
    // alert('ok');
    arg = "";
    if (value != "") {
      return arg !== value;
    }else{
      $("#error_effective_date").show();
      return true;
    }
  });
  $.validator.addMethod("valueNotEqualstower", function(value){
    // alert(value);
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_tower_name").show();
      return false;
    }
  });
  $.validator.addMethod("valueNotEqualsliving", function(value){
    // alert(value);
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_living_status").show();
      return false;
    }
  });
  /*jQuery.validator.addMethod('valueNotEqualstower', function (value) {
        return (value != '0');
  },"Please select an option");*/
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>