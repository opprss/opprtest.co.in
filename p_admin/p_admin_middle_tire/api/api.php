<?php
session_start();

/*SET OR NOT*/
function isAvailable($params){
 
	foreach($params as $param){
		if(!isset($_POST[$param])){
			return false; 
		}
	}
	return true; 
}

/*EMPTY OR NOT*/
function isEmpty($params){
	foreach ($params as $param) {
		if(empty($_POST[$param])){
			return false; 
		}
	}
	return true; 
}

/*FOR LOGIN*/
function login($mobile, $password){

	require '../connection/connection-pdo.php';
 	$response = array();

    $sql ="SELECT `user_admin_id`, `user_ac_status` FROM `user_admin` WHERE `user_mobile`=:user_mobile AND `user_password`=:user_password";
    $query  = $pdoconn->prepare($sql);
   	$query->execute(array('user_mobile'=>$mobile,'user_password'=>md5($password)));
    $count=$query->rowCount();
    
    if($count == 1){

    	$row = $query->fetch();
     	$user_id =  $row['user_admin_id'];
        $user_ac_status = $row['user_ac_status'];

        /*var_dump($user_ac_status);*/

        switch ($user_ac_status) {
            case "A":
                /*$response['message'] = 'login sucessfull';*/
                $token = sha1(uniqid());
                $active_flag_first = FLAG_Y;
                $active_flag_second = FLAG_N;
                

                if(tokenCheck($user_id)){

                    $sql = "INSERT INTO `token` (`user_id`, `token_value`, `active_flag`) VALUES ('$user_id', '$token', '$active_flag_first')";
                    $query = $pdoconn->prepare($sql);
                    $query->execute();  

                    if ($query){
                        $response['status'] = 1;
                        $response['mobile'] = $mobile;
                        $response['token'] = $token;
                        $response['message'] = 'login sucessfull';
                    }else{
                        $response['status'] = 0;
                        $response['message'] = 'token not found';
                    }

                }
                break;
            case "T":
                $response['status'] = 0;
                $response['message'] = 'temp locked';
                break;
            case "L":
                $response['status'] = 0;
                $response['message'] = 'locked';
                break;
            case "P":
                $response['status'] = 0;
                $response['message'] = 'payment Poblem';
                break;
            case "D":
                $response['status'] = 0;
                $response['message'] = 'delete';
                break;
            default:
                $response['status'] = 0;
                $response['message'] = "other issue";
        }
			
		return json_encode($response);
            
    }elseif ($count > 1) {
        $response['status'] = 0;
        $response['message'] = 'You have some problem, Please contact to us';
        return json_encode($response);
    }
    else{

        $response['status'] = 0;
        $response['message'] = 'Username or password is wrong';
        return json_encode($response);
	}
}


/*checking if youer is loged in or not*/
function tokenCheck($user_id){

	require '../connection/connection-pdo.php';

	$active_flag_first = FLAG_Y;
	$active_flag_second = FLAG_N;
	$sql = "SELECT `user_id` FROM `token` WHERE `user_id`='$user_id' AND `active_flag`='$active_flag_first'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count == 1){
    	//UPDATE `otp_details` SET `active_flag` = '$active_flag_second' WHERE `otp_id` = '$otp_id'"
    	$sql = "UPDATE `token` SET `active_flag` = '$active_flag_second' WHERE `user_id` = '$user_id' AND `active_flag` = '$active_flag_first'";
    	$query  = $pdoconn->prepare($sql);
    	$query->execute();
    }
    return true;
}


/*user status check*/
function user_status_check($mobile){

    require '../connection/connection-pdo.php';

    $response = array();

    $sql ="SELECT  `user_ac_status` FROM `user_admin` WHERE `user_mobile`=:user_mobile";
    $query  = $pdoconn->prepare($sql);
    $query->execute(array('user_mobile'=>$mobile));
    $count=$query->rowCount();

    if($count == 1){

        $row = $query->fetch();
        /*$user_id =  $row['user_admin_id'];*/
        $user_ac_status = $row['user_ac_status'];

        switch ($user_ac_status) {
            case "A":
                $response['status'] = 0;
                $response['message'] = 'active';
                break;
            case "T":
                $response['status'] = 0;
                $response['message'] = 'temp locked';
                break;
            case "L":
                $response['status'] = 0;
                $response['message'] = 'locked';
                break;
            case "P":
                $response['status'] = 0;
                $response['message'] = 'payment Poblem';
                break;
            case "D":
                $response['status'] = 0;
                $response['message'] = 'delete';
                break;
            default:
                $response['status'] = 0;
                $response['message'] = "other issue";
        }
        return json_encode($response);
    }elseif ($count > 1) {
        $response['status'] = 0;
        $response['message'] = 'You have some problem, Please contact to us';
        return json_encode($response);
    }
    else{

        $response['status'] = 1;
        $response['message'] = 'Username or password is wrong';
        return json_encode($response);
    }
    
}
?>