<!-- ################################################
  
  Description: Parking area can issue gate pass to a particular Visitor.
  Developed by: Soemen Banerjee
  Created Date: 17-03-2018
####################################################-->
<?php 
  include "all_nav/header.php";
  if (isset($_SESSION['prk_admin_id'])) {
    $user_name=$_SESSION['parking_admin_name'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
  }
  $respon1=prk_user_add_list($prk_admin_id);
 // echo $respon1;prk_user_add_list($prk_admin_id)
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: gray;

  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
    <!-- for datatable -->
    <link href="../lib/highlightjs/github.css" rel="stylesheet">
    <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet"> 
    <!-- header position -->
    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">User Verification <?php //echo $respon1;?></h5>
      </div><!-- am-pagetitle -->
      	
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card pd-20 pd-sm-40 col-md-12">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="wd-5p">Image</th>
                  <th class="">Name</th>
                  <th class="">Mobile</th>
                  <th class="">Tower & Flat</th>
                  <th class="wd-20p">Send Date</th>
                  <th class="wd-20p">Action</th>
                </tr>
              </thead>
              <tbody>
              	<?php 
                // echo($respon1);
                $respon = json_decode($respon1, true);
                if($respon['status']){
                  foreach($respon['user_address'] as $value){
                ?>
                <tr style="<?php if($value['user_add_verify_flag']==FLAG_Y){ ?>background-color: #def4f7;<?php }?>">
               	  <td><img src="<?php echo $value['user_img']; ?>" style="height: 40px; border-radius: 20%;"> </td>
                  <td><?php echo $value['user_name']; ?></td>
                  <td><?php echo $value['user_mobile']; ?></td>
                  <td><?php echo $value['tower_name'].','.$value['falt_name']; ?></td>
                  <td><?php echo $value['in_date']; ?></td>
	                <td>

                    <button style="color:white; background-color:green; width: 59px;" type="button" class="out btn" id="<?php echo($value['user_add_id'])?>" onclick="user_add_verify(this.id)" data-toggle="tooltip" data-placement="top" <?php if($value['user_add_verify_flag']==FLAG_Y){ ?> disabled <?php }?>><i class="fa fa-sign-out"></i><?php if($value['user_add_verify_flag']==FLAG_Y){ ?>Verified<?php }else{?>Verify<?php }?></button>
                  
                    <button style="color:white; background-color:red;" type="button" class="out btn" id="<?php echo($value['user_add_id'])?>" onclick="user_add_delete(this.id)" data-toggle="tooltip" data-placement="top"><i class="fa fa-sign-out"></i> Reject</button>
                  </td>
                </tr>
                <?php }} ?>
              </tbody>
            </table>
          </div>
          </div>
        </div>
      </div>
      <div class="am-pagebody">
      <!-- footer part -->
      <?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<style>
  .error_size{
    font-size: 11px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
<script>
  var prkUrl = "<?php echo PRK_URL; ?>";
  var user_name = "<?php echo $user_name; ?>";
  var prk_admin_id = "<?php echo $prk_admin_id; ?>";
  var token = "<?php echo $token; ?>";
  $(function(){
    'use strict';

    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });

    $('#datatable2').DataTable({
      bLengthChange: false,
      searching: false,
       "scrollX": true
    });
    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
  function user_add_verify(user_add_id){
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will verify the user address</p>",
      theme: 'modern',
      type: 'green',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Verify',
          btnClass: 'btn-green',
          keys: ['Y', 'shift'],
          action: function(){
            // alert(user_add_id);
            if (user_add_id != '') {
              var urlDtl = prkUrl+'prk_user_add_verify.php';
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :
                {
                  'prk_admin_id':prk_admin_id,
                  'user_name':user_name,
                  'user_add_id':user_add_id,
                  'token':token
                },
                dataType:'html',
                success  :function(data)
                {
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Address Verified Successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                          location.reload(true);
                          // visitor_list();
                        }
                      }
                    });
                  }else{
                    if (json.session=='0') {
                      window.location.replace("logout.php");
                    }
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
  function user_add_delete(user_add_id){
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>This will reject user's Address</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Reject',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            // alert(user_add_id);
            if (user_add_id != '') {
              var urlDtl = prkUrl+'prk_user_add_reject.php';
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :
                {
                  'prk_admin_id':prk_admin_id,
                  'user_name':user_name,
                  'user_add_id':user_add_id,
                  'token':token
                },
                dataType:'html',
                success  :function(data)
                {
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Address rejected Successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                          location.reload(true);
                          // visitor_list();
                        }
                      }
                    });
                  }else{
                    if (json.session=='0') {
                      window.location.replace("logout.php");
                    }
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
</script>
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
