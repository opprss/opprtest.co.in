<!-- ################################################
  
Description: List of user's vechile | user can delete vehicle from this page.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
include "all_nav/header.php";
?>

<!-- header position -->

    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Vehicle Details</h5>
        <!-- <form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card single pd-20 pd-sm-40 col-md-12">
          <div class="row">
            <div class="col-md-12">
              <form id="selectFilter" method="post" action="">
                <div class="form-layout">
                  <div class="row mg-b-25">
                    <!-- <div class="col-md-2"></div> -->
                    <div class="col-md-10">
                      
                    </div>
                    <div class="col-md-2 error_show pull-right">
                      <div class="form-group">
                          <!-- <button class="btn btn-block" name="add_vehicle" id="add_vehicle">Add Vehicle</button> -->
                          <button type="button" class="btn btn-block user_button" name="add_vehicle" id="add_vehicle" onclick="document.location.href='add-vehicle';">ADD VEHICLE</button>
                      </div>
                    </div>
                  </div><!-- form-layout -->
                </div>
              </form>
            </div>
          </div>
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            
            <div class="row">
              <div class="col-md-12">                  
                  <div class="list-group widget-list-group">
                    <?php
                      $user_id = $_SESSION['user_admin_id'];
                      //$user_id = 99;
                      if (isset($_POST['v_type']))
                        $vehicle = $_POST['v_type'];
                      else
                        $vehicle = '';
                      
                      if(isset($user_id) ){
                        $response = array();
                        $response = vehicleList($user_id,$vehicle);
                        if($response['status']){
                          $count = count($response['vehicle']);

                        for ($i=0; $i < $count ; $i++) { 
                          $user_veh_dtl_id = $response['vehicle'][$i]['user_veh_dtl_id'];
                          $img = $response['vehicle'][$i]['vehicle_typ_img'];
                          $v_reg = $response['vehicle'][$i]['user_veh_reg'];
                          $v_exp = $response['vehicle'][$i]['user_veh_reg_exp_dt'];

                          $user_ins_number = $response['vehicle'][$i]['user_ins_number'];
                          $user_ins_exp_dt = $response['vehicle'][$i]['user_ins_exp_dt'];
                          $user_emu_number = $response['vehicle'][$i]['user_emu_number'];
                          $user_emu_exp_dt = $response['vehicle'][$i]['user_emu_exp_dt'];
                        
                        ?>

                        <div class="list-group-item rounded-top-0" >
                          <div class="media pd--b-20">
                            <div class="d-flex mg-l-0 mg-t-10 mg-r-20 wd-40">
                              <img src='<?php echo OPPR_BASE_URL.$img; ?>' style='width: 50px; height: 50px;'>
                            </div><!-- d-flex -->
                            <div class="media-body">
                              <div class="row">
                                <div class="col-lg-12 tx-18 pd-b-10">
                                    <?php echo $response['vehicle'][$i]['user_veh_number'] ;?>
                                </div>
                                <div class="col-lg-3">
                                   <p class="mg-b-0 tx-12">
                                    Reg No-
                                     <?php
                                    if (!empty($v_reg))
                                       echo $v_reg;
                                     else
                                      echo "N/A";
                                  ?>
                                   <br>
                                    Exp Date- 
                                    <?php
                                      if (!empty($v_exp))
                                         echo $v_exp;
                                       else
                                        echo "N/A";
                                     ?>
                                   </p>
                                </div>
                                <div class="col-lg-3">
                                   <p class="mg-b-0 tx-12">
                                    Inc No-
                                     <?php
                                    if (!empty($user_ins_number))
                                       echo $user_ins_number;
                                     else
                                      echo "N/A";
                                  ?>
                                   <br>
                                    Exp Date- 
                                    <?php
                                      if (!empty($user_ins_exp_dt))
                                         echo $user_ins_exp_dt;
                                       else
                                        echo "N/A";
                                     ?>
                                   </p>
                                </div>
                                <div class="col-lg-3">
                                   <p class="mg-b-0 tx-12">
                                    PUC No-
                                     <?php
                                    if (!empty($user_emu_number))
                                       echo $user_emu_number;
                                     else
                                      echo "N/A";
                                  ?>
                                   <br>
                                    Exp Date- 
                                    <?php
                                      if (!empty($user_emu_exp_dt))
                                         echo $user_emu_exp_dt;
                                       else
                                        echo "N/A";
                                     ?>
                                   </p>
                                </div>
                              </div>                              
                            </div><!-- media-body -->
                            <div class="d-flex">
                              <a class="mg-l-10"  href="<?php echo 'vehicle-modify.php?list_id='.base64_encode($user_veh_dtl_id)?>" class="" data-toggle="tooltip" data-placement="top" title="Modify">
                                <i class="fa fa-pencil" style="font-size:18px"></i>
                              </a>
                              &nbsp;&nbsp;&nbsp;&nbsp;

                              <button type="button" class="clean-button" name="delete" id="delete<%=count++%>" value="<?php echo $user_veh_dtl_id; ?>" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
                              
                            </div>
                          </div><!-- media -->
                        </div><!-- list-group-item -->
                    <?php
                        }
                      }else{
                    ?>
                    <h5 class="text-center">Vehical Not Found</h5>
                    <?php
                      }
                    }else{
                         $response['status'] = 0; 
                         $response['message'] = 'Invalid API Call';
                         echo json_encode($response);
                         //header('location: signin.php');
                      }
                    ?>
                  </div>

              </div>
              
            </div>
          </div>
        </div><!-- card -->
      </div><!-- am-pagebody -->
      

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->

<script type="text/javascript">
  /*for Tool Tip*/
  $(document).ready(function(){
    var userUrl = "<?php echo USER_URL; ?>";
    /*delete popup*/ 
    $('button[id^="delete"]').on('click', function() {
      var user_admin_id = "<?php echo $_SESSION['user_admin_id'];?>";
      var user_veh_dtl_id = this.value;
      /*'user_veh_dtl_id','user_admin_id','mobile','token'*/
      $.confirm({
        title: 'Are You Sure?',
        content: "<p style='font-size:0.8em;'>It will delete the item permanently</p>",
        theme: 'modern',
        type: 'red',
        buttons: {
          cancel: function () {
          },
          somethingElse: {
            text: 'Delete',
            btnClass: 'btn-red',
            keys: ['Y', 'shift'],
            action: function(){
              if (user_veh_dtl_id != '') {
                //alert(prk_rate_id);
                var urlDtl = userUrl+'add_vehicle_delete.php';
                //alert(urlDtl);
                $.ajax({
                  url :urlDtl,
                  type:'POST',
                  data :
                  {
                    'user_veh_dtl_id':user_veh_dtl_id,
                    'user_admin_id':user_admin_id,
                    'mobile':"<?php echo $_SESSION['mobile'] ;?>",
                    'token': '<?php echo $token;?>'
                  },
                  dataType:'html',
                  success  :function(data)
                  {
                    var json = $.parseJSON(data);
                    if (json.status){
                      location.reload(true);
                    }else{
                      if( typeof json.session !== 'undefined'){
                        if (!json.session) {
                          window.location.replace("logout.php");
                        }
                      }else{
                        $.alert({
                        icon: 'fa fa-frown-o',
                        theme: 'modern',
                        title: 'Error !',
                        content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                        type: 'red'
                      });
                      }
                    }
                  }
                });
              }else{
                $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
                type: 'red'
                });
              }
            }
          }
        }
      });
    });
   /*/delete popup*/




    $('[data-toggle="tooltip"]').tooltip();

  });
</script>

<script>
// <!-- Chiranjib Added this part
let d=document.getElementsByClassName('clean-button');

for(let i=0;i<d.length;i++){
d[i].addEventListener("mouseover",function(){
										d[i].style.cursor="pointer";
										}
								);
}
// <!-- End-->
</script>