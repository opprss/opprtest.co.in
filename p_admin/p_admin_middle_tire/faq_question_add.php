<?php
    require_once 'api/parkAreaReg_api.php';
    // require_once 'api/global_api.php';
    
    $response = array();
    if(isAvailable(array('question'))){
     	if(isEmpty(array('question'))){
            $question = trim($_POST['question']);
            $response = faq_question_add($question);
     	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
        }
     }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
     }
     echo ($response);
?>