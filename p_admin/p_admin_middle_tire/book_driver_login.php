<?php 
/*
Description: parking area rate delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
$response = array();
if(isAvailable(array('book_driver_mobile','book_driver_password'))){
 	if(isEmpty(array('book_driver_mobile','book_driver_password'))){ 
	 	$book_driver_mobile = trim($_POST['book_driver_mobile']);
	 	$book_driver_password = trim($_POST['book_driver_password']);
		$response = book_driver_login($book_driver_mobile,$book_driver_password);
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 	$response = json_encode($response);
}
echo $response;
?>