<?php 
/*
Description: Parking area sub unit insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('bok_tran_id','book_driver_dtl_id','veh_trip_end_gps_address','veh_trip_end_longitude','veh_trip_end_latitude','veh_trip_end_gps_date','veh_trip_end_veh_km','tran_delay_reason'))){
    if(isEmpty(array('bok_tran_id','book_driver_dtl_id','veh_trip_end_veh_km'))){
        $prk_admin_id = isset($_REQUEST['prk_admin_id']) ? trim($_REQUEST['prk_admin_id']) : ''; 
        $bok_tran_id = trim($_REQUEST['bok_tran_id']);
        $book_driver_dtl_id = trim($_REQUEST['book_driver_dtl_id']);
        $veh_trip_end_gps_address = trim($_REQUEST['veh_trip_end_gps_address']);
        $veh_trip_end_longitude = trim($_REQUEST['veh_trip_end_longitude']);
        $veh_trip_end_latitude = trim($_REQUEST['veh_trip_end_latitude']);
        $veh_trip_end_gps_date = trim($_REQUEST['veh_trip_end_gps_date']);
        $veh_trip_end_veh_km = trim($_REQUEST['veh_trip_end_veh_km']);
        $tran_delay_reason = trim($_REQUEST['tran_delay_reason']);
        $oth1 = isset($_REQUEST['oth1']) ? trim($_REQUEST['oth1']) : '';  
        $oth2 = isset($_REQUEST['oth2']) ? trim($_REQUEST['oth2']) : '';  
        $oth3 = isset($_REQUEST['oth3']) ? trim($_REQUEST['oth3']) : '';  
        $oth4 = isset($_REQUEST['oth4']) ? trim($_REQUEST['oth4']) : '';  
        $oth5 = isset($_REQUEST['oth5']) ? trim($_REQUEST['oth5']) : '';  

        $response = book_veh_trip_track_end($prk_admin_id,$bok_tran_id,$book_driver_dtl_id,$veh_trip_end_gps_address,$veh_trip_end_longitude,$veh_trip_end_latitude,$veh_trip_end_gps_date,$veh_trip_end_veh_km,$tran_delay_reason,$oth1,$oth2,$oth3,$oth4,$oth5);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>