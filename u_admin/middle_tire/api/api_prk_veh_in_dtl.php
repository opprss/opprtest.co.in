<?php
/*
Description: vehicle in parking area the show in details.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function prk_veh_in_dtl($prk_veh_trc_dtl_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    

    $sql="SELECT  `user_detail`.`user_nick_name`,
        `prk_area_dtl`.`prk_area_name`, 
        `prk_area_address`.`prk_add_area`, 
        `prk_area_address`.`prk_add_city`, 
        `prk_area_address`.`prk_add_state`, 
        `prk_area_address`.`prk_add_country`, 
        `prk_veh_trc_dtl`.`prk_veh_type`,
        `vehicle_type`.`vehicle_type_dec`,
        `prk_veh_trc_dtl`.`veh_number`,
        `prk_veh_trc_dtl`.`veh_out_otp`,
        `prk_veh_trc_dtl`.`veh_token`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%D-%M-%Y') AS `veh_in_date`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%I:%i %p') AS `veh_in_time`,
        `state`.`state_name`,
        `city`.`city_name`
    FROM `prk_area_dtl`, `prk_area_address`, `prk_veh_trc_dtl`, `user_detail`,`state`,`city`,vehicle_type
    WHERE `prk_veh_trc_dtl`.`prk_veh_type`=`vehicle_type`.`vehicle_sort_nm`
    AND `vehicle_type`.`active_flag`='Y'
    AND `prk_veh_trc_dtl`.`prk_admin_id` = `prk_area_dtl`.`prk_admin_id`
    AND `prk_veh_trc_dtl`.`prk_admin_id` = `prk_area_address`.`prk_admin_id` 
    AND `prk_veh_trc_dtl`.`user_admin_id`= `user_detail`.`user_admin_id` 
    AND `prk_area_dtl`.`active_flag`='".FLAG_Y."'
    AND `prk_area_address`.`active_flag`='".FLAG_Y."' 
    AND `prk_area_address`.`del_flag`='".FLAG_N."'
    AND `user_detail`.`active_flag` ='".FLAG_Y."'
    AND `city`.`e_id`= `prk_area_address`.`prk_add_city`
    AND `state`.`e`= `prk_area_address`.`prk_add_state`
    AND `prk_veh_trc_dtl`.`prk_veh_out_time` IS null
    AND `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id` = '$prk_veh_trc_dtl_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['user_nick_name'] = $val['user_nick_name'];
        $response['prk_area_name'] = $val['prk_area_name'];
        $response['prk_add_area'] = $val['prk_add_area'];
        $response['prk_add_city'] = $val['prk_add_city'];
        $response['prk_add_state'] = $val['prk_add_state'];
        $response['prk_add_country'] = $val['prk_add_country'];
        $response['prk_veh_type'] = $val['prk_veh_type'];
        $response['vehicle_type_dec'] = $val['vehicle_type_dec'];
        $response['veh_number'] = $val['veh_number'];
        $response['veh_in_date'] = $val['veh_in_date'];
        $response['veh_in_time'] = $val['veh_in_time'];
        $response['state_name'] = $val['state_name'];
        $response['city_name'] = $val['city_name'];
        $response['veh_out_otp'] = $val['veh_out_otp'];
        $response['veh_token'] = $val['veh_token'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>