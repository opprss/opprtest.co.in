<?php 
/*
Description: vehicle in advance payment resive dtl show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
    // require_once 'api/global_api.php';
    require_once 'api/parkAreaReg_api.php';
    $response = array();
    if(isAvailable(array('prk_veh_trc_dtl_id'))){
        if(isEmpty(array('prk_veh_trc_dtl_id'))){

            $prk_veh_trc_dtl_id = trim($_POST['prk_veh_trc_dtl_id']);
            $response= cash_memo_in($prk_veh_trc_dtl_id);
            echo ($response);

        }else{
                $response['status'] = 0;
                $response['message'] = 'All Fields Are Mandatory';
                echo json_encode($response); 
         }
     }else{
         $response['status'] = 0; 
         $response['message'] = 'Invalid API Call';
         echo json_encode($response); 
     } 
?>
 