<!-- ################################################
  
Description: user can add licences.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php include "all_nav/header.php"; ?>
<style type="text/css">
  .size{
    font-size: 11px;
  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
  .error{
    font-size: 11px;
    color: red;
  }
  .error_size{
    font-size: 11px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
  .user-lebel{
    font-size: 11px !important;
    text-transform: uppercase;
  }
</style>
<!-- header position -->
    <div class="am-mainpanel"><!-- cloding in footer -->
      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Add Licence</h5>
      </div><!-- am-pagetitle -->
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card single pd-20 pd-sm-40 col-md-12">
          <!-- <h6 class="card-body-title">Sign Up Here</h6>
          <p class="mg-b-20 mg-sm-b-30">A form with a label on top of each form control.</p> -->
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <div class="row">
              <div class="col-md-12">
                <p class="mg-l-10 tx-inverse tx-12">
                  <b>Note:</b> Please fill all the details exactly as your driving licence
                </p>
                  <form id="modifyVehicle" method="post" action="#">
                    <div class="form-layout">
                      <div class="row mg-b-25">

                        <div class="col-md-3">
                          <div class="form-group">                            
                            <label class="form-control-label user-lebel">Licence Number <span class="tx-danger">*</span></label>
                            <input type="text" class="form-control" placeholder="Licence Number" name="user_lic_number" id="user_lic_number" onkeydown="upperCase(this)">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Licence Holder Name <span class="tx-danger">*</span></label>

                            <input type="text" class="form-control" placeholder="Name on Licence" name="user_lic_name" id="user_lic_name" onkeydown="upperCase(this)">
                          </div>
                        </div>
                        <!-- <div class="col-md-3">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">S/D/W of<span class="tx-danger">*</span></label>

                            <input type="text" class="form-control" placeholder="S/D/W of" name="s_d_w_of" id="s_d_w_of" onkeydown="upperCase(this)">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Blood Gr.<span class="tx-danger">*</span></label>

                            <input type="text" class="form-control" placeholder="Blood Gr." name="blood_gr" id="blood_gr" onkeydown="upperCase(this)">
                          </div>
                        </div> -->
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Vehicle Class <span class="tx-danger">*</span></label>

                            <input type="text" class="form-control" placeholder="Vehicle Class" name="user_lic_veh_class" id="user_lic_veh_class" onkeydown="upperCase(this)">
                          </div>
                        </div>
                        <!-- <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Date of Birth(DD-MM-YYYY) <span class="tx-danger">*</span></label>

                            <input type="text" name="date_of_birth" id="date_of_birth" placeholder="Date of Birth(DD-MM-YYYY)" class="form-control prevent_readonly" readonly="readonly">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Address<span class="tx-danger">*</span></label>

                            <input type="text" class="form-control" placeholder="Address" name="address" id="address" onkeydown="upperCase(this)">
                          </div>
                        </div> -->
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Issue Date (DD-MM-YYYY) <span class="tx-danger">*</span></label>

                            <input type="text" name="user_lic_issue_date" id="user_lic_issue_date" placeholder="Issue Date (DD-MM-YYYY)" class="form-control prevent_readonly" readonly="readonly">
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Valid Till (DD-MM-YYYY) <span class="tx-danger">*</span></label>

                            <input type="text" name="user_lic_valid_till" id="user_lic_valid_till" placeholder="Valid Till (DD-MM-YYYY)" class="form-control prevent_readonly" readonly="readonly">
                          </div>
                        </div>
                        <!-- <div class="col-md-3">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Licence Issued By <span class="tx-danger">*</span></label>

                            <input type="text" class="form-control" placeholder="Issued By" name="user_lic_issued_by" id="user_lic_issued_by" onkeydown="upperCase(this)">
                          </div>
                        </div> -->
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Licence Image<span class="tx-danger"></span></label>
                            <input type="file" name="licence_img" accept="image/*" id="licence_img" style="font-size: 13px;">
                            <input type="hidden" name="licence_image" class="licence_image" id="licence_image">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <div id="results">captured Licence image will appear here...</div>
                          </div>
                        </div>
                        <div class="col-md-3"></div>
                        <div class="col-lg-2 error_show">
                          <div class="form-group">

                            <input type="button" value="SAVE" class="btn btn-block btn-primary wl_user_button" name="save" id="save">

                          </div>
                        </div>
                        <div class="col-lg-2 error_show">
                          <div class="form-group">
                            <button type="reset" class="btn btn-block btn-primary wl_user_button" id="reset">Reset</button>
                          </div>
                        </div>
                        <div class="col-lg-2 error_show">
                          <div class="form-group">
                            <button type="button" class="btn btn-block wl_user_button_skip" name="skip" id="skip" onclick="document.location.href='licence';">Back</button>
                          </div>
                        </div>
                    </div><!-- form-layout -->
                  </form>
              </div>
              
            </div>
          </div>
        </div><!-- card -->
        </div><!-- am-pagebody -->
      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">
  $( document ).ready( function () {
    var userUrl = "<?php echo USER_URL; ?>";
    /*FORM SUBMIT*/
    $('#save').click(function(){
      var user_lic_number = $('#user_lic_number').val();
      var user_lic_name = $('#user_lic_name').val();
      var user_lic_veh_class = $('#user_lic_veh_class').val();
      var user_lic_issue_date = $('#user_lic_issue_date').val();
      var user_lic_valid_till = $('#user_lic_valid_till').val();
      var licence_image=$('#licence_image').val();
      //var user_lic_issued_by = $('#user_lic_issued_by').val();
      //var s_d_w_of = $('#s_d_w_of').val();
      //var blood_gr = $('#blood_gr').val();
      //var date_of_birth = $('#date_of_birth').val();
      //var address = $('#address').val();
      var user_admin_id = "<?php echo $_SESSION['user_admin_id'] ?>";
      var mobile ="<?php echo $user_mobile ?>";
      var token="<?=$token?>";

      var form_data = new FormData();
      form_data.append('user_lic_number', user_lic_number);
      form_data.append('user_lic_name', user_lic_name);
      form_data.append('user_lic_veh_class', user_lic_veh_class);
      form_data.append('user_lic_issue_date', user_lic_issue_date);
      form_data.append('user_lic_valid_till', user_lic_valid_till);
      form_data.append('user_lic_issued_by', '');
      form_data.append('user_admin_id', user_admin_id);
      form_data.append('mobile', mobile);
      form_data.append('user_lic_img_web', licence_image);
      form_data.append('s_d_w_of', '');
      form_data.append('blood_gr', '');
      form_data.append('date_of_birth', '');
      form_data.append('address', '');
      form_data.append('token', token);

      if($("#modifyVehicle").valid()){
        // alert('ok');
        var urlUserLicDtl = userUrl+'user_lic_dtl.php';
        $(this).val('Wait ...').prop('disabled', true);
        $.ajax({
          url :urlUserLicDtl,
          type:'POST',
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,                         
          dataType:'html',
          success  :function(data){
            $('#save').val('SAVE').prop('disabled', false);
            var json = $.parseJSON(data);
            // alert(data);
            if (json.status) {
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.8em;'>Licence added successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                      window.location='licence';
                  }
                }
              });
            }else{
              if( typeof json.session !== 'undefined'){
                if (!json.session) { 
                   // window.location.replace("logout.php"); 
                   alert('redirect');                   
                }                  
              }else{                    
                $.alert({                    
                  icon: 'fa fa-frown-o',                    
                  theme: 'modern',                    
                  title: 'Error !',                    
                  content: "Somting went wrong",                    
                  type: 'red'                  
                });                  
              }                
            }
          }
        });
      }
    });
  });
  function gatePassUppercase() {
    var x = document.getElementById("user_gate_pass_num");
    x.value = x.value.toUpperCase();
  }
  function upperCase(a){
    setTimeout(function(){
        a.value = a.value.toUpperCase();
    }, 1);
  }
  /*alphabets only*/
  $("#user_lic_name").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)){
        event.preventDefault();
    }
  });
  /*date*/
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    //alert(now);
    pickmeup('#date_of_birth', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
        if (date > now) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
        }
        return {};
      } 
    });
    pickmeup('#user_lic_issue_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
        if (date > now) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
        }
        $('#error_user_lic_issue_date').text('');
        return {};
      } 
    });
    pickmeup('#user_lic_valid_till', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
        if (date < now) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        $('#error_user_lic_valid_till').text('');
        return {};
      } 
    });
  });
</script>
<script type="text/javascript">
  $( document ).ready( function () {
    $( "#modifyVehicle" ).validate( {
      rules: {
        user_lic_number: "required",
        user_lic_name: "required",
        //s_d_w_of: "required",
        //blood_gr: "required",
        user_lic_veh_class: "required",
        //address: "required",
        //user_lic_issued_by: "required",
        //date_of_birth: "required",
        user_lic_issue_date: "required",
        user_lic_valid_till: "required"
        //licence_img:"required"
      },
      messages: {
      }
    });
    /*$("#owner_living_status").on('change', function(){
      var v_type = $('#owner_living_status').val();
      if(v_type == "")
      {
        $('#error_owner_living_status').show();
        return false;
      }else{
          $('#error_owner_living_status').hide();
          return true;
      }
    });*/
  });
</script>
<script type="text/javascript">
  $(function() {
    $("#licence_img").change(function() {
      //$("#message").empty(); // To remove the previous error message
      var file = this.files[0];
      var imagefile = file.type;
      var match= ["image/jpeg","image/png","image/jpg"];
      if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
      {
      return false;
      }
      else
      {
      var reader = new FileReader();
      reader.onload = imageIsLoaded;
      reader.readAsDataURL(this.files[0]);
      }
    });
    $("#reset").click(function() {
      imageIsLoaded_disable();
      var validator = $("#modifyVehicle").validate();
      validator.resetForm();
    });
  });
  function imageIsLoaded(e) {
    $(".licence_image").val(e.target.result);
    document.getElementById('results').innerHTML = '<img style="height: 100px;width: 230px;" src="'+e.target.result+'"/>';
  };
  function imageIsLoaded_disable(e) {
    document.getElementById('results').innerHTML = 'captured Licence image will appear here...';
  };
</script>