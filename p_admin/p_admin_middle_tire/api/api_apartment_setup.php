<?php
/*
Description: parking area employ create.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function apartment_setup($prk_admin_id,$parking_admin_name,$last_due_day,$choose_due_day,$flat_sq_ft,$rate_sq_ft,$maint_start_date,$adv_payment_month,$due_payment_month,$crud_type,$late_fine_payable_time,$late_fine_charrge,$late_fine_rate_type,$state_mant_date){
   $response = array();
   $allplayerdata = array();
    global $pdoconn;
    switch ($crud_type) {
        case 'I':
            $sql="UPDATE `apartment_setup` SET `updated_by`='$parking_admin_name',`updated_date`='".TIME."',`active_flag`='".FLAG_N."' WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $apartment_setup_id=uniqid();
                $sql="INSERT INTO `apartment_setup`(`apartment_setup_id`, `prk_admin_id`, `last_due_day`, `choose_due_day`, `flat_sq_ft`, `rate_sq_ft`, `maint_start_date`, `adv_payment_month`, `due_payment_month`, `inserted_by`, `inserted_date`, `late_fine_payable_time`, `late_fine_charrge`, `late_fine_rate_type`, `state_mant_date`) VALUES ('$apartment_setup_id','$prk_admin_id','$last_due_day','$choose_due_day','$flat_sq_ft','$rate_sq_ft','$maint_start_date','$adv_payment_month','$due_payment_month','$parking_admin_name','".TIME."','$late_fine_payable_time','$late_fine_charrge','$late_fine_rate_type','$state_mant_date')";
                $query  = $pdoconn->prepare($sql);
                if($query->execute()){
                    $response['status'] = 1;
                    $response['message'] = 'Successful';
                }else{
                    $response['status'] = 0;
                    $response['message'] = 'Not Successful ';
                }  
            }else{
                $response['status'] = 0;
                $response['message'] = 'Not Successful ';
            }  
            break;
        case 'U':
            /*$sql="UPDATE `flat_details` SET `tower_id`='$tower_name',`flat_name`='$flat_name',`end_date`='$end_date',`updated_by`='$parking_admin_name',`updated_date`='".TIME."',`flat_size`='$flat_size',`two_wh_spa`='$two_wh_spa',`four_wh_spa`='$four_wh_spa' WHERE `flat_id`='$flat_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";

            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Flat Modify Save';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Flat Modify Not Save ';
            }  */
            $response['status'] = 0;
            $response['message'] = 'Wrong Select';
            break;
        case 'S':
            $sql="SELECT `apartment_setup_id`, `prk_admin_id`, `last_due_day`, `choose_due_day`, `flat_sq_ft`, `rate_sq_ft`, DATE_FORMAT(`maint_start_date`,'%d-%m-%Y') AS `maint_start_date`, `adv_payment_month`, `due_payment_month`, `late_fine_payable_time`, `late_fine_charrge`, `late_fine_rate_type`,`state_mant_date` FROM `apartment_setup` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['apartment_setup_id'] =  $val['apartment_setup_id'];
                $response['prk_admin_id'] =  $val['prk_admin_id'];
                $response['last_due_day'] =  $val['last_due_day'];
                $response['choose_due_day'] =  $val['choose_due_day'];
                $response['flat_sq_ft'] =  $val['flat_sq_ft'];
                $response['rate_sq_ft'] =  $val['rate_sq_ft'];
                $response['maint_start_date'] =  $val['maint_start_date'];
                $response['adv_payment_month'] =  $val['adv_payment_month'];
                $response['due_payment_month'] =  $val['due_payment_month'];
                $response['late_fine_payable_time'] =  $val['late_fine_payable_time'];
                $response['late_fine_charrge'] =  $val['late_fine_charrge'];
                $response['late_fine_rate_type'] =  $val['late_fine_rate_type'];
                $response['state_mant_date'] =  $val['state_mant_date'];
            }else{
                $response['status'] = 0;
                $response['message'] = 'Not Successful';
            }  
            break;
        case 'LS':
            /*$sql="SELECT `flat_details`.`flat_id`,
                `flat_details`.`tower_id`,
                `tower_details`.`tower_name`,
                `flat_details`.`flat_name`,
                `flat_details`.`prk_admin_id`,
                `flat_details`.`flat_size`,
                `flat_details`.`two_wh_spa`,
                `flat_details`.`four_wh_spa`,
                `flat_details`.`eff_date`,
                `flat_details`.`end_date`
                FROM `flat_details`,`tower_details` 
                WHERE `tower_details`.`tower_id`=`flat_details`.`tower_id`
                AND `flat_details`.`prk_admin_id`='$prk_admin_id' 
                AND `flat_details`.`active_flag`='".FLAG_Y."' 
                AND `flat_details`.`del_flag`='".FLAG_N."' 
                ORDER BY `flat_details`.`flat_unique_id` DESC";
                // AND '' BETWEEN `eff_date` AND `end_date`
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $arr_catagory = $query->fetchAll();
                foreach($arr_catagory as $val)
                {
                    $park['flat_id'] =  $val['flat_id'];
                    $park['tower_id'] =  $val['tower_id'];
                    $park['tower_name'] =  $val['tower_name'];
                    $park['flat_name'] =  $val['flat_name'];
                    $park['prk_admin_id'] =  $val['prk_admin_id'];
                    $park['flat_size'] =  $val['flat_size'];
                    $park['two_wh_spa'] =  $val['two_wh_spa'];
                    $park['four_wh_spa'] =  $val['four_wh_spa'];
                    $park['eff_date'] =  $val['eff_date'];
                    $park['end_date'] =  $val['end_date'];
                    // $park['living_status'] =  $val['living_status'];
                    array_push($allplayerdata, $park);
                }
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['flat_list'] = $allplayerdata;
            }else{
                $response['status'] = 0;
                $response['message'] = 'List Empty';
            }*/
            $response['status'] = 0;
            $response['message'] = 'Wrong Select';
            break;
        case 'D':
            /*$sql="UPDATE `flat_details` SET `del_flag`='".FLAG_Y."',`updated_by`='$parking_admin_name',`updated_date`='".TIME."' WHERE `flat_id`='$flat_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Delete Successful';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Delete Not Successful';
            }*/
            $response['status'] = 0;
            $response['message'] = 'Wrong Select';
            break;
        default:
            $response['status'] = 0;
            $response['message'] = 'Wrong Select';
            break;
    }
    return json_encode($response);  
}
?>