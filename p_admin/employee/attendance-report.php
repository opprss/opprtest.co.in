<!-- ################################################
  
Description: 
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
 
<?php 
  include "all_nav/header.php";
  $prk_sub_user_list =prk_sub_user_list($prk_admin_id);

  $all_month1=all_month();
  $all_month = json_decode($all_month1, true);
  $all_year1=all_year();
  $all_year = json_decode($all_year1, true);
?>
  <style type="text/css">
    .size{
      font-size: 11px;
      text-transform: uppercase;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
  </style> 
  <link href="../../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
  <link href="../../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
  <link href="../../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
  <div class="am-mainpanel">
    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Attendance Report</h5>
    </div>
  </div>
  <div class="am-mainpanel">
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40" style="padding: 10px;">
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <form id="attnd_list" method="post">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label class="form-control-label size">Select Employee<span class="tx-danger">*</span></label>
                  <select name="prk_area_user" id="prk_area_user" style="opacity: 0.8">
                    <option value="">SELECT EMPLOYEE</option>
                    <option value="all">ALL</option>
                    <?php
                      $count = count($prk_sub_user_list);
                      if ($count == 0) {
                    ?>
                        <option value="NA">NA</option>
                    <?php     
                      }else{
                        foreach ($prk_sub_user_list as $val){
                    ?>
                          <option value="<?php echo $val['prk_area_user_id']?>"><?php echo $val['prk_user_name']?></option>
                    <?php
                        }  
                      } 
                    ?>
                  </select><br>
                <span id="error_prk_area_user" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="form-control-label size">Select Month<span class="tx-danger">*</span></label>
                  <select name="month" id="month" style="">
                      <option value="">SELECT MONTH</option>
                      <?php
                        foreach ($all_month['all_month'] as $val){ ?>  
                          <option value="<?php echo $val['sort_name']?>"><?php echo $val['full_name']?></option>
                        <?php          
                        }
                      ?> 
                  </select><br>
                  <span id="error_month" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="form-control-label size">Select Year<span class="tx-danger">*</span></label>
                  <select name="year" id="year" style="">
                    <option value="">SELECT YEAR</option>
                    <?php
                      foreach ($all_year['all_year'] as $val){ ?>  
                        <option value="<?php echo $val['sort_name']?>"><?php echo $val['full_name']?></option>
                      <?php          
                      }
                    ?>
                  </select><br>
                  <span id="error_year" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <input type="button" value="Search" class="btn btn-block btn-primary prk_button" style="margin-top: 20px;" name="ok" id="ok">
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="am-pagebody" id="data">
      <div class="card pd-20 pd-sm-40" style="padding: 10px;">
        <div class="table-wrapper">
          <table id="datatable1" class="table display responsive nowrap" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="wd-5p">Image</th>
                <th class="wd-5p">Month</th>
                <th class="wd-15p">Employee Name</th>
                <th class="wd-5p">In Date</th>
                <th class="wd-5p">In Time</th>
                <th class="wd-5p">Out Date</th>
                <th class="wd-5p">Out Time</th>
              </tr>
            </thead>
            <tbody id="dataLines"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <?php include"all_nav/footer.php"; ?>
  <script src="../../lib/highlightjs/highlight.pack.js"></script>
  <script src="../../lib/datatables/jquery.dataTables.js"></script>
  <script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
  <script src="../../lib/datatables-responsive/jquery.dataTables.min.js"></script>
  <script src="../../lib/datatables-responsive/dataTables.buttons.min.js"></script>
  <script src="../../lib/datatables-responsive/jszip.min.js"></script>
  <script src="../../lib/datatables-responsive/pdfmake.min.js"></script>
  <script src="../../lib/datatables-responsive/vfs_fonts.js"></script>
  <script src="../../lib/datatables-responsive/buttons.html5.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#prk_area_user").select2();
      $("#month").select2();
      $("#year").select2();
      attendanceListSearch();
      $('#ok').click(function(){
        if($("#attnd_list").valid()){
          attendanceListSearch();
        }
      });
    });
    function attendanceListSearch(){
      $('#refresh_button').show();
      var prk_area_user_id_search=$('#prk_area_user').val();
      var month=$('#month').val();
      var year=$('#year').val();
      var current_veh = pkrEmpUrl+'attendance_list.php';
      // 'prk_admin_id','prk_area_user_id','prk_area_user_id_search','token','month','year'
      $.ajax({
        url :current_veh,
        type:'POST',
        data :{
          'prk_admin_id':prk_admin_id,
          'prk_area_user_id':prk_area_user_id,
          'prk_area_user_id_search':prk_area_user_id_search,
          'month':month,
          'year':year,
          'token':token
        },
        dataType:'html',
        success  :function(data){
          // alert(data);
          var demoLines = '';
          var json = $.parseJSON(data);
          if (json.status){
            var c = 0;
            for (var key in json.atten_list) {
              c +=1;
              var keyy = json.atten_list[key];
              demoLines += '<tr>\
                <td> <img src="'+keyy['prk_user_img']+'" style="height: 40px;"></td>\
                <td>'+keyy['this_month']+'</td>\
                <td>'+keyy['prk_user_name']+'</td>\
                <td>'+keyy['in_date']+'</td>\
                <td>'+keyy['in_time']+'</td>\
                <td>'+keyy['out_date']+'</td>\
                <td>'+keyy['out_time']+'</td>\
                </tr>';
            }
          }
          $('#datatable1').dataTable().fnDestroy();
          $("#dataLines").html(demoLines);
          datatable_show();
        }
      });
    }
    function datatable_show(){
      'use strict';
      $('#datatable1').DataTable({
        responsive: true,
        dom: 'Blfrtip',
        language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
        },
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
          {
            text: '<span id="refresh_button"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
            action: function ( e, dt, node, config ) {
                // alert( 'Button activated' );
                attendanceListSearch();
            }
          },
          { 
            extend: 'copyHtml5', 
            //footer: true,
            title: 'ATTENDANCE DETAILS SHEET' 
          },
          { 
            extend: 'excelHtml5', 
            //footer: true,
            title: 'ATTENDANCE DETAILS SHEET'  
          },
          {
              extend: 'pdfHtml5',
              //footer: true,
              title: 'ATTENDANCE DETAILS SHEET',
              customize: function(doc) {
                doc.content[1].margin = [ 0, 0, 0, 0 ] //left, top, right, bottom
              }
          },
        ]
      });
      $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
      $('#refresh_button').hide();
    }
  </script>
<script type="text/javascript">
  $( document ).ready( function () {
    $( "#attnd_list" ).validate({
      rules: {
        prk_area_user: "valueNotEqualPrk_area_user",
        month: "valueNotEqualMonth",
        year: "valueNotEqualYear"
      },
      messages: {
        prk_area_user: "",
        month: "",
        year: ""
      }
    });
  });
  $.validator.addMethod("valueNotEqualPrk_area_user", function(value){
    arg = "";
    if (value != "") {
      $("#error_prk_area_user").text('');
      return arg !== value;
    }else{
      $("#error_prk_area_user").text('This field is required.');
      return false;
    }
  });
  $.validator.addMethod("valueNotEqualMonth", function(value){
    arg = "";
    if (value != "") {
      $("#error_month").text('');
      return arg !== value;
    }else{
      $("#error_month").text('This field is required.');
      return false;
    }
  });
  $.validator.addMethod("valueNotEqualYear", function(value){
    arg = "";
    if (value != "") {
      $("#error_year").text('');
      return arg !== value;
    }else{
      $("#error_year").text('This field is required.');
      return false;
    }
  });
</script>
