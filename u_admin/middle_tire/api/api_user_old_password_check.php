<?php 
/*
Description: user old passwod change. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function oldPasswordChange($mobile,$oldpassword){
    global $pdoconn;
    $response = array();
    $sql ="SELECT `user_admin_id`,`user_ac_status` FROM `user_admin` WHERE `user_mobile`=:user_mobile AND `user_password`=:user_password";
    $query  = $pdoconn->prepare($sql);
    $query->execute(array('user_mobile'=>$mobile,'user_password'=>md5($oldpassword)));
    $count=$query->rowCount();
    if($count>0){
        $response['status'] = 1;
        $response['message'] = 'Right Password';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Old Password And Mobile Number is Wrong';
        return json_encode($response);
    }
}
?>