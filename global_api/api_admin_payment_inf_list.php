<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function admin_payment_inf_list($prk_admin_id,$oth1,$oth2){
    $allplayerdata = array();
    global $pdoconn;
    $sql = "SELECT `admin_payment_inf_id`, 
        `admin_payment_by`, 
        `prk_admin_id`, 
        `payment_id`, 
        `payment_key`, 
        `website`, 
        `industry_type`, 
        `channel_id_app`, 
        `channel_id_web`, 
        `payment_name`, 
        `payment_purpose`,
        `payment_image`
        FROM `admin_payment_inf` 
        WHERE `active_flag`='".FLAG_Y."' 
        AND `del_flag`='".FLAG_N."' 
        AND `prk_admin_id`='$prk_admin_id' 
        ORDER BY `order_by_list` ASC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['admin_payment_inf_id'] = $val['admin_payment_inf_id'];
            $park['admin_payment_by'] = $val['admin_payment_by'];
            $park['admin_payment_description'] = $val['admin_payment_description'];
            // $park['prk_admin_id'] = $val['prk_admin_id'];
            $park['payment_id'] = $val['payment_id'];
            $park['payment_key'] = $val['payment_key'];
            $park['website'] = $val['website'];
            $park['industry_type'] = $val['industry_type'];
            $park['channel_id_app'] = $val['channel_id_app'];
            $park['channel_id_web'] = $val['channel_id_web'];
            $park['payment_name'] = $val['payment_name'];
            $park['payment_purpose'] = $val['payment_purpose'];
            $park['payment_image'] = $val['payment_image'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['payment_inf_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>