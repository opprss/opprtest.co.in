<?php
/*
Description: Parking area login.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
    //oppr_super_employee_login
    require_once 'api/parkAreaReg_api.php';
    $response = array();
 
    if(isAvailable(array())){

        if(isEmpty(array())){
            // $parking_type = trim($_POST['parking_type']);
            // $acount_status = trim($_POST['acount_status']);
            // $employee_user_name = trim($_POST['employee_user_name']);
            // $prk_admin_id = trim($_POST['prk_admin_id']);
            $response = active_otp_list();
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandator';
            $response = json_encode($response);
        }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        $response = json_encode($response);
    }
    echo $response;
?>