<?php 
/*
Description: parking area visit the people all list. 
Developed by: Rakhal Raj Mandal
Created Date: 09-04-2018
Update date : -------
*/
require_once 'api/parkAreaReg_api.php';
$response = array();
$resp = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','veh_type','veh_number','prk_sub_unit_id','veh_owner_name','mobile','end_date','effective_date','prk_user_username','prk_gate_pass_num','token'))){
 	if(isEmpty(array('prk_admin_id','prk_area_user_id','veh_type','veh_number','prk_sub_unit_id','veh_owner_name','mobile','effective_date','prk_user_username','prk_gate_pass_num','token'))){ 
	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_area_user_id = trim($_POST['prk_area_user_id']);
        $veh_type = trim($_POST['veh_type']);
        $veh_number = trim($_POST['veh_number']);
        $prk_sub_unit_id = trim($_POST['prk_sub_unit_id']);
        $veh_owner_name = trim($_POST['veh_owner_name']);
        $mobile = trim($_POST['mobile']); 
        $end_date = trim($_POST['end_date']);
        $effective_date = trim($_POST['effective_date']);
        $user_name = trim($_POST['prk_user_username']);
        $gate_pass = trim($_POST['prk_gate_pass_num']);
        $tower_id = isset($_POST['tower_id']) ? trim($_POST['tower_id']) : ''; 
        $flat_id = isset($_POST['flat_id']) ? trim($_POST['flat_id']) : ''; 
        $living_status = isset($_POST['living_status']) ? trim($_POST['living_status']) : 'V'; 
        $park_lot_no = isset($_POST['park_lot_no']) ? trim($_POST['park_lot_no']) : ''; 
        $token = trim($_POST['token']);
        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
		if($json->status=='1'){

        	$response = prk_gate_pass_dtl($prk_admin_id,$veh_type,$veh_number,$prk_sub_unit_id,$mobile,$end_date,$veh_owner_name,$effective_date,$gate_pass,$user_name,$tower_id,$flat_id,$living_status,$park_lot_no);
        }else{

            $response = $resp;
        }
 	}else{
        $response['status'] = 0;
    	$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>