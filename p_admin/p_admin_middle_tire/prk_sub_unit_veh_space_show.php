<?php
/*
Description: Parking area sub unit vehicle space  SHOW.
Developed by: Rakhal Raj Mandal
Created Date: 05-05-2018
Update date : -------
*/ 
// require_once 'api/global_api.php';
require_once 'api/parkAreaReg_api.php';
$response = array();

if(isAvailable(array('prk_sub_unit_veh_space_id'))){

    if(isEmpty(array('prk_sub_unit_veh_space_id'))){
        $prk_sub_unit_veh_space_id = trim($_POST['prk_sub_unit_veh_space_id']);

        $response =  prk_sub_unit_veh_space_show($prk_sub_unit_veh_space_id);

    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>