<?php 
/*
Description: gate pass dtl list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
 require_once 'api/reg_api.php';
// include_once 'api/global_api.php'; 
$response = array();
if(isAvailable(array('user_admin_id','token','user_vehicle_no'))){
 	if(isEmpty(array('user_admin_id','token','user_vehicle_no'))){
	 	$user_admin_id = trim($_POST['user_admin_id']);
        $user_vehicle_no = trim($_POST['user_vehicle_no']);

	 	$token = ($_POST['token']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $response = user_veh_gate_pass_dtl_list($user_admin_id,$user_vehicle_no);
            echo ($response);
        }else{
            $response['status'] = 0; 
            $response['session'] = 0;
            $response['message'] =  'Session Expired Please Login Again';
            echo json_encode($response);
        }
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?>