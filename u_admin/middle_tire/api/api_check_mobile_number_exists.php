<?php
/*
Description: mobile number registor yes or not.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function check_mobile_number_exists($mobile){
    $response = array();
    global $pdoconn;
    $sql ="SELECT `user_admin_id`,`user_ac_status` FROM `user_admin` WHERE `user_mobile`=:user_mobile";
    $query  = $pdoconn->prepare($sql);
    $query->execute(array('user_mobile'=>$mobile));
    $count=$query->rowCount();
    if($count>0){
        $response['status'] = 1;
        $response['message'] = 'User Exists';
    }else{
        $response['status'] = 0;
        $response['message'] = 'User Not Exists';
    }
    return json_encode($response);
}
?>