<?php 
/*  Description:  Famali group Add.
    Developed by: Rakhal Raj Mandal
    Created Date: 06-05-2019
    Update date : 06-05-2019
*/ 
include_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_admin_id','mobile','token','user_add_id','prk_admin_id','user_family_name','user_family_gender','user_family_relation','user_family_mobile','intercom_per'))){
    if(isEmpty(array('user_admin_id','mobile','token','user_add_id','user_family_name','user_family_gender'))){
        $user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
        $token = trim($_POST['token']);
        $user_add_id = trim($_POST['user_add_id']);
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $user_family_name = trim($_POST['user_family_name']);
        $user_family_gender = ($_POST['user_family_gender']);
        $user_family_relation = ($_POST['user_family_relation']);
        $user_family_mobile = ($_POST['user_family_mobile']);
        $intercom_per = ($_POST['intercom_per']);
        $user_family_img = isset($_POST['user_family_img']) ? trim($_POST['user_family_img']) : ''; 
        $call_type = isset($_POST['call_type']) ? trim($_POST['call_type']) : 'M'; 
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $response=user_family_group($user_admin_id,$mobile,$user_add_id,$prk_admin_id,$user_family_name,$user_family_gender,$user_family_relation,$user_family_mobile,$intercom_per,$user_family_img,$call_type);
        }else{
            $response= $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response= json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response= json_encode($response);
}
echo ($response);
?>