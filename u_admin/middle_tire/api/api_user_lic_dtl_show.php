<?php
/*
Description: user licence dtails.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_lic_dtl_show($user_lic_id){
    $response = array();
    $allplayerdata = array();
    $gate_pass = array();
    global $pdoconn;
    $sql = "SELECT * FROM `user_lic_dtl` WHERE `user_lic_id`='$user_lic_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
        $gate_pass['status'] = 1;
        $gate_pass['message'] = 'Successful';
        $gate_pass['user_lic_id'] = $val['user_lic_id'];
        $gate_pass['user_lic_number'] = $val['user_lic_number'];
        $gate_pass['user_lic_name'] = $val['user_lic_name'];
        $gate_pass['user_lic_veh_class'] = $val['user_lic_veh_class'];
        $gate_pass['user_lic_issue_date'] = $val['user_lic_issue_date'];
        $gate_pass['user_lic_issued_by'] = $val['user_lic_issued_by'];
        $gate_pass['user_lic_valid_till'] = $val['user_lic_valid_till'];
        $gate_pass['s_d_w_of'] = $val['s_d_w_of'];
        $gate_pass['blood_gr'] = $val['blood_gr'];
        $gate_pass['date_of_birth'] = $val['date_of_birth'];
        $gate_pass['address'] = $val['address'];
        $gate_pass['user_lic_img'] = $val['user_lic_img'];
        $gate_pass['user_lic_img_status'] = $val['user_lic_img_status'];
        return json_encode($gate_pass);
}
?>