<?php
/*
Description: Right vehicle number check
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function v_numberChecker($v_number){
    $response = array();
    $len = strlen($v_number);
    $wb=substr($v_number,0,2);
    $no=substr($v_number,2,2);
    $lno=substr($v_number,-4);
    global $pdoconn;
    if($len>=8){

        $sql = "SELECT * FROM `state` WHERE `short_name`='$wb'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $count=$query->rowCount();
        if($count>0){
            if (preg_match("/[0-9]{2}/",$no)) {
                if (preg_match("/[0-9]{4}/",$lno)) {
                    $response['status'] =1;
                    $response['message'] = 'Welcome';
                }else{
                    $response['status'] =0;
                    $response['message'] = 'Please Input Correct Vehicle Number';
                }
            }else{
                $response['status'] =0;
                $response['message'] = 'Please Input Correct Vehicle Number';
            }
        }else{
            $response['status'] =0;
            $response['message'] = 'Vehicle State Code is Wrong';
        }
    }else{
        $response['status'] =0;
        $response['message'] = 'Enter Vehicle Number atleast 8 characters';
    }
    return json_encode($response);
}
?>