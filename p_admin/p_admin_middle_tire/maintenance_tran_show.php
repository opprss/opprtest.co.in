<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2019
*/ 
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','parking_admin_name','token','year','form_month','tower_id','flat_id','owner_id','living_status','tenant_id','flat_master_id'))){
    if(isEmpty(array('prk_admin_id','parking_admin_name','token','year','form_month','tower_id','flat_id','owner_id','living_status'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
        $parking_admin_name = trim($_POST['parking_admin_name']);
        $token = trim($_POST['token']);
        $year = trim($_POST['year']);
        $form_month = trim($_POST['form_month']);
        // $to_month = trim($_POST['to_month']);
        $tower_id = trim($_POST['tower_id']);
        $flat_id = trim($_POST['flat_id']);
        $owner_id = trim($_POST['owner_id']);
        $living_status = trim($_POST['living_status']);
        $tenant_id = trim($_POST['tenant_id']);
        $flat_master_id = trim($_POST['flat_master_id']);
        $resp=prk_token_check($prk_admin_id,$token);
            $json = json_decode($resp);
            if($json->status){
                $response = maintenance_tran_show($prk_admin_id,$parking_admin_name,$year,$form_month,$tower_id,$flat_id,$owner_id,$living_status,$tenant_id,$flat_master_id);
            }else{
                $response = $resp;
            }
    }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
 }
echo $response;
?>