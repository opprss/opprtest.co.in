<?php
/*
Description: Parking visitor gate pass issue.
Developed by: Rakhal Raj Mandal
Created Date: 31-10-2018
Update date :31-10-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','visitor_name','visitor_mobile','visitor_gender','emergency_no','visitor_date_of_birth','visitor_blood_group','visitor_contractor','visitor_department','visitor_pf_number','visitor_esi_number','visitor_add','visitor_landmark','visitor_country','visitor_state','visitor_city','visitor_pin_cod','effective_date','end_date','inserted_by','visitor_img'))){
    if(isEmpty(array('prk_admin_id','visitor_name','visitor_mobile','visitor_gender','effective_date','inserted_by'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $visitor_name = trim($_POST['visitor_name']);
        $visitor_mobile = trim($_POST['visitor_mobile']);
        $visitor_gender = trim($_POST['visitor_gender']);
        $emergency_no = trim($_POST['emergency_no']);
        $visitor_date_of_birth = trim($_POST['visitor_date_of_birth']);
        $visitor_blood_group = trim($_POST['visitor_blood_group']);
        $visitor_contractor = trim($_POST['visitor_contractor']);
        $visitor_department = trim($_POST['visitor_department']);
        $visitor_pf_number = trim($_POST['visitor_pf_number']);
        $visitor_esi_number = trim($_POST['visitor_esi_number']);
        $visitor_add = trim($_POST['visitor_add']);
        $visitor_landmark = trim($_POST['visitor_landmark']);
        $visitor_country = trim($_POST['visitor_country']);
        $visitor_state = trim($_POST['visitor_state']);
        $visitor_city = trim($_POST['visitor_city']);
        $visitor_pin_cod = trim($_POST['visitor_pin_cod']);
        $effective_date = $_POST['effective_date'];
        $end_date = $_POST['end_date'];
        $inserted_by = trim($_POST['inserted_by']);
        $visitor_img = trim($_POST['visitor_img']);
        if (!empty($visitor_img)) {      
            $PNG_TEMP_DIR ='./../uploades/'.$inserted_by.'/visitor_pro_img/';       
            if (!file_exists($PNG_TEMP_DIR)) {
                $old_mask = umask(0);
                mkdir($PNG_TEMP_DIR, 0777, TRUE);
                umask($old_mask);
            }
            $image_parts = explode(";base64,", $visitor_img);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
          
            $image_base64 = base64_decode($image_parts[1]);
            $fileName = uniqid() . '.png';
            $file = $PNG_TEMP_DIR . $fileName;
            file_put_contents($file, $image_base64);
            $visitor_img = "uploades/".$inserted_by."/visitor_pro_img/".$fileName;
        }else{

            $visitor_img = '';
        }
        $response = contractual_gate_pass_add($prk_admin_id,$visitor_name,$visitor_mobile,$visitor_gender,$emergency_no,$visitor_date_of_birth,$visitor_blood_group,$visitor_contractor,$visitor_department,$visitor_pf_number,$visitor_esi_number,$visitor_add,$visitor_landmark,$visitor_country,$visitor_state,$visitor_city,$visitor_pin_cod,$effective_date,$end_date,$inserted_by,$visitor_img);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>