<?php
/*
Description: State name select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
    require_once 'api/global_api.php';

    // require_once 'api/global_api.php';
    $response = array();
    if(isAvailable(array('state_id'))){
        if(isEmpty(array('state_id'))){
            $state_id = trim($_POST['state_id']);
            $response=state_name($state_id);
            echo $response;
        }else{
                $response['status'] = 0;
                $response['message'] = 'All Fields Are Mandatory';
                echo json_encode($response); 
        }

    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        echo json_encode($response); 
    } 
?>