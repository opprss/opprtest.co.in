<?php
/*
Description: prk_veh_type_by_pay_amt.
Developed by: Rakhal Raj Mandal
Created Date: 15-06-2018
Update date : ----------
*/ 
function prk_tot_pay_amt($start_date,$end_date,$prk_admin_id){
    $allplayerdata = array();
    global $pdoconn;
    $format = "Y-m-d";
    if(date($format, strtotime($start_date)) == date($start_date) && date($format, strtotime($end_date)) == date($end_date)) {
        $start_date= $start_date;
        $end_date=  $end_date ;
    } else {
        $start_date = date("Y-m-d", strtotime($start_date));
        $end_date = date("Y-m-d", strtotime($end_date));
    }  
    $sql = "SELECT ifnull(sum(payment_amount),0) as total_pay, '$start_date' as payment_amount 
        from payment_receive where prk_admin_id = '$prk_admin_id' and del_flag = '".FLAG_N."'
        and DATE_FORMAT(payment_rec_date ,'%Y-%m-%d')  BETWEEN '$start_date' AND '$end_date'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Ssucessfull';
        $response['total_pay'] = $val['total_pay'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }    
    return json_encode($response);
}
?>