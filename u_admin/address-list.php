<!-- ################################################
  
Description: user can add or modify address here. add and modify is difided with a reffer(from where page requist is comming that URL) variable in JS.
Developed by: Rakhal Raj Mandal
Created Date: 17-03-2018

 ####################################################-->
<?php 
  include "all_nav/header.php"; 
  if (isset($_SESSION['user_admin_id'])) {

    $user_admin_id = $_SESSION['user_admin_id'];
    $response_address = user_address_list($user_admin_id);
    $address_list = json_decode($response_address, true);
  }
?>
<style>
  .user-lebel{
  	text-transform: uppercase!importent;
  }
    .size{
      font-size: 11px;
    }
  .error_size{
    font-size: 11px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
  .fir_add{
    /*border-style: dashed;
    height: 266px;
    width: 320px;
    border-width: 2px;
    box-sizing: border-box;
    border-color: #C7C7C7;
    text-align: center;
    display: table-cell;
    vertical-align: middle;
    border-radius: 10px;*/

    height: 266px;
    width: 320px;
    border-width: 1px;
    box-sizing: border-box;
    border-color: #C7C7C7;
    box-shadow: 0 2px 1px 0 rgba(0,0,0,.16);
    border-style: solid;
    vertical-align: middle;
  }
  .all_add{
    height: 266px;
    width: 320px;
    border-width: 1px;
    box-sizing: border-box;
    border-color: #C7C7C7;
    box-shadow: 0 2px 1px 0 rgba(0,0,0,.16);
    border-style: solid;
  }
</style>
  <!-- header position -->
  <div class="am-mainpanel">

    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Address List <?php //echo $address_list['status'];?></h5>
    </div><!-- am-pagetitle -->
    
    <div class="am-pagebody">
      <div class="card single pd-20 pd-sm-40 col-md-12">
        <div class="editable tx-16 pd-30 mg-l-70 tx-inverse" style="padding-bottom: 0px;padding: 5px;">

          <div class="row">
            <div class="col-md-3 mg-r-50  mg-t-20 fir_add">
              <a href="address"><div style="padding-top: 82px;">
                <i class="fa fa-plus tx-50 tx" style="color: #eaeded;padding-left: 82px;"></i>
                <h2 class="a-color-tertiary"><b style="
                  color: #767676!important; font-size: 20px;padding-left: 46px;">Add address</b></h2>
              </div></a>
            </div>

            <?php 
            if(!$address_list['status']==0){
            $cou=0;
            foreach($address_list['user_address'] as $value){
              $add=$value['user_address'].', '.$value['landmark'].', '.$value['city_name'].', '.$value['state_name'].', '.$value['user_add_country'].', PIN CODE : '.$value['user_add_pin'];
              $cou+=1;
              ?>
              <!-- $prk_admin_name = $value['prk_admin_name']; -->
            <div class="col-md-3 mg-r-50 mg-t-20 all_add" style="<?php if($value['user_add_verify_flag']=='N'){ ?>background-color: antiquewhite;<?php } ?>">
              <div><h2 class="a-color-tertiary"><b  style="font-size: 24px;padding-left: 38px;">Address-<?php echo $cou; ?></b></h2></div>
              <h6><b style="font-size: 15px;">Area: <?php echo $value['prk_area_name']; ?></b></h6>
              <label style="font-size: 14px;"><?php echo $add; ?></label>

              <div id="ya-myab-edit-address-desktop-row-0" style="position: absolute; bottom: 20px; left: 22px; color: #00b8d4" class="a-row edit-address-desktop-link">
                <?php if($value['user_add_verify_flag']=='N'|| $value['prk_area_name']=='OTHER AREA'){?>
                  <a href="#" onclick="return false" data-original-title="Family Group"><i class="fa fa-user tx-20 tx" style="color: #00b8d4;"></i></a> |
                <?php }else{?>
                  <a href="<?php echo 'user-family-group?user_add_id='.base64_encode($value['user_add_id']).'&prk_admin_id='.base64_encode($value['prk_admin_id']);?>" data-original-title="Family Group"><i class="fa fa-user tx-20 tx" style="color: #00b8d4;"></i></a> |
                <?php }?>
                <a href="user-edit-address?user_add_id=<?PHP echo base64_encode($value['user_add_id']);?>" data-original-title="Edit"><i class="fa fa-edit tx-20 tx" style="color: #00b8d4;"></i></a> | 
                <button id="<?PHP echo $value['user_add_id'];?>" onclick="add_delete(this.id)" style="padding: 0;border: none;background: none;cursor: pointer;"><i class="fa fa-trash-o tx-20 tx" style="color: #f31a4b;"></i></button> 

              </div>
            </div>
            <?php }} ?>
          </div>
        </div>
      </div><!-- card user_add_id-->
    </div><!-- am-pagebody -->
  </div>
  <!-- footer part -->
    <?php include"all_nav/footer.php"; ?>
  <!-- footer part End-->
<script type="text/javascript">
  $( document ).ready( function () {
  });
  function add_delete(user_add_id){
    // alert('ok');
    var user_admin_id = "<?php echo $user_admin_id; ?>";
    var user_mobile = "<?php echo $user_mobile; ?>";
    var userUrl = "<?php echo USER_URL; ?>";
    // alert(user_add_id);
    var urlUserDelAddress = userUrl+'user_address_delete.php';
    
    $.confirm({
    title: 'Are You Sure?',
    content: "<p style='font-size:0.8em;'>Delete This Address</p>",
    theme: 'modern',
    type: 'green',
    buttons: {
      cancel: function () {
      },
      somethingElse: {
        text: 'Delete',
        btnClass: 'btn-red',
        keys: ['Y', 'shift'],
        action: function(){
          if (user_add_id != '') {
            $.ajax({
              url :urlUserDelAddress,
              type:'POST',
              data :
              {
                'user_add_id':user_add_id,
                'user_admin_id':user_admin_id,
                'mobile':user_mobile,
                'token':"<?php echo $token?>"
              },//'user_add_id','user_admin_id','mobile','token'
              dataType:'html',
              success  :function(data)
              {
                // alert(data);
                if (data = true){
                  $.alert({
                    icon: 'fa fa-smile-o',
                    theme: 'modern',
                    title: 'Success !',
                    content: "<p style='font-size:0.9em;'>Delete successfully</p>",
                    type: 'green',
                    buttons: {
                      Ok: function () {
                        location.reload(true);
                      }
                    }
                  });
                }else{
                  if( typeof json.session !== 'undefined'){
                    if (!json.session) {
                      window.location.replace("logout.php");
                    }
                  }else{
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.8em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                  }
                }
              }
            });
          }else{
            $.alert({
            icon: 'fa fa-frown-o',
            theme: 'modern',
            title: 'Error !',
            content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
            type: 'red'
            });
          }
        }
      }
    }
  });
  }
</script>