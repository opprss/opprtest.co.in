<?php
/*
Description: parking area employ update.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
// function prk_area_user_update($prk_area_user_id,$prk_admin_id,$prk_user_username,$prk_user_emp_no,$prk_user_name,$prk_user_password,$prk_user_repassword){
function prk_area_user_update($prk_area_user_id,$prk_admin_id,$prk_user_username,$prk_user_emp_no,$prk_user_name,$prk_user_mobile,$prk_user_gender,$prk_user_address,$prk_user_landmark,$prk_user_state,$prk_user_city,$prk_user_pincode,$inserted_by,$prk_user_emp_category,$prk_user_img){
    global $pdoconn;
    $response = array();
    $sql="UPDATE `prk_area_user` SET `prk_user_username`='$prk_user_username',`prk_user_emp_no`='$prk_user_emp_no',`prk_user_name`='$prk_user_name',`prk_user_mobile`='$prk_user_mobile',`prk_user_gender`='$prk_user_gender',`prk_user_img`='$prk_user_img',`prk_user_address`='$prk_user_address',`prk_user_landmark`='$prk_user_landmark',`prk_user_country`='INDIA',`prk_user_state`='$prk_user_state',`prk_user_city`='$prk_user_city',`prk_user_pincode`='$prk_user_pincode',`updated_by`='$inserted_by',`updated_date`='".TIME."',`prk_user_emp_category`='$prk_user_emp_category' WHERE `del_flag`='".FLAG_N."' AND `prk_area_user_id`='$prk_area_user_id'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Update sucessfull';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Update Not sucessfull';
        return json_encode($response);
    }
}
?>