<?php
/*
Description: Parking area login.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_area_login($prk_admin_name,$prk_admin_password){
    if(accountStatusLogin($prk_admin_name)=='true'){
        global $pdoconn;
        $response = array();
        $sql ="SELECT `prk_admin_id` FROM `prk_area_admin` WHERE `prk_admin_name`=:prk_admin_name AND `prk_admin_password`=:prk_admin_password";
        $query  = $pdoconn->prepare($sql);
        $query->execute(array('prk_admin_name'=>$prk_admin_name,'prk_admin_password'=>md5($prk_admin_password)));
        $count=$query->rowCount();
        if($count>0){
            $row = $query->fetch();
            $prk_admin_id =  $row['prk_admin_id'];
            $token = sha1(uniqid());
            if(prk_admin_token_check($prk_admin_id)){
                
                $sql = "INSERT INTO `prk_admin_token` (`prk_admin_id`, `token_value`,`start_date`, `active_flag`) VALUES ('$prk_admin_id', '$token','".TIME."', '".FLAG_Y."')";
                $query = $pdoconn->prepare($sql);
                $query->execute();  
                if ($query){
                    $response['status'] = 1;
                    $response['token'] = $token;
                    $response['prk_admin_id'] = $prk_admin_id;
                    $response['parking_admin_name'] = $prk_admin_name;
                    $response['message'] = 'login Sucessful';
                    /*$_SESSION["parking_admin_name"]=$prk_admin_name;
                $_SESSION["prk_admin_id"]=$prk_admin_id;*/
                }else{
                    $response['status'] = 0;
                    $response['token'] = $token;
                    $response['message'] = 'token not found';
                }
            }
            return json_encode($response);
        }
        else{
            $response['status'] = 0;
            $response['message'] = 'username or password is wrong';
            return json_encode($response);
        }
    }else{
        $response=accountStatusLogin($prk_admin_name);
        return json_encode($response);
    }
}
?>