<?php
/*
Description: Parking area vehicle rate delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function emergency_add($prk_admin_id,$user_admin_id,$adm_mer_dtl_name,$adm_mer_dtl_number,$adm_mer_dtl_remarks,$inserted_status,$inserted_by){
    global $pdoconn;
    $response = array();
    $adm_mer_dtl_id=uniqid();
    $sql = "INSERT INTO `admin_mer_dtl`(`adm_mer_dtl_id`, `prk_admin_id`, `user_admin_id`, `adm_mer_dtl_name`, `adm_mer_dtl_number`, `adm_mer_dtl_remarks`, `inserted_status`, `inserted_by`, `inserted_date`) VALUES ('$adm_mer_dtl_id','$prk_admin_id','$user_admin_id','$adm_mer_dtl_name','$adm_mer_dtl_number','$adm_mer_dtl_remarks','$inserted_status','$inserted_by','".TIME."')";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Sucessfull';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Unsucessfull';
        return json_encode($response);
    }
}
?>