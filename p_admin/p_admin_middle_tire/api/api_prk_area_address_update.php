<?php
/*
Description: Parking adimn Address Update.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_area_address_update($prk_address,$prk_land_mark,$prk_add_area,$prk_add_city,$prk_add_state,$prk_add_country,$prk_add_pin,$prk_admin_id,$prk_admin_name){
    global $pdoconn;
    $response = array();
    $sql ="UPDATE `prk_area_address` SET `active_flag`='".FLAG_N."', `updated_date`='".TIME."',`updated_by`='$prk_admin_name' WHERE `prk_admin_id`='$prk_admin_id'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
         $sql = "INSERT INTO `prk_area_address`(`prk_address`,`prk_land_mark`,`prk_add_area`,`prk_add_city`,`prk_add_state`,`prk_add_country`,`prk_add_pin`,`prk_admin_id`,`inserted_by`,`inserted_date`) VALUE('$prk_address','$prk_land_mark','$prk_add_area','$prk_add_city','$prk_add_state','$prk_add_country','$prk_add_pin','$prk_admin_id','$prk_admin_name','".TIME."')";
        $query = $pdoconn->prepare($sql);
        if($query->execute()){
            $response['status'] = 1;
            $response['message'] = 'Address Update sucessfull';
        }else{
            $response['status'] = 0;
            $response['message'] = 'Address Update Not sucessfull';
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'Update Not sucessfull';
    }
    return json_encode($response);
}
?>