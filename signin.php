<!-- ################################################
  
Description: User login | parking area admin login
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php
include('global_asset/config.php');
session_start();
session_destroy();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Meta -->
    <meta name="description" content="OPPRSS provides best quality digital parking services and digital parking solutions."/>
    <meta name="keywords" content="parking solution free , parking management software free , digital parikig solution , smart parking solution, app based parking , parking solution , automatic carparking system , parking solution in kolkata , parking ticket solutions/oppr parking solution , oppr software , parking management , autopay parking system , digital ticketing system , mobile parking solution , vehicle parking system" />
    <meta name="author" content="oppr software solution private limited">
    <title>OPPRSS | SIGNIN</title>
    <link rel="shortcut icon" href="<?php echo FAVICON ?>" type="image/x-icon">
    <!-- vendor css -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">
    <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <!-- Amanda CSS -->
    <link rel="stylesheet" href="css/amanda.css">
    <link rel="stylesheet" href="css/style-up.css">
  </head>
  <body>
    <div class="am-signin-wrapper">
      <div class="am-signin-box">
        <div class="row no-gutters">
          <div class="col-lg-7" style="background:url('img/index-banner.jpg');padding-bottom: 0px;">
            <div class="signin-logo">
              <div style="text-align: left;">
              <a href="http://opprss.com" target="_blank"><img style="width: 210px; text-align: left;" src="<?php echo LOGO ?>"></a></div>
              <div class="content">
                <h1>Welcome to OPPRSS</h1>
                <h5 class="d-none d-sm-block pd-b-10">We provide an innovative and executive parking & security solution that improves parking & security experience more than ever before.</h5>  
              </div>
              <div class="row features-icon">
                <div class="col-lg-4">
                  <i class="fa fa-tasks fa-3x"></i>
                  <h6 class="pd-t-5">Digitally Tracked</h6>
                </div> 
                <div class="col-lg-4">
                  <i class="fa fa-american-sign-language-interpreting fa-3x"></i>
                  <h6 class="pd-t-5">Better Communication</h6>
                </div> 
                <div class="col-lg-4">
                  <i class="fa fa-android fa-3x "></i>
                  <h6 class="pd-t-5">App Based</h6>
                </div>                
              </div>                           
              <div class="content-footer features-icon mg-t-50" style="text-align: right;">
                <button class="btn-tab" onclick="window.open('https://www.opprss.com/contact-us', '_blank');" id="con_us">Contact Us</button>
                <button class="btn-tab" id="user_tab">User</button>
                <button class="btn-tab" id="admin_tab">Parking Area</button>
                <button class="btn-tab" id="super_admin_tab">Super Adnin</button>
                 <hr>
                <p class="tx-center tx-white-5 tx-12 mg-t-0 mg-b-0"><?php echo COPYRIGHT_EMAIL?></p>
              </div>
            </div>
          </div>
          <div class="col-lg-5">
            <h2 class="tx-gray-800 mg-b-50 text-center">Login to <span id="prk_area">Admin/ Employee</span> <span id="user_area" style="display: none;">User</span><span id="super_admin" style="display: none;">Super Admin</span></h2>
            <div class="form-group tx-center mg-b-30" id="tab">
              <div class="col-lg-12">
                <div class="input-group">
                  <div id="radioBtn" class="btn-group">
                    <div class="row">
                  	<div class="col-lg-6" style="margin-left: -18px;"><a class="btn btn-custom active radio" data-toggle="login_for" data-title="A" name="login_for" id="parking" style="width: 150px;">Admin</a></div>

                    <div class="col-lg-6" style="margin-left: -18px;"><a class="btn btn-custom notActive radio" data-toggle="login_for" data-title="E" name="login_for" value="u" id="user" style="width: 150px;">Employee</a></div>
                  </div>
                  </div>
                  <input type="hidden" name="login_for" id="login_for" value="U">
                </div>
              </div>
            </div>
            <!-- parking admin -->
            <form id="AdminLoginForm" method="post" action="">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group error_show">
                    <input type="text" name="prk_admin_name" id="prk_admin_name" onkeyup="caps();"  class="form-control inputText" required>
                    <span class="floating-label" id="both">USERNAME</span>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group error_show">
                    <input type="password" name="admin_password" id="admin_password" class="form-control inputText" required>
                    <span class="floating-label">PASSWORD</span>
                  </div>
                </div>
              </div>
              <br/>
              <button type="button" class="btn btn-block" name="admin_signin" id="admin_signin">Login</button>
              <p id="admin_error" class="pd-t-10" style="color: #FFA07A;"></p>
              <div class="mg-t-10" style="font-size: auto;">
                <a class="pd-r-110" href="<?php echo PRK_BASE_URL.'vender-signup'?>">Signup</a>Forget
                <a href="<?php echo OPPR_BASE_URL.'forget-password'?>">Password</a> -or- 
                <a href="<?php echo OPPR_BASE_URL.'forget-username'?>">Username</a>
              </div>
            </form>
            <!-- /parking admin -->

            <!-- parking employee -->
            <form id="empLoginForm" method="post" action="#" style="display: none">

              <div class="row mg-t-10">
                <div class="col-md-10">
                  <div class="form-group">
                    <input autocomplete="off" type="text" name="username" id="username" class="form-control inputText"  onkeypress="return valid(event)" class="form-control inputText" required/>
                    <span class="floating-label">USERNAME</span>

                  </div>
                </div>
                <div class="col-md-2" style="padding:0px; margin:0px;">
                  <button type="button" class="" id="view">View</button>
                </div>
                <div class="col-md-12" style="padding:0px; margin:0px;">
                  <p class="ajax-loader text-center" style="padding: 0px; margin: 0px; visibility: hidden;">
                      <img src="<?php echo IMAGE_URL.'small-loader.gif';?>">
                    </p> 
                </div>
              </div>
              <!-- <div style="padding: 0px; margin: 0px; visibility: hidden;"> -->
                
              <!-- </div> -->
              <div class="row mg-t-10" id="employee_info" style="display: none;">
                <div class="col-md-4" id="prk_area_img">
                </div>
                <div class="col-md-8">
                  <h6 class="tx-white-10 mg-t-0 mg-b-0"><b>Area Name</b></h6>
                  <p class="tx-white-10 mg-t-0 mg-b-0" id="prk_area_name"></p>
                  <h6 class="tx-white-10 mg-t-10 mg-b-0"><b>Address</b></h6>
                  <p class="tx-white-10 mg-t-0 mg-b-0" id="prk_area_address"></p>
                </div>
                <div class="col-md-12 mg-t-10">
                  <p class="mg-t-0 mg-b-0">
                    <input type="checkbox" class="checkbox" id="agree" name="agree" required>
                    <label for="agree">Please Verify All Details</label>
                  </p>
                  <!-- <p class="mg-t-0 mg-b-0"><label for="agree" class="error block"></label></p> -->
                </div>
              </div>
              <div class="row mg-t-10">
                <div class="col-md-12">
                  <div class="form-group" id="password_block" style="display: none;"><!--  -->
                    <input autocomplete="off" type="password" name="password" id="password" class="form-control inputText" onchange="return trim(this)" required>
                    <span class="floating-label">PASSWORD</span>
                  </div>
                </div>
              </div>
              <div class="row mg-t-10">
                <div class="col-md-12">
                  <div class="form-group">
                     <button type="button" class="btn btn-block disabled" id="emp_signin" disabled="disabled">LOGIN</button>
                  </div>
                  <p id="error" style="color: #FFA07A;"></p>
                </div>
              </div>
              
              <!-- <p class="tx-white-10 tx-12 mg-t-10 mg-b-0">By creating this account, you agree to  our
              <a href="<?php //echo OPPR_BASE_URL.'#'?>"> Terms &amp; Conditions</a> &amp; <a href="<?php //echo OPPR_BASE_URL.'#'?>"> Privacy Policy</a>.</p> -->
            </form>
            <!--/ parking employee -->

            <!-- user -->
            <form id="userLoginForm" method="post" action="" style="display: none;">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group error_show">
                    <input type="text" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" name="mobile" id="mobile" onkeyup="caps();"  class="form-control inputText" maxlength="10" required>
                    <span class="floating-label" id="both">MOBILE</span>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group error_show">
                    <input type="password" name="user_password" id="user_password" class="form-control inputText" required>
                    <span class="floating-label">PASSWORD</span>
                  </div>
                </div>
              </div>
              <br/>
              <button type="button" class="btn btn-block" name="user_signin" id="user_signin">Login</button>
              <p id="user_error" class="pd-t-10" style="color: #FFA07A;"></p>

              <div class="mg-t-10" style="font-size: auto;">
                <a class="pd-r-110" href="<?php echo USER_BASE_URL.'signup'?>">Signup</a>

                Forget
                <a href="<?php echo OPPR_BASE_URL.'forget-password'?>">Password</a> -or- 
                <a href="<?php echo OPPR_BASE_URL.'forget-username'?>">Username</a>
              </div>
            </form>
            <!-- user -->
            <!-- Super Admin -->
            <form id="SuperLoginForm" method="post" action="" style="display: none;">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group error_show">
                    <input type="text" name="super_admin_user" id="super_admin_user" onkeyup="caps();"  class="form-control inputText" required>
                    <span class="floating-label" id="both">USERNAME</span>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group error_show">
                    <input type="password" name="super_admin_password" id="super_admin_password" class="form-control inputText" required>
                    <span class="floating-label">PASSWORD</span>
                  </div>
                </div>
              </div>
              <br/>
              <button type="button" class="btn btn-block" name="super_admin_signin" id="super_admin_signin">Login</button>
              <p id="super_admin_error" class="pd-t-10" style="color: #FFA07A;"></p>

              <div class="mg-t-10" style="font-size: auto;">
                <!-- <button type="button" id="admin_signup" class="mg-r-50">Admin Signup</button> -->
                <a class="pd-r-110" href="<?php echo PRK_BASE_URL.'vender-signup'?>">Signup</a>


                Forget
                <a href="<?php echo OPPR_BASE_URL.'forget-password'?>">Password</a> -or- 
                <a href="<?php echo OPPR_BASE_URL.'forget-username'?>">Username</a>
              </div>
            </form>
            <!-- Super Admin End -->
          </div>
        </div>
      </div>
    </div>
    <script src="lib/jquery/jquery.js"></script>
    <script src="lib/popper.js/popper.js"></script>
    <script src="lib/bootstrap/bootstrap.js"></script>
    <script src="lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
    <script src="js/amanda.js"></script>
    <script src="lib/validation/jquery.validate.js"></script>
  </body>
  <script type="text/javascript">
    var userUrl = "<?php echo USER_URL; ?>";
    var prkUrl = "<?php echo PRK_URL; ?>";
    var pkrEmpUrl = "<?php echo PRK_EMP_URL; ?>";
    var prkBaseUrl = "<?php echo PRK_BASE_URL; ?>";
    var prkEmpBaseUrl ="<?php echo PRK_EMP_BASE_URL; ?>";
    var userBaseUrl = "<?php echo USER_BASE_URL; ?>";
    var SUP_EMP_URL = "<?php echo SUP_EMP_URL; ?>";
    var SUP_EMP_BASE_URL = "<?php echo SUP_EMP_BASE_URL; ?>";
    /*Dynaic Radio Button*/
    var urlPrkSuperUserLogin = prkUrl+'prk_super_user_login.php';
    var urlLogin = userUrl+'login_web.php';
    $('#radioBtn a').on('click', function(){
      var login_for = $(this).data('title');
      //alert(login_for);
      var tog = $(this).data('toggle');
      $('#'+tog).prop('value', login_for);
      $('#admin_error').text(''); 
      $('#error').text('');
      $('a[data-toggle="'+tog+'"]').not('[data-title="'+login_for+'"]').removeClass('active').addClass('notActive');
      $('a[data-toggle="'+tog+'"][data-title="'+login_for+'"]').removeClass('notActive').addClass('active');
      if(login_for == 'A'){
        $('#AdminLoginForm').trigger("reset");
        $("#AdminLoginForm").show();
        $("#empLoginForm").hide();
        $("#SuperLoginForm").hide();
        $('#prk_area_name').text('');
        $('#prk_area_address').text('');
        $('#employee_info').hide();
        $('#prk_area_img').html('');
        $('#password_block').hide();
        $('#emp_signin').prop("disabled",true);
        $('#emp_signin').addClass("disabled");
        $('#password').val('');
        $('#agree').prop('checked', false);
        $('#error').text('');
        /**/
      }else if(login_for == 'E'){
        $('#empLoginForm').trigger("reset");
        $("#empLoginForm").show();
        $("#AdminLoginForm").hide();
        $("#SuperLoginForm").hide();
      }else{
        alert("chose somthing");
      }
    });
    $('#user_tab').click(function(){
      $('#AdminLoginForm').hide();
      $('#empLoginForm').hide();
      $('#SuperLoginForm').hide();
      $('#user_admin_toggle').hide();
      $('#tab').hide();
      $('#userLoginForm').show();
      $('#admin_user_toggle').show();
      $('#user_area').show();
      $('#prk_area').hide();
      $('#super_admin').hide();

      var login_for = $('#radioBtn a').data('title');
      var tog = $('#radioBtn a').data('toggle');
      $('#'+tog).prop('value', login_for);
      $('#admin_error').text(''); 
      $('#error').text('');
      //alert(hid);            
      $('a[data-toggle="'+tog+'"]').not('[data-title="'+login_for+'"]').removeClass('active').addClass('notActive');
      $('a[data-toggle="'+tog+'"][data-title="'+login_for+'"]').removeClass('notActive').addClass('active');
    });
    $('#admin_tab').click(function(){
      $('#AdminLoginForm').show();
      $('#empLoginForm').hide();
      $('#SuperLoginForm').hide();
      $('#user_admin_toggle').show();
      $('#tab').show();
      $('#userLoginForm').hide();
      $('#admin_user_toggle').hide();
      $('#super_admin').hide();
      $('#user_area').hide();
      $('#prk_area').show();
      var login_for = $('#radioBtn a').data('title');
      var tog = $('#radioBtn a').data('toggle');
      $('#'+tog).prop('value', login_for);
      $('#admin_error').text(''); 
      $('#error').text('');
      //alert(hid);            
      $('a[data-toggle="'+tog+'"]').not('[data-title="'+login_for+'"]').removeClass('active').addClass('notActive');
      $('a[data-toggle="'+tog+'"][data-title="'+login_for+'"]').removeClass('notActive').addClass('active');
    });
    /*user toggle*/
    /*Super Admin*/
    $('#super_admin_tab').click(function(){
      $('#SuperLoginForm').show();
      $('#AdminLoginForm').hide();
      $('#empLoginForm').hide();
      $('#user_admin_toggle').hide();
      $('#tab').hide();
      $('#userLoginForm').hide();
      $('#admin_user_toggle').show();
      $('#super_admin').show();
      $('#user_area').hide();
      $('#prk_area').hide();

      var login_for = $('#radioBtn a').data('title');
      var tog = $('#radioBtn a').data('toggle');
      $('#'+tog).prop('value', login_for);
      $('#super_admin_error').text(''); 
      $('#error').text('');
      //alert(hid);            
      $('a[data-toggle="'+tog+'"]').not('[data-title="'+login_for+'"]').removeClass('active').addClass('notActive');
      $('a[data-toggle="'+tog+'"][data-title="'+login_for+'"]').removeClass('notActive').addClass('active');
    });
    /*Super Admin End*/
    $('#agree').change(function() {
      if ($(this).is(":checked")) { 
        // alert('ok');
        // $("#username").attr("readonly", true);
      }else {
        // alert('NO');
        // $("#username").attr("readonly", false);
      }
    });
    /*validation*/
    $( document ).ready( function () {
      /*admin login*/
      $( "#AdminLoginForm" ).validate( {
        rules: {
          prk_admin_name: {
            required: true
          },
          admin_password: {
            required: true,
            minlength: 2
          }
        },
        messages: {
          prk_admin_name: {
            required: "Please enter a your username"
          },
          admin_password: {
            required: "Please provide a password",
            minlength: "Password must be more than 5 digits"
          }
        }
      });
      $('#admin_signin').click(function () {
        if($("#AdminLoginForm").valid()){
          var url = "<?php echo PRK_URL?>prk_super_user_login.php";
          var prk_admin_name = $("#prk_admin_name").val();
          var password = $("#admin_password").val();
          //'prk_admin_name','password'
          $.ajax({
            url :url,
            type:'POST',
            data :
            {
              'prk_admin_name':prk_admin_name,
              'password':password
            },
            dataType:'html',
            success  :function(data){
              // alert(data);
              var json = $.parseJSON(data);
              if (json.status) {
                var next_page = prkBaseUrl+json.next_page;
                window.location= next_page;
              }else{
                $('#admin_error').text(json.message); 
              }
            }
          });
        }
      });
      /*/admin login*/
      /*Super admin login*/
      $( "#SuperLoginForm" ).validate( {
        rules: {
          super_admin_user: {
            required: true
          },
          super_admin_password: {
            required: true,
            minlength: 2
          }
        },
        messages: {
          super_admin_user: {
            required: "Please enter a your username"
          },
          super_admin_password: {
            required: "Please provide a password",
            minlength: "Password must be more than 5 digits"
          }
        }
      });
      $('#super_admin_signin').click(function () {
        if($("#SuperLoginForm").valid()){
          var url = SUP_EMP_URL+"employee_user_login.php";
          //'prk_admin_name','password'
          var super_admin_user = $("#super_admin_user").val();
          var super_admin_password = $("#super_admin_password").val();
          //alert(password+"---"+ prk_admin_name);
          //'prk_admin_name','password'
          // alert(SUP_EMP_BASE_URL);
          $.ajax({
            url :url,
            type:'POST',
            data :
            {
              'super_admin_user':super_admin_user,
              'super_admin_password':super_admin_password
            },
            dataType:'html',
            success  :function(data){
              var json = $.parseJSON(data);
              if (json.status) {
                // alert(data);
                // {"status":1,"employee_user_id":"1","employee_user_name":"DEMO_1234","message":"login Suce
                var next_page = SUP_EMP_BASE_URL+"dashboard";
                window.location= next_page;
                
                // $_SESSION['employee_user_id'] = $json->employee_user_id;
                // $_SESSION['employee_user_name'] = $json->employee_user_name;
                // $.session.set("employee_user_id", $json->employee_user_id);
                // $.session.set("employee_user_name", $json->employee_user_name);
              }else{
                $('#super_admin_error').text(json.message); 
              }
            }
          });
        }
      });
      /*/ Super admin login*/
      /*employee login*/
      $( "#empLoginForm" ).validate( {
        rules: {
          username: "required",
          password: {
            required: true,
            minlength: 5
          }
        },
        messages: {
          representative: "Please enter your name",
          username: "Please enter employee username",
          password: {
            required: "Enter Password",
            minlength: "Minimum 5 charecter"
          }
        }
      });
      /*username validate*/
      $("#username").keyup(function () {
        ajaxCall(); 
      });
      $("#view").click(function () {
        ajaxCall(); 
      });
      /*checkbox*/
      $('#agree').change(function() {
        if(this.checked) {
            $('#password_block').show();
        }else{
          $('#password_block').hide();
          $('emp_signin').prop("disabled",true);
          $('#emp_signin').addClass("disabled");
          $('#password').val('');
          $('#error').text('');
        }    
      });
      /*login button*/
      $("#password").keyup(function () {
        var password = $('#password').val();
        if (password.length >= 5 ){
          $('#emp_signin').prop("disabled",false);
              $('#emp_signin').removeClass("disabled");
        }else{
          $('#emp_signin').prop("disabled",true);
          $('#emp_signin').addClass("disabled");
          $('#error').text('');
        }
      });
      $('#emp_signin').click(function () {
        if($("#empLoginForm").valid()){
          var username = $('#username').val();
          var password = $('#password').val(); 
          var urlParkAreaLogin = pkrEmpUrl+'prk_area_user_login_web.php';
          $.ajax({
            url :urlParkAreaLogin,
            type:'POST',
            data :{
              'prk_user_username':username,
              'prk_user_password':password,
              'device_token_id':'',
              'device_type':'WEB'
            },
            dataType:'html',
            success  :function(data){
              //alert(data);
              var json = $.parseJSON(data);
              if (json.status){
                window.location = prkEmpBaseUrl+'gate-list';
              }else{
                $('#error').text(json.message);
              }
            }
          });
        }
      });
      //uppercase
      $("#username").on('input', function(evt) {
        var input = $(this);
        var start = input[0].selectionStart;
        $(this).val(function (_, val) {
          return val.toUpperCase();
        });
        input[0].selectionStart = input[0].selectionEnd = start;
      });
      /*/employee login*/
      /*user login*/
      $( "#userLoginForm" ).validate( {
        rules: {
          mobile: {
            required: true,
            number: true,
            minlength: 10,
          },
          user_password: {
            required: true,
            minlength: 2
          }
        },
        messages: {
          mobile: {
            required: "Please enter a your mobile number",
            number: "Please enter a valid mobile number",
            minlength: "Please enter a valid mobile number",
          },
          user_password: {
            required: "Please provide a password",
            minlength: "Password must be more than 5 digits"
          }
        }
      });
      $('#user_signin').click(function () {
        if($("#userLoginForm").valid()){
          var urlLogin = userUrl+'login_web.php';
          var mobile = $("#mobile").val();
          var password = $("#user_password").val();
          var device_token_id = '';
          var device_type = 'WEB';
          //'prk_admin_name','password'
          $.ajax({
            url :urlLogin,
            type:'POST',
            data :{
              'mobile':mobile,
              'password':password,
              'device_token_id':device_token_id,
              'device_type':device_type
            },
            dataType:'html',
            success  :function(data){
              // alert(data);
              var json = $.parseJSON(data);
              if (json.status) {
                var next_page = userBaseUrl+json.next_page;
                window.location= next_page;
              }else{
                $('#user_error').text(json.message); 
              }
            }
          });
        }
      });
    });
    //ajax call
    function ajaxCall(){
      var username = $('#username').val();
      if (username.length >= 4 ){
        $('.ajax-loader').css("visibility", "visible");
        var urlParkAreaSuperUserDtl = pkrEmpUrl+'prk_area_super_user_dtl.php';
        $.ajax({
          url :urlParkAreaSuperUserDtl,
          type:'POST',
          beforeSend: function(){
            $('.ajax-loader').css("visibility", "visible");
          },
          data :
          {
           'prk_user_username':username
          },
          complete: function(){
            $('.ajax-loader').css("visibility", "hidden");
          },
          dataType:'html',
          success  :function(data){
            //alert(data);
            var json = $.parseJSON(data);
            if (json.status)  {
              //alert(data);
              $('#prk_area_name').text(json.prk_area_name);
              var address = json.prk_add_area+', '+json.city_name+', '+json.state_name+','+json.prk_add_country;
              $('#prk_area_address').html(address);
              $('#employee_info').show();
              var imgUrl = prkBaseUrl+json.prk_area_pro_img;
              //alert(imgUrl);
              if (json.prk_area_pro_img == null) {
                $('#prk_area_img').html('<img src="img/parking.png" class="wd-100" alt="">');
              }else if(json.prk_area_pro_img == '' ) {
                $('#prk_area_img').html('<img src="img/parking.png" class="wd-100" alt="">');
              }else{
                $('#prk_area_img').html('<img src="'+imgUrl+'" class="wd-100 rounded-circle" alt="">');
              }
            }else{
              $('#prk_area_name').text('');
              $('#prk_area_address').text('');
              $('#employee_info').hide();
              $('#prk_area_img').html('');
              $('#password_block').hide();
              $('#emp_signin').prop("disabled",true);
              $('#emp_signin').addClass("disabled");
              $('#password').val('');
              $('#agree').prop('checked', false);
              $('#error').text('');
              //alert(hello);
              //$('#error_username').text('Incorrect');
              //$('#oldPasword').focus();
            }
          }
        });
      }else{
        $('#prk_area_name').text('');
        $('#prk_area_address').text('');
        $('#employee_info').hide();
        $('#prk_area_img').html('');
        $('#password_block').hide();
        $('#emp_signin').prop("disabled",true);
        $('#emp_signin').addClass("disabled");
        $('#password').val('');
        $('#agree').prop('checked', false);
        $('#error').text('');
      }
    }
    //trim
    function trim (el) {
      el.value = el.value.
      replace (/(^\s*)|(\s*$)/gi, ""). // removes leading and trailing spaces
      replace (/[ ]{2,}/gi," ").       // replaces multiple spaces with one space 
      replace (/\n +/,"\n");           // Removes spaces after newlines
      return;
    }
    function caps() {
      var x = document.getElementById("prk_admin_name");
      var y = document.getElementById("super_admin_user");
      x.value = x.value.toUpperCase();
      y.value = y.value.toUpperCase();
    }
  </script>
</html>
