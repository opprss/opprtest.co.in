<?php
/*
Description: vehicle number chaker.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
 include_once 'api/parkAreaReg_api.php';
    //include_once 'api/global_api.php';
    $response = array();
    if(isAvailable(array('v_number','prk_admin_id'))){
        if(isEmpty(array('v_number','prk_admin_id'))){
            $v_number = trim($_POST['v_number']);
            $prk_admin_id = trim($_POST['prk_admin_id']);
            $prk_inf =prk_inf($prk_admin_id);
            $json = json_decode($prk_inf);
            if($json->status=='1'){
                $prk_area_short_name = $json->prk_area_short_name;
                $veh_valid_flag = $json->veh_valid_flag; 
                if ($veh_valid_flag==FLAG_Y) {
                    $response = v_numberChecker($v_number);
                }else{
                    $len = strlen($v_number);
                    if($len>=4){
                        $response['status'] =1;
                        $response['message'] = 'Welcome';
                        $response = json_encode($response); 
                    }else{
                        $response['status'] =0;
                        $response['message'] = 'Please Input Correct Vehicle Number';
                        $response = json_encode($response); 
                    }
                }
            }else{
                $response =  $prk_inf; 
            }
        }else{
                $response['status'] = 0;
                $response['message'] = 'All Fields Are Mandatory';
                $response = json_encode($response); 
        }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        $response = json_encode($response); 
    } 
echo $response;
?> 