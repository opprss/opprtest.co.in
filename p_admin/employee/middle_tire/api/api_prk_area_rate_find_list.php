<?php 
/*
Description: parking area visit find the rate. 
Developed by: Rakhal Raj Mandal
Created Date: 09-06-2018
Update date : ----------
*/
/*function prk_area_rate_find_list($prk_admin_id,$veh_type){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "select 1 as rate_id, 12 as hr, 300 as amount from dual union select 2, 24 , 500  from dual";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val) 
        {
            $response['status'] = 1;
            $response['message'] = 'Successful';
            $park['prk_rate_id'] = $val['rate_id'];
            $park['veh_rate_hour'] = $val['hr'];
            $park['veh_rate'] = $val['amount'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['rate_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}*/
function prk_area_rate_find_list($prk_admin_id,$veh_type){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT `prk_area_rate`.`prk_rate_id`,`prk_area_rate`.`veh_rate_hour`,`prk_area_rate`.`veh_rate` 
        FROM `prk_area_rate` 
        WHERE `prk_area_rate`.`prk_admin_id` ='$prk_admin_id' 
        AND `prk_area_rate`.`active_flag` ='".ACTIVE_FLAG_y."' 
        AND `prk_area_rate`.`del_flag`='".DEL_FLAG_N."' 
        AND `prk_area_rate`.`prk_vehicle_type`='$veh_type'
        AND  '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(`prk_area_rate`.`prk_rate_eff_date`, '%d-%m-%Y'), '%Y-%m-%d')    
        AND  DATE_FORMAT(STR_TO_DATE(IFNULL(`prk_area_rate`.`prk_rate_end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['prk_rate_id'] = $val['prk_rate_id'];
        $response['veh_rate_hour'] = $val['veh_rate_hour'];
        $response['veh_rate'] = $val['veh_rate'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?> 