<?php 
/*
Description: OTP Validate. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
    require_once 'api/reg_api.php';
    // require_once 'api/global_api.php';
    $response = array();
 
    if(isAvailable(array('mobile','otp','email'))){

        if(isEmpty(array('otp'))){
            $mobile = trim($_POST['mobile']);
            $otp = trim($_POST['otp']);
            $email = trim($_POST['email']);
            if(mobileChecker($mobile)=='true'){
                $respon=otpValidation($mobile,$otp);
                echo ($respon);
            }elseif (emailChecker($email)=='true'){
                $respon=otpValidation($email,$otp);
                echo ($respon);
            }
            else{
                $response['status'] = 0;
                $response['message'] = 'Mobile Number Not Valid';
                echo json_encode($response);
            }
        }else{
                $response['status'] = 0;
                $response['message'] = 'All Fields Are Mandatory';
                echo json_encode($response);
        }
    }else{
             $response['status'] = 0; 
             $response['message'] = 'Invalid API Call';
            echo json_encode($response);
     }
?>