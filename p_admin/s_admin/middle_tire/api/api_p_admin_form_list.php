<?php
/*
Description: Parking area account status check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function p_admin_form_list(){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $resp = array();
    // $sql = "SELECT * FROM `form_dtl` where `active_flag`='Y' AND `del_flag`='N' AND `form_seq`=1 AND `group_form_id`<>0";
    $sql="SELECT for1.`form_id`,
        for2.`form_display_name` as form_display_name2,
        for2.`form_order` as form_order2,
        for1.`form_order`,
        for1.`form_name`,
        for1.`form_display_name`,
        for1.`form_php_name`,
        for1.`group_form_id`,
        for1.`form_seq`,
        for1.`form_img` 
        FROM `form_dtl` for1,`form_dtl` for2  
        where for1.`active_flag`='Y' 
        AND for1.`del_flag`='N' 
        AND for1.`form_seq`=1 
        AND for1.`group_form_id`<>0
        AND for2.`form_id`=for1.`group_form_id`
        ORDER BY 3,4 ASC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $resp['form_id'] = $val['form_id'];
            $resp['form_name'] = $val['form_name'];
            $resp['form_display_name'] = $val['form_display_name'];
            $resp['form_php_name'] = $val['form_php_name'];
            $resp['form_seq'] = $val['form_seq'];
            $allplayerdata[$val['form_display_name2']][] = $resp;

            // array_push($allplayerdata, $resp);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['from_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>