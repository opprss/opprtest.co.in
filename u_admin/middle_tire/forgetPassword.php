<?php 
/*
Description: user forget password 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/  
 require_once 'api/reg_api.php';
// include_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('mobile','password','repassword'))){
 	if(isEmpty(array('repassword','mobile','password'))){

	 	$mobile = trim($_POST['mobile']);
	 	$password = trim($_POST['password']);
        $repassword = trim($_POST['repassword']);
        
        $response=forgetPassword($mobile,$password,$repassword);
        echo ($response);
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?>