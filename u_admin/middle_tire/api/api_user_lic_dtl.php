<?php
/*
Description: user licence add.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_lic_dtl($user_admin_id,$user_lic_number,$user_lic_name,$user_lic_veh_class,$user_lic_issue_date,$user_lic_issued_by,$user_lic_valid_till,$mobile,$s_d_w_of,$blood_gr,$date_of_birth,$address,$user_lic_img,$user_lic_img_status){
    global $pdoconn;
    $response = array();
    $sql = "INSERT INTO `user_lic_dtl`(`user_admin_id`,`user_lic_number`,`user_lic_name`,`user_lic_veh_class`,`user_lic_issue_date`,`user_lic_issued_by`,`user_lic_valid_till`,`inserted_by`,`inserted_date`,`s_d_w_of`,`blood_gr`,`date_of_birth`,`address`,`user_lic_img`,`user_lic_img_status`) VALUE('$user_admin_id','$user_lic_number','$user_lic_name','$user_lic_veh_class','$user_lic_issue_date','$user_lic_issued_by','$user_lic_valid_till','$mobile','".TIME."','$s_d_w_of','$blood_gr','$date_of_birth','$address','$user_lic_img','$user_lic_img_status')";
    $query  = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Add Successful';
        return json_encode($response);  
    }else{
        $response['status'] = 0;
        $response['message'] = 'Add Not Successful';
        return json_encode($response);  
    }  
    return json_encode($response); 
}
?>