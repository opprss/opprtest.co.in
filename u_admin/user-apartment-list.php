<!-- ################################################
Description: Property List.
Developed by: Rakhal Raj Mandal
Created Date: 2019-11-11
####################################################-->
<?php 
  include "all_nav/header.php"; 
  $apartment_list1 = user_apartment_list($user_admin_id);
  $apartment_list = json_decode($apartment_list1, true);
?>
<style type="text/css">
  th,td{
    text-align: center;
  }
</style>
<script src="../lib/validation/jquery.validate.js"></script>
<link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../lib/select2/css/select2.min.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Property List<?//=$apartment_list1?></h5>
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40">
      <div class="form-layout">
        <div class="row mg-b-25">
          <div class="col-md-10">
          </div>
          <div class="col-md-2 error_show pull-right">
            <div class="form-group">
                <button type="button" class="btn btn-block user_button" name="add_vehicle" id="add_vehicle" onclick="document.location.href='user-apartment';">Add Property</button>
            </div>
          </div>
        </div>
      </div>
      <div class="table-wrapper">
        <table id="datatable1" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="wd-5p">Sl No.</th>
              <th class="wd-15p">Property Name</th>
              <th class="wd-15p">Property Type</th>
              <th class="wd-10p">Configaration</th>
              <th class="wd-10p">House No</th>
              <th class="wd-10p">Furnished</th>
              <th class="wd-10p">Action</th>
            </tr>
          </thead>
          <tbody>
          <?php
            if($apartment_list['status']){
            $x=0;
            foreach($apartment_list['apartment_list'] as $val){
              $x+=1;
              $user_apar_id = $val['user_apar_id'];
              $user_apar_name = $val['user_apar_name'];
              $user_apar_address = $val['user_apar_address'];
          ?>
            <tr>
              <td><?=$x; ?></td>
              <td><?=$val['user_apar_name']; ?></td>
              <td><?=$val['user_apar_type']; ?></td>
              <td><?=$val['user_apar_configuration']; ?></td>
              <td><?=$val['user_apar_house_no']; ?></td>
              <td><?=$val['user_apar_furnished']; ?></td>
              <td>
                <a href="<?php echo 'user-add-group-apartment?list_id='.base64_encode($user_apar_id).'&list_name='.base64_encode($user_apar_name)?>" class="" data-toggle="tooltip" data-placement="top" title="Add Tenant">
                  <i class="fa fa-handshake-o" style="font-size:18px"></i>
                </a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?php echo 'user-apartment?list_id='.base64_encode($user_apar_id)?>" class="" data-toggle="tooltip" data-placement="top" title="Modify">
                  <i class="fa fa-pencil" style="font-size:18px"></i>
                </a>&nbsp;&nbsp;&nbsp;
                <button type="button" class="clean-button" name="delete" id="delete<%=count++%>" value="<?php echo $user_apar_id; ?>" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
              </td>
            </tr>
           <?php }}?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
<?php include"all_nav/footer.php"; ?>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
  var userUrl = "<?php echo USER_URL; ?>";
  var user_admin_id = "<?php echo $user_admin_id; ?>";
  var token = "<?php echo $token; ?>";
  var mobile = "<?php echo $user_mobile; ?>";
  $(document).ready(function(){
    $('button[id^="delete"]').on('click', function() {
      var user_apar_id = this.value;
      $.confirm({
        title: 'Are You Sure?',
        content: "<p style='font-size:0.8em;'>It will delete the item permanently</p>",
        theme: 'modern',
        type: 'red',
        buttons: {
          cancel: function () {
          },
          somethingElse: {
            text: 'Delete',
            btnClass: 'btn-red',
            keys: ['Y', 'shift'],
            action: function(){
              if (user_apar_id != '') {
                var urlDtl = userUrl+'user_apartment_delete.php';
                // 'user_admin_id','mobile','token','user_apar_id'
                $.ajax({
                  url :urlDtl,
                  type:'POST',
                  data :{
                    'user_admin_id':user_admin_id,
                    'mobile':mobile,
                    'token': token,
                    'user_apar_id':user_apar_id
                  },
                  dataType:'html',
                  success  :function(data){
                    // alert(data);
                    var json = $.parseJSON(data);
                    if (json.status) {
                      $.alert({
                        icon: 'fa fa-smile-o',
                        theme: 'modern',
                        title: 'Success !',
                        content: "<p style='font-size:0.8em;'>Delete successfully</p>",
                        type: 'green',
                        buttons: {
                          Ok: function () {
                            location.reload();
                              // window.location='licence';
                          }
                        }
                      });
                    }else{
                      if( typeof json.session !== 'undefined'){
                        if (!json.session) { 
                          window.location.replace("logout.php"); 
                        }                  
                      }else{                    
                        $.alert({                    
                          icon: 'fa fa-frown-o',                    
                          theme: 'modern',                    
                          title: 'Error !',                    
                          content: "Somting went wrong",                    
                          type: 'red'                  
                        });                  
                      }                
                    }
                  }
                });
              }else{
                $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
                type: 'red'
                });
              }
            }
          }
        }
      });
    });
  }); 
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
</script>