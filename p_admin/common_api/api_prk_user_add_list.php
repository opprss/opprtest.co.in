<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function prk_user_add_list($prk_admin_id){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
    $sql = "SELECT `user_address`.`user_add_id`,
        `user_address`.`user_address`,
        `user_address`.`landmark`,
        `user_address`.`user_add_area`,
        `user_address`.`user_add_city`,
        `user_address`.`user_add_state`,
        `state`.`state_name`,
        `city`.`city_name`,
        `user_address`.`user_add_country`,
        `user_address`.`user_add_pin`,
        `user_address`.`user_admin_id`,
        `user_address`.`prk_admin_id`,
        `user_address`.`tower_name` as 'tower_id',
        `user_address`.`falt_name` as 'flat_id', 
        `user_address`.`default_flag`,
        `user_address`.`user_add_verify_flag`,
        `user_address`.`inserted_date`,
        if(`tower_details`.`tower_name` IS NULL,'NA',`tower_details`.`tower_name`) as 'tower_name',
        if(`flat_details`.`flat_name` IS NULL,'NA',`flat_details`.`flat_name`) as 'falt_name',
        DATE_FORMAT(`user_address`.`inserted_date`,'%d-%m-%Y %I:%i %p') AS `in_date`,
        `user_admin`.`user_mobile`,
        `user_detail`.`user_name`,
        `user_detail`.`user_nick_name`,
        `user_detail`.`user_email`,
        IF(`user_detail`.`user_img` IS NULL or `user_detail`.`user_img` = '', (IF( user_detail.user_gender= '".FLAG_F."','".FEMALE_IMG_D."','".MALE_IMG_D."')), CONCAT('".USER_BASE_URL."',`user_detail`.`user_img`)) as `user_img`
        FROM `user_address` JOIN `state` JOIN `city` JOIN `user_admin` JOIN `user_detail` 
        LEFT JOIN `tower_details` ON `tower_details`.`tower_id`=`user_address`.`tower_name`
        AND `tower_details`.`prk_admin_id`=`user_address`.`prk_admin_id` AND `tower_details`.`active_flag`='".FLAG_Y."' 
        AND `tower_details`.`del_flag`='".FLAG_N."'
        LEFT JOIN  `flat_details` ON `flat_details`.`flat_id`=`user_address`.`falt_name` 
        AND `flat_details`.`prk_admin_id`=`user_address`.`prk_admin_id` AND `flat_details`.`active_flag`='".FLAG_Y."'           AND `flat_details`.`del_flag`='".FLAG_N."'
        WHERE `user_address`.`prk_admin_id`='$prk_admin_id' 
        AND `user_address`.`active_flag`='".FLAG_Y."'
        AND `user_address`.`del_flag`='".FLAG_N."'
        AND `state`.`active_flag`='".FLAG_Y."'
        AND `city`.`e_id`= `user_address`.`user_add_city`
        AND `state`.`e`= `user_address`.`user_add_state`
        AND `user_admin`.`user_admin_id`= `user_address`.`user_admin_id`
        AND `user_admin`.`user_ac_status`='".FLAG_A."'
        AND `user_detail`.`user_admin_id`= `user_address`.`user_admin_id`
        AND `user_detail`.`active_flag`='".FLAG_Y."'";

    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){

        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val)
        {
            $park['user_add_id'] = $val['user_add_id'];
            $park['user_address'] = $val['user_address'];
            $park['landmark'] = $val['landmark'];
            $park['user_add_city'] = $val['user_add_city'];
            $park['user_add_state'] = $val['user_add_state'];
            $park['state_name'] = $val['state_name'];
            $park['city_name'] = $val['city_name'];
            $park['user_add_country'] = $val['user_add_country'];
            $park['user_add_pin'] = $val['user_add_pin'];
            $park['prk_admin_id'] = $val['prk_admin_id'];
            $park['tower_id'] = $val['tower_id'];
            $park['tower_name'] = $val['tower_name'];
            $park['flat_id'] = $val['flat_id'];
            $park['falt_name'] = $val['falt_name'];
            $park['default_flag'] = $val['default_flag'];
            $park['user_add_verify_flag'] = $val['user_add_verify_flag'];
            $park['user_mobile'] = $val['user_mobile'];
            $park['user_name'] = $val['user_name'];
            $park['user_nick_name'] = $val['user_nick_name'];
            $park['user_email'] = $val['user_email'];
            $park['user_img'] = $val['user_img'];
            $park['in_date'] = $val['in_date'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['user_address'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>