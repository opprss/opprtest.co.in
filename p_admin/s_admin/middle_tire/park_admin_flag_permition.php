<?php
/*
Description: Parking area login.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
    //oppr_super_employee_login
    require_once 'api/parkAreaReg_api.php';
    $response = array();
    if(isAvailable(array('prient_in_flag','prient_out_flag','advance_flag','advance_pay','ad_flag','sms_flag','veh_out_otp_flag','veh_token_flag','visitor_token_flag','veh_valid_flag','parking_type','visitor_out_verify_flag','veh_out_verify_flag','employee_user_name','prk_admin_id','visitor_otp_flag','visitor_delay_time','alarm_time','bb_flag','visitor_park_flag','visitor_image_flag','resident_flag','vis_prient_in_flag','vis_prient_out_flag','office_flag','printer_name'))){
        if(isEmpty(array('prient_in_flag','prient_out_flag','advance_flag','ad_flag','sms_flag','veh_out_otp_flag','veh_token_flag','visitor_token_flag','veh_valid_flag','parking_type','visitor_out_verify_flag','veh_out_verify_flag','employee_user_name','prk_admin_id','visitor_otp_flag','bb_flag','visitor_park_flag','visitor_image_flag','resident_flag','vis_prient_in_flag','vis_prient_out_flag','office_flag','printer_name'))){
            $prient_in_flag = trim($_POST['prient_in_flag']);
            $prient_out_flag = trim($_POST['prient_out_flag']);
            $advance_flag = $_POST['advance_flag'];
            $advance_pay = trim($_POST['advance_pay']);
            $ad_flag = trim($_POST['ad_flag']);
            $sms_flag = trim($_POST['sms_flag']);
            $veh_out_otp_flag = trim($_POST['veh_out_otp_flag']);
            $veh_token_flag = trim($_POST['veh_token_flag']);
            $visitor_token_flag = trim($_POST['visitor_token_flag']);
            $veh_valid_flag = trim($_POST['veh_valid_flag']);
            $parking_type = trim($_POST['parking_type']);
            $visitor_out_verify_flag = trim($_POST['visitor_out_verify_flag']);
            $veh_out_verify_flag = trim($_POST['veh_out_verify_flag']);
            $employee_user_name = trim($_POST['employee_user_name']);
            $prk_admin_id = trim($_POST['prk_admin_id']);
            $visitor_otp_flag = trim($_POST['visitor_otp_flag']);
            $visitor_delay_time = trim($_POST['visitor_delay_time']);
            $alarm_time = trim($_POST['alarm_time']);
            $bb_flag = trim($_POST['bb_flag']);
            $visitor_park_flag = trim($_POST['visitor_park_flag']);
            $visitor_image_flag = trim($_POST['visitor_image_flag']);
            $resident_flag = trim($_POST['resident_flag']);
            $vis_prient_in_flag = trim($_POST['vis_prient_in_flag']);
            $vis_prient_out_flag = trim($_POST['vis_prient_out_flag']);
            $office_flag = trim($_POST['office_flag']);
            $printer_name = trim($_POST['printer_name']);

            $response = park_admin_flag_permition($prient_in_flag,$prient_out_flag,$advance_flag,$advance_pay,$ad_flag,$sms_flag,$veh_out_otp_flag,$veh_token_flag,$visitor_token_flag,$veh_valid_flag,$parking_type,$visitor_out_verify_flag,$veh_out_verify_flag,$employee_user_name,$prk_admin_id,$visitor_otp_flag,$visitor_delay_time,$alarm_time,$bb_flag,$visitor_park_flag,$visitor_image_flag,$resident_flag,$vis_prient_in_flag,$vis_prient_out_flag,$office_flag,$printer_name);
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandator';
            $response = json_encode($response);
        }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        $response = json_encode($response);
    }
    echo $response;
?>