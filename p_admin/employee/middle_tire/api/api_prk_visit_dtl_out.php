<?php 
/*
Description: parking area visit the people out. 
Developed by: Rakhal Raj Mandal
Created Date: 09-04-2018
Update date : 27-05-2018
*/
function prk_visit_dtl_out($prk_visit_dtl_id,$prk_user_username,$prk_area_gate_id,$prk_area_user_id,$remarks){
    global $pdoconn;
    $who_veh_out = FLAG_P;
    $response = array();
    $sql = "SELECT `visit_name`,`prk_admin_id`,`visit_with_veh`,`veh_type`,`veh_number`,`prk_veh_trc_dtl_id` FROM `prk_visit_dtl` WHERE `prk_visit_dtl_id` ='$prk_visit_dtl_id' AND `end_date` IS NULL";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $prk_admin_id = $val['prk_admin_id'];
    $visit_with_veh = $val['visit_with_veh'];
    $veh_type = $val['veh_type'];
    $veh_number = $val['veh_number']; 
    $prk_veh_trc_dtl_id = $val['prk_veh_trc_dtl_id'];
    $sql = "UPDATE `prk_visit_dtl` SET `end_date`='".TIME."',`remarks`='$remarks',`updated_by`= '$prk_user_username',`updated_date`='".TIME."' WHERE `prk_visit_dtl_id`='$prk_visit_dtl_id'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['visit_with_veh'] = $visit_with_veh;
        $response['session'] = 1;
        $response['message'] = 'Visitor OUT Sucessful';
        $response = json_encode($response);
    }else{
        $response['status'] = 0;
        $response['session'] = 1;
        $response['message'] = 'Visitor OUT not Sucessful';
        $response = json_encode($response);
    }

    if($visit_with_veh == VISIT_WITH_VEH_Y){
        $prk_inf =prk_inf($prk_admin_id);
        $json = json_decode($prk_inf);
        if($json->status=='1'){
            $prk_admin_name = $json->prk_admin_name;
            $advance_flag = $json->advance_flag;
            $prient_in_flag = $json->prient_in_flag;
            $prient_out_flag = $json->prient_out_flag;
            $sms_flag = $json->sms_flag;
            $prk_area_name = $json->prk_area_name;
            $prk_area_short_name = $json->prk_area_short_name; 

        $rate_cal = rate_calculate($prk_veh_trc_dtl_id,$prk_user_username,$prk_admin_id,$prk_area_gate_id,$who_veh_out);
        $json = json_decode($rate_cal);
        if($json->status=='1'){
            $payment_dtl_id = $json->payment_dtl_id;
            $payment_type = TDGP;
            payment_receive($payment_dtl_id,$prk_user_username,$payment_type);
                //$response = $veh_out_dtl;
        }
        }else{
            // $response['status'] = 0; 
            // $response['message'] = 'Transition Failed';
            // $response = json_encode($response);
        }
    }
    return $response;
}
?> 