<?php 
/*
Description: user vehicle in and gate pass match. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function visitor_mobile_search($prk_admin_id,$visitor_mobile){
    $allplayerdata = array();
    global $pdoconn;
    $sql="SELECT *
        FROM(SELECT vgpd.visitor_mobile as list_mobile,
        'V' as list_type,
        vgpd.prk_admin_id 
        FROM visitor_gate_pass_dtl vgpd
        WHERE vgpd.active_flag='".FLAG_Y."' 
        AND vgpd.del_flag='".FLAG_N."'
        UNION
        SELECT ugl.guest_mobile as list_mobile,
        'G' as list_type,
        ugl.prk_admin_id
        FROM user_guest_list ugl
        WHERE ugl.active_flag='".FLAG_Y."'
        AND ugl.del_flag='".FLAG_N."'
        AND ugl.visitor_in_complete='".FLAG_N."'
        AND ugl.in_date='".TIME_TRN_WEB."'
        UNION
        SELECT pvdm.visit_mobile  as list_mobile,
        'O' as list_type,
        '$prk_admin_id' as prk_admin_id
        FROM prk_visit_dtl_master pvdm 
        WHERE active_flag='".FLAG_Y."'
        AND del_flag='".FLAG_N."') vl 
        WHERE vl.prk_admin_id='$prk_admin_id'
        AND vl.list_mobile  LIKE '%".$visitor_mobile."%'
        -- GROUP BY vl.list_mobile
        LIMIT 10";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['list_mobile'] = $val['list_mobile'];
            $park['list_type'] = $val['list_type'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['visitor_mobile_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>