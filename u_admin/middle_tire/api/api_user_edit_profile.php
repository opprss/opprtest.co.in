<?php
/*
Description: user edit profile.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_edit_profile($user_admin_id,$user_name,$user_nick_name,$user_gender,$user_dob,$user_email,$user_email_verify,$user_aadhar_id,$mobile){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT `user_img` FROM `user_detail` WHERE `active_flag`='".FLAG_Y."' AND `user_admin_id`='$user_admin_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $user_img= $val['user_img'];
    $sql ="UPDATE `user_detail` SET `active_flag`='".FLAG_N."', `updated_date`='".TIME."', `updated_by`='$mobile' WHERE `user_admin_id`='$user_admin_id' AND `active_flag`='".FLAG_Y."'";
        $query = $pdoconn->prepare($sql);
        if($query->execute()){
            $sql = "INSERT INTO `user_detail`(`user_name`,`user_nick_name`,`user_gender`,`user_dob`,`user_email`,`user_admin_id`,`inserted_by`,`user_email_verify`,`user_aadhar_id`,`user_img`,`inserted_date`) VALUE ('$user_name','$user_nick_name','$user_gender','$user_dob','$user_email','$user_admin_id','$mobile','$user_email_verify','$user_aadhar_id','$user_img','".TIME."')";
                $query = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['name'] = $user_nick_name;
                $response['user_email'] = $user_email;
                $response['message'] = 'Update Successfull';
                return json_encode($response);
            }
        }
}
?>