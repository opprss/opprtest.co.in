<?php
/*
Description: parkin area gate pass issue mes.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function user_gate_pass_mess($mobile,$gate_pass,$veh_type,$veh_number,$prk_admin_name){
    global $pdoconn;
    $result=array();

    $mobile_no=$mobile;
    //$text="Hi User, Register mobile no ".$mobile.". Issued gate pass by ".$prk_admin_name.", Digital Gate Pass number ".$gate_pass.".Thanks www.opprss.com";
    $text = "Dear User, Registered mobile# ".$mobile.". ".$prk_admin_name." issued gate pass for your vehicle ".$veh_number." &Digital Gate Pass # ".$gate_pass.". For more details please visit www.opprss.com";
    mobile_massage($mobile_no,$text);

    $response['status'] = 1;
    $response['message'] = 'Sms Send Sucessfully';
    return json_encode($response);
}
?>