<?php
/*
Description: Parking visitor gate pass issue.
Developed by: Rakhal Raj Mandal
Created Date: 31-10-2018
Update date :31-10-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
$prk_admin_id = trim($_POST["prk_admin_id"]); 
$inserted_by = trim($_POST["parking_admin_name"]); 
$flat_master_id = trim($_POST["flat_master_id"]); 
$total_space = empty(trim($_POST["total_space"]))?'0':trim($_POST["total_space"]); 
$sql = "UPDATE `prk_lot_dtl` SET `updated_by`='$inserted_by',`updated_date`='".TIME."',`active_flag`='".FLAG_N."' WHERE `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."' AND `flat_master_id`='$flat_master_id' AND `prk_admin_id`='$prk_admin_id'";
$query  = $pdoconn->prepare($sql);
$query->execute();
if($total_space > 0){ 
    $number = count($_POST["prk_lot_number"]); 
    if($number > 0){ 
        for($i=0; $i<$number; $i++) {  
            $prk_lot_dtl=$_POST["prk_lot_dtl"][$i];
            $prk_lot_dtl_id=$_POST["prk_lot_dtl_id"][$i];
            $prk_lot_number=$_POST["prk_lot_number"][$i];
            $response = prk_lot_dtl_add($prk_admin_id,$flat_master_id,$prk_lot_dtl,$prk_lot_dtl_id,$prk_lot_number,$inserted_by);
        }                    
        $response['status'] = 1;
        $response['message'] = 'Successful';
        // $response['number'] = $number;
        $response = json_encode($response);   
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Empty Space';
    $response = json_encode($response);
}
echo $response;
// print_r($_POST);
?>