<?php 
/*
Description: duplicate vehicle payment history list 
Developed by: Rakhal Raj Mandal
Created Date: 09-06-2018
Update date :----------
*/ 
function duplicate_payment_dtl_list($prk_admin_id,$veh_number,$start_date,$end_date){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    if(!empty($start_date) && !empty($end_date)) { 
	    $format = "Y-m-d";
	    if(date($format, strtotime($start_date)) == date($start_date) && date($format, strtotime($end_date)) == date($end_date)) {
	        $start_date= $start_date;
	        $end_date=  $end_date ;
	    } else {
	        $start_date = date("Y-m-d", strtotime($start_date));
	        $end_date = date("Y-m-d", strtotime($end_date));
	    }
	}else{
        $end_date= TIME_TRN;
        $start_date= TIME_TRN;
    }
	$sql = "Select pd.veh_number,
		pd.vehicle_type,
		vt.vehicle_type_dec,
		pd.payment_type as payment_type_sort,
		 (CASE pd.payment_type WHEN '".CASH."' ThEN '".CASH."' WHEN '".WALLET."' THEN '".WALLET."'
		  WHEN '".TDGP."' THEN '".TDGP_F."' WHEN '".DGP."' THEN '".DGP_F."' WHEN '".PP."' THEN '".CASH."' end) as 'payment_type',
		pd.payment_dtl_id,
		pt.prk_veh_trc_dtl_number,
		DATE_FORMAT(pt.prk_veh_in_time,'%d-%b-%Y') AS `in_date`,
		DATE_FORMAT(pt.prk_veh_in_time,'%I:%i %p') AS `in_time`,
		pd.total_pay,
		pd.payment_rec_date,
		DATE_FORMAT(pd.payment_rec_date,'%d-%b-%Y') AS `out_date`,
		DATE_FORMAT(pd.payment_rec_date,'%I:%i %p') AS `out_time`,
		'O' as in_out_flag,
		vt.vehicle_typ_img 
		from payment_dtl pd,prk_veh_trc_dtl pt,vehicle_type vt 
		where pd.prk_veh_trc_dtl_id = pt.prk_veh_trc_dtl_id
		AND pd.pay_dtl_del_flag ='".FLAG_N."'
		AND pd.vehicle_type = vt.vehicle_sort_nm
		AND vt.active_flag = '".FLAG_Y."'
		AND IF('$veh_number' IS NULL OR '$veh_number'='',pd.veh_number,'$veh_number')=pd.veh_number
		AND pd.prk_admin_id = '$prk_admin_id'
		AND DATE_FORMAT(pd.payment_rec_date,'%Y-%m-%d') BETWEEN '$start_date' AND '$end_date'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
	    $arr_catagory = $query->fetchAll();
	    foreach($arr_catagory as $val){
	        $park['veh_number'] = $val['veh_number'];
	        $park['prk_veh_type'] = $val['vehicle_type'];
	        $park['vehicle_type_dec'] = $val['vehicle_type_dec'];
	        $park['payment_type'] = $val['payment_type'];
	        $park['payment_dtl_id'] = $val['payment_dtl_id'];
	        $park['payment_receive_number'] = $val['prk_veh_trc_dtl_number'];
	        $park['payment_amount'] = $val['total_pay'];
	        $park['inserted_date'] = $val['payment_rec_date'];
	        $park['in_out_flag'] = $val['in_out_flag'];
	        $park['vehicle_typ_img'] = $val['vehicle_typ_img'];
	        $park['in_date'] = $val['in_date'];
	        $park['in_time'] = $val['in_time'];
	        $park['out_date'] = $val['out_date'];
	        $park['out_time'] = $val['out_time'];
	        array_push($allplayerdata, $park);
	    }
        $response['status'] = 1;
        $response['message'] = 'Successful';
	    $response['parking'] = $allplayerdata;
	}else{
		$response['status'] = 0;
	    $response['message'] = 'List Empty';
	}
    return json_encode($response);
}
?>