<?php 
/*  Description:  Apartment Add.
    Developed by: Rakhal Raj Mandal
    Created Date: 06-10-2019
    Update date : 06-10-2019
*/ 
include_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_admin_id','mobile','token','user_relation_id'))){
    if(isEmpty(array('user_admin_id','mobile','token','user_relation_id'))){
        $user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
        $token = trim($_POST['token']);
        $user_relation_id = trim($_POST['user_relation_id']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            // api_user_to_user_relation_dtl
            $response = user_to_user_relation_transaction_list($user_admin_id,$user_relation_id);
        }else{
            #..
            $response= $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response= json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response= json_encode($response);
}
echo ($response);
?>