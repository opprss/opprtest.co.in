<?php
/*
Description: User Registration.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
 require_once 'api/reg_api.php';
// include_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('mobile','password','name','t_c_flag'))){
 	if(isEmpty(array('mobile','password','name','t_c_flag'))){
      $device_token_id = isset($_POST['device_token_id']) ? $_POST['device_token_id'] : '';
		$device_type = isset($_POST['device_type']) ? $_POST['device_type'] : 'OTH';
	 	$mobile = trim($_POST['mobile']);
	 	$password = trim($_POST['password']);
        $name = trim($_POST['name']);
	 	$t_c_flag = trim($_POST['t_c_flag']);
        
        $respon=registration($mobile,$password,$name,$t_c_flag,$device_token_id,$device_type);
        echo ($respon);
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 } 
?>