<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','prk_area_gate_name','prk_area_gate_dec','prk_area_gate_landmark','prk_area_gate_type','effective_date','end_date','user_name','token'))){
 	if(isEmpty(array('prk_admin_id','prk_area_gate_name','prk_area_gate_dec','prk_area_gate_type','effective_date','user_name','token'))){
	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_area_gate_name = trim($_POST['prk_area_gate_name']);
	 	$prk_area_gate_dec = trim($_POST['prk_area_gate_dec']);
        $prk_area_gate_landmark = trim($_POST['prk_area_gate_landmark']);
	 	$prk_area_gate_type = trim($_POST['prk_area_gate_type']);
        $effective_date = trim($_POST['effective_date']);
        $end_date = trim($_POST['end_date']);
        $user_name = trim($_POST['user_name']);
        $token = trim($_POST['token']);
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            $sql = "SELECT * FROM `prk_area_gate_dtl` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";         
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count<=0){

                prk_area_gate_dtl($prk_admin_id,'SKIP GATE','SKIP GATE','',$effective_date,FUTURE_DATE_WEB,FLAG_S,$user_name);
            }
            $respon=prk_area_gate_dtl($prk_admin_id,$prk_area_gate_name,$prk_area_gate_dec,$prk_area_gate_landmark,$effective_date,$end_date,$prk_area_gate_type,$user_name);
            echo ($respon);
        }else{
            $response = $resp;
            echo $response;
        }
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        echo json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response);
}
?>