<!-- ################################################
Description: User Address verify
Developed by: Rakhal Raj Mandal
Created Date: 27-08-2019
####################################################-->
<?php 
  include "all_nav/header.php";
  $list_id = isset($_REQUEST['list_id']) ? trim($_REQUEST['list_id']) : ''; 
  $prk_gate_pass_id = base64_decode($list_id);
  $tower_lis = tower_list($prk_admin_id);
  $tower_list = json_decode($tower_lis, true);
  $category_type='APT_FLT_HLD';
  $prk_user_emp_cat = common_drop_down_list($category_type);
  $prk_user_emp_category = json_decode($prk_user_emp_cat, true);
  $prk_inf_dtl = prk_inf($prk_admin_id);
  $prk_inf_dtll = json_decode($prk_inf_dtl, true);
  $parking_type = $prk_inf_dtll['parking_type'];
  $response_show = prk_gate_pass_dtl_show($prk_gate_pass_id);
  if (!empty($response_show)) {
    $prk_gate_pass_id = $response_show['prk_gate_pass_id']; 
    $user_admin_id = $response_show['user_admin_id'];
    $veh_type = $response_show['veh_type'];
    $vehicle_type_dec = $response_show['vehicle_type_dec'];
    $veh_number = $response_show['veh_number'];
    $tower_id = $response_show['tower_id'];
    $flat_id = $response_show['flat_id'];
    $flat_name = $response_show['flat_name'];
    $tower_name = $response_show['tower_name'];
    $living_status = $response_show['living_status'];
    $park_lot_no = $response_show['park_lot_no'];
    $sub_unit_name = $response_show['prk_sub_unit_name'];
    $prk_sub_unit_id = $response_show['prk_sub_unit_id'];
    $veh_owner_name = $response_show['veh_owner_name'];
    $mobile = $response_show['mobile'];
    $prk_gate_pass_num =$response_show['prk_gate_pass_num'];
    $effective_date = $response_show['effective_date'];
    $end_date = $response_show['end_date'];
  }
?>
<style type="text/css">
  .size{
    font-size: 11px;
    text-transform: uppercase;
  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
  .error_size,.error{
    font-size: 11px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
</style> 
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Digital Gate Pass</h5>
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40 col-md-12">
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
      <div class="row">
        <div class="col-md-12">
          <form id="addGate" method="get" action="">
            <div class="form-layout">
              <div class="row mg-b-25">
                <div class="col-lg-3 error_show">
                  <div class="form-group">
                    <label class="form-control-label size">VEHICLE TYPE <span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="veh_type" id="veh_type" style="font-size: 14px; opacity: 0.8;">
                          <option value="">Select Type</option>
                          <?php
                              $response = array();
                              $response =vehicleType();
                              $count = count($response);

                              for ($i=0; $i < $count ; $i++) { 
                             ?>
                              <option value="<?php echo $response[$i]['vehicle_Sort_nm']?>"><?php echo $response[$i]['vehicle_type']?></option>

                              <?php
                              }
                          ?>
                      </select>
                    </div>
                    <span style="position: absolute;" id="error_user_veh_type" class="error_size"></span>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="form-control-label size">VEHICLE NUMBER <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Vehicle Number" name="v_number" id="veh_number" onkeyup="vehNoUppercase();" maxlength="10">
                    <span style="position: absolute;" id="error_veh_number" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-3 error_show">
                  <div class="form-group">
                    <label class="form-control-label size">SUB UNIT LIST <span class="tx-danger">*</span></label>
                      <div class="select">
                        <select name="sub_unit_name" id="sub_unit_name" style="opacity: 0.8;font-size:14px">
                            <option value="">Select Type</option>
                            <?php
                                $response = array();
                                $response =prk_sub_unit_dtl_list($prk_admin_id);
                                //s$response_count = json_decode($response, true);
                                $count = count($response);
                                if ($count == 0) {
                            ?>
                                <option value="NA">NA</option>
                            <?php     
                                }else{
                                  foreach($response as $value){ 
                               ?>
                                  <option value="<?php echo $value['prk_sub_unit_id']?>"><?php echo $value['prk_sub_unit_name']?></option>
                                <?php
                              }  }
                            ?>
                        </select>
                      </div>
                      <span style="position: absolute;" id="error_sub_unit_name" class="error_size"></span>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="form-control-label size">GATE PASS NUMBER <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control readonly" placeholder="Gate Pass Number" name="prk_gate_pass_num" id="prk_gate_pass_num" readonly="readonly" style="background-color: transparent;">
                    <span style="position: absolute;" id="error_prk_gate_pass_num" class="error_size"></span>
                  </div>
                </div>
                <div class=" row col-md-6" id="for_residential">
                  <div class="col-lg-4">
                    <div class="form-group">
                      <label class="form-control-label size">TOWER NAME <span class="tx-danger">*</span></label>
                      <div class="select">
                        <select name="tower_name" id="tower_name" style="opacity: 0.8;font-size:14px">
                          <option value="">Select Tower</option>
                          <?php if($tower_list['status']){
                            foreach($tower_list['tower_list'] as $value){?>
                            <option value="<?=$value['tower_id'];?>"><?=$value['tower_name'];?></option>
                          <?php }}?>
                        </select>
                      </div>
                    <span style="position: absolute; display: none;" id="error_tower_name" class="error">This field is required.</span>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <label class="form-control-label size">FLAT NAME <span class="tx-danger">*</span></label>
                      <div class="select">
                        <select name="flat_name" id="flat_name" style="opacity: 0.8;font-size:14px">
                          <option value="">Select Flat</option>
                        </select>
                      </div>
                    <span style="position: absolute; display: none;" id="error_flat_name" class="error">This field is required.</span>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <label class="form-control-label size">LIVING STATUS<span class="tx-danger">*</span></label>
                      <div class="select">
                        <select name="living_status" id="living_status" style="opacity: 0.8;font-size:14px">
                          <option value="">Select Living</option>
                            <?php
                            foreach ($prk_user_emp_category['drop_down'] as $val){ 
                              if($val['sort_name']!='B'& $val['sort_name']!='V'){?>  
                              <option value="<?php echo $val['sort_name']?>"><?php echo $val['full_name']?></option>
                            <?php          
                            }}
                          ?>
                        </select>
                      </div>
                    <span style="position: absolute; display: none;" id="error_living_status" class="error">This field is required.</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label class="form-control-label size">VEHICLE OWNER NAME <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Owner Name" name="veh_owner_name" id="veh_owner_name" onkeydown="upperCase(this)">
                    <span style="position: absolute;" id="error_veh_owner_name" class="error_size"></span>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label class="form-control-label size">MOBILE NUMBER <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Mobile Number" name="mobile" id="mobile" maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                    <span style="position: absolute;" id="error_mobile" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2" id="park_lot_id">
                  <div class="form-group">
                    <label class="form-control-label size">Parking Lot No.<span class="tx-danger"></span></label>
                    <input type="text" name="park_lot_no" id="park_lot_no" placeholder="Parking Lot No." class="form-control prevent_readonly" readonly="readonly">
                  <span style="position: absolute; display: none;" id="error_living_status" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-2 error_show">
                  <div class="form-group">
                    <label class="form-control-label size">EFFECTIVE DATE <span class="tx-danger">*</span></label>
                    <input type="text" name="effective_date" id="effective_date" placeholder="Effective Date" class="form-control prevent_readonly" readonly="readonly">
                    <span style="position: absolute;" id="error_effective_date" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2 error_show">
                  <div class="form-group">
                    <label class="form-control-label size">END DATE</label>
                    <input type="text" name="end_date" id="end_date" placeholder="End Date" class="form-control prevent_readonly" readonly="readonly">
                    <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2 mg-t-30">
                  <div class="form-group">
                    <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
                  </div>
                </div>
                <div class="col-lg-2 mg-t-30">
                  <div class="form-group">
                    <button type="button" class="btn btn-block prk_button_skip" name="skip" id="skip" onclick="document.location.href='vehicle-gate-pass';">Back</button>
                  </div>
                </div>
              </div>
          </form>
        </div>
      </div>
    </div>
    </div>
  </div>
</form>
<?php include"all_nav/footer.php"; ?>
<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  var prk_gate_pass_id = "<?php echo $prk_gate_pass_id;?>";
  $('#veh_type').val('<?=$veh_type?>').prop('disabled', true);
  $('#veh_number').val('<?=$veh_number?>').prop('disabled', true);
  $('#sub_unit_name').val('<?=$prk_sub_unit_id?>').prop('disabled', true);
  $('#prk_gate_pass_num').val('<?=$prk_gate_pass_num?>').prop('disabled', true);
  $('#tower_name').val('<?=$tower_id?>').prop('disabled', true);
  // $('#flat_name').val('<?=$flat_id?>');
  var flat_id='<?=$flat_id?>';
  var flat_name='<?=$flat_name?>';
  if(flat_id!=''){
    var areaOption = '<option value="' +flat_id+ '">'+flat_name+'</option>'
    $("#flat_name").html(areaOption).prop('disabled', true);
  }
  $('#living_status').val('<?=$living_status?>').prop('disabled', true);
  $('#park_lot_no').val('<?=$park_lot_no?>').prop('disabled', true);
  /*var park_lot_no='<?=$park_lot_no?>';
  if(park_lot_no!=''){
    var areaLotno = '<option value="' + park_lot_no + '">' + park_lot_no + '</option>';
    $("#park_lot_no").html(areaLotno).prop('disabled', true);
  }*/
  $('#veh_owner_name').val('<?=$veh_owner_name?>').prop('disabled', true);
  $('#mobile').val('<?=$mobile?>').prop('disabled', true);
  $('#effective_date').val('<?=$effective_date?>').prop('disabled', true);
  var end_date_lod = ('<?=$end_date?>'==FUTURE_DATE)?'':'<?=$end_date?>';
  $('#end_date').val(end_date_lod);

  if (parking_type=='R') {
    $("#for_residential").show();
    $("#park_lot_id").show();
  }else{
    $("#for_residential").hide();
    $("#park_lot_id").hide();
  }
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#effective_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
        if (date < now) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        $('#error_effective_date').text('');
        return {};
      } 
    });
  });
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#effective_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
        if (date < now_end) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        return {};
      } 
    });
  });
  $('#effective_date').focus(function(){
    $('#end_date').val('');
  });
</script>
<script type="text/javascript">
  $("#veh_owner_name").keypress(function(event){
    var inputValue = event.charCode;
    // if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
    if(!(inputValue >= 60) && (inputValue != 0 & inputValue !=32)){
    // if(!(inputValue >= 97) && (inputValue != 46)){
        event.preventDefault();
    }
  });
  /*$("#mobile").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0)){
      event.preventDefault();
    }
  });*/
  function upperCase(a){
    setTimeout(function(){
      a.value = a.value.toUpperCase();
    }, 1);
  }
  function gatePassUppercase() {
    var x = document.getElementById("user_gate_pass_num");
    x.value = x.value.toUpperCase();
  }
  function vehNoUppercase() {
    var x = document.getElementById("veh_number");
    x.value = x.value.toUpperCase();
  }
</script>
<script type="text/javascript">
  $('#tower_name').on("change", function() {
    var id=($("#tower_name").val());
    var urlCity = pkrEmpUrl+'flat_list.php';
    // 'prk_admin_id','prk_area_user_id','prk_user_username','token','tower_id'
    $.ajax ({
      type: 'POST',
      url: urlCity,
      data: {
        prk_admin_id:prk_admin_id,
        prk_area_user_id:prk_area_user_id,
        prk_user_username:prk_user_username,
        token:token,
        tower_id:id
      },
      success : function(data) {
        // alert(data);
        var obj=JSON.parse(data);
        var areaOption = "<option value=''>Select Flat</option>";
        if (obj['status']) {
          $.each(obj['flat_list'], function(val, key) {
            areaOption += '<option value="' + key['flat_id'] + '">' + key['flat_name'] + '</option>'
          });
        }
        $("#flat_name").html(areaOption);
      }
    });
  });
  $('#flat_name').on("change", function() {
    var tower_id=($("#tower_name").val());
    var tower_id=($("#tower_name").val());
    var flat_id=($("#flat_name").val());
    var urlCity = pkrEmpUrl+'flat_master.php';
    // 'prk_admin_id','prk_area_user_id','prk_user_username','token','tower_id','flat_id'
    $.ajax ({
      type: 'POST',
      url: urlCity,
      data: {
        'prk_admin_id':prk_admin_id,
        'prk_area_user_id':prk_area_user_id,
        'prk_user_username':prk_user_username,
        'token':token,
        'tower_id':tower_id,
        'flat_id':flat_id
      },
      success : function(data) {
        // alert(data);
        var json = $.parseJSON(data);
        if (json.status) {
          // alert(json.owner_mobile);
          var flat_master_id = json.flat_master_id;
          $('#living_status').val(json.living_status).prop('disabled', true);
          if (json.living_status=='O') {
            $('#veh_owner_name').val(json.owner_name).prop('disabled', true);
            $('#mobile').val(json.owner_mobile).prop('disabled', true);
          }else{
            $('#veh_owner_name').val(json.tenant_name).prop('disabled', true);
            $('#mobile').val(json.tenant_mobile).prop('disabled', true);
          }
          var flat_prk_lot_list = pkrEmpUrl+'flat_prk_lot_list.php';
          // 'prk_admin_id','prk_area_user_id','token','flat_master_id'
          $.ajax ({
            type: 'POST',
            url: flat_prk_lot_list,
            data: {
              'prk_admin_id':prk_admin_id,
              'prk_area_user_id':prk_area_user_id,
              'token':token,
              'flat_master_id':flat_master_id
            },
            success : function(data) {
              var obj=JSON.parse(data);
              var areaOption = "<option value=''>Select Lot No</option>";
              if (obj['status']) {
                $.each(obj['prk_lot_list'], function(val, key) {
                  areaOption += '<option value="' + key['prk_lot_number'] + '">' + key['prk_lot_number'] + '</option>';
                });
              }
              $("#park_lot_no").html(areaOption);
            }
          });
        }else{
          $.alert({
            icon: 'fa fa-frown-o',
            theme: 'modern',
            title: 'Error !',
            content: "<p style='font-size:0.8em;'>No Person Is There In The Flat</p>",
            type: 'red'
          });
          $('#living_status').val('');
          $('#veh_owner_name').val('');
          $('#mobile').val('');
        }
      }
    });
  });
  $( '#sub_unit_name').on("change", function() {
    var prk_sub_unit_id = $('#sub_unit_name').val();
    var urlGatePassGenerate = pkrEmpUrl+'gate_pass_generate.php';
    // 'prk_admin_id','prk_area_user_id','token','prk_sub_unit_id'
    $.ajax({
      url :urlGatePassGenerate,
      type:'POST',
      data :{
        'prk_admin_id':prk_admin_id,
        'prk_area_user_id':prk_area_user_id,
        'token': token,
        'prk_sub_unit_id':prk_sub_unit_id
      },
      dataType:'html',
      success  :function(data){
        //alert(data);
        var json = $.parseJSON(data);
        if (json.status) {
          $("#prk_gate_pass_num").val(json.gate_pass);
        }else{
          if( typeof json.session !== 'undefined'){
            if (!json.session) {
              window.location.replace("logout.php");
            }
          }else{
            $('#sub_unit_name').val('');
            $('#prk_gate_pass_num').val('');

            $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>First you have to select SUB UNIT to generate DIGITAL GATE PASS </p>",
              type: 'red'
            });
          }
        }
      }
    });
  });
  $('#save').click(function(){
    // var veh_type = $('#veh_type').val();
    // var prk_sub_unit_id =$('#sub_unit_name').val();
    // var veh_number = $('#veh_number').val();
    var veh_owner_name = $('#veh_owner_name').val();
    // var mobile = $('#mobile').val();
    var end_date = ($('#end_date').val()!= '') ? $('#end_date').val() : FUTURE_DATE;
    // var effective_date = $('#effective_date').val();
    // var prk_gate_pass_num = $('#prk_gate_pass_num').val();
    // var tower_id = $('#tower_name').val();
    // var flat_id = $('#flat_name').val();
    // var living_status = $('#living_status').val();
    // var park_lot_no = $('#park_lot_no').val();
    if($("#addGate").valid()){
      // alert('Ok');
      $('#save').val('Wait ...').prop('disabled', true);
      var urlPrkAreGaGatePassDtl = pkrEmpUrl+'prk_gate_pass_dtl_update.php';
      // 'prk_admin_id','prk_area_user_id','prk_user_username','token','prk_gate_pass_id','end_date','veh_owner_name'
      $.ajax({
        url :urlPrkAreGaGatePassDtl,
        type:'POST',
        data :
        {
          'prk_admin_id':prk_admin_id,
          'prk_area_user_id':prk_area_user_id,
          'prk_user_username':prk_user_username,
          'token': token,
          'prk_gate_pass_id':prk_gate_pass_id,
          'end_date':end_date,
          'veh_owner_name':veh_owner_name
        },
        dataType:'html',
        success  :function(data){
          $('#save').val('SAVE').prop('disabled', false);
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>Gate pass Modify successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                  window.location='vehicle-gate-pass';
                }
              }
            });
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
              type: 'red'
            });
            }
          }
        }
      });
    }
  });
</script>
<script type="text/javascript">
  $( document ).ready( function () {  
    var urlVehNoCheck = prkUrl+'v_numberChecker.php';
    $( "#addGate" ).validate( {
      rules: {
        veh_type: "required",
        // veh_number: "required",
        v_number: {
          required: true,
          remote: {
            url: urlVehNoCheck,
            type: "POST",
            data: {},
            cache: false,
            dataType: "json",
            dataFilter: function(data) {
              // alert(data);
              var json = $.parseJSON(data);                      
              if (json.status) {
                  return true;
              } else {
                  return false;
              }
            }
          }
        },
        sub_unit_name: "required",
        tower_name: "required",
        flat_name: "required",
        living_status: "required",
        park_lot_no: "required",
        veh_owner_name: "required",
        mobile: {
          required: true,
          number: true,
          minlength: 10
        },
        effective_date: "required"
      },
      messages: {
        v_number: {
          required: "This field is required.",
          remote: "Vehicle number is invalid."
        },
        mobile: {
          required: "This field is required.",
          number: "Enter valid mobile Numberr.",
          minlength: "Enter valid mobile Number."
        }
      }
    });
  });
</script>