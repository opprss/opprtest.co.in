<?php 
/*
Description: vehicle out all details show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
    // require_once 'api/global_api.php';
    require_once 'api/parkAreaReg_api.php';
    $response = array();
    if(isAvailable(array('payment_dtl_id'))){
        if(isEmpty(array('payment_dtl_id'))){

            $payment_dtl_id = trim($_POST['payment_dtl_id']);
            $response=prk_veh_out_dtl($payment_dtl_id);
            echo ($response);

        }else{
                $response['status'] = 0;
                $response['message'] = 'All Fields Are Mandatory';
                echo json_encode($response); 
         }
     }else{
         $response['status'] = 0; 
         $response['message'] = 'Invalid API Call';
         echo json_encode($response); 
     } 
?>
