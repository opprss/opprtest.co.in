<?php
/*
Description: Parking area in all vehicle.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
    // require_once 'api/global_api.php';
    require_once 'api/parkAreaReg_api.php';
    $response = array();
    if(isAvailable(array('prk_admin_id'))){
        if(isEmpty(array('prk_admin_id'))){            
            $prk_admin_id = trim($_POST['prk_admin_id']);
            $response=prk_veh_trc_dtl_list($prk_admin_id);
            echo ($response);
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response); 
         }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        echo json_encode($response); 
    } 
?>