<?php 
/*
Description: vehicle out full status send user mobile.
Developed by: Rakhal Raj Mandal
Created Date: 06-04-2018
Update date : 06-04-2018
*/ 
function prk_veh_out_dtl_sms($user_mobile,$prk_area_name,$veh_in_date,$veh_in_time,$veh_out_date,$veh_out_time,$total_pay,$tot_hr,$round_hr){
   
    $mobile_no=$user_mobile;
    $text="Dear User,
	Parking Area: 
	".$prk_area_name." . In Time: ".$veh_in_date." ".$veh_in_time." Out Time: ".$veh_out_date." ".$veh_out_time." Total Time: ".$tot_hr."  Round-Of Hour: ".$round_hr."  Total Pay: ".$total_pay." Thanks Visit Again For more details please visit ".SMS_URL.".";
    mobile_massage($mobile_no,$text);

    $response['status'] = 1;
    $response['message'] = 'SMS Send Sucessfully';
    return json_encode($response);
}
?>