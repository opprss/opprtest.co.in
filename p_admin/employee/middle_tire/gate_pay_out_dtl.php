<?php 
/*
Description: Vehicle out parkin employ all details show .
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
// require_once 'api/global_api.php';
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('payment_dtl_id','prk_admin_id','prk_area_user_id','token'))){
    if(isEmpty(array('payment_dtl_id','prk_admin_id','prk_area_user_id','token'))){
        $payment_dtl_id = trim($_POST['payment_dtl_id']);
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $token = trim($_POST['token']);
        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $sql = "SELECT `payment_dtl_id` FROM `payment_dtl` WHERE `payment_rec_status`='".payment_rec_status_I."' AND `payment_dtl_id`='$payment_dtl_id'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $response=prk_veh_out_dtl($payment_dtl_id);
                echo ($response);
            }else{
                $response['status'] = 0;
                $response['message'] = 'Payment Already Received';
                echo json_encode($response);
            }
        }else{
            $response['status'] = 0; 
            $response['session'] = 0;
            $response['message'] = 'Session expired please login again';
            echo json_encode($response);
        }
    }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response); 
    }
}else{
 $response['status'] = 0; 
 $response['message'] = 'Invalid API Call';
 echo json_encode($response); 
}
?>