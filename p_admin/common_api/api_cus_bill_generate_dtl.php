<?php
/*
Description: Parking area account status check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function cus_bill_generate_list($prk_admin_id){
    $allplayerdata = array();
    global $pdoconn;
    $sql = "SELECT `cus_bill_dtl_id`,
        `prk_admin_id`, 
        `invoice_number`, 
        `billing_month`, 
        `date_of_invoice`, 
        DATE_FORMAT(`bill_due_date`,'%d-%b-%Y') AS `bill_due_date`,
        DATE_FORMAT(`bill_period_start_date`,'%d-%b-%Y') AS `bill_period_start_date`,
        DATE_FORMAT(`bill_period_end_date`,'%d-%b-%Y') AS `bill_period_end_date`,
        `bill_amount`, 
        `discount_amount`, 
        `tax_value`, 
        `cgst_amount`, 
        `sgst_amount`, 
        `igst_amount`, 
        `round_of_amount`, 
        `total_invoice_value`, 
        `payment_type`, 
        `payment_receive_flag`, 
        `payment_receive_amount`, 
        IFNULL(DATE_FORMAT(`payment_receive_date`,'%d-%b-%Y %I:%i %p'),'NA') AS `payment_receive_date`,
        `bank_name`, 
        `bank_branch_name`, 
        `beneficiary_name`, 
        `cheque_number`, 
        `online_payment_dtl_id` 
        FROM `customer_bill_dtl` 
        WHERE `prk_admin_id`='$prk_admin_id' 
        AND `active_flag`='".FLAG_Y."' 
        AND `del_flag`='".FLAG_N."'
        ORDER BY `cus_bill_dtl_unique_id` DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['cus_bill_dtl_id'] = $val['cus_bill_dtl_id'];
            $park['prk_admin_id'] = $val['prk_admin_id'];
            $park['invoice_number'] = $val['invoice_number'];
            $park['billing_month'] = $val['billing_month'];
            $park['date_of_invoice'] = $val['date_of_invoice'];
            $park['bill_due_date'] = $val['bill_due_date'];
            $park['bill_period_start_date'] = $val['bill_period_start_date'];
            $park['bill_period_end_date'] = $val['bill_period_end_date'];
            // $park['bill_amount'] = $val['bill_amount'];
            // $park['discount_amount'] = $val['discount_amount'];
            // $park['tax_value'] = $val['tax_value'];
            // $park['cgst_amount'] = $val['cgst_amount'];
            // $park['sgst_amount'] = $val['sgst_amount'];
            // $park['igst_amount'] = $val['igst_amount'];
            // $park['round_of_amount'] = $val['round_of_amount'];
            $park['total_invoice_value'] = $val['total_invoice_value'];
            $park['payment_type'] = $val['payment_type'];
            $park['payment_receive_flag'] = $val['payment_receive_flag'];
            $park['payment_receive_amount'] = $val['payment_receive_amount'];
            $park['payment_receive_date'] = $val['payment_receive_date'];
            // $park['bank_name'] = $val['bank_name'];
            // $park['bank_branch_name'] = $val['bank_branch_name'];
            // $park['beneficiary_name'] = $val['beneficiary_name'];
            // $park['cheque_number'] = $val['cheque_number'];
            // $park['online_payment_dtl_id'] = $val['online_payment_dtl_id'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['cus_bill_gen_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
function cus_bill_generate($prk_admin_id,$bill_generate_date,$inserted_by){
    global $pdoconn;
    if(!empty($bill_generate_date)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($bill_generate_date)) == date($bill_generate_date)) {
            $bill_generate_date = $bill_generate_date;
        } else {
            $bill_generate_date = date("Y-m-d", strtotime($bill_generate_date));
        }
    }else{

        $bill_generate_date = TIME_TRN;
    }

    $cus_dtl_show = prk_area_cus_dtl_show($prk_admin_id);
    $cus = json_decode($cus_dtl_show);
    $prk_area_cus_dtl_id=$cus->prk_area_cus_dtl_id;
    // $kind_alt=$cus->kind_alt;
    // $gst_number=$cus->gst_number;
    // $bill_to_add=$cus->bill_to_add;
    // $shipped_to_add=$cus->shipped_to_add;
    $payment_mode=$cus->payment_mode;
    $par_month_rate=$cus->par_month_rate;
    $tax_type=$cus->tax_type;
    $bill_date=$cus->bill_date;
    $payment_due_day=$cus->payment_due_day;
    // $service_name=$cus->service_name;
    $discount=$cus->discount;

    $invoice_number=cus_bill_invoice_number_generate($bill_generate_date,$prk_admin_id);
    $bill_ge_date=date_create($bill_generate_date);
    $billing_month=date_format($bill_ge_date,"M-Y");
    $date_of_invoice=$bill_generate_date;
    $bill_due_date = date('Y-m-d', strtotime("+".$payment_due_day." days", strtotime($bill_generate_date)));
    // $par_month_rate='Y';
    $payment_no_of_month=0;
    switch ($payment_mode) {
        case 'M':
            $payment_no_of_month=1;
            break;
        case 'Q':
            $payment_no_of_month=3;
            break;
        case 'Y':
            $payment_no_of_month=12;
            break;
        default:
        $payment_no_of_month=0;
    }
    $bill_period_start_date=$bill_generate_date;
    $bill_period_end_date = date('Y-m-d', strtotime("+".$payment_no_of_month." months", strtotime($bill_generate_date)));
    $bill_period_end_date = date('Y-m-d', strtotime("-1 days", strtotime($bill_period_end_date)));
    $bill_amount=number_format($par_month_rate*$payment_no_of_month, 2, '.', '');
    $discount_amount=number_format((($bill_amount*$discount)/100), 2, '.', '');
    $tax_value=number_format(($bill_amount-$discount_amount), 2, '.', '');

    $cgst_amount=number_format((($tax_value*9)/100), 2, '.', '');
    $sgst_amount=number_format((($tax_value*9)/100), 2, '.', '');
    $igst_amount='0.00';
    if ($tax_type=='IGST') {
        $cgst_amount='0.00';
        $sgst_amount='0.00';
        $igst_amount=number_format((($tax_value*18)/100), 2, '.', '');
    }
    $total_invoice_value=round($tax_value);
    $round_of_amount = round(($total_invoice_value-$tax_value),2);
    $total_invoice_value=number_format(($total_invoice_value), 2, '.', '');

    $cus_bill_dtl_id = uniqid();
    $sql = "INSERT INTO `customer_bill_dtl`(`cus_bill_dtl_id`, `prk_admin_id`, `invoice_number`, `billing_month`, `date_of_invoice`, `bill_due_date`, `bill_period_start_date`, `bill_period_end_date`, `bill_amount`, `discount_amount`, `tax_value`, `cgst_amount`, `sgst_amount`, `igst_amount`, `round_of_amount`, `total_invoice_value`, `prk_area_cus_dtl_id`, `inserted_by`, `inserted_date`) VALUES ('$cus_bill_dtl_id','$prk_admin_id','$invoice_number','$billing_month','$date_of_invoice','$bill_due_date','$bill_period_start_date','$bill_period_end_date','$bill_amount','$discount_amount','$tax_value','$cgst_amount','$sgst_amount','$igst_amount','$round_of_amount','$total_invoice_value','$prk_area_cus_dtl_id','$inserted_by','".TIME."')";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['cus_bill_dtl_id'] = $cus_bill_dtl_id;
    }else{
        $response['status'] = 0;
        $response['message'] = 'Unsuccessful';
    }
    return json_encode($response);
}
function cus_bill_generate_show($cus_bill_dtl_id){
    $response = array();
    global $pdoconn;
    $sql = "SELECT `cus_bill_dtl_id`,
        `prk_admin_id`, 
        `invoice_number`, 
        `billing_month`, 
        DATE_FORMAT(`date_of_invoice`,'%d-%b-%Y') AS `date_of_invoice`,
        DATE_FORMAT(`bill_due_date`,'%d-%b-%Y') AS `bill_due_date`,
        DATE_FORMAT(`bill_period_start_date`,'%d-%b-%Y') AS `bill_period_start_date`,
        DATE_FORMAT(`bill_period_end_date`,'%d-%b-%Y') AS `bill_period_end_date`,
        `bill_amount`, 
        `discount_amount`, 
        `tax_value`, 
        `cgst_amount`, 
        `sgst_amount`, 
        `igst_amount`, 
        `round_of_amount`, 
        `total_invoice_value`, 
        `payment_type`, 
        `payment_receive_flag`, 
        `payment_receive_amount`, 
        IFNULL(DATE_FORMAT(`payment_receive_date`,'%d-%b-%Y %I:%i %p'),'NA') AS `payment_receive_date`,
        `bank_name`, 
        `bank_branch_name`, 
        `beneficiary_name`, 
        `cheque_number`, 
        `online_payment_dtl_id` 
        FROM `customer_bill_dtl` 
        WHERE `cus_bill_dtl_id`='$cus_bill_dtl_id' 
        AND `active_flag`='".FLAG_Y."' 
        AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['cus_bill_dtl_id'] = $val['cus_bill_dtl_id'];
        $response['prk_admin_id'] = $val['prk_admin_id'];
        $response['invoice_number'] = $val['invoice_number'];
        $response['billing_month'] = $val['billing_month'];
        $response['date_of_invoice'] = $val['date_of_invoice'];
        $response['bill_due_date'] = $val['bill_due_date'];
        $response['bill_period_start_date'] = $val['bill_period_start_date'];
        $response['bill_period_end_date'] = $val['bill_period_end_date'];
        $response['bill_amount'] = $val['bill_amount'];
        $response['discount_amount'] = $val['discount_amount'];
        $response['tax_value'] = $val['tax_value'];
        $response['cgst_amount'] = $val['cgst_amount'];
        $response['sgst_amount'] = $val['sgst_amount'];
        $response['igst_amount'] = $val['igst_amount'];
        $response['round_of_amount'] = $val['round_of_amount'];
        $response['total_invoice_value'] = $val['total_invoice_value'];
        $response['payment_type'] = $val['payment_type'];
        $response['payment_receive_flag'] = $val['payment_receive_flag'];
        $response['payment_receive_amount'] = $val['payment_receive_amount'];
        $response['payment_receive_date'] = $val['payment_receive_date'];
        $response['bank_name'] = $val['bank_name'];
        $response['bank_branch_name'] = $val['bank_branch_name'];
        $response['beneficiary_name'] = $val['beneficiary_name'];
        $response['cheque_number'] = $val['cheque_number'];
        $response['online_payment_dtl_id'] = $val['online_payment_dtl_id'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
function cus_bill_receive($cus_bill_dtl_id,$payment_type,$payment_receive_amount,$payment_receive_by,$inserted_by,$bank_name,$bank_branch_name,$beneficiary_name,$cheque_number,$online_payment_dtl_id){
    global $pdoconn;
    $sql = "UPDATE `customer_bill_dtl` 
        SET `payment_type`='$payment_type',
        `payment_receive_flag`='Y',
        `payment_receive_amount`='$payment_receive_amount',
        `payment_receive_date`='".TIME."',
        `payment_receive_by`='$payment_receive_by',
        `payment_receive_by_name`='$inserted_by',
        `bank_name`='$bank_name',
        `bank_branch_name`='$bank_branch_name',
        `beneficiary_name`='$beneficiary_name',
        `cheque_number`='$cheque_number',
        `online_payment_dtl_id`='$online_payment_dtl_id'
        WHERE `cus_bill_dtl_id`='$cus_bill_dtl_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $response['status'] = 1;
        $response['message'] = 'Successful';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Unsuccessful';
    }
    return json_encode($response);
}
function cus_next_bill_generate_date_show($prk_admin_id){
    $response = array();
    global $pdoconn;
    $cus_dtl_show = prk_area_cus_dtl_show($prk_admin_id);
    $cus = json_decode($cus_dtl_show);
    // $kind_alt=$cus->kind_alt;
    // $gst_number=$cus->gst_number;
    // $bill_to_add=$cus->bill_to_add;
    // $shipped_to_add=$cus->shipped_to_add;
    // $payment_mode=$cus->payment_mode;
    // $par_month_rate=$cus->par_month_rate;
    // $tax_type=$cus->tax_type;
    $bill_date=$cus->bill_date;
    // $payment_due_day=$cus->payment_due_day;
    // $service_name=$cus->service_name;
    // $discount=$cus->discount;
    $sql = "SELECT `cus_bill_dtl_id`,`prk_admin_id`,`bill_period_start_date`,`bill_period_end_date` FROM `customer_bill_dtl` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."' ORDER BY `cus_bill_dtl_unique_id` DESC LIMIT 1";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $bill_period_start_date = $val['bill_period_start_date'];
        $bill_period_end_date = $val['bill_period_end_date'];
        $bill_date = date('d-m-Y', strtotime("+1 days", strtotime($bill_period_end_date)));
    }
    $response['status'] = 1;
    $response['message'] = 'Successful';
    $response['next_bill_date'] = $bill_date;
    return json_encode($response);
}
function cus_bill_invoice_number_generate($bill_generate_date,$prk_admin_id){
    $response = array();
    global $pdoconn;
    $prk_in1 =prk_inf($prk_admin_id);
    $prk_in = json_decode($prk_in1);
    $parking_type=$prk_in->parking_type;
    $bill_to_year='';
    $bill_ge_date=date_create($bill_generate_date);
    $billing_month=date_format($bill_ge_date,"n");
    $billing_year=date_format($bill_ge_date,"y");
    $int_month = (int)$billing_month;
    if ($int_month<=3) {
        # code...
        $bill_to_year=($billing_year-1).'-'.$billing_year;
    }else{
        # code...
        $bill_to_year=$billing_year.'-'.($billing_year+1);
    }
    $invoice_number='IN/'.$parking_type.'/'.$bill_to_year.'/00001';
    $sql = "SELECT 
        `cus_bill_dtl_unique_id`,
        `invoice_number`,
        LPAD((SUBSTRING_INDEX(SUBSTRING_INDEX(`invoice_number`, '/', -2), '/', -1)+1), 5, '0') as next_invoice_number,
        `cus_bill_dtl_id`,
        `prk_admin_id` 
        FROM `customer_bill_dtl`
        WHERE SUBSTRING_INDEX(SUBSTRING_INDEX(`invoice_number`, '/', -2), '/', 1)='$bill_to_year'
        ORDER BY 1 DESC
        LIMIT 1";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $next_invoice_number = $val['next_invoice_number'];
        $invoice_number='IN/'.$parking_type.'/'.$bill_to_year.'/'.$next_invoice_number;
    }
    return $invoice_number;
}
?>