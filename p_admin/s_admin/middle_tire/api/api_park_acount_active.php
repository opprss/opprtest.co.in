<?php
/*
Description: Parking area account status check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function park_acount_active($parking_type,$acount_status,$employee_user_name,$prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "UPDATE `prk_area_admin` SET `park_ac_status`='$acount_status',`parking_type`='$parking_type',`updated_by`='$employee_user_name',`updated_date`='".TIME."' WHERE `prk_admin_id`='$prk_admin_id'";
    $query  = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Sucessfull';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Sucessfull';
    }
    return json_encode($response);
}
?>