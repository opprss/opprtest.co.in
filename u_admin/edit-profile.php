<!-- ################################################
  
Description: user can modify basic profile inforrmation. also image.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################--><?php
include "all_nav/header.php"; 

if (isset($_SESSION['user_admin_id'])) {

  $user_admin_id = $_SESSION['user_admin_id'];
  $response = array();
  $response_address = array();

  $response = user_profile_show($user_admin_id);
  $response = json_decode($response, true);
  if (!empty($response)) {
    $user_name = $response['user_name'];
    $user_nick_name = $response['user_nick_name'];
    $user_gender = $response['user_gender'];
    $user_dob = $response['user_dob'];
    $user_email = $response['user_email'];
    $user_email_verify = $response['user_email_verify'];
    $user_aadhar_id = $response['user_aadhar_id'];  
    $user_img = $response['user_img'];  

  }
}
?>

<!-- header position --> 
<!-- <link href="../css/style.css" rel="stylesheet"> -->
<link href="./css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="./css/style-image.css">
<script type="text/javascript">
  function showUploadOption(){
      $("#profile-upload-option").css('display','block');
    }

    function hideUploadOption(){
      $("#profile-upload-option").css('display','none');
    }
</script>

<style>
  .size{
    font-size: 11px;
  }
.error_size{
  font-size: 11px;
  color: gray;

}
.success{
  font-size: 11px;
  color: green;
}
.user-lebel{
    font-size: 11px !important;
    text-transform: uppercase;
  }
</style>
    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Edit Profile</h5>
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card single pd-20 pd-sm-40 col-md-12">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <div class="row">
              <div class="col-md-3">
                  <div id="imgContainer" style="padding-bottom: 30px; padding-top: 20px">
                    <form enctype="multipart/form-data" action="<?php echo USER_URL ?>profile_image_upload.php" method="post" name="image_upload_form" id="image_upload_form">
                      <div id="imgArea" style="border: 1px solid #33ADFF;">
                        <?php
                          if (!empty($user_img)) {
                        ?>
                          <img src="<?php echo USER_BASE_URL.$user_img;?>" class="wd-32" alt="">                      
                        <?php
                          }else{
                        ?>
                          <img src="./img/default.jpg">
                        <?php
                          }
                        ?>
                        <div class="progressBar">
                          <div class="bar"></div>
                          <div class="percent">0%</div>
                        </div>
                        <div class="icon-choose-image" onClick="showUploadOption()">
                          <i class="ion-ios-camera-outline tx-50"></i>
                          <p class="edit-text">Edit photo</p>
                        </div>
                        <div id="profile-upload-option">
                          <div class="profile-upload-option-list">

                            <input type="file" accept="image/*" name="image_upload_file" id="image_upload_file"></input><span>Upload</span>
                          </div>
                          <?php
                            if (!empty($user_img)) {
                          ?>
                          <div class="profile-upload-option-list" onClick="removeProfilePhoto();">Remove</div>
                          <?php
                            }
                          ?>
                          <hr style="padding: 0px; margin: 0px;">
                          <div class="profile-upload-option-list" onClick="hideUploadOption();">Cancel</div>
                        </div>

                        <input type="hidden" name="user_mobile" value="<?php echo $user_mobile; ?>">
                        <input type="hidden" name="user_id" value="<?php echo $user_admin_id; ?>">
                        <input type="hidden" name="token" value="<?php echo $token; ?>">
                      </div>
                    </form>
                  </div>
              </div>

              <div class="col-md-9">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="form-control-label user-lebel">NAME <span class="tx-danger">*</span></label>

                      <input type="text" class="form-control readonly" placeholder="Name" name="user_name" id="user_name" value="<?php echo $user_name; ?>" style="background-color: transparent;" readonly="readonly">
                      <span style="position: absolute;" id="error_name" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="form-control-label user-lebel">nickname </label>

                      <input type="text" class="form-control" placeholder="Nickname" name="user_nick_name" id="user_nick_name" value="<?php if(isset($user_nick_name)) echo $user_nick_name; else echo ""?>" onkeyup="caps();">
                      <span style="position: absolute;" id="error_name" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="form-control-label user-lebel">mobile <span class="tx-danger">*</span></label>

                      <input type="text" class="form-control readonly" placeholder="Mobile" name="mobile" id="mobile" value="<?php if(isset($user_mobile)) echo $user_mobile; else echo ""?>" style="background-color: transparent;" readonly="readonly">
                      <span style="position: absolute;" id="error_name" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="form-control-label user-lebel">Email <span class="tx-danger">*</span></label>

                      <input type="email" class="form-control" placeholder="Email" name="user_email" id="user_email" value="<?php if(isset($user_email)) echo $user_email; else echo ""?>">
                      <span style="position: absolute;color: red" id="error_email" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="form-control-label user-lebel">Date of birth <span class="tx-danger">*</span></label>

                      <input type="text" class="form-control" placeholder="DOB (mm/dd/yyyy)" name="user_dob" id="user_dob" value="<?php if(isset($user_dob)) echo $user_dob; else echo ""?>" style="background-color: transparent;">
                      <span style="position: absolute;" id="error_name" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label class="form-control-label user-lebel">gender<span class="tx-danger">*</span></label>

                        <?php
                          if ($user_gender == "M") {
                        ?>
                        <label class="rdiobox">
                          <input name="user_gender" id="user_gender" value="M" type="radio" checked>
                          <span>Male</span>
                        </label>
                        <label class="rdiobox">
                          <input name="user_gender" id="user_gender" value="F" type="radio" disabled="disabled">
                          <span>Female</span>
                        </label>
                        <?php
                          }else if($user_gender == "F"){
                            ?>
                            <label class="rdiobox">
                              <input name="user_gender" id="user_gender" value="M" type="radio" disabled="disabled">
                              <span>Male</span>
                            </label>
                            <label class="rdiobox">
                              <input name="user_gender" id="user_gender" value="F" type="radio"  checked>
                              <span>Female</span>
                            </label>
                            <?php
                          }else{
                        ?>
                        <label class="rdiobox">
                          <input name="user_gender" id="user_gender" value="M" type="radio" checked>
                          <span>Male</span>
                        </label>
                        <label class="rdiobox">
                          <input name="user_gender" id="user_gender" value="F" type="radio">
                          <span>Female</span>
                        </label>
                        <?php } ?>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group" id="staticParent">
                      <label class="form-control-label user-lebel">Aadhar ID number</label>

                      <input type="text" class="form-control" placeholder="Aadhar Card ID" name="user_aadhar_id" id="user_aadhar_id" value="<?php if(isset($user_aadhar_id)) echo $user_aadhar_id; else echo ""?>" maxlength="12">
                      <span style="position: absolute;" id="error_name" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <button type="button" class="btn btn-block wl_user_button" name="save" id="save">Save</button>
                      <span style="position: absolute;" id="error_name" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <button type="button" class="btn btn-block wl_user_button_skip" name="skip" id="skip" onclick="document.location.href='profile';">back</button>
                      <span style="position: absolute;" id="error_name" class="error_size"></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div><!-- card -->
      </div><!-- am-pagebody -->
      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
<script src="./js/jquery.form.js"></script>
<script type="text/javascript">
  $(document).on('change', '#image_upload_file', function () {
    //alert("h");
    hideUploadOption();
    var progressBar = $('.progressBar'), bar = $('.progressBar .bar'), percent = $('.progressBar .percent');

    $('#image_upload_form').ajaxForm({
        beforeSend: function() {
        progressBar.fadeIn();
            var percentVal = '0%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        success: function(html, statusText, xhr, $form) {
        //alert(html);  
        obj = $.parseJSON(html);
        var path = 'uploades/medium/'+obj.image_medium;
        if(obj.status){   
          var percentVal = '100%';
          bar.width(percentVal);
          percent.html(percentVal);
          $("#imgArea>img").prop('src',obj.filepath);  
          $.alert({
            icon: 'fa fa-smile-o',
            theme: 'modern',
            title: 'Success !',
            content: "<p style='font-size:0.9em;'>Profile Picture Changed Successfully</p>",
            type: 'green',
            buttons: {
              Ok: function () {
                  location.reload()
              }
            }
          }); 
        }else{
          if( typeof obj.session !== 'undefined'){
            if (!obj.session) {
              window.location.replace("logout.php");
            }
          }else{
            $.alert({
            icon: 'fa fa-frown-o',
            theme: 'modern',
            title: 'Error !',
            content: "<p style='font-size:0.9em;'>"+obj.error+"</p>",
            type: 'red'
          });
          }
        }
        },
      complete: function(xhr) {
        progressBar.fadeOut();      
      } 
    }).submit();    
  });
  var userUrl = "<?php echo USER_URL; ?>";
  $('#save').click(function () {
    var user_admin_id = "<?php echo $_SESSION['user_admin_id']; ?>";
    var user_name = $('#user_name').val();
    var user_nick_name = $('#user_nick_name').val();
    var user_gender = $('#user_gender').val();
    var user_dob = $('#user_dob').val();
    var user_email = $('#user_email').val();
    var user_email_db = "<?php echo $user_email;?>";
    var user_aadhar_id = $('#user_aadhar_id').val();
    var mobile = "<?php echo $user_mobile; ?>";
    var user_email_verify = "<?php echo $user_email_verify; ?>";
    var user_img = "<?php echo $user_img;?>";

    if (user_email != user_email_db) {
      var user_email_verify = "F";
    }
    if(user_email!='' && isEmail(user_email)){
      var urlUserEditPofile = userUrl+'user_edit_profile.php';

      $.ajax({
        url :urlUserEditPofile,
        type:'POST',
        data :
        {
          'user_admin_id':user_admin_id,
          'user_name':user_name,
          'user_nick_name':user_nick_name,
          'user_gender':user_gender,
          'user_dob':user_dob,            
          'user_email':user_email,
          'user_email_verify':user_email_verify,
          'user_aadhar_id':user_aadhar_id,
          'mobile':mobile,
          'token':"<?php echo $token;?>"
        },
        dataType:'html',
        success  :function(data)
        {
          var json = $.parseJSON(data);
          if (json.status) {
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>Profile updated successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                    window.location='profile';
                }
              }
            });
          }else{
            if( typeof obj.session !== 'undefined'){
              if (!obj.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>Something went wrong</p>",
              type: 'red'
            });
            }
          }
        }
      }).responseText;
    }else{
      if(user_email==''){
        $('#error_email').text('Email is required');
      }
      if(!isEmail(user_email)){
        $('#error_email').text('Email is invalid');
      }
    }   
  });
  $("#user_email").blur(function () {
    var user_email=$('#user_email').val();
    //alert(isEmail(user_email));
    if(user_email==''){
      $('#error_email').text('Email is required');
      return false;
    }else if(!isEmail(user_email)){
      $('#error_email').text('Email is invalid');
    }else{
      $('#error_email').text('');
    }
  });

  function caps() {
    var x = document.getElementById("user_nick_name");
    x.value = x.value.toUpperCase();
  }
  $(function() {

    $('#staticParent').on('keydown', '#user_aadhar_id', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
  })
  function removeProfilePhoto(){
    hideUploadOption();
    var user_admin_id = "<?php echo $_SESSION['user_admin_id']; ?>";
    var urlRemove = userUrl+'image_remove.php';
    $.ajax({
      url :urlRemove,
      type:'POST',
      data :
      {
        'user_admin_id':user_admin_id,
        'token':'<?php echo $token; ?>'
      },
      dataType:'html',
      success  :function(data)
      {
        var json = $.parseJSON(data);
        if (json.status){
              $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>Profile Image Removed</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                    location.reload();
                }
              }
            });
          }else{
            if( typeof obj.session !== 'undefined'){
              if (!obj.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>Something went wrong</p>",
              type: 'red'
            });
            }
          }
      }
        });
  }
  function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }
  if ("<?php echo $user_dob; ?>"!='') { 
    // alert('ok');
    $("#user_dob").attr("readonly", true);
    $('#user_dob').css("background-color", "#d9e0e0");
  }else {
    // alert('NO');
    $("#user_dob").attr("readonly", false);
    addEventListener('DOMContentLoaded', function () {
      var now = new Date;
      var now = now.setDate(now.getDate() - 1);
      pickmeup('#user_dob', {
        default_date   : false,
        position       : 'bottom',
        hide_on_select : true,
        render : function (date) {
          if (date > now) {
              return {
                disabled : true,
                class_name : 'date-in-past'
              };
          }
          return {};
        } 
      });
    });
  }
</script>