<!-- ################################################
  
Description: user's licence detail lists and delete.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php include "all_nav/header.php"; ?>
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    
    <h5 class="am-title">Licence Details</h5>
  </div>
  <div class="am-pagebody">
    <div class="card single pd-20 pd-sm-40 col-md-12">
      <div class="row">
        <div class="col-md-12">
          <form id="selectFilter" method="post" action="">
            <div class="form-layout">
              <div class="row mg-b-25">
                <div class="col-md-10">
                </div>
                <div class="col-md-2 error_show pull-right">
                  <div class="form-group">
                      <button type="button" class="btn btn-block user_button" name="add_vehicle" id="add_vehicle" onclick="document.location.href='licence-add';">Add Licence</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <div class="row">
          <div class="col-md-12">
            <div class="list-group widget-list-group">
              <?php
                $user_id = $_SESSION['user_admin_id'];
                //$user_id = 159;
                if (isset($_POST['v_type']))
                  $vehicle = $_POST['v_type'];
                else
                  $vehicle = '';
                
                if(isset($user_id) ){
                  $response = array();
                  $response = user_lic_dtl_list($user_id);
                  $response = json_decode($response, true);
                  if(!empty($response['user_lic'])){
                    $count = count($response['user_lic']);
                    $sl_no = 1;
                  for ($i=0; $i < $count ; $i++) { 
                    $user_lic_id = $response['user_lic'][$i]['user_lic_id'];
                    $user_lic_number = $response['user_lic'][$i]['user_lic_number'];
                    $user_lic_name = $response['user_lic'][$i]['user_lic_name'];
                    $user_lic_veh_class = $response['user_lic'][$i]['user_lic_veh_class'];
                    $user_lic_issue_date = $response['user_lic'][$i]['user_lic_issue_date'];
                    $user_lic_issued_by = $response['user_lic'][$i]['user_lic_issued_by'];
                    $user_lic_valid_till = $response['user_lic'][$i]['user_lic_valid_till'];
                  ?>
                  <div class="list-group-item rounded-top-0">
                    <div class="media">
                      <div class="media-body">
                        <div class="row">
                          <div class="col-lg-12 tx-18 pd-b-10">
                              <?php echo $response['user_lic'][$i]['user_lic_number'].' ('.$user_lic_veh_class.')' ;?>
                          </div>
                          <div class="col-lg-3">
                             <p class="mg-b-0 tx-12">
                              NAME-
                               <?php
                              if (!empty($user_lic_name))
                               echo strtoupper($user_lic_name);
                             else
                              echo "N/A";
                            ?>
                             <br>
                              Issue Date-
                              <?php
                                if (!empty($user_lic_issue_date))
                                   echo $user_lic_issue_date;
                                 else
                                  echo "N/A";
                               ?>
                             </p>
                          </div>
                          <div class="col-lg-3">
                             <p class="mg-b-0 tx-12">
                              Valid Upto- 
                          <?php
                            if (!empty($user_lic_valid_till))
                               echo $user_lic_valid_till;
                             else
                              echo "&nbsp; __/__/____";
                           ?>
                             <br>
                              Issued By- 
                          <?php
                            if (!empty($user_lic_issued_by))
                               echo $user_lic_issued_by;
                             else
                              echo "N/A";
                           ?>
                             </p>
                          </div>
                        </div>                              
                      </div>

                      <div class="d-flex mg-l-20" style="margin-top: 20px;">
                        <a href="<?php echo 'licence-modify?l_id='.base64_encode($user_lic_id)?>" class="" data-toggle="tooltip" data-placement="top" title="Modify">
                          <i class="fa fa-pencil" style="font-size:18px"></i>
                        </a>&nbsp;
                          <button type="button" class="clean-button" name="delete" id="delete<%=count++%>" value="<?php echo $user_lic_id; ?>" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
                          
                      </div>
                    </div>
                  </div>
              <?php
                    $sl_no++;
                  }
                }else{
              ?>
              <h5 class="text-center">You have't add any licence yet!</h5>
              <?php
                }
              }else{
                   $response['status'] = 0; 
                   $response['message'] = 'Invalid API Call';
                   echo json_encode($response);
                   //header('location: signin.php');
                }
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php include"all_nav/footer.php"; ?>
<script type="text/javascript">
  $(document).ready(function(){
    var userUrl = "<?php echo USER_URL; ?>";
    $('button[id^="delete"]').on('click', function() {
      var user_admin_id = "<?php echo $user_id;?>";
      var mobile=' <?php echo $_SESSION['mobile']; ?>';
      var user_lic_id = this.value;
      $.confirm({
        title: 'Are You Sure?',
        content: "<p style='font-size:0.8em;'>It will delete the item permanently</p>",
        theme: 'modern',
        type: 'red',
        buttons: {
          cancel: function () {
          },
          somethingElse: {
            text: 'Delete',
            btnClass: 'btn-red',
            keys: ['Y', 'shift'],
            action: function(){
              if (user_lic_id != '') {
                var urlDtl = userUrl+'user_lic_dtl_delete.php';
                //alert(urlDtl);
                $.ajax({
                  url :urlDtl,
                  type:'POST',
                  data :
                  {
                    'user_admin_id':user_admin_id,
                    'mobile':mobile,
                    'user_lic_id':user_lic_id,
                    'token': '<?php echo $token;?>'
                  },
                  dataType:'html',
                  success  :function(data)
                  {
                    // alert(data);
                    var json = $.parseJSON(data);
                    if (json.status) {
                      $.alert({
                        icon: 'fa fa-smile-o',
                        theme: 'modern',
                        title: 'Success !',
                        content: "<p style='font-size:0.8em;'>Licence delete successfully</p>",
                        type: 'green',
                        buttons: {
                          Ok: function () {
                              window.location='licence';
                          }
                        }
                      });
                    }else{
                      if( typeof json.session !== 'undefined'){
                        if (!json.session) { 
                          window.location.replace("logout.php"); 
                        }                  
                      }else{                    
                        $.alert({                    
                          icon: 'fa fa-frown-o',                    
                          theme: 'modern',                    
                          title: 'Error !',                    
                          content: "Somting went wrong",                    
                          type: 'red'                  
                        });                  
                      }                
                    }
                  }
                });
              }else{
                $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
                type: 'red'
                });
              }
            }
          }
        }
      });
    });
  });
  let d=document.getElementsByClassName('clean-button');
  for(let i=0;i<d.length;i++){
    d[i].addEventListener("mouseover",function(){
      d[i].style.cursor="pointer";
    });
  }    
</script>