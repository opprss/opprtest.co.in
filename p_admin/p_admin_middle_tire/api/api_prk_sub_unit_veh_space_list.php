<?php
/*
Description: Parking area sub unit vehicle space LIST SHOW.
Developed by: Rakhal Raj Mandal
Created Date: 05-05-2018
Update date : -------
*/ 
function prk_sub_unit_veh_space_list($prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();

    $sql = "SELECT `prk_sub_unit_veh_space`.`prk_sub_unit_veh_space_id`,
`prk_sub_unit_veh_space`.`prk_sub_unit_id`,
`prk_sub_unit_veh_space`.`prk_admin_id`,
`prk_sub_unit_veh_space`.`veh_type`,
`prk_sub_unit_veh_space`.`veh_space`,
`prk_sub_unit_veh_space`.`prk_sub_unit_veh_space_eff_date`,
`prk_sub_unit_veh_space`.`prk_sub_unit_veh_space_end_date`,
`prk_sub_unit`.`prk_sub_unit_short_name`
FROM `prk_sub_unit_veh_space` ,`prk_sub_unit`
WHERE `prk_sub_unit_veh_space`.`prk_admin_id`='$prk_admin_id' 
AND `prk_sub_unit_veh_space`.`active_flag`='".FLAG_Y."' 
AND `prk_sub_unit_veh_space`.`del_flag`='".FLAG_N."' 
AND `prk_sub_unit`.`prk_sub_unit_id` = `prk_sub_unit_veh_space`.`prk_sub_unit_id`
AND  '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(`prk_sub_unit_veh_space`.`prk_sub_unit_veh_space_eff_date`, '%d-%m-%Y'), '%Y-%m-%d') 
AND DATE_FORMAT(STR_TO_DATE(IFNULL(`prk_sub_unit_veh_space`.`prk_sub_unit_veh_space_end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')";

    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val) 
    {

        $response['status'] = 1;
        $response['message'] = 'data ssucessfull';
        $park['prk_sub_unit_veh_space_id'] =  $val['prk_sub_unit_veh_space_id'];
        $park['prk_sub_unit_id'] =  $val['prk_sub_unit_id'];
        $park['prk_sub_unit_short_name'] =  $val['prk_sub_unit_short_name'];
        $park['prk_admin_id'] =  $val['prk_admin_id'];
        $park['veh_type'] =  $val['veh_type'];
        $park['veh_space'] =  $val['veh_space'];
        $park['prk_sub_unit_veh_space_eff_date'] =  $val['prk_sub_unit_veh_space_eff_date'];
        $park['prk_sub_unit_veh_space_end_date'] =  $val['prk_sub_unit_veh_space_end_date'];
        array_push($allplayerdata, $park);
        // $response['sub_uni_veh_space'] = $allplayerdata;
    }
    return ($allplayerdata);
}
?>