<?php  
/*
Description: user insurance image upload.
Developed by: Rakhal Raj Mandal
Created Date: 08-04-2018
Update date :08-04-2018
*/
function user_emu_img_upload($user_emu_img, $user_veh_dtl_id, $user_admin_id,$mobile){
	global $pdoconn;
	//$mobile=md5($mobile);
    $response = array();
    $PNG_TEMP_DIR ='./../uploades/'.$mobile.'/EMU/';
    include "phpqrcode/qrlib.php";
    if (!file_exists($PNG_TEMP_DIR)) {
        $old_mask = umask(0);
        mkdir($PNG_TEMP_DIR, 0777, TRUE);
        umask($old_mask);
    }
	$decode_file = base64_decode($user_emu_img);
	$file= uniqid().'.png';
	$file_dir=$PNG_TEMP_DIR.$file;
    if(file_put_contents($file_dir, $decode_file)){
	    $file='uploades/'.$mobile.'/insurance/'.$file;
	    
	    $sql = "UPDATE `user_vehicle_detail` SET `user_emu_img`='$file',`user_emu_img_status`='".FLAG_F."' WHERE `user_veh_dtl_id`='$user_veh_dtl_id' AND `user_admin_id`='$user_admin_id' AND `active_flag` ='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";

	    $query = $pdoconn->prepare($sql);
	    if($query->execute()){
	    	$response['status'] = 1;
	    	$response['user_emu_img'] = $file;
	        $response['message'] = 'Upload Successful';
	    }else{
	        $response['status'] = 0;
	        $response['message'] = 'Upload Not Successful';
	    } 
	}else{
        $response['status'] = 0;
        $response['message'] = 'Not Image Upload';
	}
	return json_encode($response); 
}
?>