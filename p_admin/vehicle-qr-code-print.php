<!-- ################################################  
Description: Parking area can issue gate pass to a particular user.
Developed by: Soemen Banerjee
Created Date: 17-03-2018
####################################################-->
<?php 
  include "all_nav/header.php"; 
  $gate_pass_dtl_list1= prk_gate_pass_dtl_list($prk_admin_id);
  $gate_pass_dtl_list = json_decode($gate_pass_dtl_list1, true);
  $prk_inf = prk_inf($prk_admin_id);
  $prk_inf = json_decode($prk_inf, true);
  $prk_area_name = $prk_inf['prk_area_name'];
  $prk_area_short_name = $prk_inf['prk_area_short_name'];
  $prk_area_logo_img = $prk_inf['prk_area_logo_img'];
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: gray;
  }
  .success{
    font-size: 11px;
    color: green;
  }
  input::-webkit-input-placeholder {
    font-size: 12px;
  }
</style>
    <link href="../lib/highlightjs/github.css" rel="stylesheet">
    <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet"> 
    <div class="am-mainpanel">
      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Vehicle QR Code Print</h5>
      </div>
      <!-- datatable -->
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40">
          <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
            <div class="col-lg-4">
              <input type="button" value="Download QR" class="btn btn-primary pull-right" name="download_qr" id="download_qr" disabled="" style="margin-bottom: 10px;" onclick="multiple_vehicle_qrcode_print()">
            </div>
          </div>
          <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th></th>
                  <th class="">vehicle type</th>
                  <th class="">Gate pass number</th>
                  <th class="">vehicle number</th>
                  <th class="">Owner Name</th>
                  <th class="">Sub unit</th>
                  <th class="">Effective Date</th>
                  <th class="">End Date</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach($gate_pass_dtl_list as $value){
                // $prk_gate_pass_id = $value['prk_gate_pass_id'];
                $mobile = $value['mobile'];
                $veh_type = $value['veh_type'];
                $veh_number = $value['veh_number'];
                $vehicle_type_dec = $value['vehicle_type_dec'];
                $prk_gate_pass_num = $value['prk_gate_pass_num'];
                $veh_owner_name = $value['veh_owner_name'];
                $sub_unit_name = $value['sub_unit_name'];
                $effective_date = $value['effective_date'];
                $end_date = $value['end_date'];
                $tower_name = $value['tower_name'];
                $flat_name = $value['flat_name'];
                // $sub_unit_name = $value['sub_unit_name'];
                $mobile=empty($mobile)?'NA':$mobile;
                $veh_qr_code=$mobile.'-'.$veh_type.'-'.$veh_number;
                // 9679309040-4W-WB123456
                $qr_code_print=base64_encode($veh_qr_code.'||'.$veh_number.'||'.$veh_type.'||'.$vehicle_type_dec.'||'.$veh_owner_name.'||'.$tower_name.'||'.$flat_name.'||'.$prk_area_name.'||'.$prk_area_logo_img);
                ?>
                <tr>
                  <td><input type="checkbox" name="" class="p_'$qr_code_print'" id="<?=$qr_code_print?>" onclick="multiple_qr_print(this.id)"></td>
                  <td><img src="<?php echo OPPR_BASE_URL.$value['vehicle_typ_img']; ?>" style="height: 40px;"> </td>
                  <td><?=$prk_gate_pass_num;?></td>
                  <td><?=$veh_number;?></td>
                  <td><?=$veh_owner_name;?></td>
                  <td><?=$sub_unit_name;?></td>                 
                  <td><?=$effective_date;?></td>
                  <td><?=$end_date;?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
<?php include"all_nav/footer.php"; ?>
<script type="text/javascript">
  var myArray = [];
  function multiple_qr_print(str) {
    if(document.getElementById(str).checked){
      myArray[str] = str;
      // alert('ok');
    }else{
      delete myArray[str];
      // alert('no');
    }
    disable_submit();
  }
  function disable_submit(){
    var size = Object.keys(myArray).length;
    if(size>0){
      $('#download_qr').prop('disabled', false);
    }else{
      $('#download_qr').prop('disabled', true);
    }
  }
  function multiple_vehicle_qrcode_print() {
    var obj1=Object.values(myArray);
    str=obj1;
    urlPrint ="p_admin_middle_tire/multiple_vehicle_qrcode_print.php?list_id="+str;
    window.open(urlPrint,'mywindow','width=850,height=600');
    location.reload();
  }
</script>
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Pages',
      }
    });
    $('#datatable2').DataTable({
      bLengthChange: false,
      searching: false,
      responsive: true
    });
    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

  });
</script>