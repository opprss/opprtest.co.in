<?php 
/*
Description: Parking employ vehicle QR code scan and find vehicle number .
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function qr_code_division($qr_code){
	$response = array();
    $pieces = explode("-", $qr_code);
	global $pdoconn;
	$sql = $pdoconn->prepare("SELECT `vehicle_typ_img`,`vehicle_type_dec` FROM `vehicle_type` WHERE `active_flag`='".FLAG_Y."' AND `vehicle_sort_nm`='$pieces[1]'");
    $sql->execute();
    $val = $sql->fetch();
    $response['status'] = 1;
    $response['message'] = 'Sucessful';
    $response['mobile'] = $pieces[0];
    $response['veh_type'] = $pieces[1];
    $response['veh_number'] = $pieces[2];
    $response['vehicle_type_dec'] = $val['vehicle_type_dec'];
    $response['vehicle_typ_img'] = $val['vehicle_typ_img'];
    return json_encode($response);
}
?>