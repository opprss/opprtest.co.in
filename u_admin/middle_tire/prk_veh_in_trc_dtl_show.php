<?php
/*
Description: vehicle in parking area the show in details.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
include_once 'api/reg_api.php';
// include_once 'api/global_api.php'; 
    $response = array();
    if(isAvailable(array('prk_veh_trc_dtl_id'))){
        if(isEmpty(array('prk_veh_trc_dtl_id'))){
            $prk_veh_trc_dtl_id = trim($_POST['prk_veh_trc_dtl_id']);
            $response= prk_veh_in_dtl($prk_veh_trc_dtl_id);
            echo $response;
        }else{
                $response['status'] = 0;
                $response['message'] = 'All Fields Are Mandatory';
                echo json_encode($response); 
        }

    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        echo json_encode($response); 
    } 
?>