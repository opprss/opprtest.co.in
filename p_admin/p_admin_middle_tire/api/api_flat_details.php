<?php
/*
Description: parking area employ create.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function flat_details($prk_admin_id,$tower_name,$flat_name,$effective_date,$end_date,$parking_admin_name,$flat_id,$crud_type,$flat_size,$two_wh_spa,$four_wh_spa){
   $response = array();
   $allplayerdata = array();
    global $pdoconn;
    switch ($crud_type) {
        case 'I':
            $m_account_number= mt_rand(1000000000, mt_getrandmax());
            $flat_id=uniqid();
            $sql="INSERT INTO `flat_details`(`flat_id`, `tower_id`, `flat_name`, `prk_admin_id`, `eff_date`, `end_date`, `inserted_by`, `inserted_date`,`flat_size`,`two_wh_spa`,`four_wh_spa`, `m_account_number`) VALUES ('$flat_id','$tower_name','$flat_name','$prk_admin_id','$effective_date','$end_date','$parking_admin_name','".TIME."','$flat_size','$two_wh_spa','$four_wh_spa','$m_account_number')";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $account_due=0;
                flat_account_details($prk_admin_id,$tower_name,$flat_id,$m_account_number,$account_due,$parking_admin_name,$crud_type);
                $response['status'] = 1;
                $response['message'] = 'Flat Save';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Flat Not Save ';
            }  
            break;
        case 'U':
            $sql="UPDATE `flat_details` SET `tower_id`='$tower_name',`flat_name`='$flat_name',`end_date`='$end_date',`updated_by`='$parking_admin_name',`updated_date`='".TIME."',`flat_size`='$flat_size',`two_wh_spa`='$two_wh_spa',`four_wh_spa`='$four_wh_spa' WHERE `flat_id`='$flat_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";

            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Flat Modify Save';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Flat Modify Not Save ';
            }  
            break;
        case 'S':
            $sql="SELECT  `flat_id`, `tower_id`, `flat_name`, `prk_admin_id`,`flat_size`,`two_wh_spa`,`four_wh_spa`, `eff_date`, `end_date` FROM `flat_details` WHERE `flat_id`='$flat_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";

            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['flat_id'] =  $val['flat_id'];
                $response['tower_id'] =  $val['tower_id'];
                $response['flat_name'] =  $val['flat_name'];
                $response['prk_admin_id'] =  $val['prk_admin_id'];
                $response['flat_size'] =  $val['flat_size'];
                $response['two_wh_spa'] =  $val['two_wh_spa'];
                $response['four_wh_spa'] =  $val['four_wh_spa'];
                $response['eff_date'] =  $val['eff_date'];
                $response['end_date'] =  $val['end_date'];
                // $response['living_status'] =  $val['living_status'];
            }else{
                $response['status'] = 0;
                $response['message'] = 'Not Successful';
            }  
            break;
        case 'LS':
            $sql="SELECT `flat_details`.`flat_id`,
                `flat_details`.`tower_id`,
                `tower_details`.`tower_name`,
                `flat_details`.`flat_name`,
                `flat_details`.`prk_admin_id`,
                `flat_details`.`flat_size`,
                `flat_details`.`two_wh_spa`,
                `flat_details`.`four_wh_spa`,
                `flat_details`.`eff_date`,
                `flat_details`.`end_date`
                FROM `flat_details`,`tower_details` 
                WHERE `tower_details`.`tower_id`=`flat_details`.`tower_id`
                AND `flat_details`.`prk_admin_id`='$prk_admin_id' 
                AND `flat_details`.`active_flag`='".FLAG_Y."' 
                AND `flat_details`.`del_flag`='".FLAG_N."' 
                ORDER BY `flat_details`.`flat_unique_id` DESC";
                // AND '' BETWEEN `eff_date` AND `end_date`
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $arr_catagory = $query->fetchAll();
                foreach($arr_catagory as $val)
                {
                    $park['flat_id'] =  $val['flat_id'];
                    $park['tower_id'] =  $val['tower_id'];
                    $park['tower_name'] =  $val['tower_name'];
                    $park['flat_name'] =  $val['flat_name'];
                    $park['prk_admin_id'] =  $val['prk_admin_id'];
                    $park['flat_size'] =  $val['flat_size'];
                    $park['two_wh_spa'] =  $val['two_wh_spa'];
                    $park['four_wh_spa'] =  $val['four_wh_spa'];
                    $park['eff_date'] =  $val['eff_date'];
                    $park['end_date'] =  $val['end_date'];
                    // $park['living_status'] =  $val['living_status'];
                    array_push($allplayerdata, $park);
                    // flat_account_details($val['prk_admin_id'],$val['tower_id'],$val['flat_id'],$val['m_account_number'],'0','RFINAL_5775','I');
                }
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['flat_list'] = $allplayerdata;
            }else{
                $response['status'] = 0;
                $response['message'] = 'List Empty';
            }
            break;
        case 'D':
            $sql="UPDATE `flat_details` SET `del_flag`='".FLAG_Y."',`updated_by`='$parking_admin_name',`updated_date`='".TIME."' WHERE `flat_id`='$flat_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Delete Successful';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Delete Not Successful';
            }
            break;
        default:
            $response['status'] = 0;
            $response['message'] = 'Wrong Select';
            break;
    }
    return json_encode($response);  
}
?>