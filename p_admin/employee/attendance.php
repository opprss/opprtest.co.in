<!-- ################################################
  
Description: 
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
 
<?php include "all_nav/header.php";?>
<style type="text/css">
  .size{
    font-size: 11px;
    text-transform: uppercase;
  }
  .error_size,.error{
    font-size: 11px;
    color: red;
  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
  th,tr{
    text-align: center;
  }
</style> 
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="../../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Attendance Add</h5>
  </div>
</div>
<div class="am-mainpanel">
  <div class="am-pagebody" id="data">
    <div class="card pd-20 pd-sm-40" style="padding: 10px;">
      <div class="table-wrapper">
        <table id="datatable1" class="table display responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="wd-5p">Img</th>
              <th class="wd-5p">Month</th>
              <th class="wd-20p">Employee Name</th>
              <th class="wd-10p">In date</th>
              <th class="wd-10p">In Time</th>
              <th class="wd-10p">Out date</th>
              <th class="wd-10p">Out Time</th>
              <th class="wd-5p text-center">Action</th>
            </tr>
          </thead>
          <tbody id="list_vis"></tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include"all_nav/footer.php"; ?>
<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="../../lib/datatables-responsive/jquery.dataTables.min.js"></script>
<script src="../../lib/datatables-responsive/dataTables.buttons.min.js"></script>
<script src="../../lib/datatables-responsive/jszip.min.js"></script>
<script src="../../lib/datatables-responsive/pdfmake.min.js"></script>
<script src="../../lib/datatables-responsive/vfs_fonts.js"></script>
<script src="../../lib/datatables-responsive/buttons.html5.min.js"></script>
<script>
  $( document ).ready( function () {
    // code...
    currentAttendanceList();
  });
  function currentAttendanceList() {
    $('#refresh_button').show();
    var cur_att_list = pkrEmpUrl+'current_attendance_list.php';
    // 'prk_admin_id','prk_area_user_id','token'
    $.ajax({
      url :cur_att_list,
      type:'POST',
      data :{
        'prk_admin_id':prk_admin_id,
        'prk_area_user_id':prk_area_user_id,
        'token':token
      },
      dataType:'html',
      success  :function(data){
        // alert(data);
        var demoLines = '';
        var json = $.parseJSON(data);
        if (json.status){
          var c = 0;
          for (var key in json.atten_list) {
            var keyy = json.atten_list[key];
            c +=1;
            demoLines += '<tr>\
              <td> <img src="'+keyy['prk_user_img']+'" style="height: 40px;"></td>\
              <td>'+keyy['this_month']+'</td>\
              <td>'+keyy['prk_user_name']+'</td>\
              <td>'+keyy['in_date']+'</td>\
              <td>'+keyy['in_time']+'</td>\
              <td>'+keyy['out_date']+'</td>\
              <td>'+keyy['out_time']+'</td>\
              <td>\
                <button style="color:white; background-color:green;" type="button" class="out btn prk_button" id="'+keyy['prk_user_username']+'" onclick="attendance_out(this.id)" data-toggle="tooltip" data-placement="top" disabled><i class="fa fa-sign-out"></i> OUT</button>\
              </td>\
              </tr>';
          }
        }
        $('#datatable1').dataTable().fnDestroy();
        $("#list_vis").html(demoLines);
        datatable_show();
      }
    });
  }
  function attendance_out(prk_user_username_out){
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will Out Attendance</p>",
      theme: 'modern',
      type: 'green',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Out',
          btnClass: 'btn-green',
          keys: ['Y', 'shift'],
          action: function(){
            /*if (prk_user_username_out != '') {
              var urlDtl = pkrEmpUrl+'employee_attendance_out.php';
              //'prk_admin_id','prk_area_user_id','out_prk_area_user_id','prk_user_username','token'
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :{
                  'out_prk_area_user_id':prk_user_username_out,
                  'prk_admin_id':prk_admin_id,
                  'prk_area_user_id':prk_area_user_id,
                  'prk_user_username':prk_user_username,
                  'token':token
                },
                dataType:'html',
                success  :function(data){
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Attendance Out successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                           current_veh_loadAjax();
                        }
                      }
                    });
                  }else{
                    if (json.session=='0') {
                      window.location.replace("logout.php");
                    }
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>"+json.message+"</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{*/
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            // }
          }
        }
      }
    });
  }
  function datatable_show(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      language: {
      searchPlaceholder: 'Search Here',
      sSearch: '',
      lengthMenu: '_MENU_ Page',
      },
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      buttons: [
        {
          text: '<span id="refresh_button"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
          action: function ( e, dt, node, config ) {
              // alert( 'Button activated' );
              currentAttendanceList();
          }
        },
        { 
          extend: 'copyHtml5', 
          //footer: true,
          title: 'CURRENT ATTENDANCE DETAILS SHEET' 
        },
        { 
          extend: 'excelHtml5', 
          //footer: true,
          title: 'CURRENT ATTENDANCE DETAILS SHEET'  
        },
        {
            extend: 'pdfHtml5',
            //footer: true,
            title: 'CURRENT ATTENDANCE DETAILS SHEET',
            customize: function(doc) {
              doc.content[1].margin = [ 60, 0, 0, 0 ] //left, top, right, bottom
            },
            exportOptions: {
                columns: [0,1,2,3,4,5,6]
            }
        },
      ]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    $('#refresh_button').hide();
  }
</script>
