<?php
/*
Description: Parking area sub unit insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function flat_account_details($prk_admin_id,$tower_id,$flat_id,$m_account_number,$account_due,$inserted_by,$crud_type){
    global $pdoconn;
    $response = array();
    switch ($crud_type) {
        case 'I':
            $id=uniqid();
            $sql = "INSERT INTO `flat_account`(`flat_account_id`, `prk_admin_id`, `tower_id`, `flat_id`, `m_account_number`, `account_due`, `transitions_date`, `inserted_by`, `inserted_date`) VALUES ('$id', '$prk_admin_id', '$tower_id', '$flat_id', '$m_account_number', '$account_due', '".TIME."', '$inserted_by', '".TIME."')";
            $query = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Sucessfull';
                return json_encode($response);
            }else{
                $response['status'] = 0;
                $response['message'] = 'Not sucessfull';
                return json_encode($response);
            }
            break;
        case 'U':
            $sql = "SELECT `account_due` FROM `flat_account` WHERE `prk_admin_id`='$prk_admin_id' AND `m_account_number`='$m_account_number' AND `acc_status`='".FLAG_A."' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query = $pdoconn->prepare($sql);
            $query->execute();
            $val = $query->fetch();
            $old_account_due = $val['account_due'];
            $account_due =($old_account_due+$account_due);
            #---------------
            $sql = "UPDATE `flat_account` SET `account_due`='$account_due',`transitions_date`='".TIME."' WHERE `prk_admin_id`='$prk_admin_id' AND `m_account_number`='$m_account_number' AND `acc_status`='".FLAG_A."' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Sucessfull';
                return json_encode($response);
            }else{
                $response['status'] = 0;
                $response['message'] = 'Not sucessfull';
                return json_encode($response);
            }
            break;
        default:
            $response['status'] = 0;
            $response['message'] = 'Wrong Select';
            return json_encode($response);
            break;
    }
}
?>