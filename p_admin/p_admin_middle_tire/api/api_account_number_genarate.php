<?php 
/*
Description: Parking adimn account number genarate.
Developed by: Manaranjan sarkar
Created Date: -------
Update date :31-03-2018
*/ 
function account_number_genarate(){
    global $pdoconn;
    
    do {
        $account_number= mt_rand(1000000000, mt_getrandmax());

        $sql = "SELECT `account_number` FROM `prk_area_admin` WHERE `account_number`=:account_number";

        $query  = $pdoconn->prepare($sql);
        $query->execute(array('account_number'=>$account_number));        
        $count=$query->rowCount();
    } while ($count >= 1);
    
    return  $account_number;
}
?>