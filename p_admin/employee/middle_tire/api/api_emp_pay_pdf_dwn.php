<?php
/*
Description: parking employ today payment type history api
Developed by: Manoranjan
Created Date: -------
Update date :27-05-2018
*/ 
@include_once "../fpdf/fpdf.php";
function emp_pay_pdf_dwn($response){
    class PDF extends FPDF
{
function Footer()
{
    // Go to 1.5 cm from bottom
    $this->SetY(-15);
    // Select Arial italic 8
    $this->SetFont('Arial','I',8);
    // Print centered page number
    $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
}
}
    $pdf = new \PDF('P','mm','A4');        
    $res=json_decode($response,true);
// print_r($res);
    // die();
    $x1=40;
    $x2=80;
    $x4=100;
    $x5=140;
    $x6=180;
    $x3=160;
    $y=45;
    $y2=27;
    $xnummer=15;
    $ynummer=30;
    $nummer=0;
    $xlijn=10;
    $ylijn=41;
    $total_pay=51;
     $myl=47;
    $x=0;
    $xnummer=10;
    $ynummer=45;
    $nummer=0;
$pdf->addPage();
$pdf->SetMargins(10, 10); 
$pdf->SetAutoPageBreak(true, 10);


    foreach ($res['payment_history'] as $key => $item){
        
        // if($key=='cash'){
        //     $ky='Cash';
        //     $pdf->Text(100, 15, "{$ky}", TRUE);
        // }
        //  if($key=='wallet'){
        //     $ky='wallet';
        //     $pdf->Text(100, 15, "{$ky}", TRUE);
        // }
        // if($key=='dig_g_p_p'){
        //     $ky='Digital Gate Pass';
        //     $pdf->Text(80, 15, "{$ky}", TRUE);
        // } if($key=='tdgp'){
        //     $ky='Temporary Digital Gate Pass';
        //     $pdf->Text(70, 15, "{$ky}", TRUE);
        // }

        // $pdf->Line($xlijn, $ylijn, 190, $ylijn);
        foreach ($res['payment_history'][$key] as $key_p => $item) {
        $total_pay=$total_pay+7; 
        $myl=$myl+7;
 $pdf->Image(prk_LOGO_pdf,5,10,35);
           
        // $pdf->Cell(100, 10,$prk_area_name , 0, 1, 'C');
        $pdf->SetFont('Arial','',8); 
         $pdf->Text(85, 15, "{$res['prk_area_name']}", TRUE);
        
        $pdf->Text(85, 20, "Employee Payment Report ", TRUE);
        $date=date_create($item['start_date']);
        $date2=date_format($date,"d-m-Y");
        $pdf->Text(160, 15, "Start Date: {$date2}", TRUE);

        $end_date=date_create($item['end_date']);
        $end_date=date_format($end_date,"d-m-Y");
        $pdf->Text(160, 20, "End Date: {$end_date}", TRUE);

$pdf->SetFont('Arial','B',14); 
        $pdf->SetTextColor(0,0, 0);
        $pdf->SetFont('Arial','B',8);
        $pdf->Text(10, 40, "Vehicle Type", TRUE);
        $pdf->Text(40, 40, "Vehicle Number", TRUE);
        $pdf->Text(75, 40, "Payment Type", TRUE);
        $pdf->Text(100, 40, "Payment Recept Date", TRUE);
        $pdf->Text(135, 40, "Payment Recept Time", TRUE);
        $pdf->Text(177, 40, "Total Pay", TRUE);

            // print_r($item);
            $pdf->SetFont('Arial','',11);
            $nummer++;
            $y = $y+7;
            $y2 = $y2+7;
            $ynummer = $ynummer+7;
            $ylijn = $ylijn+7;
            $pdf->SetXY($x1, $y);
            if($nummer % 30 === 0){
                $x1=40;
                $x2=80;
                $x4=100;
                $x5=140;
                $x6=180;
                $x3=160;
                $y=45;
                $y2=27;
                $total_pay=51;
        	 $myl=47;
                $xnummer=10;
                $ynummer=45;
                $xlijn=10;
                $ylijn=41;
                $pdf->AddPage();
            }
            $pdf->SetFont('Arial','',8);
            $pdf->Text($xnummer,$ynummer,"{$item['vehicle_type_dec']}",1,'L', TRUE);

            // $pdf->Image($item['vehicle_typ_img'],$xnummer , $ynummer); 

            $pdf->Text($x1,$y,"{$item['veh_number']}",1,'L', TRUE);
            
            if($item['payment_type']== DGP){
            $pdf->Text($x2,$y,"dgp",1,'L', TRUE);
            }else{
            $pdf->Text($x2,$y,"{$item['payment_type']}",1,'L', TRUE);
            }
            
            $pdf->Text($x4,$y,"{$item['payment_rec_date']}",1,'L', TRUE);
            $pdf->Text($x5,$y,"{$item['payment_rec_time']}",1,'L', TRUE);
            $pdf->Text($x6,$y,"{$item['total_pay']}",1,'L', TRUE);
            $pdf->Line($xlijn, $ylijn, 190, $ylijn);
            $x=$x+1;   

             $pdf->SetXY(50,270);
$pdf->SetFont('Arial','',7);
$pdf->Cell(10,5,COPYRIGHT_PDF,'C');        
        }
    }
    
 $pdf->SetFont('Arial','B',11);
$pdf->Text($x6,$total_pay,"{$res['total_pay']}",1,'L', TRUE);
$pdf->Text($x5,$total_pay,"Grand Total",1,'L', TRUE);
$pdf->setXY(10,$myl);
    $pdf->cell(183,6,"",1);
    
    $name= uniqid();
    $filename='../user-pdf-dwn/';
    if (!file_exists($filename)) {
        $old_mask = umask(0);
        mkdir($filename, 0777, TRUE);
        umask($old_mask);
    }
    $fulpath=$filename.'/'.$name.".pdf";
    $path='user-pdf-dwn/'.$name.".pdf";
    if($x>0){
    $pdf->Output($fulpath,'F');
        $report['status']=1;
        $report['url']= $path;
        $report['message']='PDF Created Successful';
    }else{
        $report['status']=0;
        $report['message']='PDF Created Failed';
    }
    echo json_encode($report);
}
?>