<?php
  session_start();
  include "all_nav/header.php";
  function dropdown_list(){
    $html=" <option value=''>Select Option</option><option value='Y'>YES</option><option value='N'>No</option>";
    return $html;
  }
  if (isset($_REQUEST["prk_admin_id"])) {
    $prk_admin_id= trim($_REQUEST["prk_admin_id"]);
    if(empty($prk_admin_id)){
     header('Location: prk-list.php');
    }
  }else{

     header('Location: prk-list');
  }
  $admin_dtl_show = p_admin_dtl_show($prk_admin_id);
  $admin_dtl_sh = json_decode($admin_dtl_show);
  $prient_in_flag=$admin_dtl_sh->prient_in_flag;
  $prient_out_flag=$admin_dtl_sh->prient_out_flag;
  $advance_flag=$admin_dtl_sh->advance_flag;
  $advance_pay=$admin_dtl_sh->advance_pay;
  $ad_flag=$admin_dtl_sh->ad_flag;
  $sms_flag=$admin_dtl_sh->sms_flag;
  $veh_out_otp_flag=$admin_dtl_sh->veh_out_otp_flag;
  $veh_token_flag=$admin_dtl_sh->veh_token_flag;
  $visitor_token_flag=$admin_dtl_sh->visitor_token_flag;
  $veh_valid_flag=$admin_dtl_sh->veh_valid_flag;
  $parking_type=$admin_dtl_sh->parking_type;
  $visitor_out_verify_flag=$admin_dtl_sh->visitor_out_verify_flag;
  $veh_out_verify_flag=$admin_dtl_sh->veh_out_verify_flag;
  $park_ac_status=$admin_dtl_sh->park_ac_status;
  $prk_area_name=$admin_dtl_sh->prk_area_name;
  $prk_admin_name=$admin_dtl_sh->prk_admin_name;
  $prk_area_email=$admin_dtl_sh->prk_area_email;
  $prk_area_rep_mobile=$admin_dtl_sh->prk_area_rep_mobile;
  // $prk_admin_name=$admin_dtl_sh->prk_admin_name;
  // $prk_admin_name=$admin_dtl_sh->prk_admin_name;
  // $prk_admin_name=$admin_dtl_sh->prk_admin_name;
?>
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
<style type="text/css">
  .size{
    font-size: 11px;
  }
</style>
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Acouunt Seting</h5>
  </div>
</div>
<div class="am-mainpanel">
	<div class="am-pagebody">
    <div class="card pd-20 pd-sm-40" >
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <form role="form" id="addVis" method="post"  enctype="multipart/form-data">
          <div class="row mg-b-25">
            <div class="col-md-3">
              <div class="form-group">
                <label class="form-control-label size">Premises Name<span class="tx-danger"></span></label>
                  <input type="text" name="prk" value="<?php echo $prk_area_name; ?>">
                  <span style="position: absolute;" id="error_advance_flag" class="error_size"></span>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="form-control-label size">Premises User Name<span class="tx-danger"></span></label>
                  <input type="text" name="prk" value="<?php echo $prk_admin_name; ?>">
                  <span style="position: absolute;" id="error_advance_flag" class="error_size"></span>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="form-control-label size">Premises Email Id<span class="tx-danger"></span></label>
                  <input style="font-size: 11px;" type="text" name="prk" value="<?php echo $prk_area_email; ?>">
                  <span style="position: absolute;" id="error_advance_flag" class="error_size"></span>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="form-control-label size">Premises Mobile Number<span class="tx-danger"></span></label>
                  <input type="text" name="prk" value="<?php echo $prk_area_rep_mobile; ?>">
                  <span style="position: absolute;" id="error_advance_flag" class="error_size"></span>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="form-control-label size">Premises Area Type<span class="tx-danger"></span></label>
                <div class="select">
                  <select id="parking_type">
                    <option value="">Select Parking Type</option>
                    <option value="R">Residential Parking</option>
                    <option value="C">Commercial Parking</option>
                  </select>
                  <span style="position: absolute;" id="error_parking_type" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="form-control-label size">Premises Acount Status<span class="tx-danger"></span></label>
                <div class="select">
                  <select id="acount_status">
                    <option value="">Select Acount Status</option>
                    <option value="A">Active</option>
                    <option value="D">Draft</option>
                    <option value="L">Logged</option>
                    <option value="T">Temporary Logged</option>
                    <option value="P">Payment Due</option>
                  </select>
                  <span style="position: absolute;" id="error_parking_type" class="error_size"></span>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <!-- <button style="margin-top: 33px;" type="button" class="btn btn-block prk_button" name="visitor_save" id="visitor_save">SAVE</button> -->
                <!-- <button value="SAVE jghjghjghjgjhghjg" type="button" name="visitor_save" id="visitor_save"></button> -->
                <input type="button" value="SAVE" style="margin-top: 33px;" class="btn btn-block btn-primary prk_button" name="visitor_save" id="visitor_save">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <button style="margin-top: 33px;" type="reset" class="btn btn-block prk_button">RESET</button>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <button style="margin-top: 33px;" id="back" type="button" class="btn btn-block prk_button">BACK</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>   
<!-- datatable -->
<?php include"all_nav/footer.php"; ?>
<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>

<script>
  var SUP_EMP_URL = "<?php echo SUP_EMP_URL ;?>";
  var employee_user_id = "<?php echo $employee_user_id ;?>";
  var employee_user_name = "<?php echo $employee_user_name ;?>";
  var prk_admin_id = "<?php echo $prk_admin_id ;?>";
  var park_ac_status = "<?php echo $park_ac_status ;?>";
  var parking_type = "<?php echo $parking_type ;?>";

  $( document ).ready( function () {
    $('#parking_type').val(parking_type);
    $('#acount_status').val(park_ac_status);

    $('#visitor_save').click(function(){
      var parking_type=$('#parking_type').val();
      var acount_status=$('#acount_status').val();
      // alert(parking_type);
      $(this).val('Wait ...').attr('disabled','disabled');
      var urlgate_pass = SUP_EMP_URL+'park_acount_active.php';
      $.ajax({
        url :urlgate_pass,
        type:'POST',
        data :
        {
          'parking_type':parking_type,
          'acount_status':acount_status,
          'employee_user_name':employee_user_name,
          'prk_admin_id':prk_admin_id
        },
        dataType:'html',
        success:function(data)
        {
          var json = $.parseJSON(data);
          if (json.status){
            $(this).val('SAVE').attr('enable','enable');
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success',
              content: "<p style='font-size:0.9em;'>Set successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                  window.location='prk-draft-list';
                }
              }
            });
          }else{
            $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.7em;'>Somting went wrong 1</p>",
              type: 'red'
            });
          }
        }
      });
    });
    $('#back').click(function(){
      window.location='prk-draft-list';
    });
  });
</script> 8340445064->Omrita Sing