<!-- ################################################
  
Description: Parking area admin profile update. with image.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
include "all_nav/header.php";
$sessionDetails = getSessionDetails();
$parking_admin_name = $sessionDetails['parking_admin_name'];

if (isset($_SESSION['prk_admin_id'])) {

  $user_name=$_SESSION['prk_admin_id'];
  $prk_admin_id = $_SESSION['prk_admin_id'];

  $response = array();

  $response = prk_area_profile_show($prk_admin_id);
  $response = json_decode($response, true);

  if (!empty($response)) {
    $prk_area_name = $response['prk_area_name'];
    $prk_area_short_name = $response['prk_area_short_name'];
    $prk_area_short_na = $response['prk_area_short_name'];
    $prk_area_email = $response['prk_area_email'];
    $prk_area_rep_name = $response['prk_area_rep_name'];
    $prk_area_rep_mobile = $response['prk_area_rep_mobile']; 
    $prk_email_verify = $response['prk_email_verify'];
    $prk_mobile_verify = $response['prk_mobile_verify']; 
    $prk_area_pro_img = $response['prk_area_pro_img'];
    $prk_area_logo_img = $response['prk_area_logo_img'];
    $set_last_due_day = $response['last_due_day'];
    $set_choose_due_day = $response['choose_due_day'];
    $prk_area_display_name = $response['prk_area_display_name'];
  }
}
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .essess{
    font-size: 11px;
    color: green;
  }

  /*image*/
  .bgColor {
      max-width: 440px;
      height: 400px;
      background-color: #fff4ca;
      padding: 30px;
      border-radius: 4px;
      text-align: center;    
    }
    .upload-preview {border-radius:4px;width: 200px;height: 200px;}
    #body-overlay {background-color: rgba(0, 0, 0, 0.6);z-index: 999;position: absolute;left: 0;top: 0;width: 100%;height: 100%;display: none;}
    #body-overlay div {position:absolute;left:50%;top:50%;margin-top:-32px;margin-left:-32px;}
    #targetOuter{ 
      position:relative;
      text-align: center;
      background-color: #F0E8E0;
      margin: 20px auto;
      width: 200px;
      height: 200px;
      border-radius: 4px;
    }
    .btnSubmit {
      background-color: #565656;
      border-radius: 4px;
      padding: 10px;
      border: #333 1px solid;
      color: #FFFFFF;
      width: 200px;
      cursor:pointer;
    }
    .inputFile{
      margin-top: 0px;
      left: 0px;
      right: 0px;
      top: 0px;
      width: 200px;
      height: 36px;
      background-color: #FFFFFF;
      overflow: hidden;
      opacity: 0;
      position: absolute;
      cursor: pointer;
    }
    .icon-choose-image {
      position: absolute;
      /*opacity: 0.5;*/
      top: 50%;
      left: 50%;
      margin-top: -24px;
      margin-left: -50px;
      width: 100px;
      height: 100px;
      cursor:pointer;
      color: #FFFFFF;
    }
    .edit-text{
      font-size: 1.5em;
      margin-top: -15px;
      color: #FFF;
    }
    #profile-upload-option, #logo-upload-option{
      display:none;
      position: absolute;
      top: 163px;
      left: 23px;
      margin-top: -70px;
      margin-left: -24px;
      border: #33ADFF 1px solid;
      border-radius: 4px;
      background-color: #FFFFFF;
      width: 150px;
    }
    .profile-upload-option-list{
      margin: 1px;
      height: 35px;
      /*border-bottom: 1px solid #cecece;*/
      cursor: pointer;
      position: relative;
      padding:5px 0px;
    }
    .profile-upload-option-list:hover{
      background-color: #33ADFF;
    }
  input[type="file"] {
    bottom: 0;
    cursor: pointer;
    height: 100%;
    left: 0;
    margin: 0;
    opacity: 0;
    padding: 0;
    position: absolute;
    width: 100%;
    z-index: 0;
  }
</style>
<script type="text/javascript">
  function showUploadOption(){
    $("#profile-upload-option").css('display','block');
  }
  function hideUploadOption(){
    $("#profile-upload-option").css('display','none');
  }
  function showLogoUploadOption(){
    $("#logo-upload-option").css('display','block');
  }
  function hideLogoUploadOption(){
    $("#logo-upload-option").css('display','none');
  }
</script>
<link href="./css/style.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Edit Profile</h5>
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40 col-md-12" style="min-height: 505px;">
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">    
        <div class="row">
          <div class="col-md-3">
            <div id="imgContainer" style="padding-bottom: 30px; padding-top: 20px">
              <p style="padding: 0px 0px 5px 0px; margin: 0px;">PROFILE PHOTO</p>

              <form enctype="multipart/form-data" action="<?php echo PRK_URL ?>image_upload_demo_submit.php" method="post" name="image_upload_form" id="image_upload_form">
                <div id="imgArea" style="border: 1px solid #33ADFF;">
                  <?php
                    if (!empty($prk_area_pro_img)) {
                  ?>
                    <img src="<?php echo PRK_BASE_URL.$prk_area_pro_img;?>" class="wd-32" alt="">                      
                  <?php
                    }else{
                  ?>
                    <img src="<?php echo IMAGE_URL ?>default.jpg">
                  <?php
                    }
                  ?>
                  <div class="progressBar">
                    <div class="bar"></div>
                    <div class="percent">0%</div>
                  </div>
                  <div class="icon-choose-image" onClick="showUploadOption()">
                    <i class="ion-ios-camera-outline tx-50"></i>
                    <p class="edit-text">Edit photo</p>
                  </div>
                  <div id="profile-upload-option">
                    <div class="profile-upload-option-list">
                      <input type="file" accept="image/*" name="image_upload_file" id="image_upload_file"></input><span>Upload</span>
                    </div>
                    <?php
                      if (!empty($prk_area_pro_img)) {
                    ?>
                    <div class="profile-upload-option-list" onClick="removeProfilePhoto();">Remove</div>
                    <?php
                      }
                    ?>
                    <hr style="padding: 0px; margin: 0px;">
                    <div class="profile-upload-option-list" onClick="hideUploadOption();">Cancel</div>
                  </div>

                  <input type="hidden" name="parking_admin_name" value="<?php echo $parking_admin_name; ?>">
                  <input type="hidden" name="user_name" value="<?php echo $user_name; ?>">
                  <input type="hidden" name="token" value="<?php echo $token; ?>">
                </div>
              </form>
            </div>
          </div>
          <!-- logo -->
          <div class="col-md-3">
            <div id="imgContainer" style="padding-bottom: 30px; padding-top: 20px">
              <p style="padding: 0px 0px 5px 0px; margin: 0px;">LOGO</p>

              <form enctype="multipart/form-data" action="<?php echo PRK_URL ?>logo_upload.php" method="post" name="logo_upload_form" id="logo_upload_form">
                <div id="logoArea" style="border: 1px solid #33ADFF;">
                  <?php
                    if (!empty($prk_area_logo_img)) {
                  ?>
                    <img src="<?php echo PRK_BASE_URL.$prk_area_logo_img;?>" class="wd-32" alt="">                      
                  <?php
                    }else{
                  ?>
                    <img src="<?php echo IMAGE_URL ?>default.jpg">
                  <?php
                    }
                  ?>
                  <div class="progressBarLogo">
                    <div class="bar"></div>
                    <div class="percent">0%</div>
                  </div>
                  <div class="icon-choose-image" onClick="showLogoUploadOption()">
                    <i class="ion-ios-camera-outline tx-50"></i>
                    <p class="edit-text">Edit photo</p>
                  </div>
                  <div id="logo-upload-option">
                    <div class="profile-upload-option-list">
                      <input type="file" accept="image/*" name="image_upload_file" id="logo_upload_file"></input><span>Upload</span>
                    </div>
                    <?php
                      if (!empty($prk_area_logo_img)) {
                    ?>
                    <div class="profile-upload-option-list" onClick="removeLogoPhoto();">Remove</div>
                    <?php
                      }
                    ?>
                    <hr style="padding: 0px; margin: 0px;">
                    <div class="profile-upload-option-list" onClick="hideLogoUploadOption();">Cancel</div>
                  </div>

                  <input type="hidden" name="parking_admin_name" value="<?php echo $parking_admin_name; ?>">
                  <input type="hidden" name="user_name" value="<?php echo $user_name; ?>">
                  <input type="hidden" name="token" value="<?php echo $token; ?>">
                </div>
              </form>
            </div>
          </div>
          <!-- /logo -->
        </div>
        <div class="row">
          <div class="col-md-12">
            <form id="addGate" method="get" action="som">
              <div class="form-layout">
                <div class="row mg-b-25 mg-t-20">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="form-control-label size">AREA NAME <span class="tx-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="Park Area Name" name="prk_area_name" id="prk_area_name" value="<?php echo $prk_area_name; ?>" onkeydown="upperCase(this)">
                      <span style="position: absolute;" id="error_prk_area_name" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="form-control-label size">AREA SHORT NAME <!-- <span class="tx-danger">*</span> --></label>
                      <input type="text" maxlength="3" class="form-control" placeholder="Park Area Short Name" name="prk_area_short_name" id="prk_area_short_name" value="<?php echo $prk_area_short_name; ?>" onkeydown="upperCase(this)">
                      <span style="position: absolute;" id="error_prk_area_short_name" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="form-control-label size">AREA DISPLAY NAME</label>
                      <input type="text" class="form-control" placeholder="Park Area display Name" name="prk_area_display_name" id="prk_area_display_name" value="<?php echo $prk_area_display_name; ?>" onkeydown="upperCase(this)">
                      <span style="position: absolute;" id="error_prk_area_short_name" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-md-2" style="display: none;">
                    <div class="form-group">
                      <label class="form-control-label size">Maintenance Due Date <span class="tx-danger"></span></label>
                      <label class="ckbox mg-t-10">
                        <input id="last_due_day" value="Y" type="checkbox">
                        <span style="font-size: 11px;">Last Day of the Month</span>
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2" style="display: none;">  
                    <div class="form-group">
                      <label class="form-control-label size">Choose Day <span class="tx-danger"></span></label>
                      <input type="text" class="form-control" placeholder="Choose Day" name="choose_due_day" id="choose_due_day" value="<?php echo $set_choose_due_day; ?>" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label class="form-control-label size">REPRESENTATIVE NAME <span class="tx-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="Representative name" name="prk_area_rep_name" id="prk_area_rep_name" value="<?php echo $prk_area_rep_name; ?>" onkeydown="upperCase(this)">
                      <span style="position: absolute;" id="error_prk_area_rep_name" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label class="form-control-label size">AREA EMAIL <span class="tx-danger">*</span></label>
                      <input type="email" class="form-control" placeholder="Park Area email" name="prk_area_email" id="prk_area_email" value="<?php echo $prk_area_email; ?>">
                      <span style="position: absolute;" id="error_prk_area_email" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label class="form-control-label size">MOBILE NUMBER<span class="tx-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="Representative mobile" name="prk_area_rep_mobile" id="prk_area_rep_mobile" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $prk_area_rep_mobile; ?>" maxlength="10">
                      <span style="position: absolute;" id="error_prk_area_rep_mobile" class="error_size"></span>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-layout-footer mg-t-30">
                      
                      <div class="row">
                        <div class="col-lg-6">
                          <button type="button" class="btn mg-b-10 btn-block prk_button" name="save" id="save">SAVE</button>
                        </div>
                        <div class="col-lg-6">
                          <button type="button" class="btn mg-b-10 btn-block prk_button_skip" name="skip" id="skip" onclick="document.location.href='parking-area-profle';">Back</button>
                          <span style="position: absolute;" id="error_all" class="error_size"></span>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include"all_nav/footer.php"; ?>
<script src="./js/jquery.form.js"></script>
<script type="text/javascript">
  if ("<?=$set_last_due_day?>"=='Y') {
    $("#last_due_day").prop( "checked", true );
    $('#choose_due_day').val('').prop('disabled', true);
  }
  $(document).on('change', '#image_upload_file', function () {
    hideUploadOption();
    var progressBar = $('.progressBar'), bar = $('.progressBar .bar'), percent = $('.progressBar .percent');
    $('#image_upload_form').ajaxForm({
      beforeSend: function() {
      progressBar.fadeIn();
          var percentVal = '0%';
          bar.width(percentVal)
          percent.html(percentVal);
      },
      uploadProgress: function(event, position, total, percentComplete) {
          var percentVal = percentComplete + '%';
          bar.width(percentVal)
          percent.html(percentVal);
      },
      success: function(html, statusText, xhr, $form) {  
      //alert(html);
      obj = $.parseJSON(html);
      var path = 'uploades/medium/'+obj.image_medium;
      if(obj.status){   
        var percentVal = '100%';
        bar.width(percentVal);
        percent.html(percentVal);
        $("#imgArea>img").prop('src',obj.filepath);  
        $.alert({
          icon: 'fa fa-smile-o',
          theme: 'modern',
          title: 'Success !',
          content: "<p style='font-size:0.9em;'>Profile picture changed successfully</p>",
          type: 'green',
          buttons: {
            Ok: function () {
                location.reload()
            }
          }
        }); 
      }else{
        if( typeof obj.session !== 'undefined'){
          if (!obj.session) {
            window.location.replace("logout.php");
          }
        }else{
          $.alert({
          icon: 'fa fa-frown-o',
          theme: 'modern',
          title: 'Error !',
          content: "<p style='font-size:0.9em;'>"+obj.error+"</p>",
          type: 'red'
        });
        }
      }
      },
      complete: function(xhr) {
        progressBar.fadeOut();      
      } 
    }).submit();    
  });
  /*logo*/
  $(document).on('change', '#logo_upload_file', function () {
    hideLogoUploadOption();
    var progressBar = $('.progressBarLogo'), bar = $('.progressBarLogo .bar'), percent = $('.progressBarLogo .percent');
    $('#logo_upload_form').ajaxForm({
      beforeSend: function() {
      progressBar.fadeIn();
          var percentVal = '0%';
          bar.width(percentVal)
          percent.html(percentVal);
      },
      uploadProgress: function(event, position, total, percentComplete) {
          var percentVal = percentComplete + '%';
          bar.width(percentVal)
          percent.html(percentVal);
      },
      success: function(html, statusText, xhr, $form) {  
      obj = $.parseJSON(html);
      var path = 'uploades/medium/'+obj.image_medium;
      if(obj.status){   
        var percentVal = '100%';
        bar.width(percentVal);
        percent.html(percentVal);
        $("#logoArea>img").prop('src',obj.filepath);  
        $.alert({
          icon: 'fa fa-smile-o',
          theme: 'modern',
          title: 'Success !',
          content: "<p style='font-size:0.9em;'>Logo changed successfully</p>",
          type: 'green',
          buttons: {
            Ok: function () {
                location.reload()
            }
          }
        }); 
      }else{
        if( typeof obj.session !== 'undefined'){
          if (!obj.session) {
            window.location.replace("logout.php");
          }
        }else{
          $.alert({
          icon: 'fa fa-frown-o',
          theme: 'modern',
          title: 'Error !',
          content: "<p style='font-size:0.9em;'>"+obj.error+"</p>",
          type: 'red'
        });
        }
      }
      },
      complete: function(xhr) {
        progressBarLogo.fadeOut();      
      } 
    }).submit();    
  });
  /*/lofo*/
  var prkUrl = "<?php echo PRK_URL; ?>";
  var present_prk_area_email = "<?php echo $prk_area_email; ?>";
  $( document ).ready( function () {
    /*FORM SUBMIT*/
    $('#save').click(function(){
        var prk_admin_id = "<?php echo $prk_admin_id;?>";
        var prk_admin_name = "<?php echo $parking_admin_name;?>";
        var prk_area_name = $('#prk_area_name').val();
        var prk_area_rep_name = $('#prk_area_rep_name').val();
        var prk_area_pro_img = "<?php echo $prk_area_pro_img;?>";

        var prk_area_rep_mobile = $('#prk_area_rep_mobile').val();
        var prk_area_short_name = $('#prk_area_short_name').val();
        var prk_area_rep_mobile_db = "<?php echo $prk_area_rep_mobile;?>";
        var prk_area_email =$('#prk_area_email').val();
        var prk_area_email_db = "<?php echo $prk_area_email;?>";

        var prk_email_verify = "<?php echo $prk_email_verify; ?>";
        if (prk_area_email != prk_area_email_db) {
          var prk_email_verify = "F";
        }
        if (prk_area_rep_mobile != prk_area_rep_mobile_db) {
          var prk_mobile_verify = "F";
        }
        if (prk_area_name!='' && prk_area_rep_name!='' && prk_area_rep_mobile!='' && prk_area_email!='' && isEmail(prk_area_email ) && isMobile(prk_area_rep_mobile) && prk_area_short_name!='') {
          // alert("if Enter");
          var urlVehNoCheck = prkUrl+'duplicate_email_check.php';
          //team122221@gmail.com
          if (prk_area_email != present_prk_area_email) {
            $.ajax({
              url :urlVehNoCheck,
              type:'POST',
              data :
              {
                'email':prk_area_email
              },
              dataType:'html',
              success  :function(data)
              {
                var json = $.parseJSON(data);
                if (json.status) {
                  $('#error_prk_area_email').text('');
                  ajaxCall();
                }else{
                  $('#error_prk_area_email').text('Email is already exist');
                  return false;
                }

              }
            });
          }else{
            $('#error_prk_area_email').text('');
            ajaxCall();
          }              
        }else{
          // alert('No');
          if (prk_area_rep_name == '') {
          	$('#error_prk_area_rep_name').text('Representative name required');
        	}if (prk_area_rep_mobile == '') {
          	$('#error_prk_area_rep_mobile').text('Mobile number required');
          //return false;
        	}if (prk_area_email == '') {
          	$('#error_prk_area_email').text('Required  email');
        	}
        	if (!isEmail(prk_area_email )) {
          	$('#error_prk_area_email').text('Enter valid email');
        	}
          if (prk_area_short_name=='') {
            $('#error_prk_area_short_name').text('Enter The Short Name');
          }
        }
    });
    $("#prk_area_rep_mobile").blur(function () {
      var prk_area_rep_mobile=$('#prk_area_rep_mobile').val();
      if(prk_area_rep_mobile==''){
        $('#error_prk_area_rep_mobile').text('Mobile number required');
        return false;
      }else if (!isMobile(prk_area_rep_mobile)){
        $('#error_prk_area_rep_mobile').text('Mobile is not valid');
        return false;
      }else{
        $('#error_prk_area_rep_mobile').text('');
      }
    });
    $("#prk_area_rep_name").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 60) && (inputValue != 0 & inputValue !=32)){
        // if(!(inputValue >= 97) && (inputValue != 46)){
        event.preventDefault();
      }
    });
    $("#prk_area_rep_name").blur(function () {
      var prk_area_rep_name = $('#prk_area_rep_name').val();
      if(prk_area_rep_name==''){
        $('#error_prk_area_rep_name').text('Representative name required');
        return false;
      }else{
        $('#error_prk_area_rep_name').text('');
      }
    });
    $("#prk_area_short_name").blur(function () {
      var prk_area_rep_name = $('#prk_area_short_name').val();
      if(prk_area_rep_name==''){
        $('#error_prk_area_short_name').text('Enter The Short Name');
        return false;
      }else{
        $('#error_prk_area_short_name').text('');
      }
    });
    /*emial check and validation*/
    $("#prk_area_email").blur(function(){
      var prk_area_email = $('#prk_area_email').val();
      if (prk_area_email != present_prk_area_email){
        IsEmailValid(prk_area_email);
      }else{
          $('#error_prk_area_email').text('');
      }
    });
    function IsEmailValid(prk_area_email) {
      var urlVehNoCheck = prkUrl+'duplicate_email_check.php';
      if(isEmail(prk_area_email)){
        $.ajax({
          url :urlVehNoCheck,
          type:'POST',
          data :
          {
            'email':prk_area_email
          },
          dataType:'html',
          success  :function(data)
          {
           // alert(data);
            var json = $.parseJSON(data);
            emailReturn(json.status);
          }
        });
      }else{
        $('#error_prk_area_email').text('Email is not valid');
        return false;
      }
    } 
    function emailReturn(flag){
      if(!flag){
          $('#error_prk_area_email').text('Email is already exist');
          //return false;
      }else{
          $('#error_prk_area_email').text('');
          //return true;
      }
    }
    /*/email check an validation*/
    /*save*/
    function ajaxCall(){
      var prk_admin_id = "<?php echo $prk_admin_id;?>";
      var prk_admin_name = "<?php echo $parking_admin_name;?>";
      var prk_area_name = $('#prk_area_name').val();
      var prk_area_short_name = $('#prk_area_short_name').val();
      var prk_area_display_name = $('#prk_area_display_name').val();
      var prk_area_short_na = "<?php echo $prk_area_short_na; ?>";
      var sub_unit_flag='';
      if(prk_area_short_na==''){
        sub_unit_flag='Y';
      }else{
        sub_unit_flag='N';
      }
      var prk_area_rep_name = $('#prk_area_rep_name').val();
      var prk_area_pro_img = "<?php echo $prk_area_pro_img;?>";

      var prk_area_rep_mobile = $('#prk_area_rep_mobile').val();
      var prk_area_rep_mobile_db = "<?php echo $prk_area_rep_mobile;?>";
      var prk_area_email =$('#prk_area_email').val();
      var prk_area_email_db = "<?php echo $prk_area_email;?>";

      var prk_email_verify = "<?php echo $prk_email_verify; ?>";
      var prk_mobile_verify = "<?php echo $prk_mobile_verify; ?>";

      if (prk_area_email != prk_area_email_db) {
        var prk_email_verify = "F";
      }
      if (prk_area_rep_mobile != prk_area_rep_mobile_db) {
        var prk_mobile_verify = "F";
      }
      var last_due_day='';
      if(document.getElementById("last_due_day").checked) {
        // alert('ok');
        var last_due_day='Y';
      } else {
        // alert('no');
        var last_due_day='N';
      }
      var choose_due_day = $('#choose_due_day').val();
      var urlPrkAreaProfileUpdate = prkUrl+'prk_area_profile_update.php';
      $.ajax({
        url :urlPrkAreaProfileUpdate,
        type:'POST',
        data :
        {
          'prk_admin_id':prk_admin_id,
          'prk_admin_name':prk_admin_name,
          'prk_area_short_name':prk_area_short_name,
          'prk_area_display_name':prk_area_display_name,
          'prk_area_name':prk_area_name,
          'prk_area_rep_name':prk_area_rep_name,
          'prk_area_rep_mobile':prk_area_rep_mobile,
          'prk_area_email':prk_area_email,
          'prk_email_verify':prk_email_verify,
          'prk_mobile_verify':prk_mobile_verify,
          'prk_area_pro_img':prk_area_pro_img,
          'sub_unit_flag':sub_unit_flag,
          'last_due_day':last_due_day,
          'choose_due_day':choose_due_day,
          'token': '<?php echo $token;?>'

        },
        dataType:'html',
        success  :function(data){
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>Profile updated successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                    window.location='parking-area-profle';
                }
              }
            });
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                type: 'red'
              });
            }
          }
        }
      });
    }
    if ("<?php echo $prk_area_short_na; ?>"!='') { 
      // alert('ok');
      $("#prk_area_short_name").attr("readonly", true);
    }else {
      // alert('NO');
      $("#prk_area_short_name").attr("readonly", false);
    }
    $("#prk_area_name").attr("readonly", true);
    $('#last_due_day').click(function(){
      // alert($('#last_due_day').val());
      console.log(this.checked);
      if(this.checked == true) {
        $('#choose_due_day').val('').prop('disabled', true);
      } else {
        $('#choose_due_day').prop('disabled', false);
      }
    });
    $("#choose_due_day").keyup(function(){
      var mm = $('#choose_due_day').val()
     /* if (mm > 15 && mm < 59){
        $.confirm({
          icon: 'fa fa-exclamation-circle',
          theme: 'modern',
          title: 'Hold on!',
          content: "<p style='font-size:0.9em;'>Round up time is more then <b><span style='color:red'>'"+mm+"'</span></b> minute. Are you sure? </p>",
          type: 'red',
          buttons: {
            Yes: function () {
              //ok no job
            },
            No: function () {
              $('#choose_due_day').val('');
            }
          }
        });
      }else*/ if(mm > 28){
        $('#choose_due_day').val('28');
      }
    });
  });
  /* end velidation*/
  //remove DP
  function removeProfilePhoto(){
    hideUploadOption();
    var prk_admin_id = "<?php echo $prk_admin_id;?>";
    var token =  '<?php echo $token;?>';
    // alert(token);
    var urlRemove = prkUrl+'image_remove.php';
    $.ajax({
      url :urlRemove,
      type:'POST',
      data :
      {
        'prk_admin_id':prk_admin_id,
        'token': '<?php echo $token;?>'
      },
      dataType:'html',
      success  :function(data)
      {
        // alert(data);
        var json = $.parseJSON(data);
        if (json.status){
              $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>Profile Image Removed</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                    location.reload();
                }
              }
            });
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
              type: 'red'
            });
            }
          }
      }
    });
  }
  //remove Logo
  function removeLogoPhoto(){
    hideUploadOption();
    var prk_admin_id = "<?php echo $prk_admin_id;?>";
    var token =  '<?php echo $token;?>';
    // alert(token);
    var urlRemove = prkUrl+'logo_remove.php';
    $.ajax({
      url :urlRemove,
      type:'POST',
      data :
      {
        'prk_admin_id':prk_admin_id,
        'token':token
        // 'token': '<?php //echo $token;?>'
      },
      dataType:'html',
      success  :function(data)
      {
        // alert(data);
        var json = $.parseJSON(data);
        if (json.status){
          $.alert({
            icon: 'fa fa-smile-o',
            theme: 'modern',
            title: 'Success !',
            content: "<p style='font-size:0.9em;'>Logo Image Removed</p>",
            type: 'green',
            buttons: {
              Ok: function () {
                location.reload();
              }
            }
          });
        }else{
          if( typeof json.session !== 'undefined'){
            if (!json.session) {
              window.location.replace("logout.php");
            }
          }else{
            $.alert({
            icon: 'fa fa-frown-o',
            theme: 'modern',
            title: 'Error !',
            content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
            type: 'red'
          });
          }
        }
      }
    });
  }
  function upperCase(a){
      setTimeout(function(){
          a.value = a.value.toUpperCase();
      }, 1);
  }
  function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }
  function isMobile(mobile){
    var regex = /^\d{10}$/;
    return regex.test(mobile);
  }
</script>