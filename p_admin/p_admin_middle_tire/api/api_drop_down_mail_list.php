<?php
/*
Description: Parking area vehicle all rate dtl list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function drop_down_mail_list($prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql="select 'AM' as id,'ALL RESIDENT MEMBER' as 'name'
        UNION 
        select 'AF' as id,'ALL ASSOCIATION MEMBER' as 'name'
        UNION 
        select 'AO' as id,'ALL OWNER' as 'name'
        UNION 
        select 'AT' as id,'ALL TENANT' as 'name'
        UNION 
        SELECT CONCAT('TO-',tow_d.tower_id)  as 'id',
        tow_d.tower_name as 'name'
        FROM tower_details tow_d 
        WHERE tow_d.active_flag='".FLAG_Y."' 
        AND tow_d.del_flag='".FLAG_N."' 
        AND tow_d.prk_admin_id='$prk_admin_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['id'] = $val['id'];
            $park['name'] = $val['name'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Sucessfull';
        $response['drop_down_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>