<?php
/*
Description: Parking area all payment history.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_my_trn($prk_admin_id,$start_date,$end_date,$vehicle_number,$vehicle_type,$payment_type){
    if(!empty($start_date)&& !empty($end_date)){
        $format = "Y-m-d";
        if(date($format, strtotime($start_date)) == date($start_date) && date($format, strtotime($end_date)) == date($end_date)) {
            $start_date= $start_date;
            $end_date=  $end_date ;
        } else {
            $start_date = date("Y-m-d", strtotime($start_date));
            $end_date = date("Y-m-d", strtotime($end_date));
        }
    }else{
        $end_date= date("Y-m-d");
        $start_date= date("Y-m-d",strtotime("-7 Days"));
    }
    global $pdoconn;
    $park = array();
    $total = 0;
    $sql = "SELECT `prk_area_dtl`.`prk_area_name`,
        `payment_dtl`.`veh_number`,
        `payment_dtl`.`vehicle_type`,
        `payment_dtl`.`total_hr`,
        `payment_dtl`.`round_hr`,
        `payment_dtl`.`total_pay`,
        `payment_dtl`.`payment_type` as payment_type_sort,
        (CASE `payment_dtl`.`payment_type` WHEN '".CASH."' ThEN '".CASH."' WHEN '".WALLET."' THEN '".WALLET."' WHEN '".TDGP."' THEN '".TDGP_F."' WHEN '".DGP."' THEN '".DGP_F."' WHEN '".PP."' THEN '".CASH."' end) as 'payment_type',
        `payment_dtl`.`prk_veh_trc_dtl_id`,
        `vehicle_type`.`vehicle_typ_img`,
        `vehicle_type`.`vehicle_type_dec`,
        `prk_veh_trc_dtl`.`user_mobile`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%d-%m-%Y') AS `veh_in_date`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%I:%i %p') AS `veh_in_time`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%d-%m-%Y') AS `veh_out_date`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%I:%i %p') AS `veh_out_time`,
        `prk_veh_trc_dtl`.`transition_date`
        FROM `payment_dtl`,`prk_veh_trc_dtl`,`prk_area_dtl`,`vehicle_type`
        WHERE `payment_dtl`.`prk_veh_trc_dtl_id`=`prk_veh_trc_dtl`.`prk_veh_trc_dtl_id`
        AND `payment_dtl`.`prk_admin_id`= `prk_area_dtl`.`prk_admin_id`  
        AND `payment_dtl`.`pay_dtl_del_flag`='".FLAG_N."' 
        AND `payment_dtl`.`vehicle_type`=`vehicle_type`.`vehicle_sort_nm`
        AND IF('$payment_type' IS NULL OR '$payment_type'='',`payment_dtl`.`payment_type`,'$payment_type')=`payment_dtl`.`payment_type`
        AND IF('$vehicle_type' IS NULL OR '$vehicle_type'='',`payment_dtl`.`vehicle_type`,'$vehicle_type')=`payment_dtl`.`vehicle_type`
        AND `payment_dtl`.`veh_number` LIKE CONCAT('%', IF('$vehicle_number' IS NULL OR '$vehicle_number'='',`payment_dtl`.`veh_number`,'$vehicle_number') ,'%')
        AND `prk_area_dtl`.`active_flag`='".FLAG_Y."'
        AND `payment_dtl`.`prk_admin_id`='$prk_admin_id'
        AND `prk_veh_trc_dtl`.`transition_date` BETWEEN '$start_date' AND '$end_date'
        ORDER BY  `prk_veh_trc_dtl`.`inserted_date` DESC ";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $prk_area_name='';
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $total = $total +  $val['total_pay'];
            $resp['veh_number'] = $val['veh_number'];
            $resp['vehicle_type'] = $val['vehicle_type'];
            $resp['vehicle_type_dec'] = $val['vehicle_type_dec'];
            $resp['total_hr'] = $val['total_hr'];
            $resp['round_hr'] = $val['round_hr'];
            $resp['total_pay'] = $val['total_pay'];
            $resp['payment_type'] = $val['payment_type'];
            $resp['user_mobile'] = $val['user_mobile'];
            $resp['veh_in_date'] = $val['veh_in_date'];
            $resp['veh_in_time'] = $val['veh_in_time'];
            $resp['veh_out_date'] = $val['veh_out_date'];
            $resp['veh_out_time'] = $val['veh_out_time'];
            $resp['transition_date'] = $val['transition_date'];
            $resp['prk_veh_trc_dtl_id'] = $val['prk_veh_trc_dtl_id'];
            $resp['vehicle_typ_img'] = $val['vehicle_typ_img'];
            $prk_area_name=$val['prk_area_name'];

            $park[$val['transition_date']][] = $resp;
        }
        $response['status'] = 1;
        $response['message'] = 'Deta Sucessfully';
        $response['prk_area_name'] = $prk_area_name;
        $response['start_date'] = $start_date;
        $response['end_date'] = $end_date;
        $response['total_pay'] = $total;
        $response['payment_history'] = $park;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>