<?php
/*
Description: vehicle in parking area
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function prk_veh_trc_dtl($prk_admin_id,$user_admin_id,$prk_veh_type,$veh_number,$prk_user_username,$prk_in_rep_name,$payment_type,$gate_pass_num){
    global $pdoconn;
    $response = array();
    $sql = "INSERT INTO `prk_veh_trc_dtl`(`prk_admin_id`,`user_admin_id`,`prk_veh_type`,`veh_number`,`transition_date`,`prk_in_rep_name`,`inserted_by`,`payment_type`,`gate_pass_num`,`prk_veh_in_time`,`inserted_date`) VALUE ('$prk_admin_id','$user_admin_id','$prk_veh_type','$veh_number','".TIME_TRN."','$prk_in_rep_name','$prk_user_username','$payment_type','$gate_pass_num','".TIME."','".TIME."')";
    $query = $pdoconn->prepare($sql); 
    if($query->execute()){
        $id = $pdoconn->lastInsertId();
        $response['status'] = 1;
        $response['prk_veh_trc_dtl_id'] = $id;
        $response['message'] = 'Successful';
        return json_encode($response); 
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
        return json_encode($response);
    }   
} 
?>