<?php
@session_start();
@require('library/php-excel-reader/excel_reader2.php');
@require('library/SpreadsheetReader.php');
@require ('Classes/PHPExcel.php');

@include_once('../global/config.php');
@include_once('../global/conn-pdo.php');

$session_id = session_id();
$CurrentWorkSheetIndex = 0;
$max_day1 = $_POST['max_day1'];
$site_hide_code = $_POST['site_hide_code'];
$date_period = $_POST['date_period'];
$_SESSION["date_period"] = $date_period;
$date_period_end = $_POST['date_period_end'];
$_SESSION["date_period_end"] = $date_period_end;
$month=$_POST['date_period_end']; 
$abcm=date('m', strtotime($month));
$_SESSION["m_a_month"] = $abcm;
$abcy=date('Y', strtotime($month));
$_SESSION["m_a_year"] = $abcy;
$piece=explode("-",$month);
$year = date('Y');
$file_post = "uploads/".time()."_".$_FILES['file']['name'];
$format = "Y-m-d";
if(date($format, strtotime($date_period)) == date($date_period)) {
  $date_period= $date_period;
}else{
  $date_period = date("Y-m-d", strtotime($date_period));
}
$inserted_by=$_SESSION['user_id'];   
$sql = "SELECT `period_date` FROM `bill_wage` WHERE CONCAT(SUBSTRING(`period_date`, 7, 4),'-',SUBSTRING(`period_date`,4,2),'-',SUBSTRING(`period_date`,1,2))>='$date_period' AND `bill_wage_flag`='S' ORDER by `id` DESC LIMIT 1";
$query  = $pdoconn->prepare($sql);
$query->execute();
$count=$query->rowCount();
if($count>0){
  echo "uploaded";
}else{
  if($_FILES['file']['name']){
    $file_post = "uploads/".time()."_".$_FILES['file']['name'];
    @copy($_FILES['file']['tmp_name'],$file_post);
      if($handle = fopen($file_post, "r")){
      $indexing = 1;     
      while (($Row = fgetcsv($handle, 10000, ",")) !== FALSE) {
        if($indexing == 1){
          $indexing++;
          continue;
        }
        $emp_id = isset($Row[2]) ? $Row[2] : '';
        $emp_name = isset($Row[4]) ? $Row[4] : '';
        $category = isset($Row[5]) ? $Row[5] : '';
        $category_code = isset($Row[6]) ? $Row[6] : '';
        $category_duty_hrs = isset($Row[7]) ? $Row[7] : '';
        $payroll_type = isset($Row[8]) ? $Row[8] : '';
        $nd = isset($Row[9]) ? $Row[9] : '';
        $ot = isset($Row[10]) ? $Row[10] : '';
        $late_hrs = isset($Row[11]) ? $Row[11] : '';
        $off_day = isset($Row[12]) ? $Row[12] : '';
        $nh = isset($Row[14]) ? $Row[14] : '';
        $pl = isset($Row[13]) ? $Row[13] : '';
        $cl = isset($Row[11]) ? $Row[11] : '';
        $sl = isset($Row[12]) ? $Row[12] : '';
        $mi = 0;
        $tot_attd_duty = isset($Row[9]) ? $Row[9] : '';
        $gross_wage_monthly = isset($Row[15]) ? $Row[15] : '';
        $max_day = $max_day1;
        $posted_at = isset($Row[17]) ? $Row[17] : '';
        $bill = isset($Row[18]) ? $Row[18] : '';

        $sql = "SELECT `emp_id`,`emp_code1` FROM `employee_dtl` WHERE `emp_code1`=LPAD('$emp_id',12,'0')";
        $query = $pdoconn->prepare($sql);
        $query->execute();
        $val = $query->fetch();
        $emp_id = $val['emp_id'];
        $emp_code1 = $val['emp_code1'];

        $sql="INSERT INTO `monthly_attd` (`emp_code1`,`emp_id`,`site_hide_code`, `emp_name`, `category`, `category_code`, `category_duty_hrs`, `payroll_type`, `nd`, `ot`, `late_hrs`, `off_day`, `nh`, `pl`, `cl`, `sl`, `mi`, `tot_attd_duty`, `gross_wage_monthly`, `max_day`, `posted_at`, `bill`, `inserted_month`,`inserted_year`,`inserted_by`, `inserted_date`) VALUES ('".$emp_code1."','".$emp_id."', '".$site_hide_code."','".$emp_name."', '".$category."', '".$category_code."', '".$category_duty_hrs."', '".$payroll_type."', '".$nd."', '".$ot."', '".$late_hrs."', '".$off_day."', '".$nh."', '".$pl."', '".$cl."', '".$sl."', '".$mi."', '".$tot_attd_duty."', '".$gross_wage_monthly."', '".$max_day."', '".$posted_at."', '".$bill."','".$abcm."','".$abcy."', '".$inserted_by."', '".TIME."')";
         
         $query = $pdoconn->prepare($sql);
         $query->execute();
      }
    }
  }  
  echo 'ok';
}
//}
?>