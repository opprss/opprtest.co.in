<?php 
/*
Description: user address show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
require_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_admin_id','mobile','token','adm_mer_dtl_name','adm_mer_dtl_number','adm_mer_dtl_remarks'))){
 	if(isEmpty(array('user_admin_id','mobile','token','adm_mer_dtl_name','adm_mer_dtl_number'))){
        $prk_admin_id = isset($_REQUEST['prk_admin_id']) ? trim($_REQUEST['prk_admin_id']) : '';
        $user_admin_id = trim($_POST['user_admin_id']);
        $adm_mer_dtl_name = trim($_POST['adm_mer_dtl_name']);
        $adm_mer_dtl_number = trim($_POST['adm_mer_dtl_number']);
        $adm_mer_dtl_remarks = trim($_POST['adm_mer_dtl_remarks']);
        $inserted_status = isset($_POST['inserted_status']) ? trim($_POST['inserted_status']) : 'U';
        $inserted_by = trim($_POST['mobile']);
        $token = trim($_POST['token']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){ 
            $response = emergency_add($prk_admin_id,$user_admin_id,$adm_mer_dtl_name,$adm_mer_dtl_number,$adm_mer_dtl_remarks,$inserted_status,$inserted_by);
        }else{
            $response = $resp; 
        }
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response =  json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>