<?php 
/*
Description: user login. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function login($mobile, $password,$device_token_id,$device_type){
    global $pdoconn;
 	$response = array();
    $sql ="SELECT `user_admin_id`,`user_ac_status` FROM `user_admin` WHERE `user_mobile`=:user_mobile AND `user_password`=:user_password";
    $query  = $pdoconn->prepare($sql);
   	$query->execute(array('user_mobile'=>$mobile,'user_password'=>md5($password)));
    $count=$query->rowCount();
    if($count>0){
    	$row = $query->fetch();
     	$user_id =  $row['user_admin_id'];
     	$user_ac_status=  $row['user_ac_status'];
		$token = sha1(uniqid());
		$active_flag_first = "Y";
		$active_flag_second = "N";
		if(tokenCheck($user_id)){
            $sqli = "SELECT `user_name`,`user_nick_name`,`user_img`,`user_email` FROM `user_detail` WHERE `user_admin_id`='$user_id' AND `active_flag`='".FLAG_Y."'";
			$queryi = $pdoconn->prepare($sqli);
        	$queryi->execute();
            $row = $queryi->fetch();
     	    $user_name =  $row['user_name'];
     	    $user_nick_name =  $row['user_nick_name'];
     	    if(!empty($user_nick_name)){
     	    	$name = $user_nick_name;
     	    }else{
     	    	$name = $user_name;
     	    }
     	    $user_img =  $row['user_img'];
     	    $user_email =  $row['user_email'];
            $sql = "INSERT INTO `token` (`user_admin_id`, `token_value`, `device_token_id`, `device_type`, `start_date`) VALUES ('$user_id', '$token','$device_token_id','$device_type','".TIME."')";
			$query = $pdoconn->prepare($sql);
        	$query->execute();	
            if ($query){
				$response['status'] = 1;
                $response['user_admin_id'] = $user_id;
            	$response['mobile'] = $mobile;
            	$response['user_ac_status'] = $user_ac_status;
            	$response['name'] = $name;
            	$response['user_img'] = $user_img;
            	$response['user_email'] = $user_email;
            	$response['token'] = $token;
            	$response['message'] = 'Login Successful';
			}else{
                $response['status'] = 0;
            	$response['token'] = $token;
            	$response['message'] = 'token not found';
			}
		}
		return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'username or password is wrong';
        return json_encode($response);
	}
}
?>