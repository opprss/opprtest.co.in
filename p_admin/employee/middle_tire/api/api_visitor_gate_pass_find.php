<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function visitor_gate_pass_find($visitor_mobile,$prk_admin_id,$visitor_type){
    global $pdoconn;
    $pieces = explode("-", $visitor_mobile);
    $visitor_mobile=trim($pieces[0]);
    #---------- VISITOR GATE PASS--------- #
    if (empty($visitor_type) || $visitor_type=='') {
        $sql ="SELECT  vg.`visitor_gate_pass_dtl_id` as id,
            vg.`visitor_mobile` as mobile, 
            vg.`visitor_name` as name,
            vg.`visitor_gender` as gender,
            vg.`id_number` as id_number,
            IF(vg.visitor_img IS NULL or vg.visitor_img = '', 
            (CASE WHEN vg.visitor_gender= 'M' then '".MALE_IMG_D."' WHEN vg.visitor_gender= 'F' THEN '".FEMALE_IMG_D."' end),
            CONCAT('".PRK_BASE_URL."',vg.visitor_img)) as `show_img`,
            CONCAT(vg.`visitor_add1`,', ',`state`.`state_name`,', ',`city`.`city_name`,', ',vg.`visitor_pin_cod`) as `address`,
            'Visitor Gate Pass' as purpose,
            '".FLAG_V."' as visitor_type,
            '1' as no_of_guest,
            '' as in_verify,
            '' as out_verify,
            '' as me_to_mobile,
            '' as me_to_name,
            '' as tower_id,
            '' as tower_name,
            '' as flat_id,
            '' as flat_name,
            '' as `me_to_add`,
            IF(vg.visitor_vehicle_type IS NULL or vg.visitor_vehicle_type = '','".FLAG_N."','".FLAG_Y."') as vehicle_with,
            vg.visitor_vehicle_type as vehicle_type,
            vg.visitor_vehicle_number as vehicle_number
            FROM `visitor_gate_pass_dtl` vg,`state`,`city`
            WHERE vg.`visitor_mobile`='$visitor_mobile'
            AND vg.`prk_admin_id`='$prk_admin_id'
            AND vg.`active_flag`='".FLAG_Y."' 
            AND vg.`del_flag`='".FLAG_N."'
            AND `state`.`e`=vg.`visitor_state`
            AND `city`.`e_id`=vg.`visitor_city`";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $count=$query->rowCount();
        if($count<=0){
            #---------- CONTRACTUAL GATE PASS--------- #
            $sql ="SELECT `contractual_gate_pass_dtl_id` as id,
                `visitor_mobile` as mobile, 
                `visitor_name` as name,
                `visitor_gender` as gender, 
                '' as id_number,
                IF(visitor_img IS NULL or visitor_img = '',(CASE WHEN visitor_gender= 'M' then '".MALE_IMG_D."' WHEN visitor_gender= 'F' THEN '".FEMALE_IMG_D."' end), CONCAT('".PRK_BASE_URL."',visitor_img)) as `show_img`,
                CONCAT(`visitor_add`,', ',`state`.`state_name`,', ',`city`.`city_name`,', ',`visitor_pin_cod`) as `address`,
                'Contractual Gate Pass' as purpose,
                'C' as visitor_type,
                '1' as no_of_guest,
                '' as in_verify,
                '' as out_verify,
                '' as me_to_mobile,
                '' as me_to_name,
                '' as tower_id,
                '' as tower_name,
                '' as flat_id,
                '' as flat_name,
                '' as me_to_add,
                'N' as vehicle_with,
                '' as vehicle_type,
                '' as vehicle_number,
                `state`.`state_name`,`city`.`city_name`
                FROM `contractual_gate_pass` LEFT JOIN `state` 
                ON `state`.`e`=`visitor_state`
                LEFT JOIN `city` ON `city`.`e_id`=`visitor_city`
                WHERE `prk_admin_id`='$prk_admin_id' 
                AND `visitor_mobile`='$visitor_mobile' 
                AND `contractual_gate_pass`.`active_flag`='".FLAG_Y."' 
                AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count<=0){
                #---------- GUEST PASS--------- #
                $sql="SELECT ug.user_guest_list_id as id,
                    ug.guest_mobile as mobile,
                    ug.guest_name as 'name',
                    ug.guest_gender as gender,
                    ug.guest_id_proof as id_number,
                    (CASE WHEN ug.guest_gender= 'M' then '".MALE_IMG_D."' WHEN ug.guest_gender= 'F' then '".FEMALE_IMG_D."' end) as `show_img`,
                    ug.guest_address as address,
                    ug.guest_purpose as purpose,
                    '".FLAG_G."' as visitor_type,
                    ug.no_of_guest as no_of_guest,
                    ug.guest_in_verify as in_verify,
                    ug.guest_out_verify as out_verify,
                    ua.user_mobile as me_to_mobile,
                    ud.user_name as me_to_name,
                    towd.tower_id,
                    if(towd.tower_name is NULL,'NA',towd.tower_name) as tower_name,
                    flad.flat_id,
                    if(flad.flat_name is NULL,'NA',flad.flat_name) as flat_name,
                    CONCAT(if(towd.tower_name is NULL,'NA',towd.tower_name),', ',if(flad.flat_name is NULL,'NA',flad.flat_name)) as `me_to_add`,
                    '".FLAG_N."' vehicle_with,
                    '' as vehicle_type,
                    '' as vehicle_number
                    FROM user_guest_list ug JOIN user_admin ua JOIN user_detail ud JOIN user_address uaa 
                    LEFT JOIN tower_details towd ON towd.tower_id=uaa.tower_name 
                    AND towd.active_flag='".FLAG_Y."' 
                    AND towd.del_flag='".FLAG_N."'
                    LEFT JOIN flat_details flad ON flad.flat_id=uaa.falt_name
                    AND flad.active_flag='".FLAG_Y."' 
                    AND flad.del_flag='".FLAG_N."'
                    WHERE ug.guest_mobile='$visitor_mobile'
                    AND ug.prk_admin_id='$prk_admin_id'
                    AND ug.active_flag='".FLAG_Y."'
                    AND ug.visitor_in_complete='".FLAG_N."'
                    AND ug.in_date between '".TIME_TRN_WEB."' AND '".TIME_TRN_WEB."'
                    AND ug.del_flag='".FLAG_N."'
                    AND ua.user_admin_id=ug.user_admin_id
                    AND ud.user_admin_id=ug.user_admin_id
                    AND ud.active_flag='".FLAG_Y."'
                    AND uaa.user_admin_id=ug.user_admin_id
                    AND uaa.prk_admin_id=ug.prk_admin_id
                    AND uaa.active_flag='".FLAG_Y."'
                    AND uaa.del_flag='".FLAG_N."'
                    AND uaa.`user_add_verify_flag`='".FLAG_Y."'";
                $query  = $pdoconn->prepare($sql);
                $query->execute();
                $count=$query->rowCount();
                if($count<=0){
                    #---------- OPPR USER PART--------- #
                    $sql ="SELECT ua.user_admin_id as id,
                        ua.user_mobile as mobile,
                        ud.user_name as 'name',
                        ud.user_gender as gender,
                        ud.user_aadhar_id as id_number,
                        IF(ud.user_img IS NULL or ud.user_img = '', 
                        (CASE WHEN ud.user_gender= 'M' then '".MALE_IMG_D."' WHEN ud.user_gender= 'F' THEN '".FEMALE_IMG_D."' end), CONCAT('".USER_BASE_URL."',ud.user_img)) as `show_img`,
                        CONCAT(uaa.user_address,', ',uaa.landmark,', ',`state`.`state_name`,', ',`city`.`city_name`,', ',uaa.user_add_pin) as `address`,
                        '' as purpose,
                        'U' as visitor_type,
                        '' as no_of_guest,
                        '' as in_verify,
                        '' as out_verify,
                        '' as me_to_mobile,
                        '' as me_to_name,
                        '' as tower_id,
                        '' as tower_name,
                        '' as flat_id,
                        '' as flat_name,
                        '' as `me_to_add`,
                        '".FLAG_N."' vehicle_with,
                        '' as vehicle_type,
                        '' as vehicle_number
                        FROM user_admin ua ,user_detail ud,user_address uaa ,`state`,`city` 
                        WHERE ud.user_admin_id=ua.user_admin_id
                        AND ud.active_flag='".FLAG_Y."'
                        AND `state`.`e`=uaa.user_add_state
                        AND `city`.`e_id`= uaa.user_add_city
                        AND uaa.user_admin_id=ua.user_admin_id
                        AND uaa.active_flag='".FLAG_Y."' 
                        AND uaa.del_flag='".FLAG_N."'
                        AND uaa.user_add_verify_flag='".FLAG_Y."'
                        AND ua.user_mobile='$visitor_mobile' 
                        AND ua.user_ac_status='".FLAG_A."'
                        LIMIT 1";
                    $query  = $pdoconn->prepare($sql);
                    $query->execute();
                    $count=$query->rowCount();
                    if($count<=0){
                        #---------- OTHER PART--------- #
                        /*$sql="SELECT `prk_visit_dtl_id` as id,
                            `visit_mobile` as mobile,
                            `visit_name` as name,
                            `visit_gender` as gender,
                            `visit_card_id` as id_number,
                            IF(`visit_img` IS NULL OR `visit_img` = '',
                                (CASE WHEN `visit_gender`= 'M' then '".MALE_IMG_D."' WHEN `visit_gender`= 'F' THEN '".FEMALE_IMG_D."' end),
                                CONCAT('".PRK_BASE_URL."',`visit_img`)) as `show_img`,
                            `visit_address` as `address`,
                            `visit_purpose` as purpose,
                            'O' as visitor_type,
                            '1' as no_of_guest,
                            '' as in_verify,
                            '' as out_verify,
                            '' as me_to_mobile,
                            '' as me_to_name,
                            '' as tower_id,
                            '' as tower_name,
                            '' as flat_id,
                            '' as flat_name,
                            '' as `me_to_add`,
                            `visit_with_veh` as vehicle_with,
                            `veh_type` as vehicle_type,
                            `veh_number` as vehicle_number
                            FROM `prk_visit_dtl` 
                            WHERE if(`visitor_type`='G' OR `visitor_type`='O','Y','N')='Y'
                            AND `prk_admin_id`='$prk_admin_id'
                            AND `visit_mobile`='$visitor_mobile'
                            ORDER BY `prk_visit_dtl_id` DESC
                            LIMIT 1";*/
                        $sql = "SELECT `prk_visit_dtl_master_id` as id,
                            `visit_mobile` as mobile,
                            `visit_name` as name,
                            `visit_gender` as gender,
                            `visit_card_id` as id_number,
                            IF(`visit_img` IS NULL OR `visit_img` = '',
                                (CASE WHEN `visit_gender`= 'M' then '".MALE_IMG_D."' WHEN `visit_gender`= 'F' THEN '".FEMALE_IMG_D."' end),
                                CONCAT('".PRK_BASE_URL."',`visit_img`)) as `show_img`,
                            `visit_address` as `address`,
                            `visit_purpose` as purpose,
                            'O' as visitor_type,
                            '1' as no_of_guest,
                            '' as in_verify,
                            '' as out_verify,
                            '' as me_to_mobile,
                            '' as me_to_name,
                            '' as tower_id,
                            '' as tower_name,
                            '' as flat_id,
                            '' as flat_name,
                            '' as `me_to_add`,
                            `visit_with_veh` as vehicle_with,
                            `veh_type` as vehicle_type,
                            `veh_number` as vehicle_number 
                            FROM `prk_visit_dtl_master` 
                            WHERE `active_flag`='".FLAG_Y."'
                            AND `del_flag`='".FLAG_N."'
                            AND `visit_mobile`='$visitor_mobile'";
                        $query  = $pdoconn->prepare($sql);
                        $query->execute();
                        $count=$query->rowCount();
                        if($count<=0){
                            $sql="SELECT '' as id,
                                '$visitor_mobile' as mobile,
                                '' as name,
                                '' as gender,
                                '' as id_number,
                                '' as `show_img`,
                                '' as `address`,
                                '' as purpose,
                                'O' as visitor_type,
                                '1' as no_of_guest,
                                '' as in_verify,
                                '' as out_verify,
                                '' as me_to_mobile,
                                '' as me_to_name,
                                '' as tower_id,
                                '' as tower_name,
                                '' as flat_id,
                                '' as flat_name,
                                '' as `me_to_add`,
                                'N' as vehicle_with,
                                '' as vehicle_type,
                                '' as vehicle_number";
                        }
                    }
                }
            }
        }
    }else{
        switch ($visitor_type) {
            case 'V':
                $sql ="SELECT  vg.`visitor_gate_pass_dtl_id` as id,
                    vg.`visitor_mobile` as mobile, 
                    vg.`visitor_name` as name,
                    vg.`visitor_gender` as gender,
                    vg.`id_number` as id_number,
                    IF(vg.visitor_img IS NULL or vg.visitor_img = '', 
                    (CASE WHEN vg.visitor_gender= 'M' then '".MALE_IMG_D."' WHEN vg.visitor_gender= 'F' THEN '".FEMALE_IMG_D."' end),
                    CONCAT('".PRK_BASE_URL."',vg.visitor_img)) as `show_img`,
                    CONCAT(vg.`visitor_add1`,', ',`state`.`state_name`,', ',`city`.`city_name`,', ',vg.`visitor_pin_cod`) as `address`,
                    'Visitor Gate Pass' as purpose,
                    '".FLAG_V."' as visitor_type,
                    '1' as no_of_guest,
                    '' as in_verify,
                    '' as out_verify,
                    '' as me_to_mobile,
                    '' as me_to_name,
                    '' as tower_id,
                    '' as tower_name,
                    '' as flat_id,
                    '' as flat_name,
                    '' as `me_to_add`,
                    IF(vg.visitor_vehicle_type IS NULL or vg.visitor_vehicle_type = '','".FLAG_N."','".FLAG_Y."') as vehicle_with,
                    vg.visitor_vehicle_type as vehicle_type,
                    vg.visitor_vehicle_number as vehicle_number
                    FROM `visitor_gate_pass_dtl` vg,`state`,`city`
                    WHERE vg.`visitor_mobile`='$visitor_mobile'
                    AND vg.`prk_admin_id`='$prk_admin_id'
                    AND vg.`active_flag`='".FLAG_Y."' 
                    AND vg.`del_flag`='".FLAG_N."'
                    AND `state`.`e`=vg.`visitor_state`
                    AND `city`.`e_id`=vg.`visitor_city`";
                break;
            case 'G':
                $sql="SELECT ug.user_guest_list_id as id,
                    ug.guest_mobile as mobile,
                    ug.guest_name as 'name',
                    ug.guest_gender as gender,
                    ug.guest_id_proof as id_number,
                    (CASE WHEN ug.guest_gender= 'M' then '".MALE_IMG_D."' WHEN ug.guest_gender= 'F' then '".FEMALE_IMG_D."' end) as `show_img`,
                    ug.guest_address as address,
                    ug.guest_purpose as purpose,
                    '".FLAG_G."' as visitor_type,
                    ug.no_of_guest as no_of_guest,
                    ug.guest_in_verify as in_verify,
                    ug.guest_out_verify as out_verify,
                    ua.user_mobile as me_to_mobile,
                    ud.user_name as me_to_name,
                    towd.tower_id,
                    if(towd.tower_name is NULL,'NA',towd.tower_name) as tower_name,
                    flad.flat_id,
                    if(flad.flat_name is NULL,'NA',flad.flat_name) as flat_name,
                    CONCAT(if(towd.tower_name is NULL,'NA',towd.tower_name),', ',if(flad.flat_name is NULL,'NA',flad.flat_name)) as `me_to_add`,
                    '".FLAG_N."' vehicle_with,
                    '' as vehicle_type,
                    '' as vehicle_number
                    FROM user_guest_list ug JOIN user_admin ua JOIN user_detail ud JOIN user_address uaa 
                    LEFT JOIN tower_details towd ON towd.tower_id=uaa.tower_name 
                    AND towd.active_flag='".FLAG_Y."' 
                    AND towd.del_flag='".FLAG_N."'
                    LEFT JOIN flat_details flad ON flad.flat_id=uaa.falt_name
                    AND flad.active_flag='".FLAG_Y."' 
                    AND flad.del_flag='".FLAG_N."'
                    WHERE ug.guest_mobile='$visitor_mobile'
                    AND ug.prk_admin_id='$prk_admin_id'
                    AND ug.active_flag='".FLAG_Y."'
                    AND ug.visitor_in_complete='".FLAG_N."'
                    AND ug.in_date between '".TIME_TRN_WEB."' AND '".TIME_TRN_WEB."'
                    AND ug.del_flag='".FLAG_N."'
                    AND ua.user_admin_id=ug.user_admin_id
                    AND ud.user_admin_id=ug.user_admin_id
                    AND ud.active_flag='".FLAG_Y."'
                    AND uaa.user_admin_id=ug.user_admin_id
                    AND uaa.prk_admin_id=ug.prk_admin_id
                    AND uaa.active_flag='".FLAG_Y."'
                    AND uaa.del_flag='".FLAG_N."'
                    AND uaa.`user_add_verify_flag`='".FLAG_Y."'";
                break;
            case 'O':
                $sql="SELECT `prk_visit_dtl_id` as id,
                    `visit_mobile` as mobile,
                    `visit_name` as name,
                    `visit_gender` as gender,
                    `visit_card_id` as id_number,
                    IF(`visit_img` IS NULL OR `visit_img` = '',
                        (CASE WHEN `visit_gender`= 'M' then '".MALE_IMG_D."' WHEN `visit_gender`= 'F' THEN '".FEMALE_IMG_D."' end),
                        CONCAT('".PRK_BASE_URL."',`visit_img`)) as `show_img`,
                    `visit_address` as `address`,
                    `visit_purpose` as purpose,
                    'O' as visitor_type,
                    '1' as no_of_guest,
                    '' as in_verify,
                    '' as out_verify,
                    '' as me_to_mobile,
                    '' as me_to_name,
                    '' as tower_id,
                    '' as tower_name,
                    '' as flat_id,
                    '' as flat_name,
                    '' as `me_to_add`,
                    `visit_with_veh` as vehicle_with,
                    `veh_type` as vehicle_type,
                    `veh_number` as vehicle_number
                    FROM `prk_visit_dtl` 
                    WHERE if(`visitor_type`='G' OR `visitor_type`='O','Y','N')='Y'
                    AND `prk_admin_id`='$prk_admin_id'
                    AND `visit_mobile`='$visitor_mobile'
                    ORDER BY `prk_visit_dtl_id` DESC
                    LIMIT 1";
                break;
            case 'U':
                $sql ="SELECT ua.user_admin_id as id,
                    ua.user_mobile as mobile,
                    ud.user_name as 'name',
                    ud.user_gender as gender,
                    ud.user_aadhar_id as id_number,
                    IF(ud.user_img IS NULL or ud.user_img = '', 
                    (CASE WHEN ud.user_gender= 'M' then '".MALE_IMG_D."' WHEN ud.user_gender= 'F' THEN '".FEMALE_IMG_D."' end), CONCAT('".USER_BASE_URL."',ud.user_img)) as `show_img`,
                    CONCAT(uaa.user_address,', ',uaa.landmark,', ',`state`.`state_name`,', ',`city`.`city_name`,', ',uaa.user_add_pin) as `address`,
                    '' as purpose,
                    'U' as visitor_type,
                    '' as no_of_guest,
                    '' as in_verify,
                    '' as out_verify,
                    '' as me_to_mobile,
                    '' as me_to_name,
                    '' as tower_id,
                    '' as tower_name,
                    '' as flat_id,
                    '' as flat_name,
                    '' as `me_to_add`,
                    '".FLAG_N."' vehicle_with,
                    '' as vehicle_type,
                    '' as vehicle_number
                    FROM user_admin ua ,user_detail ud,user_address uaa ,`state`,`city` 
                    WHERE ud.user_admin_id=ua.user_admin_id
                    AND ud.active_flag='".FLAG_Y."'
                    AND `state`.`e`=uaa.user_add_state
                    AND `city`.`e_id`= uaa.user_add_city
                    AND uaa.user_admin_id=ua.user_admin_id
                    AND uaa.active_flag='".FLAG_Y."' 
                    AND uaa.del_flag='".FLAG_N."'
                    AND uaa.user_add_verify_flag='".FLAG_Y."'
                    AND ua.user_mobile='$visitor_mobile' 
                    AND ua.user_ac_status='".FLAG_A."'
                    LIMIT 1";
                break;
            default:
                $sql="SELECT '' as id,
                    '$visitor_mobile' as mobile,
                    '' as name,
                    '' as gender,
                    '' as id_number,
                    '' as `show_img`,
                    '' as `address`,
                    '' as purpose,
                    'O' as visitor_type,
                    '1' as no_of_guest,
                    '' as in_verify,
                    '' as out_verify,
                    '' as me_to_mobile,
                    '' as me_to_name,
                    '' as tower_id,
                    '' as tower_name,
                    '' as flat_id,
                    '' as flat_name,
                    '' as `me_to_add`,
                    'N' as vehicle_with,
                    '' as vehicle_type,
                    '' as vehicle_number";
                break;
        }
    }
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $vehicle['status'] = 1;
        $vehicle['message'] = 'Successfully';
        $vehicle['id'] = $val['id'];
        $vehicle['mobile'] = $val['mobile'];
        $vehicle['name'] = $val['name'];
        $vehicle['gender'] = $val['gender'];
        $vehicle['id_number'] = $val['id_number'];
        $vehicle['show_img'] = $val['show_img'];
        $vehicle['address'] = $val['address'];
        $vehicle['purpose'] = $val['purpose'];
        $vehicle['visitor_type'] = $val['visitor_type'];
        $vehicle['no_of_guest'] = $val['no_of_guest'];
        $vehicle['in_verify'] = $val['in_verify'];
        $vehicle['out_verify'] = $val['out_verify'];
        $vehicle['me_to_mobile'] = $val['me_to_mobile'];
        $vehicle['me_to_name'] = $val['me_to_name'];
        $vehicle['tower_id'] = $val['tower_id'];
        $vehicle['tower_name'] = $val['tower_name'];
        $vehicle['flat_id'] = $val['flat_id'];
        $vehicle['flat_name'] = $val['flat_name'];
        if ($val['visitor_type']=='V') {
            # code...
            $vehicle['me_to_add'] = visitor_pass_me_to_add($prk_admin_id,$val['id']);
        }else{
            # code...
            $vehicle['me_to_add'] = $val['me_to_add'];
        }
        $vehicle['vehicle_with'] = $val['vehicle_with'];
        $vehicle['vehicle_type'] = $val['vehicle_type'];
        $vehicle['vehicle_number'] = $val['vehicle_number'];
    } else{
        $vehicle['status'] = 0;
        $vehicle['message'] = 'Not Successfully';
    }
    return json_encode($vehicle);
}
function visitor_pass_me_to_add($prk_admin_id,$visitor_gate_pass_dtl_id){
    global $pdoconn;
    $mee_to_add='';
    $sql ="SELECT tower_name,falt_name 
        FROM visitor_service_dtl 
        WHERE visitor_gate_pass_dtl_id='$visitor_gate_pass_dtl_id' 
        AND prk_admin_id='$prk_admin_id' 
        AND active_flag='".FLAG_Y."' 
        AND del_flag='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $mee_to_add.=$val['tower_name'].','.$val['falt_name'].'/';
        }
    }
    return strtoupper(substr($mee_to_add, 0, -1));
}
?>