
<?php 
/*
Description: parking employ today payment type history
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :03-05-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$previous=$_SERVER['HTTP_REFERER'];
$response = array();
$resp = array();
if(isAvailable(array('prk_admin_id','payment_rec_emp_name','payment_type','start_date','end_date'))){
    if(isEmpty(array('prk_admin_id','payment_rec_emp_name','payment_type'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $payment_rec_emp_name = trim($_POST['payment_rec_emp_name']);
        $payment_type = trim($_POST['payment_type']);
        $start_date = trim($_POST['start_date']);
        $end_date = trim($_POST['end_date']);
        $prk_user= prk_user_payment_type_trn($prk_admin_id,$payment_rec_emp_name,$payment_type,$start_date,$end_date);
    $res=json_decode($prk_user,true);
    $words=api_number_to_word($res['total_pay']);
    /*foreach ($res['payment_history'] as $key => $item){
        foreach ($item as $key_p => $values) {
        }
    }*/
    // echo($prk_user);
    // $words=api_number_to_word($res['total']);
    $html="<html>
        <head>
        <style type='text/css'>
            @media print
            {
            @page {
            margin-top: 0px;
            margin-bottom: 0px;
            font-size:10px;
            margin: 0;
            }
            body  {
            padding-top: 3px;
            padding-bottom: 72px ;
            font-size:10px;
            }
            }
            .tg  {border-spacing:0;width: 100%;margin-top: 5px;border:1px solid #fff;}
            .tg1  {border-collapse:collapse;border-spacing:0;width: 100%;}
            .tg td{font-family:Arial, sans-serif;font-size:10px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
            .tg th{font-family:Arial, sans-serif;font-size:10px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
            .tg .tg-xldj{border-color:inherit;text-align:left}
            .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
        </style>
        </head>
        <body>
        <table class='tg1'>
          <tr>
            <th class='tg-s268' colspan='2' style='width: 40%;'><img style='height: 70px;' src='../../../img/logo_pdf.png'></th>
            <th class='tg-s268' colspan='3' style='font-weight: 100;font-size: 12px;width: 30%;'>".$res['prk_area_name']."<br>Vehicle Transaction Report</th>
            <th class='tg-0lax' colspan='3' style='font-weight: 100;font-size: 12px;width: 30%;'>Start Date: ".$res['start_date']."<br>End Date: ".$res['end_date']."</th>
          </tr>
        </table>
        <table class='tg'>
            <tr style='border-bottom:1px solid #e4e0e0'>
                <th class='tg-xldj'><b>Vehicle Type</b></th>
                <th class='tg-xldj'><b>Vehicle Number</b></th>
                <th class='tg-0pky'><b>Payment Type</b></th>
                <th class='tg-0pky'><b>Payment Receive Date</b></th>
                <th class='tg-0pky'><b>Payment Receive Time</b></th>
                <th class='tg-0pky'><b>Total Pay</b></th>
            </tr>";
            $html3="";
           foreach ($res['payment_history'] as $key => $item){
                foreach ($item as $key_p => $values) {
                    // echo $item['veh_number'];
                    $html1="<tr style='border-bottom:0px solid #e4e0e0'>
                                <td class='tg-0pky'>".$values['vehicle_type_dec']."</td>
                                <td class='tg-0pky'>".$values['veh_number']."</td>
                                <td class='tg-0pky'>".$values['payment_type']."</td>
                                <td class='tg-0pky'>".$values['payment_rec_date']."</td>
                                <td class='tg-0pky'>".$values['payment_rec_time']."</td>
                                <td class='tg-0pky'>".$values['payment_amount']."/-</td>
                            </tr>";
                    $html3.=$html1;
                }
            }
            $html2="
            <tr style='border-bottom:0px solid #e4e0e0'>
                <td class='tg-0pky' colspan='4'></td>
                <td class='tg-0pky'>Grand Total</td>
                <td class='tg-0pky'>".$res['total_pay']."/-</td>
            </tr>
            <tr style='border-bottom:0px solid #e4e0e0'>
                <td class='tg-0pky' colspan='6'>RUPEES ".$words."</td>
            </tr>
        </table>
        </body>
        </html>";
    // echo ($mess);
    $html=$html."".$html3."".$html2;
    echo ($html);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        echo $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo $response = json_encode($response); 
} 
?>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous">
</script>
<script type="text/javascript">
    $(window).on('load', function() {
        window.print();
        setTimeout(function(){window.close();}, 1);
        window.location.href='<?php echo $previous; ?>';
    });
</script>