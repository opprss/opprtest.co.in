<?php
	include 'api/parkAreaReg_api.php';
	@session_start();
	$list_id = isset($_REQUEST['list_id']) ? trim($_REQUEST['list_id']) : ''; 
	$prk_gate_pass_id = base64_decode($list_id);
	$response = array();
	$response = prk_gate_pass_dtl_show($prk_gate_pass_id);
	if (!empty($response)) {
		$veh_owner_name = $response['veh_owner_name'];//
		$tower_name = $response['tower_name'];
		$flat_name = $response['flat_name'];
		$veh_number = $response['veh_number'];//
		$mobile = $response['mobile'];//
		$veh_type = $response['veh_type'];//
		$veh_qr_code=$mobile.'-'.$veh_type.'-'.$veh_number;//
	}
	$prk_admin_id = $_SESSION['prk_admin_id'];
	$prk_inf = prk_inf($prk_admin_id);
	$prk_inf = json_decode($prk_inf, true);
	$prk_area_name = $prk_inf['prk_area_name'];
	$prk_area_short_name = $prk_inf['prk_area_short_name'];
	$prk_area_logo_img = $prk_inf['prk_area_logo_img'];
	
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style type="text/css">
	.circle {
			width: 342px;
			height: 342px;
			border-radius: 50%;
			font-size: 50px;
			line-height: 500px;
			text-align: center;
			background: #fff;
			border: 2px solid black;
		}
		.logo_under_brdr {
		    width: 284px;
	    	margin-left: 27px
		}
		.vl {
			border-left: 3px solid #000;
			height: 157px;
			left: 36px;
			display: block;
			top: -41px
		}
		@media print{
			@page {
			   margin-top: 0px;
			   margin-bottom: 0px;
			   size: A4;
			   height: auto;
			   width: 100%;
			   margin-left: 2px;
			   margin-right: 2px;
			}
			body  {
			   padding-top: 10px;
			   padding-bottom: 10px ;
			   margin-left: 0px;
			}
			 #Header, #Footer { display: none !important; }
			.col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
		        float: left;
		   	}
		   	.col-sm-12 {
		        width: 100%;
		   	}
		   	.col-sm-11 {
		        width: 91.66666667%;
		   	}
		   	.col-sm-10 {
		        width: 83.33333333%;
		   	}
		   	.col-sm-9 {
		        width: 75%;
		   	}
		   	.col-sm-8 {
		        width: 66.66666667%;
		   	}
		   	.col-sm-7 {
		        width: 58.33333333%;
		   	}
		   	.col-sm-6 {
		        width: 50%;
		   	}
		   	.col-sm-5 {
	   	     	width: 41.66666667%;
		   	}
		   	.col-sm-4 {
		        width: 33.33333333%;
		   	}
		   	.col-sm-3 {
		        width: 25%;
		   	}
		   	.col-sm-2 {
		        width: 16.66666667%;
		   	}
		   	.col-sm-1 {
		        width: 8.33333333%;
		   	}
		   	.logo_under_brdr {
			    width: 284px;
		    	margin-left: 27px
			}
			img {
				max-width: none !important;
			}
			.full_div {
				margin-left: 240px;
			}
		}
</style>
<div class="full_div" style="display: block;">
	<div class="circle">
		<center><img style="height: 60px;width:auto;margin-top: 13px;margin-bottom: 2px;display: block;" src="<?php echo LOGO_PDF;?>"></center>
		<div class="col-sm-12 logo_under_brdr" style="border: 1px solid black;display: block;"></div>
	</div>
	<div class="col-sm-12" style="width: 263px;height:auto;top: -223px;margin-left: 18px;">
		<div class="row" >
			<div class="col-sm-5" style="display: block;">
				<div class="row">
					<div class="col-sm-12" style="top: -25px;font-size: 14px;display: block;"><b><?php echo $prk_area_name; ?></b></div>
					<div class="col-sm-12" style="display: block;"><img style="height: 68px;width:95px;margin-top: -12px;" src="<?php echo PRK_BASE_URL.$prk_area_logo_img;?>"></div>
				</div>
			</div>
			<div class="col-sm-2 vl"></div>
			<div class="col-sm-5" style="top: -34px;right: 15px;display: block;"><img style="height: 144px !important;width: 144px !important;" src="../../library/QRCodeGenerate/generate.php?text=<?php echo $veh_qr_code;?>" alt=""></div>
		</div>
		<div class="col-sm-12" style="border: 1px solid black;width: 314px;left: -20px;top: -41px;display: block;"></div>
		<div class="col-sm-12" style="top: -32px;display: block;">
			<div class="row">
				<div class="col-sm-12" style="margin-left: 0px;font-size: 14px;"><b>Name:</b> <?php echo ucwords(strtolower($veh_owner_name)); ?></div>
				<div class="col-sm-12" style="top: 5px;margin-left: 12px;"><b>Tower:</b> <?php echo $tower_name; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Flat:</b> <?php echo $flat_name; ?></div>
				<div class="col-sm-12" style="top: 11px;margin-left: 43px;"><b>Vehicle No:</b> <?php echo $veh_number; ?></div>
			</div>
		</div>
	</div>
</div>


<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous">
</script>
<script type="text/javascript">
	$(window).on('load', function() {
	 	window.print();
		setTimeout(function(){window.close();}, 1);
	});
</script>
