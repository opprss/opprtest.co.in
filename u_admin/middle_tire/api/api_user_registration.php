<?php
/*
Description: User Registration.
Developed by: Rakhal Raj Mandal
Created Date: 01-01-2018
Update date : 06-05-2018
*/
function registration($mobile,$password,$name,$t_c_flag,$device_token_id,$device_type){
    global $pdoconn;
    $response = array();
    $resp = array();
    $resp =accountStatus($mobile);
    $json = json_decode($resp);
    if($json->status=='1'){
        $password=md5($password);
        $response = array();
            #-------User Registration---------#
        $sql = "INSERT INTO `user_admin`(`user_mobile`,`user_password`,`user_ac_status`,`inserted_by`,`inserted_date`,`t_c_flag`) VALUE ('$mobile','$password','".FLAG_D."','$mobile','".TIME."','$t_c_flag')";
        $query = $pdoconn->prepare($sql);
        $query->execute();
        $user_admin_id = $pdoconn->lastInsertId();
            #-------User Details---------#
        $sql = "INSERT INTO `user_detail`(`user_name`,`user_nick_name`,`user_admin_id`,`inserted_by`,`inserted_date`) VALUE ('$name','$name','$user_admin_id','$mobile','".TIME."')";
        $query = $pdoconn->prepare($sql);
        if($query->execute()){
            $token = sha1(uniqid());
            if(tokenCheck($user_admin_id)){
                $sql = "INSERT INTO `token` (`user_admin_id`, `token_value`, `device_token_id`, `device_type`, `start_date`) VALUES ('$user_admin_id', '$token','$device_token_id','$device_type','".TIME."')";
                $query = $pdoconn->prepare($sql);
                $query->execute();  
                if ($query){
                    $response['status'] = 1;
                    $response['user_admin_id'] = $user_admin_id;
                    $response['mobile'] = $mobile;
                    $response['token'] = $token;
                    $response['name'] = $name;
                    $response['user_ac_status'] = 'A';
                    $response['message'] = 'Registration Successful';
                    return json_encode($response); 
                }else{
                    $response['status'] = 0;
                    $response['token'] = 0;
                    $response['message'] = 'Token Not Found';
                    return json_encode($response);
                }
            }
        }
    }else{
        return ($resp);
    }
}
?>
