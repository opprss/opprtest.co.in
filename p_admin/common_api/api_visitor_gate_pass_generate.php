<?php
/*
Description: Parking adimn visttor gate pass genaret for user.
Developed by: Rakhal Raj Mandal
Created Date: 31-02-2018
Update date : 21-06-2019
*/ 
function visitor_gate_pass_generate($prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $prk_in =prk_inf($prk_admin_id);
    $json = json_decode($prk_in);
    if($json->status=='1'){
        $prk_admin_name = $json->prk_admin_name;
        $prk_area_short_name = $json->prk_area_short_name;
        $gate=$prk_area_short_name."-VISITOR-";
        $sql = "SELECT max(`visitor_gate_pass_num`) AS gate_pass FROM `visitor_gate_pass_dtl` WHERE `prk_admin_id` ='$prk_admin_id' AND `visitor_gate_pass_num` LIKE '%$gate%'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $val = $query->fetch();
        $gate_pass= $val['gate_pass'];
        $piec = explode("-", $gate_pass);//$pieces[0]
        if(!empty($gate_pass))
        {
            $gate_pass=$piec[2]+1;
        }else{
            $gate_pass=1;
        }
        $gate_pass = sprintf("%'.04d", $gate_pass);
        $gate=$gate."".$gate_pass;
        $response['status'] = 1;
        $response['message'] = 'Data Sucessfull';
        $response['gate_pass'] = $gate;
    }else{
        $response['status'] = 0;
        $response['message'] = 'Data Not Successfull';
    }
    return json_encode($response);
}
?>