<?php
/*
Description: user profile show show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_profile_show($user_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    // $sql = "SELECT * FROM `user_detail` WHERE `user_admin_id`='$user_admin_id' AND `active_flag`='".FLAG_Y."'";
    $sql = "SELECT `user_detail`.`user_detail_id`,
        `user_detail`.`user_name`,
        `user_detail`.`user_nick_name`,
        `user_detail`.`user_gender`,
        `user_detail`.`user_dob`,
        `user_detail`.`user_email`,
        `user_detail`.`user_email_verify`,
        `user_detail`.`user_aadhar_id`,
        `user_detail`.`user_img`,
        `user_admin`.`user_mobile`,
        `user_admin`.`ad_flag`,
        `user_admin`.`sms_flag`,
        `user_admin`.`visitor_verify_flag`
        FROM `user_detail`,`user_admin` 
        WHERE `user_detail`.`user_admin_id`='$user_admin_id' 
        AND `user_detail`.`active_flag`='".FLAG_Y."'
        AND `user_admin`.`user_admin_id`=`user_detail`.`user_admin_id`";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $response['status'] = 1;
    $response['message'] = 'Successful';
    $response['user_detail_id'] = $val['user_detail_id'];
    $response['user_name'] = $val['user_name'];
    $response['user_nick_name'] = $val['user_nick_name'];
    $response['user_gender'] = $val['user_gender'];
    $response['user_dob'] = $val['user_dob'];
    $response['user_email'] = $val['user_email'];
    $response['user_email_verify'] = $val['user_email_verify'];
    $response['user_aadhar_id'] = $val['user_aadhar_id'];
    $response['user_img'] = $val['user_img'];
    $response['user_mobile'] = $val['user_mobile'];
    $response['ad_flag'] = $val['ad_flag'];
    $response['sms_flag'] = $val['sms_flag'];
    $response['visitor_verify_flag'] = $val['visitor_verify_flag'];
    return json_encode($response);
}// user_name,user_nick_name,user_gender,user_dob,user_email,user_email_verify,user_aadhar_id,user_img 
?>