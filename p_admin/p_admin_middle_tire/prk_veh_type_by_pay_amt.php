<?php 
/*
Description: Parking area all payment history.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','start_date','end_date','token'))){
    if(isEmpty(array('prk_admin_id','token'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $start_date = trim($_POST['start_date']);
        $end_date = trim($_POST['end_date']); 
        $token = ($_POST['token']);
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            $response= prk_veh_type_by_pay_amt($start_date,$end_date,$prk_admin_id);
        }else{
            $response=$resp;
        }
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response= json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response= json_encode($response); 
}
echo ($response);
?>