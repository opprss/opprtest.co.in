<?php 
/*  Description:  User kid add.
    Developed by: Rakhal Raj Mandal
    Created Date: 06-05-2019
    Update date : 06-05-2019
*/ 
include_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_admin_id','mobile','token','prk_admin_id','user_family_id','alone_flag','rep_name','rep_image','date','from_time','to_time'))){
    if(isEmpty(array('user_admin_id','mobile','token','prk_admin_id','user_family_id','alone_flag','date','from_time','to_time'))){
        $user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
        $token = trim($_POST['token']);
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $user_family_id = trim($_POST['user_family_id']);
        $alone_flag = ($_POST['alone_flag']);
        $rep_name = ($_POST['rep_name']);
        $rep_image = ($_POST['rep_image']);
        $date = ($_POST['date']);
        $from_time = ($_POST['from_time']);
        $to_time = ($_POST['to_time']);
        $call_type = isset($_POST['call_type']) ? trim($_POST['call_type']) : 'M'; 
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $response = user_kids_add($user_admin_id,$mobile,$prk_admin_id,$user_family_id,$alone_flag,$rep_name,$rep_image,$date,$from_time,$to_time,$call_type);
        }else{
            $response= $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response= json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response= json_encode($response);
}
echo ($response);
?>