<!-- ################################################
  
Description: Parking area profile show
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
include "all_nav/header.php";
if (isset($_SESSION['prk_admin_id'])) {

  $user_name=$_SESSION['prk_admin_id'];
  $prk_admin_id = $_SESSION['prk_admin_id'];

  $response = array();

  //for basic info
  $response = prk_area_profile_show($prk_admin_id);
  $response = json_decode($response, true);

  if (!empty($response)) {
    $prk_area_name = $response['prk_area_name'];
    $prk_area_email = $response['prk_area_email'];
    $prk_area_rep_name = $response['prk_area_rep_name'];
    $prk_area_rep_mobile = $response['prk_area_rep_mobile'];
    $prk_email_verify = $response['prk_email_verify'];
    $prk_mobile_verify = $response['prk_mobile_verify'];
    $prk_area_pro_img = $response['prk_area_pro_img'];
  }

  //for address
  $response_address = prk_area_address_show($prk_admin_id);
  $response_address = json_decode($response_address, true);
  if (!empty($response_address)) {
    $prk_address = $response_address['prk_address'];
    $prk_land_mark = $response_address['prk_land_mark'];
    $prk_add_area = $response_address['prk_add_area'];
    $prk_add_city = $response_address['prk_add_city'];
    $city_name = $response_address['city_name'];
    $prk_add_state = $response_address['prk_add_state'];
    $state_name = $response_address['state_name'];
    $prk_add_country = $response_address['prk_add_country'];
    $prk_add_pin = $response_address['prk_add_pin'];
  }
}

  

?>

<!-- header position -->

    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Edit Profile</h5>
        <!-- <form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
      
     <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card single col-md-12">
          <div class="row">
              <div class="col-md-12 text-center pd-20 pd-sm-40 mg-b-40 profile-body" style="background-image: url('<?php echo IMAGE_URL ?>prof-bg-black.jpg');">
                <?php
                  if (!empty($prk_area_pro_img)) {
                ?>
                  <img src="<?php echo PRK_BASE_URL.$prk_area_pro_img;?>" class="rounded-circle" alt="" style="width: 100px;">
                <?php
                  }else{
                ?>
                  <i class="fa fa-user-circle-o tx-info tx-60 tx"></i>
                <?php
                  }
                 ?>
                 <h2 class="prk-name"><?php echo $prk_area_name ?></h2>

                <?php
                if (!empty($prk_address)) {
                ?>
                <p class="prk-address mg-b-0"><i class="fa fa-map-marker"></i>&nbsp; 
                <?php if(empty($prk_land_mark)){
                    echo $prk_address.', '.$prk_add_area.', '.$city_name.','.$state_name.', '.$prk_add_country.'-'.$prk_add_pin; 
                  }else{
                  echo $prk_address.' ('.$prk_land_mark.') '.$prk_add_area.', '.$city_name.','.$state_name.', '.$prk_add_country.'-'.$prk_add_pin; 
                  }
                  ?>
                </p>
                <?php
                }else{
                ?>
                  <a href="vender-address" style="color: #FFF"><i class="fa fa-pencil"></i> Add address </a>
                <?php
                }
                ?>
              </div>
              <div class="col-md-4 pd-b-100">
                <h5>
                  Representative Information
                  <a href="parking-area-profile-edit" class="mg-b-20 pull-right" data-toggle="tooltip" data-placement="top" title="Modify basic">
                    <i class="fa fa-pencil"></i></a>
                </h5>
                <hr>
                <h6><?php echo $prk_area_rep_name ?></h6>
                  <div class="tx-15">
                    <p class="mg-b-0">Email - <?php echo $prk_area_email ; //echo "---".$prk_email_verify?> 
                      <?php
                        if ($prk_email_verify == SET_T) {
                         ?>
                         <font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>
                         <?php 
                        }else{
                        ?>
                        <a class="verify" href="verify-email" data-toggle="tooltip" data-placement="top" title="Verify">
                        <i class="fa fa-times"></i>
                      </a>
                        <?php
                        }
                       ?>
                    </p>

                    <p class="mg-b-0">Mobile - <?php echo $prk_area_rep_mobile?> 
                      <?php
                        if ($prk_mobile_verify == SET_T) {
                         ?>
                         <font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>
                         <?php 
                        }else{
                        ?>
                        <a class="verify" href="verify-mobile" data-toggle="tooltip" data-placement="top" title="Verify">
                        <i class="fa fa-times"></i>
                      </a>
                        <?php
                        }
                       ?>
                    </p>
                  </div>
              </div>
          </div>
        </div><!-- card -->
      </div><!-- am-pagebody -->

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">

 $('a.delete').confirm({
    title: "Are you sure?",
    content: "<p style='font-size:0.7em;'>It will delete the item permanently</p>",
  });
  $('a.delete').confirm({
      buttons: {
         btnClass: 'btn-red',
          hey: function(){
              location.href = this.$target.attr('href');
          }
      }
  });
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

        
</script>