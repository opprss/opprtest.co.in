<!-- ################################################
Description: List of user's vechile | user can delete vehicle from this page.
Developed by: Soemen Banerjee
Created Date: 17-03-2018
 ####################################################-->
<?php  
  include "all_nav/header.php";
  $user_relation_id = isset($_REQUEST['list_id']) ? base64_decode(trim($_REQUEST['list_id'])) : '';
  $previous_url=$_SERVER['HTTP_REFERER'];
  $transaction_list=user_to_user_relation_transaction_list($user_admin_id,$user_relation_id);
  $tran_his = json_decode($transaction_list, true);
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
  .error{
    font-size: 11px;
    color: red;
  }
  th,td{
    text-align: center;
  }
</style>
<script src="../lib/validation/jquery.validate.js"></script>
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
  <div class="am-mainpanel">
    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Apartment Transaction History</h5>
    </div>
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40">
        <div class="row" style="margin-top: -25px;">
          <div class="col-md-12">
            <div class="form-layout">
              <div class="row mg-b-1">
                <div class="col-md-10"></div>
                <div class="col-md-2 error_show pull-right">
                  <div class="form-group">
                      <button type="button" class="btn btn-block user_button" name="add_vehicle" id="add_vehicle" onclick="document.location.href='<?=$previous_url?>';">Back</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="table-wrapper">
          <table id="datatable1" class="table display" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="wd-5p">Sl No.</th>
                <th class="wd-20p">Apartment Name</th>
                <th class="wd-20p">User Name</th>
                <th class="wd-10p">Amount</th>
                <th class="wd-10p">Payment Type</th>
                <th class="wd-10p">Transaction Date</th>
                <th class="wd-10p">Transaction Time</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $x=1;
                if ($tran_his['status']){
                foreach($tran_his['transaction_list'] as $value){
              ?>
              <tr>
                <td><?=$x; ?></td>
                <td><?=$value['user_apar_name']; ?></td>
                <td><?=$value['user_relation_name']; ?></td>
                <td><?=$value['trn_amount']; ?></td>
                <td><?=$value['payment_type']; ?></td>
                <td><?=$value['transaction_date']; ?></td>
                <td><?=$value['transaction_time']; ?></td>
              </tr>
              <?php $x++; }} ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  <?php include"all_nav/footer.php"; ?>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>
