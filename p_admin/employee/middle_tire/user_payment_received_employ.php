<?php 
/*
Description: parking employ payment resive
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
//include_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('prk_veh_trc_dtl_id','payment_type','prk_user_username'))){
 	if(isEmpty(array('prk_veh_trc_dtl_id','payment_type','prk_user_username'))){

	 	$prk_veh_trc_dtl_id = trim($_POST['prk_veh_trc_dtl_id']);
	 	$payment_type = $_POST['payment_type'];
        $prk_user_username =$_POST['prk_user_username'];
        
        $response= user_payment_received_employ($prk_veh_trc_dtl_id,$payment_type,$prk_user_username);
        echo ($response);
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response);
 }
?>