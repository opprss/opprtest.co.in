<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function prk_admin_form_list_show2($form_seq,$group_form_id,$prk_admin_id){
    $allplayerdata = array();
    $response = array();
    $vehicle = array();
    global $pdoconn;
    $sql = "SELECT for1.`form_id`,
        for2.`form_display_name` as form_display_name2,
        for2.`form_order` as form_order2,
        for1.`form_order`,
        for1.`form_name`,
        for1.`form_display_name`,
        for1.`form_php_name`,
        for1.`group_form_id`,
        for1.`form_seq`,
        for1.`form_img` 
        FROM `form_dtl` for1,`form_dtl` for2,prk_admin_permission pap
        where for1.`form_id`=pap.`form_id`
        AND for1.`active_flag`='Y' 
        AND for1.`del_flag`='N' 
        AND for1.`form_seq`='$form_seq' 
        AND for1.`group_form_id`<>'$group_form_id'
        AND for2.`form_id`=for1.`group_form_id`
        AND pap.`prk_admin_id`='$prk_admin_id'
        AND pap.`active_flag`='Y' 
        AND pap.`del_flag`='N' 
        ORDER BY 3,4 ASC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $resp['form_id'] = $val['form_id'];
            $resp['form_php_name'] = $val['form_php_name'];
            $resp['form_display_name'] = $val['form_display_name'];
            $resp['form_img'] = $val['form_img'];
            $allplayerdata[$val['form_display_name2']][] = $resp;
        }
        $response['status'] = 1;
        $response['message'] = 'Successfully';
        $response['form_dtl'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
/*function prk_admin_form_list_show2($rowid,$prk_admin_id){
    $allplayerdata = array();
    $response = array();
    $vehicle = array();
    global $pdoconn;

    $sql="SELECT `form_dtl`.`form_id`,
        `form_dtl`.`form_php_name`,
        `form_dtl`.`form_display_name`,
        `form_dtl`.`form_img` 
        FROM `form_dtl`,`prk_admin_permission`
        WHERE `form_dtl`.group_form_id='$rowid'
        AND `form_dtl`.`form_id`=`prk_admin_permission`.`form_id`
        AND `prk_admin_permission`.`prk_admin_id`='$prk_admin_id'
        AND `prk_admin_permission`.`active_flag`='".FLAG_Y."' 
        AND `prk_admin_permission`.`del_flag`='".FLAG_N."'
        AND `form_dtl`.`active_flag`='".FLAG_Y."' 
        AND `form_dtl`.`del_flag`='".FLAG_N."'
        ORDER BY `form_dtl`.`form_order`";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val) 
        {
            $vehicle['form_id'] = $val['form_id'];
            $vehicle['form_php_name'] = $val['form_php_name'];
            $vehicle['form_display_name'] = $val['form_display_name'];
            $vehicle['form_img'] = $val['form_img'];
            array_push($allplayerdata, $vehicle);
        }
        $response['status'] = 1;
        $response['message'] = 'Successfully';
        $response['form_dtl'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}*/
?>