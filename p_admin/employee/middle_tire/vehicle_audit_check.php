<?php 
/*
Description: Parking visiter in list
Developed by: Rakhal Raj Mandal
Created Date: 31-05-2018
Update date : 
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','token','veh_number'))){
    if(isEmpty(array('prk_admin_id','prk_area_user_id','token','veh_number'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $token = trim($_POST['token']);
        $veh_number = trim($_POST['veh_number']);
        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $gate_pass_find = prk_gate_pass_find($prk_admin_id,$veh_number);
            $gate_pass = json_decode($gate_pass_find);
            if($gate_pass->status=='1'){
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['park_veh_for'] = 'OWN_VEH';
                $response['prk_gate_pass_id'] = $gate_pass->prk_gate_pass_id;
                $response['user_admin_id'] = $gate_pass->user_admin_id;
                $response['veh_type'] = $gate_pass->veh_type;
                $response['veh_number'] = $gate_pass->veh_number;
                $response['tower_id'] = $gate_pass->tower_id;
                $response['tower_name'] = $gate_pass->tower_name;
                $response['flat_id'] = $gate_pass->flat_id;
                $response['flat_name'] = $gate_pass->flat_name;
                $response['park_lot_no'] = $gate_pass->park_lot_no;
                $response['owner_name'] = $gate_pass->veh_owner_name;
                $response['vehicle_type_dec'] = $gate_pass->vehicle_type_dec;
                $response['vehicle_typ_img'] = $gate_pass->vehicle_typ_img;
                $response['mobile'] = $gate_pass->mobile;
            }else{
                $sql = "SELECT pvtd.gate_pass_num AS prk_gate_pass_id,
                    uad.user_admin_id,
                    pvtd.prk_veh_type as veh_type,
                    pvtd.veh_number,
                    pvd.tower_id,
                    td.tower_name,
                    pvd.flat_id,
                    fd.flat_name,
                    vpd.vis_prk_full_name as park_lot_no,
                    pvd.visitor_meet_to_name as owner_name,
                    vty.vehicle_type_dec,
                    vty.vehicle_typ_img,
                    pvd.visit_meet_to as mobile
                    FROM prk_visit_dtl pvd JOIN prk_veh_trc_dtl pvtd JOIN visitor_parking_dtl vpd JOIN vehicle_type vty
                    JOIN tower_details td ON pvd.tower_id=td.tower_id
                    AND td.active_flag='".FLAG_Y."'
                    AND td.del_flag='".FLAG_N."'
                    JOIN flat_details fd ON pvd.flat_id=fd.flat_id
                    AND fd.active_flag='".FLAG_Y."'
                    AND fd.del_flag='".FLAG_N."'
                    LEFT JOIN user_admin uad ON pvd.visit_meet_to = uad.user_mobile
                    WHERE vty.vehicle_sort_nm=pvtd.prk_veh_type
                    AND vty.active_flag='".FLAG_Y."'
                    AND vpd.prk_admin_id=pvd.prk_admin_id
                    AND vpd.vis_prk_id=pvtd.gate_pass_num
                    AND vpd.active_flag='".FLAG_Y."'
                    AND vpd.del_flag='".FLAG_N."'
                    AND pvtd.prk_admin_id=pvd.prk_admin_id
                    AND pvtd.prk_veh_trc_dtl_id=pvd.prk_veh_trc_dtl_id
                    AND pvtd.prk_veh_out_time IS NULL
                    AND pvd.end_date is NULL 
                    AND pvd.visit_with_veh='".FLAG_Y."'
                    AND pvd.prk_admin_id='$prk_admin_id'
                    AND pvtd.veh_number='$veh_number'";
                $query  = $pdoconn->prepare($sql);
                $query->execute();
                $count=$query->rowCount();
                if($count>0){
                    $val = $query->fetch();
                    $response['status'] = 1;
                    $response['message'] = 'Successful';
                    $response['park_veh_for'] = 'VIS_VEH';
                    $response['prk_gate_pass_id'] = $val['prk_gate_pass_id'];
                    $response['user_admin_id'] = $val['user_admin_id'];
                    $response['veh_type'] = $val['veh_type'];
                    $response['veh_number'] = $val['veh_number'];
                    $response['tower_id'] = $val['tower_id'];
                    $response['tower_name'] = $val['tower_name'];
                    $response['flat_id'] = $val['flat_id'];
                    $response['flat_name'] = $val['flat_name'];
                    $response['park_lot_no'] = $val['park_lot_no'];
                    $response['owner_name'] = $val['owner_name'];
                    $response['vehicle_type_dec'] = $val['vehicle_type_dec'];
                    $response['vehicle_typ_img'] = $val['vehicle_typ_img'];
                    $response['mobile'] = $val['mobile'];
                }else{
                    $response['status'] = 0;
                    $response['message'] = 'Gate Pass Not Issue';
                }
            }
            $response = json_encode($response); 
        }else{

            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response); 
} 
echo $response;
?> 