<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function meet_to_person_list($prk_admin_id){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
    $sql = "SELECT `me_to_per_id`,
        `me_to_per_name`,
        `me_to_per_mobile`,
        `me_to_per_department`,
        `prk_admin_id`,
        DATE_FORMAT(`eff_date`,'%d-%b-%Y') AS `eff_date`,
        DATE_FORMAT(`end_date`,'%d-%b-%Y') AS `end_date`
        FROM `meet_to_person_list` 
        WHERE `prk_admin_id`='$prk_admin_id' 
        AND `active_flag`='".FLAG_Y."' 
        AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['me_to_per_id'] = $val['me_to_per_id'];
            $park['me_to_per_name'] = $val['me_to_per_name'];
            $park['me_to_per_mobile'] = $val['me_to_per_mobile'];
            $park['me_to_name_mobile'] = $val['me_to_per_name'].'||'.$val['me_to_per_mobile'].'||'.$val['me_to_per_department'];
            $park['me_to_per_department'] = $val['me_to_per_department'];
            $park['prk_admin_id'] = $val['prk_admin_id'];
            $park['eff_date'] = $val['eff_date'];
            $park['end_date'] = $val['end_date'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['meet_to_person_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
function meet_to_person_show($prk_admin_id,$me_to_per_id){
    global $pdoconn;
    $sql = "SELECT `me_to_per_id`,
        `me_to_per_name`,
        `me_to_per_mobile`,
        `me_to_per_department`,
        `prk_admin_id`,
        DATE_FORMAT(`eff_date`,'%d-%m-%Y') AS `eff_date`,
        DATE_FORMAT(`end_date`,'%d-%m-%Y') AS `end_date`
        FROM `meet_to_person_list` 
        WHERE `prk_admin_id`='$prk_admin_id' 
        AND `me_to_per_id`='$me_to_per_id' 
        AND `active_flag`='".FLAG_Y."' 
        AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['me_to_per_id'] = $val['me_to_per_id'];
        $response['me_to_per_name'] = $val['me_to_per_name'];
        $response['me_to_per_mobile'] = $val['me_to_per_mobile'];
        $response['me_to_per_department'] = $val['me_to_per_department'];
        $response['prk_admin_id'] = $val['prk_admin_id'];
        $response['eff_date'] = $val['eff_date'];
        $response['end_date'] = $val['end_date'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
function meet_to_person_delete($prk_admin_id,$me_to_per_id,$parking_admin_name){
    global $pdoconn;
    $sql = "UPDATE `meet_to_person_list` SET `updated_by`='$parking_admin_name',`updated_date`='".TIME."',`del_flag`='".FLAG_Y."' WHERE `prk_admin_id`='$prk_admin_id' AND `me_to_per_id`='$me_to_per_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Delete Successful';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Delete Not Successful';
    }
    return json_encode($response);
}
function meet_to_person_add($prk_admin_id,$parking_admin_name,$me_to_per_id,$me_to_per_name,$me_to_per_mobile,$me_to_per_department,$eff_date,$end_date){
    if(!empty($eff_date)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($eff_date)) == date($eff_date)) {
            $eff_date= $eff_date;
        } else {
            $eff_date = date("Y-m-d", strtotime($eff_date));
        }
    }else{
        $eff_date= TIME_TRN;
    }
    if(!empty($end_date)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($end_date)) == date($end_date)) {
            $end_date=  $end_date ;
        } else {
            $end_date = date("Y-m-d", strtotime($end_date));
        }
    }else{
        $end_date= FUTURE_DATE;
    }
    global $pdoconn;
    $sql="INSERT INTO `meet_to_person_list`(`me_to_per_id`, `me_to_per_name`, `me_to_per_mobile`, `me_to_per_department`, `prk_admin_id`, `eff_date`, `end_date`, `inserted_by`, `inserted_date`) VALUES ('$me_to_per_id','$me_to_per_name','$me_to_per_mobile','$me_to_per_department','$prk_admin_id','$eff_date','$end_date','$parking_admin_name','".TIME."')";
    $query  = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['me_to_per_id'] = $me_to_per_id;
    }else{
        $response['status'] = 0;
        $response['message'] = 'Unsuccessfully';
    }
    return json_encode($response);
}
function meet_to_person_edite($prk_admin_id,$parking_admin_name,$me_to_per_id,$me_to_per_name,$me_to_per_mobile,$me_to_per_department,$eff_date,$end_date){
    global $pdoconn;
    $sql = "UPDATE `meet_to_person_list` SET `updated_by`='$parking_admin_name',`updated_date`='".TIME."',`active_flag`='".FLAG_N."' WHERE `prk_admin_id`='$prk_admin_id' AND `me_to_per_id`='$me_to_per_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    if($query->execute()){
        // $response['status'] = 1;
        // $response['message'] = 'Delete Successful';
        $response = meet_to_person_add($prk_admin_id,$parking_admin_name,$me_to_per_id,$me_to_per_name,$me_to_per_mobile,$me_to_per_department,$eff_date,$end_date);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
        $response = json_encode($response);
    }
    return($response);
}
function meet_to_person_search($prk_admin_id,$me_to_name){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
    $sql = "SELECT '' as user_admin_id,
        mtpl.`me_to_per_mobile` as 'meet_mobile',
        mtpl.`me_to_per_name` as user_name,
        '' as 'tower_id',
        '' as 'flat_id',
        'Y' as 'app_use',
        mtpl.`me_to_per_department`,
        'NA' as 'tower_name',
        'NA'  as 'flat_name',
        pad.`prk_area_name`
        FROM `meet_to_person_list` mtpl,`prk_area_dtl` pad
        WHERE pad.`prk_admin_id`= mtpl.`prk_admin_id`
        AND pad.`active_flag`='Y'
        AND mtpl.`prk_admin_id`='$prk_admin_id'
        AND mtpl.`active_flag`='Y'
        AND mtpl.`del_flag`='N'
        AND mtpl.`me_to_per_name` LIKE '%".$me_to_name."%'
        LIMIT 10";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['user_admin_id'] = $val['user_admin_id'];
            $park['meet_mobile'] = $val['meet_mobile'];
            $park['user_name'] = $val['user_name'];
            $park['tower_id'] = $val['tower_id'];
            $park['flat_id'] = $val['flat_id'];
            $park['app_use'] = $val['app_use'];
            $park['tower_name'] = $val['tower_name'];
            $park['flat_name'] = $val['flat_name'];
            $park['prk_area_name'] = $val['prk_area_name'];
            $park['me_to_per_department'] = $val['me_to_per_department'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['meet_to_person_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>