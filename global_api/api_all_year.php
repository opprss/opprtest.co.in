<?php
/*
Description: show the all vehicle type.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function all_year(){
    $allplayerdata = array();
    global $pdoconn;
    $sql = "SELECT `all_drop_down_id`,`sort_name`,`full_name` FROM `all_drop_down` WHERE `category`='YER' AND `del_flag`='".FLAG_N."' ORDER BY `sort_name` ASC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val)
    {
        $response['status'] = 1;
        $response['message'] = 'Successfully';
        $vehicle['sort_name'] = $val['sort_name'];
        $vehicle['full_name'] = $val['full_name'];
        array_push($allplayerdata, $vehicle);
    }
    $response['all_year'] = $allplayerdata;
    return json_encode($response);
}
?>