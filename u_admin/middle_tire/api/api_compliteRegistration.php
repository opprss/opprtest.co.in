<?php
/*
Description: user complite registration only for draft user.
Developed by: Rakhal Raj Mandal
Created Date: 10-02-2018
Update date : 10-05-2018
*/
function compliteRegistration($user_admin_id,$mobile,$password,$name,$t_c_flag){
    global $pdoconn;
    $response = array();
    $resp=forgetPassword($mobile,$password,$password);
    $json = json_decode($resp);
    if($json->status=='1'){
        $sql ="UPDATE `user_admin` SET  `user_ac_status`='A', `t_c_flag`='$t_c_flag',`temp_password`= NULL WHERE `user_admin_id`='$user_admin_id'";
        $query = $pdoconn->prepare($sql);
        $query->execute();
    #----------------------------#
        $sql ="UPDATE `user_detail` SET  `active_flag`='".FLAG_N."', `updated_by`='$mobile', `updated_date`='".TIME."' WHERE `user_admin_id`='$user_admin_id'";
        $query = $pdoconn->prepare($sql);
        $query->execute();        
    #----------------------------#
        $sql = "INSERT INTO `user_detail`(`user_name`,`user_nick_name`,`user_admin_id`,`inserted_by`,`inserted_date`) VALUE ('$name','$name','$user_admin_id','$mobile','".TIME."')";
        $query = $pdoconn->prepare($sql);
        if($query->execute()){
            $response['status'] = 1;
            $response['name'] = $name;
            $response['message'] = 'Update Successful';
            return json_encode($response);
        }else{
            $response['status'] = 0;
            $response['message'] = 'Update Not Save';
            return json_encode($response);
        }
    }else{
        return $resp;
    }
} 
/*function compliteRegistration($user_admin_id,$mobile,$password,$name){
    global $pdoconn;
    $response = array();
    $resp=forgetPassword($mobile,$password,$password);
    $json = json_decode($resp);
    if($json->status=='1'){
        $sql ="UPDATE `user_admin` SET  `user_ac_status`='A', `temp_password`= NULL WHERE `user_admin_id`='$user_admin_id'";
        $query = $pdoconn->prepare($sql);
        $query->execute();
    #----------------------------#
        $sql ="UPDATE `user_detail` SET  `active_flag`='".ACTIVE_FLAG_N."', `updated_by`='$mobile', `updated_date`='".TIME."' WHERE `user_admin_id`='$user_admin_id'";
        $query = $pdoconn->prepare($sql);
        $query->execute();        
    #----------------------------#
        $sql = "INSERT INTO `user_detail`(`user_name`,`user_nick_name`,`user_admin_id`,`inserted_by`,`inserted_date`) VALUE ('$name','$name','$user_admin_id','$mobile','".TIME."')";
        $query = $pdoconn->prepare($sql);
        if($query->execute()){
            $response['status'] = 1;
            $response['message'] = 'Update Successful';
            return json_encode($response);
        }else{
            $response['status'] = 0;
            $response['message'] = 'Update Not Save';
            return json_encode($response);
        }
    }else{
        return $resp;
    }
}
/*function compliteRegistration($user_admin_id,$mobile,$password,$repassword,$email,$name,$dob,$gender){
    global $pdoconn;
    $response = array();
    $resp = array();
    if(emailChecker($email)=='true'){
    	$resp =dofChecker($dob);
    	$json = json_decode($resp);
       if($json->status=='1'){
        $resp=forgetPassword($mobile,$password,$repassword);
        $json = json_decode($resp);
            if($json->status=='1'){
                $sql ="UPDATE `user_admin` SET  `user_ac_status`='A', `temp_password`= NULL WHERE `user_admin_id`='$user_admin_id'";
                $query = $pdoconn->prepare($sql);
                $query->execute();

                $sql ="UPDATE `user_detail` SET  `active_flag`='".ACTIVE_FLAG_N."', `updated_by`='$mobile', `updated_date`='".TIME."' WHERE `user_admin_id`='$user_admin_id'";
                $query = $pdoconn->prepare($sql);
                $query->execute();
                $sql = "INSERT INTO `user_detail`(`user_name`,`user_nick_name`,`user_gender`,`user_dob`,`user_email`,`user_admin_id`,`inserted_by`,`inserted_date`) VALUE ('$name','$name','$gender','$dob','$email','$user_admin_id','$mobile','".TIME."')";
                $query = $pdoconn->prepare($sql);
                if($query->execute()){
                    $response['status'] = 1;
                    $response['message'] = 'Update Successful';
                    return json_encode($response);
                }else{
                    $response['status'] = 0;
                    $response['message'] = 'Update Not Save';
                    return json_encode($response);
                }    
             }else{
                $response=forgetPassword($mobile,$password,$repassword);
                return ($response);
            }
        }else{
            $response=dofChecker($dob);
            return ($response);
        }
    }else{
        $response=emailChecker($email);
        return json_encode($response);
    }
}*/
?>