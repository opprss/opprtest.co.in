<!-- ################################################
  
Description: Tower detalis.
Developed by: Rakhal Raj 
Created Date: 12-03-2019

 ####################################################-->
<?php include "all_nav/header.php";
  $maint_id=base64_decode($_REQUEST['list_id']);
  $crud_type='S';
  $owner_id=base64_decode($_REQUEST['list_id']);
  $maintenance_charge=maintenance_charge($prk_admin_id,'','','','','','','',$parking_admin_name,$crud_type,$maint_id);
  $maintenance_charge_show = json_decode($maintenance_charge, true);

  $category_type='APT_FLT_HLD';
  $prk_user_emp_cat = common_drop_down_list($category_type);
  $prk_user_emp_category = json_decode($prk_user_emp_cat, true);
  $category_type='APT_CH_PAY_TI';
  $payable_time = common_drop_down_list($category_type);
  $payable_time_list = json_decode($payable_time, true);
?>

<!-- header position -->
  <style type="text/css">
    .size{
      font-size: 11px;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .error{
      font-size: 11px;
      color: red;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .success{
      font-size: 11px;
      color: green;
    }
  </style>
  <!-- links for datatable -->
  <link href="../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
  
    <div class="am-mainpanel">

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Manitenance Charge Details Modify<//?=$maintenance_charge?></h5>
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40" >
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form id="park_flat">
              <!-- <form role="form" id="park_flat" method="post"  enctype="multipart/form-data"> -->
              <div class="row">
                <div class="col-lg-8">
                  <div class="row">
                    <div class="col-lg-5">
                      <div class="form-group">
                        <label class="form-control-label size">Type of Maintenance <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control" placeholder="Name" name="maint_type" id="maint_type">
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label size">Maintenance Short Name <span class="tx-danger"></span></label>
                        <input type="text" class="form-control" placeholder="Short Name" name="maint_type_short" id="maint_type_short">
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label class="form-control-label size">Maintenance Charge <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control" placeholder="Charge" name="maint_charge" id="maint_charge">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label size">Payable Time <span class="tx-danger">*</span></label>
                        <div class="select">
                          <select name="payable_time" id="payable_time" style="opacity: 0.8;font-size:14px">
                            <option value="">Select Type</option>
                              <?php
                              foreach ($payable_time_list['drop_down'] as $val){ if($val['sort_name']=='M'){?>  
                                <option value="<?php echo $val['sort_name']?>"><?php echo $val['full_name']?></option>
                              <?php }} ?>
                          </select>
                        </div>
                        <span style="position: absolute; display: none;" id="error_payable_time" class="error">This field is required.</span>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label size">Applicable for <span class="tx-danger">*</span></label>
                        <div class="select">
                          <select name="holder_type" id="holder_type" style="opacity: 0.8;font-size:14px">
                            <option value="">Select Holder</option>
                              <?php
                              foreach ($prk_user_emp_category['drop_down'] as $val){ 
                                if($val['sort_name']!='V'){?>  
                                <option value="<?php echo $val['sort_name']?>"><?php echo $val['full_name']?></option>
                              <?php          
                              }}
                            ?>
                          </select>
                        </div>
                        <span style="position: absolute; display: none;" id="error_holder_type" class="error">This field is required.</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Effective Date <span class="tx-danger">*</span></label>
                    <input type="text" name="effective_date" id="effective_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                    <span style="position: absolute; display: none;" id="error_effective_date" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">End Date </label>
                    <input type="text" name="end_date" id="end_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                   <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2 mg-t-25">

                  <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
                </div>
                <div class="col-lg-2 mg-t-25">

                  <a href="maintenance-charge"><button class="btn btn-block btn-primary prk_button" type="button">BACK</button></a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#end_date', {
      default_date   : false,
      position       : 'bottom',
      format  : 'Y-m-d',
      hide_on_select : true,
      render : function (date) {
        if (date < now) {
          return {
            // disabled : true,
            class_name : 'date-in-past'
          };
        }
        return {};
      } 
    });
  });
</script>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE ?>";
  var prk_admin_id="<?=$prk_admin_id?>";
  var parking_admin_name="<?=$parking_admin_name?>";
  var token="<?=$token?>";
  var maint_id="<?=$maintenance_charge_show['maint_id']?>";
  $("#maint_type").val("<?=$maintenance_charge_show['maint_type']?>");
  $("#maint_type_short").val("<?=$maintenance_charge_show['maint_type_short']?>");
  $("#maint_charge").val("<?=$maintenance_charge_show['maint_charge']?>");
  $("#payable_time").val("<?=$maintenance_charge_show['payable_time']?>");
  $("#holder_type").val("<?=$maintenance_charge_show['holder_type']?>");
  $("#effective_date").val("<?=$maintenance_charge_show['effective_date']?>");
  if ("<?=$maintenance_charge_show['end_date']?>"==FUTURE_DATE) {

    var end_date_o='';
  }else{

    var end_date_o="<?=$maintenance_charge_show['end_date']?>";
  }
  $("#end_date").val(end_date_o);
  var urlPrkAreGateDtl = prkUrl+'maintenance_charge.php';
  // alert(token);
  // $( document ).ready( function () {
  $('#save').click(function(){
    var effective_date=($("#effective_date").val());
    var end_date=($("#end_date").val());

    // var str_array = end_date.split('-');
    // var dd = str_array[0];
    // var mm = str_array[1];
    // var yy = str_array[2];
    // var end_dat = yy+'-'+mm+'-'+dd;
    end_date = (!end_date == '') ? end_date : FUTURE_DATE;
    var maint_type=($("#maint_type").val());
    var maint_type_short=($("#maint_type_short").val());
    var maint_charge=($("#maint_charge").val());
    var payable_time=($("#payable_time").val());
    var holder_type=($("#holder_type").val());
    // alert(end_date);
    // var str_array = effective_date.split('-');
    // var dd = str_array[0];
    // var mm = str_array[1];
    // var yy = str_array[2];
    // var effective_dat = yy+'-'+mm+'-'+dd;
    if($("#park_flat").valid()&effective_date!=''){
      // alert('ok');
       $('#save').val('Wait ...').prop('disabled', true);
      $.ajax({
        url :urlPrkAreGateDtl,
        type:'POST',//'prk_admin_id','maint_type','maint_charge','payable_time','holder_type','effective_date','end_date','parking_admin_name','token','crud_type','maint_id'
        data :{
          'prk_admin_id':prk_admin_id,
          'maint_type':maint_type,
          'maint_type_short':maint_type_short,
          'maint_charge':maint_charge,
          'payable_time':payable_time,
          'holder_type':holder_type,
          'effective_date':effective_date,
          'end_date':end_date,
          'parking_admin_name':parking_admin_name,
          'token': token,
          'crud_type':'U',
          'maint_id':maint_id
        },
        dataType:'html',
        success  :function(data){
          $('#save').val('SAVE').prop('disabled', false);
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>Maintenance Charge Modify successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                  // location.reload(true);
                  window.location.href='maintenance-charge';
                }
              }
            });
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
              type: 'red'
            });
            }
          }
        }
      });
    }
  });
</script>
<script type="text/javascript">
  $( document ).ready( function () {
    $( "#park_flat" ).validate( {
      rules: {
        maint_type: "required",
        maint_charge: "required",
        payable_time: "valueNotEqualspay",
        holder_type: "valueNotEqualshol",
        effective_date: "valueNotEqualsEff"
      },
      messages: {
      }
    });
    $("#effective_date").blur(function () {
      var effective_date = $('#effective_date').val();
      if(effective_date == ''){
        $('#error_effective_date').show();
        return false;
      }else{
        $('#error_effective_date').hide();
        return true;
      }
    });
    $("#payable_time").on('change', function(){
      var v_type = $('#payable_time').val();
      if(v_type == "")
      {
        $('#error_payable_time').show();
        return false;
      }else{
          $('#error_payable_time').hide();
          return true;
      }
    });
    $("#holder_type").on('change', function(){
      var v_type = $('#holder_type').val();
      if(v_type == "")
      {
        $('#error_holder_type').show();
        return false;
      }else{
          $('#error_holder_type').hide();
          return true;
      }
    });
  });
  $.validator.addMethod("valueNotEqualsEff", function(value){
    // alert('ok');
    arg = "";
    if (value != "") {
      return arg !== value;
    }else{
      $("#error_effective_date").show();
      return true;
    }
  });
  $.validator.addMethod("valueNotEqualspay", function(value){
    // alert(value);
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_payable_time").show();
      return false;
    }
  });
  $.validator.addMethod("valueNotEqualshol", function(value){
    // alert(value);
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_holder_type").show();
      return false;
    }
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
  $("#maint_charge").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0)){
      event.preventDefault();
    }
  });
</script>