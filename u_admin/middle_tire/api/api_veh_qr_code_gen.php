<?php
/*
Description: vehicle QR code generator.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function veh_qr_code_gen($veh_number, $veh_type,$mobile,$user_veh_dtl_id){
        global $pdoconn;
        $file = sha1(uniqid());
        $response = array();
        $prk_qr_code=$mobile.'-'.$veh_type.'-'.$veh_number;
        $PNG_TEMP_DIR ='./../uploades/'.$mobile.'/veh_QR_code/';
        include "phpqrcode/qrlib.php";
        if (!file_exists($PNG_TEMP_DIR)) {
            $old_mask = umask(0);
            mkdir($PNG_TEMP_DIR, 0777, TRUE);
            umask($old_mask);
        }
        $filename = $PNG_TEMP_DIR.'opprs.png';
        $errorCorrectionLevel = 'L';
        if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L','M','Q','H')))
            $errorCorrectionLevel = $_REQUEST['level'];   
        $matrixPointSize = 4;
        if (isset($_REQUEST['size']))
            $matrixPointSize = min(max((int)$_REQUEST['size'], 1), 10);
        if (isset($prk_qr_code)) {
            
            //it's very important!
            if (trim($prk_qr_code) == '')
                  die('data cannot be empty! <a href="?">back</a>');
             $filename = $PNG_TEMP_DIR.''.$file.'.png';
           $file='uploades/'.$mobile.'/veh_QR_code/'.$file.'.png';

            $sql_up="UPDATE `user_vehicle_detail` SET `veh_qr_code`='$file' WHERE `user_veh_dtl_id`='$user_veh_dtl_id'";
            $query = $pdoconn->prepare($sql_up);
            $query->execute();

            QRcode::png($prk_qr_code, $filename, $errorCorrectionLevel, $matrixPointSize, 2); 
            $response['status'] = 1;
    	$response['message'] = 'Successful';
        }else{
            $response['status'] = 0;
    	    $response['message'] = 'Not Successful';
        }
        return json_encode($response);
    }
?>