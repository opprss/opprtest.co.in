<?php
/*
Description: Parking area vehicle all rate dtl list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
// parking_admin_name, owner_id & tenant_id not use.
function maintenance_tran_show($prk_admin_id,$year,$form_month,$tower_id,$flat_id,$living_status,$flat_master_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $allplayerda = array();
    $park = array();
    $resp = array();
    $current_date=TIME_TRN;
    $prk_in =prk_inf($prk_admin_id);
    $json = json_decode($prk_in);
    if($json->status=='1'){
        $rate_sq_ft=$json->rate_sq_ft;        
        $sql="SELECT * FROM `flat_master` WHERE `flat_master_id`='$flat_master_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $val = $query->fetch();
        $two_wh_spa_allw =  $val['two_wh_spa_allw'];
        $four_wh_spa_allw =  $val['four_wh_spa_allw'];
        $club_membership =  $val['club_membership'];
        $date = $year.'-'.$form_month;

        $w2 = ($two_wh_spa_allw==FLAG_Y) ? '':'2W';
        // $w2 = ($two_wh_spa_allw==FLAG_Y) ? '2W':'';
        $w4 = ($four_wh_spa_allw==FLAG_Y) ? '':'4W';
        // $w4 = ($four_wh_spa_allw==FLAG_Y) ? '4W':'';
        $club_member = ($club_membership==FLAG_Y) ? '':'CM';

        $sql="SELECT `flat_size` FROM `flat_details` WHERE `flat_id`='$flat_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $val = $query->fetch();
        $flat_size =  $val['flat_size'];
        $sql = "SELECT maint_id,
            maint_type,
            maint_charge,
            payable_time,
            (SELECT `full_name` FROM `all_drop_down` WHERE `sort_name`=`payable_time` AND `category`='APT_CH_PAY_TI') as payable_time_full,
            holder_type,
            `maint_type_short`
            FROM maintenance_charge 
            WHERE prk_admin_id='$prk_admin_id' 
            AND active_flag='".FLAG_Y."' 
            AND del_flag='".FLAG_N."' 
            AND holder_type IN ('$living_status','".FLAG_B."') 
            AND '$date' BETWEEN DATE_FORMAT(effective_date,'%Y-%m') and  DATE_FORMAT(end_date,'%Y-%m') 
            AND IF(`maint_type_short` IS NULL or `maint_type_short` = '', 'A-OPPR', `maint_type_short`)  NOT IN  ('$w2','$w4','$club_member')";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $count=$query->rowCount();
        if($count>0){
            $arr_catagory = $query->fetchAll();
            // $total_pay_m=0;
            $flat_maintenance_amu=($flat_size*$rate_sq_ft);
            $total_pay_m=$flat_maintenance_amu;
            $park['maint_id'] = 'F_M';
            $park['maint_type'] = 'FLAT MAINTENANCE';
            $park['maint_charge'] = $flat_maintenance_amu;
            $park['payable_time'] = 'MON';
            $park['payable_time_full'] = 'MONTHLY';
            array_push($allplayerdata, $park);
            foreach($arr_catagory as $val){
                $park['maint_id'] = $val['maint_id'];
                $park['maint_type'] = $val['maint_type'];
                $park['maint_charge'] = $val['maint_charge'];
                $park['payable_time'] = $val['payable_time'];
                $park['payable_time_full'] = $val['payable_time_full'];
                $total_pay_m+=$val['maint_charge'];
                array_push($allplayerdata, $park);
            }
            $response['status'] = 1;
            $response['message'] = 'Successfully';
            $response['mani_trn_list'] = $allplayerdata; 
            $response['total_pay_m'] = $total_pay_m; 
            // $response['diff'] = $flat_size; 
        }else{
            $response['status'] = 0;
            $response['message'] = 'Transaction Not Parmint For This Month & Year';
        }   
    }else{
        $response['status'] = 0;
        $response['message'] = 'Setupe Not Completed';
    }
    return json_encode($response);
}
?>