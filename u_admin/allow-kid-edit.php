<!-- ################################################
  
Description: user can add or modify address here. add and modify is difided with a reffer(from where page requist is comming that URL) variable in JS.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
  include "all_nav/header.php";
  $kids_allow_id = isset($_REQUEST['list_id']) ? trim(base64_decode($_REQUEST['list_id'])) : 'E';
  $type_kides = isset($_REQUEST['type']) ? trim($_REQUEST['type']) : '';
  $prk_area_list1 = user_add_prk_area_list($user_admin_id);
  $prk_area_list = json_decode($prk_area_list1, true);
  $kids_show1 = user_kids_show($user_admin_id,$user_mobile,$kids_allow_id);
  $kids_show = json_decode($kids_show1, true);
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
  .error{
    font-size: 11px;
    color: red;
  }
  td,th{
    text-align: center;
  }
</style>
<script src="./../js/webcam.min.js"></script>
<script src="js/timepicker.min.js"></script>
<link href="css/timepicker.min.css" rel="stylesheet">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script src="../lib/validation/jquery.validate.js"></script>
<link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../lib/select2/css/select2.min.css" rel="stylesheet">

<div class="am-mainpanel">
    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Allow Kid<?//=$drop_down?></h5>
    </div>
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40" >
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <form role="form" id="family_grp" method="post"  enctype="multipart/form-data">
            <div class="row mg-b-25">
              <div class="col-lg-2">
                <div class="form-group">
                    <label class="form-control-label user-lebel">Area Name<span class="tx-danger">*</span></label>
                    <div class="select" style="font-size:14px">
                    <select name="prk_admin_id" id="prk_admin_id">
                      <option value="">Select Area Name</option>
                      <?php
                        foreach ($prk_area_list['prk_area'] as $val){ ?>  
                          <option value="<?php echo $val['prk_admin_id']?>"><?php echo $val['prk_area_name']?></option>
                        <?php          
                        }
                      ?> 
                    </select>
                    <span style="position: absolute;" id="error_prk_admin_id" class="error_size"></span>
                  </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                    <label class="form-control-label user-lebel">Kid Name<span class="tx-danger">*</span></label>
                    <div class="select" style="font-size:14px">
                    <select name="user_family_id" id="user_family_id">
                      <option value="">Select Kid Name</option>
                    </select>
                    <span style="position: absolute;" id="error_user_family_id" class="error_size"></span>
                  </div>
                </div>
              </div>
              <div class="col-lg-1">
              	<div class="form-group">
                	<label class="form-control-label size">Kid Alone<span class="tx-danger">*</span></label>
                  	<label class="rdiobox">
                    	<input name="alone_flag" id="yes" value="Y" type="radio" checked onclick="javascript:hidden_div_show();">
                    	<span style="font-size: 11px;">Yes </span>
                  	</label>
                    <label class="rdiobox">
                        <input name="alone_flag" id="no" value="N" type="radio" onclick="javascript:hidden_div_show();">
                        <span style="font-size: 11px;">No </span>
                    </label>
              	</div>
              </div>
              <div class="col-lg-7" id="hidden_div" style="visibility: hidden;">
                <div class="row mg-b-25">
                  <div class="col-lg-4" >
      	            <div class="form-group">
      	                <label class="form-control-label size">Person Name<span class="tx-danger">*</span></label>
      	                <input type="text"  name="rep_name" class="form-control" placeholder="Person Name" id="rep_name">
      	                <span style="position: absolute;" id="error_rep_name" class="error_size"></span>
      	            </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <div id="my_camera"></div><br/>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <button type="button" class="btn btn-block btn-primary" onClick="take_snapshot()"><p style="margin-bottom: 0px;font-size: 11px;margin-left: -3px;">Capture<br>Image</p></button>
                      <button type="button" class="btn btn-block btn-primary" id="my-button"><p style="margin-bottom: 0px;font-size: 11px;margin-left: -2px;">Upload<br>Image</p></button>
                      <input type="file" name="visitor_img" accept="image/*" id="visitor_img" style="visibility: hidden;">
                      <input type="hidden" name="user_family_img" class="user_family_img" id="user_family_img">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <div id="results">captured Visitor image will appear here...</div>
                      <!-- <span  id="error_vis_image" class="error_size"></span> -->
                      <label style="position: absolute;" class="error_size" id="error_vis_image"></label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-2" style="margin-top: -80px;">
                <div class="form-group">
                  <label class="form-control-label size">Date<span class="tx-danger">*</span></label>
                    <input type="text"  name="in_date" class="form-control" placeholder="DD-MM-YYYY" id="in_date">
                    <span style="position: absolute;" id="error_date" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-3" style="margin-top: -80px;">
                <div class="row mg-b-25">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="form-control-label size">Time (From)<span class="tx-danger">*</span></label>
                      <input type="text"  name="from_time" class="form-control prevent_readonly" placeholder="HH:MM" id="from_time" readonly="readonly">
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="form-control-label size">Time (To)<span class="tx-danger">*</span></label>
                      <input type="text"  name="to_time" class="form-control prevent_readonly" placeholder="HH:MM" id="to_time" readonly="readonly">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-2" style="margin-top: -80px;">
                <div class="form-group mg-t-35">
                <!-- <div class="form-group"> -->
                  <input type="button" value="SAVE" class="btn btn-block btn-primary wl_user_button" name="save" id="save">
                </div>
              </div>
              <div class="col-lg-2" style="margin-top: -80px;">
                <div class="form-group mg-t-35">
                    <button type="button" class="btn btn-block wl_user_button_skip" name="skip" id="skip" onclick="document.location.href='allow-kid';">Back</button>
                </div>
              </div>
            </div> 
          </form>
        </div>
      </div>
    </div>
<?php include"all_nav/footer.php"; ?>
<script type="text/javascript">
  function hidden_div_show() {
    if (document.getElementById('yes').checked) {
      document.getElementById('hidden_div').style.visibility = 'hidden';
    }else{
      document.getElementById('hidden_div').style.visibility = 'visible';
    }
  }
  var type_kides = '<?=$type_kides?>';
  var kids_allow_id = '<?=$kids_show['kids_allow_id']?>';
  $("#prk_admin_id").val('<?=$kids_show['prk_admin_id']?>');
  var user_family_id = '<?=$kids_show['user_family_id']?>';
  var user_family_name = '<?=$kids_show['user_family_name']?>';
  var areaOption = '<option value="' +user_family_id+ '">' +user_family_name+ '</option>';
  $("#user_family_id").html(areaOption);
  var alone_flag = '<?=$kids_show['alone_flag']?>';
  $('input:radio[name="alone_flag"][value="'+alone_flag+'"]').attr('checked',true);
  if (alone_flag=='Y') {

    document.getElementById('hidden_div').style.visibility = 'hidden';
  }else{
    
    document.getElementById('hidden_div').style.visibility = 'visible';
  }
  $("#rep_name").val('<?=$kids_show['rep_name']?>');
  var rep_image = '<?=$kids_show['rep_image']?>';
  if (rep_image!='') {

    document.getElementById('results').innerHTML = '<img style="height: 100px;width: 130px;" src="'+rep_image+'"/>';
    if (type_kides=='R') {
      // $(".user_family_img").val(rep_image);
    }
  }
  $("#in_date").val('<?=$kids_show['date']?>');
  $("#from_time").val('<?=$kids_show['from_time']?>');
  $("#to_time").val('<?=$kids_show['to_time']?>');
  // alert(type_kides);
  var userUrl = '<?php echo USER_URL; ?>';
  var user_admin_id = '<?=$user_admin_id;?>';
  var mobile='<?=$user_mobile;?>';
  var token='<?=$token;?>';
  $(document).ready(function () {
    addEventListener('DOMContentLoaded', function () {
      var now = new Date;
      var now = now.setDate(now.getDate() - 1);
      pickmeup('#in_date', {
        default_date   : false,
        format  : 'd-m-Y',
        hide_on_select : true,
        render : function (date) {
          if (now > date) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
          }
          return {};
        } 
      });
    });
    $("#rep_name").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 60) && (inputValue != 0 & inputValue !=32)){
        // if(!(inputValue >= 97) && (inputValue != 46)){
        event.preventDefault();
      }
    });
  });
</script>
<style type="text/css">
  ._jw-tpk-container{
    height: 180px;
  }
</style>
<script type="text/javascript">
  var to_time = document.getElementById('to_time');
  var timepicker = new TimePicker(['from_time', 'to_time'], {
      lang: 'en',
      theme: 'dark'
  });
  timepicker.on('change', function(evt) {
      var value = (evt.hour || '00') + ':' + (evt.minute || '00');
      if (evt.element.id === 'to_time') {
        to_time.value = value;
      } else {
        from_time.value = value;
      }
  });
</script>
<script type="text/javascript">
	$( document ).ready( function () {
    // var check_data=$('input:radio[name="alone_flag"]:checked').val();
		$( "#family_grp" ).validate( {
	      rules: {
	        prk_admin_id: "required",
	        user_family_id: "required",
          /*rep_name:  {
            required: function(){
              if(check_data="N"){
                return true;
              }else{
                return false;
              }
            }
          },*/
          in_date: "required",
          from_time: "required",
	        to_time: "required"
	      },
	      messages: {
          
	      }
	    });
		$('#save').click(function () {
			if($("#family_grp").valid()){
				// alert("ok");
        var prk_admin_id = $("#prk_admin_id").val();
        var user_family_id = $("#user_family_id").val();
        var alone_flag = $('input:radio[name="alone_flag"]:checked').val();
        var rep_name = $("#rep_name").val();
        var rep_image = $("#user_family_img").val();
        var date = $("#in_date").val();
        var from_time = $("#from_time").val();
        var to_time = $("#to_time").val();
        var url_is=(type_kides=='R')?'user_kids_add.php':'user_kids_edit.php';
        var user_kids_add = userUrl+''+url_is;
        // 'user_admin_id','mobile','token','prk_admin_id','user_family_id','alone_flag','rep_name','rep_image','date','from_time','to_time','kids_allow_id'
        // alert(user_kids_add);
        $.ajax({
          url :user_kids_add,
          type:'POST',
          data :{
            'user_admin_id':user_admin_id,
            'mobile':mobile,
            'token':token,
            'prk_admin_id':prk_admin_id,
            'user_family_id':user_family_id,
            'alone_flag':alone_flag,
            'rep_name':rep_name,
            'rep_image':rep_image,
            'date':date,
            'from_time':from_time,
            'to_time':to_time,
            'kids_allow_id':kids_allow_id,
            'call_type':'W'
          },
          dataType:'html',
          success  :function(data){
            $('#save').val('SAVE').prop('disabled', false);
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.9em;'>Successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                      // location.reload();
                      window.location='allow-kid';
                  }
                }
              });
            }else{
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.7em;'>Somthing went wrong</p>",
                type: 'red'
              });
              if (json.session==0) {
                window.location.replace("logout.php");
              }
            }
          }
        });
			}
		});
	});  
</script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#user_family_name,#user_family_relation").keypress(function(event){
			var inputValue = event.charCode;
			if(!(inputValue >= 60) && (inputValue != 0 & inputValue !=32)){
			// if(!(inputValue >= 97) && (inputValue != 46)){
			event.preventDefault();
			}
		});
	});
</script>
<script language="JavaScript">
  Webcam.set({
    width: 130,
    height: 100,
    image_format: 'jpeg',
    jpeg_quality: 90
  });
  Webcam.attach( '#my_camera' );
  function take_snapshot() {
    Webcam.snap( function(data_uri) {
      $(".user_family_img").val(data_uri);
      document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
      vis_image();
    });
  }
  $(function() {
    $("#visitor_img").change(function() {
      var file = this.files[0];
      var imagefile = file.type;
      var match= ["image/jpeg","image/png","image/jpg"];
      if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))) {
        $('#previewing').attr('src','../images/noimage.jpg');
        return false;
      }else{
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(this.files[0]);
      }
    });
  });
  function imageIsLoaded(e) {
    // alert('ok');
    $(".user_family_img").val(e.target.result);
    document.getElementById('results').innerHTML = '<img style="height: 100px;width: 130px;" src="'+e.target.result+'"/>';
    vis_image();
  };
  $('#my-button').click(function(){
    $('#visitor_img').click();
  });
</script>
<script type="text/javascript">
  $( document ).ready( function () {
    $('#prk_admin_id').on("change", function() {
      var prk_admin_id=($("#prk_admin_id").val());
      var urlCity = userUrl+'user_kids_dropdown_list.php';
      // 'user_admin_id','mobile','token','prk_admin_id'
      // alert($('input:radio[name="alone_flag"]:checked').val());
      // $('input[name=gender]:checked').val()
      $.ajax ({
        type: 'POST',
        url: urlCity,
        data: {
          'user_admin_id':user_admin_id,
          'mobile':mobile,
          'token':token,
          'prk_admin_id':prk_admin_id
        },
        success : function(data) {
          // alert(data);
          var obj=JSON.parse(data);
          var areaOption = "<option value=''>Select Kid Name</option>";
          $.each(obj['kids_list'], function(val, key) {
            areaOption += '<option value="' + key['user_family_id'] + '">' + key['user_family_name'] + '</option>'
          });
          $("#user_family_id").html(areaOption);
        }
      });
    });
  });
</script>