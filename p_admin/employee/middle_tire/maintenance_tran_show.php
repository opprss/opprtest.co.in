<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','year','form_month','tower_id','flat_id','living_status','flat_master_id'))){
    if(isEmpty(array('prk_admin_id','year','form_month','tower_id','flat_id','living_status','flat_master_id'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
        $year = trim($_POST['year']);
        $form_month = trim($_POST['form_month']);
        $tower_id = trim($_POST['tower_id']);
        $flat_id = trim($_POST['flat_id']);
        $living_status = trim($_POST['living_status']);
        $flat_master_id = trim($_POST['flat_master_id']);

            $response = maintenance_tran_show($prk_admin_id,$year,$form_month,$tower_id,$flat_id,$living_status,$flat_master_id);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>