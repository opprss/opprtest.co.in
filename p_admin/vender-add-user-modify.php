<!-- ################################################
  
Description: Parkig admin can register(login ID and password)/delete/change password/activate-deactivate there employee(security gurd) for there gate managment. with this login credentials parking area employee can lgin intu the parking gate managment mobile app.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
 
<?php 
include "all_nav/header.php";
if (isset($_SESSION['prk_admin_id'])) {
  $insert_by=$_SESSION['parking_admin_name'];
  $prk_admin_id = $_SESSION['prk_admin_id'];
  $prk_area_user_id=base64_decode($_REQUEST['list_id']);
}
$category_type='P_EMP_CAT';
$prk_user_emp_cat = common_drop_down_list($category_type);
$prk_user_emp_category = json_decode($prk_user_emp_cat, true);
$state1 = state();
$state = json_decode($state1, true);
?>
<!-- vendor css -->
  <style type="text/css">
    .size{
      font-size: 11px;
    }

    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
  </style> 


    <div class="am-mainpanel"><!-- cloding in footer -->
      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Users</h5>
      </div><!-- am-pagetitle -->
    </div>
    <div class="am-mainpanel">
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40" >
          <!-- <h6 class="card-body-title">Add Employee</h6> -->
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form name="add_user" action="#" method="post">
              <div class="row mg-b-25">
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">CATEGORY<span class="tx-danger">*</span></label>
                      <div class="select">
                        <select name="category" id="category" style="opacity: 0.8;font-size:12px">
                            <option value="">SELECT CATEGORY</option>
                            <?php
                              foreach ($prk_user_emp_category['drop_down'] as $val){ ?>  
                                <option value="<?php echo $val['all_drop_down_id']?>"><?php echo $val['full_name']?></option>
                              <?php          
                              }
                            ?> 
                        </select>
                      </div>
                    <span style=" position: absolute;" id="error_category" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">EMPLOYEE NUMBER <span class="tx-danger">*</span></label>
                    <input class="form-control error_msg" type="text" name="employee_number" placeholder="Enter number" id="employee_number" onkeydown="upperCase(this)">
                      <span style="position: absolute;" id="error_number" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label class="form-control-label size">EMPLOYEE NAME <span class="tx-danger">*</span></label>
                    <input class="form-control error_msg" type="text" name="employee_name"  placeholder="Enter name" id="employee_name" onkeydown="upperCase(this)">
                    <span style="position: absolute;" id="error_employee_name" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label class="form-control-label size"> USER NAME <span class="tx-danger"></span></label>
                    <input class="form-control readonly" type="text" name="user_name"  placeholder="User name" id="user_name" readonly="" style="background-color: white" onkeydown="upperCase(this)">
                      <span style=" position: absolute;" id="error_user_name" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size"> USER MOBILE <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="user_mobile"  placeholder="User Mobile" id="user_mobile" style="background-color: white" maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                      <span style=" position: absolute;" id="error_user_mobile" class="error_size"></span>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label class="form-control-label size">Gender<span class="tx-danger">*</span></label>
                      <label class="rdiobox">
                        <input type="radio" name="visitor_gender" id="vis_gender_m" value="M" type="radio" checked>
                        <span>Male </span>
                      </label>

                      <label class="rdiobox">
                        <input type="radio" name="visitor_gender" id="vis_gender_f" value="F" type="radio">
                        <span>Female </span>
                      </label>
                      <span style="position: absolute;" id="error_vis_gender" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">PASSWORD <span class="tx-danger">*</span></label>
                    <input class="form-control" type="password" name="password" placeholder="Password" id="password">
                    <span style="position: absolute;" id="error_password" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">CONFIRM PASSWORD<span class="tx-danger">*</span></label>
                    <input class="form-control" type="password" name="repassword"  placeholder="Repassword" id="repassword">
                    <span style=" position: absolute;" id="error_repassword" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label class="form-control-label size">ADDRESS<span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="address"  placeholder="Address" id="address">
                    <span style=" position: absolute;" id="error_address" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label class="form-control-label size">LANDMARK<span class="tx-danger"></span></label>
                    <input class="form-control" type="text" name="landmark"  placeholder="Landmark" id="landmark">
                    <span style=" position: absolute;" id="error_landmark" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="form-layout-footer">
                    <div class="row">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label size">STATE<span class="tx-danger">*</span></label>
                            <div class="select">
                              <select name="state" id="state" style="opacity: 0.8;font-size:14px">
                                <option value="">SELECT STATE</option>
                                <?php
                                  foreach ($state['state'] as $val){ ?>  
                                    <option value="<?php echo $val['sta_id']?>"><?php echo $val['state_name']?></option>
                                  <?php          
                                  }
                                ?> 
                              </select>
                            </div>
                          <span style=" position: absolute;" id="error_state" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label size">CITY<span class="tx-danger">*</span></label>
                            <div class="select">
                              <select name="city" id="city" style="opacity: 0.8;font-size:14px">
                              </select>
                            </div>
                          <span style=" position: absolute;" id="error_city" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label size">PIN CODE<span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="pincode"  placeholder="Pin Code" id="pincode" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" maxlength="6">
                          <span style=" position: absolute;" id="error_pincode" class="error_size"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-layout-footer">
                    <div class="row">
                      <div class="col-md-7">
                        <div class="form-group">
                          <label class="form-control-label user-lebel">Upload Image<span class="tx-danger"></span></label>
                          <input type="file" name="user_img" accept="image/*" id="user_img" style="font-size: 13px;">
                          <input type="hidden" name="user_image" class="user_image" id="user_image">
                        </div>
                      </div>
                      <!-- <div class="col-lg-4">
                        <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                      </div> -->
                      <div class="col-md-5">
                        <div class="form-group">
                          <div id="results">captured image will appear here...</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-2">
                  <input type="button" value="MODIFY" class="btn btn-block btn-primary prk_button" name="submit" id="submit">
                  <div style="position: absolute;" id="success_submit" class="success"></div>
                </div>
                <div class="col-lg-2">

                  <a href="vender-add-user"><button type="button" id="back" class="btn btn-block prk_button_skip">BACK</button></a>
                </div>
            </form>
          </div>
        </div>
        </div>
      </div>
    <!-- datatable -->
    <!-- <div class="am-mainpanel"> -->
      <div class="am-pagebody">
    <?php include"all_nav/footer.php"; ?>

<style>
  .error_size{
    font-size: 11px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>

<script type="text/javascript">
  $('#password').attr("disabled", true);
  $('#repassword').attr("disabled", true);
  var prkUrl = "<?php echo PRK_URL; ?>";
  var PRK_BASE_URL = "<?php echo PRK_BASE_URL; ?>";
  var prk_admin_id='<?php echo $prk_admin_id; ?>';
  var insert_by='<?php echo $insert_by; ?>';
  var token='<?php echo $token; ?>';
  var prk_area_user_id='<?php echo $prk_area_user_id; ?>';
  //alert(prk_area_user_id);

  $(document).ready(function(){
    /*var urlState = prkUrl+'state.php';
    $.ajax ({
      type: 'POST',
      url: urlState,
      data: "",
      success : function(data) {
        var obj=JSON.parse(data);
        var areaOption = "<option value=''>SELECT STATE</option>";
        $.each(obj['state'], function(val, key) {
          areaOption += '<option value="' + key['sta_id'] + '">' + key['state_name'] + '</option>'
        });
        $("#state").html(areaOption);
      }
    });*/
    $('#state').on("change", function() {
      var id=($("#state").val());
      var urlCity = prkUrl+'city.php';
      $.ajax ({
        type: 'POST',
        url: urlCity,
        data: {id:id},
        success : function(data) {
          var obj=JSON.parse(data);
          var areaOption = "<option value=''>SELECT CITY</option>";
          $.each(obj['city'], function(val, key) {
            areaOption += '<option value="' + key['city_id'] + '">' + key['city_name'] + '</option>'
          });
          $("#city").html(areaOption);
        }
      });
    });
    var urlUser_s = prkUrl+'prk_area_user_show.php';
    // alert(urlUser_s);
    $.ajax ({
      type: 'POST',
      url: urlUser_s,
      data:{
        'prk_admin_id':prk_admin_id,
        'prk_area_user_id':prk_area_user_id,
        'token':token
      },
      success : function(data) {
        var json = $.parseJSON(data);
        if (json.status){
          // alert(json.prk_user_img);
          $('#employee_number').val(json.prk_user_emp_no);
          $('#employee_name').val(json.prk_user_name);
          $('#user_name').val(json.prk_user_username);
          $('#user_mobile').val(json.prk_user_mobile);
          $('#password').val(json.prk_user_password);
          $('#repassword').val(json.prk_user_password);
          $('input:radio[name="visitor_gender"][value="'+json.prk_user_gender+'"]').attr('checked',true);
          $('#address').val(json.prk_user_address);
          $('#pincode').val(json.prk_user_pincode);
          $('#landmark').val(json.prk_user_landmark);
          $('#category').val(json.prk_user_emp_category);
          if(!json.prk_user_img==''){
            document.getElementById('results').innerHTML = '<img style="height: 80px;width: 100px;" src="'+PRK_BASE_URL+''+json.prk_user_img+'"/>';
          }else{
            document.getElementById('results').innerHTML = 'Capture image will appear here...';
          }
          if (json.prk_user_state==''||json.prk_user_state==null||json.prk_user_state=='0') {
          }else{
          $('#state').val(json.prk_user_state);
          }
          var demoLin = '';
          if (json.prk_user_city==''||json.prk_user_city=='0'||json.prk_user_city==null) {
            demoLin +='<option value="">SELECT CITY</option>'; 
          }else{
            demoLin +='<option value='+json.prk_user_city+'>'+json.city_name+'</option>'; 
          }
          $("#city").html(demoLin);
        }
      }
    });

    $('#submit').click(function(){
      var category=$('#category').val();
      var employee_number=$('#employee_number').val();
      var employee_name=$('#employee_name').val();
      var user_name=$('#user_name').val();
      var password=$('#password').val();
      var repassword=$('#repassword').val();
      var user_mobile=$('#user_mobile').val();
      var address=$('#address').val();
      var pincode=$('#pincode').val();
      var state=$('#state').val();
      var city=$('#city').val();
      var landmark=$('#landmark').val();
      var gender =  $('input[name=visitor_gender]:checked').val();
      var user_img =  $('#user_image').val();

      var prk_admin_id=' <?php echo $prk_admin_id; ?>';
      var insert_by=' <?php echo $insert_by; ?>';
      var token=' <?php echo $token; ?>';

      var form_data = new FormData();
      form_data.append('prk_admin_id', prk_admin_id);
      form_data.append('prk_user_emp_category', category);
      form_data.append('prk_user_emp_no', employee_number);
      form_data.append('prk_user_username', user_name);
      form_data.append('prk_user_name', employee_name);
      form_data.append('prk_user_password', password);
      form_data.append('prk_user_repassword', repassword);
      form_data.append('prk_user_mobile', user_mobile);
      form_data.append('prk_user_gender', gender);
      form_data.append('prk_user_address', address);
      form_data.append('prk_user_landmark', landmark);
      form_data.append('prk_user_state', state);
      form_data.append('prk_user_city', city);
      form_data.append('prk_user_pincode', pincode);
      form_data.append('inserted_by', insert_by);
      form_data.append('prk_user_img', user_img);
      form_data.append('token', token);
      form_data.append('prk_area_user_id', prk_area_user_id);
      // alert('mano');
      if(employee_name_check(employee_name)==true & employee_number_check(employee_number)==true & password_check(password)==true & repassword_check(password,repassword)==true & user_mobile_check(user_mobile)==true & address_check(address)==true & pincode_check(pincode)==true & state_check(state)==true & city_check(city)==true & category_check(category)==true){
        $('#submit').val('Wait ...').prop('disabled', true);
        var urlPrkAreUser = prkUrl+'prk_area_user_update.php';
        // alert(urlPrkAreUser);
        $.ajax({
          url :urlPrkAreUser,
          type:'POST',
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,                         
          dataType:'html',
          success  :function(data)
          {
            $('#submit').val('MODIFY').prop('disabled', false);
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.9em;'>Employee Details Modify Successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                    window.location.href='vender-add-user';
                  }
                }
              });
            }else{
              if( typeof json.session !== 'undefined'){
                  if (!json.session) { 
                    window.location.replace("logout.php");                    
                  }                  
                }else{                    
                  $.alert({                    
                    icon: 'fa fa-frown-o',                    
                    theme: 'modern',                    
                    title: 'Error !',                    
                    content: "Somting went wrong",                    
                    type: 'red'                  
                  });                  
                }                
              }
          }
        });
      }
    });
    $('#reset').click(function(){

      $('.error_size').text('');
    });
    $('#category').on("change", function() {
      var category=$('#category').val();
      category_check(category);
    });
    $("#employee_number").blur(function () {
      var employee_number=$('#employee_number').val();
      employee_number_check(employee_number);
    });
    $("#employee_name").blur(function () {
      var employee_name=$('#employee_name').val();
      employee_name_check(employee_name);
      // user_name_genarate(employee_name);
    });
    $("#user_mobile").blur(function () {
      var user_mobile=$('#user_mobile').val();
      user_mobile_check(user_mobile);
    });
    $("#repassword").blur(function () {
      var password=$('#password').val();
      var repassword=$('#repassword').val();
      repassword_check(password,repassword);
    });
    $("#password").blur(function () {
      var password=$('#password').val();
      password_check(password);
    });
    $("#address").blur(function () {
      var address=$('#address').val();
      address_check(address);
    });
    $("#pincode").blur(function () {
      var pincode=$('#pincode').val();
      pincode_check(pincode);
    });
    $('#state').on("change", function() {
      var state=$('#state').val();
      state_check(state);
    });
    $('#city').on("change", function() {
      var city=$('#city').val();
      city_check(city);
    });
    /*only aplhabets*/
    $("#employee_name").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)){
            event.preventDefault();
        }
    });
  });
  function category_check(category){
    // alert("ok");
    if(category==''){
      $('#error_category').text('Select Category');
      return false;
    }else{
      $('#error_category').text('');
      return true;
    }
  }
  function employee_name_check(employee_name){
    if(employee_name.trim()==''){
      $('#error_employee_name').text('Employee Name Required');
      return false;
    }else{
      $('#error_employee_name').text('');
      return true;
    }
  }
  function user_name_genarate(employee_name){
    if(employee_name!=''){
      parking_area_name = employee_name.replace(/^([^\s]*)(.*)/, "$1");
      var random_number = Math.floor(1000 + Math.random() * 9000);
      var g_username = (parking_area_name+'_'+random_number);
      $("#user_name").val(g_username);
      $('#user_mobile').focus();
    }else{
      $("#user_name").val('');
    }
  }
  function employee_number_check(employee_number){
    if(employee_number==''){
      $('#error_number').text('Employee Number Required');
      return false;
    }else{
      $('#error_number').text('');
      return true;
    }
  }
  function password_check(password){
    password_le= password.length;
    if(password==''){
      $('#error_password').text('Password Required');
      return false;
    }else if (password_le<5){
      $('#error_password').text('Minimum 5 characters');
      return false;
    }else{
      $('#error_password').text('');
      return true;
    }
  }
  function repassword_check(password,repassword){
    password_le= repassword.length;
    if(repassword==''){
      $('#error_repassword').text('Password Required');
      return false;
    }else if (password_le<5){
      $('#error_repassword').text('Minimum 5 characters');
      return false;
    }else if (password!=repassword){
      $('#error_repassword').text('Password mismatch');
      return false;
    }else{
      $('#error_repassword').text('');
      return true;
    }
  }
  function user_mobile_check(val){
    var n = val.length;
    // alert(n);
    if(val==''){
      $('#error_user_mobile').text('Mobile Number Required');
      return false;
    }else{
      if(n>=10){
        $('#error_user_mobile').text('');
        return true;
      }else{
        $('#error_user_mobile').text('Mobile Number Must be 10 Digits');
        return false;
      }
    }
  }
  function address_check(val){
    if(val==''){
      $('#error_address').text('Address Required');
      return false;
    }else{
      $('#error_address').text('');
      return true;
    }
  }
  function state_check(val){
    if(val==''){
      $('#error_state').text('Select State');
      return false;
    }else{
      $('#error_state').text('');
      return true;
    }
  }
  function city_check(val){
    if(val==''){
      $('#error_city').text('Select City');
      return false;
    }else{
      $('#error_city').text('');
      return true;
    }
  }
  function pincode_check(val){
    if(val==''){
      $('#error_pincode').text('Pincode Required');
      return false;
    }else{
      $('#error_pincode').text('');
      return true;
    }
  }
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>
<script type="text/javascript">
  $(function() {
    $("#user_img").change(function() {
      //$("#message").empty(); // To remove the previous error message
      var file = this.files[0];
      var imagefile = file.type;
      var match= ["image/jpeg","image/png","image/jpg"];
      if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
      {
      return false;
      }
      else
      {
      var reader = new FileReader();
      reader.onload = imageIsLoaded;
      reader.readAsDataURL(this.files[0]);
      }
    });
    
  });
  function imageIsLoaded(e) {
    $(".user_image").val(e.target.result);
    document.getElementById('results').innerHTML = '<img style="height: 80px;width: 100px;" src="'+e.target.result+'"/>';
  };
  
</script>