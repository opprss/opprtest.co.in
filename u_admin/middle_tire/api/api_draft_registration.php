<?php
/*
Description: parking area registor user.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function draft_Registration($user_mobile,$user_veh_type,$user_veh_number){
    global $pdoconn;
    $resp =v_numberChecker($user_veh_number);
    $json = json_decode($resp);
    if($json->status=='1'){
        $sql = "SELECT `user_admin_id`,`user_ac_status`,`temp_password` FROM `user_admin` WHERE `user_mobile`='$user_mobile'";
         $query  = $pdoconn->prepare($sql);
        $query->execute();
        $val = $query->fetch();
        $user_admin_id= $val['user_admin_id'];
        $count=$query->rowCount();
        if($count>0){
            $user_ac_status= $val['user_ac_status'];
            $temp_password= $val['temp_password'];

            $resp=vehicle_count($user_veh_number,$user_admin_id);
            $json = json_decode($resp);
            if($json->status=='1'){
                if($user_ac_status=='D'){
                    $response['status'] = 1;
                    $response['user_admin_id'] = $user_admin_id;
                    $response['user_ac_status'] = $user_ac_status;
                    $response['user_veh_insert'] = 'N';
                    $response['user_mobile'] = $user_mobile;
                    $response['password'] = $temp_password;
                    $response['message'] = 'Registration Successful';
                    return json_encode($response);
                }else{
                    $response['status'] = 1;
                    $response['user_admin_id'] = $user_admin_id;
                    $response['user_ac_status'] = $user_ac_status;
                    $response['user_veh_insert'] = 'N';
                    $response['password'] = $temp_password;
                    $response['user_mobile'] = $user_mobile;
                    $response['message'] = 'Registration Successful';
                    return json_encode($response);
                }
            }else{
                $sqli = "INSERT INTO `user_vehicle_detail`(`user_veh_type`,`user_veh_number`,`user_admin_id`,`inserted_by`,`inserted_date`) VALUE ('$user_veh_type','$user_veh_number','$user_admin_id','$user_mobile','".TIME."')";
                $queryi = $pdoconn->prepare($sqli);
                if($queryi->execute()){
                    $user_veh_dtl_id = $pdoconn->lastInsertId();
                    $resp=veh_qr_code_gen($user_veh_number, $user_veh_type,$user_mobile,$user_veh_dtl_id);
                    $json = json_decode($resp);
                    if($json->status=='1'){
                        if($user_ac_status=='D'){
                            $response['status'] = 1;
                            $response['user_admin_id'] = $user_admin_id;
                            $response['user_ac_status'] = $user_ac_status;
                            $response['user_veh_insert'] = 'Y';
                            $response['user_mobile'] = $user_mobile;
                            $response['password'] = $temp_password;
                            $response['message'] = 'Registration Successful';
                            return json_encode($response);
                        }else{

                            $response['status'] = 1;
                            $response['user_admin_id'] = $user_admin_id;
                            $response['user_ac_status'] = $user_ac_status;
                            $response['user_veh_insert'] = 'Y';
                            $response['password'] = $temp_password;
                            $response['user_mobile'] = $user_mobile;
                            $response['message'] = 'Registration Successful';
                            return json_encode($response);
                        }
                    }else{
                        $response['status'] = 0;
                        $response['user_admin_id'] = $user_admin_id;
                        $response['user_ac_status'] = $user_ac_status;
                        $response['user_veh_insert'] = 'N';
                        $response['user_mobile'] = $user_mobile;
                        $response['password'] = $temp_password;
                        $response['message'] = 'Registration Successful';
                        return json_encode($response);
                    }
                }else{
                    $response['status'] = 0;
                    $response['user_admin_id'] = $user_admin_id;
                    $response['user_ac_status'] = $user_ac_status;
                    $response['user_veh_insert'] = 'N';
                    $response['user_mobile'] = $user_mobile;
                    $response['password'] = $temp_password;
                    $response['message'] = 'Registration Successful';
                    return json_encode($response);
                }
            } 
        }else{
            $pass=random_password();
            $password=md5($pass);
            $response = array();
            $sql = "INSERT INTO `user_admin`(`user_mobile`,`user_password`,`temp_password`,`inserted_by`,`user_ac_status`,`inserted_date`) VALUE ('$user_mobile','$password','$pass','$user_mobile','D','".TIME."')";
            $query = $pdoconn->prepare($sql);
            if($query->execute()){

                $sql = "SELECT * FROM `user_admin` WHERE `user_mobile`='$user_mobile'";
                $query  = $pdoconn->prepare($sql);
                $query->execute();
                $arr_catagory = $query->fetchAll();
                foreach($arr_catagory as $val)
                {
                    $user_admin_id=$val['user_admin_id'];
                }

                $sqli = "INSERT INTO `user_vehicle_detail`(`user_veh_type`,`user_veh_number`,`user_admin_id`,`inserted_by`,`inserted_date`) VALUE ('$user_veh_type','$user_veh_number','$user_admin_id','$user_mobile','".TIME."')";
                $queryi = $pdoconn->prepare($sqli);
                if($queryi->execute()){
                    $user_veh_dtl_id = $pdoconn->lastInsertId();
                    $resp=veh_qr_code_gen($user_veh_number, $user_veh_type,$user_mobile,$user_veh_dtl_id);
                    $json = json_decode($resp);
                    if($json->status=='1'){
                        $response['status'] = 1;
                        $response['user_admin_id'] = $user_admin_id;
                        $response['user_veh_insert'] = 'Y';
                        $response['user_ac_status'] = 'D';
                        $response['user_mobile'] = $user_mobile;
                        $response['password'] = $pass;
                        $response['message'] = 'Registration Successful';
                        return json_encode($response);
                    }else{
                        $response['status'] = 0;
                        $response['message'] = 'Registration Wrong';
                        return json_encode($response);
                    }
                }else{
                    $response['status'] = 0;
                    $response['message'] = 'Registration Wrong';
                    return json_encode($response);
                }
            }else{
                $response['status'] = 0;
                $response['message'] = 'Registration Wrong';
                return json_encode($response);
            }
        }
    }else{
        $response=v_numberChecker($user_veh_number);
        return ($response);
    }
}
?>