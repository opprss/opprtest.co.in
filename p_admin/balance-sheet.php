<!-- ################################################
  
Description: Tower detalis.
Developed by: Rakhal Raj 
Created Date: 12-03-2018

 ####################################################-->
<?php include "all_nav/header.php";
//$apartment_balance_sheet=balance_sheet($prk_admin_id);
//$apartment_balance_sheet_list = json_decode($apartment_balance_sheet, true);

  $category_type='YER';
  $prk_year = common_drop_down_list($category_type);
  $prk_year_list = json_decode($prk_year, true);

  $category_type='MON';
  $prk_month = common_drop_down_list($category_type);
  $prk_month_list = json_decode($prk_month, true);
?>
<style type="text/css">
  .size{
    font-size: 11px;
  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
  .error_size{
    font-size: 11px;
    color: red;
  }
  .error{
    font-size: 11px;
    color: red;
  }
  .error_size{
    font-size: 11px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
<link href="../lib/highlightjs/github.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../lib/select2/css/select2.min.css" rel="stylesheet">

<link href="../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">

<style>
  table.dataTable tfoot th, table.dataTable tfoot td {
    padding: 10px 18px 6px 9px;
    border-top: 1px solid #111;
</style>


<div class="am-mainpanel">
    <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Balance Sheet<?//=$apartment_balance_sheet?></h5>
    </div>
    <form id="maint_charge">
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40" >
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <div class="row mg-b-5">
              <div class="col-md-3">
                <div class="form-group">
                  <label class="form-control-label size">From Date<span class="tx-danger">*</span></label>
                  <input type="text" name="start_date" id="start_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="form-control-label size">To Date<span class="tx-danger">*</span></label>
                  <input type="text" name="end_date" id="end_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-layout-footer mg-t-30">
                  <div class="row">
                    <div class="col-lg-4">
                      <input type="button" value=" SHOW DETAILS" class="btn btn-block btn-primary prk_button" name="save" id="save">
                    </div>
                    <div class="col-lg-4">
                      <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40">
        <div class="table-wrapper">
          <table id="datatable1" class="table display" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th >SL No</th>
                <th >Transaction Date</th>
                <th >Payment Type</th>
                <th >Amount Credit(In Rupees)</th>
                <th >Amount Debit(In Rupees)</th>
              </tr>
            </thead>
            <tbody id="trn_show">
            
            </tbody>
            <tfoot align="right">
              <tr><th></th><th></th><th></th><th></th><th></th></tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
<?php include"all_nav/footer.php"; ?>
<!-- scripts for datatable -->
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>

<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
<script src="../lib/datatables-responsive/jquery.dataTables.min.js"></script>
<script src="../lib/datatables-responsive/dataTables.buttons.min.js"></script>
<script src="../lib/datatables-responsive/jszip.min.js"></script>
<script src="../lib/datatables-responsive/pdfmake.min.js"></script>
<script src="../lib/datatables-responsive/vfs_fonts.js"></script>
<script src="../lib/datatables-responsive/buttons.html5.min.js"></script>

<script type="text/javascript">
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    // var now = now.setDate(now.getDate() - 1);
    pickmeup('#start_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
        // if (date < now) {
          return {
            disabled : false,
            class_name : 'date-in-past'
          };
        // }
        return {};
      } 
    });
  });
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#start_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      hide_on_select : true,
      render : function (date) {
        if (date < now_end) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
        }
        return {};
      } 
    });
  });
  $('#start_date').focus(function(){

    $('#end_date').val('');
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
    apart_balance_sheet();
  });
  function datatable_show(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      language: {
      searchPlaceholder: 'Search Here',
      sSearch: '',
      lengthMenu: '_MENU_ Page',
      },
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      buttons: [
        { 
          extend: 'copyHtml5', 
          footer: true,
          title: 'APARTMENT TRANSACTION BALANCE SHEET' 
        },
        { 
          extend: 'excelHtml5', 
          footer: true,
          title: 'APARTMENT TRANSACTION BALANCE SHEET'  
        },
        {
            extend: 'pdfHtml5',
            footer: true,
            title: 'APARTMENT TRANSACTION BALANCE SHEET'
        },
      ],
      "footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;
        // converting to interger to find total
        var intVal = function ( i ) {
          return typeof i === 'string' ?
          i.replace(/[\$,]/g, '')*1 :
          typeof i === 'number' ?
          i : 0;
        };
        var creTotal = api
        .column( 4 )
        .data()
        .reduce( function (a, b) {
            return intVal(a) + intVal(b);
        }, 0 );
        var debTotal = api
        .column( 3 )
        .data()
        .reduce( function (a, b) {
            return intVal(a) + intVal(b);
        }, 0 );
        var deduct = 'BALANCE RS.'+''+''+'('+(creTotal-debTotal)+'/-'+')';
        $( api.column( 2 ).footer() ).html('Total');
        $( api.column( 4 ).footer() ).html(creTotal+'/-');
        $( api.column( 3 ).footer() ).html(debTotal+'/-');
        // $( api.column(  ).footer() ).html(deduct);
      }
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
   }
</script>
<script type="text/javascript">
  $('#save').click(function(){
    if($("#maint_charge").valid()){
      apart_balance_sheet();
      $('#datatable1').dataTable().fnDestroy();
    }
  });
</script>
<script type="text/javascript">
  function apart_balance_sheet(){
    var prkUrl = "<?php echo PRK_URL; ?>";
    var FUTURE_DATE = "<?php echo FUTURE_DATE ?>";
    var prk_admin_id="<?=$prk_admin_id?>";
    var parking_admin_name="<?=$parking_admin_name?>";
    var token="<?=$token?>";
    var start_date=($("#start_date").val());
    var end_date=($("#end_date").val());
    var urlPrkAreGateDtl = prkUrl+'balance_sheet.php';

    $.ajax({
      url :urlPrkAreGateDtl,
      type:'POST',
      data :{
          'prk_admin_id':prk_admin_id,
          'parking_admin_name':parking_admin_name,
          'token': token,
          'start_date':start_date,
          'end_date':end_date
        },
        dataType:'html',
        success  :function(data){
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            var demoLines="";
            var c = 0;
            // alert(json.status);
            // if (json.status==1) {
              for (var key in json.balance_sheet_list) {
                var keyy = json.balance_sheet_list[key];
                c +=1;
                 demoLines += '<tr>\
                  <td> '+c+'</td>\
                  <td>'+keyy['tran_date']+'</td>\
                  <td>'+keyy['payment_type']+'</td>';
                  if (keyy['tran_flag']=='CR') {
                    demoLines += '<td>'+keyy['total_amount']+'</td>\
                    <td>0</td>';
                  }else{
                    demoLines += '<td>0</td>\
                    <td>'+keyy['total_amount']+'</td>';
                  }
                  demoLines +='</tr>';
              }
            // }
          }else{
           demoLines = '';
          }
          $("#trn_show").html(demoLines);
          datatable_show();
        }
    });
  }
</script>
<script type="text/javascript">
  $( document ).ready( function () {
    $( "#maint_charge" ).validate( {
      rules: {
        start_date: "required",
        end_date: "required"
      },
      messages: {
      }
    });
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>



