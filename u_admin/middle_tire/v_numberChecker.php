<?php
/*
Description: vehicle number chaker.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
    include_once 'api/reg_api.php';
    // include_once 'api/global_api.php';
    $response = array();
    if(isAvailable(array('v_number'))){
        if(isEmpty(array('v_number'))){
            $v_number = trim($_POST['v_number']);
            $response= v_numberChecker($v_number);
            echo ($response);
        }else{
                $response['status'] = 0;
                $response['message'] = 'All Fields Are Mandatory';
                echo json_encode($response); 
        }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        echo json_encode($response); 
    } 
?> 