<?php 
/*
Description: vehicle QR code img show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
//require_once '../../connection-pdo.php';
require_once 'api/reg_api.php';
//require_once 'api/global_api.php';
 
$response = array();
 
if(isAvailable(array('user_veh_dtl_id'))){

 	if(isEmpty(array('user_veh_dtl_id'))){

	 	$user_veh_dtl_id = trim($_POST['user_veh_dtl_id']);
	 	$response= veh_qr_code($user_veh_dtl_id);
	 	echo ($response);

 	}else{
 			$response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response); 
	 }

 }else{
	 $response['status'] = 0; 
	 $response['message'] = 'Invalid API Call';
     echo json_encode($response); 
 } 
?>