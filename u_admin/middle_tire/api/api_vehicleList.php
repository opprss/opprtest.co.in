<?php
/*
Description: user vehicle list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-05-2019
*/
function vehicleList($admin_id,$vehicl){
    $allplayerdata = array();
    $response = array();
    $vehicle = array();
    global $pdoconn;
    if(!empty($vehicl)){
        $w2=$vehicl;
        $w3=$vehicl;
        $w4=$vehicl;
        $w8=$vehicl;
    }else{
        $w2='2W';
        $w3='3W';
        $w4='4W';
        $w8='8W';
    }
    $sql="SELECT `user_vehicle_detail`.`user_veh_dtl_id`,
        `user_vehicle_detail`.`user_veh_type`,
        `user_vehicle_detail`.`user_veh_number`,
        `user_vehicle_detail`.`user_veh_reg`,
        `user_vehicle_detail`.`user_veh_reg_exp_dt`,
        `user_vehicle_detail`.`user_ins_number`,
        `user_vehicle_detail`.`user_ins_exp_dt`,
        IF(`user_vehicle_detail`.`user_ins_img` IS NULL or `user_vehicle_detail`.`user_ins_img` = '', '', CONCAT('".USER_BASE_URL."',`user_vehicle_detail`.`user_ins_img`)) as 'user_ins_img',
        `user_vehicle_detail`.`user_ins_img_status`,
        `user_vehicle_detail`.`user_emu_number`,
        `user_vehicle_detail`.`user_emu_exp_dt`,
        IF(`user_vehicle_detail`.`user_emu_img` IS NULL or `user_vehicle_detail`.`user_emu_img` = '', '', CONCAT('".USER_BASE_URL."',`user_vehicle_detail`.`user_emu_img`)) as 'user_emu_img',
        `user_vehicle_detail`.`user_emu_img_status`,
        `user_vehicle_detail`.`veh_qr_code`,
        `vehicle_type`.`vehicle_typ_img`,
        `vehicle_type`.`vehicle_type_dec`,
        IF(`user_vehicle_detail`.`rc_img` IS NULL or `user_vehicle_detail`.`rc_img` = '', '', CONCAT('".USER_BASE_URL."',`user_vehicle_detail`.`rc_img`)) as 'rc_img',
        `user_vehicle_detail`.`rc_img_status`,
        `user_vehicle_detail`.`oth_doc_no1`,
        `user_vehicle_detail`.`oth_doc_no1_exp_dt`,
        IF(`user_vehicle_detail`.`oth_doc_no1_img` IS NULL or `user_vehicle_detail`.`oth_doc_no1_img` = '', '', CONCAT('".USER_BASE_URL."',`user_vehicle_detail`.`oth_doc_no1_img`)) as 'oth_doc_no1_img',
        `user_vehicle_detail`.`oth_doc_no1_img_status`,
        `user_vehicle_detail`.`oth_doc_no2`,
        `user_vehicle_detail`.`oth_doc_no2_exp_dt`,
        IF(`user_vehicle_detail`.`oth_doc_no2_img` IS NULL or `user_vehicle_detail`.`oth_doc_no2_img` = '', '', CONCAT('".USER_BASE_URL."',`user_vehicle_detail`.`oth_doc_no2_img`)) as 'oth_doc_no2_img',
        `user_vehicle_detail`.`oth_doc_no1_img_status`,
        `user_vehicle_detail`.`oth_doc_no2_img_status` 
        FROM `user_vehicle_detail`,`vehicle_type` 
        WHERE `user_vehicle_detail`.`user_admin_id`='$admin_id' 
        AND `user_vehicle_detail`.`active_flag`='".FLAG_Y."' 
        AND `user_vehicle_detail`.`del_flag`='".FLAG_N."'
        AND `vehicle_type`.`active_flag`='".FLAG_Y."'
        AND `vehicle_type`.`vehicle_sort_nm`=`user_vehicle_detail`.`user_veh_type`
        ORDER BY  `user_vehicle_detail`.`user_veh_dtl_id` DESC ";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $sql = "SELECT uld.`user_lic_id`,
                uld.`user_lic_number`,
                uld.`user_lic_name`,
                uld.`user_lic_veh_class`,
                uld.`user_lic_issue_date`,
                uld.`user_lic_issued_by`,
                uld.`user_lic_valid_till`,
                uld.`s_d_w_of`,
                uld.`blood_gr`,
                uld.`date_of_birth`,
                uld.`address`,
                IF(uld.`user_lic_img` IS NULL or uld.`user_lic_img` = '', '', CONCAT('".USER_BASE_URL."',uld.`user_lic_img`)) as 'user_lic_img',
                uld.`user_lic_img_status`
                FROM `vehicle_class` vcl, `user_lic_dtl` uld
                WHERE uld.`user_lic_veh_class`=vcl.`class_name`
                AND vcl.`active_flag`='".FLAG_Y."'
                AND uld.`user_admin_id`='$admin_id' 
                AND uld.`active_flag`='".FLAG_Y."' 
                AND uld.`del_flag`='".FLAG_N."'
                AND vcl.`vehicle_sort_nm`='".$val['user_veh_type']."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $valll = $query->fetch(); 
                $user_lic_id = $valll['user_lic_id'];
                $user_lic_number = $valll['user_lic_number'];
                $user_lic_name = $valll['user_lic_name'];
                $user_lic_veh_class = $valll['user_lic_veh_class'];
                $user_lic_issue_date = $valll['user_lic_issue_date'];
                $user_lic_issued_by = $valll['user_lic_issued_by'];
                $user_lic_valid_till = $valll['user_lic_valid_till'];
                $s_d_w_of = $valll['s_d_w_of'];
                $blood_gr = $valll['blood_gr'];
                $date_of_birth = $valll['date_of_birth'];
                $address = $valll['address'];
                $user_lic_img = $valll['user_lic_img'];
                $user_lic_img_status = $valll['user_lic_img_status'];
            }else{
                $user_lic_id = '';
                $user_lic_number = '';
                $user_lic_name = '';
                $user_lic_veh_class = '';
                $user_lic_issue_date = '';
                $user_lic_issued_by = '';
                $user_lic_valid_till = '';
                $s_d_w_of = '';
                $blood_gr = '';
                $date_of_birth = '';
                $address = '';
                $user_lic_img = '';
                $user_lic_img_status = '';
                $user_lic_id = '';
            } 
            $vehicle['user_veh_dtl_id'] = $val['user_veh_dtl_id'];
            $vehicle['user_veh_type'] = $val['user_veh_type'];
            $vehicle['vehicle_type_dec'] = $val['vehicle_type_dec'];
            $vehicle['vehicle_typ_img'] = $val['vehicle_typ_img'];
            $vehicle['user_veh_number'] = $val['user_veh_number'];
            $vehicle['user_veh_reg'] = $val['user_veh_reg'];
            $vehicle['user_veh_reg_exp_dt'] = $val['user_veh_reg_exp_dt'];
            $vehicle['user_ins_number'] = $val['user_ins_number'];
            $vehicle['user_ins_exp_dt'] = $val['user_ins_exp_dt'];
            $vehicle['user_ins_img'] = $val['user_ins_img'];
            $vehicle['user_ins_img_status'] = $val['user_ins_img_status'];
            $vehicle['user_emu_number'] = $val['user_emu_number'];
            $vehicle['user_emu_exp_dt'] = $val['user_emu_exp_dt'];
            $vehicle['user_emu_img'] = $val['user_emu_img'];
            $vehicle['user_emu_img_status'] = $val['user_emu_img_status'];
            $vehicle['veh_qr_code'] = $val['veh_qr_code'];
            $vehicle['rc_img'] = $val['rc_img'];
            $vehicle['rc_img_status'] = $val['rc_img_status'];
            $vehicle['oth_doc_no1'] = $val['oth_doc_no1'];
            $vehicle['oth_doc_no1_exp_dt'] = $val['oth_doc_no1_exp_dt'];
            $vehicle['oth_doc_no1_img'] = $val['oth_doc_no1_img'];
            $vehicle['oth_doc_no1_img_status'] = $val['oth_doc_no1_img_status'];
            $vehicle['oth_doc_no2'] = $val['oth_doc_no2'];
            $vehicle['oth_doc_no2_exp_dt'] = $val['oth_doc_no2_exp_dt'];
            $vehicle['oth_doc_no2_img'] = $val['oth_doc_no2_img'];
            $vehicle['oth_doc_no2_img_status'] = $val['oth_doc_no2_img_status'];
            $vehicle['user_lic_id'] = $user_lic_id;
            $vehicle['user_lic_number'] = $user_lic_number;
            $vehicle['user_lic_name'] = $user_lic_name;
            $vehicle['user_lic_veh_class'] = $user_lic_veh_class;
            $vehicle['user_lic_issue_date'] = $user_lic_issue_date;
            $vehicle['user_lic_issued_by'] = $user_lic_issued_by;
            $vehicle['user_lic_valid_till'] = $user_lic_valid_till;
            $vehicle['s_d_w_of'] = $s_d_w_of;
            $vehicle['blood_gr'] = $blood_gr;
            $vehicle['date_of_birth'] = $date_of_birth;
            $vehicle['address'] = $address;
            $vehicle['user_lic_img'] = $user_lic_img;
            $vehicle['user_lic_img_status'] = $user_lic_img_status;
            array_push($allplayerdata, $vehicle);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';  
        $response['vehicle'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return ($response);
}
/*function vehicleList($admin_id,$vehicl){
    $allplayerdata = array();
    $response = array();
    $vehicle = array();
    global $pdoconn;
    if(!empty($vehicl)){
        $w2=$vehicl;
        $w3=$vehicl;
        $w4=$vehicl;
        $w8=$vehicl;
    }else{
        $w2='2W';
        $w3='3W';
        $w4='4W';
        $w8='8W';
    }
    $sql="SELECT uvd.`user_veh_dtl_id`,
        uvd.`user_veh_type`,
        uvd.`user_veh_number`,
        uvd.`user_veh_reg`,
        uvd.`user_veh_reg_exp_dt`,
        uvd.`user_ins_number`,
        uvd.`user_ins_exp_dt`,
        IF(uvd.`user_ins_img` IS NULL or uvd.`user_ins_img` = '', '', CONCAT('".USER_BASE_URL."',uvd.`user_ins_img`)) as 'user_ins_img',
        uvd.`user_ins_img_status`,
        uvd.`user_emu_number`,
        uvd.`user_emu_exp_dt`,
        IF(uvd.`user_emu_img` IS NULL or uvd.`user_emu_img` = '', '', CONCAT('".USER_BASE_URL."',uvd.`user_emu_img`)) as 'user_emu_img',
        uvd.`user_emu_img_status`,
        uvd.`veh_qr_code`,
        vet.`vehicle_typ_img`,
        vet.`vehicle_type_dec`,
        IF(uvd.`rc_img` IS NULL or uvd.`rc_img` = '', '', CONCAT('".USER_BASE_URL."',uvd.`rc_img`)) as 'rc_img',
        uvd.`rc_img_status`,
        uvd.`oth_doc_no1`,
        uvd.`oth_doc_no1_exp_dt`,
        IF(uvd.`oth_doc_no1_img` IS NULL or uvd.`oth_doc_no1_img` = '', '', CONCAT('".USER_BASE_URL."',uvd.`oth_doc_no1_img`)) as 'oth_doc_no1_img',
        uvd.`oth_doc_no1_img_status`,
        uvd.`oth_doc_no2`,
        uvd.`oth_doc_no2_exp_dt`,
        IF(uvd.`oth_doc_no2_img` IS NULL or uvd.`oth_doc_no2_img` = '', '', CONCAT('".USER_BASE_URL."',uvd.`oth_doc_no2_img`)) as 'oth_doc_no2_img',
        uvd.`oth_doc_no1_img_status`,
        uvd.`oth_doc_no2_img_status`,
        uld.`user_lic_id`,
        uld.`user_lic_number`,
        uld.`user_lic_name`,
        uld.`user_lic_veh_class`,
        uld.`user_lic_issue_date`,
        uld.`user_lic_issued_by`,
        uld.`user_lic_valid_till`,
        uld.`s_d_w_of`,
        uld.`blood_gr`,
        uld.`date_of_birth`,
        uld.`address`,
        IF(uld.`user_lic_img` IS NULL or uld.`user_lic_img` = '', '', CONCAT('".USER_BASE_URL."',uld.`user_lic_img`)) as 'user_lic_img',
        uld.`user_lic_img_status`
        FROM `user_vehicle_detail` uvd JOIN `vehicle_type` vet
        ON uvd.`user_admin_id`='$admin_id' 
        AND uvd.`active_flag`='".FLAG_Y."' 
        AND uvd.`del_flag`='".FLAG_N."'
        AND vet.`active_flag`='".FLAG_Y."'
        AND vet.`vehicle_sort_nm`= uvd.`user_veh_type`
        LEFT JOIN `vehicle_class` vcl ON vcl.`vehicle_sort_nm`= uvd.`user_veh_type`
        left JOIN `user_lic_dtl` uld ON  uld.`user_admin_id`=uvd.`user_admin_id`
        AND vcl.`class_name`=uld.`user_lic_veh_class`
        AND uld.`active_flag`='".FLAG_Y."' 
        AND uld.`del_flag`='".FLAG_N."'
        GROUP BY uvd.`user_veh_dtl_id`
        ORDER BY  uvd.`user_veh_dtl_id` DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
	    $arr_catagory = $query->fetchAll();
	    foreach($arr_catagory as $val){
            $response['status'] = 1;
            $response['message'] = 'Successful';   
	        $vehicle['user_veh_dtl_id'] = $val['user_veh_dtl_id'];
	        $vehicle['user_veh_type'] = $val['user_veh_type'];
            $vehicle['vehicle_type_dec'] = $val['vehicle_type_dec'];
            $vehicle['vehicle_typ_img'] = $val['vehicle_typ_img'];
	        $vehicle['user_veh_number'] = $val['user_veh_number'];
	        $vehicle['user_veh_reg'] = $val['user_veh_reg'];
	        $vehicle['user_veh_reg_exp_dt'] = $val['user_veh_reg_exp_dt'];
	        $vehicle['user_ins_number'] = $val['user_ins_number'];
            $vehicle['user_ins_exp_dt'] = $val['user_ins_exp_dt'];
            $vehicle['user_ins_img'] = $val['user_ins_img'];
	        $vehicle['user_ins_img_status'] = $val['user_ins_img_status'];
	        $vehicle['user_emu_number'] = $val['user_emu_number'];
	        $vehicle['user_emu_exp_dt'] = $val['user_emu_exp_dt'];
            $vehicle['user_emu_img'] = $val['user_emu_img'];
            $vehicle['user_emu_img_status'] = $val['user_emu_img_status'];
	        $vehicle['veh_qr_code'] = $val['veh_qr_code'];
            $vehicle['rc_img'] = $val['rc_img'];
            $vehicle['rc_img_status'] = $val['rc_img_status'];
            $vehicle['oth_doc_no1'] = $val['oth_doc_no1'];
            $vehicle['oth_doc_no1_exp_dt'] = $val['oth_doc_no1_exp_dt'];
            $vehicle['oth_doc_no1_img'] = $val['oth_doc_no1_img'];
            $vehicle['oth_doc_no1_img_status'] = $val['oth_doc_no1_img_status'];
            $vehicle['oth_doc_no2'] = $val['oth_doc_no2'];
            $vehicle['oth_doc_no2_exp_dt'] = $val['oth_doc_no2_exp_dt'];
            $vehicle['oth_doc_no2_img'] = $val['oth_doc_no2_img'];
            $vehicle['oth_doc_no2_img_status'] = $val['oth_doc_no2_img_status'];
            $vehicle['user_lic_id'] = $val['user_lic_id'];
            $vehicle['user_lic_number'] = $val['user_lic_number'];
            $vehicle['user_lic_name'] = $val['user_lic_name'];
            $vehicle['user_lic_veh_class'] = $val['user_lic_veh_class'];
            $vehicle['user_lic_issue_date'] = $val['user_lic_issue_date'];
            $vehicle['user_lic_issued_by'] = $val['user_lic_issued_by'];
            $vehicle['user_lic_valid_till'] = $val['user_lic_valid_till'];
            $vehicle['s_d_w_of'] = $val['s_d_w_of'];
            $vehicle['blood_gr'] = $val['blood_gr'];
            $vehicle['date_of_birth'] = $val['date_of_birth'];
            $vehicle['address'] = $val['address'];
            $vehicle['user_lic_img'] = $val['user_lic_img'];
            $vehicle['user_lic_img_status'] = $val['user_lic_img_status'];

	        array_push($allplayerdata, $vehicle);
	        $response['vehicle'] = $allplayerdata;
	    }
	}else{
       	$response['status'] = 0;
       	$response['message'] = 'List Empty';
	}
	return ($response);
}*/
?>