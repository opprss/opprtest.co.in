<?php 
/*
Description: email verifyed 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
// require_once '../../connection-pdo.php';
require_once 'api/reg_api.php';
// require_once 'api/global_api.php';
$response = array();
if(isAvailable(array('user_admin_id','email','token'))){

 	if(isEmpty(array('user_admin_id','email','token'))){

	 	$user_admin_id = trim($_POST['user_admin_id']);
	 	$email = trim($_POST['email']);
	 	$token = trim($_POST['token']);
	 	$respon=email_verify($user_admin_id, $email);
        echo ($respon);
 	}else{
		$response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
 		echo json_encode($response);
    }
 }else{
	 $response['status'] = 0; 
	 $response['message'] = 'Invalid API Call';
     echo json_encode($response); 
 } 
?>