<?php
/*
Description: user address insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
    function user_address($user_admin_id,$user_address,$landmark,$user_add_area,$user_add_city,$user_add_state,$user_add_country,$user_add_pin,$mobile,$prk_admin_id,$tower_name,$falt_name){
        $response = array();
        $user_add_verify_flag=FLAG_Y;
        if (empty($prk_admin_id)|| $prk_admin_id=='' || $prk_admin_id=='0' || $prk_admin_id=='000' || $prk_admin_id<=0) {
            $user_add_verify_flag=FLAG_Y;

        }else{
            $user_add_verify_flag=FLAG_N;
        }
        global $pdoconn;
        $sql = "INSERT INTO `user_address`(`user_admin_id`,`user_address`,`landmark`,`user_add_area`,`user_add_city`,`user_add_state`,`user_add_country`,`user_add_pin`,`inserted_by`,`inserted_date`,`prk_admin_id`,`tower_name`,`falt_name`,`user_add_verify_flag`) VALUE ('$user_admin_id','$user_address','$landmark','$user_add_area','$user_add_city','$user_add_state','$user_add_country','$user_add_pin','$mobile','".TIME."','$prk_admin_id','$tower_name','$falt_name','$user_add_verify_flag')";
            $query = $pdoconn->prepare($sql);
        if($query->execute()){
            $user_add_id = $pdoconn->lastInsertId();
            $sql = "SELECT user_admin.user_mobile,
                user_detail.user_name,
                prk_area_dtl.prk_area_email,
                prk_area_dtl.prk_area_rep_name,
                tower_details.tower_name,
                flat_details.flat_name as falt_name
                FROM user_admin JOIN user_detail JOIN user_address JOIN prk_area_dtl 
                LEFT JOIN tower_details ON tower_details.tower_id=user_address.tower_name
                AND tower_details.prk_admin_id=user_address.prk_admin_id AND tower_details.active_flag='".FLAG_Y."' AND tower_details.del_flag='".FLAG_N."'
                LEFT JOIN  flat_details ON flat_details.flat_id=user_address.falt_name 
                AND flat_details.prk_admin_id=user_address.prk_admin_id AND flat_details.active_flag='".FLAG_Y."' AND flat_details.del_flag='".FLAG_N."'
                where user_admin.user_admin_id= '$user_admin_id' 
                and user_admin.user_admin_id = user_detail.user_admin_id
                and user_detail.active_flag='".FLAG_Y."'
                and user_address.user_admin_id=user_admin.user_admin_id
                and user_address.active_flag='".FLAG_Y."'
                and prk_area_dtl.prk_admin_id='$prk_admin_id'
                and prk_area_dtl.active_flag='".FLAG_Y."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();

                $user_mobile = $val['user_mobile'];
                $user_name = $val['user_name'];
                $tower_name = $val['tower_name'];
                $falt_name = $val['falt_name'];
                $prk_area_email = $val['prk_area_email'];
                $prk_area_rep_name = $val['prk_area_rep_name'];
                user_add_notification($user_mobile,$user_name,$tower_name,$falt_name,$prk_area_email,$prk_area_rep_name);
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['user_add_id'] = $user_add_id;
                $response = json_encode($response);
            }else{
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response = json_encode($response);
            }
        }else{
            $response['status'] = 0;
            $response['message'] = 'Not Successful';
            $response = json_encode($response);
        }
        return ($response); 
    }
	/*function user_address($user_admin_id,$user_address,$landmark,$user_add_area,$user_add_city,$user_add_state,$user_add_country,$user_add_pin,$mobile,$prk_admin_id,$tower_name,$falt_name){
	    $response = array();
        $user_add_verify_flag=FLAG_Y;
        if (empty($prk_admin_id)|| $prk_admin_id=='' || $prk_admin_id=='0' || $prk_admin_id=='000' || $prk_admin_id<=0) {
            $user_add_verify_flag=FLAG_Y;

        }else{
            $user_add_verify_flag=FLAG_N;
        }
	    global $pdoconn;
	    $sql = "INSERT INTO `user_address`(`user_admin_id`,`user_address`,`landmark`,`user_add_area`,`user_add_city`,`user_add_state`,`user_add_country`,`user_add_pin`,`inserted_by`,`inserted_date`,`prk_admin_id`,`tower_name`,`falt_name`,`user_add_verify_flag`) VALUE ('$user_admin_id','$user_address','$landmark','$user_add_area','$user_add_city','$user_add_state','$user_add_country','$user_add_pin','$mobile','".TIME."','$prk_admin_id','$tower_name','$falt_name','$user_add_verify_flag')";
            $query = $pdoconn->prepare($sql);
        if($query->execute()){
            $user_add_id = $pdoconn->lastInsertId();
            $sql = "SELECT user_admin.user_mobile,
                user_detail.user_name,
                user_address.tower_name,
                user_address.falt_name,
                prk_area_dtl.prk_area_email,
                prk_area_dtl.prk_area_rep_name
                FROM user_admin,user_detail,user_address,prk_area_dtl 
                where user_admin.user_admin_id= '$user_admin_id' 
                and user_admin.user_admin_id = user_detail.user_admin_id
                and user_detail.active_flag='".FLAG_Y."'
                and user_address.user_admin_id=user_admin.user_admin_id
                and user_address.active_flag='".FLAG_Y."'
                and prk_area_dtl.prk_admin_id='$prk_admin_id'
                and prk_area_dtl.active_flag='".FLAG_Y."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();

                $user_mobile = $val['user_mobile'];
                $user_name = $val['user_name'];
                $tower_name = $val['tower_name'];
                $falt_name = $val['falt_name'];
                $prk_area_email = $val['prk_area_email'];
                $prk_area_rep_name = $val['prk_area_rep_name'];
                user_add_notification($user_mobile,$user_name,$tower_name,$falt_name,$prk_area_email,$prk_area_rep_name);
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['user_add_id'] = $user_add_id;
                $response = json_encode($response);
            }else{
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response = json_encode($response);
            }
        }else{
            $response['status'] = 0;
            $response['message'] = 'Not Successful';
            $response = json_encode($response);
        }
	    return ($response); 
	}*/
?>