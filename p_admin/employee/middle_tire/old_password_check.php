<?php 
/*
Description: parkin employ username and passwod check
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// include_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('prk_user_username','oldpassword'))){
 	if(isEmpty(array('prk_user_username','oldpassword'))){

	 	$prk_user_username = trim($_POST['prk_user_username']);
	 	$oldpassword = trim($_POST['oldpassword']);
        
        $response=old_password_check($prk_user_username,$oldpassword);
        echo ($response);
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?>