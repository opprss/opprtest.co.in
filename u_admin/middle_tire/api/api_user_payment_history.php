<?php
/*
Description: user payment history.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_payment_history($user_admin_id,$month,$start_date,$end_date,$payment_type){
    if(!empty($start_date)&& !empty($end_date)){
       // $start_date=$start_date;
       // $end_date=$end_date;
       $format = "Y-m-d";
        if(date($format, strtotime($start_date)) == date($start_date) && date($format, strtotime($end_date)) == date($end_date)) {
            $start_date= $start_date;
            $end_date=  $end_date ;

        } else {
            $start_date = date("Y-m-d", strtotime($start_date));
            $end_date = date("Y-m-d", strtotime($end_date));
        }
    }elseif (!empty($month)) {
        date_default_timezone_set('Asia/Kolkata');
        $end_date= TIME_TRN;
        $start_date= date("Y-m-d",strtotime("-$month Months"));
    }else{
        $start_date='2017-08-02';
        $end_date='2999-12-31';
    }
    if(empty($payment_type)){
        $cash= CASH;
        $wallet= WALLET;
        $NA= TDGP;
        $dig= DGP;
        $a='';
    }else{
        $cash=$payment_type;
        $wallet=$payment_type;
        $NA=$payment_type;
        $dig=$payment_type;
        $a=$payment_type;
    }
    global $pdoconn;
    $response = array();
    $total = 0;
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT `payment_dtl`.`veh_number`,
    `payment_dtl`.`vehicle_type`,
    `vehicle_type`.`vehicle_type_dec`,
    `payment_dtl`.`total_hr`,
    `payment_dtl`.`round_hr`,
    `payment_dtl`.`total_pay`,
    `payment_dtl`.`payment_type` as payment_type_srt,
        (CASE `payment_dtl`.`payment_type` WHEN '".CASH."' ThEN '".CASH."' WHEN '".WALLET."' THEN '".WALLET."'
         WHEN '".TDGP."' THEN '".TDGP_F."' 
         WHEN '".DGP."' THEN '".DGP_F."' end) as 'payment_type',
    DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%D-%b-%Y') AS `veh_in_date`,
    DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%I:%i %p') AS `veh_in_time`,
    DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%D-%b-%Y') AS `veh_out_date`,
    DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%I:%i %p') AS `veh_out_time`,
    `prk_veh_trc_dtl`.`transition_date`,
    `payment_dtl`.`prk_admin_id`,
    `prk_area_dtl`.`prk_area_name`,
    `prk_area_dtl`.`prk_area_pro_img`,
    `user_detail`.`user_name`
    FROM `payment_dtl`,`prk_veh_trc_dtl`,`prk_area_dtl`,`vehicle_type`,`user_detail`
    WHERE `payment_dtl`.`prk_veh_trc_dtl_id`=`prk_veh_trc_dtl`.`prk_veh_trc_dtl_id`
    AND  `payment_dtl`.`prk_admin_id`= `prk_area_dtl`.`prk_admin_id`
    AND  `payment_dtl`.`payment_type` IN ('$cash','$wallet','$NA','$dig')
    AND `payment_dtl`.`pay_dtl_del_flag`='".FLAG_N."'
    AND  `prk_area_dtl`.`active_flag`='".FLAG_Y."'
    AND  `payment_dtl`.`user_admin_id`='$user_admin_id'
    AND  `payment_dtl`.`user_admin_id`= `user_detail`.`user_admin_id`
    AND `user_detail`.`active_flag` ='".FLAG_Y."'
    AND  `prk_veh_trc_dtl`.`transition_date` BETWEEN '$start_date' 
    AND '$end_date' AND `vehicle_type`.`vehicle_sort_nm` = `payment_dtl`.`vehicle_type`
     AND `vehicle_type`.`active_flag` = '".FLAG_Y."'
    ORDER BY  `prk_veh_trc_dtl`.`inserted_date` DESC ";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    $group = array();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val)
        {
            $td_check=$val['transition_date'];
            $resp['veh_number'] = $val['veh_number'];
            $resp['vehicle_type'] = $val['vehicle_type'];
            $resp['total_hr'] = $val['total_hr'];
            $resp['round_hr'] = $val['round_hr'];
            $resp['total_pay'] = $val['total_pay'];
            $total= $total + $val['total_pay'];
            $user_name= $val['user_name'];
            $resp['payment_type_srt'] = $val['payment_type_srt'];
            $resp['payment_type'] = $val['payment_type'];
            $resp['veh_in_date'] = $val['veh_in_date'];
            $resp['veh_in_time'] = $val['veh_in_time'];
            $resp['veh_out_date'] = $val['veh_out_date'];
            $resp['veh_out_time'] = $val['veh_out_time'];
            $resp['transition_date'] = $val['transition_date'];
            $resp['prk_area_name'] = $val['prk_area_name'];
            $resp['prk_area_pro_img'] = $val['prk_area_pro_img'];
            $resp['vehicle_type_dec'] = $val['vehicle_type_dec'];
            $park[$val['transition_date']][] = $resp;
            $response['status'] = 1;
            $response['user_name'] = $user_name;
            $response['total'] = $total;
            $response['start_date'] = $start_date;
            $response['end_date'] = $end_date;

            $response['message'] = 'Successful';
            $response['payment_history'] = $park;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'Empty Payment History';
    }
    return json_encode($response);
}
?>