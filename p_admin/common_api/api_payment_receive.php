<?php 
/*
Description: parking employ payment resive verify.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function  payment_receive($payment_dtl_id,$prk_out_rep_name,$payment_type){
    global $pdoconn;
    $response = array();
    $sql ="SELECT payment_dtl.prk_veh_trc_dtl_id,
        payment_dtl.user_admin_id,
        payment_dtl.prk_admin_id,
        payment_dtl.total_pay,
        payment_dtl.who_veh_out,
        prk_area_admin.sms_flag,
        prk_veh_trc_dtl.advance_pay,
        prk_veh_trc_dtl.user_mobile,
        (payment_dtl.total_pay - prk_veh_trc_dtl.advance_pay) as gross_pay,
        prk_area_dtl.prk_area_short_name
        FROM payment_dtl,prk_veh_trc_dtl,prk_area_dtl,prk_area_admin 
        where prk_veh_trc_dtl.prk_veh_trc_dtl_id = payment_dtl.prk_veh_trc_dtl_id
        and  prk_area_dtl.prk_admin_id = payment_dtl.prk_admin_id
        and prk_area_admin.prk_admin_id= payment_dtl.prk_admin_id
        and  prk_area_dtl.active_flag = '".FLAG_Y."'
        AND payment_dtl_id = '$payment_dtl_id'";        
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $prk_veh_trc_dtl_id = $val['prk_veh_trc_dtl_id'];
        $user_admin_id = $val['user_admin_id'];
        $prk_admin_id = $val['prk_admin_id'];
        $total_pay = $val['total_pay'];
        $advance_pay = $val['advance_pay'];
        $sms_flag = $val['sms_flag'];
        $user_mobile = $val['user_mobile'];
        $who_veh_out = $val['who_veh_out'];
        $gross_pay = $val['gross_pay'];
        $prk_area_short_name = $val['prk_area_short_name'];
        if($gross_pay>0){
            $unique_prk_short_name = $prk_area_short_name;
            $unique_type = FLAG_CR;
            $unique_ke = unique_key_generate($unique_prk_short_name,$unique_type,$prk_admin_id);
            $json = json_decode($unique_ke);
            if($json->status=='1'){
                $payment_receive_number = $json->invoice_no;
                $unique_id = $json->unique_id;
            }
            $payment_amount = $gross_pay;
            $prk_user_username = $prk_out_rep_name;
            $payment_statas = FLAG_S;
            $in_out_flag = FLAG_O;
            $payment_re = payment_receive_insert($payment_receive_number,$prk_admin_id,$prk_veh_trc_dtl_id,$payment_type,$payment_amount,$payment_statas,$prk_user_username,$in_out_flag);
        }

        $sql ="UPDATE `payment_dtl` SET `payment_rec_status`='".FLAG_P."',`updated_by`='$prk_out_rep_name',`payment_rec_emp_name`='$prk_out_rep_name',`payment_rec_date`='".TIME."',`updated_date`='".TIME."',`payment_type`='$payment_type' WHERE `payment_dtl_id`='$payment_dtl_id'";
        $query = $pdoconn->prepare($sql);
        $query->execute();
        $sql = "UPDATE `prk_veh_trc_dtl` SET `prk_out_rep_name`='$prk_out_rep_name',`payment_rec_emp_name`='$prk_out_rep_name' WHERE  prk_veh_trc_dtl_id ='$prk_veh_trc_dtl_id'";
        $query = $pdoconn->prepare($sql);
        if($query->execute()){
            if ($sms_flag==FLAG_Y & $who_veh_out==FLAG_P &!empty($user_mobile)) {
                $veh_out_dtl = prk_veh_out_dtl($payment_dtl_id);
                $json = json_decode($veh_out_dtl);
                if($json->status=='1'){
                    $prk_area_name=$json->prk_area_name;
                    $veh_in_date=$json->veh_in_date;
                    $veh_in_time=$json->veh_in_time;
                    $veh_out_time=$json->veh_out_time;
                    $veh_out_date=$json->veh_out_date;
                    $total_pay=$json->total_pay;
                    $tot_hr=$json->tot_hr;
                    $round_hr=$json->round_hr;
                    $payment_type=$json->payment_type;
                    $user_mobile=$json->user_mobile;
                    prk_veh_out_dtl_sms($user_mobile,$prk_area_name,$veh_in_date,$veh_in_time,$veh_out_date,$veh_out_time,$total_pay,$tot_hr,$round_hr); 
                }
            }
            $response['status'] = 1;
            $response['message'] = 'Successful';
        }else{
            $response['status'] = 0;
            $response['message'] = 'Not Successful';
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
    }
    return json_encode($response);
}
?>