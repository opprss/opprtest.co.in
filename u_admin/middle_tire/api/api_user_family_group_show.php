<?php 
/*
Description: subumit lisst.
Developed by: Rakhal Raj Mandal
Created Date: 14-06-2018
Update date :---------
*/
function user_family_group_show($user_admin_id,$user_add_id,$user_family_id){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
	$sql="SELECT `user_family_unique_id`, `user_family_id`, `user_admin_id`, `user_add_id`, `prk_admin_id`, `user_family_name`, `user_family_gender`, `user_family_relation`, `user_family_mobile`, `intercom_per`,
        IF(`user_family_img` IS NULL OR `user_family_img`='','',CONCAT('".USER_BASE_URL."',`user_family_img`)) as user_family_img
        FROM `user_family_group` WHERE `user_admin_id`='$user_admin_id' AND `user_family_id`='$user_family_id' AND `user_add_id`='$user_add_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."' ORDER BY 1 DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
	    $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['user_family_unique_id'] = $val['user_family_unique_id'];
        $response['user_family_id'] = $val['user_family_id'];
        $response['user_admin_id'] = $val['user_admin_id'];
        $response['user_add_id'] = $val['user_add_id'];
        $response['prk_admin_id'] = $val['prk_admin_id'];
        $response['user_family_name'] = $val['user_family_name'];
        $response['user_family_gender'] = $val['user_family_gender'];
        $response['user_family_relation'] = $val['user_family_relation'];
        $response['user_family_mobile'] = $val['user_family_mobile'];
        $response['intercom_per'] = $val['intercom_per'];
        $response['user_family_img'] = $val['user_family_img'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>