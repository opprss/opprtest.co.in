<?php 
/*
Description: Parking employ vehicle verify and data insert trc table.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
 // 'prk_admin_id','prk_area_user_id','in_prk_area_user_id','prk_user_username','token'
function employee_attendance_list($prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql ="SELECT `employee_attendance_id`,`prk_admin_id`,`prk_area_user_id`,`attendance_date`,`in_time`,`out_time` FROM `employee_attendance_list` WHERE `prk_admin_id`= '$prk_admin_id' AND (`out_time` IS NULL or `out_time` = '')";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val) {
            $response['status'] = 1;
            $response['message'] = 'Successful';
            $park['employee_attendance_id'] = $val['employee_attendance_id'];
            $park['prk_admin_id'] = $val['prk_admin_id'];
            $park['prk_area_user_id'] = $val['prk_area_user_id'];
            $park['attendance_date'] = $val['attendance_date'];
            $park['in_time'] = $val['in_time'];
            $park['out_time'] = $val['out_time'];
            
            array_push($allplayerdata, $park);
            $response['employee'] = $allplayerdata;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response); 
}
?>