<?php
/*
Description: user address insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_kids_add($user_admin_id,$mobile,$prk_admin_id,$user_family_id,$alone_flag,$rep_name,$rep_image,$date,$from_time,$to_time,$call_type){
    $response = array();
    global $pdoconn;
    if(!empty($rep_image)){
        $PNG_TEMP_DIR ='./../uploades/'.$mobile.'/family_rep_image/';
        if (!file_exists($PNG_TEMP_DIR)) {
            $old_mask = umask(0);
            mkdir($PNG_TEMP_DIR, 0777, TRUE);
            umask($old_mask);
        }
        if ($call_type=='M') {
            $decode_file = base64_decode($rep_image);
            $file= uniqid().'.jpg';
            $file_dir=$PNG_TEMP_DIR.$file;
            if(file_put_contents($file_dir, $decode_file)){
                api_image_upload_compress($file_dir);
                $rep_image='uploades/'.$mobile.'/family_rep_image/'.$file;
            }else{
                $rep_image = NULL;
            }
        }else if ($call_type=='W') {
            $image_parts = explode(";base64,", $rep_image);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
          
            $image_base64 = base64_decode($image_parts[1]);
            $fileName = uniqid() . '.png';
            $file = $PNG_TEMP_DIR . $fileName;
            if(file_put_contents($file, $image_base64)){
                # code...
                $rep_image='uploades/'.$mobile.'/family_rep_image/'.$fileName;
            }else{
                # code...
                $rep_image = '';
            }
        }else{
            # code...
            $rep_image = '';
        }
    }else{
        # code...
        $rep_image = '';
    }
    $format = "Y-m-d";
    if(date($format, strtotime($date)) == date($date)) {
        $date= $date;
    }else{
        $date = date("Y-m-d", strtotime($date));
    }
    $id=uniqid();
    $sql = "INSERT INTO `kids_allow`(`kids_allow_id`, `prk_admin_id`, `user_admin_id`, `user_family_id`, `alone_flag`, `rep_name`, `rep_image`, `date`, `from_time`, `to_time`, `in_date`, `inserted_by`, `inserted_date`) VALUES ('$id','$prk_admin_id','$user_admin_id','$user_family_id','$alone_flag','$rep_name','$rep_image','$date','$from_time','$to_time','".TIME."','$mobile','".TIME."')";
        $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response = json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
        $response = json_encode($response);
    }
    return ($response); 
}
?>