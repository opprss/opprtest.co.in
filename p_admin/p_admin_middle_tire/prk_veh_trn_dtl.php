<?php 
/*
Description: Parking area admin out the vehicle.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
	require_once 'api/parkAreaReg_api.php';
	// require_once 'api/global_api.php';
	$response = array();
	if(isAvailable(array('prk_veh_trc_dtl_id','prk_admin_id','prk_out_rep_name','token','prk_area_short_name'))){
	 	if(isEmpty(array('prk_veh_trc_dtl_id','prk_admin_id','prk_out_rep_name','token','prk_area_short_name'))){

	 		$prk_area_gate_id = 'ADMIN';
	 		//$prk_area_short_name = 'HDA';
	 		
		 	$prk_admin_id = trim($_POST['prk_admin_id']);
		 	$prk_veh_trc_dtl_id = trim($_POST['prk_veh_trc_dtl_id']);
		 	$prk_out_rep_name = trim($_POST['prk_out_rep_name']);
		 	$token = trim($_POST['token']);
		 	$prk_area_short_name = trim($_POST['prk_area_short_name']);

		 	$resp=prk_token_check($prk_admin_id,$token);
	        $json = json_decode($resp);
	        if($json->status){
	        	$rate_cal= rate_calculate($prk_veh_trc_dtl_id,$prk_out_rep_name,$prk_admin_id,$prk_area_gate_id,$prk_area_short_name);
	        	$json = json_decode($rate_cal);
 				if($json->status=='1'){
	 				$payment_dtl_id=$json->payment_dtl_id;
	 				$response = prk_veh_out_dtl($payment_dtl_id);
	  				echo ($response);
 				}else{
				//   $response['status'] = 0; 
				//   $response['message'] = 'payment_dtl data not insert';
				// 	 echo json_encode($response);
				 	echo $rate_cal;
				}
	        }else{
	            $response = $resp;
	            echo $response;
	        }
	 	}else{
	            $response['status'] = 0;
		 		$response['message'] = 'All Fields Are Mandatory';
	            echo json_encode($response);
	    }
	}else{
	    $response['status'] = 0; 
	    $response['message'] = 'Invalid API Call';
	 	echo json_encode($response);
	}
?>