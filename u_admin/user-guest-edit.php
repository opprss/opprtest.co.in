<!-- ################################################
Description: List of user's vechile | user can delete vehicle from this page.
Developed by: Soemen Banerjee
Created Date: 17-03-2018
 ####################################################-->
<?php 
  include "all_nav/header.php";
  $user_guest_list_id=base64_decode($_REQUEST['list_id']);
  $revisit=base64_decode($_REQUEST['re']);
  $prk_area_list1=user_add_prk_area_list($user_admin_id);
  $prk_area_list = json_decode($prk_area_list1, true);
  $guest_show1 = user_guest_list_show($user_guest_list_id,$user_admin_id,$user_mobile);
  $guest_show = json_decode($guest_show1, true);
  // $guest_show['guest_name']
?>
<!-- Time Picker -->
  <script src="js/timepicker.min.js"></script>
  <link href="css/timepicker.min.css" rel="stylesheet">
<!-- Time Picker -->
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
  .error{
    font-size: 11px;
    color: red;
  }
</style>
  <div class="am-mainpanel"><!-- cloding in footer -->
    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Guest Edit<//?=$revisit?></h5>
    </div><!-- am-pagetitle -->
    
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40" >
        <!-- <h6 class="card-body-title">Add Employee</h6> -->
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <form role="form" id="addGuest" method="post"  enctype="multipart/form-data">
            <div class="row mg-b-25">
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">Choose Area<span class="tx-danger">*</span></label>
                  <div class="select" style="font-size:14px">
                    <select name="prk_admin_id" id="prk_admin_id" style="color: skyblue">
                      <option value="">SELECT AREA</option>
                       <?php
                        foreach ($prk_area_list['prk_area'] as $val){ ?>  
                          <option value="<?php echo $val['prk_admin_id']?>"><?php echo $val['prk_area_name']?></option>
                        <?php          
                        }
                      ?> 
                    </select>
                  </div>
                  <span style="position: absolute; display: none;" id="error_prk_admin_id" class="error_size">Select Area</span>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">Visitor Name<span class="tx-danger">*</span></label>
                  <input type="text"  name="vis_name" class="form-control" placeholder="Visitor Name" id="vis_name">
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">Visitor Mobile<span class="tx-danger">*</span></label>
                  <input type="text"  name="vis_mob" class="form-control" placeholder="Visitor Mobile" id="vis_mob" maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                </div>
              </div>
              <div class="col-lg-3">
                <div class="row mg-b-25">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="form-control-label size">Number of Visitor<span class="tx-danger">*</span></label>
                      <input type="text"  name="num_of_vis" class="form-control" placeholder="No. of Visito" id="num_of_vis" value="1">
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="form-control-label size">Visitor Gender<span class="tx-danger">*</span></label>
                        <label class="rdiobox">
                          <input name="gender" id="male" value="M" type="radio">
                          <span>Male </span>
                        </label>
                        <label class="rdiobox">
                          <input name="gender" id="female" value="F" type="radio">
                          <span>Female </span>
                        </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">Visitor Address<span class="tx-danger">*</span></label>
                  <input type="text"  name="vis_add" class="form-control" placeholder="Visitor Address" id="vis_add">
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">Visitor Purpose<span class="tx-danger">*</span></label>
                  <input type="text"  name="vis_pur" class="form-control" placeholder="Visitor Purpose" id="vis_pur">
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">Visitor ID Proof<span class="tx-danger"></span></label>
                  <input type="text"  name="vis_id_pro" class="form-control" placeholder="Visitor ID Proof" id="vis_id_pro">
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">Visitor IN Date<span class="tx-danger">*</span></label>
                  <input type="text"  name="vis_in_da" class="form-control prevent_readonly" placeholder="Visitor IN Date Purpose" id="vis_in_da" readonly="readonly">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="row mg-b-25">
                  <div class="col-lg-3">
                    <div class="form-group">
                      <label class="form-control-label size">Time (From)<span class="tx-danger">*</span></label>
                      <input type="text"  name="vis_out_da" class="form-control prevent_readonly" placeholder="HH:MM" id="vis_out_da" readonly="readonly">
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="form-group">
                      <label class="form-control-label size">Time (To)<span class="tx-danger">*</span></label>
                      <input type="text"  name="form_time" class="form-control prevent_readonly" placeholder="HH:MM" id="form_time" readonly="readonly">
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="form-group">
                      <label class="form-control-label size">Verify Visitor In<span class="tx-danger">*</span></label>
                        <div class="select">
                          <select name="vis_in_ver" id="vis_in_ver" value="" style="opacity: 0.8; font-size:14px">
                            <option value="">Select</option>
                            <option value="Y">YES</option>
                            <option value="N">NO</option>
                          </select>
                        </div>
                        <span style="position: absolute; display: none;" id="error_vis_in_ver" class="error_size">Please Select</span>
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="form-group">
                      <label class="form-control-label size">Verify Visitor Out<span class="tx-danger">*</span></label>
                        <div class="select">
                          <select name="vis_out_ver" id="vis_out_ver" value="" style="opacity: 0.8; font-size:14px">
                            <option value="">Select</option>
                            <option value="Y">YES</option>
                            <option value="N">NO</option>
                          </select>
                        </div>
                        <span style="position: absolute; display: none;" id="error_vis_out_ver" class="error_size">Please Select</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="row mg-b-25 mg-t-35">
                  <div class="col-lg-4">
                    <!-- <div class="form-group mg-t-35"> -->
                    <div class="form-group">
                      <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="vis_save" id="vis_save">
                    </div>
                  </div>
                  <!-- <div class="col-lg-4">
                    <div class="form-group">
                      <button type="reset" class="btn btn-block prk_button btn-primary">Reset</button>
                    </div>
                  </div> -->
                  <div class="col-lg-4">
                    <!-- <div class="form-group mg-t-35"> -->
                    <div class="form-group">
                      <button type="button" class="btn btn-block prk_button btn-primary" id="back">Back</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div><!-- am-pagebody -->
    </div>
  <!-- footer part -->
  <?php include"all_nav/footer.php"; ?>
  <!-- footer part -->
<script type="text/javascript">
  var userUrl = "<?php echo USER_URL; ?>";
  var user_admin_id = "<?php echo $user_admin_id; ?>";
  var token = "<?php echo $token; ?>";
  var mobile = "<?php echo $user_mobile; ?>";
  var user_guest_list_id = "<?php echo $user_guest_list_id; ?>";
  var revisit = "<?php echo $revisit; ?>";
  // alert(revisit);
  // -----------------------------
  $( document ).ready( function () {
    $('#prk_admin_id').val('<?php echo $guest_show['prk_admin_id'];?>');
    $('#vis_name').val('<?php echo $guest_show['guest_name'];?>');
    $('#vis_mob').val('<?php echo $guest_show['guest_mobile'];?>');
    $('#vis_add').val('<?php echo $guest_show['guest_address'];?>');
    $('#vis_pur').val('<?php echo $guest_show['guest_purpose'];?>');
    $('#vis_id_pro').val('<?php echo $guest_show['guest_id_proof'];?>');
    $('#num_of_vis').val('<?php echo $guest_show['no_of_guest'];?>');
    var user_guest_list_in = userUrl+'user_guest_list_in.php';
    if (revisit=='N') {
      $('#vis_in_da').val('<?php echo $guest_show['in_date'];?>');
      $('#vis_out_da').val('<?php echo $guest_show['to_time'];?>');
      $('#form_time').val('<?php echo $guest_show['form_time'];?>');
      user_guest_list_in = userUrl+'user_guest_list_edit.php';
    }
    $('#vis_in_ver').val('<?php echo $guest_show['guest_in_verify'];?>');
    $('#vis_out_ver').val('<?php echo $guest_show['guest_out_verify'];?>');
    $("input[name=gender][value='<?php echo $guest_show['guest_gender'];?>']").prop("checked",true);
    // $('#vis_in_da').attr("disabled", true);
    $('#vis_save').click(function () {
      // alert('ok');
      var vis_name=$('#vis_name').val();
      var vis_mob=$('#vis_mob').val();
      var gender =  $('input[name=gender]:checked').val();
      var vis_add=$('#vis_add').val();
      var vis_pur=$('#vis_pur').val();
      var num_of_vis=$('#num_of_vis').val();
      var vis_id_pro=$('#vis_id_pro').val();
      var vis_in_da=$('#vis_in_da').val();
      var vis_out_da=$('#vis_out_da').val();
      var form_time=$('#form_time').val();
      var vis_in_ver=$('#vis_in_ver').val();
      var vis_out_ver=$('#vis_out_ver').val();
      var prk_admin_id=$('#prk_admin_id').val();
      if($("#addGuest").valid()){
        $(this).val('Wait ...').attr("disabled", true);
        $.ajax({
          url :user_guest_list_in,
          type:'POST',
          data :{//user_guest_list_id','guest_name','user_admin_id','guest_mobile','guest_gender','guest_address','guest_purpose','no_of_guest','guest_id_proof','in_date','to_time','form_time','guest_in_verify','guest_out_verify','mobile','token','prk_admin_id
            'user_guest_list_id':user_guest_list_id,
            'guest_name':vis_name,
            'user_admin_id':user_admin_id,
            'guest_mobile':vis_mob,
            'guest_gender':gender,
            'guest_address':vis_add,
            'guest_purpose':vis_pur,
            'no_of_guest':num_of_vis,
            'guest_id_proof':vis_id_pro,
            'in_date':vis_in_da,
            'to_time':vis_out_da,
            'form_time':form_time,
            'guest_in_verify':vis_in_ver,
            'guest_out_verify':vis_out_ver,
            'mobile':mobile,
            'token':token,
            'prk_admin_id':prk_admin_id
          },
          dataType:'html',
          success  :function(data){
            // $('#prk_area_name').attr("disabled", true);
            $('#vis_save').val('SAVE').attr('disabled', false);
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.9em;'>Guest Edit Successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                    window.location='user-guest-list';
                    // location.reload();
                  }
                }
              });
            }else{
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
                type: 'red'
              });
              if (json.session==0) {
                window.location.replace("logout.php");
              }
            }
          }
        });
      }
    });
    $('#back').click(function(){

      window.location='user-guest-list';
    });
    $( "#addGuest" ).validate( {
      rules: {
        vis_name: "required",
        vis_mob:{
          required:true,
          maxlength:10,
          minlength:10
        },
        vis_add: "required",
        vis_pur: "required",
        vis_in_da: "required",
        vis_out_da: "required",
        vis_in_ver: "vis_in_ver_check",
        vis_out_ver: "vis_out_ver_check",
        num_of_vis:"required",
        prk_admin_id:"prk_admin_id_check"
      },
      messages: {
        vis_name:"Please Enter the Name",
        vis_mob:{
          required:"Please Enter the Mobile No.",
          minlength:"Minimum length is 10",
          maxlength:"Maximum length is 10"
        },
        vis_add:"Please Enter the Address",
        vis_pur:"Please Enter the Purpose",
        vis_in_da:"Please Enter the IN Data",
        vis_out_da:"Please Enter the Out Date",
        num_of_vis:"Enter The No. of Visitor"
      }
    });
    $.validator.addMethod("vis_in_ver_check", function(value){
      // alert(value);
      arg = "";
      if (value != "") {
        //return true;
        $("#error_vis_in_ver").hide();
        return arg !== value;
      }else{
        $("#error_vis_in_ver").show();
        return false;
      }
    });
    $.validator.addMethod("vis_out_ver_check", function(value){
      // alert(value);
      arg = "";
      if (value != "") {
        //return true;
        $("#error_vis_out_ver").hide();
        return arg !== value;
      }else{
        $("#error_vis_out_ver").show();
        return false;
      }
    });
    $.validator.addMethod("prk_admin_id_check", function(value){
      // alert(value);
      arg = "";
      if (value != "") {
        //return true;
        $("#error_prk_admin_id").hide();
        return arg !== value;
      }else{
        $("#error_prk_admin_id").show();
        return false;
      }
    });
  });
</script>
<script>
  $(document).ready(function () {
    addEventListener('DOMContentLoaded', function () {
      var now = new Date;
      var now = now.setDate(now.getDate() - 1);
      pickmeup('#vis_in_da', {
        default_date   : false,
        format  : 'd-m-Y',
        hide_on_select : true,
        render : function (date) {
          if (now > date) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
          }
          return {};
        } 
      });
    });
    /*$('#vis_out_da').focus(function () {
      var start = new Date;
      var end = $('#vis_in_da').val();
      var d = new Date(end.split("-").reverse().join("-"));
      var dd=d.getDate();
      var mm=d.getMonth()+1;
      var yy=d.getFullYear();
      var hh = start.getHours();
      var min = start.getMinutes();
      var sec = start.getSeconds();
      var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
      var end = new Date(newdate);
      days = (end - start) / (1000 * 60 * 60 * 24)-1;
      days = Math.round(days);
      now_end = start.setDate(start.getDate() + days);
      pickmeup('#vis_out_da', {
        default_date   : false,
        hide_on_select : true,
        render : function (date) {
          if (date < now_end) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
          }
          return {};
        } 
      });
    });
    $('#vis_in_da').focus(function(){

      $('#vis_out_da').val('');
    });*/
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>
<style type="text/css">
  ._jw-tpk-container{
    height: 180px;
  }
</style>
<script type="text/javascript">
  var form_time = document.getElementById('form_time');
  var timepicker = new TimePicker(['vis_out_da', 'form_time'], {
      lang: 'en',
      theme: 'dark'
  });
  timepicker.on('change', function(evt) {
      var value = (evt.hour || '00') + ':' + (evt.minute || '00');
      if (evt.element.id === 'form_time') {
        form_time.value = value;
      } else {
        vis_out_da.value = value;
      }
  });
</script>
