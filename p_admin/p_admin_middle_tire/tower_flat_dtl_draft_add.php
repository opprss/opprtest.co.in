<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','inserted_by','tower_id','flat_id','owner_name','owner_mobile','owner_email','owner_gender','living_status','tenant_name','tenant_mobile','tenant_email','tenant_gender','four_wheeler_1','four_wheeler_2','four_wheeler_3','tow_wheeler_1','tow_wheeler_2','tow_wheeler_3','parking_space_1','parking_space_2','parking_space_3'))){
    if(isEmpty(array('prk_admin_id','tower_id','flat_id','owner_name','owner_mobile','living_status'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $inserted_by = trim($_POST['inserted_by']);
        $tower_id = trim($_POST['tower_id']);
        $flat_id = trim($_POST['flat_id']);
        $owner_name = trim($_POST['owner_name']);
        $owner_mobile = trim($_POST['owner_mobile']);
        $owner_email = trim($_POST['owner_email']);
        $owner_gender = trim($_POST['owner_gender']);
        $living_status = trim($_POST['living_status']);
        $tenant_name = trim($_POST['tenant_name']);
        $tenant_mobile = trim($_POST['tenant_mobile']);
        $tenant_email = trim($_POST['tenant_email']);
        $tenant_gender = trim($_POST['tenant_gender']);
        $four_wheeler_1 = trim($_POST['four_wheeler_1']);
        $four_wheeler_2 = trim($_POST['four_wheeler_2']);
        $four_wheeler_3 = trim($_POST['four_wheeler_3']);
        $tow_wheeler_1 = trim($_POST['tow_wheeler_1']);
        $tow_wheeler_2 = trim($_POST['tow_wheeler_2']);
        $tow_wheeler_3 = trim($_POST['tow_wheeler_3']);
        $parking_space_1 = trim($_POST['parking_space_1']);
        $parking_space_2 = trim($_POST['parking_space_2']);
        $parking_space_3 = trim($_POST['parking_space_3']);

        #--------------------#
        $tow_flat_dtl_draft_id=uniqid();
        $response = tower_flat_dtl_draft_add($tow_flat_dtl_draft_id,$prk_admin_id,$tower_id,$flat_id,$owner_name,$owner_mobile,$owner_email,$owner_gender,$living_status,$tenant_name,$tenant_mobile,$tenant_email,$tenant_gender,$four_wheeler_1,$four_wheeler_2,$four_wheeler_3,$tow_wheeler_1,$tow_wheeler_2,$tow_wheeler_3,$parking_space_1,$parking_space_2,$parking_space_3,$inserted_by);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
 }
echo $response;
?>