<?php
/*
Description: user draft registration.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
/*------------------------*/
function draft_registration($user_mobile,$user_veh_type,$user_veh_number){
    global $pdoconn;
    $v_num=v_numberChecker($user_veh_number);
    $json = json_decode($v_num);
    $user_veh_reg = NULL;
    $user_veh_reg_exp_dt = NULL;
    if($json->status=='1'){

        $sql = "SELECT `user_admin_id`,`user_ac_status`,`temp_password` FROM `user_admin` WHERE `user_mobile`='$user_mobile'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $val = $query->fetch();
        $user_admin_id= $val['user_admin_id'];
        $count=$query->rowCount();
        if($count>0){
            $user_ac_status= $val['user_ac_status'];
            $temp_password= $val['temp_password'];

            /*$resp=vehicle_count($user_veh_number,$user_admin_id);
            $json = json_decode($resp);
            if($json->status=='1'){*/
                 if($user_ac_status== FLAG_D){
                     $response['status'] = 1;
                     $response['user_admin_id'] = $user_admin_id;
                     $response['user_ac_status'] = $user_ac_status;
                     $response['user_veh_insert'] = FLAG_N;
                     $response['user_mobile'] = $user_mobile;
                     $response['password'] = $temp_password;
                     $response['message'] = 'registration sucessfull';
                     $return = json_encode($response);
                 }else{
                     $response['status'] = 1;
                     $response['user_admin_id'] = $user_admin_id;
                     $response['user_ac_status'] = $user_ac_status;
                     $response['user_veh_insert'] = FLAG_N;
                     $response['password'] = $temp_password;
                     $response['user_mobile'] = $user_mobile;
                     $response['message'] = 'registration sucessfull';
                     $return =  json_encode($response);
                 }
            /*}else{
                $add_veh = add_vehicle($user_admin_id,$user_veh_type,$user_veh_number,$user_veh_reg,$user_veh_reg_exp_dt,$user_mobile);
                $json = json_decode($add_veh);
                if($json->status=='1'){
                    if($user_ac_status== FLAG_D){
                        $response['status'] = 1;
                        $response['user_admin_id'] = $user_admin_id;
                        $response['user_ac_status'] = $user_ac_status;
                        $response['user_veh_insert'] = FLAG_Y;
                        $response['user_mobile'] = $user_mobile;
                        $response['password'] = $temp_password;
                        $response['message'] = 'registration sucessfull';
                        $return =  json_encode($response);
                    }
                }else{
                    $response['status'] = 1;
                    $response['user_admin_id'] = $user_admin_id;
                    $response['user_ac_status'] = $user_ac_status;
                    $response['user_veh_insert'] = FLAG_N;
                    $response['password'] = $temp_password;
                    $response['user_mobile'] = $user_mobile;
                    $response['message'] = 'registration sucessfull';
                    $return =  json_encode($response);
                }
            }*/
         }else{
             $pass=random_password();
             $password=md5($pass);
             $response = array();
             $sql = "INSERT INTO `user_admin`(`user_mobile`,`user_password`,`temp_password`,`inserted_by`,`user_ac_status`,`inserted_date`) VALUE ('$user_mobile','$password','$pass','$user_mobile','".FLAG_D."','".TIME."')";
             $query = $pdoconn->prepare($sql);
             if($query->execute()){
                $user_admin_id = $pdoconn->lastInsertId();
                /*$add_veh = add_vehicle($user_admin_id,$user_veh_type,$user_veh_number,$user_veh_reg,$user_veh_reg_exp_dt,$user_mobile);
                $json = json_decode($add_veh);
                if($json->status=='1'){*/
                         $response['status'] = 1;
                         $response['user_admin_id'] = $user_admin_id;
                         $response['user_veh_insert'] = FLAG_Y;
                         $response['user_ac_status'] = FLAG_D;
                         $response['user_mobile'] = $user_mobile;
                         $response['password'] = $pass;
                         $response['message'] = 'registration sucessfull';
                         $return = json_encode($response);
                /*}else{
                     $response['status'] = 0;
                     $response['message'] = 'registration wrong';
                     $return = json_encode($response);
                }*/
            }else{
                $response['status'] = 0;
                $response['message'] = 'registration wrong';
                $return = json_encode($response);
            }
        }
    }else{
        $return = $v_num;
    }
    return $return;
}
?>