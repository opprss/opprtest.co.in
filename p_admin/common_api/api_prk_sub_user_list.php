<?php
/*
Description: Parking area employ list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_sub_user_list($prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT `prk_area_user_id`,
        `prk_user_username`,
        `prk_user_emp_category`,
        `prk_user_emp_no`,
        `prk_user_name`,
        `prk_user_password`,
        `prk_user_mobile`,
        `prk_user_gender`,
        `prk_user_img`, 
        `active_flag` 
        FROM `prk_area_user` 
        WHERE `prk_admin_id`='$prk_admin_id' AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['prk_area_user_id'] = $val['prk_area_user_id'];
            $park['prk_user_username'] = $val['prk_user_username'];
            $park['prk_user_mobile'] = $val['prk_user_mobile'];
            $park['prk_user_gender'] = $val['prk_user_gender'];
            $park['prk_user_emp_no'] = $val['prk_user_emp_no'];
            $park['prk_user_name'] = $val['prk_user_name'];
            $park['active_flag'] = $val['active_flag'];
            $park['prk_user_img'] = $val['prk_user_img'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['mail_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return $allplayerdata;
    // return json_encode($response);
}
?>