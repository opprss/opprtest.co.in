<?php 
/*
Description: parking area visit the people. 
Developed by: Rakhal Raj Mandal
Created Date: 09-04-2018
Update date : 29-05-2018
*/ 
function prk_visit_dtl($visit_name,$visit_gender,$visit_mobile,$visit_purpose,$visit_meet_to,$visit_address,$visit_with_veh,$visit_card_id,$visit_img,$prk_admin_id,$prk_area_user_id,$prk_user_username,$veh_type,$veh_number,$prk_veh_trc_dtl_id,$visitor_meet_to_name,$visitor_meet_to_address,$visitor_token,$unique_id,$visitor_type,$in_verify,$out_verify,$no_of_guest,$call_type,$tower_id,$flat_id){
    global $pdoconn;
    $response = array();
    $prk_in =prk_inf($prk_admin_id);
    $json = json_decode($prk_in);
    $visitor_out_verify='';
    $visitor_in_verify='';
    if($json->status=='1'){
        $prk_admin_name = $json->prk_admin_name;
        $prk_area_name = $json->prk_area_name;
        $visitor_out_verify_flag = $json->visitor_out_verify_flag;
        $prient_in_flag = $json->prient_in_flag;
        switch ($visitor_type) {
            case FLAG_V:
                $visitor_in_verify=FLAG_Y;
                $visitor_out_verify=FLAG_Y;
                $visit_img='';
                break;
            case FLAG_G:
                if($visitor_out_verify_flag==FLAG_Y){
                    if ($in_verify==FLAG_Y) {
                        $visitor_in_verify=FLAG_D;
                    }else{
                        $visitor_in_verify=FLAG_Y;
                    }
                    if ($out_verify==FLAG_Y) {
                        $visitor_out_verify=FLAG_N;
                    }else{
                        $visitor_out_verify=FLAG_Y;
                    }
                }else{
                    $visitor_out_verify=FLAG_Y;
                    $visitor_in_verify=FLAG_Y;
                }
                break;
            case FLAG_U:
                $visit_img='';
                if($visitor_out_verify_flag==FLAG_Y){
                    $sql = "SELECT `user_admin_id`,`user_mobile`,`user_ac_status`,`visitor_verify_flag` FROM `user_admin` WHERE `user_mobile`='$visit_meet_to' AND `user_ac_status`='".FLAG_A."'";
                    $query  = $pdoconn->prepare($sql);
                    $query->execute();
                    $count=$query->rowCount();
                    if($count>0){
                        $val = $query->fetch();
                        $visitor_verify_flag = $val['visitor_verify_flag'];
                        if ($visitor_verify_flag==FLAG_Y) {
                            $visitor_out_verify=FLAG_N;
                            $visitor_in_verify=FLAG_D;
                        }else{
                            $visitor_out_verify=FLAG_Y;
                            $visitor_in_verify=FLAG_Y;
                        }
                    }else{
                        $visitor_out_verify=FLAG_Y;
                        $visitor_in_verify=FLAG_Y;
                    }
                }else{
                    $visitor_out_verify=FLAG_Y;
                    $visitor_in_verify=FLAG_Y;
                }
                break;
            default:
                if($visitor_out_verify_flag==FLAG_Y){
                    $sql = "SELECT `user_admin_id`,`user_mobile`,`user_ac_status`,`visitor_verify_flag` FROM `user_admin` WHERE `user_mobile`='$visit_meet_to' AND `user_ac_status`='".FLAG_A."'";
                    $query  = $pdoconn->prepare($sql);
                    $query->execute();
                    $count=$query->rowCount();
                    if($count>0){
                        $val = $query->fetch();
                        $visitor_verify_flag = $val['visitor_verify_flag'];
                        if ($visitor_verify_flag==FLAG_Y) {
                            $visitor_out_verify=FLAG_N;
                            $visitor_in_verify=FLAG_D;
                        }else{
                            $visitor_out_verify=FLAG_Y;
                            $visitor_in_verify=FLAG_Y;
                        }
                    }else{
                        $visitor_out_verify=FLAG_Y;
                        $visitor_in_verify=FLAG_Y;
                    }
                }else{
                    $visitor_out_verify=FLAG_Y;
                    $visitor_in_verify=FLAG_Y;
                }
        }
        if(!empty($visit_img)){
            $PNG_TEMP_DIR ='./../../uploades/'.$prk_admin_name.'/visitor_img/';
            // include "phpqrcode/qrlib.php";
            if (!file_exists($PNG_TEMP_DIR)) {
                $old_mask = umask(0);
                mkdir($PNG_TEMP_DIR, 0777, TRUE);
                umask($old_mask);
            }
            if ($call_type=='M') {
                $decode_file = base64_decode($visit_img);
                $file= uniqid().'.jpg';
                $file_dir=$PNG_TEMP_DIR.$file;
                if(file_put_contents($file_dir, $decode_file)){
                    api_image_upload_compress($file_dir);
                    $vis_img='uploades/'.$prk_admin_name.'/visitor_img/'.$file;
                }else{
                    $vis_img = NULL;
                }
            }else if ($call_type=='W') {
                # code...
                $image_parts = explode(";base64,", $visit_img);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];
              
                $image_base64 = base64_decode($image_parts[1]);
                $fileName = uniqid() . '.png';
                $file = $PNG_TEMP_DIR . $fileName;
                if(file_put_contents($file, $image_base64)){

                    $vis_img='uploades/'.$prk_admin_name.'/visitor_img/'.$fileName;
                }else{
                    # code...
                    $vis_img = '';
                }
            }else{
                # code...
                $vis_img = '';
            }
        }else{
            # code...
            $vis_img = '';
        }
        $prk_visit_dtl_master1=prk_visit_dtl_master($visit_name,$visit_gender,$visit_mobile,$visit_purpose,$visit_address,$visit_with_veh,$visit_card_id,$vis_img,$prk_admin_id,$veh_type,$veh_number,$no_of_guest,$prk_user_username);
        $prk_visit_dtl_master= json_decode($prk_visit_dtl_master1);
        if($prk_visit_dtl_master->status=='1'){
            $vis_img=$prk_visit_dtl_master->visit_img;
            $sql = "INSERT INTO `prk_visit_dtl`(`visit_name`, `visit_gender`, `visit_mobile`, `visit_purpose`, `visit_meet_to`, `visit_address`, `visit_with_veh`, `visit_card_id`, `visit_img`, `prk_admin_id`, `prk_area_user_id`, `start_date`, `veh_type`, `veh_number`, `prk_veh_trc_dtl_id`,`inserted_by`, `inserted_date`,`visitor_meet_to_name`,`visitor_meet_to_address`,`visitor_out_verify_flag`,`visitor_in_verify_flag`,`visitor_token`,`unique_id`,`visitor_type`,`no_of_guest`,`tower_id`,`flat_id`) VALUES ('$visit_name','$visit_gender','$visit_mobile','$visit_purpose','$visit_meet_to','$visit_address','$visit_with_veh','$visit_card_id','$vis_img','$prk_admin_id','$prk_area_user_id','".TIME."','$veh_type','$veh_number','$prk_veh_trc_dtl_id','$prk_user_username','".TIME."','$visitor_meet_to_name','$visitor_meet_to_address','$visitor_out_verify','$visitor_in_verify','$visitor_token',IF('".$unique_id."' IS NULL or '".$unique_id."' = '', NULL , '".$unique_id."'),'$visitor_type','$no_of_guest','$tower_id','$flat_id')";

            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                //$visit_meet_to
                $prk_visit_dtl_id=$pdoconn->lastInsertId();
                if ($visitor_type==FLAG_G) {

                    user_guest_in_complete($prk_visit_dtl_id,$unique_id,$visitor_type);
                }
                $sql="SELECT `user_admin`.`user_admin_id`,`token`.`device_token_id`,`token`.`device_type`,`token`.`token_value` FROM `user_admin`,`token` WHERE `user_mobile`='$visit_meet_to' AND `token`.`active_flag`='".FLAG_Y."' AND `token`.`user_admin_id`=`user_admin`.`user_admin_id`";
                $query  = $pdoconn->prepare($sql);
                $query->execute();
                $count=$query->rowCount();
                if($count>0){
                    $val = $query->fetch();
                    $device_token_id =  $val['device_token_id'];
                    $device_type =  $val['device_type'];
                    if ($device_type=='ANDR') {
                        $payload = array();
                        // $payload['area_name'] = $prk_area_name;
                        // $payload['name'] = $visit_name;
                        $payload['in_date'] = TIME;
                        $payload['url'] = 'VISITOR IN';
                        $payload['prk_visit_dtl_id'] = $prk_visit_dtl_id;
                        $title = 'OPPRSS';
                        $message = $visit_name.' WANTS TO MEET YOU.';
                        $push_type = 'individual';
                        $include_image='';
                        $clickaction='VISITOR IN';
                        $oth1='';
                        $oth2='';
                        $oth3='';
                        $paylo = push_send($title,$message,$push_type,$include_image,$device_token_id,$payload,$clickaction,$oth1,$oth2,$oth3);
                    }
                }
                $response['status'] = 1;
                $response['session'] = 1;
                $response['visit_img'] = $vis_img;
                $response['prk_visit_dtl_id'] = $prk_visit_dtl_id;
                $response['message'] = 'Successful';
            }else{ 
                $response['status'] = 0;
                $response['session'] = 1;
                $response['message'] = 'Not Successful ';
            }
        }else{ 
            $response['status'] = 0;
            $response['session'] = 1;
            $response['message'] = 'Not Successful ';
        }
    }else{ 
        $response['status'] = 0;
        $response['session'] = 1;
        $response['message'] = 'Not Successful ';
    }  
    return json_encode($response); 
}
function user_guest_in_complete($prk_visit_dtl_id,$unique_id,$visitor_type){
    global $pdoconn;
    $sql ="UPDATE `user_guest_list` SET `prk_visit_dtl_id`='$prk_visit_dtl_id',`visitor_in_complete`='".FLAG_Y."',`visitor_in_complete_date`='".TIME."' WHERE `user_guest_list_id`='$unique_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
}
function prk_visit_dtl_master($visit_name,$visit_gender,$visit_mobile,$visit_purpose,$visit_address,$visit_with_veh,$visit_card_id,$visit_img,$prk_admin_id,$veh_type,$veh_number,$no_of_guest,$prk_user_username){
    global $pdoconn;
    $sql = "SELECT `prk_visit_dtl_master_id`,`visit_name`, `visit_gender`,`visit_mobile`,`visit_img`,`visit_address` FROM `prk_visit_dtl_master` WHERE `visit_mobile`='$visit_mobile' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $prk_visit_dtl_master_id=$val['prk_visit_dtl_master_id'];
        $visit_img_old=$val['visit_img'];
        $visit_name_old=$val['visit_name'];
        $visit_gender_old=$val['visit_gender'];
        $visit_address_old=$val['visit_address'];
        #------Visitor Name Update----------#
            if($visit_name_old!=$visit_name){
                $sql = "UPDATE `prk_visit_dtl_master` 
                SET `visit_name`='$visit_name' 
                WHERE `prk_visit_dtl_master_id`='$prk_visit_dtl_master_id' 
                AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
                $query  = $pdoconn->prepare($sql);
                $query->execute();
            }
        #------Visitor Name End------#
        #------Visitor Gender Update----------#
            if($visit_gender_old!=$visit_gender){
                $sql = "UPDATE `prk_visit_dtl_master` 
                SET `visit_gender`='$visit_gender' 
                WHERE `prk_visit_dtl_master_id`='$prk_visit_dtl_master_id' 
                AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
                $query  = $pdoconn->prepare($sql);
                $query->execute();
            }
        #------Visitor Gender End------#
        #------Visitor Address Update----------#
            if($visit_address_old!=$visit_address){
                $sql = "UPDATE `prk_visit_dtl_master` 
                SET `visit_address`='$visit_address' 
                WHERE `prk_visit_dtl_master_id`='$prk_visit_dtl_master_id' 
                AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
                $query  = $pdoconn->prepare($sql);
                $query->execute();
            }
        #------Visitor Address End------#
        if ($visit_img!='') {
            if($visit_img_old=='' || empty($visit_img_old)){
                $sql = "UPDATE `prk_visit_dtl_master` SET `visit_img`='$visit_img' WHERE `prk_visit_dtl_master_id`='$prk_visit_dtl_master_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
                $query  = $pdoconn->prepare($sql);
                $query->execute();
            }
            $response['status'] = 1;
            $response['message'] = 'Successful';
            $response['visit_img'] = $visit_img;
        }else{
            $response['status'] = 1;
            $response['message'] = 'Successful';
            $response['visit_img'] = $visit_img_old;
        }
    }else{
        $prk_visit_dtl_id='';
        // $visit_purpose=($visit_purpose=='Visitor Gate Pass')?'':$visit_purpose;
        $visit_purpose=($visit_purpose=='Visitor Gate Pass')?'':'';
        $sql = "INSERT INTO `prk_visit_dtl_master`(`prk_visit_dtl_id`, `visit_name`, `visit_gender`, `visit_mobile`, `visit_purpose`, `visit_address`, `visit_with_veh`, `visit_card_id`, `visit_img`, `no_of_guest`, `prk_admin_id`, `veh_type`, `veh_number`, `inserted_by`, `inserted_date`) VALUES ('$prk_visit_dtl_id','$visit_name','$visit_gender','$visit_mobile','$visit_purpose','$visit_address','$visit_with_veh','$visit_card_id','$visit_img','$no_of_guest','$prk_admin_id','$veh_type','$veh_number','$prk_user_username','".TIME."')";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['visit_img'] = $visit_img;
    }
    return json_encode($response); 
}
?> 