<?php 
/*
Description: user gate pass list show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
require_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_admin_id','token'))){
 	if(isEmpty(array('user_admin_id','token'))){
	 	$user_admin_id = trim($_POST['user_admin_id']);
	 	$token = ($_POST['token']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $response = user_guest_list($user_admin_id);
        }else{
            $response =$resp;
        }
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo ($response);
?>