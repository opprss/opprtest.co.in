<?php 
require_once 'api/parkAreaReg_api.php';
 $output = '';  
if(isAvailable(array('prk_admin_id','meet_mobile'))){
    if(isEmpty(array('prk_admin_id'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $meet_mobile = trim($_POST['meet_mobile']);

        $meet_to_status = isset($_POST['meet_to_status']) ? trim($_POST['meet_to_status']) : 'M'; 
        $tower_id = isset($_POST['tower_id']) ? trim($_POST['tower_id']) : ''; 
        $flat_id = isset($_POST['flat_id']) ? trim($_POST['flat_id']) : ''; 

        $response = meet_to_user($prk_admin_id,$meet_mobile,$meet_to_status,$tower_id,$flat_id);
    }else{ 
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response); 
}
echo $response;
?>