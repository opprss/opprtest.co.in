<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','parking_admin_name','token','start_date','end_date'))){
    if(isEmpty(array('prk_admin_id','parking_admin_name','token'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $parking_admin_name = trim($_POST['parking_admin_name']);
        $token = trim($_POST['token']);
        $start_date = trim($_POST['start_date']);
        $end_date = trim($_POST['end_date']);
        if(!empty($start_date) && !empty($end_date)) { 
            $format = "Y-m-d";
            if(date($format, strtotime($start_date)) == date($start_date) && date($format, strtotime($end_date)) == date($end_date)) {
                $start_date= $start_date;
                $end_date=  $end_date ;
            } else {
                $start_date = date("Y-m-d", strtotime($start_date));
                $end_date = date("Y-m-d", strtotime($end_date));
            }
        }else{
            DATE;
            $end_date= TIME_TRN;
            $start_date= date("Y-m-d",strtotime("-1 Month"));
        }
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            $response = balance_sheet($prk_admin_id,$start_date,$end_date);
        }else{
            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
 }
echo $response;
?> 