<?php 
/*
Description: duplicate vehicle payment history list 
Developed by: Rakhal Raj Mandal
Created Date: 09-06-2018
Update date :----------
*/ 
function vehicle_audit_check_list($prk_admin_id,$start_date,$end_date){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
   	if(!empty($start_date) && !empty($end_date)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($start_date)) == date($start_date) && date($format, strtotime($end_date)) == date($end_date)) {
            $start_date= $start_date;
            $end_date=  $end_date ;
        } else {
            $start_date = date("Y-m-d", strtotime($start_date));
            $end_date = date("Y-m-d", strtotime($end_date));
        }
    }else{
        $end_date= TIME_TRN;
        $start_date= date("Y-m-d",strtotime("-1 Month"));
    }
	$sql = "SELECT vac.`vehicle_audit_check_id`,
		vac.`prk_admin_id`,
		vac.`prk_gate_pass_id`,
		vac.`user_admin_id`,
		vac.`veh_type`,
		vt.`vehicle_type_dec`,
		vt.`vehicle_typ_img`,
		vac.`veh_number`,
		vac.`allocated_park_space`,
		vac.`park_space`,
		vac.`remarks`,
		DATE_FORMAT(vac.`audit_date`,'%d-%b-%Y') AS `audit_date`,
		DATE_FORMAT(vac.`audit_date`,'%I:%i %p') AS `audit_time`,
        vac.`owner_name`,
        vac.`mobile`,
        vac.`tower_name`,
        vac.`flat_name`,
        vac.`park_veh_for`
		FROM `vehicle_audit_checklist` vac JOIN vehicle_type vt
		WHERE vac.`veh_type` = vt.`vehicle_sort_nm`
		AND vt.`active_flag` = '".FLAG_Y."'
		AND vac.`prk_admin_id`='$prk_admin_id'
		AND vac.`active_flag`='".FLAG_Y."'
		AND vac.`del_flag`='".FLAG_N."'
		AND DATE_FORMAT(vac.`audit_date`,'%Y-%m-%d') BETWEEN '$start_date' AND '$end_date'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
	    $arr_catagory = $query->fetchAll();
	    foreach($arr_catagory as $val){
	        $park['vehicle_audit_check_id'] = $val['vehicle_audit_check_id'];
	        $park['prk_admin_id'] = $val['prk_admin_id'];
	        $park['prk_gate_pass_id'] = $val['prk_gate_pass_id'];
	        $park['user_admin_id'] = $val['user_admin_id'];
	        $park['veh_type'] = $val['veh_type'];
	        $park['vehicle_type_dec'] = $val['vehicle_type_dec'];
	        $park['vehicle_typ_img'] = $val['vehicle_typ_img'];
	        $park['veh_number'] = $val['veh_number'];
	        $park['allocated_park_space'] = $val['allocated_park_space'];
	        $park['park_space'] = $val['park_space'];
	        $park['remarks'] = $val['remarks'];
	        $park['audit_date'] = $val['audit_date'];
	        $park['audit_time'] = $val['audit_time'];
	        $park['owner_name'] = $val['owner_name'];
	        $park['mobile'] = $val['mobile'];
	        $park['tower_name'] = $val['tower_name'];
	        $park['flat_name'] = $val['flat_name'];
	        $park['park_veh_for'] = $val['park_veh_for'];
	    }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        array_push($allplayerdata, $park);
        $response['vehicle_audit_list'] = $allplayerdata;
	}else{
		$response['status'] = 0;
	    $response['message'] = 'List Empty';
	}
    return json_encode($response);
}
?>