<!-- ################################################
  
Description: parking admin can upload a picture(QR code screenshot).
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
include "all_nav/header.php";
$sessionDetails = getSessionDetails();
$parking_admin_name = $sessionDetails['parking_admin_name'];

if (isset($_SESSION['prk_admin_id'])) {

  $user_name=$_SESSION['prk_admin_id'];
  $prk_admin_id = $_SESSION['prk_admin_id'];

  $response = array();

  $response = payment_dtl_img_show($prk_admin_id);
  $response = json_decode($response, true);

  if (!empty($response)) {
    $payment_dtl_img = $response['payment_dtl_img'];
  }
}
?>
<style>
  .size{
    font-size: 11px;
  }
.error_size{
  font-size: 11px;
  color: gray;

}
.success{
  font-size: 11px;
  color: green;
}

/*image*/
.bgColor {
      max-width: 440px;
      height: 400px;
      background-color: #fff4ca;
      padding: 30px;
      border-radius: 4px;
      text-align: center;    
    }
    .upload-preview {border-radius:4px;width: 200px;height: 200px;}
    #body-overlay {background-color: rgba(0, 0, 0, 0.6);z-index: 999;position: absolute;left: 0;top: 0;width: 100%;height: 100%;display: none;}
    #body-overlay div {position:absolute;left:50%;top:50%;margin-top:-32px;margin-left:-32px;}
    #targetOuter{ 
      position:relative;
      text-align: center;
      background-color: #F0E8E0;
      margin: 20px auto;
      width: 200px;
      height: 200px;
      border-radius: 4px;
    }
    .btnSubmit {
      background-color: #565656;
      border-radius: 4px;
      padding: 10px;
      border: #333 1px solid;
      color: #FFFFFF;
      width: 200px;
      cursor:pointer;
    }
    .inputFile{
      margin-top: 0px;
      left: 0px;
      right: 0px;
      top: 0px;
      width: 200px;
      height: 36px;
      background-color: #FFFFFF;
      overflow: hidden;
      opacity: 0;
      position: absolute;
      cursor: pointer;
    }
    .icon-choose-image {
      position: absolute;
      /*opacity: 0.5;*/
      top: 50%;
      left: 50%;
      margin-top: -24px;
      margin-left: -50px;
      width: 100px;
      height: 100px;
      cursor:pointer;
      /*background-color: #fff;*/
      color: #33ADFF;
    }
    .edit-text{
      /*background-color: #fff;*/
      font-size: 1.5em;
      margin-top: -15px;
      color: #33ADFF;
    }
    #profile-upload-option{
      display:none;
      position: absolute;
      top: 163px;
      left: 23px;
      margin-top: -70px;
      margin-left: -24px;
      border: #33ADFF 1px solid;
      border-radius: 4px;
      background-color: #FFFFFF;
      width: 150px;
    }
    .profile-upload-option-list{
      margin: 1px;
      height: 35px;
      /*border-bottom: 1px solid #cecece;*/
      cursor: pointer;
      position: relative;
      padding:5px 0px;
    }
    .profile-upload-option-list:hover{
      background-color: #33ADFF;
    }
  input[type="file"] {
    bottom: 0;
    cursor: pointer;
    height: 100%;
    left: 0;
    margin: 0;
    opacity: 0;
    padding: 0;
    position: absolute;
    width: 100%;
    z-index: 0;
  }
</style>
<script type="text/javascript">
  function showUploadOption(){
      $("#profile-upload-option").css('display','block');
    }

    function hideUploadOption(){
      $("#profile-upload-option").css('display','none');
    }
</script>
<link href="./css/style.css" rel="stylesheet">

<!-- header position -->

    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Add Payment</h5>
        <!--<form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card single pd-20 pd-sm-40 col-md-12">
          <div class="row">
              <div class="col-d-12">
              </div>               
              <div class="col-md-6">
                <h6 class="card-body-title">Upload your wallet QR Code</h6>
                <p class="mg-b-20 mg-sm-b-30">Screenshot your payment gateway QR Code and upload it here.</p>
                <div id="imgContainer" style="padding-bottom: 30px;">
                  <form enctype="multipart/form-data" action="<?php echo PRK_URL ?>payment_img_upload.php" method="post" name="image_upload_form" id="image_upload_form">
                    <div id="imgArea" style="border: 1px solid #33ADFF;">
                      <?php
                        if (!empty($payment_dtl_img)) {
                      ?>
                        <img src="<?php echo PRK_BASE_URL.$payment_dtl_img;?>" style="height: 146px; width: 146px">  

                      <?php
                        }else{
                      ?>
                        <i class="fa fa-qrcode fa-5x"></i>
                      <?php
                        }
                      ?>
                      <div class="progressBar">
                        <div class="bar"></div>
                        <div class="percent">0%</div>
                      </div>
                      <div class="icon-choose-image" onClick="showUploadOption()">
                        <i class="ion-ios-camera-outline tx-50"></i>
                        <p class="edit-text">Edit photo</p>
                      </div>
                      <div id="profile-upload-option">
                        <div class="profile-upload-option-list">
                          <input type="file" accept="image/*" name="image_upload_file" id="image_upload_file"></input><span>Upload</span>
                        </div>
                        <?php
                          if (!empty($payment_dtl_img)) {
                        ?>
                        <div class="profile-upload-option-list" onClick="removeProfilePhoto();">Remove</div>
                        <?php
                          }
                        ?>
                        <hr style="padding: 0px; margin: 0px;">
                        <div class="profile-upload-option-list" onClick="hideUploadOption();">Cancel</div>
                      </div>

                      <input type="hidden" name="parking_admin_name" value="<?php echo $parking_admin_name; ?>">
                      <input type="hidden" name="user_name" value="<?php echo $user_name; ?>">
                      <input type="hidden" name="token" value="<?php echo $token; ?>">
                    </div>
                  </form>
                </div>
              </div>
              <div class="col-md-6 text-center">
                <?php
                  if (!empty($payment_dtl_img)) {
                ?>
                  <img src="<?php echo PRK_BASE_URL.$payment_dtl_img;?>" style="height: 420px; border:1px solid #33adff;">
                <?php
                  }
                ?>
                
              </div>
          </div>
        </div><!-- card -->
        <!-- /add employee form -->
      </div><!-- am-pagebody -->
      

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
<!-- <script src="./js/jquery.min.js"></script> -->
<script src="./js/jquery.form.js"></script>
      <!-- footer part -->
<script type="text/javascript">
  $(document).on('change', '#image_upload_file', function () {
      hideUploadOption();
        var progressBar = $('.progressBar'), bar = $('.progressBar .bar'), percent = $('.progressBar .percent');

        $('#image_upload_form').ajaxForm({
            beforeSend: function() {
            progressBar.fadeIn();
                var percentVal = '0%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            success: function(html, statusText, xhr, $form) {
            obj = $.parseJSON(html);
            var path = 'uploades/medium/'+obj.image_medium;
            if(obj.status){  
              var percentVal = '100%';
              bar.width(percentVal);
              percent.html(percentVal);
              $("#imgArea>img").prop('src',obj.filepath);  
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.9em;'>Payment QR code uploaded successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                      location.reload()
                  }
                }
              }); 
            }else{
              if( typeof obj.session !== 'undefined'){
                if (!obj.session) {
                  window.location.replace("logout.php");
                }
              }else{
                $.alert({
                  icon: 'fa fa-frown-o',
                  theme: 'modern',
                  title: 'Error !',
                  content: "<p style='font-size:0.9em;'>"+obj.error+"</p>",
                  type: 'red'
                });
              }
            }
          },
          complete: function(xhr) {
            progressBar.fadeOut();      
          } 
        }).submit();    

    });
      var prkUrl = "<?php echo PRK_URL; ?>";

        /* end velidation*/
      //remove DP
          function removeProfilePhoto(){
            hideUploadOption();
            var prk_admin_id = "<?php echo $prk_admin_id;?>";
            var urlRemove = prkUrl+'payment_img_remove.php';
            $.ajax({
              url :urlRemove,
              type:'POST',
              data :
              {
                'prk_admin_id':prk_admin_id,
                'token': '<?php echo $token;?>'
              },
              dataType:'html',
              success  :function(data)
              {
                
                var json = $.parseJSON(data);
                if (json.status){
                      $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success !',
                      content: "<p style='font-size:0.9em;'>Profile image removed</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                            location.reload();
                        }
                      }
                    });
                  }else{
                    if( typeof json.session !== 'undefined'){
                      if (!json.session) {
                        window.location.replace("logout.php");
                      }
                    }else{
                      $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                    }
                  }
              }
                });
           }

    </script>