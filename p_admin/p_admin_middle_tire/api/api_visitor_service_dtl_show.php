<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function visitor_service_dtl_show($prk_admin_id,$visitor_gate_pass_dtl_id){
    $allplayerdata = array();
    $response = array();
    $vehicle = array();
    global $pdoconn;
    $sql = "SELECT `visitor_service_dtl_id`,`visitor_gate_pass_dtl_id`,`tower_name`,`falt_name`,`in_time`,`out_time`,`visitor_remark` FROM `visitor_service_dtl` WHERE `prk_admin_id`='$prk_admin_id' and `visitor_gate_pass_dtl_id`='$visitor_gate_pass_dtl_id' AND `active_flag`='".FLAG_Y."' and `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute(); $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val) 
        {
            $vehicle['visitor_service_dtl_id'] = $val['visitor_service_dtl_id'];
            $vehicle['visitor_gate_pass_dtl_id'] = $val['visitor_gate_pass_dtl_id'];
            $vehicle['tower_name'] = $val['tower_name'];
            $vehicle['falt_name'] = $val['falt_name'];
            $vehicle['in_time'] = $val['in_time'];
            $vehicle['out_time'] = $val['out_time'];
            $vehicle['visitor_remark'] = $val['visitor_remark'];
            
            array_push($allplayerdata, $vehicle);
        }
        $response['status'] = 1;
        $response['message'] = 'Successfully';
        $response['state'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>