<?php 
/*
Description: vehicle out trc table data update.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function prk_veh_trc_dtl_update($prk_veh_trc_dtl_id,$prk_out_rep_name,$total_pay,$payment_type,$Out_time){
    global $pdoconn;
    $response = array();
    $sql ="UPDATE `prk_veh_trc_dtl` SET  `updated_date`='".TIME."',`updated_by`='$prk_out_rep_name',`total_pay`='$total_pay',`prk_veh_out_time`='$Out_time',`prk_out_rep_name`='$prk_out_rep_name',`payment_rec_emp_name` ='$prk_out_rep_name',`payment_status`='".FLAG_T."',`transition_date`='".TIME_TRN."',`payment_type`='$payment_type' WHERE `prk_veh_trc_dtl_id`='$prk_veh_trc_dtl_id'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Update sucessful';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Update Not Save';
        return json_encode($response);
    }  
}
?>