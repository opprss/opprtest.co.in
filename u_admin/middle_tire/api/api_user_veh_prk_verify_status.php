<?php
/*
Description: user vehicle verify parking employ.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_veh_prk_verify_status($user_prk_veh_varify_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT `user_prk_veh_verify_status`,`prk_veh_trc_dtl_id`,`user_prk_remark` FROM `user_prk_veh_verify` WHERE `user_prk_veh_varify_id` = '$user_prk_veh_varify_id' AND active_flag='".FLAG_Y."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $response['status'] = 1;
    $response['message'] = 'Successful';
    $response['user_prk_veh_verify_status'] = $val['user_prk_veh_verify_status'];
    $response['prk_veh_trc_dtl_id'] = $val['prk_veh_trc_dtl_id'];
    $response['user_prk_remark'] = $val['user_prk_remark'];
    return json_encode($response);
}
?>