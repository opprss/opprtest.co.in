<?php
/*
Description: parking area email verified.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_email_verify($prk_admin_id, $prk_area_email){
    global $pdoconn;
    $response = array();
    if(emailChecker($prk_area_email)=='true'){
        $sql ="UPDATE `prk_area_dtl` SET `prk_email_verify`='".FLAG_T."' WHERE `prk_admin_id`='$prk_admin_id' AND `prk_area_email`='$prk_area_email' AND `active_flag`='".FLAG_Y."'";
        $query = $pdoconn->prepare($sql);
        if($query->execute()){
            $response['status'] = 1;
            $response['message'] = 'Email Verified';
        }else{
            $response['status'] = 0;
            $response['message'] = 'Update Not Save';
        }
    }else{
        $response = emailChecker($prk_area_email);
    }
    return json_encode($response);
}
?>