<?php
/*
Description: OTP send. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function otp_send($usernam,$email,$mobile_n){
    global $pdoconn;
    $result=array();
    if($mobile_n!='' || $email!=''){ 
        $shuffled=mt_rand(100000,999999);
        //$text=$shuffled." It's The OTP for your transaction, it's valid for 20 minutes. Thanks www.opprss.com";
        $text="Dear User, ".$shuffled." is your one time password(OTP).Valid for 20 minutes. Please do not share OTP. For more details please visit www.opprss.com.";
        mobile_massage($mobile_n,$text);
            $email_logo = EMAIL_LOGO."oppr-logo.png";
          $email_logo_fb=EMAIL_LOGO_fb."fb.jpg";
          $email_logo_gmail=EMAIL_LOGO_gmail."google.png";
          $email_logo_indeed=EMAIL_LOGO_indeed."inidied.png";
        $message = '
            <html>
              <body>
                  <div style="border: 1px solid #00b8d4; width: 600px;">
                    <div style="background-color: #00b8d4; height: 80px; width: 600px;">
                      <span style="float: left;">
                     <img src="'.$email_logo.'" height="50" width="100">
                      </span>
                      <br><div style="margin-top: 5px">
                        <a href="#" style="margin-left: 350px;width: 20px;height: 20px;">
                          
                            <img src="'.$email_logo_fb.'" alt="FaceBook" height="30" width="30" / >
                        </a>

                       <span class="google">
                        <a href="#" >

                          <img src="'.$email_logo_gmail.'" alt="Gmail" height="30" width="30" />
                        </a>
                      </span>
                       <span class="linkedin">
                        <a href="#">
                         <img src="'.$email_logo_indeed.'" height="30" width="30" alt="Indeed" />
                        </a>
                      </span>
                    </div>

                      <div style="margin-top: 80px; margin-left: 20px;">
                        <span>
                          <b>Dear Sir/Madam,</b>
                        </span>
                      </div>
                      <div style="margin-top: 20px; margin-left: 30px;width: 555px">
                       <p>Your One Time Password (OTP) is '.$shuffled.' echo  to reset your Password. <br>
                      This OTP is valid for 20 minutes or 1 successful attempt whichever is earlier.<br>
                       Please do not share this One Time Password with anyone.</p>
                       <p>In case you need any further clarification for the same, please do get in touch immediately with care@opprss.com or your nearest branch. </p>

                       

                       <p> This E-Notification was automatically generated. Please do not reply to this mail. For any suggestions / feedback, please click on Feedback link provided on<a href="http://opprss.com"> www.opprss.com</a></p>
                      </div>
                      <div style="margin-top: 10px; margin-left: 20px;">

                          <b>Regards,</b><br>
                          <p>OPPR Team.<br>
                          A Smart Parking & Security Solution.
                        </p><div style="width: 555px">
                           &emsp;"The information contained in this electronic message and any attachments to this message are intended for exclusive use of the addressee(s) and may contain confidential or privileged information. If you are not the intended recipient, please notify the sender at OPPR Software Solution Pvt Ltd or care@opprss.com immediately and
                        destroy all copies of this message and any attachments. The views expressed in this E-mail message / Attachments, are those of the individual sender."
                        </div>
                      </div>

                      </div>
                      <div style="margin-top: 520px; text-align: center;">
                      <span><a href="http://opprss.com/about-us">About Us</a> |</span> <span><a href="http://opprss.com/contact-us">Contect Us </a></span>
                      </div>
                      <div style="text-align: center; margin-top: 20px;padding-bottom: 20px; font-size: 11px"> &copy; 2017 BY OPPR SOFTWARE SOLUTION. ALL RIGHTS RESERVED.<br><br>
                      </div>
                    </div>
                  </div>
              </body>
            </html>
            ';
        email_massage($email,$message); 

        
        $sql = "SELECT `otp_id` FROM `otp_details` WHERE (`mobile_no`='$mobile_n' OR `admin_email`='$email') AND `active_flag`='".ACTIVE_FLAG_Y."'";
        $query = $pdoconn->prepare($sql);
        $query->execute();
        $count=$query->rowCount();
        if($count >0){
            $row = $query->fetchAll();
            foreach($row as $rows){
                $otp_id = $rows['otp_id'];
                $sql ="UPDATE `otp_details` SET `active_flag` = '".ACTIVE_FLAG_N."' WHERE `otp_id` = '$otp_id'";
                $query = $pdoconn->prepare($sql);
                $query->execute(); 
            }
             $sql="INSERT INTO `otp_details`(`otp`,`active_flag`,`admin_id`,`admin_email`,`mobile_no`,`otp_start_time`) VALUES('$shuffled','".ACTIVE_FLAG_Y."','$usernam','$email','$mobile_n','".TIME."')";
            $query = $pdoconn->prepare($sql);
            $query->execute();
            $json_report['status']=1;
            $json_report['message']='Successfully';
            return json_encode($json_report);
        }else{
            $sql="INSERT INTO `otp_details`(`otp`,`active_flag`,`admin_id`,`admin_email`,`mobile_no`,`otp_start_time`) VALUES('$shuffled','".ACTIVE_FLAG_Y."','$usernam','$email','$mobile_n','".TIME."')";
            $query = $pdoconn->prepare($sql);
            $query->execute();
            $json_report['status']=1;
            $json_report['message']='Successfully';
            return json_encode($json_report);
        }

    }else{
        return false;
    }
}
?>