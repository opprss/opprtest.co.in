<?php
/*
Description: new user vehicle parking parking area 
Developed by: Rakhal Raj Mandal
Created Date: 10-04-2018
Update date : 17-05-2018,25-05-2018,
*/
require_once 'api/reg_api.php';
// include_once 'api/global_api.php';
$response = array();
$re = array();
if(isAvailable(array('user_mobile','user_veh_type','user_veh_number','prk_admin_id','prk_in_rep_name','payment_type','gate_pass_num','prk_area_user_id','token'))){
    if(isEmpty(array('user_veh_number','user_veh_type','prk_admin_id','prk_in_rep_name','payment_type','gate_pass_num','prk_area_user_id','token'))){
        
        $user_mobile = trim($_POST['user_mobile']);
        $user_veh_type = trim($_POST['user_veh_type']);
        $user_veh_number = trim($_POST['user_veh_number']);
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $prk_in_rep_name = trim($_POST['prk_in_rep_name']);
        $payment_type = trim($_POST['payment_type']);
        $gate_pass_num = trim($_POST['gate_pass_num']);

        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $token = trim($_POST['token']);
        global $pdoconn;
        $sql = "SELECT `token_value` FROM `prk_token` WHERE `prk_area_user_id`='$prk_area_user_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".ACTIVE_FLAG_Y."' AND `token_value`='$token'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $count=$query->rowCount();
        if($count>0){
            $prk_wh_co= prking_wheeler_count($prk_admin_id,$user_veh_type,$user_veh_number); 
            $json = json_decode($prk_wh_co);
            if($json->status=='1'){
                $wheeler_co=wheeler_count($prk_admin_id,$user_veh_type);
                $json = json_decode($wheeler_co);
                $total_hwe=$json->total_hwe;
                $tot_prk_space=$json->tot_prk_space;
                if($tot_prk_space>$total_hwe){
                    $v_nu =v_numberChecker($user_veh_number);
                    $json = json_decode($v_nu);
                    if($json->status=='1'){
                        if (!empty($user_mobile)) {
                            $draft_re = draft_registration($user_mobile,$user_veh_type,$user_veh_number);
                            $json = json_decode($draft_re);
                            $status=$json->status;
                            if($status=='1'){ 
                                $user_admin_id=$json->user_admin_id;
                                $user_ac_status=$json->user_ac_status;

                                $password=$json->password;
                                $prk_v_t_d= prk_veh_trc_dtl($prk_admin_id,$user_admin_id,$user_veh_type,$user_veh_number,$prk_in_rep_name,$prk_in_rep_name,$payment_type,$gate_pass_num);
                                $json = json_decode($prk_v_t_d);
                                $status=$json->status;
                                if($status=='1')
                                {
                                   $prk_veh_trc_dtl_id=$json->prk_veh_trc_dtl_id;
                                    $sql="SELECT `prk_area_dtl`.`prk_area_name`,
                                   `prk_veh_trc_dtl`.`prk_veh_type`,
                                   `prk_veh_trc_dtl`.`veh_number`,
                                   DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%D-%M-%Y') AS `veh_in_date`,
                                   DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%I:%i %p') AS `veh_in_time`
                                    FROM `prk_veh_trc_dtl`, `prk_area_dtl`
                                    WHERE `prk_veh_trc_dtl`.`prk_admin_id` = `prk_area_dtl`.`prk_admin_id`
                                    AND `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id`='$prk_veh_trc_dtl_id'";

                                    $query  = $pdoconn->prepare($sql);
                                    $query->execute();
                                    $val = $query->fetch();
                                    $veh_in_date = $val['veh_in_date'];
                                    $veh_in_time = $val['veh_in_time'];
                                    $veh_number = $val['veh_number'];
                                    $prk_veh_type = $val['prk_veh_type'];
                                    $prk_area_name = $val['prk_area_name'];

                                    if($user_ac_status=='D'){
                                        draft_registration_sms($password,$user_mobile);
                                        /*$response =prk_veh_trn_in_mess($user_mobile,$veh_in_date,$veh_in_time,$prk_veh_type,$veh_number,$prk_area_name);
                                        $response = $response;*/
                                    }
                                    #---------------#
                                    $prk_i = prk_inf($prk_admin_id);
                                    $json = json_decode($prk_i);
                                    if($json->sms_flag== SMS_FLAG_Y){
                                        prk_veh_trn_in_mess($user_mobile,$veh_in_date,$veh_in_time,$prk_veh_type,$veh_number,$prk_area_name);
                                        // $re['Raj'] = 0; 
                                        // $re['mandal'] = 'Transition Failed';
                                        // echo json_encode($re);
                                    }
                                    $response['status'] = 1;
                                    $response['message'] = 'Vehicle IN Successful';
                                    $response = json_encode($response);               
                                        
                                }else{
                                    $response = $prk_v_t_d;
                                }  
                            }else{
                                $response = $draft_re;
                            }
                        }else{
                            $user_admin_id=1;
                            $resp= prk_veh_trc_dtl($prk_admin_id,$user_admin_id,$user_veh_type,$user_veh_number,$prk_in_rep_name,$prk_in_rep_name,$payment_type,$gate_pass_num);
                            $json = json_decode($resp);
                            $status=$json->status;
                            if($status=='1')
                            {
                                $response['status'] = 1;
                                $response['message'] = 'Sucessfull';
                                $response = json_encode($response);
                            }else{
                                $response['status'] = 0;
                                $response['message'] = 'Not Sucessfull';
                                $response = json_encode($response);
                            }
                        }
                    }else{
                        $response = $v_nu;
                    }
                }else{
                    $respons['status'] = 0;
                    $respons['message'] = 'Parking Area Full';
                    $response = json_encode($respons);
                }
            }else{
                $response = $prk_wh_co;
            }
        }else{ 
            $response['status'] = 0; 
            $response['session'] = 0;
            $response['message'] = 'Session Expired Please Login Again';
            $response = json_encode($response);
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>