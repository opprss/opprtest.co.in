<?php 
/*
Description: Parking area vehicle space show.
Developed by: Rakhal Raj Mandal
Created Date: 14-06-2018
Update date : ----------
*/
require_once 'api/reg_api.php';
// require_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','prk_sub_unit_id'))){
 	if(isEmpty(array('prk_admin_id'))){

	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_sub_unit_id = trim($_POST['prk_sub_unit_id']);
        $response = prk_area_sub_unit_space_count($prk_admin_id,$prk_sub_unit_id);
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo ($response);
?>