<?php
/*
Description: OTP Validate. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function visitor_visit_in_verify($prk_visit_dtl_id,$user_name,$verify_status){
    $response = array();
    global $pdoconn;

    $sql = "UPDATE `prk_visit_dtl` SET `visitor_in_verify_date`='".TIME."',`visitor_in_verify_flag`='$verify_status',`visitor_in_verify_name`='$user_name' WHERE `prk_visit_dtl_id`='$prk_visit_dtl_id'";
    $query  = $pdoconn->prepare($sql);
    if($query->execute()){
        if ($verify_status=='Y') {
            $response['status']=1;
            $response['message'] = 'Visitor is Verified for IN';
            $response = json_encode($response);
        }else{
            $response['status']=1;
            $response['message'] = 'Visitor Rejeceted';
            $response = json_encode($response);
        }
        $sql = "SELECT pt.`token_value`,
            pt.`device_token_id`,
            pt.`device_type`,
            pvd.`visit_name`
            FROM `prk_token` pt JOIN `prk_visit_dtl` pvd
            WHERE pt.`prk_admin_id`=pvd.`prk_admin_id`
            AND pt.`prk_area_user_id`=pvd.`prk_area_user_id`
            AND pt.`active_flag`='".FLAG_Y."'
            AND pvd.`prk_visit_dtl_id`='$prk_visit_dtl_id'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $count=$query->rowCount();
        if($count>0){
            $val = $query->fetch();
            $device_token_id =  $val['device_token_id'];
            $device_type =  $val['device_type'];
            $visit_name =  $val['visit_name'];
            if ($device_type=='ANDR') {
                $payload = array();
                // $payload['area_name'] = $prk_area_name;
                // $payload['name'] = $user_family_name;
                $verify_status_show=($verify_status=='Y')?'VERIFIED':'REJECETED';
                $payload['in_date'] = TIME;
                $payload['url'] = 'VISITOR';
                $title = 'OPPRSS';
                $message = 'IN REQUEST OF '.$visit_name.' '.$verify_status_show.'.';
                $push_type = 'individual';
                $include_image='VISITOR';
                $clickaction='';
                $oth1='EMP';
                $oth2='';
                $oth3='';
                $paylo = push_send($title,$message,$push_type,$include_image,$device_token_id,$payload,$clickaction,$oth1,$oth2,$oth3);
            }
        }
    }else{
        $response['status']=0;
        $response['message'] = 'Visitor is not Verified for IN';
        $response = json_encode($response);
    }   
    return $response;         
}
?>