<?php
/*
Description: Parking area login.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
if(isAvailable(array('prk_admin_id','bill_generate_date','inserted_by'))){
    if(isEmpty(array('prk_admin_id','bill_generate_date','inserted_by'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $bill_generate_date = trim($_POST['bill_generate_date']);
        $inserted_by = trim($_POST['inserted_by']);
        $response = cus_bill_generate($prk_admin_id,$bill_generate_date,$inserted_by);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandator';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>