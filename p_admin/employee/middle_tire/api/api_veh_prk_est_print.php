<?php
/*
Description: vehicle estimation prnt.
Developed by: Rakhal Raj Mandal
Created Date: 11-06-2018
Update date : ----------
*/ 
function veh_prk_est_print($prk_veh_prk_est_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    
        $sql = "SELECT lpad(prk_veh_prk_est.prk_veh_prk_est_id,10,0) as prk_veh_prk_est_id,
            prk_veh_prk_est.veh_number,
            prk_veh_prk_est.veh_type,
            vehicle_type.vehicle_type_dec,
            prk_veh_prk_est.veh_rate_hour,
            prk_veh_prk_est.veh_rate,
            prk_veh_prk_est.no_of_time_multiple,
            (prk_veh_prk_est.veh_rate_hour * prk_veh_prk_est.no_of_time_multiple) as total_hour,
            prk_veh_prk_est.total_pay,
            0 as advance_pay,
            (ifnull(prk_veh_prk_est.total_pay , 0) - ifnull(0,0)) as net_pay,
            'NA' as payment_type,
            DATE_FORMAT(prk_veh_prk_est.issue_date,'%D-%M-%Y %I:%i %p') AS issue_date,
            prk_veh_prk_est.rate_validity,
            DATE_FORMAT('".TIME."','%d-%m-%Y %H:%i') AS `current_date`
            FROM prk_veh_prk_est,vehicle_type
            WHERE prk_veh_prk_est.veh_type = vehicle_type.vehicle_sort_nm
            AND prk_veh_prk_est.del_flag ='".FLAG_N."'
            AND vehicle_type.active_flag = '".FLAG_Y."'
            AND prk_veh_prk_est.prk_veh_prk_est_id = '$prk_veh_prk_est_id'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
    	$val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['barcode_valu'] = $val['prk_veh_prk_est_id'];
        $response['veh_number'] = $val['veh_number'];
        $response['veh_type'] = $val['veh_type'];
        $response['vehicle_type_dec'] = $val['vehicle_type_dec'];
        $response['veh_rate_hour'] = $val['veh_rate_hour'];
        $response['veh_rate'] = $val['veh_rate'];
        $response['no_of_time_multiple'] = $val['no_of_time_multiple'];
        $response['total_hour'] = $val['total_hour'];
        $response['total_pay'] = $val['total_pay'];
        $response['advance_pay'] = $val['advance_pay'];
        $response['net_pay'] = $val['net_pay'];
        $response['payment_type'] = $val['payment_type'];
        $response['current_date'] = $val['current_date'];
    return json_encode($response); 
}
?>