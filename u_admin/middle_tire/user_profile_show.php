<?php 
/*
Description: user profile show show.
Developed by: Rakhal Raj Mandal
Created Date: 30-03-2018
Update date :08-05-2018
*/
// require_once '../../connection-pdo.php';
require_once 'api/reg_api.php';
// require_once 'api/global_api.php';
 
$response = array();
 
if(isAvailable(array('user_admin_id','token'))){

 	if(isEmpty(array('user_admin_id','token'))){

	 	$user_admin_id = trim($_POST['user_admin_id']);

	 	$token = ($_POST['token']);
		$resp=user_token_check($user_admin_id,$token);
		$json = json_decode($resp);
		if($json->status){
			$response= user_profile_show($user_admin_id);
		}else{
			$response = $resp;
		}
 	}else{
 			$response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response); 
	 }
 }else{
	 $response['status'] = 0; 
	 $response['message'] = 'Invalid API Call';
     $response = json_encode($response); 
 } 
 echo $response;
?>