<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','owner_name','owner_mobile','owner_email','owner_gender','parking_admin_name','effective_date','end_date','owner_id','token','crud_type'))){
    if(isEmpty(array('prk_admin_id','parking_admin_name','token','crud_type'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
        $tower_id = '';
        $flat_id = '';
        $owner_name = trim($_POST['owner_name']);
        $owner_mobile = trim($_POST['owner_mobile']);
        $owner_email = trim($_POST['owner_email']);
        $owner_gender = trim($_POST['owner_gender']);

        $parking_admin_name = trim($_POST['parking_admin_name']);
        $effective_date = trim($_POST['effective_date']);
        $end_date = trim($_POST['end_date']);
        $owner_id = trim($_POST['owner_id']);
        $crud_type = trim($_POST['crud_type']);
        $token = trim($_POST['token']);
        
        $resp=prk_token_check($prk_admin_id,$token);
            $json = json_decode($resp);
            if($json->status){
                $response = owner_details($prk_admin_id,$tower_id,$flat_id,$owner_name,$owner_mobile,$owner_email,$owner_gender,$owner_id,$effective_date,$end_date,$parking_admin_name,$crud_type);
            }else{
                $response = $resp;
            }
    }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
 }
echo $response;
?>