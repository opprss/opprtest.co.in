<?php 
/*
Description: user gate pass list show.
Developed by: Rakhal Raj Mandal
Created Date: 30-03-2018
Update date :17-05-2018
*/
function user_gete_pass_dtl_list($user_admin_id){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
	$sql="SELECT `prk_gate_pass_dtl`.`prk_gate_pass_id`,
	    `prk_gate_pass_dtl`.`veh_type`,
	    `prk_gate_pass_dtl`.`veh_number`,
	    `prk_gate_pass_dtl`.`sub_unit_name`,
	    `prk_gate_pass_dtl`.`veh_owner_name`,
	    `prk_gate_pass_dtl`.`mobile`,
	    `prk_gate_pass_dtl`.`effective_date`,
	    `prk_gate_pass_dtl`.`end_date`,
	    `prk_gate_pass_dtl`.`prk_gate_pass_num`,
	    `vehicle_type`.`vehicle_typ_img` 
	    FROM `prk_gate_pass_dtl`,`vehicle_type` 
	    WHERE `prk_gate_pass_dtl`.`user_admin_id`='$user_admin_id'
	    AND `prk_gate_pass_dtl`.`active_flag`='".FLAG_Y."' 
	    AND `prk_gate_pass_dtl`.`del_flag`='".FLAG_N."'
	    AND `vehicle_type`.`active_flag`='".FLAG_Y."'
	    AND '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(`prk_gate_pass_dtl`.`effective_date`, '%d-%m-%Y'), '%Y-%m-%d') 
	    AND DATE_FORMAT(STR_TO_DATE(IFNULL(`prk_gate_pass_dtl`.`end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')
	    AND `vehicle_type`.`vehicle_sort_nm`=`prk_gate_pass_dtl`.`veh_type`
	    ORDER BY  `prk_gate_pass_dtl`.`prk_gate_pass_id` DESC";


    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
	    $arr_catagory = $query->fetchAll();
	    foreach($arr_catagory as $val)
	    {
	        $response['status'] = 1;
	        $response['message'] = 'Successful';
	        $park['prk_gate_pass_id'] = $val['prk_gate_pass_id'];
	        $park['veh_type'] = $val['veh_type'];
	        $park['veh_number'] = $val['veh_number'];
	        $park['sub_unit_name'] = $val['sub_unit_name'];
	        $park['veh_owner_name'] = $val['veh_owner_name'];
	        $park['mobile'] = $val['mobile'];
	        $park['effective_date'] = $val['effective_date'];
	        $park['end_date'] = $val['end_date'];
	        $park['prk_gate_pass_num'] = $val['prk_gate_pass_num'];
	        $park['vehicle_typ_img'] = $val['vehicle_typ_img'];
	        array_push($allplayerdata, $park);
	        $response['gate_pass'] = $allplayerdata;
	    }
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>