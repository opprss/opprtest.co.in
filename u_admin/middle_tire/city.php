<?php 
/*
Description: all city show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
    require_once 'api/reg_api.php';

    $respon = array();
    if(isAvailable(array('sta_id'))){
        if(isEmpty(array('sta_id'))){
            $sta_id = trim($_POST['sta_id']);
            $respon=city($sta_id);
            echo $respon;
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response); 
        }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        echo json_encode($response); 
    } 
?>