<?php 
/*
Description: parking employ payment resived.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/  
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('payment_dtl_id','prk_out_rep_name','payment_type'))){
 	if(isEmpty(array('payment_dtl_id','prk_out_rep_name','payment_type'))){

        $payment_dtl_id = trim($_POST['payment_dtl_id']);
        $prk_out_rep_name = trim($_POST['prk_out_rep_name']);
        $payment_type = trim($_POST['payment_type']);

        
        $respon= payment_receive($payment_dtl_id,$prk_out_rep_name,$payment_type);
        echo ($respon);
        
 	}else{
            $response['status'] = 0;
	    $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response);
}
?>