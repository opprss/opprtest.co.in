<?php 
/*
Description: parking area all gate dtl list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
$response = array();
$resp= array();
if(isAvailable(array('category_type'))){
 	if(isEmpty(array('category_type'))){
        // $category_type = 'AND_VER_CHE';
        $category_type = trim($_POST['category_type']);
        $respon=common_drop_down_list($category_type);
    	echo ($respon);
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        echo json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response);
}
?> 