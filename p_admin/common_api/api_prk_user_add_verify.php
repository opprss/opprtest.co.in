<?php
/*
Description: Parking area vehicle rate update.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_user_add_verify($prk_admin_id,$user_name,$user_add_id){
    global $pdoconn;
    $response = array();
    $sql = "UPDATE `user_address` SET `user_add_verify_flag`='".FLAG_Y."',`user_add_verify_by`='$user_name',`user_add_verify_date`='".TIME."' WHERE `user_add_id`='$user_add_id'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        /*$sql = "SELECT `user_address`.`user_add_id`,
            `user_address`.`user_admin_id`,
            `user_address`.`prk_admin_id`,
            `prk_area_dtl`.`prk_area_name`,
            `tower_details`.`tower_name`,
            `flat_details`.`flat_name`
            FROM `user_address`,`tower_details`,`flat_details`,`prk_area_dtl`
            WHERE `user_address`.`user_add_id`='$user_add_id' 
            AND `user_address`.`tower_name`=`tower_details`.`tower_id`
            AND `tower_details`.`active_flag`='".FLAG_Y."'
            AND `tower_details`.`del_flag`='".FLAG_N."'
            AND`user_address`.`falt_name`=`flat_details`.`flat_id`
            AND `flat_details`.`active_flag`='".FLAG_Y."'
            AND `flat_details`.`del_flag`='".FLAG_N."'
            AND `prk_area_dtl`.`prk_admin_id`=`user_address`.`prk_admin_id`
            AND `prk_area_dtl`.`active_flag`='".FLAG_Y."'";*/
        $sql="SELECT `user_address`.`user_add_id`,
            `user_address`.`user_admin_id`,
            `user_address`.`prk_admin_id`,
            `prk_area_dtl`.`prk_area_name`,
            `tower_details`.`tower_id`,
            `tower_details`.`tower_name`,
            `flat_details`.`flat_id`,
            `flat_details`.`flat_name`,
            `user_admin`.`user_mobile`,
            `user_detail`.`user_name`,
            `user_detail`.`user_gender`,
            `user_detail`.`user_email`
            FROM `user_address`,`tower_details`,`flat_details`,`prk_area_dtl`,`user_admin`,`user_detail`
            WHERE `user_detail`.`user_admin_id`=`user_address`.`user_admin_id`
            AND `user_detail`.`active_flag`='".FLAG_Y."'
            AND `user_admin`.`user_admin_id`=`user_address`.`user_admin_id`
            AND `user_address`.`tower_name`=`tower_details`.`tower_id`
            AND `tower_details`.`active_flag`='".FLAG_Y."'
            AND `tower_details`.`del_flag`='".FLAG_N."'
            AND`user_address`.`falt_name`=`flat_details`.`flat_id`
            AND `flat_details`.`active_flag`='".FLAG_Y."'
            AND `flat_details`.`del_flag`='".FLAG_N."'
            AND `prk_area_dtl`.`prk_admin_id`=`user_address`.`prk_admin_id`
            AND `prk_area_dtl`.`active_flag`='".FLAG_Y."'
            AND `user_address`.`user_add_id`='$user_add_id'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $count=$query->rowCount();
        $con_flat_mas='NO';
        if($count>0){
            $val = $query->fetch();
            $user_admin_id = $val['user_admin_id'];
            $prk_area_name = $val['prk_area_name'];
            $tower_id = $val['tower_id'];
            $tower_name = $val['tower_name'];
            $flat_id = $val['flat_id'];
            $flat_name = $val['flat_name'];
            $user_mobile = $val['user_mobile'];
            $user_name2 = $val['user_name'];
            $user_gender = $val['user_gender'];
            $user_email = $val['user_email'];
            $oth1=$oth2=$oth3=$oth4=$oth5='';
            $con_flat_mas=connect_flat_master($prk_admin_id,$user_name2,$user_mobile,$user_email,$user_gender,$tower_id,$flat_id,$user_name,$oth1,$oth2,$oth3,$oth4,$oth5);
            $json = json_decode($con_flat_mas);
            $con_flat_mas=$json->message;
            #-------------------#
            $sql="SELECT `token`.`device_token_id`,`token`.`device_type`,`token`.`token_value` FROM `token` WHERE `token`.`active_flag`='".FLAG_Y."' AND `token`.`user_admin_id`='$user_admin_id'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $device_token_id =  $val['device_token_id'];
                $device_type =  $val['device_type'];
                if ($device_type=='ANDR') {
                    $payload = array();
                    // $payload['area_name'] = $prk_area_name;
                    // $payload['name'] = $veh_number;
                    $payload['in_date'] = TIME;
                    $payload['url'] = 'ADDRESS';
                    $title = 'OPPRSS';
                    $message = $tower_name.', '.$flat_name.' IS VERIFIED AS YOUR ADDRESS BY '.$prk_area_name.'.';
                    $push_type = 'individual';
                    $include_image='';
                    $clickaction='ADDRESS';
                    $oth1='';
                    $oth2='';
                    $oth3='';
                    $paylo = push_send($title,$message,$push_type,$include_image,$device_token_id,$payload,$clickaction,$oth1,$oth2,$oth3);
                }
            }
        }
        $response['status'] = 1;
        $response['message'] = 'Sucessfull';
        // $response['con_flat_mas'] = $con_flat_mas;
        $response = json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Sucessfull';
        $response = json_encode($response);
    }
    echo($response);
}
?> 