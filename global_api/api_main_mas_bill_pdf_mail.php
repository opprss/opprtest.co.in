<?php
@include '../../../../../library/dompdf/autoload.inc.php';
@include '../../../../library/dompdf/autoload.inc.php';
@include '../../../library/dompdf/autoload.inc.php';
@include '../../library/dompdf/autoload.inc.php';
@include '../library/dompdf/autoload.inc.php';
@include 'library/dompdf/autoload.inc.php';
use Dompdf\Dompdf;
function main_mas_bill_pdf_mail($prk_admin_id,$main_mas_id,$living_status,$owner_id,$owner_name,$owner_email,$tenant_id,$tenant_name,$tenant_email,$prk_area_name,$tower_name,$flat_name,$late_fine_charrge,$late_fine_rate_type){
    $response = array();
    global $pdoconn;
    $sql = "SELECT mam.`main_tran_year`,
    	mam.`main_tran_month`,
    	mam.`previous_balance`,
    	mam.`current_due`,
    	mam.`fine_charges`,
		DATE_FORMAT(mam.`statement_date`,'%d-%b-%Y') AS `statement_date`,
		DATE_FORMAT(mam.`bill_due_date`,'%d-%b-%Y') AS `bill_due_date`,
		mam.`total_pay_before_due_date`,
		mam.`total_pay_after_due_date`,
		DATE_FORMAT(mam.`generate_date`,'%d-%b-%Y') AS `generate_date`,
		paa.`prk_admin_name`
 		FROM `maintenance_master` mam,`prk_area_admin` paa
 		WHERE mam.`prk_admin_id`= paa.`prk_admin_id`
 		AND mam.`main_mas_id`='$main_mas_id' 
 		AND mam.`prk_admin_id`='$prk_admin_id' 
 		AND mam.`active_flag`='".FLAG_Y."' 
 		AND mam.`del_flag`='".FLAG_N."'";
 	$query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $main_tran_year = $val['main_tran_year'];   
    $main_tran_month = $val['main_tran_month'];   
    $previous_balance = $val['previous_balance'];   
    $current_due = $val['current_due'];   
    $fine_charges = $val['fine_charges'];   
    $bill_due_date = $val['bill_due_date'];   
    $total_pay_before_due_date = $val['total_pay_before_due_date'];   
    $total_pay_after_due_date = $val['total_pay_after_due_date'];   
    $generate_date = $val['generate_date']; 
    $prk_admin_name = $val['prk_admin_name']; 
    $sql = "SELECT `main_child_id`,`maint_id`,`main_type`,`main_charge`,
		DATE_FORMAT(`generate_date`,'%d-%b-%Y') AS `generate_date`
    	FROM `maintenance_child` WHERE `main_mas_id`='$main_mas_id' 
    	AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' 
    	AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    $tot_main=0;
    foreach($arr_catagory as $val){
        $maint_id = $val['maint_id'];
        $main_type = $val['main_type'];
        $main_charge = $val['main_charge'];
        $generate_date = $val['generate_date'];
    	$tot_main+=$main_charge;
    }
    $prk_area_address = prk_area_address_show($prk_admin_id);
    $area_address = json_decode($prk_area_address, true); 
    $prk_address = $area_address['prk_address'];
    $prk_land_mark = $area_address['prk_land_mark'];
    $prk_add_area = $area_address['prk_add_area'];
    $state_name = $area_address['state_name'];
    $city_name = $area_address['city_name'];
    $prk_add_country = $area_address['prk_add_country'];
    $prk_add_pin = $area_address['prk_add_pin'];
    $full_address=$prk_area_name.', '.$state_name.', '.$city_name.', Tower Name:'.$tower_name.', Flat Name: '.$flat_name;
    $date=date_create($main_tran_year.'-'.$main_tran_month.'-15');
    $payment_month=date_format($date,"F-Y");
	$pdf_html='<!DOCTYPE html>
	<html>
		<head>
			<style type="text/css">
				.tg  {border-collapse:collapse;border-spacing:0;margin:0px auto;}
				.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
				.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
				.tg .tg-baqh{text-align:center;vertical-align:top}
				.tg .tg-nrix{text-align:center;vertical-align:middle}
				.tg1  {border-collapse:collapse;border-spacing:0;margin:0px auto;}
				.tg1 td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
				.tg1 th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
				.tg1 .tg-baqh1{text-align:center;vertical-align:top}
				.tg1 .tg-nrix1{text-align:center;vertical-align:middle}
				.tg2  {border-collapse:collapse;border-spacing:0;margin:0px auto;}
				.tg2 td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
				.tg2 th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
				.tg2 .tg-baqh2{text-align:center;vertical-align:top}
				.tg2 .tg-lqy62{text-align:right;vertical-align:top}
				.tg2 .tg-nrix2{text-align:center;vertical-align:middle}
			</style>
		</head>
		<body>
			<div>
				<div style="width: 815px; border: 1px solid black;padding-bottom: 15px;">
					<div style="width: 815px;">
						<div style="float: left; width: 300px; margin: 19px; border: 1px solid black;">
							<div >
								<div>
									<img src="../../../img/logo_pdf.PNG" style="height: 80px;width: 115px;">
								</div>
							</div>
							<div>
								<p><b>Apartment Maintenance Statement</b></p>
							</div>
							<div>
								<p><b>'.$owner_name.'</b> MAHISGOTE,2ND LANE, WEST BENGAL,KOLKATA, PIN:700129</p>
							</div>
						</div>
						<div style="float: right; width: 420px; margin-top: 40px;">
							<table class="tg">
							  <tr>
							    <th class="tg-nrix" colspan="4">Payment</th>
							  </tr>
							  <tr>
							    <td class="tg-baqh" colspan="2">Account Number</td>
							    <td class="tg-baqh" colspan="2">Total Payment Due</td>
							  </tr>
							  <tr>
							    <td class="tg-baqh" colspan="2">1120110189</td>
							    <td class="tg-baqh" colspan="2">Rs.1000/-</td>
							  </tr>
							  <tr>
							    <td class="tg-baqh">Statement Date</td>
							    <td class="tg-baqh">01-08-2019</td>
							    <td class="tg-baqh">Bill Due Date</td>
							    <td class="tg-baqh">10-08-2019</td>
							  </tr>
							</table>
						</div>
					</div></br></br></br></br>
					<div style="width: 815px;">
						<div style="width: 815px;">
							<table class="tg1" style="width: 778px;margin-top: 15px;">
							  <tr>
							    <th class="tg-nrix1">Previous Balance</th>
							    <th class="tg-baqh1">Payment</th>
							    <th class="tg-baqh1">Current Month Due</th>
							    <th class="tg-baqh1">Late Fine Charge</th>
							    <th class="tg-baqh1">Total Due</th>
							  </tr>
							  <tr>
							    <td class="tg-baqh1">-1000</td>
							    <td class="tg-baqh1">2000</td>
							    <td class="tg-baqh1">1300</td>
							    <td class="tg-baqh1">1%</td>
							    <td class="tg-baqh1">300</td>
							  </tr>
							</table>
						</div>
						<div style="width: 815px;">
							<table class="tg2" style="width: 778px;margin-top: 15px;">
							  <tr>
							    <th class="tg-nrix2">Date</th>
							    <th class="tg-baqh2">Your Money Went Here</th>
							    <th class="tg-baqh2">Amount</th>
							  </tr>
							  <tr>
							    <td class="tg-baqh2">01-08-2019</td>
							    <td class="tg-baqh2">Flat Maintanance</td>
							    <td class="tg-baqh2">1000</td>
							  </tr>
							  <tr>
							    <td class="tg-baqh2">01-08-2019</td>
							    <td class="tg-baqh2">Two Wheeler</td>
							    <td class="tg-baqh2">100</td>
							  </tr>
							  <tr>
							    <td class="tg-lqy62" colspan="2">Total</td>
							    <td class="tg-baqh2">1100/-</td>
							  </tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</body>
	</html>';
	// $html=$html.$html11.$html112.$html2.$html3;
	$dompdf = new Dompdf();
    $dompdf->loadHtml($pdf_html);
    $dompdf->setPaper('A4', 'landscape');
    $dompdf->render();
    $name= uniqid();
    $PNG_TEMP_DIR ='./../../uploades/'.$prk_admin_name.'/main_mas_bill_pdf/';
    if (!file_exists($PNG_TEMP_DIR)) {
        $old_mask = umask(0);
        mkdir($PNG_TEMP_DIR, 0777, TRUE);
        umask($old_mask);
    }
    $fulpath=$PNG_TEMP_DIR.'/'.$name.".pdf";
    $path='uploades/'.$prk_admin_name.'/main_mas_bill_pdf/'.$name.".pdf";
    if(file_put_contents($fulpath, $dompdf->output())){
        $report['status']=1;
        $report['url']= $path;
        $report['message']='PDF Created Successful';
    }else{
        $report['status']=0;
        $report['message']='PDF Created Failed';
    }
    $sql="UPDATE `maintenance_master` SET `main_mas_bill_pdf`='$path' WHERE `main_mas_id`='$main_mas_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    // return PRK_BASE_URL.$path;
    return main_mas_bill_mail($living_status,$owner_name,$owner_email,$tenant_name,$tenant_email,$full_address,$payment_month,$previous_balance,$current_due,$late_fine_charrge,$late_fine_rate_type,$total_pay_before_due_date,$bill_due_date);
}
function main_mas_bill_mail($living_status,$owner_name,$owner_email,$tenant_name,$tenant_email,$full_address,$payment_month,$previous_balance,$current_due,$late_fine_charrge,$late_fine_rate_type,$total_pay_before_due_date,$bill_due_date){
	// mobile_massage($mobile_n,$text);
	$previous_balance=($previous_balance*-1);
	$late_fine_charrge=($late_fine_rate_type=='P')?$late_fine_charrge.'%':$late_fine_charrge.'/-';

   	$email_logo = EMAIL_LOGO."oppr-logo.png";
    $email_logo_fb=EMAIL_LOGO_fb."fb.jpg";
    $email_logo_gmail=EMAIL_LOGO_gmail."google.png";
    $email_logo_indeed=EMAIL_LOGO_indeed."inidied.png";
    $message = '
        <html>
          <body>
              <div style="border: 1px solid #00b8d4; width: 600px;">
                <div style="background-color: #00b8d4; height: 80px; width: 600px;">
                  <span style="float: left;">
                 <img src="'.$email_logo.'" height="50" width="100">
                  </span>
                  <br><div style="margin-top: 5px">
                    <a href="#" style="margin-left: 350px;width: 20px;height: 20px;">
                      
                        <img src="'.$email_logo_fb.'" alt="FaceBook" height="30" width="30" / >
                    </a>

                   <span class="google">
                    <a href="#" >

                      <img src="'.$email_logo_gmail.'" alt="Gmail" height="30" width="30" />
                    </a>
                  </span>
                   <span class="linkedin">
                    <a href="#">
                     <img src="'.$email_logo_indeed.'" height="30" width="30" alt="Indeed" />
                    </a>
                  </span>
                </div>
                  <div style="margin-top: 60px; text-align: center;">
                        <span>
                          <b>Maintenance Charges</b>
                        </span>
                  </div>
                  <div style="margin-top: 20px; margin-left: 20px;">
                    <span>
                      <b>Dear Sir/Madam, '.$owner_name.'</b>
                    </span>
                  </div>
                  <div style="margin-top: 20px; margin-left: 30px;width: 555px">
                  	<p>'.$full_address.'  your estatement for '.$payment_month.'. Payment due date '.$bill_due_date.'.</p>

                    <p><table class="tg1" style="width: 500px;margin-top: 15px;">
                          <tr>
                            <th class="tg-nrix1">Previous Balance</th>
                            <th class="tg-baqh1">Payment</th>
                            <th class="tg-baqh1">Current Month Due</th>
                            <th class="tg-baqh1">Late Fine Charge</th>
                            <th class="tg-baqh1">Total Due</th>
                          </tr>
                          <tr style=" text-align: center;">
                            <td class="tg-baqh1">'.$previous_balance.'/-</td>
                            <td class="tg-baqh1">00/-</td>
                            <td class="tg-baqh1">'.$current_due.'/-</td>
                            <td class="tg-baqh1">'.$late_fine_charrge.'</td>
                            <td class="tg-baqh1">'.$total_pay_before_due_date.'/-</td>
                          </tr>
                        </table>
                    </p>

                   <p>In case you need any further clarification for the same, please do get in touch immediately with care@opprss.com or your nearest branch. </p>

                   <p> This E-Notification was automatically generated. Please do not reply to this mail. For any suggestions / feedback, please click on Feedback link provided on<a href="http://opprss.com"> www.opprss.com</a></p>
                  </div>
                  <div style="margin-top: 10px; margin-left: 20px;">

                      <b>Regards,</b><br>
                      <p>OPPR Team.<br>
                      A Smart Parking & Security Solution.
                    </p><div style="width: 555px">
                       &emsp;"The information contained in this electronic message and any attachments to this message are intended for exclusive use of the addressee(s) and may contain confidential or privileged information. If you are not the intended recipient, please notify the sender at OPPR Software Solution Pvt Ltd or care@opprss.com immediately and
                    destroy all copies of this message and any attachments. The views expressed in this E-mail message / Attachments, are those of the individual sender."
                    </div>
                  </div>

                  </div>
                  <div style="margin-top: 620px; text-align: center;">
                  <span><a href="http://opprss.com/about-us">About Us</a> |</span> <span><a href="http://opprss.com/contact-us">Contect Us </a></span>
                  </div>
                  <div style="text-align: center; margin-top: 20px;padding-bottom: 20px; font-size: 11px"> &copy; 2017 BY OPPR SOFTWARE SOLUTION. ALL RIGHTS RESERVED.<br><br>
                  </div>
                </div>
              </div>
          </body>
        </html>
        ';
    email_massage($owner_email,$message); 
    return $owner_email;
}
?> 