<?php
/*
Description: mobile number chaker. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function mobileChecker($mobile){
    $response = array();
    if (preg_match("/^[0-9]{10}$/",$mobile)) {
        return true;
    }
    else
    {
        $response['status'] = 0;
        $response['message'] = 'Mobile Number Not Valide';
        return ($response);
    }
}
?>