<?php
/*
Description: user logout
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function prk_area_logout($token){
    global $pdoconn;
    $sql ="UPDATE `prk_admin_token` SET `active_flag`='".FLAG_N."', `end_date`='".TIME."' WHERE `token_value`='$token'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Logout Successful';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Logout Not Successful';
    }
   return json_encode($response); 
}
?>