<?php
/*
Description: user date of birth 18 years old.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
	function dofChecker($dob){
	    $response = array();
	    $from = new DateTime($dob);
	    $to   = new DateTime('today');
	    $year=$from->diff($to)->y;
	    if ($year>=18) {
	        $response['status'] = 1;
	        $response['message'] = 'Welcome';
	    }
	    else{
	        $response['status'] = 0;
	        $response['message'] = 'Must be 18 Years Old.';
	    }
	    return json_encode($response);
	}
?>