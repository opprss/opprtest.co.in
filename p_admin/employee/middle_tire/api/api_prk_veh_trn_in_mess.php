<?php
/*
Description: parking area in message
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :07-04-2018
*/ 
function prk_veh_trn_in_mess($user_mobile,$veh_in_date,$veh_in_time,$prk_veh_type,$veh_number,$prk_area_name,$otp_mes){

   $text ="Welcome to ".$prk_area_name.", Vehicle # ".$veh_number."".$otp_mes." ,Vehicle In Date& Time - ".$veh_in_date." ".$veh_in_time." For more details please visit ".SMS_URL.".";
    mobile_massage($user_mobile,$text);
    $response['status'] = 1;
    $response['message'] = 'OTP Sent';
    return json_encode($response);
}
?>