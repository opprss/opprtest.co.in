<?php
require('../library/fpdf/fpdf.php');
include_once("p_admin_middle_tire/api/parkAreaReg_api.php");
/*token check in every page load*/
$json = json_decode(prk_token_check($_SESSION['prk_admin_id'],$_SESSION['token']));
if (! $json->session) {
  header('location: logout');
}
$prk_add_id=base64_decode($_REQUEST['prk_add_id']);
$result=prk_area_gate_dtl_list($prk_add_id);
$all_data=select_prk_dtl($prk_add_id);
//print_r($result);
class PDF extends FPDF{
    function Footer(){
        // Go to 1.5 cm from bottom
        $this->SetXY(30,270);
        $this->SetFont('Arial','',9);
        $this->Cell(10,5,COPYRIGHT_PDF,'C');
        $this->SetY(-15);
        $this->SetFont('Arial','I',8);
        $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
    }
    function Header(){
        // Select Arial bold 15
        $this->SetFont('Arial','',15);
        $this->setXY(90,70);
        $Image='../img/logo_pdf.png';
        $this->Image($Image,80,25,-250);
        $this->Ln(20);
    }
}
$pdf = new PDF();
foreach($result as $value){
    if($value['prk_area_gate_type']!='S'){
        $pdf->AddPage("P","A4");
        $pdf->SetFont('Arial','B',16);
        // $pdf->setXY(10,30);
        $pdf->setXY(100,60);
        $pdf->Line(10,65,200,65);
        $pdf->Line(10,68,200,68);
        $pdf->setXY(20,80);
        $pdf->Cell(10,10,'Name :-');
        // print_r($all_data); 
        foreach($all_data as $name)
      	$user_name=$name['prk_area_name'];
      	$address=$name['prk_address'];
      	$landmark=$name['prk_land_mark'];
      	$add_area=$name['prk_add_area'];
      	$add_city=$name['city_name'];
      	$add_state=$name['state_name'];
      	$add_country=$name['prk_add_country'];
      	$add_pin= $name['prk_add_pin'];
        $gate_type_full_name='For IN';
        if($value['prk_area_gate_type']=='S'){
            # code...
            $gate_type_full_name='For SKIP';
        }else if ($value['prk_area_gate_type']=='I') {
            $gate_type_full_name='For IN';
            # code...
        }else if ($value['prk_area_gate_type']=='O') {
            # code...
            $gate_type_full_name='For OUT';
        }else if ($value['prk_area_gate_type']=='B') {
            $gate_type_full_name='For BOTH';
            # code...
        }else{
            $gate_type_full_name='For OTHER';
            # code...
        }
        $pdf->setXY(50,80);
        $pdf->Cell(30,10,$user_name);
        // $pdf->SetFont('Arial','',16);
        $pdf->setXY(50,90);
        $pdf->Cell(30,10,$address);
        $pdf->setXY(50,97);
        $pdf->Cell(30,10,$landmark);
        $pdf->setXY(50,105);
        $pdf->Cell(30,10,$add_area);
        $pdf->setXY(50,110);
        $pdf->Cell(30,10,$add_city);
        $pdf->setXY(50,115);
        $pdf->Cell(30,10,$add_state);
        $pdf->setXY(50,120);
        $pdf->Cell(30,10,$add_country);
        $pdf->setXY(50,125);
        $pdf->Cell(30,10,$add_pin);
        // end
        $pdf->setXY(20,90);
        $pdf->Cell(10,10,'Address :-');

        $pdf->setXY(150,90);
        $pdf->Cell(100,10,$gate_type_full_name);
        $pdf->setXY(150,100);
        $qrcode= $value['prk_qr_code'];
        $code = OPPR_BASE_URL.'library/QRCodeGenerate/generate.php?text='.$qrcode;
        $type = pathinfo($code, PATHINFO_EXTENSION);
        $data = file_get_contents($code);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $pdf->Image($base64, 145,100,0,-200,'png');
    }
}
$pdf->Output();
?>