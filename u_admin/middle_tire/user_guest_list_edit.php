<?php
/*
Description: user address edit.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
    require_once 'api/reg_api.php';
    $response = array();
    if(isAvailable(array('user_guest_list_id','guest_name','user_admin_id','guest_mobile','guest_gender','guest_address','guest_purpose','no_of_guest','guest_id_proof','in_date','to_time','form_time','mobile','token','prk_admin_id'))){
        if(isEmpty(array('user_guest_list_id','guest_name','user_admin_id','guest_mobile','guest_gender','guest_address','guest_purpose','no_of_guest','in_date','to_time','form_time','mobile','token','prk_admin_id'))){
           $user_guest_list_id = trim($_POST['user_guest_list_id']);
           $guest_name = trim($_POST['guest_name']);
           $user_admin_id = trim($_POST['user_admin_id']);
            $guest_mobile = trim($_POST['guest_mobile']);
            $guest_gender = trim($_POST['guest_gender']);   
            $guest_address = trim($_POST['guest_address']);
            $guest_purpose = trim($_POST['guest_purpose']);
            $no_of_guest = trim($_POST['no_of_guest']);
            $guest_id_proof = trim($_POST['guest_id_proof']);

            $in_date = trim($_POST['in_date']);
            $to_time = trim($_POST['to_time']);
            $form_time = trim($_POST['form_time']);
            if (isset($_POST['guest_in_verify'])) {
                $guest_in_verify = trim($_POST['guest_in_verify']);
            }else{
                $guest_in_verify =FLAG_N;
            }
            if (isset($_POST['guest_out_verify'])) {
                $guest_out_verify = trim($_POST['guest_out_verify']);
            }else{
                $guest_out_verify =FLAG_N;
            }

            $mobile = trim($_POST['mobile']);
            $token = trim($_POST['token']);
            $prk_admin_id = trim($_POST['prk_admin_id']);
            $resp=user_token_check($user_admin_id,$token);
            $json = json_decode($resp);
            if($json->status=='1'){
                $response = user_guest_list_edit($user_guest_list_id,$guest_name,$guest_mobile,$guest_gender,$guest_address,$guest_purpose,$no_of_guest,$guest_id_proof,$user_admin_id,$in_date,$to_time,$form_time,$guest_in_verify,$guest_out_verify,$mobile,$prk_admin_id);
            }else{
                $response = $resp; 
            }
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response =  json_encode($response);
        }
    }else{
         $response['status'] = 0; 
         $response['message'] = 'Invalid API Call';
         $response =  json_encode($response);
    }
    echo ($response);         
?>