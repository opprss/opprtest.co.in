<?php 
/*
Description: parking employ out vehicle
Developed by: Rakhal Raj Mandal
Created Date: 06-04-2018
Update date : 25-05-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','prk_user_username','token','m_account_number','tower_id','flat_id','owner_id','tenant_id','amount','payment_type','bank_name','cheque_number'))){
 	if(isEmpty(array('prk_admin_id','prk_area_user_id','prk_user_username','token','m_account_number','tower_id','flat_id','amount','payment_type'))){
 		
	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_area_user_id = trim($_POST['prk_area_user_id']);
        $prk_user_username = trim($_POST['prk_user_username']);
        $token = ($_POST['token']);
	 	$m_account_number = trim($_POST['m_account_number']);
        $tower_id = trim($_POST['tower_id']);
        $flat_id = trim($_POST['flat_id']);
        $owner_id = trim($_POST['owner_id']);
        $tenant_id = trim($_POST['tenant_id']);
        $amount = trim($_POST['amount']);
        $payment_type = trim($_POST['payment_type']);
        $bank_name = trim($_POST['bank_name']);
        $cheque_number = trim($_POST['cheque_number']);
        $main_tran_by = 'E';
        $main_tran_rece_by = $prk_user_username;
        $inserted_by = $prk_user_username;
        $online_payment_dtl_id=isset($_POST["online_payment_dtl_id"]) ? trim($_POST['online_payment_dtl_id']) : ''; 
        $orderid=isset($_POST["orderid"]) ? trim($_POST['orderid']) : ''; 
        $token_ch=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($token_ch);
        if($json->status=='1'){ 
            $oth1=$oth2=$oth3=$oth4=$oth5='';
	        $response = maintenance_pay($prk_admin_id,$m_account_number,$tower_id,$flat_id,$owner_id,$tenant_id,$amount,$payment_type,$bank_name,$cheque_number,$main_tran_by,$main_tran_rece_by,$inserted_by,$online_payment_dtl_id,$orderid,$oth1,$oth2,$oth3,$oth4,$oth5);
        }else{

            $response = $token_ch;
        }
 	}else{
        $response['status'] = 0;
    	$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>