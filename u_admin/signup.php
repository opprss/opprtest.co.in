<?php
  include('../global_asset/config.php');
  require_once '../global_api/global_api.php';
  require_once 'middle_tire/api/reg_api.php';
  include 'middle_tire/api/session_api.php';
  @session_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Meta -->
    <meta name="description" content="OPPRSS provides best quality digital parking services and digital parking solutions."/>
    <meta name="keywords" content="parking solution free , parking management software free , digital parikig solution , smart parking solution, app based parking , parking solution , automatic carparking system , parking solution in kolkata , parking ticket solutions/oppr parking solution , oppr software , parking management , autopay parking system , digital ticketing system , mobile parking solution , vehicle parking system" />
    <meta name="author" content="oppr software solution private limited">
    <title>OPPRSS | SIGNUP</title>
    <link rel="shortcut icon" href="<?php echo FAVICON ?>" type="image/x-icon">
    <!-- vendor css -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">
    <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">  
    <!-- calender -->  
    <link rel="stylesheet" href="../library/date/css/pickmeup.css" type="text/css" />
    <script type="text/javascript" src="../library/date/js/pickmeup.js"></script>
    <script
      src="https://code.jquery.com/jquery-3.2.1.js"
      integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
      crossorigin="anonymous"></script>
    <script src="https://kendo.cdn.telerik.com/2017.3.1026/js/kendo.all.min.js"></script>
    <!-- Amanda CSS -->
    <link rel="stylesheet" href="../css/amanda.css">
    <link rel="stylesheet" type="text/css" href="../css/style-up.css">
  </head>
  <body>
    <div class="am-signup-wrapper">
      <div class="am-signup-box" >
        <div class="row no-gutters">
         <div class="col-lg-7" style="background:url('../img/index-banner.jpg');padding-bottom: 0px;">
            <div class="signin-logo">
              <div style="text-align: left;">
              <a href="http://opprss.com" target="_blank"><img style="width: 210px; text-align: left;" src="<?php echo LOGO ?>"></a></div>
              <div class="content">
                <h1>Welcome to OPPRSS</h1>
                <h5 class="d-none d-sm-block pd-b-10">We provide an innovative and executive parking & security solution that improves parking & security experience more than ever before.</h5>  
              </div>
              <div class="row features-icon">
                <div class="col-lg-4">
                  <i class="fa fa-tasks fa-3x"></i>
                  <h6 class="pd-t-5">Digitally Tracked</h6>
                </div> 
                <div class="col-lg-4">
                  <i class="fa fa-american-sign-language-interpreting fa-3x"></i>
                  <h6 class="pd-t-5">Better Communication</h6>
                </div> 
                <div class="col-lg-4">
                  <i class="fa fa-android fa-3x "></i>
                  <h6 class="pd-t-5">App Based</h6>
                </div>                
              </div>                           
              <div class="content-footer features-icon mg-t-50" style="text-align: right;">
                <button class="btn-tab" onclick="window.open('https://www.opprss.com/contact-us', '_blank');" id="con_us">Contact Us</button>
                <button class="btn-tab" id="user_tab">User</button>
                <button class="btn-tab" id="admin_tab">Parking Area</button>
                <button class="btn-tab" id="super_admin_tab">Super Adnin</button>
                 <hr>
                <p class="tx-center tx-white-5 tx-12 mg-t-0 mg-b-0"><?php echo COPYRIGHT_EMAIL?></p>
              </div>
            </div>
          </div>
          <div class="col-lg-5">
            <h2 class="tx-gray-800 mg-b-50 text-center">User Signup</h2>
            <form id="regForm" method="post" action="<?php echo USER_URL?>registration.php">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group error_show">
                    <input type="text" name="mobile" id="mobile" maxlength="10" class="form-control" placeholder="Mobile" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                    <span id="user_staus_error"></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group error_show">
                    <input type="text" name="name" id="name" class="form-control" placeholder="Name" onkeyup="nameUppercase()" onchange="return trim(this)">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group error_show">
                    <input type="password" name="password" id="password" class="form-control" placeholder="Type password" onchange="return trim(this)">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group error_show">
                    <input type="password" class="form-control" placeholder="Confirm Password" name="repassword" id="repassword" onchange="return trim(this)">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group error_show">
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email" onkeyup="emailLowercase()">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group error_show">
                    <input type="text" name="dob" id="dob" placeholder="MM/DD/YYYY" class="form-control" readonly="readonly" style="background-color: transparent;">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group error_show">
                    <label class="rdiobox">
                      <input name="gender" id="gender" type="radio" value="M" checked >
                      <span>Male</span>
                    </label>
                    <label class="rdiobox">
                      <input name="gender" id="gender" value="F" type="radio">
                      <span>Female</span>
                    </label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group error_show">
                    <div class="select error_show">
                      <select name="v_type" id="v_type" style="opacity: 0.8">
                        <option value="">Select Vehicle Type</option>
                      </select>
                    </div>
                    <span id="error" style="display: none; font-size:small; color: #FFA07A;">Choose vehicle type</span>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Vehicle Number" name="v_number" id="v_number" onkeyup="vNumberUppercase()" maxlength ="10" >
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group error_show">
                    <input type="text" name="otp" id="otp" class="form-control" placeholder="Type OTP" style="display: none;" maxlength="6">
                  </div>
                </div>
                <div class="col-md-4">
                  <button type="button" class="btn btn-block" id="resend_otp" style="margin-top: -6px; display: none;">RESEND OTP</button>
                  <button type="button" class="btn btn-block" id="send_otp" style="margin-top: -6px;">SEND OTP</button>
                </div>
                <div class="col-md-12 mg-t-20">
                  <p>
                    <input type="checkbox" class="checkbox" id="agree" name="agree" required>
                    <label for="agree">Terms & Condition <a href="http://opprss.com/legal" target="_blank">more information..</a></label>
                    <br>
                    <label for="agree" class="error block"></label>
                  </p>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <button type="button" class="btn btn-block" id="register" disabled="disabled">REGISTER</button>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="reset" class="btn btn-block" id="configreset" value="RESET">
                  </div>
                </div>
              </div>
              <span id="otp_msg" style="color: #FFA07A"></span>
              <?php
                if (isset($_SESSION['error'])) {
                  echo "<p style=color:red>".$_SESSION['error']."</p>";
                  unset($_SESSION['error']);
                }
              ?>
              <div class="mg-t-10 pull-right" style="font-size: auto;">
                <a href="<?php echo OPPR_BASE_URL.'signin'?>">Login</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <script src="../lib/popper.js/popper.js"></script>
    <script src="../lib/bootstrap/bootstrap.js"></script>
    <script src="../lib/jquery-ui/jquery-ui.js"></script>
    <script src="../lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
    <script src="../lib/jquery-toggles/toggles.min.js"></script>
    <script src="../lib/highlightjs/highlight.pack.js"></script>
    <script src="../lib/select2/js/select2.min.js"></script>
    <script src="../js/amanda.js"></script>
    <script src="../lib/validation/jquery.validate.js"></script>
  </body>
  <script type="text/javascript">
    var userUrl = "<?php echo USER_URL; ?>";
    var userBaseUrl = "<?php echo USER_BASE_URL; ?>";
    $.validator.addMethod("valueNotEquals", function(value){
      arg = "";
      if (value != "") {
        //return true;
        return arg !== value;
      }else{
        $("#error").show();
        return false;
      }
    });
    /*Min Age restriction*/
    $.validator.addMethod("minAge", function(value, element, min) {
      var today = new Date();
      //alert(value);
      var birthDate = new Date(value);
      //alert(birthDate);
      var age = today.getFullYear() - birthDate.getFullYear();
      //alert(age);
      if (age > min+1) {
          return true;
      }
      var m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
          age--;
      }
      return age >= min;
    }, "You are not old enough!");
    /*validation*/
    $( document ).ready( function () {
      /*V_TYPE FORM DB*/
      var urlVehType = userUrl+'vehicle_type.php';
      $.ajax ({
        type: 'POST',
        url: urlVehType,
        data: "",
        success : function(data) {
          // alert(data);
          var obj=JSON.parse(data);
          var areaOption = "<option value=''>Select Vehicle Type</option>";
          $.each(obj, function(val, key) {
            areaOption += '<option value="' + key['vehicle_Sort_nm'] + '">' + key['vehicle_type'] + '</option>'
          });
          $("#v_type").html(areaOption);
        }
      });
      /*VALIDATION*/
      var urlUserMobileCheck = userUrl+'accountStatusMobile.php';
      var urlVehNoCheck = userUrl+'v_numberChecker.php';
      var urldobCheck = userUrl+'dofChecker.php';
      $( "#regForm" ).validate( {
        rules: {
          name: "required",
          v_type: "valueNotEquals",
          agree: "required",
          mobile: {
            required: true,
            number: true,
            minlength: 10,
            remote: {
              url: urlUserMobileCheck,
              type: "POST",
              cache: false,
              dataType: "json",
              dataFilter: function(data) {
  			        // alert (data);
                var json = $.parseJSON(data);                      
                // var mo_me=json.message;                       
                if (json.status) {
  			            return true;
  			        } else {
  			            return false;
  			        }
    			    }
            }
          },
          v_number: {
            required: true,
            remote: {
              url : urlVehNoCheck,
              type: "POST",
              cache: false,
              dataType: "html",
              dataFilter: function(data) {
                var json = $.parseJSON(data);                      
                if (json.status) {
                    return true;
                } else {
                    return false;
                }
              }
            }
          },
          password: {
            required: true,
            minlength: 5
          },
          repassword: {
            required: true,
            minlength: 5,
            equalTo: "#password"
          },
          dob: {
            required: true,
            minAge: 18,
            remote: {
              url: urldobCheck,
              type: "POST",
              cache: false,
              dataType: "html",
              dataFilter: function(data) {
                var json = $.parseJSON(data);                      
                if (json.status) {
                    return true;
                } else {
                    return false;
                }
              }
            }
          },
          email: {
            required: true,
            email: true
          }
        },
        messages: {
          name: "Enter your name",
          agree: "Select Terms & Condition",
          mobile: {
            required: "Enter a mobile number",
            number: "Enter valid mobile Numberr",
            minlength: "Enter valid mobile Number",
            remote: "User alreday exist"
          },
          v_number: {
            required: "Enter a vehicle number",
            remote: "Vehicle number is invalid"
          },
          password: {
            required: "Provide a password",
            minlength: "Enter more than 5 digits"
          },
          repassword: {
            required: "Provide a repassword",
            minlength: "Enter more than 5 digits",
            equalTo: "Enter the same as password"
          },
          dob: {
              required: "Enter you date of birth",
              minAge: "Must be 18 years old!",
              // remote: "Must be 18 years old!"
          }, 
          email: "Enter a valid email"
        }
      });
      /*SEND OTP*/
      $('#send_otp').click(function () {
        $('[name="mobile"], [name="email"]').each(function () {
          $(this).rules('add', {
              required: true
          });
        });
        if($("#regForm").valid()){
          //alert("CHECKED AND CLICK OTP BTN");
          var mobile = $('#mobile').val();
          var name = $('#name').val();
          var email = $('#email').val();
          var urlOtpSend = userUrl+'otpSend.php';
          $.ajax({
            url :urlOtpSend,
            type:'POST',
            data :{
              'mobile':mobile,
              'name':name,
              'email':email
            },
            dataType:'html',
            success  :function(data){
              //alert(data);
              $('#otp').show();
              $('#otp').focus();
              $('#resend_otp').prop("disabled",true);
              $('#resend_otp').show();
              $('#send_otp').hide();
              //$('#register').show();
              $('#otp_msg').html('OTP Sent to your mobile');
              $("#mobile").prop("disabled", true);
              var myVar = setInterval(myTimer, 20000);
              function myTimer() {
                var d = new Date();
                //$('#resend_otp').removeAttr('disabled');
                $('#resend_otp').prop("disabled",false);
              }
            }
          }).responseText;
        }
      });
      /*RESEND OTP*/
      $('#resend_otp').click(function () {
        $('[name="mobile"], [name="email"]').each(function () {
          $(this).rules('add', {
              required: true
          });
        });
        if($("#regForm").valid()){
          //alert("ck and clicked resend otp btn");
          var mobile = $('#mobile').val();
          var name = $('#name').val();
          var email = $('#email').val();
          var urlOtpSend = userUrl+'otpSend.php';
          var urlOtpSend = userUrl+'otpSend.php';
          $.ajax({
            url :urlOtpSend,
            type:'POST',
            data :{
              'mobile':mobile,
              'name':name,
              'email':email
            },
            dataType:'html',
            success  :function(data){
              //alert(data);
              $('#otp').focus();
              $('#resend_otp').prop("disabled",true);
              $('#otp_msg').html('OTP Resent');
              var myVar = setInterval(myTimer, 20000);
              function myTimer() {
                var d = new Date();
                //$('#resend_otp').removeAttr('disabled');
                $('#resend_otp').prop("disabled",false);
                return true;
              }
            }
          }).responseText;
        }
      });
      /*otp validation call*/
      $('#otp').keyup(function(){
        var ch = $('#otp').val().length;
        var i = 0;
        if (ch == 6) {
          $('#register').prop("disabled",false);
        }
      });
      /*FORM SUBMIT*/
      $('#register').click(function () {
        if($("#regForm").valid()){
          var otp = $('#otp').val();
          var mobile = $('#mobile').val();
          var email = "NULL";
          //mobile','otp','email
          var urlOtpVal = userUrl+'otpVal.php';
          $.ajax({
            url :urlOtpVal,
            type:'POST',
            data :{
              'mobile':mobile,
              'email':email,
              'otp':otp
            },
            dataType:'html',
            success  :function(data){
              //alert(data);
              var json = $.parseJSON(data);                      
              if (json.status) {
                var mobile = $('#mobile').val();
                var password = $('#password').val();
                var repassword = $('#repassword').val();
                var name = $('#name').val();
                var email = $('#email').val();
                var dob = $('#dob').val();
                var gender =  $('input[name=gender]:checked').val();
                var v_type = $('#v_type :selected').val();
                var v_number = $('#v_number').val();
                var flag = "Y";
                var device_token_id = '';
                var device_type = 'WEB';
                var str_array = dob.split('-');
                var mm = str_array[0];
                var dd = str_array[1];
                var yy = str_array[2];
                var dob = dd+'-'+mm+'-'+yy;
                var urlRegistration = userUrl+'web_registration.php';                      
                $.ajax({
                  url :urlRegistration,
                  type:'POST',
                  data :{
                    'mobile':mobile,
                    'password':password,
                    'repassword':repassword,
                    'name':name,
                    'email':email,
                    'dob':dob,
                    'gender':gender,
                    'v_type':v_type,
                    'v_number':v_number,
                    't_c_flag': flag,
                    'device_token_id':device_token_id,
                    'device_type':device_type
                  },
                  dataType:'html',
                  success  :function(data){
                    // alert(data);
                    var json = $.parseJSON(data);
                    if (json.status) {
                      var next_page = userBaseUrl+json.next_page;
                      window.location= next_page;
                    }else{
                      $('#user_error').text(json.message); 
                    }
                  }
                }).responseText;
              }else{
                $('#otp_msg').html('OTP Not Matched');
              }
            }
          }).responseText;
        }
      });
      /*custom validation*/
      /*v_type*/
      $("#v_type").blur(function(){
        var v_type = $('#v_type').val();
        if(v_type == ""){
          $('#error').show();
          return false;
        }else{
          /*alert("yes");*/
          $('#error').hide();
          return true;
        }
      });
      /*only aplhabets*/
      $("#name").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)){
            event.preventDefault();
        }
      });
    });
    /*uppercase*/
    function nameUppercase() {
      var x = document.getElementById("name");
      x.value = x.value.toUpperCase();
    }
    function vNumberUppercase() {
      var x = document.getElementById("v_number");
      x.value = x.value.toUpperCase();
    }
    function emailLowercase(){
      var x = document.getElementById("email");
      x.value = x.value.toLowerCase();
    }
    function trim (el) {
      el.value = el.value.
      replace (/(^\s*)|(\s*$)/gi, ""). // removes leading and trailing spaces
      replace (/[ ]{2,}/gi," ").       // replaces multiple spaces with one space 
      replace (/\n +/,"\n");           // Removes spaces after newlines
      return;
    }
    /*date*/     
    addEventListener('DOMContentLoaded', function () {
      var now = new Date;
      //alert(now);
      pickmeup('#dob', {
        default_date   : false,
        /*position       : 'top',*/
        format  : 'm-d-Y',
        hide_on_select : true,
        render : function (date) {
          if (date > now) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
          }
          return {};
        } 
      });
    });
  </script>
</html>
