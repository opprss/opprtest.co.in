<?php
/*
Description: vehicle update.
Developed by: Rakhal Raj Mandal
Created Date: 15-01-2018
Update date : 06-05-2018
*/ 
function add_vehicle_update($user_veh_dtl_id,$user_veh_type,$user_veh_number,$user_veh_reg,$user_veh_reg_exp_dt,$user_ins_number,$user_ins_exp_dt,$user_emu_number,$user_emu_exp_dt,$user_admin_id,$mobile,$rc_img,$ins_img,$emu_img,$oth_doc_no1,$oth_doc_no1_exp_dt,$oth_doc_no1_img,$oth_doc_no2,$oth_doc_no2_exp_dt,$oth_doc_no2_img,$web_rc_img,$web_ins_img,$web_emu_img,$web_oth_doc_no1_img,$web_oth_doc_no2_img){
    global $pdoconn;
    
    $sql = "SELECT * FROM `user_vehicle_detail` WHERE `user_veh_dtl_id`='$user_veh_dtl_id'";
    $query  = $pdoconn->prepare($sql);
    if($query->execute()){
        $row = $query->fetch();
        $user_ins_img = $row['user_ins_img'];
        $user_ins_img_status = $row['user_ins_img_status'];
        $user_emu_img = $row['user_emu_img'];
        $user_emu_img_status = $row['user_emu_img_status'];
        $rc_img_d = $row['rc_img'];
        $rc_img_status = $row['rc_img_status'];
        $oth_doc_no1_img_d = $row['oth_doc_no1_img'];
        $oth_doc_no1_img_status = $row['oth_doc_no1_img_status'];
        $oth_doc_no2_img_d = $row['oth_doc_no2_img'];
        $oth_doc_no2_img_status = $row['oth_doc_no2_img_status'];
        #-----------RC IMAGE-----------#
        if (!empty($rc_img)) {
            
            $rc_imag=veh_img_upload_all($mobile,$rc_img);
        }else if (!empty($web_rc_img)) {
            # code...
            $rc_imag =web_veh_img_upload_all($mobile,$web_rc_img);
        }else{

            $rc_imag=$rc_img_d;
        }
        #------------INC IMAGE----------#
        if (!empty($ins_img)) {

            $ins_imag=veh_img_upload_all($mobile,$ins_img);
        }else if (!empty($web_ins_img)) {
            # code...
            $ins_imag =web_veh_img_upload_all($mobile,$web_ins_img);
        }else{

            $ins_imag=$user_ins_img;
        }
        #------------EMU IMAGE----------#
        if (!empty($emu_img)) {

            $emu_imag=veh_img_upload_all($mobile,$emu_img);
        }else if (!empty($web_emu_img)) {
            # code...
            $emu_imag =web_veh_img_upload_all($mobile,$web_emu_img);
        }else{

            $emu_imag=$user_emu_img;
        }
        #------------OTH1 IMAGE----------#
        if (!empty($oth_doc_no1_img)) {
            
            $oth_doc_no1_imag=veh_img_upload_all($mobile,$oth_doc_no1_img);
        }else if (!empty($web_oth_doc_no1_img)) {
            # code...
            $oth_doc_no1_imag=web_veh_img_upload_all($mobile,$web_oth_doc_no1_img);
        }else{

            $oth_doc_no1_imag=$oth_doc_no1_img_d;
        }
        #------------OTH2 IMAGE----------#
        if (!empty($oth_doc_no2_img)) {
            
            $oth_doc_no2_imag=veh_img_upload_all($mobile,$oth_doc_no2_img);
        }else if (!empty($web_oth_doc_no2_img)) {
            # code...
            $oth_doc_no2_imag=web_veh_img_upload_all($mobile,$web_oth_doc_no2_img);
        }else{

            $oth_doc_no2_imag=$oth_doc_no2_img_d;
        }
        $sql ="UPDATE `user_vehicle_detail` SET `active_flag`='".FLAG_N."', `updated_date`='".TIME."', `updated_by`='$mobile' WHERE `user_admin_id`='$user_admin_id' AND `user_veh_dtl_id`='$user_veh_dtl_id' AND `active_flag`='".FLAG_Y."'";
        $query = $pdoconn->prepare($sql);
        $query->execute();

        $qr_code=$mobile.'-'.$user_veh_type.'-'.$user_veh_number;
        $sql = "INSERT INTO `user_vehicle_detail`(`user_veh_type`,`user_veh_number`,`user_veh_reg`,`user_veh_reg_exp_dt`,`user_admin_id`,`veh_qr_code`,`inserted_by`,`inserted_date`, `user_ins_number`, `user_ins_exp_dt`, `user_ins_img`, `user_ins_img_status`, `user_emu_number`, `user_emu_exp_dt`, `user_emu_img`, `user_emu_img_status`, `rc_img`, `rc_img_status`, `oth_doc_no1`, `oth_doc_no1_exp_dt`, `oth_doc_no1_img`, `oth_doc_no1_img_status`, `oth_doc_no2`, `oth_doc_no2_exp_dt`, `oth_doc_no2_img`, `oth_doc_no2_img_status`) VALUE ('$user_veh_type','$user_veh_number','$user_veh_reg','$user_veh_reg_exp_dt','$user_admin_id','$qr_code','$mobile','".TIME."','$user_ins_number','$user_ins_exp_dt','$ins_imag','$user_ins_img_status','$user_emu_number','$user_emu_exp_dt','$emu_imag','$user_emu_img_status','$rc_imag','$rc_img_status','$oth_doc_no1','$oth_doc_no1_exp_dt','$oth_doc_no1_imag','$oth_doc_no1_img_status','$oth_doc_no2','$oth_doc_no2_exp_dt','$oth_doc_no2_imag','$oth_doc_no2_img_status')";
        $query = $pdoconn->prepare($sql);
        if($query->execute()){
            $user_veh_dtl_id = $pdoconn->lastInsertId();
            $response['status'] = 1;
            $response['user_veh_dtl_id'] = $user_veh_dtl_id;
            $response['message'] = 'Add Vehicle Successful';
            $response = json_encode($response);
        }else{
            $response['status'] = 1;
            $response['message'] = 'Add Vehicle Not Successful';
            $response = json_encode($response); 
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'Update Not Successful';
        $response = json_encode($response);
    }
    return $response;
}
?>