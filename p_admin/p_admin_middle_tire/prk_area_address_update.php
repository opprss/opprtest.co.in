<?php
/*
Description: Parking adimn Address Update.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
    // require_once 'api/global_api.php';
    require_once 'api/parkAreaReg_api.php';
    $response = array();
 
    if(isAvailable(array('prk_address','prk_land_mark','prk_add_area','prk_add_city','prk_add_state','prk_add_country','prk_add_pin','prk_admin_id','prk_admin_name'))){

        if(isEmpty(array('prk_address','prk_land_mark','prk_add_area','prk_add_city','prk_add_state','prk_add_country','prk_add_pin','prk_admin_id','prk_admin_name'))){
            $prk_address = trim($_POST['prk_address']);
            $prk_land_mark = trim($_POST['prk_land_mark']);
            $prk_add_area = trim($_POST['prk_add_area']);
            $prk_add_city = trim($_POST['prk_add_city']);
            $prk_add_state = trim($_POST['prk_add_state']);
            $prk_add_country = trim($_POST['prk_add_country']);
            $prk_add_pin = trim($_POST['prk_add_pin']);
            $prk_admin_id = trim($_POST['prk_admin_id']);
            $prk_admin_name = trim($_POST['prk_admin_name']);
            $response = prk_area_address_update($prk_address,$prk_land_mark,$prk_add_area,$prk_add_city,$prk_add_state,$prk_add_country,$prk_add_pin,$prk_admin_id,$prk_admin_name);
            echo ($response);
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
        }
    }else{
         $response['status'] = 0; 
         $response['message'] = 'Invalid API Call';
         echo json_encode($response);
     }
?>