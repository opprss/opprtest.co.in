<?php 
/*
Description: user gate pass delete.
Developed by: Rakhal Raj Mandal
Created Date: 18-02-2018
Update date : 18-05-2018
*/
 require_once 'api/reg_api.php';
// include_once 'api/global_api.php'; 
$response = array();
if(isAvailable(array('user_add_id','user_admin_id','mobile','token'))){
 	if(isEmpty(array('user_add_id','user_admin_id','mobile','token'))){
        $user_add_id = trim($_POST['user_add_id']);
	 	$user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
	 	$token = trim($_POST['token']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $response = user_address_delete($user_add_id,$user_admin_id,$mobile);
        }else{
            $response = $resp; 
        }
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            $response =  json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>