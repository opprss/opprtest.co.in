<?php
/*
Description: parking area employ create.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function designation($prk_admin_id,$desig_name,$desig_short_name,$effective_date,$end_date,$parking_admin_name,$crud_type,$designation_id){
   $response = array();
   $allplayerdata = array();
    global $pdoconn;
    switch ($crud_type) {
        case 'I':
            $id=uniqid();
            $sql="INSERT INTO `designation`(`designation_id`, `prk_admin_id`, `desig_name`, `desig_short_name`, `effective_date`, `end_date`, `inserted_by`, `inserted_date`) VALUES ('$id','$prk_admin_id','$desig_name','$desig_short_name','$effective_date','$end_date','$parking_admin_name','".TIME."')";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Designation Add';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Designation Not Add';
            } 
            break;
        case 'U':
            $sql="UPDATE `designation` SET `active_flag`='".FLAG_N."',`updated_by`='$parking_admin_name',`updated_date`='".TIME."' WHERE `designation_id`='$designation_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();

            $sql="INSERT INTO `designation`(`designation_id`, `prk_admin_id`, `desig_name`, `desig_short_name`, `effective_date`, `end_date`, `inserted_by`, `inserted_date`) VALUES ('$designation_id','$prk_admin_id','$desig_name','$desig_short_name','$effective_date','$end_date','$parking_admin_name','".TIME."')";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Designation Modify Save';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Designation Modify Not Save ';
            }  
            break;
        case 'S':
            $sql="SELECT `designation_id`,`prk_admin_id`,`desig_name`,`desig_short_name`,`effective_date`,`end_date`,IF('".TIME_TRN."' BETWEEN `effective_date` AND `end_date`, '".FLAG_Y."', '".FLAG_N."') as activ_status FROM `designation` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."' AND `designation_id`='$designation_id'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['designation_id'] =  $val['designation_id'];
                $response['prk_admin_id'] =  $val['prk_admin_id'];
                $response['desig_name'] =  $val['desig_name'];
                $response['desig_short_name'] =  $val['desig_short_name'];
                $response['effective_date'] =  $val['effective_date'];
                $response['end_date'] =  $val['end_date'];
                $response['activ_status'] =  $val['activ_status'];
            }else{
                $response['status'] = 0;
                $response['message'] = 'Not Successful';
            }  
            break;
        case 'LS':
            $sql="SELECT `designation_id`,`prk_admin_id`,`desig_name`,`desig_short_name`,`effective_date`,`end_date`,IF('".TIME_TRN."' BETWEEN `effective_date` AND `end_date`, '".FLAG_Y."', '".FLAG_N."') as activ_status FROM `designation` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $arr_catagory = $query->fetchAll();
                foreach($arr_catagory as $val){
                    $park['designation_id'] =  $val['designation_id'];
                    $park['prk_admin_id'] =  $val['prk_admin_id'];
                    $park['desig_name'] =  $val['desig_name'];
                    $park['desig_short_name'] =  $val['desig_short_name'];
                    $park['effective_date'] =  $val['effective_date'];
                    $park['end_date'] =  $val['end_date'];
                    $park['activ_status'] =  $val['activ_status'];
                    array_push($allplayerdata, $park);
                }
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['designation'] = $allplayerdata;
            }else{
                $response['status'] = 0;
                $response['message'] = 'List Empty';
            }
            break;
        case 'D':
            $sql="UPDATE `designation` SET `del_flag`='".FLAG_Y."',`updated_by`='$parking_admin_name',`updated_date`='".TIME."' WHERE `designation_id`='$designation_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Delete Successful';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Delete Not Successful';
            }
            break;
        default:
            $response['status'] = 0;
            $response['message'] = 'Wrong Select';
            break;
    }
    return json_encode($response);  
}
?>