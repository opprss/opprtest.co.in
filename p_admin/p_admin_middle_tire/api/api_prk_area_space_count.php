<?php 
@include_once '../../connection-pdo.php';
@include_once '../../global_asset/config.php';
@include_once '../../global_asset/config.php';
@include_once '../global_asset/config.php';
@include_once 'global_asset/config.php';


function prk_area_space_count($prk_admin_id){
    $allplayerdata = array();
    global $pdoconn;
         $sql = "SELECT pr.prk_admin_id, 
        'PAID PARKING' sub_unit_short_name,
        pr.prk_vehicle_type,
        pr.vehicle_type_dec,
        pr.tot_prk_space, 
        IFNULL(pt.veh_count,0) AS veh_count, 
        (IFNULL(pr.tot_prk_space,0) - IFNULL(pt.veh_count,0)) as veh_avl
        FROM 
        (SELECT `prk_admin_id`, `prk_veh_type`, COUNT(`prk_veh_type`) AS veh_count
        FROM `prk_veh_trc_dtl` 
        WHERE `prk_veh_out_time` IS NULL
        AND `payment_type` NOT in ( '".DGP."', '".TDGP."')
        GROUP BY `prk_veh_type`, `prk_admin_id`) pt right join
        (SELECT `prk_area_rate`.`prk_admin_id`, `prk_area_rate`.`prk_vehicle_type`,`vehicle_type`.`vehicle_type_dec`, `prk_area_rate`.`tot_prk_space` 
        FROM   prk_area_rate , vehicle_type 
        where   `prk_area_rate`.`prk_vehicle_type` = `vehicle_type`.`vehicle_sort_nm`
        AND   `prk_area_rate`.`active_flag`='".FLAG_Y."'
        AND   `prk_area_rate`.`del_flag`='".FLAG_N."'
        AND  '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(`prk_area_rate`.`prk_rate_eff_date`, '%d-%m-%Y'), '%Y-%m-%d')    
                AND  DATE_FORMAT(STR_TO_DATE(IFNULL(`prk_area_rate`.`prk_rate_end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')
        ) pr
        on  pr.`prk_admin_id` = pt.`prk_admin_id`AND   pr.`prk_vehicle_type` = pt.`prk_veh_type`
        where   pr.`prk_admin_id` = '$prk_admin_id'
        union            
        SELECT pr.prk_admin_id, 
        psu.prk_sub_unit_short_name,
        pr.veh_type,
        pr.vehicle_type_dec,
        pr.veh_space, 
        IFNULL(pt.veh_count,0) AS veh_count, 
        (IFNULL(pr.veh_space,0) - IFNULL(pt.veh_count,0)) as veh_avl
        FROM 
        (SELECT `prk_admin_id`, `prk_veh_type`,`prk_sub_unit_id`, COUNT(`prk_veh_type`) AS veh_count
        FROM `prk_veh_trc_dtl` 
        WHERE `prk_veh_out_time` IS NULL
        AND `payment_type` not in ( '".TDGP."', '".CASH."', '".WALLET."')
        GROUP BY `prk_veh_type`, `prk_admin_id`,`prk_sub_unit_id`) pt right join
        (SELECT `prk_sub_unit_veh_space`.`prk_admin_id`, `prk_sub_unit_veh_space`.`prk_sub_unit_id`,
        `prk_sub_unit_veh_space`.`veh_type`,
        `vehicle_type`.`vehicle_type_dec`, `prk_sub_unit_veh_space`.`veh_space` 
        FROM   prk_sub_unit_veh_space , vehicle_type 
        where   `prk_sub_unit_veh_space`.`veh_type` = `vehicle_type`.`vehicle_sort_nm`
        AND   `prk_sub_unit_veh_space`.`active_flag`='".FLAG_Y."' 
        AND `prk_sub_unit_veh_space`.`del_flag`='".FLAG_N."' 
        AND  '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(`prk_sub_unit_veh_space`.`prk_sub_unit_veh_space_eff_date`, '%d-%m-%Y'), '%Y-%m-%d')    
        AND  DATE_FORMAT(STR_TO_DATE(IFNULL(`prk_sub_unit_veh_space`.`prk_sub_unit_veh_space_end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')) pr
        on    pr.`prk_admin_id` = pt.`prk_admin_id`
        AND   pr.`veh_type` = pt.`prk_veh_type`
        AND   pr.`prk_sub_unit_id`  = pt.`prk_sub_unit_id`
        JOIN   prk_sub_unit psu on psu.`prk_sub_unit_id` = pr.`prk_sub_unit_id`  
        where   pr.`prk_admin_id` = '$prk_admin_id'
        order by 1,2,3";


    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val) 
    {
        $response['status'] = 1;
        $response['message'] = 'data ssucessfull';
        // $park['prk_admin_id'] = $val['prk_admin_id'];
        $park['sub_unit_short_name'] = $val['sub_unit_short_name'];
        $park['vehicle_type_dec'] = $val['vehicle_type_dec'];
        $park['tot_prk_space'] = $val['tot_prk_space'];
        $park['veh_count'] = $val['veh_count'];
        $park['veh_avl'] = $val['veh_avl'];
        array_push($allplayerdata, $park);
        $response['prking_space'] = $allplayerdata;
    }
    return json_encode($response);
}
 ?>