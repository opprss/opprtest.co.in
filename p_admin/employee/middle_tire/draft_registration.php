<?php
/*
Description: vehicle in parking area 
Developed by: Rakhal Raj Mandal
Created Date: 10-04-2018
Update date : 17-05-2018,25-05-2018,04-06-2018,08-06-2018,06-07-2018
*/
include_once 'api/parkAreaReg_api.php';
$response = array();
$re = array();
$r = array();
if(isAvailable(array('user_mobile','user_veh_type','user_veh_number','prk_admin_id','prk_in_rep_name','payment_type','gate_pass_num','prk_area_user_id','token','advance_pay','advance_flag','veh_token'))){
    if(isEmpty(array('user_veh_number','user_veh_type','prk_admin_id','prk_in_rep_name','payment_type','gate_pass_num','prk_area_user_id','token','advance_flag'))){
        
        $user_mobile = trim($_POST['user_mobile']);
        $user_veh_type = trim($_POST['user_veh_type']);
        $user_veh_number = strtoupper(trim($_POST['user_veh_number']));
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $prk_in_rep_name = trim($_POST['prk_in_rep_name']);
        $payment_type = trim($_POST['payment_type']);
        $gate_pass_num = trim($_POST['gate_pass_num']);
        $advance_pay = trim($_POST['advance_pay']);

        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $token = trim($_POST['token']);
        $veh_token = $_POST['veh_token'];
        $advance_flag = trim($_POST['advance_flag']);
        global $pdoconn;
        $token_check=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($token_check);
        if($json->status=='1'){
            $prk_wh_co= prking_wheeler_count($prk_admin_id,$user_veh_type,$user_veh_number); 
            $json = json_decode($prk_wh_co);
            if($json->status=='1'){
                $wheeler_co= wheeler_count($prk_admin_id,$user_veh_type,$user_veh_number,$payment_type);
                $json = json_decode($wheeler_co);
                if($json->status=='1'){
                    $otp_mes='';
                    $v_nu='';
                    $veh_chek='';
                    $veh_out_otp=NULL;
                    $user_admin_id=NULL;
                    $prk_sub_unit_id = NULL;
                    $prk_inf =prk_inf($prk_admin_id);
                    $json = json_decode($prk_inf);
                    if($json->status=='1'){
                        $prk_admin_name = $json->prk_admin_name;
                        $advance_flag = $json->advance_flag;
                        $prient_in_flag = $json->prient_in_flag;
                        $prient_out_flag = $json->prient_out_flag;
                        $sms_flag = $json->sms_flag;
                        $prk_area_name = $json->prk_area_name;
                        $prk_area_short_name = $json->prk_area_short_name;
                        $veh_out_otp_flag = $json->veh_out_otp_flag; 
                        $veh_valid_flag = $json->veh_valid_flag;
                        if($veh_valid_flag==FLAG_Y){
                            $v_nu =v_numberChecker($user_veh_number);
                            $json = json_decode($v_nu);
                            if($json->status=='1'){
                                $veh_chek=FLAG_Y;
                            }else{
                                $veh_chek=FLAG_N;
                            }
                        }else{
                            $veh_chek=FLAG_Y;
                        }
                        if($veh_chek==FLAG_Y){
                            if ($veh_out_otp_flag==FLAG_Y) {
                                $veh_out_otp = mt_rand(100000,999999);
                                $otp_mes =", Vehicle Out OTP :".$veh_out_otp;
                            }                  
                            if($payment_type == DGP){
                                $sql = "SELECT `prk_sub_unit_id` FROM `prk_gate_pass_dtl` WHERE `prk_gate_pass_num` ='$gate_pass_num'";
                                $query  = $pdoconn->prepare($sql);
                                    $query->execute();
                                $val = $query->fetch();
                                $prk_sub_unit_id = $val['prk_sub_unit_id'];
                            }
                            if (!empty($user_mobile)) {
                                $draft_re = draft_registration($user_mobile,$user_veh_type,$user_veh_number);
                                $json = json_decode($draft_re);
                                if($json->status=='1'){ 
                                $user_admin_id=$json->user_admin_id;
                                $user_ac_status=$json->user_ac_status;     
                                $password=$json->password;                                
                                    if($user_ac_status== FLAG_D){
                                        draft_registration_sms($password,$user_mobile);
                                    }
                                }
                            }
                            $veh_trc = prk_veh_trc_dtl($prk_admin_id,$user_admin_id,$user_veh_type,$user_veh_number,$prk_in_rep_name,$prk_in_rep_name,$payment_type,$gate_pass_num,$prk_sub_unit_id,$advance_pay,$user_mobile,$prk_area_short_name,$veh_out_otp,$veh_token);
                            $json = json_decode($veh_trc);
                            $status=$json->status;
                            if($status=='1')
                            {
                                $prk_veh_trc_dtl_id=$json->prk_veh_trc_dtl_id;
                                $veh_in_date=$json->veh_in_date;
                                $veh_in_time=$json->veh_in_time;
                                if($sms_flag== FLAG_Y && !empty($user_mobile)){
                                    prk_veh_trn_in_mess($user_mobile,$veh_in_date,$veh_in_time,$user_veh_type,$user_veh_number,$prk_area_name,$otp_mes);
                                }
                                $response['status'] = 1;
                                $response['message'] = 'Vehicle IN Successful';
                                $response['prk_veh_trc_dtl_id'] = $prk_veh_trc_dtl_id;
                                $response = json_encode($response);
                            }else{
                                $response['status'] = 0;
                                $response['message'] = 'Not Sucessfull';
                                $response = json_encode($response);
                            }
                        }else{
                            $response = $v_nu;
                        }
                    }else{
                        $response['status'] = 0;
                        $response['message'] = 'Technical issue Info';
                        $response = json_encode($response);
                    }
                }else{
                  $response = $wheeler_co;
                }
            }else{
                $response = $prk_wh_co;
            }
        }else{ 
            $response = $token_check;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>