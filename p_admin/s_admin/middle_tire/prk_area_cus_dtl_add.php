<?php
/*
Description: Parking area login.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
if(isAvailable(array('prk_admin_id','kind_alt','gst_number','bill_to_add','shipped_to_add','payment_mode','par_month_rate','tax_type','bill_date','payment_due_day','service_name','discount','inserted_by'))){
    if(isEmpty(array())){
        // $prk_area_cus_dtl_id = trim($_POST['prk_area_cus_dtl_id']);
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $kind_alt = trim($_POST['kind_alt']);
        $gst_number = trim($_POST['gst_number']);
        $bill_to_add = trim($_POST['bill_to_add']);
        $shipped_to_add = trim($_POST['shipped_to_add']);
        $payment_mode = trim($_POST['payment_mode']);
        $par_month_rate = trim($_POST['par_month_rate']);
        $tax_type = trim($_POST['tax_type']);
        $bill_date = trim($_POST['bill_date']);
        $payment_due_day = trim($_POST['payment_due_day']);
        $service_name = trim($_POST['service_name']);
        $discount = trim($_POST['discount']);
        $inserted_by = trim($_POST['inserted_by']);
        $response = prk_area_cus_dtl_add($prk_admin_id,$kind_alt,$gst_number,$bill_to_add,$shipped_to_add,$payment_mode,$par_month_rate,$tax_type,$bill_date,$payment_due_day,$service_name,$discount,$inserted_by);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandator';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>