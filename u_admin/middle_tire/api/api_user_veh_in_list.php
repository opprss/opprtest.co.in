<?php
/*
Description: user licence list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_veh_in_list($user_admin_id){
    $response = array();
    $allplayerdata = array();
    $gate_pass = array();
    global $pdoconn;
    $sql = "SELECT `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id`,
        `prk_veh_trc_dtl`.`prk_admin_id`,
        `prk_veh_trc_dtl`.`prk_veh_type`,
        `prk_veh_trc_dtl`.`veh_number`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%D-%b-%Y') AS `veh_in_date`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%I:%i %p') AS `veh_in_time`,
        `vehicle_type`.`vehicle_type_dec`,
        `vehicle_type`.`vehicle_typ_img`,
        `prk_veh_trc_dtl`.`veh_out_otp`,
        `prk_veh_trc_dtl`.`veh_token`,
        `prk_area_dtl`.`prk_area_name`,
        `prk_area_dtl`.`prk_area_pro_img`,
        `prk_area_dtl`.`prk_area_logo_img`,
        `visitor_parking_dtl`.`vis_prk_longitude`,
        `visitor_parking_dtl`.`vis_prk_latitude`
        FROM `prk_veh_trc_dtl` JOIN `vehicle_type` JOIN `prk_area_dtl` LEFT JOIN `visitor_parking_dtl`
        ON `visitor_parking_dtl`.`prk_admin_id`=`prk_veh_trc_dtl`.`prk_admin_id`
        AND `visitor_parking_dtl`.`vis_prk_id`= `prk_veh_trc_dtl`.`gate_pass_num`
        AND `visitor_parking_dtl`.`active_flag`='".FLAG_Y."'
        AND `visitor_parking_dtl`.`del_flag`='".FLAG_N."'
        WHERE `prk_veh_trc_dtl`.`user_admin_id` = '$user_admin_id'
        AND `prk_veh_trc_dtl`.`prk_veh_out_time` is NULL
        AND `vehicle_type`.`active_flag` = '".FLAG_Y."'
        AND `prk_veh_trc_dtl`.`prk_veh_type` = `vehicle_type`.`vehicle_sort_nm`
        AND `prk_veh_trc_dtl`.`prk_admin_id`=`prk_area_dtl`.`prk_admin_id`
        AND `prk_area_dtl`.`active_flag`='".FLAG_Y."'
        order by `prk_veh_trc_dtl`.`prk_veh_in_time` DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){            
            $gate_pass['prk_veh_trc_dtl_id'] = $val['prk_veh_trc_dtl_id'];
            $gate_pass['prk_admin_id'] = $val['prk_admin_id'];
            $gate_pass['prk_veh_type'] = $val['prk_veh_type'];
            $gate_pass['veh_number'] = $val['veh_number'];
            $gate_pass['veh_in_date'] = $val['veh_in_date'];
            $gate_pass['veh_in_time'] = $val['veh_in_time'];
            $gate_pass['vehicle_type_dec'] = $val['vehicle_type_dec'];
            $gate_pass['vehicle_typ_img'] = $val['vehicle_typ_img'];
            $gate_pass['veh_out_otp'] = $val['veh_out_otp'];
            $gate_pass['veh_token'] = $val['veh_token'];
            $gate_pass['prk_longitude'] = $val['vis_prk_longitude'];
            $gate_pass['prk_latitude'] = $val['vis_prk_latitude'];
            array_push($allplayerdata, $gate_pass);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['user_veh_in_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
/*function user_veh_in_list($user_admin_id){
    $response = array();
    $allplayerdata = array();
    $gate_pass = array();
    global $pdoconn;
    $sql ="SELECT `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id`,
        `prk_veh_trc_dtl`.`prk_admin_id`,
        `prk_veh_trc_dtl`.`prk_veh_type`,
        `prk_veh_trc_dtl`.`veh_number`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%D-%b-%Y') AS `veh_in_date`,
        DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%I:%i %p') AS `veh_in_time`,
        `vehicle_type`.`vehicle_type_dec`,
        `vehicle_type`.`vehicle_typ_img`,
        `prk_veh_trc_dtl`.`veh_out_otp`,
        `prk_veh_trc_dtl`.`veh_token`,
        `prk_area_dtl`.`prk_area_name`,
        `prk_area_dtl`.`prk_area_pro_img`,
        `prk_area_dtl`.`prk_area_logo_img`
        FROM `prk_veh_trc_dtl` ,`vehicle_type`,`prk_area_dtl`
        WHERE `prk_veh_trc_dtl`.`user_admin_id` = '$user_admin_id'
        AND `prk_veh_trc_dtl`.`prk_veh_out_time` is NULL
        AND `vehicle_type`.`active_flag` = '".FLAG_Y."'
        AND `prk_veh_trc_dtl`.`prk_veh_type` = `vehicle_type`.`vehicle_sort_nm`
        AND `prk_veh_trc_dtl`.`prk_admin_id`=`prk_area_dtl`.`prk_admin_id`
        AND `prk_area_dtl`.`active_flag`='".FLAG_Y."'
        order by `prk_veh_trc_dtl`.`prk_veh_in_time` DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val)
        {            
            $response['status'] = 1;
            $response['message'] = 'Successful';
            $gate_pass['prk_veh_trc_dtl_id'] = $val['prk_veh_trc_dtl_id'];
            $gate_pass['prk_admin_id'] = $val['prk_admin_id'];
            $gate_pass['prk_veh_type'] = $val['prk_veh_type'];
            $gate_pass['veh_number'] = $val['veh_number'];
            $gate_pass['veh_in_date'] = $val['veh_in_date'];
            $gate_pass['veh_in_time'] = $val['veh_in_time'];
            $gate_pass['vehicle_type_dec'] = $val['vehicle_type_dec'];
            $gate_pass['vehicle_typ_img'] = $val['vehicle_typ_img'];
            $gate_pass['veh_out_otp'] = $val['veh_out_otp'];
            $gate_pass['veh_token'] = $val['veh_token'];
            array_push($allplayerdata, $gate_pass);
            $response['user_veh_in_list'] = $allplayerdata;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }

        
    return json_encode($response);
}*/
?>