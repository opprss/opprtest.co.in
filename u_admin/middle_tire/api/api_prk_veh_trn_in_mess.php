<?php
/*
Description: parking area in message
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :07-04-2018
*/ 
function prk_veh_trn_in_mess($user_mobile,$veh_in_date,$veh_in_time,$prk_veh_type,$veh_number,$prk_area_name){

    $text="Welcome to: ".$prk_area_name."   Vehicle Number: ".$veh_number." , Vehicle Type:".$prk_veh_type." ,Vehicle In Date: ".$veh_in_date." And In Time: ".$veh_in_time."  please visit http://www.opprss.com";
    
    mobile_massage($user_mobile,$text);
    $response['status'] = 1;
    $response['message'] = 'Sms Send Successful';
    return json_encode($response);
}
?>