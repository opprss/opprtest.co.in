<?php
/*
Description: Parking area particular one gate dtl show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_area_gate_dtl_show($prk_area_gate_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT * FROM `prk_area_gate_dtl` WHERE `prk_area_gate_id`='$prk_area_gate_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $park['prk_area_gate_id'] = $val['prk_area_gate_id'];
    $park['prk_area_gate_name'] = $val['prk_area_gate_name'];
    $park['prk_area_gate_dec'] = $val['prk_area_gate_dec'];
    $park['prk_area_gate_landmark'] = $val['prk_area_gate_landmark'];
    $park['prk_area_gate_type'] = $val['prk_area_gate_type'];
    $park['effective_date'] = $val['effective_date'];
    $park['end_date'] = $val['end_date'];
    return ($park);
}
?>