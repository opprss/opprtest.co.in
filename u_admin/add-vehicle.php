<!-- ################################################
  
Description: user can add vechile
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
include "all_nav/header.php"; 
if (!isset($_SESSION['user_id'])) {
  $user_admin_id = $_SESSION['user_admin_id'];
}
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>

<!-- header position -->

    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Add Vehicle</h5>
        <!--<form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form> search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card single col-md-12 pd-t-30 pd-r-30 pd-l-30 pd-b-0">
          <!-- <h6 class="card-body-title">Sign Up Here</h6>
          <p class="mg-b-20 mg-sm-b-30">A form with a label on top of each form control.</p> -->
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <div class="row ">
              <!-- <dir class="col-md-1">
                
              </dir> -->
              <div class="col-md-12">
                
                  <form id="modifyVehicle" method="post" action="">
                    <div class="form-layout">
                      <div class="row mg-b-25">
                        <div class="col-lg-2 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">vehicle type <span class="tx-danger">*</span></label>

                              <div class="select" style=" margin-top: -4px; font-size:14px">
                                <select name="user_veh_type" id="user_veh_type">
                                    <option value="">VEHICLE TYPE</option>
                                    <?php
                                        $response = array();
                                        $response =vehicleType();
                                        //print_r($response);
                                        $count = count($response);
                                        for ($i=0; $i < $count ; $i++) { 
                                       ?>
                                        <option value="<?php echo $response[$i]['vehicle_Sort_nm']?>" <?php if(@$v_type == $response[$i]['vehicle_Sort_nm']) echo 'selected=selected'; ?>"><?php echo $response[$i]['vehicle_type']?></option>

                                        <?php
                                        }
                                    ?>
                                </select>
                              </div>
                              <span style="position: absolute;" id="error_user_veh_type" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-2 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">vehicle number <span class="tx-danger">*</span></label>

                            <input type="text" class="form-control" placeholder="Vehicle Number" name="user_veh_number" id="user_veh_number" onkeyup="vehNoUppercase()" maxlength="10">
                            <span style="position: absolute;" id="error_user_veh_number" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-2 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Registration Number</label>

                            <input type="text" class="form-control" placeholder="Registration No." name="user_veh_reg" id="user_veh_reg" onkeyup= "upperCase(this)">
                            <span style="position: absolute;" id="error_user_veh_reg" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-2 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Reg. Expiry Date</label>

                            <input type="text" name="user_veh_reg_exp_dt" id="user_veh_reg_exp_dt" placeholder="Reg Expiry Date" class="form-control readonly" readonly="readonly" style="background-color: transparent;" disabled>
                            <span style="position: absolute;" id="error_user_veh_reg_exp_dt" class="error_size"></span>
                          </div>
                        </div>

                        <div class="col-md-2">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Rc Image<span class="tx-danger"></span></label>
                            <input type="file" name="rc_img" accept="image/*" id="rc_img" style="font-size: 13px;">
                            <input type="hidden" name="web_rc_img" class="web_rc_img" id="web_rc_img">
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <div id="results5">Image Show Here..</div>
                          </div>
                        </div>
                        
                        <div class="col-lg-3">
                          <div class="form-group">
                            <button type="button" class="btn btn-block user_button_skip" id="insu_btn">
                              <i class="fa fa-plus mg-r-5"></i>Insurance
                            </button>
                          </div>
                          <div class="insu_block">
                              <div class="form-group">
                              <label class="form-control-label user-lebel">Insurance Number </label>

                                <input type="text" class="form-control" placeholder="Insurance Number" name="user_ins_number" id="user_ins_number" value="<?php if(isset($user_ins_number)) echo $user_ins_number;?>"  onkeydown="upperCase(this)">
                                <span style="position: absolute;" id="error_user_ins_number" class="error_size"></span>
                              </div>
                              <div class="form-group">
                                <label class="form-control-label user-lebel">Insurance Expiry Date</label>

                                <input type="text" name="user_ins_exp_dt" id="user_ins_exp_dt" placeholder="Insurance Expiry Date" class="form-control readonly" readonly="readonly" style="background-color: transparent;" disabled>
                                <span style="position: absolute;" id="error_user_ins_exp_dt" class="error_size"></span>
                              </div>
                              <div class="form-group">
                                <input type="file" name="insu_img" accept="image/*" id="insu_img" style="font-size: 13px;">
                                <input type="hidden" name="web_ins_img" class="web_ins_img" id="web_ins_img">
                              </div>
                              <div class="form-group">
                                <div id="results">captured Insurence image will appear here...</div>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-3">
                          <div class="form-group">
                            <button type="button" class="btn btn-block user_button_skip" id="emud_btn">
                              <i class="fa fa-plus mg-r-5"></i>PUC
                            </button>
                          </div>
                          <div class="emud_block">
                              <div class="form-group">
                            <label class="form-control-label user-lebel">PUC Number </label>

                                <input type="text" class="form-control" placeholder="PUC Number" name="user_emu_number" id="user_emu_number" value="<?php if(isset($user_emu_number)) echo $user_emu_number;?>"  onkeydown="upperCase(this)">
                                <span style="position: absolute;" id="error_user_emu_number" class="error_size"></span>
                              </div>

                              <div class="form-group">
                                <label class="form-control-label user-lebel">PUC Expiry Date</label>

                                <input type="text" name="user_emu_exp_dt" id="user_emu_exp_dt" placeholder="PUC Expiry Date" class="form-control readonly" value="<?php if(isset($user_emu_exp_dt)) echo $user_emu_exp_dt;?>"  style="background-color: transparent;" disabled>
                                <span style="position: absolute;" id="error_user_emu_exp_dt" class="error_size"></span>
                              </div>
                              <div class="form-group">
                                <input type="file" name="puc_img" accept="image/*" id="puc_img" style="font-size: 13px;">
                                <input type="hidden" name="web_emu_img" class="web_emu_img" id="web_emu_img">
                              </div>
                              <div class="form-group">
                                <div id="results2">captured PUC image will appear here...</div>
                              </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                          <div class="form-group">
                            <button type="button" class="btn btn-block user_button_skip" id="othr_docu_1">
                              <i class="fa fa-plus mg-r-5"></i>OTHER DOCUMENT 1
                            </button>
                          </div>
                          <div class="othr_docu_1_block">
                              <div class="form-group">
                            <label class="form-control-label user-lebel">Other Documentation Number 1</label>

                                <input type="text" class="form-control" placeholder="Document Number" name="user_othr_number_1" id="user_othr_number_1" value=""  onkeydown="upperCase(this)">
                                <span style="position: absolute;" id="error_user_othr_number_1" class="error_size"></span>
                              </div>

                              <div class="form-group">
                                <label class="form-control-label user-lebel">Other Documentation 1 Expiry Date</label>

                                <input type="text" name="user_othr_number_1_exp_dt" id="user_othr_number_1_exp_dt" placeholder="Document Expiry Date" class="form-control readonly" value=""  style="background-color: transparent;" disabled>
                                <span style="position: absolute;" id="error_user_othr_number_1_exp_dt" class="error_size"></span>
                              </div>
                              <div class="form-group">
                                <input type="file" name="docu1_img" accept="image/*" id="docu1_img" style="font-size: 13px;">
                                <input type="hidden" name="web_oth_doc_no1_img" class="web_oth_doc_no1_img" id="web_oth_doc_no1_img">
                              </div>
                              <div class="form-group">
                                <div id="results3">captured Document image will appear here...</div>
                              </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                          <div class="form-group">
                            <button type="button" class="btn btn-block user_button_skip" id="othr_docu_2">
                              <i class="fa fa-plus mg-r-5"></i>OTHER DOCUMENT 2
                            </button>
                          </div>
                          <div class="othr_docu_2_block">
                              <div class="form-group">
                            <label class="form-control-label user-lebel">Other Documentation Number 2</label>

                                <input type="text" class="form-control" placeholder="Document Number" name="user_othr_number_2" id="user_othr_number_2" value="<?php if(isset($user_emu_number)) echo $user_emu_number;?>"  onkeydown="upperCase(this)">
                                <span style="position: absolute;" id="error_user_othr_number_1" class="error_size"></span>
                              </div>

                              <div class="form-group">
                                <label class="form-control-label user-lebel">Other Documentation 2 Expiry Date</label>

                                <input type="text" name="user_othr_number_1_exp_dt" id="user_othr_number_2_exp_dt" placeholder="Document Expiry Date" class="form-control readonly" value="<?php if(isset($user_emu_exp_dt)) echo $user_emu_exp_dt;?>"  style="background-color: transparent;" disabled>
                                <span style="position: absolute;" id="error_user_othr_number_2_exp_dt" class="error_size"></span>
                              </div>
                              <div class="form-group">
                                <input type="file" name="docu2_img" accept="image/*" id="docu2_img" style="font-size: 13px;">
                                <input type="hidden" name="web_oth_doc_no2_img" class="web_oth_doc_no2_img" id="web_oth_doc_no2_img">
                              </div>
                              <div class="form-group">
                                <div id="results4">captured Document image will appear here...</div>
                              </div>
                            </div>
                        </div>


                        <div class="col-lg-2">
                          <div class="form-group">
                            <input type="button" value="SAVE" class="btn btn-block btn-primary user_button" name="save" id="save">
                          </div>
                        </div>
                        <div class="col-lg-2">
                          <div class="form-group">
                            <button type="button" class="btn btn-block user_button_skip" name="skip" id="skip" onclick="document.location.href='vehicle-details';">Back</button>
                          </div>
                        </div>
                    </div><!-- form-layout -->
                  </form>
              </div>
              
            </div>
          </div>
        </div><!-- card -->
      </div><!-- am-pagebody -->
      

<!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">

  var userUrl = "<?php echo USER_URL; ?>";


        $( document ).ready( function () {
            /*FORM SUBMIT*/
            $('#save').click(function(){

              var user_veh_type = $('#user_veh_type').val();
              var user_veh_number = $('#user_veh_number').val();
              var user_veh_reg = $('#user_veh_reg').val();
              var user_veh_reg_exp_dt = $('#user_veh_reg_exp_dt').val();
              var web_rc_img = $('#web_rc_img').val();

              var user_ins_number = $('#user_ins_number').val();
              var user_ins_exp_dt = $('#user_ins_exp_dt').val();
              var web_ins_img = $('#web_ins_img').val();

              var user_emu_number = $('#user_emu_number').val();
              var user_emu_exp_dt = $('#user_emu_exp_dt').val();
              var web_emu_img = $('#web_emu_img').val();

              var oth_doc_no1 = $('#user_othr_number_1').val();
              var oth_doc_no1_exp_dt = $('#user_othr_number_1_exp_dt').val();
              var web_oth_doc_no1_img = $('#web_oth_doc_no1_img').val();

              var oth_doc_no2 = $('#user_othr_number_2').val();
              var oth_doc_no2_exp_dt = $('#user_othr_number_2_exp_dt').val();
              var web_oth_doc_no2_img = $('#web_oth_doc_no2_img').val();

              var user_admin_id = "<?php echo $user_admin_id; ?>";
              var mobile = "<?php echo $user_mobile; ?>";
              var token = "<?php echo $token; ?>";

              var form_data = new FormData();
              form_data.append('user_veh_type', user_veh_type);
              form_data.append('user_veh_number', user_veh_number);
              form_data.append('user_veh_reg', user_veh_reg);
              form_data.append('user_veh_reg_exp_dt', user_veh_reg_exp_dt);
              form_data.append('web_rc_img', web_rc_img);

              form_data.append('user_ins_number', user_ins_number);
              form_data.append('user_ins_exp_dt', user_ins_exp_dt);
              form_data.append('web_ins_img', web_ins_img);

              form_data.append('user_emu_number', user_emu_number);
              form_data.append('user_emu_exp_dt', user_emu_exp_dt);
              form_data.append('web_emu_img', web_emu_img);

              form_data.append('oth_doc_no1', oth_doc_no1);
              form_data.append('oth_doc_no1_exp_dt', oth_doc_no1_exp_dt);
              form_data.append('web_oth_doc_no1_img', web_oth_doc_no1_img);

              form_data.append('oth_doc_no2', oth_doc_no2);
              form_data.append('oth_doc_no2_exp_dt', oth_doc_no2_exp_dt);
              form_data.append('web_oth_doc_no2_img', web_oth_doc_no2_img);

              form_data.append('user_admin_id', user_admin_id);
              form_data.append('mobile', mobile);
              form_data.append('token', token);


		          if(user_veh_reg_exp_dt!=''){
                $('#error_user_veh_reg_exp_dt').text(' ');
              }
              if(user_veh())
              if(user_veh_type!='' && user_veh_number!=''){
                //submit

                var urlVehNoCheck = userUrl+'v_numberChecker.php';

                $.ajax({
                  url :urlVehNoCheck,
                  type:'POST',
                  data :
                  {
                    'v_number':user_veh_number
                  },
                  dataType:'html',
                  success  :function(data)
                  {
                    //alert(data);
                    var json = $.parseJSON(data);
                    if (json.status) {
                      if (checkInsu() && checkEmud()) {
                        var urlAddVeh = userUrl+'add_vehicle.php';
                        $('#save').val('Wait ...').prop('disabled', true);
                        $.ajax({
                            url :urlAddVeh,
                            type:'POST',
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,                         
                            dataType:'html',
                            success  :function(data)
                            {
                              //alert(data);
                              $('#save').val('SAVE').prop('disabled', false);
                              var json = $.parseJSON(data);

                                if (json.status) {
                                  $.alert({
                                    icon: 'fa fa-smile-o',
                                    theme: 'modern',
                                    title: 'Success !',
                                    content: "<p style='font-size:0.8em;'>Vehicle added successfully</p>",
                                    type: 'green',
                                    buttons: {
                                      Ok: function () {
                                          window.location='vehicle-details';
                                      }
                                    }
                                  });
                                }else{
                                  if( typeof json.session !== 'undefined'){
                                    if (!json.session) {
                                      window.location.replace("logout.php");
                                    }
                                  }else{
                                    $.alert({
                                    icon: 'fa fa-frown-o',
                                    theme: 'modern',
                                    title: 'Error !',
                                    content: "<p style='font-size:0.8em;'>"+json.message+"</p>",
                                    type: 'red'
                                  });
                                  }
                                }
                            }
                        });
                      }else{
                        if (!checkInsu()) {
                          $('#error_user_ins_exp_dt').text('Required date');
                        }if (!checkEmud()) {
                          $('#error_user_emu_exp_dt').text('Required date');
                        }if (!checkdocu1()) {
                          $('#error_user_othr_number_1_exp_dt').text('Required date');
                        }if (!checkdocu2()) {
                          $('#error_user_othr_number_2_exp_dt').text('Required date');
                        }
                        return false;
                      }
                    }else{
                      $('#error_user_veh_number').text('Vehicle number is invalid');
                    }
                  }
                });                      
              }else{
                if (user_veh_type == '') {
                $('#error_user_veh_type').text('Required vehicle type');
                }if (user_veh_number == '') {
                  $('#error_user_veh_number').text('Required vehicle number');
                }
                return false;
              }
            });
             

            function checkInsu(){
              var user_ins_number = $('#user_ins_number').val();
              var user_ins_exp_dt = $('#user_ins_exp_dt').val();
              if (user_ins_number  != '') {
                if (user_ins_exp_dt != '') {
                  return true;
                }else{
                  $('#error_user_ins_exp_dt').text('Required date');
                  return false;
                }
              }else{
                return true;
              }
            }

            function checkEmud(){
              var user_emu_number = $('#user_emu_number').val();
              var user_emu_exp_dt = $('#user_emu_exp_dt').val();

              if (user_emu_number  != '') {
                if (user_emu_exp_dt != '') {
                  return true;
                }else{
                  $('#error_user_emu_exp_dt').text('Required date');
                  return false;
                }
              }else{
                return true;
              }
            }
            function checkdocu1(){
              var user_othr_number_1 = $('#user_othr_number_1').val();
              var user_othr_number_1_exp_dt = $('#user_othr_number_1_exp_dt').val();

              if (user_othr_number_1  != '') {
                if (user_othr_number_1_exp_dt != '') {
                  return true;
                }else{
                  $('#error_user_othr_number_1_exp_dt').text('Required date');
                  return false;
                }
              }else{
                return true;
              }
            }
            function checkdocu2(){
              var user_othr_number_2 = $('#user_othr_number_2').val();
              var user_othr_number_2_exp_dt = $('#user_othr_number_2_exp_dt').val();

              if (user_othr_number_2  != '') {
                if (user_othr_number_2_exp_dt != '') {
                  return true;
                }else{
                  $('#error_user_othr_number_2_exp_dt').text('Required date');
                  return false;
                }
              }else{
                return true;
              }
            }

            function user_veh(){
              var user_veh_reg = $('#user_veh_reg').val();
              var user_veh_reg_exp_dt = $('#user_veh_reg_exp_dt').val();

              if (user_veh_reg  != '') {
                if (user_veh_reg_exp_dt != '') {
                  return true;
                }else{
                  $('#error_user_veh_reg_exp_dt').text('Required Date');
                  return false;
                }
              }else{
                return true;
              }
            }

            $("#user_veh_type").blur(function () {
              var user_veh_type=$('#user_veh_type').val();
              if(user_veh_type==''){
                $('#error_user_veh_type').text('Required type');
              $('#user_veh_type').focus();

              }else{
                $('#error_user_veh_type').text('');
              }
            });

            /*-----------------------------*/

            $("#user_veh_number").blur(function(){
              var veh_number = $('#user_veh_number').val();
              IsVehNumber(veh_number);
            });
            function IsVehNumber(veh_number) {
                var urlVehNoCheck = userUrl+'v_numberChecker.php';

                $.ajax({
                  url :urlVehNoCheck,
                  type:'POST',
                  data :
                  {
                    'v_number':veh_number
                  },
                  dataType:'html',
                  success  :function(data)
                  {
                    //alert(data);
                    var json = $.parseJSON(data);
                    vehNumberReturn(json.status);
                  }
                });
            } 
            function vehNumberReturn(flag){
              if(!flag){
                  $('#error_user_veh_number').text('Vehicle number is invalid');
                  //return false;
              }else{
                  $('#error_user_veh_number').text('');
                  //return true;
              }
            }
            /*-----------------------------------*/
            $("#user_ins_number").on('change keyup paste', function() {

              var user_veh_reg=$('#user_ins_number').val();

              if(user_veh_reg != ''){
                $("#user_ins_exp_dt").prop("disabled", false);
                $("#user_ins_exp_dt").prop("readonly", true);
                $('#user_ins_exp_dt').addClass('prevent_readonly').removeClass('readonly');
              }else{
                $("#user_ins_exp_dt").prop("disabled", true);
                $('#user_ins_exp_dt').val('');
                $('#user_ins_exp_dt').addClass('readonly').removeClass('prevent_readonly');
                
              }
            });

            $("#user_emu_number").on('change keyup paste', function() {

              var user_emu_number = $('#user_emu_number').val();

              if(user_emu_number != ''){
                $("#user_emu_exp_dt").prop("disabled", false);
                $("#user_emu_exp_dt").prop("readonly", true);
                $('#user_emu_exp_dt').addClass('prevent_readonly').removeClass('readonly');

              }else{
                $("#user_emu_exp_dt").prop("disabled", true);
                $('#user_emu_exp_dt').val('');
                $('#user_emu_exp_dt').addClass('readonly').removeClass('prevent_readonly');
              }
            });

            $("#user_othr_number_1").on('change keyup paste', function() {

              var user_othr_number_1 = $('#user_othr_number_1').val();

              if(user_othr_number_1 != ''){
                $("#user_othr_number_1_exp_dt").prop("disabled", false);
                $("#user_othr_number_1_exp_dt").prop("readonly", true);
                $('#user_othr_number_1_exp_dt').addClass('prevent_readonly').removeClass('readonly');

              }else{
                $("#user_othr_number_1_exp_dt").prop("disabled", true);
                $('#user_othr_number_1_exp_dt').val('');
                $('#user_othr_number_1_exp_dt').addClass('readonly').removeClass('prevent_readonly');
              }
            });

            $("#user_othr_number_2").on('change keyup paste', function() {

              var user_othr_number_2 = $('#user_othr_number_2').val();

              if(user_othr_number_2 != ''){
                $("#user_othr_number_2_exp_dt").prop("disabled", false);
                $("#user_othr_number_2_exp_dt").prop("readonly", true);
                $('#user_othr_number_2_exp_dt').addClass('prevent_readonly').removeClass('readonly');

              }else{
                $("#user_othr_number_2_exp_dt").prop("disabled", true);
                $('#user_othr_number_2_exp_dt').val('');
                $('#user_othr_number_2_exp_dt').addClass('readonly').removeClass('prevent_readonly');
              }
            });

            $("#user_ins_exp_dt").blur(function () {
              var user_ins_exp_dt=$('#user_ins_exp_dt').val();
              if(user_ins_exp_dt==''){
                $('#error_user_ins_exp_dt').text('Required date');
              //$('#user_ins_exp_dtt').focus();

              }else{
                $('#error_user_ins_exp_dt').text('');
              }
            });

            $("#user_emu_exp_dt").blur(function () {
              var user_emu_exp_dt=$('#user_emu_exp_dt').val();
              if(user_emu_exp_dt==''){
                $('#error_user_emu_exp_dt').text('Required date');
              $('#user_emu_exp_dt').focus();

              }else{
                $('#error_user_emu_exp_dt').text('');
              }
            });

            $("#user_othr_number_1_exp_dt").blur(function () {
              //alert("ok");
              var user_othr_number_1_exp_dt=$('#user_othr_number_1_exp_dt').val();
              if(user_othr_number_1_exp_dt==''){
                $('#error_user_othr_number_1_exp_dt').text('Required date');
              $('#user_othr_number_1_exp_dt').focus();

              }else{
                $('#error_user_othr_number_1_exp_dt').text('');
              }
            });

            $("#user_othr_number_2_exp_dt").blur(function () {
              var user_othr_number_2_exp_dt=$('#user_othr_number_2_exp_dt').val();
              if(user_othr_number_2_exp_dt==''){
                $('#error_user_othr_number_2_exp_dt').text('Required date');
              $('#user_othr_number_2_exp_dt').focus();

              }else{
                $('#error_user_othr_number_2_exp_dt').text('');
              }
            });



            $("#user_veh_reg").on('change keyup paste', function()  {

              var user_veh_reg = $('#user_veh_reg').val();

              if(user_veh_reg != ''){
                $("#user_veh_reg_exp_dt").prop("disabled", false);
                $('#user_veh_reg_exp_dt').addClass('prevent_readonly').removeClass('readonly');

                //$("#user_emu_exp_dt").prop("readonly", true);
              }else{
                $("#user_veh_reg_exp_dt").prop("disabled", true);
                $('#user_veh_reg_exp_dt').val('');
                $('#user_veh_reg_exp_dt').addClass('readonly').removeClass('prevent_readonly');

              }
            });

          } );


        /* end velidation*/

        /*UI*/
        $( document ).ready( function () {
           $(".insu_block").hide();
          $(".emud_block").hide();
          $(".othr_docu_1_block").hide();
          $(".othr_docu_2_block").hide();

          $("#insu_btn").click(function(){
            $(".insu_block").slideToggle("slow");
            //$("#emud_block").hide("slow");
          });

          $("#emud_btn").click(function(){
            $(".emud_block").slideToggle("slow");
            //$("#insu_block").hide("slow");
          });

          $("#othr_docu_1").click(function(){
            $(".othr_docu_1_block").slideToggle("slow");
            //$("#insu_block").hide("slow");
          });

          $("#othr_docu_2").click(function(){
            $(".othr_docu_2_block").slideToggle("slow");
            //$("#insu_block").hide("slow");
          });

        } );

         /* uppercase*/
        function vehNoUppercase() {
          var x = document.getElementById("user_veh_number");
          x.value = x.value.toUpperCase();
        }

        function upperCase(a){
            setTimeout(function(){
                a.value = a.value.toUpperCase();
            }, 1);
        }
                
        /*date*/

        addEventListener('DOMContentLoaded', function () {

          var now = new Date;
          var now = now.setDate(now.getDate() - 1);
          pickmeup('#user_veh_reg_exp_dt', {
            default_date   : false,
            position       : 'bottom',
            hide_on_select : true,
            render : function (date) {
              if (date < now) {
                  return {
                    disabled : true,
                    class_name : 'date-in-past'
                  };
              }
              $('#error_user_veh_reg_exp_dt').text('');
              return {};
            } 
          });

          pickmeup('#user_ins_exp_dt', {
            default_date   : false,
            position       : 'top',
            hide_on_select : true,
            render : function (date) {
                  if (date < now) {
                    return {
                      disabled : true,
                      class_name : 'date-in-past'
                    };
                  }
                  $('#error_user_ins_exp_dt').text('');
                  return {};
              } 
          });

          pickmeup('#user_emu_exp_dt', {
            default_date   : false,
            position       : 'top',
            hide_on_select : true,
            render : function (date) {
                  if (date < now) {
                      return {
                        disabled : true,
                        class_name : 'date-in-past'
                      };
                  }
                  $('#error_user_emu_exp_dt').text('');
                  return {};
              } 
          });

          pickmeup('#user_othr_number_1_exp_dt', {
            default_date   : false,
            position       : 'top',
            hide_on_select : true,
            render : function (date) {
                  if (date < now) {
                      return {
                        disabled : true,
                        class_name : 'date-in-past'
                      };
                  }
                  $('#error_user_othr_number_1_exp_dt').text('');
                  return {};
              } 
          });

          pickmeup('#user_othr_number_2_exp_dt', {
            default_date   : false,
            position       : 'top',
            hide_on_select : true,
            render : function (date) {
                  if (date < now) {
                      return {
                        disabled : true,
                        class_name : 'date-in-past'
                      };
                  }
                  $('#error_user_othr_number_2_exp_dt').text('');
                  return {};
              } 
          });
        });

    </script>
    <script type="text/javascript">
      $(function() {
        $("#insu_img").change(function() {
          //$("#message").empty(); // To remove the previous error message
          var file = this.files[0];
          var imagefile = file.type;
          var match= ["image/jpeg","image/png","image/jpg"];
          if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
          {
          return false;
          }
          else
          {
          var reader = new FileReader();
          reader.onload = imageIsLoaded_insu;
          reader.readAsDataURL(this.files[0]);
          }
        });
      $("#puc_img").change(function() {
          //$("#message").empty(); // To remove the previous error message
          var file = this.files[0];
          var imagefile = file.type;
          var match= ["image/jpeg","image/png","image/jpg"];
          if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
          {
          return false;
          }
          else
          {
          var reader = new FileReader();
          reader.onload = imageIsLoaded_puc;
          reader.readAsDataURL(this.files[0]);
          }
        });
      $("#docu1_img").change(function() {
          //$("#message").empty(); // To remove the previous error message
          var file = this.files[0];
          var imagefile = file.type;
          var match= ["image/jpeg","image/png","image/jpg"];
          if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
          {
          return false;
          }
          else
          {
          var reader = new FileReader();
          reader.onload = imageIsLoaded_docu1;
          reader.readAsDataURL(this.files[0]);
          }
        });
      $("#docu2_img").change(function() {
          //$("#message").empty(); // To remove the previous error message
          var file = this.files[0];
          var imagefile = file.type;
          var match= ["image/jpeg","image/png","image/jpg"];
          if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
          {
          return false;
          }
          else
          {
          var reader = new FileReader();
          reader.onload = imageIsLoaded_docu2;
          reader.readAsDataURL(this.files[0]);
          }
        });
      });
      function imageIsLoaded_insu(e) {
        $(".web_ins_img").val(e.target.result);
        document.getElementById('results').innerHTML = '<img style="height: 100px;width: 234px;" src="'+e.target.result+'"/>';
      };
      function imageIsLoaded_puc(e) {
        $(".web_emu_img").val(e.target.result);
        document.getElementById('results2').innerHTML = '<img style="height: 100px;width: 234px;" src="'+e.target.result+'"/>';
      };
      function imageIsLoaded_docu1(e) {
        $(".web_oth_doc_no1_img").val(e.target.result);
        document.getElementById('results3').innerHTML = '<img style="height: 100px;width: 234px;" src="'+e.target.result+'"/>';
      };
      function imageIsLoaded_docu2(e) {
        $(".web_oth_doc_no2_img").val(e.target.result);
        document.getElementById('results4').innerHTML = '<img style="height: 100px;width: 234px;" src="'+e.target.result+'"/>';
      };
    </script>
    <script type="text/javascript">
      $(function() {
        $("#rc_img").change(function() {
          //$("#message").empty(); // To remove the previous error message
          var file = this.files[0];
          var imagefile = file.type;
          var match= ["image/jpeg","image/png","image/jpg"];
          if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
          {
          return false;
          }
          else
          {
          var reader = new FileReader();
          reader.onload = imageIsLoaded;
          reader.readAsDataURL(this.files[0]);
          }
        });
      });
      function imageIsLoaded(e) {
        $(".web_rc_img").val(e.target.result);
        document.getElementById('results5').innerHTML = '<img style="height: 70px;width: 145px;" src="'+e.target.result+'"/>';
      };
    </script>