<?php 
/*
Description: all api include
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
@session_start();
@include_once '../../../global_api/global_api.php';
@include_once '../../global_api/global_api.php';
@include_once '../global_api/global_api.php';
@include_once 'global_api/global_api.php';
@include_once'../../../global_asset/config.php';
@include_once'../../global_asset/config.php';
@include_once'../global_asset/config.php';
@include_once'global_asset/config.php';
@include_once '../../../common_api/common_api.php';
@include_once '../../common_api/common_api.php';
@include_once '../common_api/common_api.php';
@include_once 'common_api.php';

include_once 'api_prk_area_user_login.php';
include_once 'api_prk_area_super_user_dtl.php';
include_once 'api_prk_area_gate_dtl_list.php';
include_once 'api_prk_user_gate_login.php';
include_once 'api_prk_user_gate_logout.php';
include_once 'api_prk_veh_trc_dtl.php';
include_once 'api_user_prk_veh_verify_out.php';
include_once 'api_prk_veh_trc_reject.php';
include_once 'api_prk_user_logout.php';
include_once 'api_user_payment_received_employ.php';
include_once 'api_emp_pay_pdf_dwn.php';
include_once 'api_user_veh_prk_verify_out_status.php';
include_once 'api_payment_dtl_show.php';
include_once 'api_qr_code_division.php';
include_once 'api_prk_user_token_check.php';
include_once 'api_emp_payment_report_download.php';
include_once 'api_prk_visit_dtl.php';
include_once 'api_prk_visit_dtl_out.php';
include_once 'api_file_delete.php';
include_once 'api_prk_gate_pass_find.php';
include_once 'api_prk_veh_trn_in_mess.php';
include_once 'api_veh_qr_code_gen.php';
include_once 'api_prk_payment_undifine_update.php';
include_once 'api_gate_pass_match.php';
include_once 'api_visit_in_list_pdf.php';
include_once 'api_prk_veh_type.php';
include_once 'api_prk_area_advance_status.php';
include_once 'api_cash_memo_in.php';
include_once 'api_cash_memo_out.php';
include_once 'api_veh_prk_estimation.php';
include_once 'api_veh_prk_estimation_list.php';
include_once 'api_prk_area_rate_find_list.php';
include_once 'api_duplicate_payment_dtl_list.php';
include_once 'api_veh_prk_est_print.php';
include_once 'api_veh_prk_est_print.php';
include_once 'api_prk_area_rate_find_list_hda.php';
include_once 'api_prk_user_gate_login_check.php';
include_once 'api_user_prk_veh_verify_show.php';
include_once 'api_user_pay_pdf_dwn.php';
include_once 'api_meet_to_user.php';
include_once 'api_visitor_gate_pass_find.php';
include_once 'api_employee_attendance_in.php';
include_once 'api_emp_page_permissions_list.php';
include_once 'api_vehicle_audit_check_add.php';
include_once 'api_vehicle_audit_check_list.php';
include_once 'api_vehicle_number_search.php';
include_once 'api_visitor_mobile_search.php';
?>