<?php 
    /*
    Description: Parking employ today particular payment type transition
    Developed by: Rakhal Raj Mandal
    Created Date: -------
    Update date :30-03-2018
    */ 
    @include_once "../../../library/fpdf/fpdf.php"; 
    @include_once "../../library/fpdf/fpdf.php"; 
    @include_once "../../../global_asset/config.php";
    @include_once "../../global_asset/config.php";
    @include_once "../../../global_api/global_api.php";
    @include_once "../../global_api/global_api.php";
    @$prk_admin_id = trim($_POST['prk_admin_id']);
    //echo "hllo";
    //die();
    $prk_admin_id= $_POST['prk_admin_id'];
    $payment_rec_emp_name= $_POST['payment_rec_emp_name'];
    $payment_type= $_POST['payment_type'];
    $insert_by=$_POST['insert_by'];
    $start_date=$_POST['start_date'];
    $end_date=$_POST['end_date'];
    global $pdoconn;
    if(!empty($start_date) && !empty($end_date)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($start_date)) == date($start_date) && date($format, strtotime($end_date)) == date($end_date)) {
            $start_date= $start_date;
            $end_date=  $end_date ;
        } else {
            $start_date = date("Y-m-d", strtotime($start_date));
            $end_date = date("Y-m-d", strtotime($end_date));
        }
    }else{
        DATE;
        $end_date= TIME_TRN;
        $start_date= TIME_TRN;
    }
    $respons= array();
    $resp = array();
    $re = array();
    $allplayerdata = array();
    $total_pay=0;
    $total_veh=0;
    switch ($payment_type) {
        case "all":
            $w2= CASH;
            $w3= WALLET;
            $w4= DGP;
            $w8= TDGP;
            break;
        case CASH:
            $w2= CASH;
            $w3= 'NA';
            $w4= 'NA';
            $w8= 'NA';
            break;
        case WALLET:
            $w2= 'NA';
            $w3= WALLET;
            $w4= 'NA';
            $w8= 'NA';
            break;
        case DGP:
            $w2= 'NA';
            $w3= 'NA';
            $w4= DGP;
            $w8= 'NA';
            break;
        case TDGP:
            $w2= 'NA';
            $w3= 'NA';
            $w4= 'NA';
            $w8= TDGP;
            break;
        default:
            $w2= 'NA';
            $w3= 'NA';
            $w4= 'NA';
            $w8= 'NA';
    }
    $sql = "SELECT  x.prk_veh_type,
                    x.veh_number,
                    x.payment_amount,
                    x.payment_rec_date,
                    x.payment_rec_time,
                    x.payment_type,
                    x.vehicle_type_dec  ,
                    x.vehicle_typ_img
            FROM (
            SELECT  `prk_veh_trc_dtl`.`prk_admin_id`,
                    `payment_receive`.`payment_rec_by` as prk_in_rep_name,
                    `prk_veh_trc_dtl`.`prk_veh_type`,
                    `prk_veh_trc_dtl`.`veh_number`,
                    `payment_receive`.`payment_amount` ,
                    DATE_FORMAT(`payment_receive`.`payment_rec_date`,'%D-%M-%Y %I:%i %p') AS `payment_rec_date`,
                    DATE_FORMAT(`payment_receive`.`payment_rec_date`,'%I:%i %p') AS `payment_rec_time`,
                    (CASE `payment_receive`.`payment_type` WHEN '".CASH."' ThEN '".CASH."' WHEN '".WALLET."' THEN '".WALLET."' WHEN '".TDGP."' THEN '".TDGP_F."' WHEN '".DGP."' THEN '".DGP_F."' WHEN '".PP."' THEN '".CASH."' end) as payment_type,
                    `vehicle_type`.`vehicle_type_dec`,
                    `vehicle_type`.`vehicle_typ_img`
                    FROM `prk_veh_trc_dtl`, `payment_receive`, `vehicle_type`
                    WHERE `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id` = `payment_receive`.`prk_veh_trc_dtl_id`
                    AND   `prk_veh_trc_dtl`.`prk_veh_type` = `vehicle_type`.`vehicle_sort_nm`
                    AND   `payment_receive`.`payment_type` IN ('$w2', '$w3')
                    AND   `vehicle_type`.`active_flag`='".FLAG_Y."'
                    AND DATE_FORMAT(`payment_receive`.`payment_rec_date`,'%Y-%m-%d')  BETWEEN '$start_date' AND '$end_date'
                    UNION         
                    SELECT  `prk_veh_trc_dtl`.`prk_admin_id`,
                    `prk_veh_trc_dtl`.`prk_out_rep_name` as prk_in_rep_name,
                    `prk_veh_trc_dtl`.`prk_veh_type`,
                    `prk_veh_trc_dtl`.`veh_number`,
                    0 AS payment_amount ,
                    DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%D-%M-%Y %I:%i %p') AS `payment_rec_date`,
                    DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%I:%i %p') AS `payment_rec_time`,
                    (CASE `prk_veh_trc_dtl`.`payment_type` WHEN '".CASH."' ThEN '".CASH."' WHEN '".WALLET."' THEN '".WALLET."' WHEN '".TDGP."' THEN '".TDGP_F."' WHEN '".DGP."' THEN '".DGP_F."' WHEN '".PP."' THEN '".CASH."' end) as payment_type,
                    `vehicle_type`.`vehicle_type_dec`,
                    `vehicle_type`.`vehicle_typ_img`
                    FROM `prk_veh_trc_dtl`,  `vehicle_type`
                    WHERE `prk_veh_trc_dtl`.`prk_veh_type` = `vehicle_type`.`vehicle_sort_nm`
                     AND   `prk_veh_trc_dtl`.`payment_type` IN ('$w4', '$w8')
                     AND   `vehicle_type`.`active_flag`='".FLAG_Y."'
                    AND DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%Y-%m-%d')  BETWEEN '$start_date' AND '$end_date'
            ) x
            WHERE    prk_admin_id ='$prk_admin_id'
            AND prk_in_rep_name ='$payment_rec_emp_name'
            ORDER BY ( CASE payment_type WHEN 'CASH' THEN 1 WHEN 'WALLET' THEN 2 WHEN 'DGP' THEN 3 WHEN 'TDGP' THEN 4 END) desc,payment_rec_date desc";
   $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val)
    {
        $total_pay=$val['payment_amount']+$total_pay;
        $total_veh=$total_veh+1;
        $resp['vehicle_type'] =$val['prk_veh_type'];
        $resp['veh_number'] =$val['veh_number'];
        $resp['payment_amount'] =$val['payment_amount'];
        $resp['payment_rec_date'] =$val['payment_rec_date'];
        $resp['payment_rec_time'] =$val['payment_rec_time'];
        $resp['payment_type'] =$val['payment_type'];
        $resp['vehicle_typ_img'] =$val['vehicle_typ_img'];
        $resp['vehicle_type_dec'] =$val['vehicle_type_dec'];
        $re[$val['payment_type']][]= $resp;
    }
    $respons['status']=1;
    $response['session'] = 1;
    $response['message'] = 'Successful';
    $respons['total_veh'] = $total_veh;
    $respons['total_pay'] = $total_pay;
    $respons['payment_history'] = $re;
    $response = json_encode($respons);
    /*}*/

    /*pdf*/
    class PDF extends FPDF
    {
        function Footer(){
            $this->SetY(-15);
            $this->SetFont('Arial','I',8);
            $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
        }
    }
    $pdf = new \PDF('P','mm','A4');        
    $res=json_decode($response,true);
    //print_r($res);
    $x1=10;
    $x2=30;
    $x4=60;
    $x5=90;
    $x6=130;
    $x7=177;
    $x8=142;
    $x9=165;
    $x3=160;
    $y=35;
    $y2=27;
    $xnummer=15;
    $ynummer=30;
    $nummer=0;
    $xlijn=10;
    $ylijn=31;
    $x=0;
    $total_pay=42;
    $myl=38;
    $xnummer=15;
    $ynummer=30;
    $nummer=0;
    $pdf->addPage();
    $pdf->SetMargins(10, 10); 
    $pdf->SetAutoPageBreak(true, 10);


    foreach ($res['payment_history'] as $key => $item){
    // print_r($item);
        foreach ($item as $key_p => $values) {
        $total_pay=$total_pay+7; 
        $myl=$myl+7;
        $pdf->Image(prk_LOGO_pdf,5,10,35);
        $pdf->SetFont('Arial','B',8);
        $pdf->Text(170, 15, "{$values['payment_rec_date']}", TRUE);
        $pdf->SetFont('Arial','B',10); 
        $pdf->Text(80, 15, "{$insert_by}", TRUE);
        $pdf->Text(80, 20, "Employee Transaction", TRUE);
        $pdf->SetTextColor(0,0, 0);
        $pdf->SetFont('Arial','B',8);
        $pdf->Text(10, 35, "Vehicle Type", TRUE);
        // $pdf->Text(33, 25, "Gender", TRUE);
        $pdf->Text(30, 35, "Vehicle Number", TRUE);
        // $pdf->Text(63, 25, "Purpose", TRUE);
        $pdf->Text(60, 35, "Payment Type", TRUE);
        $pdf->Text(90, 35, "Payment Receive Date", TRUE);
        $pdf->Text(130, 35, "Payment Receive Time", TRUE);
        $pdf->Text(177, 35, "Total Pay", TRUE);

            // print_r($item);
            $pdf->SetFont('Arial','',8);
            $nummer++;
            $y = $y+7;
            $y2 = $y2+7;
            $ynummer = $ynummer+7;
            $ylijn = $ylijn+7;
            $pdf->SetXY($x1, $y);
            if($nummer % 30 === 0){
                $x1=10;
                $x2=30;
                $x4=60;
                $x5=90;
                $x6=130;
                $x7=177;
                $x8=142;
                $x9=165;
                $x3=160;
                $y=35;
                $y2=27;
                     $total_pay=42;
    $myl=38;
                $xnummer=15;
                $ynummer=30;
                $xlijn=10;
                $ylijn=31;
                $pdf->AddPage();
            }

            $pdf->SetFont('Arial','',7);
            $pdf->Text($x1,$y,"{$values['vehicle_type_dec']}",1,'L', TRUE);
            $pdf->Text($x2,$y,"{$values['veh_number']}",1,'L', TRUE);
            
            $pdf->Text($x4,$y,"{$values['payment_type']}",1,'L', TRUE);
            $pdf->Text($x5,$y,"{$values['payment_rec_date']}",1,'L', TRUE);
            $pdf->Text($x6,$y,"{$values['payment_rec_time']}",1,'L', TRUE);
            $pdf->Text($x7,$y,"{$values['payment_amount']}",1,'L', TRUE);

            $pdf->Line($xlijn, $ylijn, 190, $ylijn);
            $x=$x+1;   

        $pdf->SetXY(50,270);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(10,5,COPYRIGHT_PDF,'C');         
        }
    }
    $pdf->SetFont('Arial','B',11);
// $res['total_pay']
$pdf->Text($x7,$total_pay,"{$res['total_pay']}",1,'L', TRUE);
$pdf->Text($x6,$total_pay,"Grand Total",1,'L', TRUE);
$pdf->setXY(10,$myl);
    $pdf->cell(183,6,"",1);
    $name= uniqid();
    
   $fulpath=$name.".pdf";
   $pdf->Output($fulpath, "D");
?>