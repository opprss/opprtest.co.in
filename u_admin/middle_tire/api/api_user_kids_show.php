<?php
/*
Description: user payment history.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_kids_show($user_admin_id,$mobile,$kids_allow_id){
    global $pdoconn;
    $allplayerdata = array();
    $sql = "SELECT `prk_area_dtl`.`prk_area_name`,
        ka.`kids_allow_id`,
        ka.`prk_admin_id`,
        ka.`user_admin_id`,
        ka.`user_family_id`,
        ufg.`user_family_name`,
        ufg.`user_family_gender`,
        ufg.`user_family_relation`,
        IF(ufg.`user_family_img` IS NULL OR ufg.`user_family_img`='',IF(ufg.`user_family_gender`='M','".MALE_IMG_D."','".FEMALE_IMG_D."'),CONCAT('".USER_BASE_URL."',ufg.`user_family_img`)) as 'user_family_img',
        ka.`alone_flag`,
        ka.`rep_name`,
        IF(ka.`rep_image` IS NULL OR ka.`rep_image`='','',CONCAT('".USER_BASE_URL."',ka.`rep_image`)) as 'rep_image',
        DATE_FORMAT(ka.`date`,'%d-%m-%Y') AS `date`,
        ka.`from_time`,
        ka.`to_time`,
        ka.`status`
        FROM `kids_allow` ka JOIN `user_family_group` ufg join `prk_area_dtl` 
        ON ka.`prk_admin_id` = `prk_area_dtl`.`prk_admin_id` 
        WHERE ufg.`user_family_id`=ka.`user_family_id`
        AND ufg.`active_flag`='".FLAG_Y."'
        AND ufg.`del_flag`='".FLAG_N."'
        AND ka.`user_admin_id`='$user_admin_id'
        AND ka.`kids_allow_id`='$kids_allow_id'
        AND ka.`active_flag`='".FLAG_Y."'
        AND ka.`del_flag`='".FLAG_N."'
        AND `prk_area_dtl`.`active_flag` = '".FLAG_Y."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    $group = array();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['prk_area_name'] = $val['prk_area_name'];
        $response['kids_allow_id'] = $val['kids_allow_id'];
        $response['prk_admin_id'] = $val['prk_admin_id'];
        $response['user_admin_id'] = $val['user_admin_id'];
        $response['user_family_id'] = $val['user_family_id'];
        $response['user_family_name'] = $val['user_family_name'];
        $response['user_family_gender'] = $val['user_family_gender'];
        $response['user_family_relation'] = $val['user_family_relation'];
        $response['user_family_img'] = $val['user_family_img'];
        $response['alone_flag'] = $val['alone_flag'];
        $response['rep_name'] = $val['rep_name'];
        $response['rep_image'] = $val['rep_image'];
        $response['date'] = $val['date'];
        $response['from_time'] = $val['from_time'];
        $response['to_time'] = $val['to_time'];
        $response['status'] = $val['status'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>