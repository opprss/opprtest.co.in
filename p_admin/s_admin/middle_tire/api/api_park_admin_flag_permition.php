<?php
/*
Description: Parking area account status check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function park_admin_flag_permition($prient_in_flag,$prient_out_flag,$advance_flag,$advance_pay,$ad_flag,$sms_flag,$veh_out_otp_flag,$veh_token_flag,$visitor_token_flag,$veh_valid_flag,$parking_type,$visitor_out_verify_flag,$veh_out_verify_flag,$employee_user_name,$prk_admin_id,$visitor_otp_flag,$visitor_delay_time,$alarm_time,$bb_flag,$visitor_park_flag,$visitor_image_flag,$resident_flag,$vis_prient_in_flag,$vis_prient_out_flag,$office_flag,$printer_name){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "UPDATE `prk_area_admin` SET 
        `prient_in_flag`='$prient_in_flag',
        `prient_out_flag`='$prient_out_flag',
        `advance_flag`='$advance_flag',
        `advance_pay`='$advance_pay',
        `ad_flag`='$ad_flag',
        `sms_flag`='$sms_flag',
        `veh_out_otp_flag`='$veh_out_otp_flag',
        `veh_token_flag`='$veh_token_flag',
        `visitor_token_flag`='$visitor_token_flag',
        `veh_valid_flag`='$veh_valid_flag',
        `parking_type`='$parking_type',
        `visitor_out_verify_flag`='$visitor_out_verify_flag',
        `veh_out_verify_flag`='$veh_out_verify_flag',
        `updated_by`='$employee_user_name',
        `updated_date`='".TIME."',
        `visitor_otp_flag`='$visitor_otp_flag',
        `visitor_delay_time`='$visitor_delay_time',
        `alarm_time`='$alarm_time',
        `bb_flag`='$bb_flag', 
        `visitor_park_flag`='$visitor_park_flag', 
        `visitor_image_flag`='$visitor_image_flag', 
        `resident_flag`='$resident_flag', 
        `vis_prient_in_flag`='$vis_prient_in_flag', 
        `vis_prient_out_flag`='$vis_prient_out_flag', 
        `office_flag`='$office_flag', 
        `printer_name`='$printer_name' 
        WHERE `prk_admin_id`='$prk_admin_id'";
    $query  = $pdoconn->prepare($sql);
    $count=$query->rowCount();
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Sucessfull';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Sucessfull';
    }
    return json_encode($response);
}
?>