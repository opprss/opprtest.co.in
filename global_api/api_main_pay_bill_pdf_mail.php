<?php
@include '../../../../../library/dompdf/autoload.inc.php';
@include '../../../../library/dompdf/autoload.inc.php';
@include '../../../library/dompdf/autoload.inc.php';
@include '../../library/dompdf/autoload.inc.php';
@include '../library/dompdf/autoload.inc.php';
@include 'library/dompdf/autoload.inc.php';
use Dompdf\Dompdf;
function main_pay_bill_pdf_mail($prk_admin_id,$main_pay_id,$owner_id,$tenant_id){
    $response = array();
    global $pdoconn;
    $prk_in =prk_inf($prk_admin_id);
    $json = json_decode($prk_in);
    $prk_area_name=$json->prk_area_name;
    $prk_admin_name=$json->prk_admin_name;
    $sql="SELECT ma_p.`main_pay_id`,
        ma_p.`m_account_number`,
        ma_p.`tower_id`,
        ma_p.`flat_id`,
        ma_p.`owner_id`,
        ma_p.`tenant_id`,
        ma_p.`amount`,
        ma_p.`payment_type`,
        DATE_FORMAT( ma_p.`main_pay_date`,'%d-%b-%Y') AS `main_pay_date`,
        DATE_FORMAT( ma_p.`main_pay_date`,'%I:%i %p') AS `main_pay_time`,
        tow_d.`tower_name`,
        fa_d.`flat_name`,
        ow_d.`owner_name`,
        ow_d.`owner_email`,
        te_d.`tenant_name`,
        te_d.`tenant_email`
        FROM `maintenance_pay` ma_p JOIN `owner_details` ow_d JOIN `tower_details` tow_d 
            JOIN `flat_details` fa_d LEFT JOIN `tenant_details` te_d ON te_d.`tenant_id`=ma_p.`tenant_id`
            AND te_d.`prk_admin_id`=ma_p.`prk_admin_id`
            AND te_d.`active_flag`='".FLAG_Y."'
            AND te_d.`del_flag`='".FLAG_N."'
            WHERE ow_d.`prk_admin_id`=ma_p.`prk_admin_id`
            AND ow_d.`owner_id`=ma_p.`owner_id`
            AND ow_d.`active_flag`='".FLAG_Y."' 
            AND ow_d.`del_flag`= '".FLAG_N."'
            AND tow_d.`tower_id`= ma_p.`tower_id`
            AND tow_d.`prk_admin_id`=ma_p.`prk_admin_id`
            AND tow_d.`active_flag`='".FLAG_Y."' 
            AND tow_d.`del_flag`= '".FLAG_N."'
            AND fa_d.`flat_id`= ma_p.`flat_id` 
            AND fa_d.`prk_admin_id`=ma_p.`prk_admin_id`
            AND fa_d.`active_flag`='".FLAG_Y."' 
            AND fa_d.`del_flag`= '".FLAG_N."'
            AND ma_p.`main_pay_id`='$main_pay_id' 
            AND ma_p.`prk_admin_id`='$prk_admin_id' 
            AND ma_p.`active_flag`='".FLAG_Y."' 
        AND ma_p.`del_flag`= '".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $m_account_number = $val['m_account_number'];
    $amount = $val['amount'];
    $payment_type = $val['payment_type'];
    $main_pay_date = $val['main_pay_date'];
    $main_pay_time = $val['main_pay_time'];
    $tower_name = $val['tower_name'];
    $flat_name = $val['flat_name'];
    $owner_name = $val['owner_name'];
    $owner_email = $val['owner_email'];
    $tenant_name = $val['tenant_name'];
    $tenant_email = $val['tenant_email'];
    $full_address=$prk_area_name.', Tower Name:'.$tower_name.', Flat Name: '.$flat_name;
    return main_pay_bill_mail($owner_name,$owner_email,$tenant_email,$m_account_number,$main_pay_date,$amount,$payment_type,$full_address);
}
function main_pay_bill_mail($owner_name,$owner_email,$tenant_email,$m_account_number,$main_pay_date,$amount,$payment_type,$full_address){
    $email_logo = EMAIL_LOGO."oppr-logo.png";
    $email_logo_fb=EMAIL_LOGO_fb."fb.jpg";
    $email_logo_gmail=EMAIL_LOGO_gmail."google.png";
    $email_logo_indeed=EMAIL_LOGO_indeed."inidied.png";
    $message = '
        <html>
          <body>
              <div style="border: 1px solid #00b8d4; width: 600px;">
                <div style="background-color: #00b8d4; height: 80px; width: 600px;">
                  <span style="float: left;">
                 <img src="'.$email_logo.'" height="50" width="100">
                  </span>
                  <br><div style="margin-top: 5px">
                    <a href="#" style="margin-left: 350px;width: 20px;height: 20px;">
                      
                        <img src="'.$email_logo_fb.'" alt="FaceBook" height="30" width="30" / >
                    </a>

                   <span class="google">
                    <a href="#" >

                      <img src="'.$email_logo_gmail.'" alt="Gmail" height="30" width="30" />
                    </a>
                  </span>
                   <span class="linkedin">
                    <a href="#">
                     <img src="'.$email_logo_indeed.'" height="30" width="30" alt="Indeed" />
                    </a>
                  </span>
                </div>
                  <div style="margin-top: 60px; text-align: center;">
                        <span>
                          <b>Maintenance Pay</b>
                        </span>
                  </div>
                  <div style="margin-top: 20px; margin-left: 20px;">
                    <span>
                      <b>Dear Sir/Madam, '.$owner_name.'</b>
                    </span>
                  </div>
                  <div style="margin-top: 20px; margin-left: 30px;width: 555px">
                    <p>'.$full_address.'.<br> Thank you. Your payment has been successfully received with the following details.</p>

                    <p><table class="tg1" style="width: 500px;margin-top: 15px;">
                          <tr>
                            <th class="tg-nrix1">Payment Date</th>
                            <th class="tg-baqh1">Account Number</th>
                            <th class="tg-baqh1">Amount</th>
                            <th class="tg-baqh1">Payment Mode</th>
                          </tr>
                          <tr style=" text-align: center;">
                            <td class="tg-baqh1">'.$main_pay_date.'</td>
                            <td class="tg-baqh1">'.$m_account_number.'</td>
                            <td class="tg-baqh1">'.$amount.'/-</td>
                            <td class="tg-baqh1">'.$payment_type.'</td>
                          </tr>
                        </table>
                    </p>

                   <p>In case you need any further clarification for the same, please do get in touch immediately with care@opprss.com or your nearest branch. </p>

                   <p> This E-Notification was automatically generated. Please do not reply to this mail. For any suggestions / feedback, please click on Feedback link provided on<a href="http://opprss.com"> www.opprss.com</a></p>
                  </div>
                  <div style="margin-top: 10px; margin-left: 20px;">

                      <b>Regards,</b><br>
                      <p>OPPR Team.<br>
                      A Smart Parking & Security Solution.
                    </p><div style="width: 555px">
                       &emsp;"The information contained in this electronic message and any attachments to this message are intended for exclusive use of the addressee(s) and may contain confidential or privileged information. If you are not the intended recipient, please notify the sender at OPPR Software Solution Pvt Ltd or care@opprss.com immediately and
                    destroy all copies of this message and any attachments. The views expressed in this E-mail message / Attachments, are those of the individual sender."
                    </div>
                  </div>

                  </div>
                  <div style="margin-top: 620px; text-align: center;">
                  <span><a href="http://opprss.com/about-us">About Us</a> |</span> <span><a href="http://opprss.com/contact-us">Contect Us </a></span>
                  </div>
                  <div style="text-align: center; margin-top: 20px;padding-bottom: 20px; font-size: 11px"> &copy; 2017 BY OPPR SOFTWARE SOLUTION. ALL RIGHTS RESERVED.<br><br>
                  </div>
                </div>
              </div>
          </body>
        </html>';
    email_massage($owner_email,$message); 
}
?> 