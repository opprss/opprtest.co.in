<?php
/*
Description: Parking area gate update.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_area_gate_dtl_update($prk_area_gate_id,$prk_admin_id,$prk_area_gate_name,$prk_area_gate_dec,$prk_area_gate_landmark,$effective_date,$end_date,$prk_area_gate_type,$user_name){
    global $pdoconn;
    $response = array();
    $sql ="UPDATE `prk_area_gate_dtl` SET `active_flag`='".FLAG_N."', `updated_date`='".TIME."',`updated_by`='$user_name' WHERE `prk_admin_id`='$prk_admin_id' AND `prk_area_gate_id`='$prk_area_gate_id' AND `active_flag`='".FLAG_Y."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response = prk_area_gate_dtl($prk_admin_id,$prk_area_gate_name,$prk_area_gate_dec,$prk_area_gate_landmark,$effective_date,$end_date,$prk_area_gate_type,$user_name);     
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Updated';
        $response = json_encode($response);
    }
    return $response;
}
?>