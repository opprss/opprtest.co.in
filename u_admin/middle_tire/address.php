<?php 
/*
Description: user address savee.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
    require_once 'api/reg_api.php';
    // require_once 'api/global_api.php';
    $response = array();
 
    if(isAvailable(array('user_admin_id','address','lendmark','area','state','city','user_add_country','pin','mobile','token'))){

        if(isEmpty(array('user_admin_id','address','lendmark','area','state','city','user_add_country','pin','mobile','token'))){
            $user_admin_id = trim($_POST['user_admin_id']);
            $address = trim($_POST['address']);
            $lendmark = trim($_POST['lendmark']);
            $area = trim($_POST['area']);
            $state = trim($_POST['state']);
            $city = trim($_POST['city']);
            $user_add_country= trim($_POST['user_add_country']);
            $pin = trim($_POST['pin']);
            $mobile = trim($_POST['mobile']);
            
            $token = ($_POST['token']);
            $resp=user_token_check($user_admin_id,$token);
            $json = json_decode($resp);
            if($json->status=='1'){
                $respon = address($user_admin_id,$address,$lendmark,$area,$state,$city,$user_add_country,$pin,$mobile);
                echo ($respon);
            }else{
                $response['status'] = 0; 
                $response['session'] = 0;
                $response['message'] = 'Session Expired Please Login Again';
                echo json_encode($response);
            }
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
        }
    }else{
         $response['status'] = 0; 
         $response['message'] = 'Invalid API Call';
         echo json_encode($response);
    }
?>