<?php 
/*
Description: user vehicle scan and notification to parking employ wateing user. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/  
require_once 'api/parkAreaReg_api.php';
//require_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('user_prk_out_veh_varify_id'))){
 	if(isEmpty(array('user_prk_out_veh_varify_id'))){

	 	$user_prk_out_veh_varify_id = trim($_POST['user_prk_out_veh_varify_id']);
        $respon=user_veh_prk_verify_out_status($user_prk_out_veh_varify_id);
        echo ($respon);
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?>