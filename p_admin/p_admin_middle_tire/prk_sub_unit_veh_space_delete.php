<?php 
/*
Description: Parking area sub unit vehicle space update.
Developed by: Rakhal Raj Mandal
Created Date: 05-05-2018
Update date : -------
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
// $response = array();
 // $prk_sub_unit_veh_space_id = $_GET['prk_sub_unit_veh_space_id'];
//  $prk_admin_id = $_GET['prk_admin_id'];
// $user_name = $_GET['user_name'];


//  if(isset($prk_sub_unit_veh_space_id) && isset($prk_admin_id) && isset($user_name)){
    
//  $respon=prk_sub_unit_veh_space_delete($prk_sub_unit_veh_space_id,$prk_admin_id,$user_name);
//         echo ($respon);
//         if ($respon->status == 0) {
//              $_SESSION['error'] = $json->message;
//              header('Location: ' . $_SERVER['HTTP_REFERER']);
//          }else{
//             header('location: ../vender-prk-rate-subunit');
//          }
        
//  }else{
//     $response['status'] = 0; 
//     $response['message'] = 'Invalid API Call';
//     echo json_encode($response);
//  }

$response = array();
 if(isAvailable(array('prk_admin_id','user_name','prk_sub_unit_veh_space_id','token'))){
    if(isEmpty(array('prk_admin_id','user_name','prk_sub_unit_veh_space_id','token'))){ 
        $prk_admin_id = $_POST['prk_admin_id'];
        $user_name = trim($_POST['user_name']);
        $prk_sub_unit_veh_space_id = $_POST['prk_sub_unit_veh_space_id'];
        $token = trim($_POST['token']);
        
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            $response = prk_sub_unit_veh_space_delete($prk_sub_unit_veh_space_id,$prk_admin_id,$user_name);
            //echo $respon;
        }else{
            $response = $resp;
            // echo $response;
        }
    }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>