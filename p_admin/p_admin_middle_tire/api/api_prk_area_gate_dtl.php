<?php
/*
Description: Parking area gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_area_gate_dtl($prk_admin_id,$prk_area_gate_name,$prk_area_gate_dec,$prk_area_gate_landmark,$effective_date,$end_date,$prk_area_gate_type,$user_name){
    global $pdoconn;
    $response = array();
    $sql = "INSERT INTO `prk_area_gate_dtl`(`prk_admin_id`,`prk_area_gate_name`,`prk_area_gate_dec`,`prk_area_gate_landmark`,`prk_area_gate_type`,`effective_date`,`end_date`,`inserted_by`,`inserted_date`) VALUE ('$prk_admin_id','$prk_area_gate_name','$prk_area_gate_dec','$prk_area_gate_landmark','$prk_area_gate_type','$effective_date','$end_date','$user_name','".TIME."')";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $prk_area_gate_id = $pdoconn->lastInsertId();
        $query = $pdoconn->prepare("SELECT `account_number`,`prk_admin_id`,`prk_admin_name` FROM `prk_area_admin` WHERE `prk_admin_id`='$prk_admin_id'");
        $query->execute();
        $count=$query->rowCount();
        $row = $query->fetch();
        $account_number =  $row['account_number'];

        $prk_qr_code=$account_number.'-'.$prk_area_gate_id;

        $sql="UPDATE `prk_area_gate_dtl` SET `prk_qr_code`='$prk_qr_code' WHERE `prk_area_gate_id`='$prk_area_gate_id'";
        $query = $pdoconn->prepare($sql);
        if($query->execute()){
            $response['status'] = 1;
            $response['message'] = 'Gate Insert successful';
        }else{
            $response['status'] = 0;
            $response['message'] = 'Saved successful without QR code';
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'Data Not Save';
    }
    return json_encode($response);
}
?>
