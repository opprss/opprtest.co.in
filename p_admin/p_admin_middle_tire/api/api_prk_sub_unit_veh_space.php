<?php
/*
Description: Parking area sub unit vehicle space insert.
Developed by: Rakhal Raj Mandal
Created Date: 05-05-2018
Update date : -------
*/ 
function prk_sub_unit_veh_space($prk_sub_unit_id,$prk_admin_id,$veh_type,$veh_space,$prk_sub_unit_veh_space_eff_date,$prk_sub_unit_veh_space_end_date,$user_name){
    global $pdoconn;
    $response = array();

    $sql = "INSERT INTO `prk_sub_unit_veh_space`(`prk_sub_unit_id`, `prk_admin_id`, `veh_type`, `veh_space`,`prk_sub_unit_veh_space_eff_date`, `prk_sub_unit_veh_space_end_date`, `inserted_by`, `inserted_date`) VALUES ('$prk_sub_unit_id','$prk_admin_id','$veh_type','$veh_space','$prk_sub_unit_veh_space_eff_date','$prk_sub_unit_veh_space_end_date','$user_name','".TIME."')";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Insert sucessfull';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Insert Not sucessfull';
        return json_encode($response);
    }
}
?>