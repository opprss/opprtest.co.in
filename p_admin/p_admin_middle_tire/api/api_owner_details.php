<?php
/*
Description: parking area employ create.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function owner_details($prk_admin_id,$tower_id,$flat_id,$owner_name,$owner_mobile,$owner_email,$owner_gender,$owner_id,$effective_date,$end_date,$parking_admin_name,$crud_type){
   $response = array();
   $allplayerdata = array();
    global $pdoconn;
    switch ($crud_type) {
        case 'I':
            $owner_id=uniqid();
            $sql="INSERT INTO `owner_details`(`owner_id`, `prk_admin_id`, `owner_name`, `owner_mobile`, `owner_email`, `owner_gender`, `eff_date`, `end_date`, `inserted_by`, `inserted_date`) VALUES ('$owner_id','$prk_admin_id','$owner_name','$owner_mobile','$owner_email','$owner_gender','$effective_date','$end_date','$parking_admin_name','".TIME."')";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Owner Save';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Owner Not Save ';
            }  
            break;
        case 'U':
            $sql="UPDATE `owner_details` SET `active_flag`='".FLAG_N."',`updated_by`='$parking_admin_name',`updated_date`='".TIME."' WHERE `owner_id`='$owner_id' AND `prk_admin_id`='$prk_admin_id' AND `del_flag`='".FLAG_N."' AND `active_flag`='".FLAG_Y."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            #------------------------------------------#
            $sql="INSERT INTO `owner_details`(`owner_id`, `prk_admin_id`, `owner_name`, `owner_mobile`, `owner_email`, `owner_gender`, `eff_date`, `end_date`, `inserted_by`, `inserted_date`) VALUES ('$owner_id','$prk_admin_id','$owner_name','$owner_mobile','$owner_email','$owner_gender','$effective_date','$end_date','$parking_admin_name','".TIME."')";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Owner Modify Save';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Owner Modify Not Save ';
            }  
            break;
        case 'S':
            $sql="SELECT `owner_id`,`prk_admin_id`,`owner_name`,`owner_mobile`,`owner_email`,`owner_gender`,`eff_date`,`end_date` FROM `owner_details` WHERE `owner_id`='$owner_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            // $sql="SELECT  `flat_id`, `tower_id`, `flat_name`, `prk_admin_id`, `eff_date`, `end_date` FROM `flat_details` WHERE `flat_id`='$flat_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['owner_id'] =  $val['owner_id'];
                $response['prk_admin_id'] =  $val['prk_admin_id'];
                // $response['tower_id'] =  $val['tower_id'];
                // $response['flat_id'] =  $val['flat_id'];
                // $response['flat_name'] =  $val['flat_name'];
                $response['owner_name'] =  $val['owner_name'];
                $response['owner_mobile'] =  $val['owner_mobile'];
                $response['owner_email'] =  $val['owner_email'];
                $response['owner_gender'] =  $val['owner_gender'];
                $response['eff_date'] =  $val['eff_date'];
                $response['end_date'] =  $val['end_date'];
            }else{
                $response['status'] = 0;
                $response['message'] = 'Not Successful';
            }  
            break;
        case 'LS':
            $sql="SELECT `owner_details`.`owner_id`,
                `owner_details`.`prk_admin_id`,
                `owner_details`.`owner_name`,
                `owner_details`.`owner_mobile`,
                `owner_details`.`owner_email`,
                `owner_details`.`owner_gender`,
                `owner_details`.`eff_date`,
                `owner_details`.`end_date`,
                IF('".TIME_TRN."' BETWEEN `owner_details`.`eff_date` AND `owner_details`.`end_date`, '".FLAG_Y."', '".FLAG_N."') as activ_status
                FROM `owner_details` 
                WHERE `owner_details`.`prk_admin_id`='$prk_admin_id' 
                AND `owner_details`.`active_flag`='".FLAG_Y."' 
                AND `owner_details`.`del_flag`='".FLAG_N."'
                ORDER BY `owner_details`.`owner_unique_id` DESC";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $arr_catagory = $query->fetchAll();
                foreach($arr_catagory as $val)
                {
                    $park['owner_id'] =  $val['owner_id'];
                    $park['prk_admin_id'] =  $val['prk_admin_id'];
                    $park['owner_name'] =  $val['owner_name'];
                    $park['owner_mobile'] =  $val['owner_mobile'];
                    $park['owner_email'] =  $val['owner_email'];
                    $park['owner_gender'] =  $val['owner_gender'];
                    $park['eff_date'] =  $val['eff_date'];
                    $park['end_date'] =  $val['end_date'];
                    $park['activ_status'] =  $val['activ_status'];
                    array_push($allplayerdata, $park);
                }
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['owner_list'] = $allplayerdata;
            }else{
                $response['status'] = 0;
                $response['message'] = 'List Empty';
            }
            break;
        case 'D':
            $sql="UPDATE `owner_details` SET `del_flag`='".FLAG_Y."',`updated_by`='$parking_admin_name',`updated_date`='".TIME."' WHERE `owner_id`='$owner_id' AND `prk_admin_id`='$prk_admin_id' AND `del_flag`='".FLAG_N."' AND `active_flag`='".FLAG_Y."'";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Delete Successful';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Delete Not Successful';
            }
            break;
        default:
            $response['status'] = 0;
            $response['message'] = 'Wrong Select';
            break;
    }
    return json_encode($response);  
}
?>