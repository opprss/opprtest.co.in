<?php
    /*
    Description: State select.
    Developed by: Rakhal Raj Mandal
    Created Date: -------
    Update date :30-03-2018
    */
    function maintenance_master($prk_admin_id,$tower_id,$flat_id,$m_account_number,$main_tran_year,$main_tran_month,$previous_balance,$current_due,$fine_charges,$statement_date,$bill_due_date,$total_pay_before_due_date,$total_pay_after_due_date,$after_fine_charges,$inserted_by){
        $response = array();
        global $pdoconn;
        $order_id='';
        $id=uniqid();
        $sql = "INSERT INTO `maintenance_master`(`main_mas_id`, `prk_admin_id`, `tower_id`, `flat_id`, `order_id`, `m_account_number`, `main_tran_year`, `main_tran_month`, `previous_balance`, `current_due`, `fine_charges`, `statement_date`, `bill_due_date`, `total_pay_before_due_date`, `total_pay_after_due_date`, `generate_date`, `inserted_by`, `inserted_date`) VALUES ('$id','$prk_admin_id','$tower_id','$flat_id','$order_id','$m_account_number','$main_tran_year','$main_tran_month','$previous_balance','$current_due','$fine_charges','$statement_date','$bill_due_date','$total_pay_before_due_date','$total_pay_after_due_date','".TIME."','$inserted_by','".TIME."')";
        $query  = $pdoconn->prepare($sql);
        if($query->execute()){
            $response['status'] = 1;
            $response['main_mas_id'] = $id;
            $response['message'] = 'Successful';
        }else{
            $response['status'] = 0;
            $response['message'] = 'Not Successful';
        }
        return json_encode($response);
    }
?>