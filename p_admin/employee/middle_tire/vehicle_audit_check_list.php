<?php 
/*
Description: Parking visiter in list
Developed by: Rakhal Raj Mandal
Created Date: 31-05-2018
Update date : 
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','token','start_date','end_date'))){
    if(isEmpty(array('prk_admin_id','prk_area_user_id','token'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $token = trim($_POST['token']);
        $start_date = trim($_POST['start_date']);
        $end_date = trim($_POST['end_date']);
        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            
            $response = vehicle_audit_check_list($prk_admin_id,$start_date,$end_date);
        }else{

            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response); 
} 
echo $response;
?> 