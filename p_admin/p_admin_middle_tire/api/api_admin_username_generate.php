<?php
/*
Description: Parking area employ list.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function admin_username_generate($parking_area_name){
    global $pdoconn;
    $pieces = explode(" ", $parking_area_name);
    $user_name=substr($pieces[0],0,4);
    $username=admin_username_generate_check($user_name);
    $response['status'] = 1;
    $response['message'] = 'Sucessfull';
    $response['username'] = $username;
    return json_encode($response);
}
function admin_username_generate_check($user_name){
    global $pdoconn;
    $shuffled=mt_rand(1000,9999);
    $new_username=$user_name.'_'.$shuffled;
    $genarate_username=$new_username;
    $username='';
    $sql = "SELECT `prk_admin_id` FROM `prk_area_admin` WHERE `prk_admin_name`='$genarate_username'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        #------------
        admin_username_generate_check($user_name);
    }else{
        #------------
        $username=$genarate_username;
    }
    return $username;
}
?>