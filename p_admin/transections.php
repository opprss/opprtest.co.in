<!-- ################################################
  
Description: Parking area admin can add/delete parking rate by this web page.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

####################################################-->
<?php include "all_nav/header.php";?>
<style>
  .size{
    font-size: 11px;
  }
  .success{
    font-size: 11px;
    color: green;
  }
  .error_size{
    font-size:11px;
    color: red;
  }
</style>
<link href="../lib/highlightjs/github.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../lib/select2/css/select2.min.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Vehicle Transactions</h5>
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40" style="padding: 10px;">
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <form id="duplicatePayRecForm" action="" method="post">
          <div class="row">
            <div class="col-lg-10">
              <div class="row">
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Vehicle Number <span class="tx-danger"></span></label>
                    <input type="text" name="vehicle_number" id="vehicle_number" placeholder="Number" class="form-control">
                    <span style="position: absolute;" id="error_visit_name" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group" style="margin-top: 10px;">
                    <label class="form-control-label size">Vehicle Type<span class="tx-danger"></span></label>
                    <select name="vehicle_type" id="vehicle_type" value="" style="opacity: 0.8; font-size:14px">
                      <option value="">All</option>
                    </select>
                    <span style="position: absolute;" id="error_vehicle_type" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group" style="margin-top: 10px;">
                    <label class="form-control-label size">Filter By    &nbsp;<span class="tx-danger"></span></label>
                    <select name="payment_type" id="payment_type" value="" style="opacity: 0.8; font-size:14px">
                      <option value="">ALL</option>
                      <option value="<?php echo CASH ?>"><?php echo CASH ?></option>
                      <option value="<?php echo WALLET ?>"><?php echo WALLET ?></option>
                      <option value="<?php echo DGP ?>"><?php echo DGP_F ?></option>
                      <option value="<?php echo TDGP ?>"><?php echo TDGP_F ?></option>
                    </select>
                    <span style="position: absolute;" id="error_payment_type" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Start Date <span class="tx-danger">*</span></label>
                    <input type="text" name="start_date" id="start_date" placeholder="MM/DD/YYYY" class="form-control prevent_readonly" readonly="readonly">
                    <span style="position: absolute;" id="error_start_date" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">End Date <span class="tx-danger">*</span></label>
                    <input type="text" name="end_date" id="end_date" placeholder="MM/DD/YYYY" class="form-control prevent_readonly" readonly="readonly">
                    <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-layout-footer mg-t-30">
                <button type="button" class="btn btn-block prk_button" id="show">Search</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>   
  <div class="am-pagebody">
    <div class="card">
      <div class="card pd-20 pd-sm-40" style="padding: 10px;">
        <div class="table-wrapper">
          <table id="datatable1" class="table display responsive nowrap" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="wd-5p">Img</th>
                <th class="wd-10p">Vehicle Number</th>
                <th class="wd-10p">Payment Type</th>
                <th class="wd-10p">Price</th>
                <th class="wd-10p">In Date</th>
                <th class="wd-10p">In Time</th>
                <th class="wd-10p">Out Date</th>
                <th class="wd-10p">Out Time</th>
                <th class="wd-10p">Total Hour</th>
                <th class="wd-10p">Round of Hour</th>
                <th class="wd-10p">Mobile NUmber</th>
                <th class="wd-10p">Transition Date</th>
              </tr>
            </thead>
            <tbody id="dataLines"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include"all_nav/footer.php"; ?>
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="../lib/datatables-responsive/jquery.dataTables.min.js"></script>
<script src="../lib/datatables-responsive/dataTables.buttons.min.js"></script>
<script src="../lib/datatables-responsive/jszip.min.js"></script>
<script src="../lib/datatables-responsive/pdfmake.min.js"></script>
<script src="../lib/datatables-responsive/vfs_fonts.js"></script>
<script src="../lib/datatables-responsive/buttons.html5.min.js"></script>
<script type="text/javascript">
  var OPPR_BASE_URL = "<?php echo OPPR_BASE_URL?>";
  var prkUrl = "<?php echo PRK_URL; ?>";
  var prk_admin_id = "<?=$prk_admin_id?>";
  var parking_admin_name = "<?=$parking_admin_name?>";
  var token = "<?=$token?>";
  $( document ).ready( function () {
    vehicle_transactions_dtl('','','','','')
    // datatable_show();
    $("#start_date").blur(function () {
      var start_date = $('#start_date').val();
      if(start_date == ''){
        $('#error_start_date').text('Satrt date is required');
        return false;
      }else{
        $('#error_start_date').text('');
        return true;
      }
    });
    $("#end_date").blur(function () {
      var start_date = $('#end_date').val();
      if(start_date == ''){
        $('#error_end_date').text('End date is required');
        return false;
      }else{
        $('#error_end_date').text('');
        return true;
      }
    });
    $('#show').click(function () {
      var start_date = $('#start_date').val();
      var end_date = $('#end_date').val();
      var vehicle_number = $('#vehicle_number').val();
      var vehicle_type = $('#vehicle_type').val();
      var payment_type = $('#payment_type').val();
      if(start_date!='' && end_date!=''){
        // alert('ok');
        vehicle_transactions_dtl(start_date,end_date,vehicle_number,vehicle_type,payment_type);
      }else{
        if (start_date == '') {
          $('#error_start_date').text('Satrt date is required');
        }if (end_date == '') {
          $('#error_end_date').text('End date is required');
        }
      }
    });
    addEventListener('DOMContentLoaded', function () {
      var now = new Date;
      var now = now.setDate(now.getDate());
      pickmeup('#start_date', {
        default_date   : false,
        position       : 'bottom',
        hide_on_select : true,
        render : function (date) {
          if (now < date) {
              return {
                disabled : true,
                class_name : 'date-in-past'
              };
          }
          $('#error_effective_date').hide();
          return {};
        } 
      });
    });
    $('#end_date').focus(function () {
      var start = new Date;
      var end = $('#start_date').val();
      var d = new Date(end.split("-").reverse().join("-"));
      var dd=d.getDate();
      var mm=d.getMonth()+1;
      var yy=d.getFullYear();
      var hh = start.getHours();
      var min = start.getMinutes();
      var sec = start.getSeconds();
      var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
      var end = new Date(newdate);
      days = (end - start) / (1000 * 60 * 60 * 24)-1;
      days = Math.round(days);
      now_end = start.setDate(start.getDate() + days);
      pickmeup('#end_date', {
        default_date   : false,
        /*position       : 'top',*/
        hide_on_select : true,
        render : function (date) {
              if (date < now_end) {
                  return {
                    disabled : true,
                    class_name : 'date-in-past'
                  };
              }
              return {};
        } 
      });
    });
    $('#start_date').focus(function(){
      // code
      $('#end_date').val('');
    });
    $("#vehicle_type").select2();
    $("#payment_type").select2();
    var urlVehicleType = prkUrl+'vehicle_type.php';
    $.ajax ({
      type: 'POST',
      url: urlVehicleType,
      data: "",
      success : function(data) {
        // alert(data);
        var obj=JSON.parse(data);
        var areaOption = "<option value='' style='color:gray;' selected='selected'>All</option>";
        $.each(obj, function(val, key) {
          // alert("Value is :" + key['e']);
          areaOption += '<option value="' + key['vehicle_Sort_nm'] + '">' + key['vehicle_type'] + '</option>'
        });
        $("#vehicle_type").html(areaOption);
        $("#vehicle_type").select2();
      }
    });
  });
  function vehicle_transactions_dtl(start_date,end_date,vehicle_number,vehicle_type,payment_type){
    var urlVehNoCheck = prkUrl+'prk_my_trn.php';
    // 'prk_admin_id','token','start_date','end_date','vehicle_number','vehicle_type','payment_type'
    $.ajax({
      url :urlVehNoCheck,
      type:'POST',
      data :{
        'prk_admin_id':prk_admin_id,
        'start_date':start_date,
        'end_date':end_date,
        'vehicle_number':vehicle_number,
        'vehicle_type':vehicle_type,
        'payment_type':payment_type,
        'token':token
      },
      dataType:'html',
      // dataType:'json',
      success  :function(data){
        // alert(data);
        var demoLines = '';
        var json = $.parseJSON(data);
        if (json.status) {
          for (var key in json.payment_history) {
            $.each(json.payment_history[key], function() {
               demoLines += '<tr>\
                <td><img src="'+OPPR_BASE_URL+this["vehicle_typ_img"]+'" style="    height: 30px;"></td>\
                <td>'+this["veh_number"]+'</td>\
                <td>'+this["payment_type"]+'</td>\
                <td>'+this["total_pay"]+'</td>\
                <td>'+this["veh_in_date"]+'</td>\
                <td>'+this["veh_in_time"]+'</td>\
                <td>'+this["veh_out_date"]+'</td>\
                <td>'+this["veh_out_time"]+'</td>\
                <td>'+this["total_hr"]+'</td>\
                <td>'+this["round_hr"]+'</td>\
                <td>'+this["user_mobile"]+'</td>\
                <td>'+this["transition_date"]+'</td>\
              </tr>';
            });
          }
        }
        $('#datatable1').dataTable().fnDestroy();
        $("#dataLines").html(demoLines);
        datatable_show();
      }
    });
  }
  function datatable_show(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      language: {
      searchPlaceholder: 'Search Here',
      sSearch: '',
      lengthMenu: '_MENU_ Page',
      },
      // lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      buttons: [
        { 
          extend: 'copyHtml5', 
          footer: true,
          title: 'VEHICLE TRANSACTION DETAILS SHEET' 
        },
        { 
          extend: 'excelHtml5', 
          footer: true,
          title: 'VEHICLE TRANSACTION DETAILS SHEET'  
        },
        {
            extend: 'pdfHtml5',
            footer: true,
            title: 'VEHICLE TRANSACTION DETAILS SHEET',
            customize: function(doc) {
              doc.content[1].margin = [ 0, 0, 0, 0 ] //left, top, right, bottom
            },
            exportOptions: {
                columns: [1,2,3,4,5,6,7,8,9,10]
            }
        },
      ]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  }
</script>