<!-- ################################################
  
  Description: Parking area can issue gate pass to a particular Visitor.
  Developed by: Soemen Banerjee
  Created Date: 17-03-2018
####################################################-->
<?php 
  include "all_nav/header.php";
  if (isset($_SESSION['prk_admin_id'])) {
    $user_name=$_SESSION['prk_admin_id'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
  }
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: gray;

  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
    <!-- for datatable -->
    <link href="../lib/highlightjs/github.css" rel="stylesheet">
    <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet"> 
    <!-- header position -->
    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Multiple Visitor Id Card Download</h5>
          <!--<form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
        
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card pd-20 pd-sm-40 col-md-12">
          <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
            <!-- <div class="col-md-4">
              <a style="margin-bottom: 10px;" href="#" class="btn btn-primary pull-right">
                    <i class="fa fa-plus"></i>Download id card
                </a>
            </div> -->
            <div class="col-lg-4">
              <input type="button" value="Download id card" class="btn btn-primary pull-right" name="save" id="save" style="margin-bottom: 10px;">
            </div>
        </div>
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th></th>
                  <th class="">Image</th>
                  <th class="">Name</th>
                  <th class="">Mobile</th>
                  <th class="">Effective Date</th>
                  <th class="">End Date</th>
                </tr>
              </thead>
              <tbody>
                <?php 
               $respon=visitor_gate_pass_dtl_list($prk_admin_id);
               // echo $respon; 
               $respon = json_decode($respon, true);
               if($respon['status']){
                foreach($respon['state'] as $value){
                
                 ?>
               <tr>
                <td><input type="checkbox" name="" class="p_'$value['visitor_gate_pass_dtl_id']'" id="<?php echo $value['visitor_gate_pass_dtl_id']; ?>" onclick="visitor_id_print(this.id)"></td>
                <td><img src="<?php echo $value['visitor_img']; ?>" style="height: 40px;"> </td>
                <td><?php echo $value['visitor_name']; ?></td>
                <td><?php echo $value['visitor_mobile']; ?></td>
                <td><?php echo $value['effective_date']; ?></td>
                <td><?php echo $value['end_date']; ?></td>
              </tr>
              <?php }} ?>
                </tbody>
            </table>
          </div>
          </div>
        </div>
      </div>
      <div class="am-pagebody">
      <!-- footer part -->
      <?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<style>
  .error_size{
    font-size: 11px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
<script type="text/javascript">
  
</script>

<script>
      $(function(){
        'use strict';

        $('#datatable1').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search Here',
            sSearch: '',
            lengthMenu: '_MENU_ Page',
          },
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
        });

        $('#datatable2').DataTable({
          bLengthChange: false,
          searching: false,
           "scrollX": true
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

      });
    </script>


<script type="text/javascript">
$('#save').prop('disabled', true);
var prkUrl = "<?php echo PRK_URL; ?>";
var urlPrkAreGateDtl = prkUrl+'multiple_visitor_id_print.php';
var myArray = [];
//var myArray1 = [];
function visitor_id_print(str) {
  var myarr = str;

  if(document.getElementById(str).checked){
    myArray [str] = str;
  }else{
    delete myArray[str];
  }
  disable_submit();
}


$('#save').click(function(){
  var obj = convert(myArray);
  var list_id =JSON.stringify(obj);
  
  urlPrint ="p_admin_middle_tire/multiple_visitor_id_print.php?list_id="+list_id;
  // urlPrint =prkUrl+"multiple_visitor_id_print.php?list_id="+list_id;
  // console.log(urlPrint)
  window.open(urlPrint,'_blank');
});
function convert(data) {
    return Array.isArray(data)
        ? data.reduce( (obj, el, i) => (el && (obj[i] = convert(el)), obj), {} )
        : data;
}
function disable_submit(){
  var obj = convert(myArray);
  var list_id =JSON.stringify(obj);
  if(list_id.length==2){
    $('#save').prop('disabled', true);
  }else{
    $('#save').prop('disabled', false);
  }
}

</script>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  /*delete popup*/  
  $('button[id^="delete"]').on('click', function() {
    var prk_admin_id = "<?php echo $prk_admin_id;?>";
    var user_name = "<?php echo $user_name;?>";
    var visitor_gate_pass_dtl_id = this.value;
    //alert(visitor_gate_pass_dtl_id);
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the item permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            if (visitor_gate_pass_dtl_id != '') {
              // alert(visitor_gate_pass_dtl_id);
              var urlDtl = prkUrl+'visitor_gate_pass_issue_delete.php';
              // alert(urlDtl);
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :
                {
                  'visitor_gate_pass_dtl_id':visitor_gate_pass_dtl_id,
                  'user_name':user_name,
                  'prk_admin_id':prk_admin_id
                },
                dataType:'html',
                success  :function(data)
                {
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Visitor Gate Pass Delete successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                            location.reload(true);
                        }
                      }
                    });
                  }else{
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  });
</script>
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
