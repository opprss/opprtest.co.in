<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script type="text/javascript">
	$(window).on('load', function() {
	 	window.print();
		setTimeout(function(){window.close();}, 1);
	});
</script>
<?php
include 'api/parkAreaReg_api.php';
include 'barcode128.php';
@session_start();
//include 'number_to_sentanse.php';
$prk_admin_id = $_SESSION['prk_admin_id'];
$prk_inf = prk_inf($prk_admin_id);
$prk_inf = json_decode($prk_inf, true);
$prk_area_name = $prk_inf['prk_area_name'];
$prk_area_short_name = $prk_inf['prk_area_short_name'];
$prk_area_logo_img = $prk_inf['prk_area_logo_img'];

$area_address = prk_area_address_show($prk_admin_id);
$area_address = json_decode($area_address, true);
$prk_address = $area_address['prk_address'];
$prk_land_mark = $area_address['prk_land_mark'];
$prk_add_area = $area_address['prk_add_area'];
$city_name = $area_address['city_name'];
$state_name = $area_address['state_name'];
$prk_add_pin = $area_address['prk_add_pin'];
$prk_add_country = $area_address['prk_add_country'];
$address =$prk_land_mark.', ADD-'.$prk_address.', CITY-'.$city_name.', STATE-'.$state_name.', PIN-'.$prk_add_pin;

$id=$_GET['id']; 
$row=veh_prk_est_print($id);
$json=json_decode($row,true);
// print_r($json);
//die();
$num=$json['net_pay'];

   $words= api_number_to_word($num);

?>

<STYLE>
td {text-align:left; font-size:11px; padding: 0px}
td #cash{margin-left: 0px; font-size: 15px;padding: 0px;}
td #Truck b{font-size: 15px; font-weight: bolder; text-align: center;}
td #Haldia {font-size: 20px; font-style: bold;text-align: center;}
/*td #Stationary{margin-left: 50px}*/
td #barcode{margin-left: 140px}
/*td #img{margin-left: 110px}*/
td #estimation{font-size: 15px}

@media print{
  @page {
   margin-top: 0px;
   margin-bottom: 0;
  }
 body  {
   padding-top: 85px;
   padding-bottom: 72px ;
 }
}
</STYLE>
<table width="270px">
<tr>
	<td style="text-align: center;" colspan="2">
		<span id="img"><img src="<?php echo PRK_BASE_URL.''.$prk_area_logo_img; ?>" style='border-radius: 50%;' width="60" height="55" colspan="2"></span>
	</td>
</tr>

<tr>
	<td style='border-top:1px black; text-align: center;' colspan='2'>
		<!-- <span id="cash"> Cash/ Credit</span><br> -->
		<span id="Haldia"><?php echo $prk_area_name; ?></span><br>
		<span id="Stationary"><?php echo PRK_GOV; ?></span>
	</td>
</tr>
<tr>
	<td style=' font-size: 15px; text-align: center;' colspan='2'>
		<span id="Truck"><b><?php echo PRK_NAME; ?></b></span>
	</td>
</tr>
<!-- <tr><td>Holdia City Center, P.O- Debhog Haldia,</td></tr><tr><td> Dist. Purba Medinipur, W.B. 721657</td></tr> -->

<tr>
	<td style='border-top:1px dotted black; text-align: center;' colspan='2'>
		<span id="City"><?php echo $address; ?></span>
	</td>
</tr>
<!-- <tr>	
<td style='border-top:1px  black'><b>Sl No.:</b> <?php ?></td>
<td></td>
<td></td>
</tr> -->
<tr>
	<td style='border-top:1px dotted black; border-bottom:1px dotted black; padding: 2px 0px 2px 0px;' colspan='2'>	
		<?php 
		echo '<center><div style="height: 30%; width: 100%;">';
		echo bar128(stripcslashes($json['barcode_valu']));
		echo '</div></center>';
		 ?>
	</td>
</tr>
<tr>
	<td style='text-align: center;' colspan='2'>
		<span id="estimation"><b>PARKING RATE ESTIMATION</b></span>
	</td>
</tr>
<tr>	
	<td style='border-top:1px dotted black;' colspan='2'></td>
</tr>
<tr>
	<td style='border-top:1px  black;'><b>Vehicle Type:</b> <?php echo $json['vehicle_type_dec']; ?></td>
	<td><b>Vehicle #: </b><?php echo $json['veh_number']; ?></td>
</tr>
<tr>	
	<td style='border-top:1px  black'><b>Rate Hr:</b> <?php echo $json['veh_rate_hour']; ?></td>
	<td><b>Rate: </b><?php echo $json['veh_rate']; ?></td>
</tr>
<!-- <tr>	
<td style='border-top:1px  black'><b>Parking In Date:</b> </td>
<td></td>
<td><b>Parking In Time: </b> </td>
</tr> -->
<!-- <tr>	
<td style='border-top:1px  black'><b>Parking Out Date:</b> </td>
<td></td>
<td><b>Parking Out Time: </b></td>
</tr> -->

<tr>	
	<td style='border-top:1px  black'><b>Total Hr: </b><?php echo $json['total_hour']; ?></td>
	<td><b>Gross Amount: </b><?php echo $json['total_pay']; ?></td>
</tr>
<tr>	
	<td style='border-top:1px  black'><b></b> </td>
	<td><b>Adv. Amount: </b><?php echo $json['advance_pay']; ?></td>
</tr>
<tr>
	<td style='border-top:1px dotted black' colspan='2'></td>
</tr>
<tr>	
	<td style='border-top:1px  black'><b>Payment Type: </b><?php echo $json['payment_type']; ?></td>
	<td><b>Net Amount: </b><?php echo $json['net_pay']; ?> </td>
</tr>
<tr>
	<td style='border-top:1px dotted black' colspan='2'></td>
</tr>
<tr>
	<td colspan='2'><?php echo '<span style="font-weight:bold">RS.</span>'.' '.strtoupper($words); ?></td>
</tr>
<tr>
	<td style='border-top:1px dotted black' colspan='2'></td>
</tr>
<tr>	
	<td style='border-top:1px  black'><b>Mobile No:</b> </td>
	<td><b>Autherised Signatory</b></td>
</tr>
<tr>	
	<td><b>Date: </b><?php echo $json['current_date']; ?></td>
	<td><b>By Order <?php echo $prk_area_short_name; ?></b></td>
</tr>
<tr>
	<td style='border-top:1px dotted black' colspan='2'></td>
</tr>
</table>


