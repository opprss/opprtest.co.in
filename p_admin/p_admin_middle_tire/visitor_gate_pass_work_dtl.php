<?php
/*
Description: Parking visitor gate pass issue.
Developed by: Rakhal Raj Mandal
Created Date: 31-10-2018
Update date :31-10-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','visitor_gate_pass_dtl_id','inserted_by'))){

    if(isEmpty(array('prk_admin_id','visitor_gate_pass_dtl_id','inserted_by'))){
        $number = count($_POST["tower_name"]); 
        $prk_admin_id = $_POST["prk_admin_id"]; 
        $inserted_by = $_POST["inserted_by"]; 
        $visitor_gate_pass_dtl_id = $_POST["visitor_gate_pass_dtl_id"]; 
        for($i=0; $i<$number; $i++) {  
            if(trim($_POST["tower_name"][$i] != '')){  
                $falt_name=$_POST["falt_name"][$i];
                $tower_name=$_POST["tower_name"][$i];
                $start_date=$_POST["start_date"][$i];
                $end_date1=$_POST["end_date1"][$i];
                $remark=$_POST["remark"][$i];
                $sql = "INSERT INTO `visitor_service_dtl` (`visitor_gate_pass_dtl_id`,`prk_admin_id`,`tower_name`,`falt_name`,`in_time`,`out_time`,`visitor_remark`,`inserted_by`,`inserted_date`) VALUES ('$visitor_gate_pass_dtl_id','$prk_admin_id', '$tower_name','$falt_name','$start_date','$end_date1','$remark','$inserted_by','".TIME."')";
                $query = $pdoconn->prepare($sql);
                $query->execute();
                /*if($query->execute()){
                    $response['status'] = 1;
                    $response['message'] = 'Gate Pass Issue Successful';
                    $response = json_encode($response);
                }else{
                    $response['status'] = 0;
                    $response['message'] = 'Gate Pass Issue Not Successful';
                    $response = json_encode($response);
                }*/
            }  
        }  
        $response['status'] = 1;
        $response['message'] = 'Gate Pass Issue Successful';
        $response = json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>