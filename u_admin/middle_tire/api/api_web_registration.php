<?php
/*
Description: user registration.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function web_registration($mobile,$password,$repassword,$email,$name,$dob,$gender,$v_number,$v_type,$t_c_flag,$device_token_id,$device_type){
    global $pdoconn;
    $response = array();
    $resp = array();
    $resp =accountStatus($mobile);
    $json = json_decode($resp);
    if($json->status=='1'){
        if(passwordChecker($password,$repassword)=='true'){
            if(emailChecker($email)=='true'){
                $dofCh =dofChecker($dob);
                $json = json_decode($dofCh);
                if($json->status=='1'){
                    $v_numberC =v_numberChecker($v_number);
                    $json = json_decode($v_numberC);
                    if($json->status=='1'){
                        $password=md5($password);
                        $response = array();
                        $sql = "INSERT INTO `user_admin`(`user_mobile`,`user_password`,`inserted_by`,`inserted_date`,`t_c_flag`) VALUE ('$mobile','$password','$mobile','".TIME."','$t_c_flag')";
                        $query = $pdoconn->prepare($sql);
                        if($query->execute()){
                            $user_admin_id = $pdoconn->lastInsertId();
                            
                            $sql = "INSERT INTO `user_detail`(`user_name`,`user_nick_name`,`user_gender`,`user_dob`,`user_email`,`user_admin_id`,`inserted_by`,`inserted_date`) VALUE ('$name','$name','$gender','$dob','$email','$user_admin_id','$mobile','".TIME."')";
                            $query = $pdoconn->prepare($sql);
                            $query->execute();
                            #--- Vehicle add------#
                            $user_veh_reg = NULL;
                            $user_veh_reg_exp_dt = NULL;    
                            $add_veh = add_vehicle($user_admin_id,$v_type,$v_number,$user_veh_reg,$user_veh_reg_exp_dt,$mobile);
                            $json = json_decode($add_veh);
                                if($json->status=='1'){
                                    #--- Token add------#
                                    $token = sha1(uniqid());
                                    if(tokenCheck($user_admin_id)){
                                        $sql = "INSERT INTO `token` (`user_admin_id`, `token_value`, `device_token_id`, `device_type`, `start_date`) VALUES ('$user_admin_id', '$token','$device_token_id','$device_type','".TIME."')";
                                        $query = $pdoconn->prepare($sql);
                                        $query->execute();
                                        if ($query){
                                            $response['status'] = 1;
                                            $response['user_admin_id'] = $user_admin_id;
                                            $response['mobile'] = $mobile;
                                            $response['token'] = $token;
                                            $response['name'] = $name;
                                            $response['user_ac_status'] = 'A';
                                            $response['message'] = 'registration sucessfull';
                                            $return = json_encode($response);
                                        }else{
                                            $response['status'] = 0;
                                            $response['token'] = $token;
                                            $response['message'] = 'token not found';
                                            $return = json_encode($response);
                                        }
                                    }else{
                                        $response['status'] = 0;
                                        $response['token'] = $token;
                                        $response['message'] = 'token not found';
                                        $return = json_encode($response);
                                    }
                                }else{
                                    $response['status'] = 0;
                                    $response['message'] = 'registration wrong';
                                    $return = json_encode($response);
                                }
                        }else{
                            $response['status'] = 0;
                            $response['message'] = 'registration wrong';
                            $return = json_encode($response);
                        }
                     }else{
                        $return = $v_numberC;
                     }
                }else{
                    $return = $dofCh;
                    // return ($response);
                }
            }else{
                $response=emailChecker($email);
                $return = json_encode($response);
            }
        }else{
            $response=passwordChecker($password,$repassword);
            $return = json_encode($response);
        }
    }else{
        $return = accountStatus($mobile);
        // return ($response);
    }
    return $return;
}
?>