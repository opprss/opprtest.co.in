<?php
/*
Description: Parking adimn password change.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function changePasswordPrkAdmin($user_name,$password,$repassword,$prk_admin_id){
    global $pdoconn;
    $response = array();
    if(passwordChecker($password,$repassword)=='true'){
        $password=md5($password);
        
        $sql ="UPDATE `prk_area_admin` SET `prk_admin_password`='$password', `updated_by`='$user_name', `updated_date`='".TIME."' WHERE `prk_admin_name` ='$user_name'";
        $query = $pdoconn->prepare($sql);
        if($query->execute()){
            $response['status'] = 1;
            $response['message'] = 'Update sucessfull';
            return json_encode($response);
        }else{
            $response['status'] = 0;
            $response['message'] = 'Update Not Save';
            return json_encode($response);
        }
    }else{
        $response=passwordChecker($password,$repassword);
        return json_encode($response);
    }
}
?>