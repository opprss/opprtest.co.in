<?php
/*
Description: parking area profile show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_area_profile_show($prk_admin_id){
    $response = array();
    global $pdoconn;
    $sql = "SELECT * FROM `prk_area_dtl` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $response['status'] = 1;
    $response['message'] = 'data Sucessful';
    $response['prk_area_pro_img'] = $val['prk_area_pro_img'];
    $response['prk_area_name'] = $val['prk_area_name'];
    $response['prk_area_short_name'] = $val['prk_area_short_name'];
    $response['prk_area_email'] = $val['prk_area_email'];
    $response['prk_email_verify'] = $val['prk_email_verify'];
    $response['prk_area_rep_name'] = $val['prk_area_rep_name'];
    $response['prk_area_rep_mobile'] = $val['prk_area_rep_mobile'];
    $response['prk_mobile_verify'] = $val['prk_mobile_verify'];
    $response['prk_area_logo_img'] = $val['prk_area_logo_img'];
    $response['last_due_day'] = $val['last_due_day'];
    $response['choose_due_day'] = $val['choose_due_day'];
    $response['prk_area_display_name'] = $val['prk_area_display_name'];
    return json_encode($response);
}
?>