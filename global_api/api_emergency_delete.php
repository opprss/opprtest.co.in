<?php
/*
Description: Parking area vehicle rate delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function emergency_delete($adm_mer_dtl_id,$updated_by){
    global $pdoconn;
    $response = array();
    $sql = "UPDATE `admin_mer_dtl` SET `updated_by`='$updated_by',`updated_date`='".TIME."',`del_flag`='".FLAG_Y."' WHERE `adm_mer_dtl_id`='$adm_mer_dtl_id'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Sucessfull';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Unsucessfull';
        return json_encode($response);
    }
}
?>