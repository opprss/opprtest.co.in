<!-- ################################################
  
Description: parking ara gate modify
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php
include "all_nav/header.php";
if (isset($_SESSION['prk_admin_id'])) {

  $user_name=$_SESSION['prk_admin_id'];
  $prk_admin_id = $_SESSION['prk_admin_id'];
}

if (isset($_REQUEST["list_id"])) {
  $prk_area_gate_id = base64_decode($_REQUEST['list_id']);
  $response = array();

  $response = prk_area_gate_dtl_show($prk_area_gate_id);

  if (!empty($response)) {

    $prk_area_gate_id = $response['prk_area_gate_id']; 
    $prk_area_gate_name = $response['prk_area_gate_name'];
    $prk_area_gate_dec = $response['prk_area_gate_dec'];
    $prk_area_gate_landmark = $response['prk_area_gate_landmark'];
    $prk_area_gate_type = $response['prk_area_gate_type'];
    $effective_date = $response['effective_date'];
    $end_date = $response['end_date'];
  }

}
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>

<!-- header position -->

    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Parking Gate Update</h5>
        <!--<form id="searchBar" class="search-bar" action="index.html">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <div class="card single pd-20 pd-sm-40" >
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <div class="row mg-b-25">
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">GATE NAME <span class="tx-danger">*</span></label>
                  <input type="text" class="form-control" placeholder="Gate Name" name="prk_area_gate_name" id="prk_area_gate_name" value="<?php echo $prk_area_gate_name; ?>" onkeydown="upperCase(this)">
                  <span style="position: absolute;" id="error_prk_area_gate_name" class="error_size"></span>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">GATE TYPE <span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="prk_area_gate_type" id="prk_area_gate_type" value="" style="opacity: 0.8;">
                        <!-- <option value=""></option> -->
                        <option value="I">IN</option>
                        <option value="O">OUT</option>
                        <option value="B">BOTH</option>
                      </select>
                    </div>
                    <span id="error_prk_area_gate_type" style="position: absolute;" class="error_size"></span>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">GATE DESCRIPTION <span class="tx-danger">*</span></label>
                  <input type="text" class="form-control" placeholder="Gate description" name="prk_area_gate_dec" id="prk_area_gate_dec" value="<?php echo $prk_area_gate_dec; ?>" onkeydown="upperCase(this)">
                  <span style="position: absolute;" id="error_prk_area_gate_dec" class="error_size"></span>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-3">
                <div class="form-group">
                  <label class="form-control-label size">LANDMARK <span class="tx-danger"></span></label>
                  <input type="text" class="form-control" placeholder="Gate Landmark" name="prk_area_gate_landmark" id="prk_area_gate_landmark" value="<?php echo $prk_area_gate_landmark; ?>" onkeydown="upperCase(this)">
                  <span style="position: absolute;" id="error_prk_area_gate_landmark" class="error_size"></span>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">EFFECTIVE DATE<span class="tx-danger">*</span></label>
                  <input type="text" name="effective_date" id="effective_date" placeholder="MM/DD/YYYY" class="form-control prevent_readonly" value="<?php echo $effective_date ?>" readonly="readonly">
                  <span style="position: absolute;" id="error_effective_date" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">END DATE</label>
                  <input type="text" name="end_date" id="end_date" placeholder="MM/DD/YYYY" class="form-control prevent_readonly" value="<?php if($end_date == FUTURE_DATE_WEB) echo ''; else echo $end_date;?>" readonly="readonly">
                  <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-layout-footer mg-t-30">
                  <div class="row">
                    <div class="col-lg-6">
                      <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
                      <span style="position: absolute; color: red; padding-top: 2px;" id="error_all" class="error_size"></span>
                    </div>
                    <div class="col-lg-6">
                      <button type="button" class="btn btn-block prk_button_skip" name="skip" id="skip" onclick="document.location.href='park-gate';">Back</button>
                    </div>
                  </div>
                  
                  <!-- <button class="btn btn-secondary">Cancel</button> -->
              </div>
              </div>
          </div>
        </div>
        </div>
      </div>
      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">

  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE_WEB ?>"
  

        $( document ).ready( function () {

          $('#prk_area_gate_type option[value="<?php echo $prk_area_gate_type; ?>"]').attr('selected', true);
          /*FORM SUBMIT*/
          $('#save').click(function(){

              var prk_admin_id = "<?php echo $prk_admin_id;?>";
              var prk_area_gate_name = $('#prk_area_gate_name').val();
              var prk_area_gate_dec = $('#prk_area_gate_dec').val();
              var prk_area_gate_landmark = $('#prk_area_gate_landmark').val();
          var prk_area_gate_type =$("#prk_area_gate_type").find("option:selected").val(); 
              var effective_date = $('#effective_date').val();
              var end_date = $('#end_date').val();
               end_date = (!end_date == '') ? end_date : FUTURE_DATE;
              var user_name = "<?php echo $parking_admin_name;?>";
              var prk_area_gate_id = "<?php echo $prk_area_gate_id;?>";       
              if (prk_area_gate_name!='' &&  prk_area_gate_dec!='' && prk_area_gate_type!='' && effective_date!='') {
                var urlPrkAreGateDtlUpdate = prkUrl+'prk_area_gate_dtl_update.php';
                // alert(urlPrkAreGateDtlUpdate);
                $('#save').val('Wait ...').prop('disabled', true);
                $.ajax({
                      url :urlPrkAreGateDtlUpdate,
                      type:'POST',
                      data :
                      {
                        'prk_area_gate_id':prk_area_gate_id,
                        'prk_admin_id':prk_admin_id,
                        'prk_area_gate_name':prk_area_gate_name,
                        'prk_area_gate_dec':prk_area_gate_dec,
                        'prk_area_gate_landmark':prk_area_gate_landmark,
                        'prk_area_gate_type':prk_area_gate_type,
                        'effective_date':effective_date,
                        'end_date':end_date,
                        'user_name':user_name,
                        'token': '<?php echo $token;?>'
                      },
                      dataType:'html',
                      success  :function(data)
                      {
                        $('#save').val('SAVE').prop('disabled', false);
                        // alert(data);
                        var json = $.parseJSON(data);
                          if (json.status){
                              $.alert({
                              icon: 'fa fa-smile-o',
                              theme: 'modern',
                              title: 'Success !',
                              content: "<p style='font-size:0.9em;'>Gate updated successfully</p>",
                              type: 'green',
                              buttons: {
                                Ok: function () {
                                    window.location='park-gate';
                                }
                              }
                            });
                          }else{
                            if( typeof json.session !== 'undefined'){
                              if (!json.session) {
                                window.location.replace("logout.php");
                              }
                            }else{
                              $.alert({
                              icon: 'fa fa-frown-o',
                              theme: 'modern',
                              title: 'Error !',
                              content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                              type: 'red'
                            });
                            }
                          }
                      }
                  });
              
              }else{
                
                if (prk_area_gate_name == '') {
                  $('#error_prk_area_gate_name').text('Gate name is required');
                //return false;
                }if (prk_area_gate_type == '') {
                  $('#error_prk_area_gate_type').text('Gate type is required');
                  //return false;
                }if (prk_area_gate_dec == '') {
                  $('#error_prk_area_gate_dec').text('Gate description is required');
                  //return false;
                }if (effective_date == '') {
                  $('#error_effective_date').text('Effective date is required');
                  //return false;
                }
                return false;
              }
              
          });


          $('#prk_area_gate_type').on('change', function(){
              var gateType = "<?php echo $prk_area_gate_type; ?>";
              var effective_date = "<?php echo $effective_date; ?>";
              var end_date = "<?php echo $end_date; ?>";


              var prk_area_gate_type =$("#prk_area_gate_type").find("option:selected").val();
              if (gateType == prk_area_gate_type) {
                $('#prk_area_gate_type option[value="<?php echo $prk_area_gate_type; ?>"]').attr('selected', true);
                
                $("#effective_date").attr("value", effective_date);
                $('#end_date').attr('value', end_date);

              }else{
                $('#effective_date').attr('value', '');
                $('#end_date').attr('value', '');
              }

          });
          
           /*gate name*/
	        $("#prk_area_gate_name").blur(function () {
	          var prk_area_gate_name = $('#prk_area_gate_name').val();
	          if(prk_area_gate_name == ''){
	            $('#error_prk_area_gate_name').text('Gate name is required');
	            return false;
	          }else{
	            $('#error_prk_area_gate_name').text('');
	            return true;
	          }
	        });  
	
	        //prk_area_gate_type
	        $("#prk_area_gate_type").blur(function () {
	          var prk_area_gate_type = $('#prk_area_gate_type').val();
	          if(prk_area_gate_type == ''){
	            $('#error_prk_area_gate_type').text('Gate type is required');
	            return false;
	          }else{
	            $('#error_prk_area_gate_type').text('');
	            return true;
	          }
	        });
	        $("#prk_area_gate_dec").blur(function () {
	          var prk_area_gate_dec = $('#prk_area_gate_dec').val();
	          if(error_prk_area_gate_dec == ''){
	            $('#error_prk_area_gate_dec').text('Gate description is required');
	            return false;
	          }else{
	            $('#error_prk_area_gate_dec').text('');
	            return true;
	          }
	        });
	        /*gate decce*/
	        $("#effective_date").blur(function () {
	          var effective_date = $('#effective_date').val();
	          if(effective_date == ''){
	              $('#error_effective_date').text('Effective date is required');
	            return false;
	          }else{
	            $('#error_effective_date').text('');
	            return true;
	          }
	        });  

          /*//IMP*** don't remove
          $("#prk_area_gate_type").blur(function () {
            var prk_area_gate_type = $('#prk_area_gate_type').val();
            if(prk_area_gate_type == ''){
              $('#error_prk_area_gate_type').text('Gate type is required');
              return false;
            }else{
              $('#error_prk_area_gate_type').hide();
              return true;
            }
          });*/

      } );

        /* end velidation*/
/*date*/
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#effective_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
            if (date < now) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            $('#error_effective_date').text('');
            return {};
        } 
    });
  });

  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#effective_date').val();
    var d=new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var newdate=yy+"/"+mm+"/"+dd;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24);
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
            if (date < now_end) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            return {};
        } 
    });
  });

  $('#effective_date').focus(function(){
    $('#end_date').val('');
  });

        function gatePassUppercase() {
          var x = document.getElementById("user_gate_pass_num");
          x.value = x.value.toUpperCase();
        }
        function upperCase(a){
            setTimeout(function(){
                a.value = a.value.toUpperCase();
            }, 1);
        }
    </script>