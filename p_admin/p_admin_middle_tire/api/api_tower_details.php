<?php
/*
Description: parking area employ create.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function tower_details($prk_admin_id,$tower_name,$tower_short_name,$effective_date,$end_date,$parking_admin_name,$tower_id,$crud_type){
   $response = array();
   $allplayerdata = array();
    global $pdoconn;
    switch ($crud_type) {
        case 'I':
            $tower_id=uniqid();
            $sql="INSERT INTO `tower_details`(`tower_id`, `tower_name`, `tower_short_name`, `prk_admin_id`, `eff_date`, `end_date`, `inserted_by`, `inserted_date`) VALUES ('$tower_id','$tower_name','$tower_short_name','$prk_admin_id','$effective_date','$end_date','$parking_admin_name','".TIME."')";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Tower Save';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Tower Not Save ';
            }  
            break;
        case 'U':
            $sql="UPDATE `tower_details` SET `tower_name`='$tower_name',`tower_short_name`='$tower_short_name',`end_date`='$end_date',`updated_by`='$parking_admin_name',`updated_date`='".TIME."' WHERE `tower_id`='$tower_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Tower Modify Save';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Tower Modify Not Save ';
            }  
            break;
        case 'S':
            $sql="SELECT `tower_id`,`tower_name`,`tower_short_name`,`prk_admin_id`,`eff_date`,`end_date` FROM `tower_details` WHERE `tower_id`='$tower_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['tower_id'] =  $val['tower_id'];
                $response['tower_name'] =  $val['tower_name'];
                $response['tower_short_name'] =  $val['tower_short_name'];
                $response['prk_admin_id'] =  $val['prk_admin_id'];
                $response['eff_date'] =  $val['eff_date'];
                $response['end_date'] =  $val['end_date'];
            }else{
                $response['status'] = 0;
                $response['message'] = 'Not Successful';
            }  
            break;
        case 'LS':
            $sql="SELECT `tower_id`,`tower_name`,`tower_short_name`,`prk_admin_id`,`eff_date`,`end_date` FROM `tower_details` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."' ORDER BY `tower_unique_id` DESC";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $arr_catagory = $query->fetchAll();
                foreach($arr_catagory as $val)
                {
                    $park['tower_id'] =  $val['tower_id'];
                    $park['tower_name'] =  $val['tower_name'];
                    $park['tower_short_name'] =  $val['tower_short_name'];
                    $park['prk_admin_id'] =  $val['prk_admin_id'];
                    $park['eff_date'] =  $val['eff_date'];
                    $park['end_date'] =  $val['end_date'];
                    array_push($allplayerdata, $park);
                }
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['tower_list'] = $allplayerdata;
            }else{
                $response['status'] = 0;
                $response['message'] = 'List Empty';
            }
            break;
        case 'D':
            $sql="UPDATE `tower_details` SET `del_flag`='".FLAG_Y."',`updated_by`='$parking_admin_name',`updated_date`='".TIME."' WHERE `tower_id`='$tower_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $response['status'] = 1;
                $response['message'] = 'Delete Successful';
            }else{
                $response['status'] = 0;
                $response['message'] = 'Delete Not Successful';
            }
            break;
        default:
            $response['status'] = 0;
            $response['message'] = 'Wrong Select';
            break;
    }
    return json_encode($response);  
}
?>