<?php 
/*
Description: parking employ out vehicle
Developed by: Rakhal Raj Mandal
Created Date: 06-04-2018
Update date : 25-05-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
$re = array();
$resp = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','token'))){
 	if(isEmpty(array('prk_admin_id','prk_area_user_id','token'))){
	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_area_user_id = trim($_POST['prk_area_user_id']);
        $token = ($_POST['token']);
        $token_ch=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($token_ch);
		if($json->status=='1'){			
	        $response = visitor_parking_dtl_list($prk_admin_id);
        }else{
            $response = $token_ch;
        }
 	}else{
        $response['status'] = 0;
    	$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>