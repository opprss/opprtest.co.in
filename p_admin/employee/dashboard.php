<!-- ################################################
  
  Description: Parkig admin can register(login ID and password)/delete/change password/activate-deactivate there employee(security gurd) for there gate managment. with this login credentials parking area employee can lgin intu the parking gate managment mobile app.
  Developed by: Rakhal Raj Mandal
  Created Date: 17-03-2018

 ####################################################-->
<?php 
  include "all_nav/header.php";
  if ($veh_token_flag == FLAG_Y) {
    $dive_len=3;
  }else{
    $dive_len=4;
  }
?>
<style type="text/css">
  body{overflow-x: hidden;}
  .size{
    font-size: 11px;
    text-transform: uppercase;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
  input[placeholder] { 
    text-transform: capitalize;
  }
</style> 
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="../../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Vehicle</h5>
  </div>
</div>
<div class="am-mainpanel pd-l-1 ">
  <div class="am-pagebody row pd-t-10 pd-r-20 pd-l-20">
    <div class="card pd-20 pd-sm-40 col-sm-7"  style="padding: 10px;">
      <div class="editable tx-16 bd pd-35 tx-inverse" id="ve_in" style="padding-bottom: 0px;padding: 0px;">
        <form id="addVehicleForm" action="#" method="post">
          <input type="hidden" name="advance_flag" id="advance_flag" value="<?php if (isset($advance_flag)) echo $advance_flag; ?>">
          <input type="hidden" name="prk_area_short_name" id="prk_area_short_name" value="<?php echo HDA_SHORT; ?>">
          <div class="row pd-t-10 pd-l-10 pd-r-10">
            <div class="col-lg-<?php echo $dive_len;?>">
              <div class="form-group">
                <label class="form-control-label size">VEHICLE NUMBER <span class="tx-danger">*</span></label>
                <input type="text" class="form-control" placeholder="Vehicle Number" name="veh_number" id="veh_number" onkeyup="vehNoUppercase();" maxlength="10">
                <span style="position: absolute;" id="error_veh_number" class="error_size"></span>
              </div>
            </div>
            <div class="col-lg-<?php echo $dive_len;?>">
              <div class="form-group">
                <label class="form-control-label size">Vehicle Type<span class="tx-danger">*</span></label>
                  <div class="select">
                    <select name="vehicle_type" id="vehicle_type" value="" style="opacity: 0.8; font-size:14px">
                    </select>
                  </div>
                  <span id="error_vehicle_type" style="position: absolute;" class="error_size"></span>
              </div>
            </div>
            <div class="col-lg-<?php echo $dive_len;?>">
              <div class="form-group">
                <label class="form-control-label size">MOBILE NUMBER <span class="tx-danger"></span></label>
                <input type="text" name="mobile" id="mobile" maxlength="10" class="form-control" placeholder="Mobile Number" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"/>
                <span id="error_mobile_number" style="position: absolute;" class="error_size"></span>
              </div>
            </div>
            <div class="col-lg-<?php echo $dive_len;?>" <?php if ($veh_token_flag != FLAG_Y) { ?>style="display: none;<?php  } ?>">
              <div class="form-group">
                <label class="form-control-label size">VEHICLE TOKEN<span class="tx-danger">*</span></label>
                <input type="text" name="veh_token" id="veh_token" onkeyup="vehNoUppercase();" maxlength="10" class="form-control" placeholder="Vehicle Token">
                <span style="position: absolute;" id="error_veh_token" class="error_size"></span>
              </div>
            </div>
          </div>
          <div class="row pd-t-12 pd-t-10 pd-l-10 pd-r-10">
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Advance <span class="tx-danger"></span></label>
                <input type="text" name="advance" id="advance" class="form-control" value="<?php echo $advance_pay; ?>" placeholder="Advance" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"/>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="form-group">

                <label class="form-control-label size">PAYMENT TYPE <span class="tx-danger">*</span></label>
                  <div class="select">
                    <select name="payment_type" id="payment_type" value="" style="opacity: 0.8; font-size:14px">
                      <?php $payment_type_list=payment_type_list($prk_admin_id); 
                          $payment_type_list = json_decode($payment_type_list, true);
                          foreach ($payment_type_list as $key => $value) { ?>
                      <option value="<?php echo $value['payment_type_short']; ?>"><?php echo $value['payment_type'];?></option>
                      <?php }?>
                      <option hidden value="<?php echo DGP ?>"><?php echo DGP_F ?></option>
                    </select>
                  </div>
                  <input type="hidden" name="gate_pass_num" id="gate_pass_num" value="<?php echo PP ?>">
                  <span style="position: absolute;" id="payment_type_error" class="error_size"></span>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-layout-footer mg-t-20">
                <div class="row pd-t-10">
                  <?php
                    $in_mess='';
                    if (isset($print_in_flag)) {
                      if ($print_in_flag == ACTIVE_FLAG_Y) {
                        $in_mess='IN & PRINT';
                      }else{
                        $in_mess='IN';
                      }
                    }else{
                      $in_mess='IN';
                    }
                  ?>
                  <div class="col-lg-6">
                    <input type="button" name="" class="btn btn-block btn-primary prk_button" id="inAndPrint" value="<?php echo $in_mess; ?>">
                  </div>
                  <div class="col-lg-6">
                    <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- background-color: #fafafa" -->
    <div class="card pd-20 pd-sm-40 col-sm-5" style="padding: 10px;">        
      <div class="editable tx-16 bd pd-35 tx-inverse" style="padding-bottom: 0px;padding:0px;overflow: hidden;">
        <form id="addVehicleForm" action="#" method="post">

          <div class="row" id="i_verify">
            <div class="col-lg-1 pd-l-20 " >
              <div class="form-group pd-t-30">
                  <i class="fa fa-arrow-circle-down" id="in_logo" style="font-size:50px; color: #00b8d4"></i>
              </div>
            </div>
            <div class="col-lg-1 pd-l-35 " >
              <div class="form-group pd-t-30" id="in_veh_img">
               
              </div>
            </div>
            <div class="col-lg-4 pd-l-40" >
              <div class="form-group pd-t-30" style="margin-right: -40px;">
               <input type="text" class="form-control" placeholder="Vehicle Number" name="veh_number" id="veh_number_in" style="text-align: center;" disabled="disabled">
              </div>
            </div>
            <div class="col-lg-2 pd-l-20" >
              <div class="form-grou pd-t-20">
                  <button type="button" style="cursor:;background-color:; border: none;"  class="activate pd-l-10 btn btn-link" id="in_verify" data-toggle="tooltip" data-placement="top">
                  <i class="fa fa-check" id="in_verify_logo" style=" font-size:50px; color: gray;"></i></button>
              </div>
            </div>
            <div class="col-lg-2 pd-l-5" >
              <div class="form-group pd-t-20">
                  <button type="button" style="cursor:; background-color:; border: none;" class="activate pd-l-10 btn btn-link" id="in_rej" data-toggle="tooltip" data-placement="top">
                  <i class="fa fa-times-circle" id="in_rej_logo" style="font-size:50px; color: gray; "></i></button>
              </div>
            </div>
            <div class="col-lg-1  pd-l-1" >
              <div class="form-group pd-t-20">
                <button type="button" style="cursor: ;" class="activate pd-l-10 btn btn-link" id="in_refresh" data-toggle="tooltip" data-placement="top">
                  <i class="fa fa-refresh" id="in_refresh_logo" style="font-size:50px; color: #00b8d4; margin-left: -15px;"></i></button>
              </div>
            </div>
          </div>

          <div class="row" id="o_verify">
            <div class="col-lg-1 pd-l-20 " >
              <div class="form-group pd-t-10">
                  <i class="fa fa-arrow-circle-down" id="in_logo" style="font-size:50px; color: #00b8d4"></i>
              </div>
            </div>
            <div class="col-lg-1 pd-l-35 " >
              <div class="form-group pd-t-10" id="ou_veh_img">
               
              </div>
            </div>
            <div class="col-lg-4 pd-l-40" >
              <div class="form-group pd-t-10" style="margin-right: -40px;">
                <input type="text" class="form-control" placeholder="Vehicle Number" name="veh_number" id="veh_number_ou" style="text-align: center;" disabled="disabled">

              </div>
            </div>
            <div class="col-lg-2 pd-l-20 " >
              <div class="form-grou pd-t-0">
                  <button type="button" style="border: none;" class="activate pd-l-15 btn btn-link" id="ou_verify" data-toggle="tooltip" data-placement="top">
                  <i class="fa fa-rupee" id="ou_verify_logo" style=" font-size:50px; color: gray;"></i></button>
              </div>
            </div>
            <div class="col-lg-2 pd-l-5" >
              <div class="form-group pd-t-0">
                  <button type="button" style="border: none;" class="activate pd-l-10 btn btn-link" id="ou_rej" data-toggle="tooltip" data-placement="top">
                  <i class="fa fa-times-circle" id="ou_rej_logo" style="font-size:50px; color: gray;"></i></button>
              </div>
            </div>
            <div class="col-lg-1 pd-l-1 " >
              <div class="form-group pd-t-0">
                <button type="button" style="cursor:;" class="activate pd-l-10 btn btn-link" id="ou_refresh" data-toggle="tooltip" data-placement="top">
                  <i class="fa fa-refresh" id="ou_refresh_logo" style="font-size:50px; color: #00b8d4; margin-left: -15px;"></i></button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>     
  </div>
  <div class="am-pagebody" id="data">
    <div class="card pd-20 pd-sm-40" style="padding: 10px;">
      <div class="table-wrapper">
        <table id="datatable1" class="table display responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="wd-5p">Type</th>
              <th class="wd-10p">VEHICLE NUMBER</th>
              <th class="wd-10p">Token</th>
              <th class="wd-10p">In date</th>
              <th class="wd-10p">In Time</th>
              <th class="wd-10p text-center">Action</th>
            </tr>
          </thead>
          <tbody id="list_ve"></tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include"all_nav/footer.php"; ?>
<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="../../lib/datatables-responsive/jquery.dataTables.min.js"></script>
<script src="../../lib/datatables-responsive/dataTables.buttons.min.js"></script>
<script src="../../lib/datatables-responsive/jszip.min.js"></script>
<script src="../../lib/datatables-responsive/pdfmake.min.js"></script>
<script src="../../lib/datatables-responsive/vfs_fonts.js"></script>
<script src="../../lib/datatables-responsive/buttons.html5.min.js"></script>
<script type="text/javascript">
  var pkrEmpUrl = "<?php echo PRK_EMP_URL; ?>";
  var falgY = "<?php echo FLAG_Y; ?>";
  var CASH = "<?php echo CASH ;?>";
  var TDGP = "<?php echo TDGP ;?>";
  var PP = "<?php echo PP ;?>";
  var DGP = "<?php echo DGP ;?>";
  var BASE_URL = "<?php echo OPPR_BASE_URL ;?>";
  var prk_admin_id = "<?php echo $prk_admin_id; ?>";
  var prk_area_gate_type = "<?php echo $prk_area_gate_type ;?>";
  var print_in_flag = "<?php echo $print_in_flag ;?>";
  var print_out_flag = "<?php echo $print_out_flag ;?>";
  var veh_out_otp_flag = "<?php echo $veh_out_otp_flag ;?>";
  var veh_token_flag = "<?php echo $veh_token_flag ;?>";
  var advance_flag = "<?php echo $advance_flag ;?>";
  var advance_pay = "<?php echo $advance_pay ;?>";
  var in_mess = "<?php echo $in_mess ;?>";
  //-------------vehicle in load---------------
  $( document ).ready( function () {
    // alert(prk_area_gate_type);      
    //------------- Refresh 5sc ------------
    advance_test();
    current_veh_loadAjax();
    vehicle_out_loadAjax();
    vehicle_in_loadAjax();
    setInterval(function(){
      vehicle_out_loadAjax();// this will run after every 5 seconds 
      vehicle_in_loadAjax();// this will run after every 5 seconds 
      // var input = $('#myInput').val();
      // if(input==''){
      //   current_veh_loadAjax();
      // }
    }, 2000);
    //------------- Refresh 5sc End------------
    if (prk_area_gate_type == 'B') {        
    }else if (prk_area_gate_type == 'I'){
      $('#o_verify').css("background-color", "#d9e0e0");
      $('#veh_number_ou').css("background-color", "#d9e0e0");
      $('#ou_logo').css("color", "#343a40");
      // $('#ou_veh_img').css("color", "#343a40");
      $('#ou_refresh_logo').css("color", "#343a40");
    }else if (prk_area_gate_type == 'O'){
      // alert('ok');
      $("#ve_in *").prop('disabled',true);
      $('#ve_in').css("background-color", "#d9e0e0");
      $('#veh_number').css("background-color", "#d9e0e0");
      $('#advance').css("background-color", "#d9e0e0");
      $('#mobile').css("background-color", "#d9e0e0");
     // veh_number
      //-----------
      $('#i_verify').css("background-color", "#d9e0e0");
      $('#veh_number_in').css("background-color", "#d9e0e0");
      $('#in_logo').css("color", "#343a40");
      // $('#ou_veh_img').css("color", "#343a40");veh_number_in
      $('#in_refresh_logo').css("color", "#343a40");
    }else{
    }
      // alert('ok');
    $('#reset').click(function (){
      // alert('ok');
      $('.error_size').text('');
    });
    $('#in_refresh').click(function (){
      // alert('ok');
      vehicle_in_loadAjax();
    });
    //---------vehicle in refresh -----------------//
    $('#ou_refresh').click(function (){
      // alert('ok');
      vehicle_out_loadAjax();
    });
    //---------vehicle in refresh -----------------//

    window.history.pushState(null, "", window.location.href);        
    window.onpopstate = function() {
      window.history.pushState(null, "", window.location.href);
    };
    var urlVehicleType = pkrEmpUrl+'vehicle_type.php';
    $('[data-toggle="tooltip"]').tooltip();  

    $.ajax ({
      type: 'POST',
      url: urlVehicleType,
      data: "",
      success : function(data) {
       //alert(data);
        var obj=JSON.parse(data);
        var areaOption = "<option value='' style='color:gray;' selected='selected'>Vehicle Type</option>";
        //alert(obj['vehicle'][0][])
        $.each(obj, function(val, key) {
          // alert("Value is :" + key['e']);
          areaOption += '<option value="' + key['vehicle_Sort_nm'] + '">' + key['vehicle_type'] + '</option>'
        });

        $("#vehicle_type").html(areaOption);
      }
    });
    ///------------------------////
    $("#vehicle_type").blur(function () {
      
      vehicle_type_err();
    });
    $("#veh_token").blur(function () {
      
      veh_token_err();
    });
    $("#payment_type").blur(function () {
      
      payment_type_err();
    });
    $("#mobile").blur(function () {
      
      mobile_err(); 
    });
    $("#veh_number").blur(function () {
      
      veh_number_err();
    });

    ///-----------------
    $("#veh_number").keyup(function(){
      $('#vehicle_type').val('');
      $('#mobile').val('');
      $('#payment_type').val('');
      var cash = "<?php echo CASH ?>";
      // $('#payment_type').val(cash);
      $('#mobile').attr("disabled", false);
      $('#vehicle_type').attr("disabled", false);
      $('#payment_type').attr("disabled", false);
      // $('#advance').attr("disabled", false);
      advance_test();
      veh_number_err()
    });
    function vehNumberReturn(){
        var veh_number = $('#veh_number').val();
        var prk_admin_id = "<?php echo $prk_admin_id; ?>";
        var prk_area_user_id = "<?php echo $prk_area_user_id; ?>";
        var urlVehNoCheck = pkrEmpUrl+'prk_gate_pass_find.php';
        $.ajax({
          url :urlVehNoCheck,
          type:'POST',
          data :
          {
            'veh_number':veh_number,
            'prk_admin_id':prk_admin_id,
            'prk_area_user_id':prk_area_user_id,
            'token':"<?php echo $token ?>"
          },
          dataType:'html',
          success  :function(data)
          {
            var json = $.parseJSON(data);

            if (json.status) {
              $('#vehicle_type').val(json.veh_type);
              $('#mobile').val(json.mobile);
              $('#payment_type').val(json.payment_type);
              $('#gate_pass_num').val(json.prk_gate_pass_num);
              $('#advance').val(0);

              $('#mobile').attr("disabled", true);
              $('#vehicle_type').attr("disabled", true);
              $('#payment_type').attr("disabled", true);
              $('#payment_type').attr("disabled", true);
              $('#advance').attr("disabled", true);
              $('#error_vehicle_type').text('');;
              $('#payment_type_error').text('');
              $('#error_mobile_number').text('');;
            }
          }
        });
        return true;
    }
    /*payemnt type*/
    $( "#payment_type" ).change(function() {
      var payment_type = $(this).val();

      switch(payment_type){
        case CASH:
          $('#gate_pass_num').val(PP);
          break;
      case TDGP:
          $('#gate_pass_num').val(TDGP);
          break;
      default:
          $('#gate_pass_num').val(DGP);
      }          
    });
    $('#inAndPrint').click(function () {
      var prk_admin_id = "<?php echo $prk_admin_id; ?>";
      var prk_user_username = "<?php echo $prk_user_username; ?>";
      var prk_area_user_id = "<?php echo $prk_area_user_id; ?>";
      var token = "<?php echo $token; ?>";
      var veh_type = $('#vehicle_type').val();
      var advance_pay = $('#advance').val();
      if (advance_pay == '') {
        advance_pay = 0;
      }
      var mobile = $('#mobile').val();
      var payment_type = $('#payment_type').val();
      var veh_number = $('#veh_number').val();
      var prk_area_short_name = $('#prk_area_short_name').val();
      var advance_flag = $('#advance_flag').val();
      var gate_pass_num = $('#gate_pass_num').val();
      var veh_token = $('#veh_token').val();
      var veh_token_f ='';
      if(veh_token_flag==falgY){
        if(veh_token_err()== true){
          veh_token_f ='Y';
        }else{
          veh_token_f ='N';
        }
      }else{
          veh_token_f ='Y';
      }
      var urlVehIn = pkrEmpUrl+'draft_registration.php';
      if(vehicle_type_err()==true & veh_number_err()== true & veh_token_f=='Y' & mobile_err()==true & payment_type_err()==true){
        
        var urlVehNoCheck = pkrEmpUrl+'v_numberChecker.php';
        $.ajax({
          url :urlVehNoCheck,
          type:'POST',
          data :
          {
            'v_number':veh_number,
            'prk_admin_id':prk_admin_id
          },
          dataType:'html',
          success  :function(data)
          { 
            var json = $.parseJSON(data);
            if (json.status) {
              var urlVehIn = pkrEmpUrl+'draft_registration.php';
              $('#inAndPrint').val('Wait ...').prop('disabled', true);
              $.ajax({
                url :urlVehIn,
                type:'POST',
                data :
                {
                  'user_veh_type':veh_type,
                  'user_mobile':mobile,
                  'user_veh_number':veh_number,
                  'prk_admin_id':prk_admin_id,
                  'prk_in_rep_name':prk_user_username,
                  'payment_type':payment_type,
                  'gate_pass_num':gate_pass_num,
                  'prk_area_user_id':prk_area_user_id,
                  'token':token,
                  'advance_pay':advance_pay,
                  'prk_area_short_name':prk_area_short_name,
                  'advance_flag': advance_flag,
                  'veh_token': veh_token
                },
                dataType:'html',
                success  :function(data)
                {
                  $('#inAndPrint').val(in_mess).prop('disabled', false);
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    var print_in_flag = "<?php echo $print_in_flag ?>";
                    if (print_in_flag == "Y") {
                      var prk_veh_trc_dtl_id = json.prk_veh_trc_dtl_id;
                      urlPrint = pkrEmpUrl+"cash_memo_in_print.php?id="+prk_veh_trc_dtl_id;
                      window.open(urlPrint,'mywindow','width=850,height=600');
                       location.reload();
                    }else{
                      $.alert({
                        icon: 'fa fa-smile-o',
                        theme: 'modern',
                        title: 'Success',
                        content: "<p style='font-size:0.9em;'>Vehicle In successfully</p>",
                        type: 'green',
                        buttons: {
                          Ok: function () {
                              location.reload(true);
                              // Refresh_function();
                          }
                        }
                      });
                    }
                  }else{                        
                    if( typeof json.session !== 'undefined'){
                      if (!json.session) { 
                        // window.location.replace("logout.php");   
                        window.location.replace("logout.php");                 
                      }
                    }else{
                      $.alert({
                        icon: 'fa fa-frown-o',
                        theme: 'modern',
                        title: 'Error !',
                        content: "<p style='font-size:0.8em;'>"+json.message+"</p>",
                        type: 'red'
                      });                          
                    }
                  }
                }
              });
            }else{
              $('#error_veh_number').text('Vehicle number is invalid');
            }
          }
        });
      }
    });
    //--------- vehicle in---------
    $('#in_verify').click(function () {
      var prk_user_username = "<?php echo $prk_user_username; ?>";
      var prk_area_user_id = "<?php echo $prk_area_user_id; ?>";
      var token = "<?php echo $token; ?>";
      var user_prk_veh_varify_id = $('#in_verify').val();

      var urlVehIn = pkrEmpUrl+'prk_veh_trc_dtl.php';
      var cash_memo_in = pkrEmpUrl+'cash_memo_in.php';
      // alert(user_prk_veh_varify_id);
      if (!user_prk_veh_varify_id == "") {
        $('#in_verify').prop('disabled', true);
        $.ajax({
          url :urlVehIn,
          type:'POST',
          data :
          {
            'prk_admin_id':prk_admin_id,
            'user_prk_veh_varify_id':user_prk_veh_varify_id,
            'prk_area_user_id':prk_area_user_id,
            'token':token,
            'prk_user_username':prk_user_username
          },
          dataType:'html',
          success  :function(data)
          {
            // alert(data);
            $('#in_verify').prop('disabled', false);
            var json = $.parseJSON(data);
            if (json.status){
              var prk_veh_trc_dtl_id = json.prk_veh_trc_dtl_id;
              $.ajax({
                url :cash_memo_in,
                type:'POST',
                data :
                {
                  'prk_veh_trc_dtl_id':prk_veh_trc_dtl_id
                },
                dataType:'html',
                success  :function(data)
                {
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    var print_in_flag = "<?php echo $print_in_flag ?>";
                    if (print_in_flag == "Y") {
                      urlPrint = pkrEmpUrl+"cash_memo_in_print.php?id="+prk_veh_trc_dtl_id;
                      window.open(urlPrint,'mywindow','width=850,height=600');
                       location.reload();
                    }else{
                      $.alert({
                        icon: 'fa fa-smile-o',
                        theme: 'modern',
                        title: 'Vehicle In !',
                        content: "<table style='margin-left: 90px;text-align: left;'><tr><td>Park area name - </td><td>"+json.prk_area_name+"</td></tr><tr><td>Invoice Number - </td><td>"+json.invoice_no+"</td></tr><tr><td>Vehicle type - </td><td>"+json.vehicle_type_dec+"</td></tr><tr><td>Vehicle No. - </td><td>"+json.veh_number+"</td></tr><tr><td>In Date - </td><td>"+json.veh_in_date+"</td></tr><tr><td>In Time - </td><td>"+json.veh_in_time+"</td></tr></table>",
                        type: 'green',
                        columnClass: 'medium',
                        buttons: {
                          Ok: function () {
                             // location.reload(true);
                             Refresh_function();
                          }
                        }
                      });
                    }                    
                  }else{
                    if (!json.session) {
                      window.location.replace("logout.php");
                    }else{
                      $.alert({
                        icon: 'fa fa-frown-o',
                        theme: 'modern',
                        title: 'Error !',
                        content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                        type: 'red'
                      });
                    }
                  }
                }
              });
            }else{
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                type: 'red'
              });
            }
          }
        });
      }
    });
    //--------- vehicle in Reject---------
    $('#in_rej').click(function () {
      var prk_admin_id = "<?php echo $prk_admin_id; ?>";
      var prk_user_username = "<?php echo $prk_user_username; ?>";
      var prk_area_user_id = "<?php echo $prk_area_user_id; ?>";
      var token = "<?php echo $token; ?>";
      var user_prk_veh_varify_id = $('#in_rej').val();

      var veh_reject = pkrEmpUrl+'prk_veh_trc_reject.php';
      // alert(user_prk_veh_varify_id);
      // alert(veh_reject);
      //prk_admin_id','user_prk_veh_varify_id','prk_area_user_id','token
      if (!user_prk_veh_varify_id== "") {
        $('#in_rej').prop('disabled', true);
        $.ajax({
          url :veh_reject,
          type:'POST',
          data :
          {
            'prk_admin_id':prk_admin_id,
            'user_prk_veh_varify_id':user_prk_veh_varify_id,
            'prk_area_user_id':prk_area_user_id,
            'token':token
          },
          dataType:'html',
          success  :function(data)
          {
            $('#in_rej').prop('disabled', false);
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success',
                content: "<p style='font-size:0.9em;'>Vehicle Reject successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                      // location.reload(true);
                    Refresh_function();
                  }
                }
              });
            }else{                
              if (!json.session) {
                window.location.replace("logout.php");
              }else{
                $.alert({
                  icon: 'fa fa-frown-o',
                  theme: 'modern',
                  title: 'Error !',
                  content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                  type: 'red',
                  buttons: {
                    Ok: function () {
                        // location.reload(true);
                        Refresh_function();
                    }
                  }
                });
              }
            }
          }
        });
      }
    });
    //--------- vehicle out ---------
    $('#ou_verify').click(function () {
      var prk_admin_id = "<?php echo $prk_admin_id; ?>";
      var prk_user_username = "<?php echo $prk_user_username; ?>";
      var prk_area_user_id = "<?php echo $prk_area_user_id; ?>";
      var token = "<?php echo $token; ?>";
      var payment_dtl_id = $('#ou_verify').val();

      var urlVehIn = pkrEmpUrl+'prk_veh_trc_dtl.php';
      var prk_veh_out_dtl = pkrEmpUrl+'prk_veh_out_dtl.php';
      // alert(prk_veh_out_dtl);
      if(!payment_dtl_id == ''){
        $('#ou_verify').prop('disabled', true);
        $.ajax({
          url :prk_veh_out_dtl,
          type:'POST',
          data :
          {
            'payment_dtl_id':payment_dtl_id
          },
          dataType:'html',
          success  :function(data)
          {
            $('#ou_verify').prop('disabled', false);
            //alert(data);
            var json = $.parseJSON(data);
            var payment_type= json.payment_type
            $.confirm({
              title: 'Vehicle Out',
              content: "<table style='margin-left: 100px; text-align: left;'><tr><td>Park area name - </td><td>"+json.prk_area_name+"</td></tr><tr><td>State Name - </td><td>"+json.prk_add_state+"</td></tr><tr><td>City Name - </td><td>"+json.prk_add_city+"</td></tr><tr><td>Vehicle No. - </td><td>"+json.veh_number+"</td></tr><tr><td>In Date - </td><td>"+json.veh_in_date+"</td></tr><tr><td>In Time - </td><td>"+json.veh_in_time+"</td></tr><tr><td>Out Date - </td><td>"+json.veh_out_date+"</td></tr><tr><td>Out Time - </td><td>"+json.veh_out_time+"</td></tr><tr><td>Duration Time - </td><td>"+json.tot_hr+"</td></tr><tr><td>Round Of Time - </td><td>"+json.round_hr+"</td></tr><tr><td>Payment By - </td><td>"+json.payment_type_full+"</td></tr><tr><td>Total Payment - </td><td><b>"+json.total_amo_pay+"</b></td></tr><tr><td>Token - </td><td><b>"+json.veh_token+"</b></td></tr></table>",
              columnClass: 'medium',
              theme: 'modern',
              type: 'green',
              buttons: {
                cancel: function () {
                },
                something: {
                  text: 'Reject',
                  keys: ['Y', 'shift'],
                  action: function(){
                    payment_undifine(payment_dtl_id);                   
                  }
                },
                somethingElse: {
                  text: 'Out',
                  keys: ['Y', 'shift'],
                  action: function(){
                    //'payment_dtl_id','prk_out_rep_name','payment_type'    //payment_receive.php
                    var payment_receive = pkrEmpUrl+'payment_receive.php';
                    $.ajax({
                      url :payment_receive,
                      type:'POST',
                      data :
                      {
                        'payment_dtl_id':payment_dtl_id,
                        'prk_out_rep_name':prk_user_username,
                        'payment_type':payment_type
                      },
                      dataType:'html',
                      success  :function(data)
                      {
                        //alert(data);
                        var jsonn = $.parseJSON(data);
                        if (jsonn.status){
                          var print_out_flag = "<?php echo $print_out_flag ?>";
                          if (print_out_flag == falgY) {
                            // alert("hello");
                           // var payment_dtl_id = json.payment_dtl_id;
                            // alert(payment_dtl_id);
                            urlPrint = pkrEmpUrl+"cash_memo_out_print.php?id="+payment_dtl_id;
                            //alert(urlPrint);
                            window.open(urlPrint,'Print Window','width=850,height=600');
                            // window.open('http://www.example.com')
                            location.reload();
                            //alert("hello");
                          }else{
                            //alert('ok');
                            $.alert({
                              title: 'Vehicle Out',
                              content: "<table style='margin-left: 100px; text-align: left;'><tr><td>Park area name - </td><td>"+json.prk_area_name+"</td></tr><tr><td>State Name - </td><td>"+json.prk_add_state+"</td></tr><tr><td>City Name - </td><td>"+json.prk_add_city+"</td></tr><tr><td>Vehicle No. - </td><td>"+json.veh_number+"</td></tr><tr><td>In Date - </td><td>"+json.veh_in_date+"</td></tr><tr><td>In Time - </td><td>"+json.veh_in_time+"</td></tr><tr><td>Out Date - </td><td>"+json.veh_out_date+"</td></tr><tr><td>Out Time - </td><td>"+json.veh_out_time+"</td></tr><tr><td>Duration Time - </td><td>"+json.tot_hr+"</td></tr><tr><td>Round Of Time - </td><td>"+json.round_hr+"</td></tr><tr><td>Payment By - </td><td>"+json.payment_type_full+"</td></tr><tr><td>Total Payment - </td><td><b>"+json.total_amo_pay+"</b></td></tr><tr><td>Token - </td><td><b>"+json.veh_token+"</b></td></tr></table>",
                              columnClass: 'medium',
                              theme: 'modern',
                              type: 'green',
                              buttons: {
                                Ok: function () {
                                   // location.reload(true);
                                  Refresh_function();
                                }
                              }
                            });
                          }
                        }else{
                          $.alert({
                            icon: 'fa fa-frown-o',
                            theme: 'modern',
                            title: 'Error !',
                            content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                            type: 'red'
                          });
                        }
                      }
                    });
                  }
                }
              }
            });
          }
        });
      }
    });
    $('#ou_rej').click(function () {
      var payment_dtl_id = $('#ou_rej').val();
      // alert(payment_dtl_id);
      if(!payment_dtl_id == ""){
        $('#ou_rej').prop('disabled', true);
        payment_undifine(payment_dtl_id);
        $('#ou_rej').prop('disabled', false);
      }
    });
    function payment_undifine(payment_dtl_id) {
      var payment_undifine = pkrEmpUrl+'prk_payment_undifine_update.php';
      $.ajax({
        url :payment_undifine,
        type:'POST',
        data :
        {
          'payment_dtl_id':payment_dtl_id
        },
        dataType:'html',
        success  :function(data)
        {
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success',
              content: "<p style='font-size:0.9em;'>Vehicle Reject successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                    // location.reload(true);
                    Refresh_function();
                }
              }
            });
          }else{
            $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
              type: 'red',
              buttons: {
                Ok: function () {
                    // location.reload(true);
                    Refresh_function();
                }
              }
            });
          }
        }
      });
    }
    $("#veh_number").on('input', function(evt) {
      var input = $(this);
      var start = input[0].selectionStart;
      $(this).val(function (_, val) {
        return val.toUpperCase();
      });
      input[0].selectionStart = input[0].selectionEnd = start;
    });
    function vehicle_type_err() {
      var veh_type = $('#vehicle_type').val();
      if(veh_type==''){
        $('#error_vehicle_type').text('Vehicle type is required');
        return false;
      }else{
        $('#error_vehicle_type').text('');
        return true;
      }
    }
    function veh_token_err() {
      var veh_token = $('#veh_token').val();
      if(veh_token==''){
        $('#error_veh_token').text('Vehicle Token is required');
        return false;
      }else{
        $('#error_veh_token').text('');
        return true;
      }
    }
    function payment_type_err() {
      var payment_type = $('#payment_type').val();
      if(payment_type==''){
        $('#payment_type_error').text('Payment type is required');
        return false;
      }else{
        $('#payment_type_error').text('');
        return true;
      }
    }
    function mobile_err() {
      var veh_token = $('#mobile').val();
      var mob_len = veh_token.length;
      if(veh_out_otp_flag==falgY){
        if(veh_token==''){
          $('#error_mobile_number').text('Mobile Number is required');
          return false;
        }else{
          if(mob_len>=10){
            $('#error_mobile_number').text('');;
            return true;
          }else{
            $('#error_mobile_number').text('Mobile number must be 10 digits');
            return false;
          }
        }
      }else{
        if(mob_len>=10 || veh_token==''){
          $('#error_mobile_number').text('');;
          return true;
        }else{
          $('#error_mobile_number').text('Mobile number must be 10 digits');
          return false;
        }
      }
    }
    function veh_number_err() {
      var veh_number = $('#veh_number').val();
      if(veh_number==''){
        $('#error_veh_number').text('Vehicle number is required');
        return false;
      }else{
        var urlVehNoCheck = pkrEmpUrl+'v_numberChecker.php';
        $.ajax({
          url :urlVehNoCheck,
          type:'POST',
          data :
          {
            'v_number':veh_number,
            'prk_admin_id':prk_admin_id
          },
          dataType:'html',
          success  :function(data)
          {
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status) {
              $('#error_veh_number').text('');
              vehNumberReturn();
              return true;
            }else{
              $('#error_veh_number').text('Vehicle number is invalid');
              return false;
            }
          }
        });
        return true;
      }
    }
    $('#payment_type').on("change", function() {
      var payment_t=($("#payment_type").val());
      // alert(payment_t)
      if(payment_t=='TDGP'){
        $('#advance').attr("disabled", true);
        $('#advance').val('0');
      }else{
        advance_test();
      }
    });
  });
  function advance_test() {
    if(advance_flag==falgY){
      $('#advance').attr("disabled", false);
      $('#advance').val(advance_pay);
    }else{
      $('#advance').attr("disabled", true);
      $('#advance').val('0');
    }
  }
  function cu_veh_out(str) {
    var prk_admin_id = "<?php echo $prk_admin_id;?>";
    var prk_out_rep_name = "<?php echo $prk_user_username;?>";
    var prk_area_user_id = "<?php echo $prk_area_user_id;?>";
    var prk_area_gate_id = "<?php echo $prk_area_gate_id;?>";
    var token = "<?php echo $token;?>";
    var prk_veh_trc_dtl_id = str;
    // alert(prk_veh_trc_dtl_id);
    if (veh_out_otp_flag==falgY) {
      $.confirm({
        title: 'Vehicle Out OTP',
        useBootstrap: false,
        boxWidth: '30%',
        theme: 'modern',
        content: '' +
        '<form action="" class="formName">' +
        '<div class="form-group">' +
        '<input type="text"  maxlength="6" onkeypress="return alphamobile(event,number3);" placeholder="Vehicle Out Otp" class="confirm_password form-control" required />' +
        '</div>' +
        '</form>',
        buttons: {
          formSubmit: {
            text: 'OUT',
            btnClass: 'btn-green',
            action: function () {
              var otp = this.$content.find('.confirm_password').val();
              if(otp == '' ){
                $.alert({
                  title:'Error',
                  theme: 'modern',
                  content: '<p style="font-size:0.9em;">OTP is empty.</p>',
                  type: 'red'
                });
                  return false;
              }
              if(otp.length < 6){
                // $('this').attr("disabled", true);
                // return false;
                $.alert({
                  title:'Error',
                  theme: 'modern',
                  content: '<p style="font-size:0.9em;">OTP must be 6 characters long.</p>',
                  type: 'red'
                });
                return false;
              }
              // alert(otp);
              var urlEmpForgetPass = pkrEmpUrl+'veh_out_otp_check.php';
              // alert(urlEmpForgetPass);
              $.ajax({
                url :urlEmpForgetPass,
                type:'POST',
                data :
                {
                  'prk_veh_trc_dtl_id':prk_veh_trc_dtl_id,
                  'veh_out_otp':otp
                },
                dataType:'html',
                success  :function(data)
                {
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    // alert('ok');
                    prk_veh_trn_dtl_web(prk_veh_trc_dtl_id,prk_admin_id,prk_out_rep_name,prk_area_user_id,prk_area_gate_id,token);
                  }else{
                    $.alert({
                      title:'Error',
                      theme: 'modern',
                      content: '<p style="font-size:0.9em;">Otp Not Match.</p>',
                      type: 'red'
                    });
                    return false;            
                  }
                }
              });
            }
          },
          something: {
            text: 'Resend OTP',
            keys: ['Y', 'shift'],
            action: function(){
              // alert('ok');   
              var urlPrkVehTrnDtl = pkrEmpUrl +'prk_veh_trn_in_mess.php';
              $.ajax({
                url :urlPrkVehTrnDtl,
                type:'POST',
                data :
                {
                  'prk_veh_trc_dtl_id':prk_veh_trc_dtl_id
                },
                dataType:'html',
                success  :function(data)
                {
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>OTP Send successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                            // location.reload(true);
                            Refresh_function();
                        }
                      }
                    });
                  }else{
                     $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.7em;'>Somting went wrong 1</p>",
                      type: 'red'
                    });
                  }
                }
              });              
            }
          },
          cancel: function () {
              //close
          },
        },
        onContentReady: function () {
          var jc = this;
          this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
          });
        }
      });
    }else{
      $.confirm({
        title: 'Are You Sure?',
        content: "<p style='font-size:0.9em;'>It will end the current trip</p>",
        theme: 'modern',
        type: 'red',
        buttons: {
          cancel: function () {
          },
          somethingElse: {
            text: 'Out',
            btnClass: 'btn-red',
            keys: ['Y', 'shift'],
            action: function(){
              prk_veh_trn_dtl_web(prk_veh_trc_dtl_id,prk_admin_id,prk_out_rep_name,prk_area_user_id,prk_area_gate_id,token);
            }
          }
        }
      });
    }
  }
  function cu_veh_out_pi(str) {
    // alert(str);
     var print_in_flag = "<?php echo $print_in_flag ?>";
      if (print_in_flag == "Y") {
        urlPrint = pkrEmpUrl+"cash_memo_in_print.php?id="+str+'&duplicate='+falgY;
        window.open(urlPrint,'mywindow','width=850,height=600');
         location.reload();
      }
  }
  function prk_veh_trn_dtl_web(prk_veh_trc_dtl_id,prk_admin_id,prk_out_rep_name,prk_area_user_id,prk_area_gate_id,token){
    var urlPrkVehTrnDtl = pkrEmpUrl +'prk_veh_trn_dtl_web.php';
      var prk_veh_out_dtl = pkrEmpUrl+'prk_veh_out_dtl.php';
    // alert(urlPrkVehTrnDtl);
    $.ajax({
      url :urlPrkVehTrnDtl,
      type:'POST',
      data :
      {
        'prk_veh_trc_dtl_id':prk_veh_trc_dtl_id,
        'prk_admin_id':prk_admin_id,
        'prk_out_rep_name':prk_out_rep_name,
        'prk_area_user_id':prk_area_user_id,
        'prk_area_gate_id':prk_area_gate_id,
        'token':token
      },
      dataType:'html',
      success  :function(data)
      {
        // alert(data);
        var json = $.parseJSON(data);
        if (json.status){
          var print_out_flag = "<?php echo $print_out_flag ?>";
          var payment_dtl_id = json.payment_dtl_id;
          if (print_out_flag == falgY) {
            urlPrint = pkrEmpUrl+"cash_memo_out_print.php?id="+payment_dtl_id;
            window.open(urlPrint,'Print Window','width=850,height=600');
            location.reload();
          }else{
            $.ajax({
              url :prk_veh_out_dtl,
              type:'POST',
              data :
              {
                'payment_dtl_id':payment_dtl_id
              },
              dataType:'html',
              success  :function(data)
              {
                var json = $.parseJSON(data);
                $.alert({
                  icon: 'fa fa-smile-o',
                  theme: 'modern',
                  title: 'Success !',
                  content: "<table style='margin-left: 100px; text-align: left;'><tr><td>Park area name - </td><td>"+json.prk_area_name+"</td></tr><tr><td>State Name - </td><td>"+json.prk_add_state+"</td></tr><tr><td>City Name - </td><td>"+json.prk_add_city+"</td></tr><tr><td>Vehicle No. - </td><td>"+json.veh_number+"</td></tr><tr><td>In Date - </td><td>"+json.veh_in_date+"</td></tr><tr><td>In Time - </td><td>"+json.veh_in_time+"</td></tr><tr><td>Out Date - </td><td>"+json.veh_out_date+"</td></tr><tr><td>Out Time - </td><td>"+json.veh_out_time+"</td></tr><tr><td>Duration Time - </td><td>"+json.tot_hr+"</td></tr><tr><td>Round Of Time - </td><td>"+json.round_hr+"</td></tr><tr><td>Payment By - </td><td>"+json.payment_type_full+"</td></tr><tr><td>Total Payment - </td><td><b>"+json.total_amo_pay+"</b></td></tr><tr><td>Token - </td><td><b>"+json.veh_token+"</b></td></tr></table>",
                  columnClass: 'medium',
                  theme: 'modern',
                  type: 'green',
                  buttons: {
                    Ok: function () {
                      window.location='dashboard';
                    }
                  }
                });
              }
            });
            
          }
        }else{
          if (json.session==0) {
            window.location.replace("logout.php");
          }
          $.alert({
            icon: 'fa fa-frown-o',
            theme: 'modern',
            title: 'Error !',
            content: "<p style='font-size:0.7em;'>Vehicle Already Out</p>",
            type: 'red'
          });
        }
      }
    });
  }
  //------------- current vehicle list ------------//
  function current_veh_loadAjax(){
    $('#refresh_button').show();
    var current_veh = pkrEmpUrl+'prk_veh_trc_dtl_list.php';
    var prk_user_name = "<?php echo $prk_user_username; ?>";
    var prk_admin_id = "<?php echo $prk_admin_id; ?>";
    var prk_area_gate_id = "<?php echo $prk_area_gate_id; ?>";
    var prk_area_user_id = "<?php echo $prk_area_user_id; ?>";
    var token = "<?php echo $token; ?>";
    //prk_admin_id','prk_area_user_id','prk_area_gate_id','token'
    $.ajax({
      url :current_veh,
      type:'POST',
      data :{
        'prk_admin_id':prk_admin_id,
        'prk_area_user_id':prk_area_user_id,
        'prk_area_gate_id':prk_area_gate_id,
        'token':token
      },
      dataType:'html',
      success  :function(data){
        // alert(data);
        var demoLines = '';
        var json = $.parseJSON(data);
        if (json.status){
          // alert('data');
          var c = 0;
          for (var key in json.parking) {
            c +=1;
            var keyy = json.parking[key];
            var img_url= "<?php echo OPPR_BASE_URL; ?>"+keyy['vehicle_typ_img'];
            demoLines += '<tr>\
              <td> <img src="'+img_url+'" style="height: 40px;"></td>\
              <td>'+keyy['veh_number']+'</td>\
              <td>'+keyy['veh_token']+'</td>\
              <td>'+keyy['prk_veh_in_date']+'</td>\
              <td>'+keyy['prk_veh_in_time']+'</td>\
              <td class="text-center">';

              if(prk_area_gate_type == 'I'){
                var dis_val = 'N';
              }else{
                if(keyy['veh_out_verify_flag'] == 'Y'){
                  var dis_val = 'Y';
                }else{
                  var dis_val = 'N';
                }
              }
            if(dis_val == falgY){

              demoLines += '<button style="color:white; background-color:red;" type="button" class="out btn prk_button" id="'+keyy['prk_veh_trc_dtl_id']+'" onclick="cu_veh_out(this.id)" data-toggle="tooltip" data-placement="top"><i class="fa fa-sign-out"></i> OUT</button>';
            }else{

              demoLines += '<button style="color:white; background-color:#808080;" type="button" class="out btn prk_button" id="'+keyy['prk_veh_trc_dtl_id']+'" onclick="cu_veh_out(this.id)" data-toggle="tooltip" data-placement="top" disabled><i class="fa fa-sign-out"></i> OUT</button>';
            }
            if(print_in_flag == falgY){

              demoLines += '<button class="out btn prk_button print" id="'+keyy['prk_veh_trc_dtl_id']+'" onclick="cu_veh_out_pi(this.id)" style="color: #FFF;"><i class="fa fa-print"></i> PRINT</button>';
            }else{

              demoLines += '<button class="out btn prk_button print" id="'+keyy['prk_veh_trc_dtl_id']+'" onclick="cu_veh_out_pi(this.id)" style="color:white; background-color:#808080;" disabled><i class="fa fa-print"></i> PRINT</button>';
            }
            demoLines += '</td></tr>';
          }
        }else{
          if( typeof json.session !== 'undefined'){
            if (!json.session) { 
              //window.location.replace("logout.php");  
              window.location.replace("logout.php");                  
            }
          }
        }
        $('#datatable1').dataTable().fnDestroy();
        $("#list_ve").html(demoLines);
        datatable_show();
      }
    });
  }
  //---------vehicle in refresh-----------------//
  function vehicle_in_loadAjax(){
    // alert("calling");
    var prk_veh_verify_show = pkrEmpUrl+'user_prk_veh_verify_show.php';
    var prk_user_name = "<?php echo $prk_user_username; ?>";
    var prk_admin_id = "<?php echo $prk_admin_id; ?>";
    var prk_area_gate_id = "<?php echo $prk_area_gate_id; ?>";
    var prk_area_user_id = "<?php echo $prk_area_user_id; ?>";
    var token = "<?php echo $token; ?>";
    $.ajax({
      url :prk_veh_verify_show,
      type:'POST',
      data :
      {
        'prk_user_name':prk_user_name,
        'prk_admin_id':prk_admin_id,
        'prk_area_gate_id':prk_area_gate_id,
        'prk_area_user_id':prk_area_user_id,
        'token':token
      },
      dataType:'html',
      success  :function(data)
      {
        // alert(data);
        var json = $.parseJSON(data);
        if (json.status) {
          // alert('ok');
          $('#veh_number_in').val(json.user_veh_number);
          $('#in_verify').val(json.user_prk_veh_varify_id);
          $('#in_rej').val(json.user_prk_veh_varify_id);
          //$('#in_verify').title('Verify');
          var vehicle_typ_img=BASE_URL+''+json.vehicle_typ_img;
          var ajax_image = "<img src='"+vehicle_typ_img+"' class='wd-45 rounded-circle' alt='Loading...' />";
          $('#in_veh_img').html(ajax_image);
          $('#in_logo').css("color", "#00b8d4");
          $('#in_verify_logo').css("color", "#00b8d4");
          $('#in_rej_logo').css("color", "#fd3131");
          $('#in_rej').css("color", "#fd3131");
          $('#in_verify').prop('disabled', false);
          $('#in_rej').prop('disabled', false);
        }else{
          if (!json.session) {
           window.location.replace("logout.php");
          }else{
            var vehicle_typ_img=BASE_URL+'img/parkingimage.png';
            // alert(vehicle_typ_img);
            var ajax_image = "<img src='"+vehicle_typ_img+"' style='color:#343a40;'; class='wd-45 rounded-circle' alt='Loading...' />";
            $('#in_veh_img').html(ajax_image);
            $('#in_verify_logo').css("color", "gray");
            $('#in_rej_logo').css("color", "gray");
            $("#dcacl").attr('disabled','disabled');
            $('#in_verify').prop('disabled', true);
            $('#in_rej').prop('disabled', true);
          }
          $('#veh_number_in').val('');

        }
      }
    });
  }
  //---------vehicle out refresh -----------------//
  function vehicle_out_loadAjax(){
    // alert("calling");
      
    var veh_trc_dtl_list = pkrEmpUrl+'payment_dtl_show.php';
    var prk_user_name = "<?php echo $prk_user_username; ?>";
    var prk_admin_id = "<?php echo $prk_admin_id; ?>";
    var prk_area_gate_id = "<?php echo $prk_area_gate_id; ?>";
    var prk_area_user_id = "<?php echo $prk_area_user_id; ?>";
    var token = "<?php echo $token; ?>";
    $.ajax({
      url :veh_trc_dtl_list,
      type:'POST',
      data :
      {
        'prk_admin_id':prk_admin_id,
        'payment_rec_emp_name':prk_user_name,
        'prk_area_gate_id':prk_area_gate_id,
        'prk_area_user_id':prk_area_user_id,
        'token':token
      },
      dataType:'html',
      success  :function(data)
      {
        // alert(data);
        var json = $.parseJSON(data);
        if (json.status) {
          $('#veh_number_ou').val(json.veh_number);
          $('#ou_verify').val(json.payment_dtl_id);
          $('#ou_rej').val(json.payment_dtl_id);
          var vehicle_typ_img=BASE_URL+''+json.vehicle_typ_img;
          var ajax_image = "<img src='"+vehicle_typ_img+"' class='wd-45 rounded-circle' alt='Loading...' />";
          $('#ou_veh_img').html(ajax_image);
          $('#ou_verify_logo').css("color", "#00b8d4");
          $('#ou_rej_logo').css("color", "#fd3131");
          $('#ou_verify').prop('disabled', false);
          $('#ou_rej').prop('disabled', false);
        }else{
          if (!json.session) {
            window.location.replace("logout.php");
          }else{
            $('#veh_number_ou').val("");
            var vehicle_typ_img=BASE_URL+'img/parkingimage.png';
            var ajax_image = "<img src='"+vehicle_typ_img+"' style='color:#343a40;'; class='wd-45 rounded-circle' alt='Loading...' />";
            $('#ou_veh_img').html(ajax_image);
            $('#ou_verify_logo').css("color", "gray");
            $('#ou_rej_logo').css("color", "gray"); 
            $('#ou_verify').prop('disabled', true);
            $('#ou_rej').prop('disabled', true);               
          }
        }
      }
    });
  }
  function Refresh_function() {
    // body...
    vehicle_out_loadAjax();
    vehicle_in_loadAjax();
    current_veh_loadAjax();
  }
  function datatable_show(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      language: {
      searchPlaceholder: 'Search Here',
      sSearch: '',
      lengthMenu: '_MENU_ Page',
      },
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      buttons: [
        {
          text: '<span id="refresh_button"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
          action: function ( e, dt, node, config ) {
              // alert( 'Button activated' );
              current_veh_loadAjax();
          }
        },
        { 
          extend: 'copyHtml5', 
          //footer: true,
          title: 'CURRENT VEHICLE SHEET' 
        },
        { 
          extend: 'excelHtml5', 
          //footer: true,
          title: 'CURRENT VEHICLE SHEET'  
        },
        {
          extend: 'pdfHtml5',
          //footer: true,
          title: 'CURRENT VEHICLE SHEET',
          customize: function(doc) {
            doc.content[1].margin = [ 0, 0, 0, 0 ]
          }
        },
      ]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    $('#refresh_button').hide();
  }
</script>
<script>
  function upperCase(a){
    setTimeout(function(){
        a.value = a.value.toUpperCase();
    }, 1);
  }
  function lowerCase(a){
    setTimeout(function(){
      //alert(a);
        a.value = a.value.toLowerCase();
    }, 1);
  }
  $("#name").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
      event.preventDefault();
    }
  });
  $('#myInput').keyup(function(){

    this.value = this.value.toUpperCase();
  });
  $('#veh_token').keyup(function(){

    this.value = this.value.toUpperCase();
  });
</script>
<script type="text/javascript">
  var number3="0123456789";
  var string3="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-. ";
  function alphamobile(e,allow){
    var k;
    k=document.all?String(e.keyCode): String(e.which);
    return (allow.indexOf(String.fromCharCode(k))!=-1||k==8||k==9||k==13);
  }
</script>
