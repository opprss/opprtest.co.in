<?php
session_start();
require_once ('../../library/jpgraph/src/jpgraph.php');
require_once ('../../library/jpgraph/src/jpgraph_bar.php');
include('api/graph.php');  
	$start_date = $_POST['start_date'];
	$end_date= $_POST['end_date'];
	$width = 600;
	$height = 350;
	@$prk_admin_id = $_SESSION['prk_admin_id'];

$response = vehicle_chart_by_date($start_date,$end_date,$prk_admin_id);
$json = json_decode($response,true);
// print_r($json);
// die();
$two = $json['2W']; 
$three = $json['3W'];
$four = $json['4W'];
$eight = $json['8W'];

if ($two == 0 && $three == 0 && $four == 0 && $eight == 0) {
	echo "Result is Blank";
	die();
}
$data1y=array($two,$three,$four,$eight);
$graph = new Graph(600,400);
$graph->SetScale('textlin');

$graph->SetShadow();
$graph->SetMargin(40,30,20,40);

$b1plot = new BarPlot($data1y);
$b1plot->SetFillColor("white");

$graph->title->Set('Rank Of Vehicles');
 
$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->xaxis->SetTickLabels(array('2W','3W','4W','8W'));
$graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
$graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);
 $b1plot= new BarPlot($data1y);

$graph->Add($b1plot);
$b1plot->value->Show();
 foreach ($data1y as $datayvalue) {
    if ($datayvalue < '10') $barcolors[]='red';
    elseif ($datayvalue > '10' && $datayvalue < '20') $barcolors[]='green';
     elseif ($datayvalue >= '20' && $datayvalue < '40') $barcolors[]='blue';
    elseif ($datayvalue >= '40') $barcolors[]='yellow';
}
$gdImgHandler = $graph->Stroke(_IMG_HANDLER);
$fileName = "images/bar_chart_dashboard/imagefile.png";
$graph->img->Stream($fileName);
// Display the graph
//$graph->Stroke();
$b1plot->SetFillColor($barcolors);
$img = $graph->Stroke(_IMG_HANDLER);
ob_start();
imagepng($img);
$img_data = ob_get_contents();
ob_end_clean();
echo '<img width="'.$width.'" height="'.$height.'" src="data:image/png;base64,';
echo base64_encode($img_data);
echo '"/>';
/*session_start();
require_once ('../jpgraph/src/jpgraph.php');
require_once ('../jpgraph/src/jpgraph_bar.php');
// include('web/vehicle_chart_by_date.php');
 
include('api/graph.php'); 

//if (isset($_POST['start_date']) && isset($_POST['end_date'])) {
	$start_date = $_POST['start_date'];
	$end_date= $_POST['end_date'];
	$width = 600;
	$height = 350;
	@$prk_admin_id = $_SESSION['prk_admin_id'];

$response = vehicle_chart_by_date($start_date,$end_date,$prk_admin_id);
$json = json_decode($response,true);

$two = $json['2W']; 
$three = $json['3W'];
$four = $json['4W'];
$eight = $json['8W'];
if ($two == 0 && $three == 0 && $four == 0 && $eight == 0) {
	echo "Result is Blank";
	die();
}
$data1y=array($two,$three,$four,$eight);
//print_r($data1y);
//die();
$barcolors = array();

$graph = new Graph(550,400,'auto');
$graph->SetScale("textlin");

$theme_class=new UniversalTheme;
$graph->SetTheme($theme_class);

$graph->yaxis->SetTickPositions(array(0,30,60,90,120,150), array(15,45,75,105,135));
$graph->SetBox(false);

$graph->ygrid->SetFill(false);
$graph->xaxis->SetTickLabels(array('2W','3W','4W','8W'));
$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);
// Create the bar plots
$b1plot = new BarPlot($data1y);
// Create the grouped bar plot
$gbplot = new GroupBarPlot(array($b1plot));
// ...and add it to the graPH
$graph->Add($gbplot);

foreach ($data1y as $datayvalue) {
    if ($datayvalue < '8') $barcolors[]='red';
    elseif ($datayvalue >= '8' && $datayvalue < '18') $barcolors[]='yellow';
    elseif ($datayvalue >= '18') $barcolors[]='green';
}
$b1plot->SetColor("white");
$b1plot->SetFillColor($barcolors);
$graph->title->Set("Vehicle List");
$gdImgHandler = $graph->Stroke(_IMG_HANDLER);
 
// Stroke image to a file and browser
 
// Default is PNG so use ".png" as suffix
$fileName = "images/bar_chart_dashboard/imagefile.png";
$graph->img->Stream($fileName);
// Display the graph
//$graph->Stroke();
$img = $graph->Stroke(_IMG_HANDLER);
ob_start();
imagepng($img);
$img_data = ob_get_contents();
ob_end_clean();
echo '<img width="'.$width.'" height="'.$height.'" src="data:image/png;base64,';
echo base64_encode($img_data);
echo '"/>';


*/
?>