<?php 
/*
Description: vehicle in parking area
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
require_once 'api/reg_api.php';
// require_once 'api/global_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','user_admin_id','prk_veh_type','veh_number','prk_in_rep_name','prk_user_username'))){
 	if(isEmpty(array('prk_admin_id','user_admin_id','prk_veh_type','veh_number','prk_in_rep_name','prk_user_username'))){

	 	$prk_admin_id = trim($_POST['prk_admin_id']);      
        $user_admin_id = trim($_POST['user_admin_id']);
	 	$prk_veh_type = trim($_POST['prk_veh_type']);
        $veh_number = trim($_POST['veh_number']);
	 	$prk_in_rep_name = trim($_POST['prk_in_rep_name']);
        $prk_user_username = trim($_POST['prk_user_username']);

            $respon=prk_veh_trc_dtl($prk_admin_id,$user_admin_id,$prk_veh_type,$veh_number,$prk_user_username,$prk_in_rep_name);
            echo ($respon);
        
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?>