<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function connect_flat_master($prk_admin_id,$user_name,$user_mobile,$user_email,$user_gender,$tower_id,$flat_id,$inserted_by,$oth1,$oth2,$oth3,$oth4,$oth5){
    $response = array();
    global $pdoconn;
    $sql="SELECT `flat_master_id` FROM `flat_master` WHERE `tower_id`='$tower_id' AND `flat_id`='$flat_id' AND `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $response['status'] = 1;
        $response['message'] = 'Already Flat Master Connect';
    }else{
        $crud_type='I';
        $res_mem_id=$oth1=$oth2=$oth3=$oth4=$oth5='';
        $resp=resident_member_details($prk_admin_id,$user_name,$user_mobile,$user_email,$user_gender,$inserted_by,$crud_type,$res_mem_id,$oth1,$oth2,$oth3,$oth4,$oth5);
        $json = json_decode($resp);
        if($json->status){
            $res_mem_id=$json->res_mem_id;
            $owner_id=$tenant_id=$flat_master_id='';
            $living_status='R';
            $effective_date=TIME_TRN;
            $end_date=FUTURE_DATE;
            $two_wh_spa_allw='N';
            $four_wh_spa_allw='N';
            $app_use='N';
            $club_membership='N';
            $two_wh_spa='0';
            $four_wh_spa='0';
            $oth1=$oth2=$oth3=$oth4=$oth5='';
            $flat_mas_add=flat_master_add($prk_admin_id,$tower_id,$flat_id,$owner_id,$living_status,$tenant_id,$effective_date,$end_date,$inserted_by,$flat_master_id,$two_wh_spa_allw,$four_wh_spa_allw,$two_wh_spa,$four_wh_spa,$user_mobile,$user_name,$app_use,$res_mem_id,$club_membership,$oth1,$oth2,$oth3,$oth4,$oth5);
            $response['status'] = 1;
            $response['flat_mas_add'] = $flat_mas_add;
            $response['message'] = 'Flat Master Connect Sucessfull';
        }else{
            $response['status'] = 0;
            $response['message'] = 'Not Sucessfull';
        }
    }
    return json_encode($response);
}
?>