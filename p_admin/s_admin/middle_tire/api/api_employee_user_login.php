<?php
    /*
    Description: Oppr user Login.
    Developed by: Rakhal Raj Mandal
    Created Date: -------
    Update date :27-11-2018
    */ 
    function employee_user_login($super_admin_user,$super_admin_password){
        global $pdoconn;
        $response = array();
        $sql = "SELECT * FROM `employee_user_login` WHERE `employee_user_name` = '".$super_admin_user."'  AND `employee_user_password` = '".$super_admin_password."' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $count=$query->rowCount();
        if($count>0){
            $val = $query->fetch();
            $response['status'] = 1; 
            $_SESSION["employee_user_id"]=$response['employee_user_id'] = $val['employee_user_id'];
            $_SESSION["employee_user_name"]=$response['employee_user_name'] = $val['employee_user_name'];
            $response['message'] = 'login Sucessful';
        }else{
            $response['status'] = 0;
            $response['message'] = 'Username or Password is wrong';
        }
        return json_encode($response);
    }
?>