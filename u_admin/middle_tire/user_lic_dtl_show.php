<?php 
/*
Description: user licence dtails.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
	require_once 'api/reg_api.php';
	// require_once 'api/global_api.php';
	$response = array();
	if(isAvailable(array('user_lic_id'))){
	 	if(isEmpty(array('user_lic_id'))){

		 	$user_lic_id = trim($_POST['user_lic_id']);
	        $response =user_lic_dtl_show($user_lic_id);
	        echo ($response);
	 	}else{
	            $response['status'] = 0;
		 		$response['message'] = 'All Fields Are Mandatory';
	            echo json_encode($response);
	    }
	}else{ 
	    $response['status'] = 0; 
	    $response['message'] = 'Invalid API Call';
	 	echo json_encode($response);
	}
?>