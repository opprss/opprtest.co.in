<!-- ################################################
  
Description: this page takes two dates and according to dates generates a graph. 
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php include "all_nav/header.php"; ?>
<style type="text/css">
  .size{
    font-size: 11px;
  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
</style> 
<div class="am-mainpanel"><!-- closing in footer -->
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Earning Graph</h5>
  </div><!-- am-pagetitle -->
</div>

    <div class="am-mainpanel">
      <div class="am-pagebody">

        <div class="card single pd-20 pd-sm-40">
          <h6 class="card-body-title">Generate graph by dates</h6>
          <div class="row">
            <div class="col-md-3">
              <form action="earning-graph-pdf.php" method="post">
                <div class="row">
                  <div class="col-md-12">
                    <input type="text" name="start_date" class="form-control prevent_readonly" placeholder="Enter date" required id="start_date" readonly="readonly">
                    <input type="hidden" name="img_value" id="img_value">
                  </div>
                </div>
                <div class="row pd-t-20">
                  <div class="col-md-6">
                    <button type="button" class="btn btn-info btn-block" name="submit" id="submit" >Submit</button>
                  </div>
                  <div class="col-md-6">
                    <button type="submit" class="btn prk_button_skip btn-block" name="print" id="print">Print</button>
                  </div>
                </div>
              </form>
            </div>
            <div class="col-md-9 text-center" id="graph">
            <!-- <?php// echo "<img src='web/dashboard_graph.php' width=580 height=380>"; ?> -->
                <div class="tx-center">
                  <div id="container" style="width: 100%;">
                    <canvas id="canvas"></canvas>
                    <div id="blank">Please Select Date</div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
<?php include"all_nav/footer.php"; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="../js/ResizeSensor.js"></script>
<script src="../js/dashboard.js"></script>
<script src="../lib/chart.js/Chart.bundle.js"></script>
<script src="../lib/chart.js/utils.js"></script>
<script>
  /*date*/
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#start_date', {
      default_date   : false,
      hide_on_select : true
    });
  });
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#start_date').val();
    var d=new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var newdate=yy+"/"+mm+"/"+dd;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24);
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
        if (date < now_end) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        return {};
      } 
    });
  });
  $('#start_date').focus(function(){
  $('#end_date').val('');
  });
  var prkUrl = "<?php echo PRK_URL; ?>"; 
  $(document).ready(function(){
    $('#print').attr("disabled", true);
    var today = "<?php echo TIME_TRN ?>";
    createGraph(today);
    $('#submit').click(function(){
      var today = $('#start_date').val();  
      createGraph(today);
    });
  });
  function createGraph(today){
    var ctx_live = document.getElementById("canvas");
    var myChart = new Chart(ctx_live, {
      type: 'bar',
      data: {
        labels: [],
        datasets: [{
          data: [],
          borderWidth: 1,
          backgroundColor : '#00b8d4',
          borderColor:'#00b8d4',
          label: 'Total',
        }]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: "Transaction Representration ("+today+")",
        },
        legend: {
          display: false
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
            }
          }],
            xAxes: [{
              maxBarThickness: 50
          }]
        },
        animation: {
          onComplete: function() {
            done();
          }
        }
      }
    });
    urlGraph = prkUrl+'prk_veh_type_by_pay_amt';
    //alert(urlGraph);
    $.ajax({
      url: urlGraph,
      type:'POST',
      data:{
        'prk_admin_id':"<?php echo $_SESSION['prk_admin_id']?>",
        'start_date':today,
        'end_date':today,
        'token': '<?php echo $token;?>'
      },
      success: function(data) {
        $('#blank').hide();
        // $('#print').removeAttr("disabled");
        var json = $.parseJSON(data);
        $.each(json.parking_amu, function(val, key) {
          myChart.data.labels.push(key['vehicle_type']+" ("+key['payment_amount']+")");
          myChart.data.datasets[0].data.push(key['payment_amount']);
        });
        myChart.update();
      }
    });
  }
  // $('#print').click(function(){
  function done() {
    var url_base64 = document.getElementById("canvas").toDataURL("image/png");
    var url_base64jp = document.getElementById("canvas").toDataURL("image/jpg");
    $('#img_value').val(url_base64);
    // alert(url_base64);
  }
</script>

