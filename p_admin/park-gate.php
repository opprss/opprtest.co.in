<!-- ################################################
  
Description: Parking area can add/delete parking gate with description.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
include "all_nav/header.php";
if (isset($_SESSION['prk_admin_id'])) {

  $user_name=$_SESSION['prk_admin_id'];
  $prk_admin_id = $_SESSION['prk_admin_id'];;
}
?>

<!-- header position -->
  <style type="text/css">
    .size{
      font-size: 11px;
    }

    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
    .error_size{
    font-size: 11px;
    color: red;

  }
  </style>
    <!-- links for datatable -->
    <link href="../lib/highlightjs/github.css" rel="stylesheet">
    <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
  
    <div class="am-mainpanel">

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Parking Gate</h5>
        <!--<form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40" >
          <!-- <h6 class="card-body-title">Add Employee</h6> -->
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form name="park_gate">
              <div class="row mg-b-25">
                <div class="col-lg-3">
                  <div class="form-group">
                    <label class="form-control-label size">GATE NAME <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Gate Name" name="prk_area_gate_name" id="prk_area_gate_name" onkeydown="upperCase(this)">
                    <span style="position: absolute;" id="error_prk_area_gate_name" class="error_size"></span>
                  </div>
                </div><!-- col-4 -->
                <div class="col-lg-3">
                  <div class="form-group">
                    <label class="form-control-label size">GATE TYPE <span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="prk_area_gate_type" id="prk_area_gate_type" value="" style="opacity: 0.8;font-size:14px">
                        <option value="">SELECT GATE TYPE</option>
                        <option value="I">IN</option>
                        <option value="O">OUT</option>
                        <option value="B">BOTH</option>
                      </select>
                    </div>
                    <span id="error_prk_area_gate_type" style="position: absolute;" class="error_size"></span>
                    <!-- <label class="rdiobox">
                      <input name="prk_area_gate_type" id="prk_area_gate_type" type="radio" value="I" checked="checked">
                      <span>IN</span>
                    </label>

                    <label class="rdiobox">
                      <input name="prk_area_gate_type" id="prk_area_gate_type" value="O" type="radio">
                      <span>OUT</span>
                    </label> -->
                  </div>
                </div><!-- col-4 -->
                <div class="col-lg-3">
                  <div class="form-group">
                    <label class="form-control-label size">GATE DESCRIPTION <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Gate description" name="prk_area_gate_dec" id="prk_area_gate_dec" onkeydown="upperCase(this)">
                    <span style="position: absolute;" id="error_prk_area_gate_dec" class="error_size"></span>
                  </div>
                </div><!-- col-4 -->
                <div class="col-lg-3">
                  <div class="form-group">
                    <label class="form-control-label size">LANDMARK <span class="tx-danger"></span></label>
                    <input type="text" class="form-control" placeholder="Gate Landmark" name="prk_area_gate_landmark" id="prk_area_gate_landmark" onkeydown="upperCase(this)">
                    <span style="position: absolute;" id="error_prk_area_gate_landmark" class="error_size"></span>
                  </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label size">EFFECTIVE DATE<span class="tx-danger">*</span></label>
                    <input type="text" name="effective_date" id="effective_date" placeholder="Effective Date (mm/dd/yyyy)" class="form-control prevent_readonly" readonly="readonly">
                    <span style="position: absolute;" id="error_effective_date" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label size">END DATE</label>
                    <input type="text" name="end_date" id="end_date" placeholder="End Date(MM/DD/YYYY)" class="form-control prevent_readonly" readonly="readonly">
                    <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-layout-footer mg-t-30">
                    <div class="row">
                      <div class="col-lg-6">
                        <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
                      </div>
                      <div class="col-lg-6">
                        <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      

      <!-- data table -->
      <div class="am-pagebody">

        <div class="card pd-20 pd-sm-40">
          <!-- <h6 class="card-body-title"></h6> -->
          <!-- <p class="mg-b-20 mg-sm-b-30"></p> -->

          <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="wd-15p">Gate Name</th>
                  <th class="wd-15p">In/Out</th>
                  <th class="wd-20p">Description</th>
                  <th class="wd-15p">Effective Date</th>
                  <th class="wd-15p">End Date</th>
                  <th class="wd-10p">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
           $respon=prk_area_gate_dtl_list($prk_admin_id);
           //print_r($respon);
           $x=1;
              foreach($respon as $value){
                $prk_area_gate_id = $value['prk_area_gate_id'];
                 ?>

               <tr>
                 <td ><?php echo $value['prk_area_gate_name']; ?></td>
                 <td >
                  <?php 
                    if ($value['prk_area_gate_type'] == 'I') {
                      echo "IN";
                    }else if ($value['prk_area_gate_type'] == 'O') {
                     echo "OUT";
                    }else if ($value['prk_area_gate_type'] == 'B') {
                     echo "BOTH";
                    }else if ($value['prk_area_gate_type'] == 'S') {
                     echo "SKIP";
                    }else{
                      echo "N/A";
                    }
                  ?></td>
                 <td ><?php echo $value['prk_area_gate_dec']; ?></td>
                 <td><?php echo $value['effective_date']; ?></td>
                 <td><?php echo $value['end_date']; ?></td>
                 <td>
                 <?php if ($value['prk_area_gate_type'] != 'S') { ?>
                    <a href="<?php echo 'park-gate-modify?list_id='.base64_encode($prk_area_gate_id);?>" class="pd-l-10" data-toggle="tooltip" data-placement="top" title="Modify">
                      <i class="fa fa-pencil" style="font-size:18px; font-size:18px;"></i>
                    </a>
                    <?php }else{ ?>
                      <i class="fa fa-pencil pd-l-10" style="font-size:18px; font-size:18px; color: grey;"></i>
                    <?php }?>
                    <button type="button" class="clean-button" name="delete" id="delete<%=count++%>" value="<?php echo $prk_area_gate_id; ?>" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
                 </td>
                 </tr>
                <?php $x++; } ?>
         
                </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<style>
.error_size{
  font-size: 11px;
  color: red;

}
.success{
  font-size: 11px;
  color: green;
}
</style>

<script type="text/javascript">
  
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE_WEB ?>"

  /*delete popup*/  
  $('button[id^="delete"]').on('click', function() {
    var prk_admin_id = "<?php echo $prk_admin_id;?>";
    var user_name=' <?php echo $parking_admin_name; ?>';
    var prk_area_gate_id = this.value;
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the item permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            if (prk_area_gate_id != '') {
              var urlDtl = prkUrl+'prk_area_gate_dtl_delete.php';
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :
                {
                  'prk_area_gate_id':prk_area_gate_id,
                  'user_name':user_name,
                  'prk_admin_id':prk_admin_id,
                  'token': '<?php echo $token;?>'
                },
                dataType:'html',
                success  :function(data)
                {
                  var json = $.parseJSON(data);
                  if (json.status){
                    location.reload(true);
                  }else{
                    if( typeof json.session !== 'undefined'){
                      if (!json.session) {
                        window.location.replace("logout.php");
                      }
                    }else{
                      $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                    }
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  });
 /*/delete popup*/
        $( document ).ready( function () {

          $('#reset').click(function(){
            $('.error_size').text('');
          });

          /*FORM SUBMIT*/
          $('#save').click(function(){

              var prk_admin_id = "<?php echo $prk_admin_id;?>";
              var prk_area_gate_name = $('#prk_area_gate_name').val();
              var prk_area_gate_dec = $('#prk_area_gate_dec').val();
              var prk_area_gate_landmark = $('#prk_area_gate_landmark').val();
              var prk_area_gate_type =$("#prk_area_gate_type").find("option:selected").val(); 
              var effective_date = $('#effective_date').val();
              var end_date = $('#end_date').val();
              end_date = (!end_date == '') ? end_date : FUTURE_DATE;

              var user_name = "<?php echo $parking_admin_name;?>";
              if (prk_area_gate_name!='' && prk_area_gate_type !='' && prk_area_gate_dec!='' && effective_date!='') {
                $('#save').val('Wait ...').prop('disabled', true);
                var urlPrkAreGateDtl = prkUrl+'prk_area_gate_dtl.php';
                $.ajax({
                      url :urlPrkAreGateDtl,
                      type:'POST',
                      data :
                      {
                        'prk_admin_id':prk_admin_id,
                        'prk_area_gate_name':prk_area_gate_name,
                        'prk_area_gate_dec':prk_area_gate_dec,
                        'prk_area_gate_landmark':prk_area_gate_landmark,
                        'prk_area_gate_type':prk_area_gate_type,
                        'effective_date':effective_date,
                        'end_date':end_date,
                        'user_name':user_name,
                        'token': '<?php echo $token;?>'
                      },
                      dataType:'html',
                      success  :function(data)
                      {
                        $('#save').val('SAVE').prop('disabled', false);
                        // alert(data);
                        var json = $.parseJSON(data);
                          if (json.status){
                            $.alert({
                              icon: 'fa fa-smile-o',
                              theme: 'modern',
                              title: 'Success !',
                              content: "<p style='font-size:0.9em;'>Gate added successfully</p>",
                              type: 'green',
                              buttons: {
                                Ok: function () {
                                    location.reload(true);
                                }
                              }
                            });
                          }else{
                            if( typeof json.session !== 'undefined'){
                              if (!json.session) {
                                window.location.replace("logout.php");
                              }
                            }else{
                              $.alert({
                              icon: 'fa fa-frown-o',
                              theme: 'modern',
                              title: 'Error !',
                              content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                              type: 'red'
                            });
                            }
                          }
                      }
                  });
              
              }else{

                if (prk_area_gate_name == '') {
                  $('#error_prk_area_gate_name').text('Gate name is required');
                //return false;
                }if (prk_area_gate_type == '') {
                  $('#error_prk_area_gate_type').text('Gate type is required');
                  //return false;
                }if (prk_area_gate_dec == '') {
                  $('#error_prk_area_gate_dec').text('Gate description is required');
                  //return false;
                }if (effective_date == '') {
                  $('#error_effective_date').text('Effective date is required');
                  //return false;
                }
                return false;
              }
              
          });

       /*gate name*/
        $("#prk_area_gate_name").blur(function () {
          var prk_area_gate_name = $('#prk_area_gate_name').val();
          if(prk_area_gate_name == ''){
            $('#error_prk_area_gate_name').text('Gate name is required');
            return false;
          }else{
            $('#error_prk_area_gate_name').text('');
            return true;
          }
        });  

        //prk_area_gate_type
        $("#prk_area_gate_type").blur(function () {
          var prk_area_gate_type = $('#prk_area_gate_type').val();
          if(prk_area_gate_type == ''){
            $('#error_prk_area_gate_type').text('Gate type is required');
            return false;
          }else{
            $('#error_prk_area_gate_type').text('');
            return true;
          }
        });
        $("#prk_area_gate_dec").blur(function () {
          var prk_area_gate_dec = $('#prk_area_gate_dec').val();
          if(error_prk_area_gate_dec == ''){
            $('#error_prk_area_gate_dec').text('Gate description is required');
            return false;
          }else{
            $('#error_prk_area_gate_dec').text('');
            return true;
          }
        });
        /*gate decce*/
        $("#effective_date").blur(function () {
          var effective_date = $('#effective_date').val();
          if(effective_date == ''){
              $('#error_effective_date').text('Effective date is required');
            return false;
          }else{
            $('#error_effective_date').text('');
            return true;
          }
        });  

      } );

/*date*/
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#effective_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
            if (date < now) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            $('#error_effective_date').text('');
            return {};
        } 
    });
  });

  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#effective_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
            if (date < now_end) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            return {};
        } 
    });
  });

  $('#effective_date').focus(function(){
    $('#end_date').val('');
  });
        /* end velidation*/


        function gatePassUppercase() {
          var x = document.getElementById("user_gate_pass_num");
          x.value = x.value.toUpperCase();
        }
        function upperCase(a){
            setTimeout(function(){
                a.value = a.value.toUpperCase();
            }, 1);
        }
    </script>


</script>
    <!-- scripts for datatable -->
    <script src="../lib/highlightjs/highlight.pack.js"></script>
    <script src="../lib/datatables/jquery.dataTables.js"></script>
    <script src="../lib/datatables-responsive/dataTables.responsive.js"></script>

    <script>
      $(function(){
        'use strict';

        $('#datatable1').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search Here',
            sSearch: '',
            lengthMenu: '_MENU_ Page',
          }
        });

        $('#datatable2').DataTable({
          bLengthChange: false,
          searching: false,
          responsive: true
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

      });
    </script>
    <script>
    </script>

    <!-- for tool tipe -->
    <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>