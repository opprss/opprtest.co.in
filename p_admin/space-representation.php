<?php 
// include_once("../global_asset/config.php");
session_start();
include "all_nav/header.php"; 
if (isset($_SESSION['prk_admin_id'])) {
   
    @$prk_admin_id = $_SESSION['prk_admin_id'];
}
?>
 <div class="am-mainpanel"><!-- closing in footer -->

  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Full Pie Chart</h5>
    <!--<form id="searchBar" class="search-bar" action="#">
      <div class="form-control-wrapper">
        <input type="search" class="form-control bd-0" placeholder="Search...">
      </div>
      <button id="searchBtn" class="btn btn-orange" style="width: 50px"><i class="fa fa-search"></i></button>
    </form> search-bar-->
  </div><!-- am-pagetitle -->
</div>

<div class="am-mainpanel">
  <div class="am-pagebody">

    <div class="card pd-20 pd-sm-40 single">
      <h6 class="card-body-title">Select Vehicle Type Current Space Details</h6>
      <div class="row">
        <div class="col-md-4 pd-t-40">
          <form action="space-representation-pdf" method="post">
            <div class="option">
             <select name="vehicle_type" id="vehicle_type" value="" style="opacity: 0.8; font-size:14px">
              </select>
            </div>
             <br>
            <br>
            <input type="hidden" name="image_value" id="image_value">
              <button type="submit" class="btn btn-info" formtarget="_BLANK" id="print" style="width: 100px">Print</button>
          </form>

        </div>
        <div class="col-md-8">
          <div id="canvas-holder" style="width:100%">
            <canvas id="chart-area"></canvas>
          </div>
          <div id="holder"></div>
        </div>
      </div>  
    </div><!-- card -->

  </div>
  <?php include"all_nav/footer.php"; ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="../js/ResizeSensor.js"></script>
<script src="../js/dashboard.js"></script>
<script src="../lib/chart.js/Chart.bundle.js"></script>
<script src="../lib/chart.js/utils.js"></script>

<script>
  $(document).ready(function(){ 
    $('#print').attr("disabled", true);
    var prkUrl = "<?php echo PRK_URL; ?>";
    var prk_admin_id='<?php echo $prk_admin_id; ?>';
    /*pie*/
    var ctx = document.getElementById('chart-area').getContext('2d');
    var myPie = new Chart(ctx,{
      type: 'pie',
      data: {
        datasets: [{
          data: [],
          backgroundColor: [],
          label: 'Dataset 1'
        }],
        labels: []
      },
      options: {
        title: {
          display: true,
          text: "Total Parking Space Representation",
        },
        responsive: true,
          animation: {
            onComplete: function() {
              done();
            }
          }
      }
    });
    /*pie logic*/
    var getDataPie = function() {
      urlPie = prkUrl+'prk_area_rate_list';
      $.ajax({
        url: urlPie,
        type:'POST',
        data:{

          'prk_admin_id':"<?php echo $_SESSION['prk_admin_id']?>"
        },
        success: function(data) {
          var json = $.parseJSON(data);
          if(json!=''){
            $.each(json, function(val, key) {
              //alert();
              myPie.data.labels.push(key['prk_vehicle_type']+" ("+key['tot_prk_space']+")");
              myPie.data.datasets[0].data.push(key['tot_prk_space']);
              myPie.data.datasets[0].backgroundColor.push(getRandomColor());
            });
            myPie.update();
          }else{
            $('#holder').html('<div style="margin-top: 150px; margin-left:200px">Parking Rate Yet Not Added </div>');
            $('#canvas-holder').hide();
          }
        }
      });
    };
    /*/pie logic*/
    getDataPie();
    /*/pie*/
    /*select*/
    var urlVehicleType = prkUrl+'prk_veh_type_space_chart.php';
    $.ajax ({
        type: 'POST',
        url: urlVehicleType,
        data: {prk_admin_id:prk_admin_id},
        success : function(data) {
            var obj=JSON.parse(data);
           var areaOption = "<option value='' style='color:gray;' selected='selected'>SELECT VEHICLE TYPE</option>";
           $.each(obj, function(val, key) {
            areaOption += '<option value="' + key['vehicle_type_dec']+',' +key['tot_prk_space'] +','+ key['veh_count'] + '">' + key['vehicle_type_dec'] + '</option>'
           });

          $("#vehicle_type").html(areaOption);

        }
    });
    /*/selecrt*/
    /*on chnage*/
    $('#vehicle_type').change(function(){
      var pie_Data = $('#vehicle_type').val().split(",");
      var vehicle_type_dec = pie_Data[0];
      var tot_prk_space = pie_Data[1];
      var veh_count = pie_Data[2];

      var data = [tot_prk_space, veh_count]
      var ctx = document.getElementById('chart-area').getContext('2d');
      var myPie = new Chart(ctx,{
        type: 'pie',
        data: {
          datasets: [{
            data: data,
            backgroundColor: ['#FFCF79', '#92CD00'],
            label: 'Dataset 1'
          }],
          labels: ['Free ('+tot_prk_space+')', 'Fill ('+veh_count+')']
        },
        options: {
            title: {
            display: true,
            text: "Parking Space Representation For "+vehicle_type_dec,
          },
          responsive: true,
          animation: {
            onComplete: function() {
              done();
            }
          }
        }
      });
    });
    /*/on change*/
  });
  function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  function done() {
    var url_base64 = document.getElementById("chart-area").toDataURL("image/png");
    $('#image_value').val(url_base64);
  }
</script>