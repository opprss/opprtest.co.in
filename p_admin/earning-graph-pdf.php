<?php 

include('../library/fpdf/fpdf.php');
include '../global_asset/config.php';
  
  
  if(isset($_POST['img_value'])){
 $url_base64=$_POST['img_value'];
  }
  // if(empty($_POST['start_date'])){
  // 	$start_date=TIME_TRN;
  // }
  if(isset($_POST['start_date'])){
 $start_date=$_POST['start_date'];
  }
  if($start_date==''){
      $date=TIME_TRN;
      $start_date = date("d-m-Y", strtotime($date));
      }
class PDF extends FPDF
{
// Page header
function Header()
	{
    
    $this->Ln(20);
	}

// Page footer
function Footer()
		{
	
	$this->SetY(-15);
	$this->SetFont('Arial','I',8);
	$this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
		}
}
		

const TEMPIMGLOC = 'tempimg.png';

$dataURI    = $url_base64;
$dataPieces = explode(',',$dataURI);
$encodedImg = $dataPieces[1];
$decodedImg = base64_decode($encodedImg);

//  Check if image was properly decoded
if( $decodedImg!==false )
{
    //  Save image to a temporary location
    if( file_put_contents(TEMPIMGLOC,$decodedImg)!==false )
    {
        //  Open new PDF document and print image
        $pdf = new PDF();
        $pdf->AddPage();
        // $pdf->Image(TEMPIMGLOC);
        $pdf->SetFont('Arial','B',16);

		$pdf->Image(prk_LOGO_pdf,13,20,-220);
		$pdf->SetFont('Arial','',10);
		
		$pdf->setXY(175,18);
		$pdf->Cell(10,10,'Dated :'.$start_date,0,0,'C',0);
		$pdf->setXY(100,60);
		$pdf->Line(10,60,200,60);
		$pdf->Line(10,63,200,63);
		$pdf->setXY(40,60);
        $pdf->Image(TEMPIMGLOC,40,80,140,0,'PNG');
		$pdf->SetXY(30,270);
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(10,5,COPYRIGHT_PDF,'C');
        $pdf->Output();

        //  Delete image from server
        unlink(TEMPIMGLOC);
    }
}




 
// $pdf->Output();
 ?>
