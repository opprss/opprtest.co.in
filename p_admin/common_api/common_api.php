<?php
/*
Description: all api include
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
@session_start();
@include_once '../../../global_api/global_api.php';
@include_once '../../global_api/global_api.php';
@include_once '../global_api/global_api.php';
@include_once 'global_api/global_api.php';
@include_once'../../../global_asset/config.php';
@include_once'../../global_asset/config.php';
@include_once'../global_asset/config.php';
@include_once'global_asset/config.php';

include_once'api_draft_registration.php';
include_once 'api_draft_registration_sms.php';
include_once 'api_vehicle_add_user_mess.php';
include_once'api_rate_calculate.php';
include_once 'api_paymentCalculate.php';
include_once 'api_prk_veh_trc_dtl_update.php';
include_once 'api_user_veh_prk_verify_active.php';
include_once 'api_payment_dtl.php';
include_once 'api_unique_key_generate.php';
include_once 'api_payment_receive_insert.php';
include_once 'api_payment_dtl_img_show.php';
include_once 'api_payment_receive.php';
include_once 'api_prk_user_my_trn.php';
include_once 'api_prk_user_payment_type_trn.php';
include_once 'api_payment_type_list.php';
include_once 'api_prk_veh_out_dtl_sms.php';
include_once 'api_visitor_service_dtl_show.php';
include_once 'api_prk_veh_trc_dtl_list.php';
include_once 'api_prk_visit_dtl_list.php';
include_once 'api_employee_attendance_list.php';
include_once 'api_visit_in_list.php';
include_once 'api_employee_attendance_out.php';
include_once 'api_p_employee_permi_form_list.php';
include_once 'api_gate_pass_generate.php';
include_once 'api_prk_sub_unit_dtl_list.php';
include_once 'api_prk_gate_pass_dtl.php';
include_once 'api_user_gate_pass_mess.php';
include_once 'api_prk_gate_pass_dtl_list.php';
include_once 'api_prk_gate_pass_dtl_delete.php';
include_once 'api_prk_gate_pass_dtl_update.php';
include_once 'api_visitor_gate_pass_generate.php';
include_once 'api_visitor_gate_pass_dtl.php';
include_once 'api_visitor_gate_pass_dtl_list.php';
include_once 'api_visitor_gate_pass_issue_delete.php';
include_once 'api_prk_user_add_list.php';
include_once 'api_prk_user_add_verify.php';
include_once 'api_prk_user_add_reject.php';
include_once 'api_apartment_transaction.php';
include_once 'api_flat_master_list.php';
include_once 'api_kids_list.php';
include_once 'api_kids_out.php';
include_once 'api_prk_gate_pass_dtl_show.php';
include_once 'api_flat_prk_lot_list.php';
include_once 'api_visitor_vehicle_parking_space_list.php';
include_once 'api_visitor_gate_pass_dtl_show.php';
include_once 'api_visitor_parking_dtl.php';
include_once 'api_prk_visit_in_check.php';
include_once 'api_connect_flat_master.php';
include_once 'api_resident_member_details.php';
include_once 'api_meet_to_person_dtl.php';
include_once 'api_tower_flat_dtl_draft_dtl.php';
include_once 'api_contractual_gate_pass_dtl.php';
include_once 'api_prk_area_cus_dtl.php';
include_once 'api_cus_bill_generate_dtl.php';
include_once 'api_current_attendance_list.php';
include_once 'api_veh_out_verify.php';
include_once 'api_prk_sub_user_list.php';
include_once 'api_attendance_list.php';
include_once 'api_visitor_print.php';
?>