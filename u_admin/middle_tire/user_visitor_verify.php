<?php 
/*
Description: user address show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
require_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('visitor_verify_flag','user_admin_id','mobile','token'))){
 	if(isEmpty(array('visitor_verify_flag','user_admin_id','mobile','token'))){
        $visitor_verify_flag = trim($_POST['visitor_verify_flag']);
	 	$user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
	 	$token = trim($_POST['token']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $response = user_visitor_verify($user_admin_id,$visitor_verify_flag,$mobile);
        }else{
            $response = $resp; 
        }
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response =  json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>