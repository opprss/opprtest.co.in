<?php
/*
Description: Parking area sub unit insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_sub_unit($prk_admin_id,$prk_sub_unit_name,$prk_sub_unit_short_name,$prk_sub_unit_address,$prk_sub_unit_rep_name,$prk_sub_unit_rep_mob_no,$prk_sub_unit_rep_email,$prk_sub_unit_eff_date,$prk_sub_unit_end_date,$user_name){
    global $pdoconn;
    $response = array();
    $sql = "INSERT INTO `prk_sub_unit`(`prk_admin_id`,`prk_sub_unit_name`,`prk_sub_unit_short_name`,`prk_sub_unit_address`,`prk_sub_unit_rep_name`,`prk_sub_unit_rep_mob_no`,`prk_sub_unit_rep_email`,`prk_sub_unit_eff_date`,`prk_sub_unit_end_date`,`inserted_by`,`inserted_date`) VALUE ('$prk_admin_id','$prk_sub_unit_name','$prk_sub_unit_short_name','$prk_sub_unit_address','$prk_sub_unit_rep_name','$prk_sub_unit_rep_mob_no','$prk_sub_unit_rep_email','$prk_sub_unit_eff_date','$prk_sub_unit_end_date','$user_name','".TIME."')";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Insert sucessfull';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Insert Not sucessfull';
        return json_encode($response);
    }
}
?>