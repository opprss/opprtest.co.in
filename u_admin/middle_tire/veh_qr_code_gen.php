<?php 
/*
Description: vehicle QR code generator.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
 require_once 'api/reg_api.php';
// include_once 'api/global_api.php'; 
$response = array();
if(isAvailable(array('veh_number','veh_type','mobile','user_veh_dtl_id'))){
    if(isEmpty(array('veh_number','veh_type','mobile','user_veh_dtl_id'))){
        $veh_number = trim($_POST['veh_number']);
        $veh_type = trim($_POST['veh_type']);
        $mobile = trim($_POST['mobile']);
        $user_veh_dtl_id = ($_POST['user_veh_dtl_id']);
        
        $response = veh_qr_code_gen($veh_number, $veh_type,$mobile,$user_veh_dtl_id);
        echo ($response);

    }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?>