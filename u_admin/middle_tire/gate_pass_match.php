<?php 
/*
Description: user vehicle in and gate pass match. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
    require_once 'api/reg_api.php';
    // require_once 'api/global_api.php';
    
    $response = array();
if(isAvailable(array('prk_admin_id','user_gate_pass_num','user_vehicle_no'))){
 	if(isEmpty(array('prk_admin_id','user_gate_pass_num','user_vehicle_no'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $user_gate_pass_num = trim($_POST['user_gate_pass_num']);
        $user_vehicle_no = trim($_POST['user_vehicle_no']);

        
            $response = gate_pass_match($prk_admin_id,$user_gate_pass_num,$user_vehicle_no);
            echo ($response);
        
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?>