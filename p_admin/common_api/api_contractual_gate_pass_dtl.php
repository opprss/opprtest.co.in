<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function contractual_gate_pass_add($prk_admin_id,$visitor_name,$visitor_mobile,$visitor_gender,$emergency_no,$visitor_date_of_birth,$visitor_blood_group,$visitor_contractor,$visitor_department,$visitor_pf_number,$visitor_esi_number,$visitor_add,$visitor_landmark,$visitor_country,$visitor_state,$visitor_city,$visitor_pin_cod,$effective_date,$end_date,$inserted_by,$visitor_img){
    global $pdoconn;
    if(!empty($visitor_date_of_birth)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($visitor_date_of_birth)) == date($visitor_date_of_birth)) {
            $visitor_date_of_birth= $visitor_date_of_birth;
        } else {
            $visitor_date_of_birth = date("Y-m-d", strtotime($visitor_date_of_birth));
        }
    }else{
        $visitor_date_of_birth= TIME_TRN;
    }
    if(!empty($effective_date)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($effective_date)) == date($effective_date)) {
            $effective_date= $effective_date;
        } else {
            $effective_date = date("Y-m-d", strtotime($effective_date));
        }
    }else{
        $effective_date= TIME_TRN;
    }
    if(!empty($end_date)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($end_date)) == date($end_date)) {
            $end_date=  $end_date ;
        } else {
            $end_date = date("Y-m-d", strtotime($end_date));
        }
    }else{
        $end_date= FUTURE_DATE;
    }
    $sql = "INSERT INTO `contractual_gate_pass`(`prk_admin_id`, `visitor_mobile`, `visitor_name`, `visitor_gender`, `emergency_no`, `visitor_date_of_birth`, `visitor_blood_group`, `visitor_contractor`, `visitor_department`, `visitor_pf_number`, `visitor_esi_number`, `visitor_add`, `visitor_state`, `visitor_city`, `visitor_landmark`, `visitor_country`, `visitor_pin_cod`, `visitor_img`, `effective_date`, `end_date`, `inserted_by`, `inserted_date`) VALUES ('$prk_admin_id','$visitor_mobile','$visitor_name','$visitor_gender','$emergency_no','$visitor_date_of_birth','$visitor_blood_group','$visitor_contractor','$visitor_department','$visitor_pf_number','$visitor_esi_number','$visitor_add','$visitor_state','$visitor_city','$visitor_landmark','$visitor_country','$visitor_pin_cod','$visitor_img','$effective_date','$end_date','$inserted_by','".TIME."')";
    $query  = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Successful';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Unsuccessfully';
    }
    return json_encode($response);
}
function contractual_gate_pass_show($prk_admin_id,$contractual_gate_pass_dtl_id){
    global $pdoconn;
    $sql = "SELECT `contractual_gate_pass_dtl_id`,
        `prk_admin_id`,
        `visitor_mobile`,
        `visitor_name`,
        `visitor_gender`,
        `emergency_no`,
        DATE_FORMAT(`visitor_date_of_birth`,'%d-%m-%Y') AS `visitor_date_of_birth`,
        `visitor_blood_group`,
        `visitor_contractor`,
        `visitor_department`,
        `visitor_pf_number`,
        `visitor_esi_number`,
        `visitor_add`,
        `visitor_state`,
        (SELECT `city_name` FROM `city` WHERE `e_id`=`visitor_city`) as `city_name`,
        `visitor_city`,
        `visitor_landmark`,
        `visitor_country`,
        `visitor_pin_cod`,
        `visitor_img`,
        DATE_FORMAT(`effective_date`,'%d-%m-%Y') AS `effective_date`,
        DATE_FORMAT(`end_date`,'%d-%m-%Y') AS `end_date`
        FROM `contractual_gate_pass`
        WHERE `prk_admin_id`='$prk_admin_id' 
        AND `active_flag`='".FLAG_Y."' 
        AND `del_flag`='".FLAG_N."'
        AND `contractual_gate_pass_dtl_id`='$contractual_gate_pass_dtl_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['contractual_gate_pass_dtl_id'] = $val['contractual_gate_pass_dtl_id'];
        $response['prk_admin_id'] = $val['prk_admin_id'];
        $response['visitor_mobile'] = $val['visitor_mobile'];
        $response['visitor_name'] = $val['visitor_name'];
        $response['visitor_gender'] = $val['visitor_gender'];
        $response['emergency_no'] = $val['emergency_no'];
        $response['visitor_date_of_birth'] = $val['visitor_date_of_birth'];
        $response['visitor_blood_group'] = $val['visitor_blood_group'];
        $response['visitor_contractor'] = $val['visitor_contractor'];
        $response['visitor_department'] = $val['visitor_department'];
        $response['visitor_pf_number'] = $val['visitor_pf_number'];
        $response['visitor_esi_number'] = $val['visitor_esi_number'];
        $response['visitor_add'] = $val['visitor_add'];
        $response['visitor_state'] = $val['visitor_state'];
        $response['visitor_city'] = $val['visitor_city'];
        $response['city_name'] = $val['city_name'];
        $response['visitor_landmark'] = $val['visitor_landmark'];
        $response['visitor_country'] = $val['visitor_country'];
        $response['visitor_pin_cod'] = $val['visitor_pin_cod'];
        $response['visitor_img'] = (empty($val['visitor_img']))?'':PRK_BASE_URL.''.$val['visitor_img'];
        $response['effective_date'] = $val['effective_date'];
        $response['end_date'] = $val['end_date'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
function contractual_gate_pass_list($prk_admin_id){
    $allplayerdata = array();
    global $pdoconn;
    $sql = "SELECT `contractual_gate_pass_dtl_id`,
        `prk_admin_id`,
        `visitor_mobile`,
        `visitor_name`,
        `visitor_gender`,
        `emergency_no`,
        DATE_FORMAT(`visitor_date_of_birth`,'%d-%b-%Y') AS `visitor_date_of_birth`,
        `visitor_blood_group`,
        `visitor_contractor`,
        `visitor_department`,
        `visitor_pf_number`,
        `visitor_esi_number`,
        `visitor_add`,
        `visitor_state`,
        `visitor_city`,
        `visitor_landmark`,
        `visitor_country`,
        `visitor_pin_cod`,
        `visitor_img`,
        DATE_FORMAT(`effective_date`,'%d-%b-%Y') AS `effective_date`,
        DATE_FORMAT(`end_date`,'%d-%b-%Y') AS `end_date`
        FROM `contractual_gate_pass`
        WHERE `prk_admin_id`='$prk_admin_id' 
        AND `active_flag`='".FLAG_Y."' 
        AND `del_flag`='".FLAG_N."'
        ORDER BY 1 DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['contractual_gate_pass_dtl_id'] = $val['contractual_gate_pass_dtl_id'];
            $park['prk_admin_id'] = $val['prk_admin_id'];
            $park['visitor_mobile'] = $val['visitor_mobile'];
            $park['visitor_name'] = $val['visitor_name'];
            $park['visitor_gender'] = $val['visitor_gender'];
            $park['emergency_no'] = $val['emergency_no'];
            $park['visitor_date_of_birth'] = $val['visitor_date_of_birth'];
            $park['visitor_blood_group'] = $val['visitor_blood_group'];
            $park['visitor_contractor'] = $val['visitor_contractor'];
            $park['visitor_department'] = $val['visitor_department'];
            $park['visitor_pf_number'] = $val['visitor_pf_number'];
            $park['visitor_esi_number'] = $val['visitor_esi_number'];
            $park['visitor_add'] = $val['visitor_add'];
            $park['visitor_img'] = (empty($val['visitor_img']))?'':PRK_BASE_URL.''.$val['visitor_img'];
            $park['effective_date'] = $val['effective_date'];
            $park['end_date'] = $val['end_date'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['contr_gate_pass_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
function contractual_gate_pass_delete($prk_admin_id,$parking_admin_name,$contractual_gate_pass_dtl_id){
    global $pdoconn;
    $sql = "UPDATE `contractual_gate_pass` 
        SET `updated_by`='$parking_admin_name',
        `updated_date`='".TIME."',
        `del_flag`='".FLAG_Y."' 
        WHERE `prk_admin_id`='$prk_admin_id' 
        AND `contractual_gate_pass_dtl_id`='$contractual_gate_pass_dtl_id' 
        AND `active_flag`='".FLAG_Y."' 
        AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Delete Successful';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Delete Not Successful';
    }
    return json_encode($response);
}
function contractual_gate_pass_modify($prk_admin_id,$visitor_name,$visitor_mobile,$visitor_gender,$emergency_no,$visitor_date_of_birth,$visitor_blood_group,$visitor_contractor,$visitor_department,$visitor_pf_number,$visitor_esi_number,$visitor_add,$visitor_landmark,$visitor_country,$visitor_state,$visitor_city,$visitor_pin_cod,$effective_date,$end_date,$inserted_by,$visitor_img,$contractual_gate_pass_dtl_id){
    global $pdoconn;
    if(!empty($visitor_date_of_birth)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($visitor_date_of_birth)) == date($visitor_date_of_birth)) {
            $visitor_date_of_birth= $visitor_date_of_birth;
        } else {
            $visitor_date_of_birth = date("Y-m-d", strtotime($visitor_date_of_birth));
        }
    }else{
        $visitor_date_of_birth= TIME_TRN;
    }
    if(!empty($effective_date)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($effective_date)) == date($effective_date)) {
            $effective_date= $effective_date;
        } else {
            $effective_date = date("Y-m-d", strtotime($effective_date));
        }
    }else{
        $effective_date= TIME_TRN;
    }
    if(!empty($end_date)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($end_date)) == date($end_date)) {
            $end_date=  $end_date ;
        } else {
            $end_date = date("Y-m-d", strtotime($end_date));
        }
    }else{
        $end_date= FUTURE_DATE;
    }
    if (empty($visitor_img)) {
        $sql = "UPDATE `contractual_gate_pass` 
            SET `visitor_name`='$visitor_name',
            `visitor_gender`='$visitor_gender',
            `emergency_no`='$emergency_no',
            `visitor_date_of_birth`='$visitor_date_of_birth',
            `visitor_blood_group`='$visitor_blood_group',
            `visitor_contractor`='$visitor_contractor',
            `visitor_department`='$visitor_department',
            `visitor_pf_number`='$visitor_pf_number',
            `visitor_esi_number`='$visitor_esi_number',
            `visitor_add`='$visitor_add',
            `visitor_state`='$visitor_state',
            `visitor_city`='$visitor_city',
            `visitor_landmark`='$visitor_landmark',
            `visitor_pin_cod`='$visitor_pin_cod',
            `end_date`='$end_date',
            `updated_by`='$inserted_by',
            `updated_date`='".TIME."'
            WHERE `prk_admin_id`='$prk_admin_id' 
            AND `contractual_gate_pass_dtl_id`='$contractual_gate_pass_dtl_id' 
            AND `active_flag`='".FLAG_Y."' 
            AND `del_flag`='".FLAG_N."'";
    }else{
        $sql = "UPDATE `contractual_gate_pass` 
            SET `visitor_name`='$visitor_name',
            `visitor_gender`='$visitor_gender',
            `emergency_no`='$emergency_no',
            `visitor_date_of_birth`='$visitor_date_of_birth',
            `visitor_blood_group`='$visitor_blood_group',
            `visitor_contractor`='$visitor_contractor',
            `visitor_department`='$visitor_department',
            `visitor_pf_number`='$visitor_pf_number',
            `visitor_esi_number`='$visitor_esi_number',
            `visitor_add`='$visitor_add',
            `visitor_state`='$visitor_state',
            `visitor_city`='$visitor_city',
            `visitor_landmark`='$visitor_landmark',
            `visitor_pin_cod`='$visitor_pin_cod',
            `visitor_img`='$visitor_img',
            `end_date`='$end_date',
            `updated_by`='$inserted_by',
            `updated_date`='".TIME."'
            WHERE `prk_admin_id`='$prk_admin_id' 
            AND `contractual_gate_pass_dtl_id`='$contractual_gate_pass_dtl_id' 
            AND `active_flag`='".FLAG_Y."' 
            AND `del_flag`='".FLAG_N."'";
    }
    $query  = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response = json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
        $response = json_encode($response);
    }
    return($response);
}
?>