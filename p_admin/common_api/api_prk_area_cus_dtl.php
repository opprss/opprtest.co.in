<?php
/*
Description: Parking area account status check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_area_cus_dtl_show($prk_admin_id){
    global $pdoconn;
    $sql = "SELECT `prk_area_cus_dtl_id`, 
        `prk_admin_id`, 
        `kind_alt`, 
        `gst_number`, 
        `bill_to_add`, 
        `shipped_to_add`, 
        `payment_mode`, 
        `par_month_rate`, 
        `tax_type`, 
        DATE_FORMAT(`bill_date`,'%d-%m-%Y') AS `bill_date`,
        `payment_due_day`, 
        `service_name`,
        `discount` 
        FROM `prk_area_cus_dtl` 
        WHERE `prk_admin_id`='$prk_admin_id'  
        AND `active_flag`='".FLAG_Y."' 
        AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['prk_area_cus_dtl_id'] = $val['prk_area_cus_dtl_id'];
        $response['prk_admin_id'] = $val['prk_admin_id'];
        $response['kind_alt'] = $val['kind_alt'];
        $response['gst_number'] = $val['gst_number'];
        $response['bill_to_add'] = $val['bill_to_add'];
        $response['shipped_to_add'] = $val['shipped_to_add'];
        $response['payment_mode'] = $val['payment_mode'];
        $response['par_month_rate'] = $val['par_month_rate'];
        $response['tax_type'] = $val['tax_type'];
        $response['bill_date'] = $val['bill_date'];
        $response['payment_due_day'] = $val['payment_due_day'];
        $response['service_name'] = $val['service_name'];
        $response['discount'] = $val['discount'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
function prk_area_cus_dtl_add($prk_admin_id,$kind_alt,$gst_number,$bill_to_add,$shipped_to_add,$payment_mode,$par_month_rate,$tax_type,$bill_date,$payment_due_day,$service_name,$discount,$inserted_by){
    global $pdoconn;
    $bill_date_1=$bill_date;
    if(!empty($bill_date)) { 
        $format = "Y-m-d";
        if(date($format, strtotime($bill_date)) == date($bill_date)) {
            $bill_date= $bill_date;
        } else {
            $bill_date = date("Y-m-d", strtotime($bill_date));
        }
    }else{

        $bill_date= TIME_TRN;
    }
    $sql = "UPDATE `prk_area_cus_dtl` SET `updated_by`='$inserted_by',`updated_date`='".TIME."',`active_flag`='N' WHERE `active_flag`='Y' AND `del_flag`='N' AND `prk_admin_id`='$prk_admin_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $prk_area_cus_dtl_id = uniqid();
    $sql = "INSERT INTO `prk_area_cus_dtl`(`prk_area_cus_dtl_id`, `prk_admin_id`, `kind_alt`, `gst_number`, `bill_to_add`, `shipped_to_add`, `payment_mode`, `par_month_rate`, `tax_type`, `bill_date`, `payment_due_day`, `service_name`, `discount`, `inserted_by`, `inserted_date`) VALUES ('$prk_area_cus_dtl_id','$prk_admin_id','$kind_alt','$gst_number','$bill_to_add','$shipped_to_add','$payment_mode','$par_month_rate','$tax_type','$bill_date','$payment_due_day','$service_name','$discount','$inserted_by','".TIME."')";
    $query  = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['prk_area_cus_dtl_id'] = $prk_area_cus_dtl_id;
        $response['bill_date-1'] = $bill_date_1;
        $response['bill_date'] = $bill_date;
    }else{
        $response['status'] = 0;
        $response['message'] = 'Unsuccessfully';
    }
    return json_encode($response);
}
?>