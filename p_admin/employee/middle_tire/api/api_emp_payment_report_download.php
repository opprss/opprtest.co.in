<?php 
/*
Description: Parking employ Payment report download
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function emp_payment_report_download($prk_admin_id,$payment_rec_emp_name,$start_date,$end_date,$payment_type){
   if($payment_type=='all'){
        $w2= CASH;
        $w3= WALLET;
        $w4= DGP;
        $w8= TDGP;
    }else{
        $w2=$payment_type;
        $w3=$payment_type;
        $w4=$payment_type;
        $w8=$payment_type;
    }
     
    $format = "Y-m-d";
    if(date($format, strtotime($start_date)) == date($start_date) && date($format, strtotime($end_date)) == date($end_date)) {
        $start_date= $start_date;
        $end_date=  $end_date ;
    } else {
        $start_date = date("Y-m-d", strtotime($start_date));
        $end_date = date("Y-m-d", strtotime($end_date));
    }
   global $pdoconn;
    $resp = array();
    $response = array();
    $re = array();

    $sql="SELECT `payment_dtl`.`payment_dtl_id`, 
    `payment_dtl`.`vehicle_type`,
    `payment_dtl`.`veh_number`,
    `payment_dtl`.`total_pay` ,
    DATE_FORMAT(`payment_dtl`.`payment_rec_date`,'%D-%M-%Y') AS `payment_rec_date`,
    DATE_FORMAT(`payment_dtl`.`payment_rec_date`,'%I:%i %p') AS `payment_rec_time`,
    `payment_dtl`.`payment_type`,
    `vehicle_type`.`vehicle_type_dec`,
    `vehicle_type`.`vehicle_typ_img`,
    `prk_area_dtl`.`prk_area_name`    
    FROM `payment_dtl`,`vehicle_type`,`prk_area_dtl` 
    WHERE `payment_dtl`.`prk_admin_id`='$prk_admin_id'
    AND `payment_dtl`.`payment_rec_emp_name`='$payment_rec_emp_name'
    AND `payment_dtl`.`payment_rec_status`='P'  
    AND `payment_dtl`.`pay_dtl_del_flag`='".DEL_FLAG_N."' 
    AND `payment_dtl`.`payment_type` IN ('$w2','$w3','$w4','$w8')
    AND `vehicle_type`.`vehicle_sort_nm`= `payment_dtl`.`vehicle_type`
    AND `vehicle_type`.`active_flag`= '".ACTIVE_FLAG_Y."' 
    AND `payment_dtl`.`prk_admin_id`= `prk_area_dtl`.`prk_admin_id`
    AND `prk_area_dtl`.`active_flag` = '".ACTIVE_FLAG_Y."'
    AND DATE_FORMAT(`payment_dtl`.`payment_rec_date`,'%Y-%m-%d')  BETWEEN '$start_date' AND '$end_date'
    ORDER BY FIELD(`payment_dtl`.`payment_type`, 'cash', 'wallet', 'dig_g_p_p','tdgp'), DATE_FORMAT(`payment_dtl`.`payment_rec_date`,'%Y-%m-%d') DESC";
    $total_pay=0;
    $total_veh=0;
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val)
    {
        $total_pay=$val['total_pay']+$total_pay;
        $total_veh=$total_veh+1;
        $resp['vehicle_type'] =$val['vehicle_type'];
        $resp['veh_number'] =$val['veh_number'];
        $resp['total_pay'] =$val['total_pay'];
        $resp['payment_rec_date'] =$val['payment_rec_date'];
        $resp['payment_rec_time'] =$val['payment_rec_time'];
        $resp['payment_type'] =$val['payment_type'];
        $resp['vehicle_type_dec'] =$val['vehicle_type_dec'];
        $resp['vehicle_typ_img'] =$val['vehicle_typ_img'];
        $prk_area_name =$val['prk_area_name'];
        $resp['start_date'] =$start_date;
        $resp['end_date'] =$end_date;
        $re[$val['payment_type']][]= $resp;
    }
    $response['status']=1;
    $response['session'] = 1;
    $response['message'] = 'Successful';
    $response['prk_area_name'] = $prk_area_name;
    $response['total_veh'] = $total_veh;
    $response['total_pay'] = $total_pay;
    $response['payment_history'] = $re;
    return json_encode($response);
} 
/*function emp_payment_report_download($prk_admin_id,$payment_rec_emp_name,$start_date,$end_date,$payment_type){
   if($payment_type=='all'){
        $w2= CASH;
        $w3= WALLET;
        $w4= DGP;
        $w8= TDGP;
    }else{
        $w2=$payment_type;
        $w3=$payment_type;
        $w4=$payment_type;
        $w8=$payment_type;
    }
    
    $format = "Y-m-d";
    if(date($format, strtotime($start_date)) == date($start_date) && date($format, strtotime($end_date)) == date($end_date)) {
        $start_date= $start_date;
        $end_date=  $end_date ;
    } else {
        $start_date = date("Y-m-d", strtotime($start_date));
        $end_date = date("Y-m-d", strtotime($end_date));
    }
   global $pdoconn;

    $sql="SELECT `payment_dtl`.`payment_dtl_id`,
    `payment_dtl`.`vehicle_type`,
    `payment_dtl`.`veh_number`,
    `payment_dtl`.`total_pay` ,
    DATE_FORMAT(`payment_dtl`.`payment_rec_date`,'%D-%M-%Y') AS `payment_rec_date`,
    DATE_FORMAT(`payment_dtl`.`payment_rec_date`,'%I:%i %p') AS `payment_rec_time`,
    `payment_dtl`.`payment_type`,
    `vehicle_type`.`vehicle_type_dec`,
    `vehicle_type`.`vehicle_typ_img`
    FROM `payment_dtl`,`vehicle_type` 
    WHERE `payment_dtl`.`prk_admin_id`='$prk_admin_id'
    AND `payment_dtl`.`payment_rec_emp_name`='$payment_rec_emp_name'
    AND `payment_dtl`.`payment_rec_status`='P'  
    AND `payment_dtl`.`pay_dtl_del_flag`='".DEL_FLAG_N."' 
    AND `payment_dtl`.`payment_type` IN ('$w2','$w3','$w4','$w8')
    AND `vehicle_type`.`vehicle_sort_nm`= `payment_dtl`.`vehicle_type`
    AND `vehicle_type`.`active_flag`='Y' 
    AND DATE_FORMAT(`payment_dtl`.`payment_rec_date`,'%Y-%m-%d')  BETWEEN '$start_date' AND '$end_date'
    ORDER BY FIELD(`payment_dtl`.`payment_type`, 'cash', 'wallet', 'dig_g_p_p','tdgp'), DATE_FORMAT(`payment_dtl`.`payment_rec_date`,'%Y-%m-%d') DESC";
    $total_pay=0;
    $total_veh=0;
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val)
    {
        $total_pay=$val['total_pay']+$total_pay;
        $total_veh=$total_veh+1;
        $resp['vehicle_type'] =$val['vehicle_type'];
        $resp['veh_number'] =$val['veh_number'];
        $resp['total_pay'] =$val['total_pay'];
        $resp['payment_rec_date'] =$val['payment_rec_date'];
        $resp['payment_rec_time'] =$val['payment_rec_time'];
        $resp['payment_type'] =$val['payment_type'];
        $resp['vehicle_type_dec'] =$val['vehicle_type_dec'];
        $resp['vehicle_typ_img'] =$val['vehicle_typ_img'];
        $resp['start_date'] =$start_date;
        $resp['end_date'] =$end_date;
        $re[$val['payment_type']][]= $resp;
    }
    $response['status']=1;
    $response['session'] = 1;
    $response['message'] = 'Successful';
    $response['total_veh'] = $total_veh;
    $response['total_pay'] = $total_pay;
    $response['payment_history'] = $re;
    return json_encode($response);
}*/
?>