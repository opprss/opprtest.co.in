<?php
/*
Description: Parking area one employ data show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_sub_user_list_by_id($prk_area_user_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT * FROM `prk_area_user` WHERE `prk_area_user_id`='$prk_area_user_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val) 
    {
        $prk_area_user_id=$val['prk_area_user_id'];
        $prk_user_username=$val['prk_user_username'];
        $prk_admin_id=$val['prk_admin_id'];
        $prk_user_emp_no=$val['prk_user_emp_no'];
        $prk_user_name=$val['prk_user_name'];
        $prk_user_password = $val['prk_user_password'];

        $park['prk_area_user_id'] = $prk_area_user_id;
        $park['prk_user_username'] = $prk_user_username;
        $park['prk_user_emp_no'] = $prk_user_emp_no;
        $park['prk_user_name'] = $prk_user_name;
        $park['prk_user_password'] = $prk_user_password;
        
    }
    return $park;
}
?>