<?php 
/*
Description: user forget password 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/  
 require_once 'api/parkAreaReg_api.php';
	//require_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('mobile','password'))){
 	if(isEmpty(array('password','mobile'))){

 		$da = date('d-M-Y',strtotime(TIME));
 		$te = date('h:i A',strtotime(TIME));
 		
 		// $te = 0;
 		$response['status'] = 1;
        $response['message'] = 'Successful';
        $response['da'] = $da;
        $response['te'] = $te;
        echo json_encode($response);
 	}else{
            $response['status'] = 0;
	        $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?>