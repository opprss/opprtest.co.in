<!-- ################################################ 
Description: 
Developed by: Rakhal Raj Mandal
Created Date: 22-05-2018
####################################################-->
<?php
  include('../global_asset/config.php');
  include('../global_asset/parking_include.php');
  $token = 'NA';
  $list_id_admin = isset($_COOKIE['list_id_admin']) ? trim($_COOKIE['list_id_admin']) : 'NN'; 
  $list_session_id = isset($_SESSION['prk_admin_id']) ? trim($_SESSION['prk_admin_id']) : (isset($_REQUEST['prk_admin_id']) ? trim($_REQUEST['prk_admin_id']) : 'NN');
  if ($list_id_admin!=$list_session_id) {
    if($list_id_admin=='NN'){
      $list_id_admin = $prk_admin_id = $list_session_id;
      setcookie('list_id_admin', $prk_admin_id, time() + 1800);
    }else if ($list_id_admin!='NN' & $list_session_id!='NN' & $list_id_admin!=$list_session_id) {
      $list_id_admin = $prk_admin_id = $list_session_id;
      setcookie('list_id_admin', $prk_admin_id, time() + 1800);
    }
    $token = 'YA';
  }
  $prk_area_name = 'NA';
  $prk_inf1 = prk_inf($list_id_admin);
  $prk_inf_sh = json_decode($prk_inf1, true);
  if ($prk_inf_sh['status']) {
    # code...
    $prk_area_name = $prk_inf_sh['prk_area_name'];
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">
    <title>OPPRSS | Real Time Space Details</title>
    <link rel="shortcut icon" href="../img/favicon.png" type="image/x-icon">
    <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/amanda.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/thank-you-style.css">
    <style type="text/css">
      table{
        font-size: 3em;
      }
      .th{
         font-size: 3em; 
      }
      html, body {
    	  background: white;
    	  padding: 0;
    	  margin: 0;
    	}
    	.cust-btn{
        cursor: pointer;
        background-color: transparent;
        color: #FFF;
        border: none;
        padding-left: 6px;
        padding-right: 6px;
        padding-top: 3px;
    	}	
    	body {
        position:absolute;
        top:0;
        bottom:0;
        left:0;
        right:0;
      }
      :-webkit-full-screen {
        overflow-y: scroll;
      }
      :-moz-full-screen {
        overflow-y: scroll;
      }
      :-ms-fullscreen {
        overflow-y: scroll;
      } 
      :fullscreen {
        overflow-y: scroll;
      }
    </style>
  </head>
  <body>
    <div class="container-fluid ht-100v">
      <div class="row" >
        <div class="col-lg-4 heaing am-header-left" style="height: 68px;">
          <img src="<?php echo LOGO ?>" style="height:100px; width:100px !important;margin-left: 30px;">
        </div>
        <div class="col-lg-4 heaing " style="height: 68px;">
          <span></span>
          <h6 style="padding-top: 16px; color: #FFF; font-size: 20px; text-align: center;"><?=$prk_area_name?> <?//='||'.$list_session_id.'||'.$list_id_admin.'||'.$token?></h6>
        </div>
        <div class="col-lg-4 heaing" style="height: 68px;">
          <div class="row">
            <div class="col-lg-12 pull-right">
              <button type="button" class="pull-right cust-btn" id="screenToggle" style="font-size: 15px;" onclick="toggleFullScreen(document.body)">FULL SCREEN</button>
              <span class="pull-right" style="color: #FFF; height: 25px;  padding-left: 6px; padding-right: 12px; padding-top: 3px;">|</span>
              <a href="javascript:closeWindow();" class="pull-right" style="color: #FFF; height: 25px;  padding-left: 6px; padding-right: 12px; padding-top: 3px;font-size: 15px;">CLOSE</a>
            </div>
            <div class="col-lg-12">
              <h6 class="pull-right" style="padding-top: 16px; color: #FFF; font-size: 20px;padding-right: 25px;">Dated : <?php echo date("d-m-Y");?></h6>
            </div>
          </div>
        </div>
        <div class="col-lg-12 tx-center pd-t-30">
          <table class="table table-bordered">
            <thead class="bg-info tx-center">
              <th style="font-size: 20px; text-align: center;">Subunit</th>
              <th style="font-size: 20px; text-align: center;">Vehicle Type</th>
              <th style="font-size: 20px; text-align: center;">Total Space</th>
              <th style="font-size: 20px; text-align: center;">Used Space</th>
              <th style="font-size: 20px; text-align: center;">Free Space</th>
            </thead>
            <tbody id="mian_tr">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </body>
</html>
<script type="text/javascript">
  var prk_id = "<?=$list_id_admin ?>";
  var prkUrl = "<?=PRK_URL; ?>";
  var token = "";
  function loadAjax(){
    var urlPrkSpaceDtl = prkUrl+'prk_space_dtl.php';
    $.ajax({
      url :urlPrkSpaceDtl,
      type:'POST',
      data :{
        'prk_admin_id':prk_id,
        'token':token
      },
      dataType:'json',
      success  :function(data){
        if( typeof data.session !== 'undefined'){
          if (!data.session) {
            window.location.replace("logout.php");
          }
        }
        lines = '';
        for (var key in data.prking_space) {
          lines += '<tr><td style="font-size: 20px;">'+data.prking_space[key]['sub_unit_short_name']+'</td>\
    	      <td style="font-size: 20px;">'+data.prking_space[key]['vehicle_type_dec']+'</td>\
            <td class="tot_space_2w" style="font-size: 20px;">'+data.prking_space[key]['tot_prk_space']+'</td>\
            <td class="used_space_2w" style="font-size: 20px;">'+data.prking_space[key]['veh_count']+'</td>\
            <td class="free_space_2w" style="font-size: 20px;">'+data.prking_space[key]['veh_avl']+'</td></tr>';
        }
        $("#mian_tr").html(lines);
      }
    });
  }
  loadAjax();
  setInterval(function(){
    loadAjax();// this will run after every 5 seconds 
  }, 5000);
</script>
<script>
  function closeWindow() {
    window.open('','_parent','');
    window.close();
  }
  function toggleFullScreen(elem) {
	  if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
	    $("#screenToggle").text("EXIT FULL SCREEN");
	    if (elem.requestFullScreen) {
	      elem.requestFullScreen();
	    } else if (elem.mozRequestFullScreen) {
	      elem.mozRequestFullScreen();
	    } else if (elem.webkitRequestFullScreen) {
	      elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
	    } else if (elem.msRequestFullscreen) {
	      elem.msRequestFullscreen();
	    }
	  } else {
	    $("#screenToggle").text("FULL SCREEN");
	    if (document.cancelFullScreen) {
	      document.cancelFullScreen();
	    } else if (document.mozCancelFullScreen) {
	      document.mozCancelFullScreen();
	    } else if (document.webkitCancelFullScreen) {
	      document.webkitCancelFullScreen();
	    } else if (document.msExitFullscreen) {
	      document.msExitFullscreen();
	    }
	  }
  }
</script> 
