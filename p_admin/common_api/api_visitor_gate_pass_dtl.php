<?php
/*
Description: user logout
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function visitor_gate_pass_dtl($prk_admin_id,$visitor_mobile,$visitor_name,$visitor_gender,$vis_id_prooof_img,$visitor_add1,$visitor_add2,$visitor_state,$visitor_city,$visitor_landmark,$visitor_country,$visitor_pin_cod,$visitor_img,$id_name,$id_number,$visitor_gate_pass_num,$effective_date,$end_date,$inserted_by,$visitor_vehicle_type,$visitor_vehicle_number){
    global $pdoconn;
    $end_date=(empty($end_date))?FUTURE_DATE_WEB:$end_date;
    $sql = "INSERT INTO `visitor_gate_pass_dtl`(`prk_admin_id`, `visitor_mobile`, `visitor_name`, `visitor_gender`, `visitor_id_img`, `visitor_add1`, `visitor_add2`, `visitor_state`, `visitor_city`, `visitor_landmark`, `visitor_country`, `visitor_pin_cod`, `visitor_img`, `id_name`, `id_number` ,`visitor_gate_pass_num` , `effective_date`, `end_date`, `inserted_by`, `inserted_date`,`visitor_vehicle_type`,`visitor_vehicle_number`) VALUES ('$prk_admin_id','$visitor_mobile','$visitor_name','$visitor_gender','$vis_id_prooof_img','$visitor_add1','$visitor_add2','$visitor_state','$visitor_city','$visitor_landmark','$visitor_country','$visitor_pin_cod','$visitor_img','$id_name','$id_number','$visitor_gate_pass_num','$effective_date','$end_date','$inserted_by','".TIME."','$visitor_vehicle_type','$visitor_vehicle_number')";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $id=$pdoconn->lastInsertId();
        $response['status'] = 1;
        $response['message'] = 'Gate Pass Issue Successful';
        $response['visitor_gate_pass_dtl_id'] = $id;
    }else{
        $response['status'] = 0;
        $response['message'] = 'Gate Pass Issue Not Successful';
    }
   return json_encode($response); 
}
?>