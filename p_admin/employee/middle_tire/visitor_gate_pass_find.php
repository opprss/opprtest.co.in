<?php 
/*
Description: parking area visit find the rate. 
Developed by: Rakhal Raj Mandal
Created Date: 09-06-2018
Update date : ----------
*/
require_once 'api/parkAreaReg_api.php';
$response = array();
$resp= array();
if(isAvailable(array('prk_admin_id','visitor_mobile','visitor_type'))){
 	if(isEmpty(array('prk_admin_id','visitor_mobile'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
	 	$visitor_mobile = trim($_POST['visitor_mobile']);
        $visitor_type = trim($_POST['visitor_type']);
        $response = visitor_gate_pass_find($visitor_mobile,$prk_admin_id,$visitor_type);       
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?> 