<?php 
/*
Description: prk_veh_type_space_chart.
Developed by: Rakhal Raj Mandal
Created Date: 16-06-2018
Update date : --------
*/ 
require_once 'api/parkAreaReg_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id'))){
 	if(isEmpty(array('prk_admin_id'))){

	 	$prk_admin_id = trim($_POST['prk_admin_id']);
        $response= prk_veh_type_space_chart($prk_admin_id);
        echo $response;
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
}
?>