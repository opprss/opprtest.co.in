<?php
/*
Description: vehicle out payment resive dtl show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function cash_memo_out($payment_dtl_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
        $sql = "SELECT  `vehicle_type`.`vehicle_sort_nm`,
            `vehicle_type`.`vehicle_type_dec`,
            `payment_dtl`.`veh_number`,
            DATE_FORMAT('".TIME."','%d-%m-%Y %H:%i') AS `current_date`,
            ifnull(`payment_receive`.`payment_receive_number`, `prk_veh_trc_dtl`.`prk_veh_trc_dtl_number`) as `invoice_no`,
            DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%D-%b-%Y') AS `veh_in_date`,
            DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%I:%i %p') AS `veh_in_time`,
            DATE_FORMAT(`payment_dtl`.`payment_rec_date`,'%D-%b-%Y') AS `veh_out_date`,
            DATE_FORMAT(`payment_dtl`.`payment_rec_date`,'%I:%i %p') AS `veh_out_time`,
            `payment_dtl`.`total_hr`,
            `payment_dtl`.`round_hr`,
            `payment_dtl`.`total_pay`,
            `prk_veh_trc_dtl`.`advance_pay`,
            `payment_dtl`.`payment_type` as payment_type_srt,
            (CASE `payment_dtl`.`payment_type` WHEN '".CASH."' ThEN '".CASH."' WHEN '".WALLET."' THEN '".WALLET."'
                WHEN '".TDGP."' THEN '".TDGP_F."' 
                WHEN '".DGP."' THEN '".DGP_F."' end) as 'payment_type',
            `prk_veh_trc_dtl`.`user_mobile`,
            ifnull(`payment_receive`.`payment_amount`, 0) as `net_pay`,
            `prk_area_dtl`.`prk_area_name`,
            `prk_area_dtl`.`prk_area_pro_img`,
            `prk_area_rate`.`prk_ins_hr_rate`,
            `prk_area_rate`.`prk_ins_no_of_hr`,
            `prk_area_rate`.`veh_rate`,
            `prk_area_rate`.`veh_rate_hour`,
            CONCAT(`prk_veh_trc_dtl`.`prk_veh_trc_dtl_id`, '-OPPR') as prk_veh_trc_dtl_id
                FROM `payment_dtl`
                join `prk_veh_trc_dtl`  on `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id` = `payment_dtl`.`prk_veh_trc_dtl_id`
                join `vehicle_type`     on `prk_veh_trc_dtl`.`prk_veh_type` = `vehicle_type`.`vehicle_sort_nm`
                                        AND `vehicle_type`.`active_flag` = '".FLAG_Y."'
                join `prk_area_dtl`     on `prk_veh_trc_dtl`.`prk_admin_id` = `prk_area_dtl`.`prk_admin_id`
                                        AND `prk_area_dtl`.`active_flag` = '".FLAG_Y."'
               left join `prk_area_rate`    on `prk_veh_trc_dtl`.`prk_admin_id` = `prk_area_rate`.`prk_admin_id`
                                        AND `prk_veh_trc_dtl`.`prk_veh_type` = `prk_area_rate`.`prk_vehicle_type`
                                        AND `prk_area_rate`.`active_flag` ='".FLAG_Y."'
                                        AND `prk_area_rate`.`del_flag` ='".FLAG_N."'
                                        AND  '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(`prk_area_rate`.`prk_rate_eff_date`, '%d-%m-%Y'), '%Y-%m-%d')    
                                        AND  DATE_FORMAT(STR_TO_DATE(IFNULL(`prk_area_rate`.`prk_rate_end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')
               left join`payment_receive`   on `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id` = `payment_receive`.`prk_veh_trc_dtl_id`
                                    AND `payment_receive`.`in_out_flag` = '".FLAG_O."'
            WHERE `payment_dtl`.`pay_dtl_del_flag` = '".FLAG_N."'
            -- and payment_dtl.payment_rec_status = '".FLAG_P."'
            AND payment_dtl.payment_dtl_id = '$payment_dtl_id'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $val = $query->fetch();
        $response['status'] = 1;
        $response['message'] = 'Successful';

        $response['vehicle_type'] = $val['vehicle_sort_nm'];
        $response['vehicle_type_dec'] = $val['vehicle_type_dec'];
        $response['veh_number'] = $val['veh_number'];
        $response['current_date'] = $val['current_date'];
        $response['invoice_no'] = $val['invoice_no'];
        $response['veh_in_date'] = $val['veh_in_date'];
        $response['veh_in_time'] = $val['veh_in_time'];
        $response['veh_out_date'] = $val['veh_out_date'];
        $response['veh_out_time'] = $val['veh_out_time'];
        $response['total_hr'] = $val['total_hr'];
        $response['round_hr'] = $val['round_hr'];
        $response['total_pay'] = $val['total_pay'];
        $response['advance_pay'] = $val['advance_pay'];
        $response['user_mobile'] = $val['user_mobile'];
        $response['net_pay'] = $val['net_pay'];
        $response['prk_area_name'] = $val['prk_area_name'];
        $response['prk_area_pro_img'] = $val['prk_area_pro_img'];
        $response['barcode_valu'] = $val['prk_veh_trc_dtl_id'];
        $response['prk_ins_hr_rate'] = $val['prk_ins_hr_rate'];
        $response['prk_ins_no_of_hr'] = $val['prk_ins_no_of_hr'];
        $response['veh_rate'] = $val['veh_rate'];
        $response['veh_rate_hour'] = $val['veh_rate_hour'];
        $response['payment_type'] = $val['payment_type'];
        $response['payment_type_srt'] = $val['payment_type_srt'];

    return json_encode($response); 
}
?>