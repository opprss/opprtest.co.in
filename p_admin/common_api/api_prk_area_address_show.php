<?php
/*
Description: Parking adimn Address show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_area_address_show($prk_admin_id){
    $response = array();
    global $pdoconn;

    $sql = "SELECT `prk_area_address`.`prk_add_id`,
    `prk_area_address`.`prk_address`,
    `prk_area_address`.`prk_land_mark`,
    `prk_area_address`.`prk_add_area`,
    `prk_area_address`.`prk_add_city`,
    `prk_area_address`.`prk_add_state`,
    `prk_area_address`.`prk_add_country`,
    `prk_area_address`.`prk_add_pin`,
    `state`.`state_name`,
    `city`.`city_name`
    FROM `prk_area_address`,`city`,`state`
    WHERE `prk_area_address`.`prk_admin_id`='$prk_admin_id'
    AND `city`.`e_id`= `prk_area_address`.`prk_add_city`
    AND `state`.`e`= `prk_area_address`.`prk_add_state`
    AND `prk_area_address`.`active_flag`='".FLAG_Y."'
    AND `state`.`active_flag`='".FLAG_Y."'
    AND `prk_area_address`.`del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $response['status'] = 1;
    $response['message'] = 'data ssucessfull';
    $response['prk_address'] = $val['prk_address'];
    $response['prk_land_mark'] = $val['prk_land_mark'];
    $response['prk_add_area'] = $val['prk_add_area'];
    $response['prk_add_city'] = $val['prk_add_city'];
    $response['prk_add_state'] = $val['prk_add_state'];
    $response['city_name'] = $val['city_name'];
    $response['state_name'] = $val['state_name'];
    $response['prk_add_country'] = $val['prk_add_country'];
    $response['prk_add_pin'] = $val['prk_add_pin'];
    return json_encode($response);
}
?>