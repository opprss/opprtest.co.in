<?php
/*
Description: user complite registration only for draft user.
Developed by: Rakhal Raj Mandal
Created Date: 10-02-2018
Update date : 10-05-2018
*/
function user_account_active($user_admin_id,$mobile,$user_ac_status){
    global $pdoconn;
    $response = array();
    $sql ="UPDATE `user_admin` SET  `user_ac_status`='".FLAG_A."' WHERE `user_admin_id`='$user_admin_id' AND `user_mobile`='$mobile'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['user_ac_status'] = 'A';
        $response['message'] = 'Update Successful';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Update Not Save';
    }
    return json_encode($response);
}
?>