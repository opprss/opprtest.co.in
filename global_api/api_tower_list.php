<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function tower_list($prk_admin_id){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
    $sql = "SELECT `tower_id`,`tower_name` FROM `tower_details` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['tower_id'] = $val['tower_id'];
            $park['tower_name'] = $val['tower_name'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['tower_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
function tower_list_search($prk_admin_id,$tower_name){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
    $sql = "SELECT `tower_id`,
        `tower_name`
        FROM `tower_details` 
        WHERE `prk_admin_id`='$prk_admin_id' 
        AND `tower_name` LIKE '%".$tower_name."%'
        AND `active_flag`='".FLAG_Y."' 
        AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['tower_id'] = $val['tower_id'];
            $park['tower_name'] = $val['tower_name'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['tower_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>