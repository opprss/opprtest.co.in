<?php
/*
Description: Parking area vehicle rate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
// function apartment_transaction($prk_admin_id,$parking_admin_name,$con_per_name,$con_per_mobile,$con_per_email,$tran_charge,$payment_mode,$apart_tran_remark,$crud_type){
function apartment_transaction($prk_admin_id,$parking_admin_name,$con_per_name,$con_per_mobile,$con_per_email,$total_amount,$payment_type,$apart_tran_remark,$crud_type,$bank_name,$cheque_number,$apar_tran_by,$prk_area_user_id){
    global $pdoconn;
    $response = array();
    $park = array();
    $allplayerdata = array();
    switch ($crud_type) {
        case 'I':
            $tran_date=TIME;
            $apar_tran_id=uniqid();
            $sql="INSERT INTO `apartment_transaction`(`apar_tran_id`, `prk_admin_id`, `con_per_name`, `con_per_mobile`, `con_per_email`, `total_amount`, `payment_type`, `apart_tran_remark`, `apart_tran_date`, `inserted_by`, `inserted_date`, `bank_name`, `cheque_number`, `apar_tran_by`,`prk_area_user_id`) VALUES ('$apar_tran_id','$prk_admin_id','$con_per_name','$con_per_mobile','$con_per_email','$total_amount','$payment_type','$apart_tran_remark','$tran_date','$parking_admin_name','".TIME."','$bank_name','$cheque_number','$apar_tran_by','$prk_area_user_id')";
    
            $query = $pdoconn->prepare($sql);
            if($query->execute()){
                $tran_flag='DE';
                apartment_maintenance_transition($prk_admin_id,$apar_tran_id,$total_amount,$payment_type,$tran_date,$tran_flag,$parking_admin_name,$apar_tran_by,$parking_admin_name);
                $response['status'] = 1;
                $response['message'] = 'Transaction sucessfull';
                return json_encode($response);
            }else{
                $response['status'] = 0;
                $response['message'] = 'Transaction Unsucessfull';
                return json_encode($response);
            }
            break;
        case 'LS':
            $sql="SELECT `apar_tran_id`, `prk_admin_id`, `con_per_name`, `con_per_mobile`, `con_per_email`, `total_amount`, `payment_type`, `apart_tran_remark`, `apart_tran_date` FROM `apartment_transaction` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $arr_catagory = $query->fetchAll();
                foreach($arr_catagory as $val)
                {
                    $park['apar_tran_id'] =  $val['apar_tran_id'];
                    $park['prk_admin_id'] =  $val['prk_admin_id'];
                    $park['con_per_name'] =  $val['con_per_name'];
                    $park['con_per_mobile'] =  $val['con_per_mobile'];
                    $park['con_per_email'] =  $val['con_per_email'];
                    $park['total_amount'] =  $val['total_amount'];
                    $park['payment_type'] =  $val['payment_type'];
                    $park['apart_tran_remark'] =  $val['apart_tran_remark'];
                    $park['apart_tran_date'] =  $val['apart_tran_date'];
                    array_push($allplayerdata, $park);
                }
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['apart_tran_list'] = $allplayerdata;
            }else{
                $response['status'] = 0;
                $response['message'] = 'List Empty';
            }
            break;
        case 'ELS':
            $sql="SELECT `apar_tran_id`, `prk_admin_id`, `con_per_name`, `con_per_mobile`, `con_per_email`, `total_amount`, `payment_type`, `apart_tran_remark`, `apart_tran_date` FROM `apartment_transaction` WHERE `prk_admin_id`='$prk_admin_id' AND `prk_area_user_id`='$prk_area_user_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $arr_catagory = $query->fetchAll();
                foreach($arr_catagory as $val)
                {
                    $park['apar_tran_id'] =  $val['apar_tran_id'];
                    $park['prk_admin_id'] =  $val['prk_admin_id'];
                    $park['con_per_name'] =  $val['con_per_name'];
                    $park['con_per_mobile'] =  $val['con_per_mobile'];
                    $park['con_per_email'] =  $val['con_per_email'];
                    $park['total_amount'] =  $val['total_amount'];
                    $park['payment_type'] =  $val['payment_type'];
                    $park['apart_tran_remark'] =  $val['apart_tran_remark'];
                    $park['apart_tran_date'] =  $val['apart_tran_date'];
                    array_push($allplayerdata, $park);
                }
                $response['status'] = 1;
                $response['message'] = 'Successful';
                $response['apart_tran_list'] = $allplayerdata;
            }else{
                $response['status'] = 0;
                $response['message'] = 'List Empty';
            }
            break;
        default:
            $response['status'] = 0;
            $response['message'] = 'Wrong Select';
            break;
    } 
    return json_encode($response);    
}
?>