<?php 
/*
Description: Parking employ vehicle verify and data insert trc table.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function veh_prk_estimation($veh_number,$veh_type,$veh_rate_id,$veh_rate_hour,$veh_rate,$no_of_time_multiple,$total_pay,$rate_validity,$prk_user_username,$prk_admin_id){
    global $pdoconn;
    $response = array();
    
    $sql = "INSERT INTO `prk_veh_prk_est`(`veh_number`, `prk_admin_id`, `veh_type`, `veh_rate_id`, `veh_rate_hour`, `veh_rate`, `no_of_time_multiple`, `total_pay`, `issue_date`, `rate_validity`, `inserted_by`, `inserted_date`)VALUES ('$veh_number','$prk_admin_id','$veh_type','$veh_rate_id','$veh_rate_hour','$veh_rate','$no_of_time_multiple','$total_pay','".TIME."','$rate_validity','$prk_user_username','".TIME."')";
    $query = $pdoconn->prepare($sql); 
    if($query->execute()){
        $id = $pdoconn->lastInsertId();
        $response['status'] = 1;
        $response['prk_veh_prk_est_id'] = $id;
        $response['message'] = 'Sucessful';
        return json_encode($response); 
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Sucessful';
        return json_encode($response);
    }   
}
?>