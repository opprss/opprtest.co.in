<?php
function select_faq(){
    $response=array();
    $data=array();
    global $pdoconn;
    $sql="SELECT * FROM `faq`";
    $result=$pdoconn->prepare($sql);
    $result->execute();
    $data=$result->fetchAll();
    $x=0;
    foreach($data as $row){
        $sl_no=$row['sl'];
        $question=$row['question'];
        $answer=$row['answer'];

        $response[$x]['id']=$sl_no;
        $response[$x]['sl']=$sl_no;
        $response[$x]['question']=$question;
        $response[$x]['answer']=$answer;
        $x++;
    }
    return $response;
}
?>