<?php
  session_start();
  include "all_nav/header.php";
  $insert_by=$employee_user_name;
  if (isset($_REQUEST["prk_admin_id"])) {
    $prk_admin_id= trim($_REQUEST["prk_admin_id"]);
    if(empty($prk_admin_id)){
     header('Location: prk-list.php');
    }
  }else{

    header('Location: prk-list');
  }
  $admin_dtl_show = p_admin_dtl_show($prk_admin_id);
  $admin_dtl_sh = json_decode($admin_dtl_show);
  $prk_area_name=$admin_dtl_sh->prk_area_name;

  $tower_id='';
  $flat_id='';
  $admin_dtl_show = flat_master_list($prk_admin_id,$tower_id,$flat_id);
  $flat_master_list = json_decode($admin_dtl_show,true);
?>
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
<style type="text/css">
  .size{

    font-size: 11px;
  }
  .select{

    font-size: 13px;
  }
  input::-webkit-input-placeholder {

    font-size: 13px;
  }
</style>
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Permission ( Premises Area Name: <?php echo $prk_area_name; ?>)</h5>
  </div>
</div>
<div class="am-mainpanel">
	<div class="am-pagebody">
    <div class="card pd-20 pd-sm-40" >
      <div class="editable tx-16 bd tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <form role="form" id="addVis" method="post"  enctype="multipart/form-data">
          <div class="row">
            <?php //echo $admin_dtl_show; ?>
            <div class="col-md-4"></div>
            <div class="col-md-2">
              <div class="form-group">
                 <input type="button" value="SEND" style="margin-top: 15px;" class="btn btn-block btn-primary prk_button" name="visitor_save" id="visitor_save">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <button style="margin-top: 15px;" type="button" id="back" class="btn btn-block prk_button">BACK</button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="table-wrapper mg-t-10">
        <table id="datatable1" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th >Sl No.</th>
              <th >Tower Name</th>
              <th >Flat Name</th>
              <th >Owner Name</th>
              <th >Living Status</th>
              <th >Tenant Name</th>
            </tr>
          </thead>
            <?php 
            if($flat_master_list['status']){$c=0;
              foreach($flat_master_list['flat_master_list'] as $value){$c+=1;?>
                <tr>
                  <td ><?=$c;?></td>
                  <td ><?=$value['tower_name'];?></td>
                  <td ><?=$value['flat_name'];?></td>
                  <td ><?=$value['owner_name'];?></td>
                  <td ><?=$value['living_status'];?></td>
                  <!-- <td ><?//=$value['living_status_full'];?></td> -->
                  <td ><?=$value['tenant_name'];?></td>
                </tr>
              <?php }}?>
        </table>
      </div>
    </div>
  </div>
</div>   
<!-- datatable -->
<?php include"all_nav/footer.php"; ?>

<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>

<script>
  var SUP_EMP_URL = "<?php echo SUP_EMP_URL ;?>";

  $( document ).ready( function () {
    $('#visitor_save').click(function(){
      // alert('ok');
      var insert_by = '<?=$insert_by?>';
      var prk_admin_id = '<?=$prk_admin_id?>';
      var urlgate_pass = SUP_EMP_URL+'apartment_maintenance_bill.php';
      // alert(urlgate_pass);
      $(this).val('Wait ...').attr('disabled',true);
      $.ajax({
        url :urlgate_pass,
        type:'POST',
        data :{
          'prk_admin_id':prk_admin_id,
          'inserted_by':insert_by
        },
        dataType:'html',
        success  :function(data){
          $('#visitor_save').val('SEND').attr('disabled',false);
          alert(data);
          /*var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success',
              content: "<p style='font-size:0.9em;'>Successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                  location.reload(true);
                }
              }
            });
          }else{
            $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.7em;'>"+json.message+"</p>",
              type: 'red'
            });
          }*/
        }
      });
    });
    $('#back').click(function(){

      window.location='prk-list';
    });
  });
</script>