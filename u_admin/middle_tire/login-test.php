<?php 


//echo json_encode($data);


/*
Description: user login. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
 */
 
 // require_once '../..connection-pdo.php';
 require_once 'api/reg_api.php';
// require_once 'api/global_api.php';
 
$response = array();
 
if(isAvailable(array('mobile','password'))){

 	if(isEmpty(array('mobile', 'password'))){

	 	$mobile = trim($_POST['mobile']);
	 	$password = trim($_POST['password']);

	 	$respon=login($mobile, $password);
        echo ($respon);

 	}else{
 			$response['status'] = 0;
	 		$response['message'] = 'Mobile or password is empty';
            echo json_encode($response); 
	 }

 }else{
	 $response['status'] = 0; 
	 $response['message'] = 'Invalid API Call';
     echo json_encode($response); 
 } 
?>