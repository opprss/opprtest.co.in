<?php
/*
Description: Parking area vehicle rate list update.
Developed by: Rakhal Raj Mandal
Created Date: 31-03-2018
Update date : 31-05-2018
*/ 
function park_area_rate_list_update($prk_rate_id){

    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();

    $sql = "SELECT `prk_area_rate`.`prk_vehicle_type`,
    `vehicle_type`.`vehicle_type_dec`,
    `vehicle_type`.`vehicle_typ_img`,
    `prk_area_rate`.`prk_rate_type`,
    (CASE `prk_area_rate`.`prk_rate_type` WHEN 'F' ThEN 'Fixed' WHEN 'D' THEN 'Dynamic' end) as 'prk_rate_type_dec',
    `prk_area_rate`.`prk_ins_no_of_hr`,
    `prk_area_rate`.`prk_ins_hr_rate`,
    `prk_area_rate`.`prk_no_of_mt_dis`,
    `prk_area_rate`.`prk_rate_eff_date`,
    `prk_area_rate`.`prk_rate_end_date`,
    `prk_area_rate`.`tot_prk_space`,
    `prk_area_rate`.`veh_rate_hour`,
    `prk_area_rate`.`veh_rate`
    FROM `prk_area_rate`,`vehicle_type` 
    WHERE `prk_area_rate`.`prk_rate_id`='$prk_rate_id' 
    AND `prk_area_rate`.`active_flag`='".FLAG_Y."' 
    AND `prk_area_rate`.`del_flag`='".FLAG_N."'
    AND `prk_area_rate`.`prk_vehicle_type`=`vehicle_type`.`vehicle_sort_nm`
    AND `vehicle_type`.`active_flag` ='".FLAG_Y."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch(); 
        $response['status'] = 1;
        $response['message'] = 'data Sucessful';        
        $park['prk_rate_id'] = $prk_rate_id;
        $park['prk_vehicle_type'] = $val['prk_vehicle_type'];
        $park['vehicle_type_dec'] = $val['vehicle_type_dec'];
        $park['vehicle_typ_img'] = $val['vehicle_typ_img'];
        $park['prk_rate_type'] = $val['prk_rate_type'];
        $park['prk_rate_type_dec'] = $val['prk_rate_type_dec'];
        $park['prk_ins_no_of_hr'] = $val['prk_ins_no_of_hr'];
        $park['prk_ins_hr_rate'] = $val['prk_ins_hr_rate'];
        $park['prk_no_of_mt_dis'] = $val['prk_no_of_mt_dis'];
        $park['prk_rate_eff_date'] = $val['prk_rate_eff_date'];
        $park['prk_rate_end_date'] = $val['prk_rate_end_date'];
        $park['tot_prk_space'] = $val['tot_prk_space'];
        $park['veh_rate_hour'] = $val['veh_rate_hour'];
        $park['veh_rate'] = $val['veh_rate'];
        array_push($allplayerdata, $park);
    
   	return $allplayerdata;
}
?>