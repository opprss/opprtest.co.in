<?php 
/*
Description: parking employ out vehicle
Developed by: Rakhal Raj Mandal
Created Date: 06-04-2018
Update date : 25-05-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
$response = array();
$re = array();
$resp = array();
if(isAvailable(array('prk_veh_trc_dtl_id','prk_out_rep_name','prk_admin_id','prk_area_user_id','prk_area_gate_id','token','prk_area_short_name'))){
 	if(isEmpty(array('prk_veh_trc_dtl_id','prk_out_rep_name','prk_admin_id','prk_area_user_id','prk_area_gate_id','token','prk_area_short_name'))){
 		
	 	$prk_veh_trc_dtl_id = trim($_POST['prk_veh_trc_dtl_id']);
	 	$prk_out_rep_name = trim($_POST['prk_out_rep_name']);
	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_area_user_id = trim($_POST['prk_area_user_id']);
        $prk_area_gate_id = trim($_POST['prk_area_gate_id']);
        $token = ($_POST['token']);
        $prk_area_short_name = trim($_POST['prk_area_short_name']);
        $token_ch=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($token_ch);
		if($json->status=='1'){
        	$paymentCal =paymentCalculate($prk_veh_trc_dtl_id);
		 	//$response = ($paymentCal);
		 	$json = json_decode($paymentCal);
		 	if($json->status=='1'){
		 		$prk_vehicle_type=$json->prk_vehicle_type;
			 	$veh_number=$json->veh_number;
			 	$prk_admin_id=$json->prk_admin_id;
			 	$user_admin_id=$json->user_admin_id;
			 	$total_pay=$json->total_pay;
			 	$Out_time=$json->Out_time;
			 	$tot_hr=$json->need_hour;
			 	$round_hr=$json->total_hour;
			 	$advance_pay=$json->advance_pay;
			 	$need_pay=$json->need_pay;
			 	$payment_type=$json->payment_type;
		 	  	$respo = prk_veh_trc_dtl_update($prk_veh_trc_dtl_id,$prk_out_rep_name,$total_pay,$payment_type,$Out_time);
		 	  	$json = json_decode($respo);
		 		if($json->status=='1'){
		 			//user_veh_prk_verify_active($veh_number,$prk_admin_id,$user_admin_id);
		 	  		$payment_rec_status='P'; 

		 	  		global $pdoconn;
				    $sql="SELECT `user_mobile` FROM prk_veh_trc_dtl where prk_veh_trc_dtl_id = '$prk_veh_trc_dtl_id'";
				    $query  = $pdoconn->prepare($sql);
				    $query->execute();
				    $val = $query->fetch();
			        $user_mobile = $val['user_mobile'];
			        $user_nick_name = NULL;

 	  				$resp = payment_dtl($prk_veh_trc_dtl_id, $user_admin_id,$prk_admin_id, $user_mobile, $user_nick_name,$veh_number,$tot_hr, $round_hr,$total_pay, $payment_type, $prk_out_rep_name,$payment_rec_status,$prk_vehicle_type,$prk_out_rep_name,$prk_area_gate_id);
 	  				$json = json_decode($resp);
	 				if($json->status=='1'){
		 				$payment_dtl_id=$json->payment_dtl_id;
	 					$unique_prk_short_name = $prk_area_short_name;
                        $unique_type = UNIQUE_TYPE_CR;
                        $unique_ke = unique_key_generate($unique_prk_short_name,$unique_type);
                        $json = json_decode($unique_ke);
                        if($json->status=='1'){
                            $payment_receive_number = $json->invoice_no;
                            $unique_id = $json->unique_id;
                        }
		 				$payment_amount = $need_pay;
		 				$prk_user_username = $prk_out_rep_name;
                        $payment_statas = UNIQUE_STATUS_S;
		 				//$response = payment_receive_insert($payment_receive_number,$prk_admin_id,$prk_veh_trc_dtl_id,$payment_amount,$payment_statas,$prk_user_username);
		 				$in_out_flag = IN_OUT_FLAG_O;
                        $response = payment_receive_insert($payment_receive_number,$prk_admin_id,$prk_veh_trc_dtl_id,$payment_amount,$payment_statas,$prk_user_username,$in_out_flag);
	 				}else{
					    $response['status'] = 0; 
					    $response['message'] = 'Transition Failed';
					 	$response = json_encode($response);
					}
		 	  	}
		 	}else{
		 		$response = $paymentCal;
		 	}
        }else{
            $response = $token_ch;
        }
 	}else{
            $response['status'] = 0;
	    	$response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>