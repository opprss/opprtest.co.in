<?php
/*
Description: user logout
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function visitor_parking_dtl_add($prk_admin_id,$vis_prk_short_name,$vis_prk_full_name,$vis_prk_gps_location_address,$vis_prk_gps_location,$vis_prk_longitude,$vis_prk_latitude,$vis_prk_qr_code,$vis_prk_qr_code_image,$inserted_by){
    global $pdoconn;
    $vis_prk_id=uniqid();
    $sql = "INSERT INTO `visitor_parking_dtl`(`vis_prk_id`, `prk_admin_id`, `vis_prk_short_name`, `vis_prk_full_name`, `vis_prk_gps_location_address`, `vis_prk_gps_location`, `vis_prk_longitude`, `vis_prk_latitude`, `vis_prk_qr_code`, `vis_prk_qr_code_image`, `inserted_by`, `inserted_date`) VALUES ('$vis_prk_id','$prk_admin_id','$vis_prk_short_name','$vis_prk_full_name','$vis_prk_gps_location_address','$vis_prk_gps_location','$vis_prk_longitude','$vis_prk_latitude','$vis_prk_qr_code','$vis_prk_qr_code_image','$inserted_by','".TIME."')";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $id=$pdoconn->lastInsertId();
        $response['status'] = 1;
        $response['message'] = 'Successful';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
    }
   return json_encode($response); 
}
function visitor_parking_dtl_list($prk_admin_id){
    $allplayerdata = array();
    global $pdoconn;
    $sql = "SELECT `vis_prk_id`, `prk_admin_id`, `vis_prk_short_name`, `vis_prk_full_name`, `vis_prk_gps_location_address`, `vis_prk_gps_location`, `vis_prk_longitude`, `vis_prk_latitude`, `vis_prk_qr_code`, `vis_prk_qr_code_image` FROM `visitor_parking_dtl` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['vis_prk_id'] = $val['vis_prk_id'];
            // $park['prk_admin_id'] = $val['prk_admin_id'];
            $park['vis_prk_short_name'] = $val['vis_prk_short_name'];
            $park['vis_prk_full_name'] = $val['vis_prk_full_name'];
            $park['vis_prk_gps_location_address'] = $val['vis_prk_gps_location_address'];
            $park['vis_prk_gps_location'] = $val['vis_prk_gps_location'];
            $park['vis_prk_longitude'] = $val['vis_prk_longitude'];
            $park['vis_prk_latitude'] = $val['vis_prk_latitude'];
            $park['vis_prk_qr_code'] = $val['vis_prk_qr_code'];
            $park['vis_prk_qr_code_image'] = $val['vis_prk_qr_code_image'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['parking_dtl_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
function visitor_parking_dtl_delete($prk_admin_id,$vis_prk_id,$updated_by){
    global $pdoconn;
    $sql = "UPDATE `visitor_parking_dtl` SET `updated_by`='$updated_by',`updated_date`='".TIME."',`del_flag`='".FLAG_Y."' WHERE `vis_prk_id`='$vis_prk_id' AND `prk_admin_id`='$prk_admin_id'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Delete Sucessfull';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Delete Not Sucessfull';
    }
    return json_encode($response);
}
function visitor_parking_space_list($prk_admin_id){
    $allplayerdata = array();
    global $pdoconn;
    $sql = "SELECT vpd.vis_prk_id,
        vpd.vis_prk_full_name
        FROM visitor_parking_dtl vpd
        WHERE vpd.prk_admin_id='$prk_admin_id'
        AND vpd.active_flag='".FLAG_Y."'
        AND vpd.del_flag='".FLAG_N."'
        AND vpd.vis_prk_id NOT IN (SELECT 
        pvtd.gate_pass_num
        FROM prk_visit_dtl pvd,prk_veh_trc_dtl pvtd
        WHERE pvtd.prk_admin_id=pvd.prk_admin_id
        AND pvtd.prk_veh_trc_dtl_id=pvd.prk_veh_trc_dtl_id
        AND pvtd.prk_veh_out_time IS NULL
        AND pvd.end_date is NULL 
        AND pvd.visit_with_veh='".FLAG_Y."'
        AND pvd.prk_admin_id=vpd.prk_admin_id)";

    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['vis_prk_id'] = $val['vis_prk_id'];
            $park['vis_prk_full_name'] = $val['vis_prk_full_name'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['space_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>