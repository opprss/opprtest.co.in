<!-- ################################################
  
Description: this is user home page after login user redirectd to this web page, here will show all the list of user's vehicle with all details.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
include "all_nav/header.php";
$user_mobile = $_SESSION['mobile'];
$token = $_SESSION["token"];
$user_admin_id = $_SESSION['user_admin_id'];
$prk_area_list = commercial_prk_area_list();
$prk_area_list = json_decode($prk_area_list, true);
?>

<!-- header position -->
<div class="am-mainpanel"><!-- cloding in footer -->
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Parking Space</h5>
  </div><!-- am-pagetitle -->
  <div class="am-pagebody">
    <div class="card single pd-20 pd-sm-40 col-md-12">

      <div class="row" style="padding-bottom: 20px;">
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label user-lebel">Select Parking Area</label>
            <div class="select" style="">
              <select id="prk_area" style="opacity: 0.8">
                <?php
                  foreach ($prk_area_list['prk_area'] as $val){ ?>  
                    <option value="<?php echo $val['prk_admin_id']?>"><?php echo $val['prk_area_name']?></option>
                  <?php          
                  }
                ?> 
              </select>
            </div>
          </div>
        </div>
        <div class="col-md-1">
          
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label class="form-control-label user-lebel">Select Sub Unit</label>

            <div class="select" style="">
              <select id="sub_unit" style="opacity: 0.8">
                  <option value="">Select Sub Unit</option>
              </select>
            </div>
          </div>
        </div>
        <div class="col-md-1">
          
        </div>
        <div class="col-md-3">
          <div class="form-group">
              <!-- <label class="form-control-label user-lebel">&nbsp;</label> --><!-- prevent_readonly -->
              <input type="button" value="SHOW"  style="margin-top: 29px;" class="btn btn-block  btn-primary user_button" id="show">
                <!-- <button class="btn btn-block user_button" id="ok" style="margin-top: 29px;">OK</button> -->
          </div>
        </div>
      </div>

      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <form id="InList" method="post" action="" >
          <div class="row">
            <div class="col-md-12">
              <div class="list-group widget-list-group">
                <div class="col-md-12">
                  <div class="row" style="background-color:#f1f1f1;padding:10px 0;margin-top:-3px;">
                    <div class="col-md-3"><b>Vehicle Type</b></div>
                    <div class="col-md-3"><b>Total Space</b></div>
                    <div class="col-md-3"><b>Used Space</b></div>
                    <div class="col-md-3"><b>Free Space</b></div>
                  </div>
                </div>
                <div id="in_list_show" class="col-md-12">
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script type="text/javascript">
  // alert('ok');
  var user_mobile = "<?php echo $user_mobile; ?>";
  var user_admin_id = "<?php echo $user_admin_id; ?>";
  var token = "<?php echo $token; ?>";
  var userUrl = "<?php echo USER_URL; ?>";
  $(document).ready(function(){
    $('#prk_area').on("change", function() {
      var id=($("#prk_area").val());
      // alert(id);
      var urlCity = userUrl+'prk_sub_unit_list.php';
      $.ajax ({
        type: 'POST',
        url: urlCity,
        data: {prk_admin_id:id},
        success : function(data) {
          // alert(data);
          var obj=JSON.parse(data);
          //console.log(obj);
          var areaOption = "<option value=''>Select Sub Unit</option>";
          $.each(obj['prk_area'], function(val, key) {
            areaOption += '<option value="' + key['prk_sub_unit_id'] + '">' + key['prk_sub_unit_name'] + '</option>'
          });
          $("#sub_unit").html(areaOption);
        }
      });
    });
    $('#show').click(function(){
      var prk_admin_id = $('#prk_area').val();
      var prk_sub_unit_id = $('#sub_unit').val();
      parking_space_show(prk_admin_id,prk_sub_unit_id);
      // alert(prk_admin_id+prk_sub_unit_id);
    });
    demoLines='';
    demoLines += '<div class="row" style="padding:10px 0;margin-top:15px;border-bottom: 1px solid #8080806b;">\
            <div class="col-md-12" style="text-align: center;">No Data Found, Please Select The Parking Area</div>\
            </div>';
    $("#in_list_show").html(demoLines);
  });
  function parking_space_show(prk_admin_id,prk_sub_unit_id){
    var userUrl = "<?php echo USER_URL; ?>";
    var current_user = userUrl+'prk_area_sub_unit_space_count.php';
    $('#show').val('Wait ...').prop('disabled', true);
    $.ajax({
      url :current_user,
      type:'POST',
      data :
      {
        'prk_admin_id':prk_admin_id,
        'prk_sub_unit_id':prk_sub_unit_id
      },
      dataType:'html',
      success  :function(data)
      {
        $('#show').val('SHOW').prop('disabled', false);
        var demoLines = '';
        var json = $.parseJSON(data);
        if (json.status){
          // alert('data');
          var c = 0;
          for (var key in json.prking_space) {
            c +=1;
            var keyy = json.prking_space[key];
             demoLines += '<div class="row" style="padding:10px 0;margin-top:15px;border-bottom: 1px solid #8080806b;">\
              <div class="col-md-3">'+keyy['vehicle_type_dec']+'</div>\
              <div class="col-md-3">'+keyy['tot_prk_space']+'</div>\
              <div class="col-md-3">'+keyy['veh_count']+'</div>\
              <div class="col-md-3">'+keyy['veh_avl']+'</div>\
            </div>';
          }
        }else{
          demoLines += '<div class="row" style="padding:10px 0;margin-top:15px;border-bottom: 1px solid #8080806b;">\
            <div class="col-md-12" style="text-align: center;">List is Empty</div>\
            </div>';
        }
        // $("#out_list_show").html(demoLines);
        $("#in_list_show").html(demoLines);
      }
    });
  }
</script>

      <!-- footer part -->
<?php include "all_nav/footer.php"; ?>
      <!-- footer part -->
      
      