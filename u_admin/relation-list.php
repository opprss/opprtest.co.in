<!-- ################################################
Description: List of user's vechile | user can delete vehicle from this page.
Developed by: Soemen Banerjee
Created Date: 17-03-2018
 ####################################################-->
<?php  
  include "all_nav/header.php";
  // $list_id = isset($_REQUEST['list_id']) ? trim($_REQUEST['list_id']) : '';
  // $user_apar_id =base64_decode($list_id);
  $to_user_relation_list1 = to_user_relation_list($user_admin_id);
  $to_user_relation_list = json_decode($to_user_relation_list1, true);
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
  .error{
    font-size: 11px;
    color: red;
  }
  th,td{
    text-align: center;
  }
</style>
  <script src="../lib/validation/jquery.validate.js"></script>
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
  <div class="am-mainpanel">
    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Tenant Property List<?//=$to_user_relation_list1?></h5>
    </div>
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40">
        <div class="table-wrapper">
          <table id="datatable1" class="table display" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="wd-2p">Sl No.</th>
                <th class="wd-15p">Property Name</th>
                <!-- <th class="wd-20p">Address</th> -->
                <th class="wd-10p">Sender Name</th>
                <th class="wd-10p">Security Amount</th>
                <th class="wd-10p">Agreement Amount</th>
                <th class="wd-10p">Agreement Date</th>
                <th class="wd-10p">Agreement Month</th>
                <th class="wd-10p">Send Date</th>
                <!-- <th class="wd-15p">Approved Date & Time</th> -->
                <th class="wd-5p">Status</th>
                <th class="wd-15p">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $x=1;
                if ($to_user_relation_list['status']){
                foreach($to_user_relation_list['relation_list'] as $value){
                  $status_full_name='';
                  $approved_button_dis='none';
                  $approved_button_ena='';
                  if ($value['user_relation_status']=='D') {
                    # code...
                    $status_full_name='Draft';
                  }elseif ($value['user_relation_status']=='R') {
                    # code...
                    $status_full_name='Reject';
                  }elseif ($value['user_relation_status']=='A') {
                    # code...
                    $status_full_name='Approved';
                    $approved_button_ena='none';
                    $approved_button_dis='';
                  }else{
                    # code...
                    $status_full_name='No Status';
                  }
                  $relation_trn_val="relation-transaction?list_id=".base64_encode($value['user_relation_id'])."&list_apar_name=".base64_encode($value['user_apar_name'])."&list_sender_name=".base64_encode($value['send_by_name'])."&list_agreement_amount=".base64_encode($value['user_relation_agreement_amount']);
              ?>
              <tr>
                <td><?=$x; ?></td>
                <td><?=$value['user_apar_name']; ?></td>
                <!-- <td><?//=$value['user_apar_address']; ?></td> -->
                <td><?=$value['send_by_name']; ?></td>
                <td><?=$value['security_deposit_amount']; ?></td>
                <td><?=$value['user_relation_agreement_amount']; ?></td>
                <td><?=$value['agreement_start_date']; ?></td>
                <td><?=$value['rent_argument_month']; ?></td>
                <td><?=$value['send_date'].' '.$value['send_time']; ?></td>
                <!-- <td><?//=$value['approved_date'].' '.$value['approved_time']; ?></td> -->
                <td><?=$status_full_name; ?></td>
                <td>
                  <!-- Transaction list -->
                  <a style="display: <?=$approved_button_dis?>;" href="<?=$relation_trn_val?>" class="" data-toggle="tooltip" data-placement="top" title="Transaction"><i class="fa fa-money" style="font-size:18px"></i></a>
                  <a  style="display: <?=$approved_button_ena?>;" href="#" class="" data-toggle="tooltip" data-placement="top" title="Transaction"><i class="fa fa-money" style="font-size:18px; color:grey;"></i></a>&nbsp;

                  <!-- Approved list -->
                  <button class="clean-button" style="display: <?=$approved_button_ena?>;" onclick="relation_group_approved(this.id)" id="<?=$value['user_relation_id']; ?>"><a href="#" title="Approved"><i class="fa fa-check" style="font-size:22px; color:green;"></i></a></button>
                  <button class="clean-button" style="border: none; background: none; display: <?=$approved_button_dis?>;" disabled=""><a href="#" title="Approved"><i class="fa fa-check" style="font-size:22px; color:grey;"></i></a></button>

                  <!-- Reject list -->
                  <button class="clean-button" onclick="relation_group_reject(this.id)" id="<?=$value['user_relation_id']; ?>"><a href="#" title="Reject"><i class="fa fa-close" style="font-size:20px; color:red;"></i></a></button>
                </td>
              </tr>
              <?php $x++; }} ?>
            </tbody>
          </table>
        </div><!-- table-wrapper -->
      </div>
    </div>
  <?php include"all_nav/footer.php"; ?>
<script type="text/javascript">
  var userUrl = "<?php echo USER_URL; ?>";
  var user_admin_id = "<?php echo $user_admin_id; ?>";
  var token = "<?php echo $token; ?>";
  var mobile = "<?php echo $user_mobile; ?>";
  function relation_group_reject(user_relation_id){
    // alert(user_incident_id);
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.9em;'>It will Reject the item permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Reject',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            var user_relation_delete = userUrl+'to_user_relation_approved.php';
            // 'user_admin_id','mobile','token','user_relation_id','user_relation_status'
            $.ajax({
              url :user_relation_delete,
              type:'POST',
              data :{
                'user_admin_id':user_admin_id,
                'mobile':mobile,
                'token':token,
                'user_relation_id':user_relation_id,
                'user_relation_status':'R'
              },
              dataType:'html',
              success  :function(data){
                // alert(data);
                var json = $.parseJSON(data);
                if (json.status){
                  $.alert({
                    icon: 'fa fa-smile-o',
                    theme: 'modern',
                    title: 'Success !',
                    content: "<p style='font-size:0.9em;'>Reject Successfully</p>",
                    type: 'green',
                    buttons: {
                      Ok: function () {
                          location.reload();
                      }
                    }
                  });
                }else{
                  $.alert({
                    icon: 'fa fa-frown-o',
                    theme: 'modern',
                    title: 'Error !',
                    content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
                    type: 'red'
                  });
                  if(json.session==0) {
                    window.location.replace("logout.php");
                  }
                }
              }
            });
          }
        }
      }
    });
  }
  function relation_group_approved(user_relation_id){
    // alert(user_incident_id);
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.9em;'>It will Approved the item permanently</p>",
      theme: 'modern',
      type: 'green',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Approved',
          btnClass: 'btn-green',
          keys: ['Y', 'shift'],
          action: function(){
            var user_relation_delete = userUrl+'to_user_relation_approved.php';
            // 'user_admin_id','mobile','token','user_relation_id','user_relation_status'
            $.ajax({
              url :user_relation_delete,
              type:'POST',
              data :{
                'user_admin_id':user_admin_id,
                'mobile':mobile,
                'token':token,
                'user_relation_id':user_relation_id,
                'user_relation_status':'A'
              },
              dataType:'html',
              success  :function(data){
                // alert(data);
                var json = $.parseJSON(data);
                if (json.status){
                  $.alert({
                    icon: 'fa fa-smile-o',
                    theme: 'modern',
                    title: 'Success !',
                    content: "<p style='font-size:0.9em;'>Approved Successfully</p>",
                    type: 'green',
                    buttons: {
                      Ok: function () {
                          location.reload();
                      }
                    }
                  });
                }else{
                  $.alert({
                    icon: 'fa fa-frown-o',
                    theme: 'modern',
                    title: 'Error !',
                    content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
                    type: 'red'
                  });
                  if(json.session==0) {
                    window.location.replace("logout.php");
                  }
                }
              }
            });
          }
        }
      }
    });
  }
</script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>
