<?php
/*
Description: new user vehicle parking parking area 
Developed by: Rakhal Raj Mandal
Created Date: 10-04-2018
Update date : 17-05-2018,25-05-2018,
*/
require_once 'api/reg_api.php';
$response = array();
$re = array();
if(isAvailable(array('user_incident_id','user_admin_id','mobile','token'))){
    if(isEmpty(array('user_incident_id','user_admin_id','mobile','token'))){
        
        $user_incident_id = trim($_POST['user_incident_id']);
        $user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
        $token = trim($_POST['token']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){ 
            $response = incident_show($user_incident_id);
        }else{
            $response = $resp; 
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>