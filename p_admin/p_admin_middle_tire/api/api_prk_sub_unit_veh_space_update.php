<?php
/*
Description: Parking area sub unit vehicle space update.
Developed by: Rakhal Raj Mandal
Created Date: 05-05-2018
Update date : -------
*/ 
function prk_sub_unit_veh_space_update($prk_sub_unit_veh_space_id,$prk_sub_unit_id,$prk_admin_id,$veh_type,$veh_space,$prk_sub_unit_veh_space_eff_date,$prk_sub_unit_veh_space_end_date,$user_name){
    global $pdoconn;
    $response = array();

    $sql = "UPDATE `prk_sub_unit_veh_space` SET `updated_by`= '$user_name',`updated_date`= '".TIME."',`active_flag`= '".FLAG_N."' WHERE `prk_sub_unit_veh_space_id`='$prk_sub_unit_veh_space_id'";
    $query = $pdoconn->prepare($sql);
    $query->execute();
    $sql = "INSERT INTO `prk_sub_unit_veh_space`(`prk_sub_unit_id`, `prk_admin_id`, `veh_type`, `veh_space`,`prk_sub_unit_veh_space_eff_date`, `prk_sub_unit_veh_space_end_date`, `inserted_by`, `inserted_date`) VALUES ('$prk_sub_unit_id','$prk_admin_id','$veh_type','$veh_space','$prk_sub_unit_veh_space_eff_date','$prk_sub_unit_veh_space_end_date','$user_name','".TIME."')";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Update Successfull';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Update Not Successfull';
        return json_encode($response);
    }
}
?>