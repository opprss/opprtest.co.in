<?php
/*
Description: parking admin registration.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
 require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('p_ad_name','p_ad_password','p_ad_repassword','p_ar_name','prk_ar_email','p_ar_rep_name','p_ar_rep_mobile','t_c_flag','parking_type'))){
 	if(isEmpty(array('p_ad_name','p_ad_password','p_ad_repassword','p_ar_name','prk_ar_email','p_ar_rep_name','p_ar_rep_mobile','t_c_flag','parking_type'))){

		$p_ad_name = trim($_POST['p_ad_name']);
	 	$p_ad_password = trim($_POST['p_ad_password']);
	 	$p_ad_repassword = trim($_POST['p_ad_repassword']);
	 	$p_ar_name = trim($_POST['p_ar_name']);
	 	$prk_ar_email = trim($_POST['prk_ar_email']);
	 	$p_ar_rep_name = trim($_POST['p_ar_rep_name']);
	 	$p_ar_rep_mobile = trim($_POST['p_ar_rep_mobile']);
	 	$t_c_flag = trim($_POST['t_c_flag']);
	 	$parking_type = trim($_POST['parking_type']);

        $respon=parkingAreaReg($p_ad_name,$p_ad_password,$p_ad_repassword,$p_ar_name,$prk_ar_email,$p_ar_rep_name,$p_ar_rep_mobile,$t_c_flag,$parking_type);
        echo ($respon);
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?>