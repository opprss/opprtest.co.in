<?php
/*
Description: Parking area account status check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function p_employee_form_list(){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT * FROM `form_dtl` where `active_flag`='Y' AND `del_flag`='N' AND `form_seq`=3 AND `group_form_id`=0 ORDER BY `form_order` ASC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['form_id'] = $val['form_id'];
            $park['form_name'] = $val['form_name'];
            $park['form_display_name'] = $val['form_display_name'];
            $park['form_php_name'] = $val['form_php_name'];
            $park['form_seq'] = $val['form_seq'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['from_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>