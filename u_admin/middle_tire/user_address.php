<?php 
/*
Description: user gate pass delete.
Developed by: Rakhal Raj Mandal
Created Date: 18-02-2018
Update date : 18-05-2018
*/
 require_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_admin_id','user_address','landmark','user_add_area','user_add_city','user_add_state','user_add_country','user_add_pin','prk_admin_id','tower_name','falt_name','mobile','token'))){
 	if(isEmpty(array('user_admin_id','user_address','user_add_area','user_add_city','user_add_state','user_add_country','user_add_pin','mobile','token'))){
	 	$user_admin_id = trim($_POST['user_admin_id']);
        $user_address = trim($_POST['user_address']);
        $landmark = trim($_POST['landmark']);
        $user_add_area = trim($_POST['user_add_area']);
        $user_add_city = trim($_POST['user_add_city']);
        $user_add_state = trim($_POST['user_add_state']);
        $user_add_country = trim($_POST['user_add_country']);
        $user_add_pin = trim($_POST['user_add_pin']);
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $tower_name = trim($_POST['tower_name']);
        $falt_name = trim($_POST['falt_name']);
        $mobile = trim($_POST['mobile']);
        $token = trim($_POST['token']);

        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $response = user_address($user_admin_id,$user_address,$landmark,$user_add_area,$user_add_city,$user_add_state,$user_add_country,$user_add_pin,$mobile,$prk_admin_id,$tower_name,$falt_name);
        }else{
            $response = $resp; 
        }
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            $response =  json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>