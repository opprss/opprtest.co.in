<?php
include('global_asset/config.php');
session_start();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Meta -->
    <!-- <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels"> -->


    <title>OPPRSS | SIGNIN</title>
    <link rel="shortcut icon" href="<?php echo FAVICON ?>" type="image/x-icon">

    <!-- vendor css -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">
    <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="../css/style.css"> -->

    <!-- Amanda CSS -->
    <link rel="stylesheet" href="css/amanda.css">
    <link rel="stylesheet" href="css/style.css">
    <style>
      .size{
        font-size: 11px;
      }
    .error_size{
      font-size: 11px;
      color: gray;

    }
    .success{
      font-size: 11px;
      color: green;
    }
    </style>

  </head>

  <body>

    <div class="am-signin-wrapper">
      <div class="am-signin-box">
        <div class="row no-gutters">
          <div class="col-lg-5">
            <div class="signin-logo">
              <img src="<?php echo LOGO ?>" width="300px">
              <p>Welcome to OPPR Software Solution</p>
              <p class="d-none d-sm-block">A private, dynamic young, non-government company making India digital. </p>

              <hr>
              <p>Don't have an account? <br>
                Sign up as <br>
                <a href="user_admin/signup.php">User</a>
                 &nbsp;-or-&nbsp;
                <a href="prk_admin/vender_signup.php">Parking Area</a></p>
            </div>
          </div>
          <div class="col-lg-7">
            <h5 class="tx-gray-800 mg-b-25">Reset Password</h5>
            
            <div class="mg-t-10">
              <?php
                if (isset($_SESSION['error'])) {
                  echo "<p style=color:red>".$_SESSION['error']."</p>";
                  /*unset($_SESSION['login_error_msg'];*/
                  unset($_SESSION['error']);
                }
              ?>
            </div>
            <div class="mg-t-10 pull-right" style="font-size: 0.8em;"><a href="signin.php">Sign In</a></div>
          </div><!-- col-7 -->
        </div><!-- row -->
        <p class="tx-center tx-white-5 tx-12 mg-t-15">&copy; 2018 by Oppr Software Solution. All Rights Reserved</p>
      </div><!-- signin-box -->
    </div><!-- am-signin-wrapper -->

    <script src="lib/jquery/jquery.js"></script>
    <script src="lib/popper.js/popper.js"></script>
    <script src="lib/bootstrap/bootstrap.js"></script>
    <script src="lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>

    <script src="js/amanda.js"></script>

    <!-- validation -->
    <!-- <script src="../lib/validation/jquery-1.11.1.js"></script> -->
    <script src="lib/validation/jquery.validate.js"></script>
  </body>

<script type="text/javascript">

        /*validation*/
        $( document ).ready( function () {


          

        } );

        function caps() {
          var x = document.getElementById("mob_name");
          x.value = x.value.toUpperCase();
        }
    </script>


</html>
