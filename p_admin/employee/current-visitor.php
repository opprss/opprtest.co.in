<!-- ################################################
  
Description: 
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
 
<?php include "all_nav/header.php";?>
<style type="text/css">
  .size{
    font-size: 11px;
    text-transform: uppercase;
  }
  .error_size,.error{
    font-size: 11px;
    color: red;
  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
  th,tr{
    text-align: center;
  }
</style> 
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="../../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Current Visitor</h5>
  </div>
</div>
<div class="am-mainpanel">
  <div class="am-pagebody" id="data">
    <div class="card pd-20 pd-sm-40" style="padding: 10px;">
      <div class="table-wrapper">
        <table id="datatable1" class="table display responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="wd-5p">Img</th>
              <th class="wd-15p">name</th>
              <th class="wd-10p">mobile</th>
              <th class="wd-10p">Visitor Token</th>
              <th class="wd-10p">In date</th>
              <th class="wd-10p">In Time</th>
              <th class="wd-10p text-center">Action</th>
            </tr>
          </thead>
          <tbody id="list_vis"></tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include"all_nav/footer.php"; ?>
<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="../../lib/datatables-responsive/jquery.dataTables.min.js"></script>
<script src="../../lib/datatables-responsive/dataTables.buttons.min.js"></script>
<script src="../../lib/datatables-responsive/jszip.min.js"></script>
<script src="../../lib/datatables-responsive/pdfmake.min.js"></script>
<script src="../../lib/datatables-responsive/vfs_fonts.js"></script>
<script src="../../lib/datatables-responsive/buttons.html5.min.js"></script>
<script>
  $( document ).ready( function () {
    // code...
    visitor_list();
  });
  function visitor_list() {
    $('#refresh_button').show();
    var visit_dtl_list = pkrEmpUrl+'prk_visit_dtl_list.php';
    // 'prk_admin_id','prk_area_user_id','token'
    $.ajax({
      url :visit_dtl_list,
      type:'POST',
      data :{
        'prk_admin_id':prk_admin_id,
        'prk_area_user_id':prk_area_user_id,
        'token':token
      },
      dataType:'html',
      success  :function(data){
        // alert(data);
        var demoLines = '';
        var json = $.parseJSON(data);
        $('#datatable1').dataTable().fnDestroy();
        if (json.status){
          for (var key in json.visit) {
            var keyy = json.visit[key];
            demoLines += '<tr>\
              <td><img src="'+keyy['visit_img']+'" style="height: 40px; border-radius: 50%;"></td>\
              <td>'+keyy['visit_name']+'</td>\
              <td>'+keyy['visit_mobile']+'</td>\
              <td>'+keyy['visitor_token']+'</td>\
              <td>'+keyy['visit_in_date']+'</td>\
              <td>'+keyy['visit_in_time']+'</td>\
              <td class="text-center">';
           
            if(keyy['visitor_out_verify_flag'] != 'Y'){
              demoLines += '<button style="color:white; background-color:green;" type="button" class="out btn prk_button" id="'+keyy['prk_visit_dtl_id']+'" onclick="vistor_out_verify(this.id)" data-toggle="tooltip" data-placement="top"><i class="fa fa-sign-out"></i> Verify</button>';
            }else{
              demoLines += '<button style="" type="button" class="out btn" id="'+keyy['prk_visit_dtl_id']+'" onclick="vistor_out_verify(this.id)" data-toggle="tooltip" data-placement="top" disabled><i class="fa fa-sign-out"></i> Verified</button>';
            }
            demoLines += '</td>\
            </tr>';
          }
        }
        $("#list_vis").html(demoLines);
        datatable_show();
      }
    });
  }
  function vistor_out_verify(prk_visit_dtl_id){
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will Out Verify Visitor</p>",
      theme: 'modern',
      type: 'green',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Verify',
          btnClass: 'btn-green',
          keys: ['Y', 'shift'],
          action: function(){
            // alert(prk_visit_dtl_id);
            if (prk_visit_dtl_id != '') {
              var urlDtl = pkrEmpUrl+'visitor_visit_out_verify.php';
              // 'prk_admin_id','prk_area_user_id','prk_user_username','token','prk_visit_dtl_id'
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :{
                  'prk_visit_dtl_id':prk_visit_dtl_id,
                  'prk_admin_id':prk_admin_id,
                  'prk_area_user_id':prk_area_user_id,
                  'prk_user_username':prk_user_username,
                  'token':token
                },
                dataType:'html',
                success  :function(data){
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Visitor Verify successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                            // location.reload(true);
                          visitor_list();
                        }
                      }
                    });
                  }else{
                    if (json.session=='0') {
                      window.location.replace("logout.php");
                    }
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
  function datatable_show(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      language: {
      searchPlaceholder: 'Search Here',
      sSearch: '',
      lengthMenu: '_MENU_ Page',
      },
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      buttons: [
        {
          text: '<span id="refresh_button"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
          action: function ( e, dt, node, config ) {
              // alert( 'Button activated' );
              visitor_list();
          }
        },
        { 
          extend: 'copyHtml5', 
          //footer: true,
          title: 'CURRENT VISITOR SHEET' 
        },
        { 
          extend: 'excelHtml5', 
          //footer: true,
          title: 'CURRENT VISITOR SHEET'  
        },
        {
          extend: 'pdfHtml5',
          //footer: true,
          title: 'CURRENT VISITOR SHEET',
          customize: function(doc) {
            doc.content[1].margin = [ 0, 0, 0, 0 ]
          }
        },
      ]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    $('#refresh_button').hide();
  }
</script>
