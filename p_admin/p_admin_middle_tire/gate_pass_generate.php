<?php 
/*
Description: Parking adimn gate pass genaret for user.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
 $response = array();

 if(isAvailable(array('prk_admin_id','prk_sub_unit_id'))){
    if(isEmpty(array('prk_admin_id'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
        $prk_sub_unit_short_name = trim($_POST['prk_sub_unit_id']);
        $token = ($_POST['token']);

        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            $response= gate_pass_generate($prk_admin_id,$prk_sub_unit_short_name);
            echo ($response);
        }else{
            $response = $resp;
            echo $response;
        }
    }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
 
?>