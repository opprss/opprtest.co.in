<?php 
/*
Description: user address show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
require_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_admin_id','mobile','token'))){
 	if(isEmpty(array('user_admin_id','mobile','token'))){
	 	$user_admin_id = trim($_POST['user_admin_id']);
        $mobile = trim($_POST['mobile']);
        $token = trim($_POST['token']);
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        $oth11=$oth12=$oth13='';
        if($json->status=='1'){
            $sql = "SELECT u_add.`prk_admin_id`,
                t_dtl.`tower_name`,
                f_dtl.`flat_name`
                FROM `user_address` u_add,`tower_details` t_dtl, `flat_details` f_dtl
                WHERE t_dtl.`prk_admin_id`=u_add.`prk_admin_id`
                AND t_dtl.`tower_id`=u_add.`tower_name` 
                AND t_dtl.`active_flag`='".FLAG_Y."' 
                AND t_dtl.`del_flag`='".FLAG_N."'
                AND f_dtl.`prk_admin_id`=u_add.`prk_admin_id`
                AND f_dtl.`flat_id`=u_add.`falt_name`
                AND f_dtl.`active_flag`='".FLAG_Y."' 
                AND f_dtl.`del_flag`='".FLAG_N."'
                AND u_add.`user_admin_id`='$user_admin_id' 
                AND u_add.`default_flag`='".FLAG_Y."' 
                AND u_add.`active_flag`='".FLAG_Y."'
                AND u_add.`del_flag`='".FLAG_N."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $val = $query->fetch();
                $prk_admin_id = $val['prk_admin_id'];
                $tower_name = $val['tower_name'];
                $flat_name = $val['flat_name'];
                $response = user_emergency_notification($prk_admin_id,$user_admin_id,$tower_name,$flat_name,$oth11,$oth12,$oth13);
            }else{
                $response['status'] = 0;
                $response['message'] = 'Please set the default address';
                $response =  json_encode($response);
            }
        }else{
            $response = $resp; 
        }
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response =  json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>