<?php 
/*
Description: otp send.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$responsese = array();

if(isAvailable(array('mobile','name','email'))){

    if(isEmpty(array('name'))){
        $mobile = trim($_POST['mobile']);
        $name = ($_POST['name']);
        $email = trim($_POST['email']);
        
        if(mobileChecker($mobile)=='true'){
            $response=otp_send($name,$email,$mobile);
            echo ($response);
        }elseif (emailChecker($email)=='true'){
            $response=otp_send($name,$email,$mobile);
            echo ($response);
        }else{
            $responsese['status'] = 0;
            $responsese['message'] = 'Mobile or Email  Not Valide';
            echo json_encode($responsese);
        }
    }else{
            $responsese['status'] = 0;
            $responsese['message'] = 'All Fields Are Mandatory';
            echo json_encode($responsese);
    }
}else{
        $responsese['status'] = 0; 
        $responsese['message'] = 'Invalid API Call';
        echo json_encode($responsese);
}
?>