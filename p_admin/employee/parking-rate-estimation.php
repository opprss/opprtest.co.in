<!-- ################################################
  
Description: 
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
 
<?php 
include "all_nav/header.php";
if (isset($_SESSION['prk_admin_id'])) {
 //$insert_by=$_SESSION['parking_admin_name'];
  $prk_admin_id = $_SESSION['prk_admin_id'];
  $prk_user_username = $_SESSION["prk_user_username"];
  $prk_area_user_id = $_SESSION["prk_area_user_id"];
  $token = $_SESSION["token"];
  $prk_area_gate_type = $_SESSION["prk_area_gate_type"]; 

  $response = prk_area_advance_status($prk_admin_id);
  $response = json_decode($response, true);
  $advance_flag = $response['advance_flag'];
  $advance_pay = $response['advance_pay'];
  $print_in_flag = $response['print_in_flag'];
  $print_out_flag = $response['print_out_flag'];
  //Vehicle{"status":1,"message":"Successful","prk_admin_name":"GARAIA_5684","advance_flag":"N","advance_pay":"0","prient_in_flag":"N","prient_out_flag":"N"}

}
 ?>
<!-- vendor css -->
  <style type="text/css">
    .size{
      font-size: 11px;
      text-transform: uppercase;
    }
  .error_size{
    font-size: 11px;
    color: red;

  }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
  </style> 
  <!-- links for datatable -->
  <link href="../../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../../lib/select2/css/select2.min.css" rel="stylesheet">

  <div class="am-mainpanel"><!-- cloding in footer -->

    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Parking Rate Estimation</h5>
    </div><!-- am-pagetitle -->
  </div>

  <div class="am-mainpanel">
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40" style="padding: 10px;">
        
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <form id="calculateRateleForm" action="#" method="post">
            <div class="row">
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">VEHICLE NUMBER <span class="tx-danger">*</span></label>
                  <input type="text" class="form-control" placeholder="Vehicle Number" name="veh_number" id="veh_number" onkeyup="vehNoUppercase();" maxlength="10">
                  <span style="position: absolute;" id="error_veh_number" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">VEHICLE TYPE <span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="vehicle_type" id="vehicle_type" value="" style="opacity: 0.8; font-size:14px">
                      </select>
                    </div>
                    <span id="error_vehicle_type" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">Hour <span class="tx-danger"></span></label>
                    <input type="text" name="hour" id="hour" class="form-control" placeholder="HOUR"  disabled="disabled"/>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">Rate <span class="tx-danger"></span></label>
                    <input type="text" name="rate" id="rate" class="form-control" placeholder="RATE" disabled="disabled"/>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">Multiply By <span class="tx-danger"></span></label>
                    <input type="text" name="multiply" id="multiply" class="form-control" placeholder="Multiply By" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" disabled="disabled"/>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-layout-footer mg-t-20">
                  <div class="row pd-t-10">
                      <div class="col-lg-12">
                          <button type="button" class="btn btn-block prk_button" id="calculate" disabled="disabled">CALCULATE</button>
                        </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">Total Hour <span class="tx-danger"></span></label>
                    <input type="text" name="total_hour" id="total_hour" class="form-control" placeholder="TOTAL HOUR"  disabled="disabled"/>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">Total RATE <span class="tx-danger"></span></label>
                    <input type="text" name="total_rate" id="total_rate" class="form-control" placeholder="TOTAL RATE" disabled="disabled"/>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">rate validity <span class="tx-danger"></span></label>
                    <input type="text" name="total_rate" id="rate_validity" class="form-control" placeholder="rate validity" value="7" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"/>
                </div>
              </div>
              <input type="hidden" name="veh_rate_id" id="veh_rate_id">
              <div class="col-lg-4">
                <div class="form-layout-footer mg-t-20">
                  <div class="row pd-t-10">
                      <div class="col-lg-6">
                          <button type="button" class="btn btn-block prk_button" id="save" disabled="disabled">SAVE &amp; PRINT</button>
                      </div>
                      <div class="col-lg-6">
                        <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  <!-- datatable -->
  <div class="am-pagebody" id="data">

      <div class="card pd-20 pd-sm-40" style="padding: 10px;">
        <div class="table-wrapper">
          <table id="datatable1" class="table display responsive nowrap">
            <thead>
              <tr>
                <th class="wd-15p">Type</th>
                <th class="wd-15p">Vehicle Number</th>
                <th class="wd-15p">Hour</th>
                <th class="wd-15p">Rate</th>
                <th class="wd-15p">Multiply</th>
                <th class="wd-15p">total pay</th>
                <th class="wd-15p">issue date</th>
                <th class="wd-15p">rate validity</th>
                <th class="wd-15p">Action</th>
              </tr>
            </thead>
            <tbody>
            <!-- data -->
            <?php
              $response = veh_prk_estimation_list($prk_admin_id);
              $response = json_decode($response, true);
              //print_r($response['estimation'][0]);
              if (isset($response['estimation'])) {
              foreach ($response['estimation'] as $key => $value) {
                //print_r($value);
              ?>
              <tr>
                <td><img src="<?php echo OPPR_BASE_URL.$value['vehicle_typ_img']; ?>" style="height: 40px;"></td>
                <td><?php echo $value['veh_number']; ?></td>
                <td><?php echo $value['veh_rate_hour']; ?></td>
                <td><?php echo "Rs. ".$value['veh_rate']; ?></td>
                <td><?php echo $value['no_of_time_multiple']; ?></td>
                <!-- <td><?php //echo $value['veh_rate_hour']; ?></td> -->
                <td><?php echo "Rs. ".$value['total_pay']; ?></td>
                <td><?php echo $value['issue_date']; ?></td>
                <td><?php echo $value['rate_validity']." Days"; ?></td>
                <td><a class="out btn prk_button print" id="print" href="<?PHP echo PRK_EMP_URL ?>veh_prk_est_print.php?id=<?php echo $value['prk_veh_prk_est_id'] ?>" style="color: #FFF;"><i class="fa fa-print"></i> PRINT</a></td>
            </tr>
              <?php
              }}
            ?>
            <!-- /data -->
            </tbody>
          </table>
        </div><!-- table-wrapper -->
      </div><!-- card -->
  <!-- /datatable -->
  <?php include"all_nav/footer.php"; ?>
  <script type="text/javascript">
    var pkrEmpUrl = "<?php echo PRK_EMP_URL; ?>";
    var prk_admin_id = "<?php echo $prk_admin_id; ?>";
    var prk_user_username = "<?php echo $prk_user_username; ?>";
    var prk_area_user_id = "<?php echo $prk_area_user_id; ?>";
  

    $( document ).ready( function () {
      $('[data-toggle="tooltip"]').tooltip();   

      $(".print").click(function(){
        var urlPrint = $(this).attr('href');
        window.open(urlPrint,'mywindow','width=850,height=600');
        return false;
      });

      var urlVehicleType = pkrEmpUrl+'vehicle_type.php';

      /*veh-type*/
      $.ajax ({
          type: 'POST',
          url: urlVehicleType,
          data: "",
          success : function(data) {
            // alert(data);
              var obj=JSON.parse(data);
             var areaOption = "<option value='' style='color:gray;' selected='selected'>VEHICLE TYPE</option>";
             $.each(obj, function(val, key) {
              // alert("Value is :" + key['e']);
              areaOption += '<option value="' + key['vehicle_Sort_nm'] + '">' + key['vehicle_type'] + '</option>'
             });

            $("#vehicle_type").html(areaOption);

          }
      });

      $("#multiply").blur(function (){
        	var multiply = $('#multiply').val();
        	multiply = Number(multiply);
	        if(multiply==''){
	        	$('#multiply').val('1');
	        }
	        if(multiply===0){
	        	$('#multiply').val('1');
	        }
        });

      $("#vehicle_type").blur(function () {
        var veh_type = $('#vehicle_type').val();
        
        if(veh_type==''){
          $('#error_vehicle_type').text('Vehicle type required');
        }else{
          $('#error_vehicle_type').text('');;
        }
      });

      $("#veh_number").blur(function(){
          var veh_number = $('#veh_number').val();
          IsVehNumber(veh_number);
        });
        function IsVehNumber(veh_number) {
            var urlVehNoCheck = pkrEmpUrl+'v_numberChecker.php';

            $.ajax({
              url :urlVehNoCheck,
              type:'POST',
              data :
              {
                'prk_admin_id':prk_admin_id,
                'v_number':veh_number
              },
              dataType:'html',
              success  :function(data)
              {
                // alert(data);
                var json = $.parseJSON(data);
                vehNumberReturn(json.status);
              }
            });
        } 
        function vehNumberReturn(flag){
          if(!flag){
              $('#error_veh_number').text('Vehicle number is invalid');
              //return false;
          }else{
              $('#error_veh_number').text('');
              //return true;
          }
        }

        

      $('#save').click(function () {
        var veh_type = $('#vehicle_type').val();
        var veh_number = $('#veh_number').val();
        var hour = $('#hour').val();
        var rate = $('#rate').val();
        var multiply = $('#multiply').val();

        var total_hour = $('#total_hour').val();
        var total_rate = $('#total_rate').val();
        var rate_validity = $("#rate_validity").val();
        var veh_rate_id = $("#veh_rate_id").val();
        if (rate_validity == '') {
          rate_validity = 1;
        }
        
        if(veh_type!='' && veh_number!=''){
          
          var urlVehNoCheck = pkrEmpUrl+'v_numberChecker.php';

          $.ajax({
            url :urlVehNoCheck,
            type:'POST',
            data :
            {
              'prk_admin_id':prk_admin_id,
              'v_number':veh_number
            },
            dataType:'html',
            success  :function(data)
            {
              //alert(data);
              var json = $.parseJSON(data);
              if (json.status) {
                  /*-----*/
                  var urlVehIn = pkrEmpUrl+'veh_prk_estimation.php';
                  $.ajax({
                    url :urlVehIn,
                    type:'POST',
                    data :
                    {
                      'veh_type':veh_type,
                      'veh_number':veh_number,
                      'veh_rate_id':veh_rate_id,
                      'veh_rate_hour':hour,
                      'prk_user_username':prk_user_username,
                      'veh_rate':rate,
                      'no_of_time_multiple':multiply,
                      'total_pay':total_rate,
                      'rate_validity':rate_validity,
                      'prk_admin_id':prk_admin_id
                    },
                    dataType:'html',
                    success  :function(data)
                    {
                     // alert(data);
                      //$("#data").load(location.href + " #data");//reefresh only one div
                      var json = $.parseJSON(data);
                      if (json.status){
                        ///location.reload();
                             // {"status":1,"prk_veh_prk_est_id":"21","message":"Sucessful"}
                        var prk_veh_prk_est_id = json.prk_veh_prk_est_id;
                        urlPrint = pkrEmpUrl+"veh_prk_est_print.php?id="+prk_veh_prk_est_id;
                        window.open(urlPrint,'mywindow','width=850,height=600');
                        location.reload();
                        /*$.alert({
                          icon: 'fa fa-smile-o',
                          theme: 'modern',
                          title: 'Success !',
                          content: "<p style='font-size:0.9em;'>Calculation added successfully</p>",
                          type: 'green',
                          buttons: {
                            Ok: function () {
                              location.reload();
                            }
                          }
                        });*/
                      }else{
                        $.alert({
                          icon: 'fa fa-frown-o',
                          theme: 'modern',
                          title: 'Error !',
                          content: "<p style='font-size:0.8em;'>Somting went wrong</p>",
                          type: 'red'
                        });
                      }
                    }
                  });
                  /*------*/
                }else{
                  $('#error_veh_number').text('Vehicle number is invalid');
                }
            }
          });
        }else{
          if (veh_type == '') {
            $('#error_vehicle_type').text('Vehicle type required');
          }if (veh_number == '') {
            $('#error_veh_number').text('Vehicle number is required');
          }
        }
      });
      /*rate*/
      $( "#vehicle_type" ).change(function() {
          var vehicle_type = $('#vehicle_type').val();
          var prk_admin_id = "<?php echo $prk_admin_id;?>";
          var urlAreaFindList = pkrEmpUrl+'prk_area_rate_find_list.php';

          $.ajax({
            url :urlAreaFindList,
            type:'POST',
            data :
            {
              'prk_admin_id':prk_admin_id,
              'veh_type':vehicle_type,
            },
            dataType:'html',
            success  :function(data)
            {
              //{"status":1,"message":"Successful","veh_rate_hour":"10","veh_rate":"50"} 
              //alert(data);
              var json = $.parseJSON(data);
              if (json.status) {
                $("#hour").val(json.veh_rate_hour);
                $("#rate").val(json.veh_rate);
                $("#veh_rate_id").val(json.prk_rate_id);
                $("#multiply").attr("disabled", false);
                $("#multiply").val('1');
                $("#calculate").attr("disabled", false);

                $('#calculate').click(function () {
                  var multiply = $('#multiply').val();
                  if (multiply == '') {
                    multiply = 1;
                  }/*if(multiply == 0){
                    multiply = 1;
                  }*/
                  var totalHour = json.veh_rate_hour * multiply;
                  var totalRate = json.veh_rate * multiply;
                  $("#total_hour").val(totalHour);
                  $("#total_rate").val(totalRate);
                  $("#save").attr("disabled", false);
                });
              }else{
                $("#multiply").attr("disabled", true);
                $("#calculate").attr("disabled", true);
                $("#save").attr("disabled", true);

                $("#hour").val('');
                $("#rate").val('');
                $("#total_hour").val('');
                $("#total_rate").val('');
                $.alert({
                  icon: 'fa fa-frown-o',
                  theme: 'modern',
                  title: 'Error !',
                  content: "<p style='font-size:0.9em;'>You must have to choose vehicle type</p>",
                  type: 'red'
                });
              }
            }
          });

        });
      /*rate*/
      $('#multiply').on('keyup', function(){
        var multiply = $('#multiply').val();

        if (multiply == '') {
          $("#calculate").attr("disabled", true);
          $("#save").attr("disabled", true);
          $("#total_hour").val('');
          $("#total_rate").val('');
        }else{
          $("#calculate").attr("disabled", false);
        }
        
      });



      $("#veh_number").on('input', function(evt) {
      var input = $(this);
      var start = input[0].selectionStart;
      $(this).val(function (_, val) {
        return val.toUpperCase();
      });
      input[0].selectionStart = input[0].selectionEnd = start;
    }); 

    });
  </script>
  <!-- scripts for datatable -->
  <script src="../../lib/highlightjs/highlight.pack.js"></script>
  <script src="../../lib/datatables/jquery.dataTables.js"></script>
  <script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>

  <script>
      $(function(){
        'use strict';

        $('#datatable1').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search Here',
            sSearch: '',
            lengthMenu: '_MENU_ Page',
            sEmptyTable: 'No parking area found, please add.',
          }
        });

        $('#datatable2').DataTable({
          bLengthChange: false,
          searching: false,
          responsive: true
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

      });


      //functions
      function upperCase(a){
        setTimeout(function(){
            a.value = a.value.toUpperCase();
        }, 1);
      }
      function lowerCase(a){
        setTimeout(function(){
          //alert(a);
            a.value = a.value.toLowerCase();
        }, 1);
      }
      $("#name").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
            event.preventDefault();
          }
        });
  </script>
