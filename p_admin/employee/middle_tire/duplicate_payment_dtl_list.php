<?php 
/*
Description: Parking employ Payment report download
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
include_once 'api/parkAreaReg_api.php';
// include_once 'api/global_api.php';

 $response = array();
 if(isAvailable(array('prk_admin_id','veh_number','start_date','end_date'))){
 	if(isEmpty(array('prk_admin_id'))){

	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$veh_number = $_POST['veh_number'];
        $start_date =$_POST['start_date'];
        $end_date =$_POST['end_date'];
        
        $response = duplicate_payment_dtl_list($prk_admin_id,$veh_number,$start_date,$end_date);
            
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
 }
 echo $response;
?>