<?php 
/*
Description: parking area visit the people all list. 
Developed by: Rakhal Raj Mandal
Created Date: 09-04-2018
Update date : -------
*/
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
$response = array();
$resp = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','token','prk_sub_unit_id'))){
 	if(isEmpty(array('prk_admin_id','prk_area_user_id','token','prk_sub_unit_id'))){ 	
	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_area_user_id = trim($_POST['prk_area_user_id']);
        $token = trim($_POST['token']);
        $prk_sub_unit_short_name = trim($_POST['prk_sub_unit_id']);
        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
		if($json->status=='1'){
            
        	$response = gate_pass_generate($prk_admin_id,$prk_sub_unit_short_name);
        }else{

            $response = $resp;
        }
 	}else{
        $response['status'] = 0;
    	$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>