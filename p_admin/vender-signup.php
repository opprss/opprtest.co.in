<!-- ################################################
Description: New parking area rgistration.After sucessfull registration it will redirect to signup-thanks.php page
Developed by: Soemen Banerjee
Created Date: 17-03-2018
 ####################################################-->
<?php
  @session_start();
  if(isset($_SESSION['prk_admin_id'])){
    header('location:dashboard');
  }
  include('../global_asset/config.php');
  @session_start();
  if (isset($_SESSION['prk_admin_id'])) {
    
    $prk_admin_id = $_SESSION['prk_admin_id'];
  }
?>
<?php header('Access-Control-Allow-Origin:*') ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Meta -->
    <meta name="description" content="OPPRSS provides best quality digital parking services and digital parking solutions."/>
    <meta name="keywords" content="parking solution free , parking management software free , digital parikig solution , smart parking solution, app based parking , parking solution , automatic carparking system , parking solution in kolkata , parking ticket solutions/oppr parking solution , oppr software , parking management , autopay parking system , digital ticketing system , mobile parking solution , vehicle parking system" />
    <meta name="author" content="oppr software solution private limited">
    <title>OPPRSS | PARKING AREA SIGNUP</title>
    <link rel="shortcut icon" href="<?php echo FAVICON ?>" type="image/x-icon">
    <!-- vendor css -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">
    <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">  
    <!-- calender -->  
    <link rel="stylesheet" href="date2/css/pickmeup.css" type="text/css" />
    <script type="text/javascript" src="date2/js/pickmeup.js"></script>
    <script type="text/javascript" src="date2/demo/demo.js"></script>
    <script
      src="https://code.jquery.com/jquery-3.2.1.js"
      integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
      crossorigin="anonymous"></script>
    <script src="https://kendo.cdn.telerik.com/2017.3.1026/js/kendo.all.min.js"></script>
    <!-- Amanda CSS -->
    <link rel="stylesheet" href="../css/amanda.css">
    <link rel="stylesheet" type="text/css" href="../css/style-up.css">
    <style type="text/css">
      select{
        background: transparent;
        border-right-style:none;
        border-left-style:none; 
        border-top-style:none;
        border-color:#dcdcdc;
        border-width: 2px;
        outline: none; 
        outline: none;
        padding-left: 0px !important;
        padding-bottom: 5px !important; 
      }
      .error_size{
        padding: 0px !important;
        margin: 0px !important;
        font-size: small;
        color: #FFA07A;
      }
    </style>
  </head>
  <body>
    <div class="am-signup-wrapper">
      <div class="am-signup-box">
        <div class="row no-gutters">
          <div class="col-lg-7" style="background:url('../img/index-banner.jpg');padding-bottom: 0px;">
            <div class="signin-logo">
              <div style="text-align: left;">
              <a href="http://opprss.com" target="_blank"><img style="width: 210px; text-align: left;" src="<?php echo LOGO ?>"></a></div>
              <div class="content">
                <h1>Welcome to OPPRSS</h1>
                <h5 class="d-none d-sm-block pd-b-10">We provide an innovative and executive parking & security solution that improves parking & security experience more than ever before.</h5>  
              </div>
              <div class="row features-icon">
                <div class="col-lg-4">
                  <i class="fa fa-tasks fa-3x"></i>
                  <h6 class="pd-t-5">Digitally Tracked</h6>
                </div> 
                <div class="col-lg-4">
                  <i class="fa fa-american-sign-language-interpreting fa-3x"></i>
                  <h6 class="pd-t-5">Better Communication</h6>
                </div> 
                <div class="col-lg-4">
                  <i class="fa fa-android fa-3x "></i>
                  <h6 class="pd-t-5">App Based</h6>
                </div>                
              </div>                           
              <div class="content-footer features-icon mg-t-50" style="text-align: right;">
                <button class="btn-tab" onclick="window.open('https://www.opprss.com/contact-us', '_blank');" id="con_us">Contact Us</button>
                <button class="btn-tab" id="user_tab">User</button>
                <button class="btn-tab" id="admin_tab">Parking Area</button>
                <button class="btn-tab" id="super_admin_tab">Super Adnin</button>
                 <hr>
                <p class="tx-center tx-white-5 tx-12 mg-t-0 mg-b-0"><?php echo COPYRIGHT_EMAIL?></p>
              </div>
            </div>
          </div>
          <div class="col-lg-5">
            <h2 class="tx-gray-800 mg-b-50 text-center">Parking Area Signup</h2>
            <form id="regForm">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group ">
                    <input type="text" name="parking_area_name" id="parking_area_name" class="form-control inputText"  onkeypress="return valid(event)" class="form-control inputText" required/>
                    <span class="floating-label">PARKING AREA NAME</span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <!-- <label class="form-control-label">Mobile:</label> -->
                    <input type="text" name="username" id="username" maxlength="10" placeholder="USERNAME" class="form-control" disabled>
                    <span id="user_staus_error" class="error_show" style="position: absolute;"></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="password" name="password" id="password" class="form-control inputText" required>
                    <span class="floating-label">PASSWORD</span>
                    <span class="error_show" style="position: absolute;" id="password1"></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group error_show">
                    <input type="password" name="re_password" id="re_password" class="form-control inputText" required>
                    <span class="floating-label">CONFIRM PASSWORD</span>
                  </div><!-- form-group -->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" name="email" id="email" onkeyup="emailLowercase()" class="form-control inputText" required>
                    <span class="floating-label">EMAIL</span>
                    <!-- <div id="error_email" class="error" style="position: absolute;"></div> -->
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="select">
                    <select  name="parking_type" id="parking_type" class="select" style="opacity: 0.7">
                      <option value="">Parking Type</option>
                      <option value="R">Residential</option>
                      <option value="C">Commercial</option>
                    </select>
                    <!-- <i class="fa fa-caret-down" aria-hidden="true"></i> -->
                  </div>
                  <span style="position: absolute; display: none;" id="error_parking_type" class="error_size">Choose Parking Type</span>
                </div>
                <div class="col-md-6">
                  <div class="form-group error_show">
                    <input type="text" name="representative" id="name" onkeyup="nameUppercase()" class="form-control inputText" required>
                    <span class="floating-label">REPRESENTATIVE NAME</span>
                  </div>
                </div>
                <div class="col-md-6" id="onlyNum">
                  <div class="form-group error_show">
                   <input type="text" name="mobile" id="mobile" maxlength="10" class="form-control inputText" required>
                   <span class="floating-label">REPRESENTATIVE MOBILE</span>
                  <span id="user_staus_error"></span>
                  </div><!-- form-group -->
                </div>
                <div class="col-md-12">
                  <input type="checkbox" class="checkbox" id="agree" name="agree" required>
                  <label for="agree">Terms & Condition <a href="http://opprss.com/legal" target="_blank">more information..</a></label>
                  <br>
                  <label for="agree" class="error block"></label>
                </div> 
                <div class="col-md-6 mg-t-10">
                  <!--  -->
                  <button type="button" class="btn btn-block" id="register" >REGISTER</button>
                </div>
                <div class="col-md-6 mg-t-10">
                  <!--  -->
                  <input type="reset" class="btn btn-block" id="configreset" value="RESET">
                </div>
              </div>
              <div class="mg-t-10 pull-right" style="font-size: auto;">
                <a href="<?php echo OPPR_BASE_URL.'signin'?>">Login</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <script src="../lib/popper.js/popper.js"></script>
    <script src="../lib/bootstrap/bootstrap.js"></script>
    <script src="../lib/jquery-ui/jquery-ui.js"></script>
    <script src="../lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
    <script src="../lib/jquery-toggles/toggles.min.js"></script>
    <script src="../lib/highlightjs/highlight.pack.js"></script>
    <script src="../lib/select2/js/select2.min.js"></script>
    <script src="../js/amanda.js"></script>
    <script src="../lib/validation/jquery.validate.js"></script>
  </body>
  <script>
    $(document).ready(function() {
      $("#parking_area_name , #username , #parking_area_re").on('input', function(evt) {
        var input = $(this);
        var start = input[0].selectionStart;
        $(this).val(function (_, val) {
          return val.toUpperCase();
        });
        input[0].selectionStart = input[0].selectionEnd = start;
      }); 
      //for random number username
      $('#parking_area_name').blur(function(){
        var parking_area_name = $("#parking_area_name").val();
        var hasSpace = $('#parking_area_name').val().indexOf(' ')==0;
        if (!parking_area_name.length == 0 && !hasSpace ){
          // parking_area_name = parking_area_name.replace(/^([^\s]*)(.*)/, "$1");
          // var random_number = Math.floor(1000 + Math.random() * 9000);
          // var g_username = (parking_area_name+'_'+random_number);
          // $("#username").val(g_username);
          $("#username").attr("disabled", "disabled"); 
          $("#password").focus();
          //$("#username").css("background-color","white");
          $.ajax({
            url :'p_admin_middle_tire/admin_username_generate.php',
            type:'POST',
            data :{
              'parking_area_name':parking_area_name
            },
            dataType:'html',
            success  :function(data){
              // alert(data);
              var json = $.parseJSON(data);
              if (json.status){
                $("#username").val(json.username);
                // window.location='signup-thanks';
              }else{
                alert("somthing went wrong");
              }
            }
          });
        }else{
          //$('#error_parking').html('Must have a name');
          $("#username").val('');          
          return false;
        }
      });
    });
  </script>
  <script type="text/javascript">
    var prkUrl = "<?php echo PRK_URL; ?>";
    /*CUSTOM METHODS FOR JQUERY VALIDATION*/
    /*indian v_number validation*/
    $.validator.addMethod("v_number", function(value) {
      var patt = / /g;
      patt.compile(patt);
      var str = value.replace(patt,"");
      return /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*_#?&])[A-Za-z\d$@$!%*_#?&]{8,}$/.test(str);
      /*return value == "SOMEN";*/
    }, 'Minimum 8 characters, atleast 1 number, 1 alphabet & 1 spcial character.');
    //Password is not strong enough
    /*validation*/
    $( document ).ready( function () {
      urlPrkEmailCheck = prkUrl +'duplicate_email_check.php';
      $( "#regForm" ).validate( {
        rules: {
          'terms[]': { required: true },
          name: "required",
          representative: "required",
          v_number: "v_number",
          agree: "required",
          mobile: {
            required: true,
            number: true,
            minlength: 10                      
          },
          parking_type: "valueNotEqualsCity",
          password: {
            required: true,
            v_number: "v_number",
            minlength: 5
          },
          re_password: {
            required: true,
            equalTo: "#password"
          },
          email: {
            required: true,
            email: true,
            remote: {
              url: urlPrkEmailCheck,
              type: "POST",
              cache: false,
              dataType: "json",
              dataFilter: function(data) {
                var json = $.parseJSON(data);                      
                if (json.status) {
                  return true;
                } else {
                  return false;
                }
              }
            }
          },
        },
        messages: {
          representative: "Please enter representative name",
          name: "Please enter parking space name",
          agree: "Please Select Terms & Condition",
          mobile: {
            required: "Mobile number required",
            number: "Enter valid mobile Numberr",
            minlength: "Enter valid mobile Number"
          },
          /*parking_type: {
            required: "Choose Parking Type"
          },*/
          password: {
            required: "Enter Password"
          },
          confirm_password: {
            required: "Confirm password",
            equalTo: "Password mismatch"
          },
          email: {
            required: "Please enter your email",
            email: "Enter a valid email",
            remote: "Email address is already registered"
          }
        }
      });
      /*FORM SUBMIT*/
      $('#register').click(function () {
        if($("#regForm").valid()){
          var p_ar_name = $('#parking_area_name').val();
          var p_ad_name = $('#username').val();
          var p_ad_password = $('#password').val();
          var p_ad_repassword = $('#re_password').val();
          var prk_ar_email = $('#email').val();
          var p_ar_rep_name = $('#name').val();
          var p_ar_rep_mobile = $('#mobile').val(); 
          var parking_type = $('#parking_type').val(); 
          var t_c_flag= "Y";
          var urlParkAreaReg = prkUrl+'parkAreaReg.php';
          // alert(parking_type);
          $.ajax({
            url :urlParkAreaReg,
            type:'POST',
            data :{
              'p_ar_name':p_ar_name,
              'p_ad_name':p_ad_name,
              'p_ad_password':p_ad_password,
              'p_ad_repassword':p_ad_repassword,
              'prk_ar_email':prk_ar_email,
              'p_ar_rep_name':p_ar_rep_name,
              'p_ar_rep_mobile':p_ar_rep_mobile,
              't_c_flag':t_c_flag,
              'parking_type':parking_type
              // 'prk_admin_id':prk_admin_id
            },
            dataType:'html',
            success  :function(data){
              // alert(data);
               //window.location='vender_address.php';
              var json = $.parseJSON(data);
              //alert(json);
              if (json.status){
                window.location='signup-thanks';
              }else{
                alert("somthing went wrong");
              }
            }
          });
        }//if valid end
      });
      // Datepicker
      $('.fc-datepicker').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true
      });
    });
    $.validator.addMethod("valueNotEqualsCity", function(value){
      arg = "";
      if (value != "") {
        //return true;
        $("#error_parking_type").hide();
        return arg !== value;
      }else{
        $("#error_parking_type").show();
        return false;
      }
    });
    /*uppercase*/
    function nameUppercase() {
      var x = document.getElementById("name");
      x.value = x.value.toUpperCase();
    }
    function emailLowercase(){
      var x = document.getElementById("email");
      x.value = x.value.toLowerCase();
      //alert(x.value.toLowerCase());
    }
    $("#name").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 60) && (inputValue != 0 & inputValue !=32)){
        event.preventDefault();
      }
    });
    $(function() {

      $('#onlyNum').on('keydown', '#mobile', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
    })
  </script>
</html>
