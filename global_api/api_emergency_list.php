<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function emergency_list($prk_admin_id,$user_admin_id,$prk_area_user_id,$oth1,$oth2){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
    $sql = "SELECT adm_mer_dtl_id,
        prk_admin_id,
        adm_mer_dtl_name,
        adm_mer_dtl_number,
        adm_mer_dtl_remarks,
        inserted_status
        FROM admin_mer_dtl 
        WHERE active_flag='".FLAG_Y."' 
        AND del_flag='".FLAG_N."' 
        AND IF(mer_status='I',(
           IF(inserted_status='U',user_admin_id,prk_admin_id)
        ),mer_status) IN ('D','$prk_admin_id','$user_admin_id') 
        ORDER BY IF(form_order IS NULL OR form_order='','A',form_order) ASC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['adm_mer_dtl_id'] = $val['adm_mer_dtl_id'];
            $park['adm_mer_dtl_name'] = $val['adm_mer_dtl_name'];
            $park['adm_mer_dtl_number'] = $val['adm_mer_dtl_number'];
            $park['adm_mer_dtl_remarks'] = $val['adm_mer_dtl_remarks'];
            $park['inserted_status'] = $val['inserted_status'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['tower_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>