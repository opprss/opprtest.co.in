<?php 
/*
Description: email verifyed 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
require_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_admin_id','token'))){
 	if(isEmpty(array('user_admin_id','token'))){
	 	$user_admin_id = trim($_POST['user_admin_id']);
	 	$token = trim($_POST['token']);
	 	$response= user_token_check($user_admin_id,$token);
 	}else{
		$response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
 		$response= json_encode($response);
    }
}else{
	$response['status'] = 0; 
	$response['message'] = 'Invalid API Call';
	$response= json_encode($response); 
} 
echo ($response);
?>