<?php
/*
Description: Parking area login.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
if(isAvailable(array('prk_admin_id'))){
    if(isEmpty(array('prk_admin_id'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $response = cus_bill_generate_list($prk_admin_id);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandator';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>