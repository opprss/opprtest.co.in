<?php 
/*
Description: Parking area profile update.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
 require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','prk_area_name','prk_area_short_name','prk_area_email','prk_area_rep_name','prk_area_rep_mobile','prk_admin_name','prk_email_verify','prk_mobile_verify','token','sub_unit_flag','last_due_day','choose_due_day','prk_area_display_name'))){
 	if(isEmpty(array('prk_admin_id','prk_area_name','prk_area_short_name','prk_area_email','prk_area_rep_name','prk_area_rep_mobile','prk_admin_name','prk_email_verify','prk_mobile_verify','token','sub_unit_flag'))){

	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_area_name = trim($_POST['prk_area_name']);
	 	$prk_area_short_name = trim($_POST['prk_area_short_name']);
	 	$prk_area_email = trim($_POST['prk_area_email']);
	 	$prk_area_rep_name = trim($_POST['prk_area_rep_name']);
	 	$prk_area_rep_mobile = trim($_POST['prk_area_rep_mobile']);
	 	$prk_admin_name = trim($_POST['prk_admin_name']);
	 	$prk_email_verify = trim($_POST['prk_email_verify']);
	 	$prk_mobile_verify = trim($_POST['prk_mobile_verify']);
        $prk_area_pro_img = trim($_POST['prk_area_pro_img']);
	 	$sub_unit_flag = trim($_POST['sub_unit_flag']);
        $prk_area_display_name = trim($_POST['prk_area_display_name']);
	 	$token = trim($_POST['token']);
        $last_due_day = ($_POST['last_due_day']);
        $choose_due_day = ($_POST['choose_due_day']);

        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            if($sub_unit_flag=='Y'){
                $prk_sub_unit_address='Parking_admin';
                prk_sub_unit($prk_admin_id,$prk_area_name,$prk_area_short_name,$prk_sub_unit_address,$prk_area_rep_name,$prk_area_rep_mobile,$prk_area_email,TIME_TRN_WEB,FUTURE_DATE_WEB,$prk_admin_name);
            }
            $respon=prk_area_profile_update($prk_admin_id,$prk_area_name,$prk_area_short_name,$prk_area_email,$prk_area_rep_name,$prk_area_rep_mobile,$prk_admin_name,$prk_email_verify,$prk_mobile_verify,$prk_area_pro_img,$last_due_day,$choose_due_day,$prk_area_display_name);
    		echo ($respon);
        }else{
            $response = $resp;
            echo $response;
        }
        
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 	echo json_encode($response);
 }
?>