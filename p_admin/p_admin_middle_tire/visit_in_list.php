<?php 
/*
Description: Parking visiter in list
Developed by: Rakhal Raj Mandal
Created Date: 31-05-2018
Update date : 
*/ 
// require_once 'api/global_api.php';
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','start_date','end_date','token'))){
    if(isEmpty(array('prk_admin_id','token'))){            
        $prk_admin_id = trim($_POST['prk_admin_id']); 
        $start_date = trim($_POST['start_date']);
        $end_date = trim($_POST['end_date']);
         $token = ($_POST['token']);
        $visit_name = isset($_REQUEST['visit_name']) ? trim($_REQUEST['visit_name']) : ''; 
        $visit_mobile = isset($_REQUEST['visit_mobile']) ? trim($_REQUEST['visit_mobile']) : ''; 
        $tower_id = isset($_REQUEST['tower_id']) ? trim($_REQUEST['tower_id']) : ''; 
        $flat_id = isset($_REQUEST['flat_id']) ? trim($_REQUEST['flat_id']) : ''; 
         $resp=prk_token_check($prk_admin_id,$token);
         $json = json_decode($resp);
         if($json->status){
        $response = visit_in_list($prk_admin_id,$start_date,$end_date,$visit_name,$visit_mobile,$tower_id,$flat_id);
        echo $response;
    }
        else{
            echo $response=$resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        // $response = json_encode($response); 
      echo json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    // $response = json_encode($response); 
    echo json_encode($response); 
} 
// echo $response;
?> 