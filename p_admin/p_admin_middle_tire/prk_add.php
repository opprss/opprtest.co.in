<?php
/*
Description: Parking adimn Address save.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
// require_once 'api/global_api.php';
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_address','prk_land_mark','prk_add_area','prk_add_city','prk_add_state','prk_add_country','prk_add_pin','prk_admin_id','inserted_by','token'))){
    if(isEmpty(array('prk_address','prk_add_area','prk_add_city','prk_add_state','prk_add_country','prk_add_pin','prk_admin_id','inserted_by','token'))){
        $prk_address = trim($_POST['prk_address']);
        $prk_land_mark = trim($_POST['prk_land_mark']);
        $prk_add_area = trim($_POST['prk_add_area']);
        $prk_add_city = trim($_POST['prk_add_city']);
        $prk_add_state = trim($_POST['prk_add_state']);
        $prk_add_country = trim($_POST['prk_add_country']);
        $prk_add_pin = trim($_POST['prk_add_pin']);
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $inserted_by = trim($_POST['inserted_by']);
        $token = trim($_POST['token']);
        
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            $respon = prk_address($prk_address,$prk_land_mark,$prk_add_area,$prk_add_city,$prk_add_state,$prk_add_country,$prk_add_pin,$prk_admin_id,$inserted_by);
            echo ($respon);
        }else{
            $response = $resp;
            echo $response;
        }        
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        echo json_encode($response);
    }
}else{
     $response['status'] = 0; 
     $response['message'] = 'Invalid API Callll';
     echo json_encode($response);
 }
?>