<?php
/*
Description: Gender checker. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function nameChecker($name){
    $response = array();
    if (preg_match("/^[A-Z ]*$/",$name)) {
        return true;
    }
    else
    {
        $response['status'] = 0;
        $response['message'] = 'Only Capital letters and white space allowed.';
        return json_encode($response);
        //return false;
    }
}
?>