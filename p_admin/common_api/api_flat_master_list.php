<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function flat_master_list($prk_admin_id,$tower_id,$flat_id){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
    $sql="SELECT `flat_details`.`flat_id`,
    `flat_details`.`flat_name`,
    `flat_details`.`m_account_number`,
    `flat_details`.`tower_id`,
    `tower_details`.`tower_name`,
    `flat_details`.`prk_admin_id`,
    `flat_details`.`eff_date`,
    `flat_details`.`end_date`,
    `flat_master`.`flat_master_id`,
    `flat_master`.`owner_id`,
    if(`owner_details`.`owner_name` IS NULL or `owner_details`.`owner_name`='','NOT ASSIGNED',`owner_details`.`owner_name`) as 'owner_name',
    `owner_details`.`owner_email`,
    `flat_master`.`tenant_id`,
    if(`tenant_details`.`tenant_name` IS NULL or `tenant_details`.`tenant_name`='','NOT ASSIGNED',`tenant_details`.`tenant_name`) as 'tenant_name',
    `tenant_details`.`tenant_email`,
    `resident_member`.`res_mem_id`,
    `resident_member`.`res_mem_name`,
    `resident_member`.`res_mem_email`,
    `flat_master`.`living_status`,
    if(`all_drop_down`.`full_name` IS NULL OR `all_drop_down`.`full_name`='','VACANT',`all_drop_down`.`full_name`) as 'living_status_full'
    FROM `flat_details` JOIN `tower_details` ON `tower_details`.`tower_id`=`flat_details`.`tower_id`
    AND `flat_details`.`active_flag`='".FLAG_Y."' 
    AND `flat_details`.`del_flag`='".FLAG_N."' 
    AND `tower_details`.`active_flag`='".FLAG_Y."' 
    AND `tower_details`.`del_flag`='".FLAG_N."'
    left JOIN `flat_master` ON `flat_master`.`prk_admin_id`=`flat_details`.`prk_admin_id`
    AND `flat_master`.`flat_id`=`flat_details`.`flat_id`
    AND `flat_master`.`tower_id`=`flat_details`.`tower_id`
    AND '".TIME_TRN."' BETWEEN `flat_master`.`effective_date` AND `flat_master`.`end_date`
    AND `flat_master`.`active_flag`='".FLAG_Y."'
    AND `flat_master`.`del_flag`='".FLAG_N."'
    LEFT JOIN `owner_details` ON `owner_details`.`owner_id`=`flat_master`.`owner_id` 
    AND `owner_details`.`active_flag`='".FLAG_Y."'
    AND `owner_details`.`del_flag`='".FLAG_N."'
    AND `owner_details`.`prk_admin_id`=`flat_details`.`prk_admin_id`
    LEFT JOIN `tenant_details` ON `tenant_details`.`tenant_id`=`flat_master`.`tenant_id` 
    AND `tenant_details`.`active_flag`='".FLAG_Y."'
    AND `tenant_details`.`del_flag`='".FLAG_N."'
    AND `tenant_details`.`prk_admin_id`=`flat_details`.`prk_admin_id`
    LEFT JOIN `all_drop_down` ON `all_drop_down`.`category`='APT_FLT_HLD'
    AND `all_drop_down`.`sort_name`=`flat_master`.`living_status`
    AND `all_drop_down`.`del_flag`='".FLAG_N."'
    LEFT JOIN `resident_member` ON `resident_member`.`res_mem_id`=`flat_master`.`res_mem_id`
    AND `resident_member`.`active_flag`='".FLAG_Y."'
    AND `resident_member`.`del_flag`='".FLAG_N."'
    WHERE `flat_details`.`prk_admin_id`='$prk_admin_id' 
    AND IF('$tower_id' IS NULL OR '$tower_id'='',`tower_details`.`tower_id`,'$tower_id')=`tower_details`.`tower_id`
    AND IF('$flat_id' IS NULL OR '$flat_id'='',`flat_details`.`flat_id`,'$flat_id')=`flat_details`.`flat_id`
    ORDER BY `tower_details`.`tower_unique_id`,`flat_details`.`flat_unique_id`ASC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['flat_id'] = $val['flat_id'];
            $park['flat_name'] = $val['flat_name'];
            $park['tower_id'] = $val['tower_id'];
            $park['tower_name'] = $val['tower_name'];
            $park['prk_admin_id'] = $val['prk_admin_id'];
            $park['eff_date'] = $val['eff_date'];
            $park['end_date'] = $val['end_date'];
            $park['flat_master_id'] = $val['flat_master_id'];
            $park['owner_id'] = $val['owner_id'];
            $park['owner_name'] = $val['owner_name'];
            $park['owner_email'] = $val['owner_email'];
            $park['tenant_id'] = $val['tenant_id'];
            $park['tenant_name'] = $val['tenant_name'];
            $park['tenant_email'] = $val['tenant_email'];
            $park['res_mem_id'] = $val['res_mem_id'];
            $park['res_mem_name'] = $val['res_mem_name'];
            $park['res_mem_email'] = $val['res_mem_email'];
            $park['living_status'] = empty($val['living_status'])?'V':$val['living_status'];
            $park['living_status_full'] = $val['living_status_full'];
            $park['m_account_number'] = $val['m_account_number'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['flat_master_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>