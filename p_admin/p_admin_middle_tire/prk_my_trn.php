<?php 
/*
Description: Parking area all payment history.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','token','start_date','end_date','vehicle_number','vehicle_type','payment_type'))){
    if(isEmpty(array('prk_admin_id','token'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);
        $token = trim($_POST['token']);
        $start_date = trim($_POST['start_date']);
        $end_date = trim($_POST['end_date']);
        $vehicle_number = trim($_POST['vehicle_number']);
        $vehicle_type = trim($_POST['vehicle_type']);
        $payment_type = trim($_POST['payment_type']);
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            $response= prk_my_trn($prk_admin_id,$start_date,$end_date,$vehicle_number,$vehicle_type,$payment_type);
        }else{
            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response); 
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response); 
}
echo ($response);
?>