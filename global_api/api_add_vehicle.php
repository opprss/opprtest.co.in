<?php
/*
Description: Add vehicle.
Developed by: Rakhal Raj Mandal
Created Date: 15-01-2018
Update date : 21-05-2018
*/
//@include_once 'global_api/global_api.php';
// add_vehicle($user_admin_id,$user_veh_type,$user_veh_number,$user_veh_reg,$user_veh_reg_exp_dt,$user_ins_number,$user_ins_exp_dt,$user_emu_number,$user_emu_exp_dt,$mobile)

function add_vehicle($user_admin_id,$user_veh_type,$user_veh_number,$user_veh_reg,$user_veh_reg_exp_dt,$user_ins_number,$user_ins_exp_dt,$user_emu_number,$user_emu_exp_dt,$mobile,$rc_img,$ins_img,$emu_img,$oth_doc_no1,$oth_doc_no1_exp_dt,$oth_doc_no1_img,$oth_doc_no2,$oth_doc_no2_exp_dt,$oth_doc_no2_img,$web_rc_img,$web_ins_img,$web_emu_img,$web_oth_doc_no1_img,$web_oth_doc_no2_img){
    global $pdoconn;
    $response = array();
    $add_veh_co= add_vehicle_count($user_admin_id,$user_veh_number); 
    $json = json_decode($add_veh_co);

    // echo ($json);
    if($json->status=='1'){
        $v_numberCh =v_numberChecker($user_veh_number);
        $json = json_decode($v_numberCh);
        if($json->status=='1'){
            // $rc_img =veh_img_upload_all($mobile,$rc_img);
            // $ins_img =veh_img_upload_all($mobile,$ins_img);
            // $emu_img =veh_img_upload_all($mobile,$emu_img);
            // $oth_doc_no1_img =veh_img_upload_all($mobile,$oth_doc_no1_img);
            // $oth_doc_no2_img =veh_img_upload_all($mobile,$oth_doc_no2_img);
            if (!empty($rc_img)) {
                # code...
                $rc_img =veh_img_upload_all($mobile,$rc_img);
            }else{

                $rc_img =web_veh_img_upload_all($mobile,$web_rc_img);
            }
            if (!empty($ins_img)) {
                # code...
                $ins_img =veh_img_upload_all($mobile,$ins_img);
            }else{

                $ins_img =web_veh_img_upload_all($mobile,$web_ins_img);
            }
            if (!empty($emu_img)) {
                # code...
                $emu_img =veh_img_upload_all($mobile,$emu_img);
            }else{

                $emu_img =web_veh_img_upload_all($mobile,$web_emu_img);
            }
            if (!empty($oth_doc_no1_img)) {
                # code...
                $oth_doc_no1_img =veh_img_upload_all($mobile,$oth_doc_no1_img);
            }else{

                $oth_doc_no1_img =web_veh_img_upload_all($mobile,$web_oth_doc_no1_img);
            }
            if (!empty($oth_doc_no2_img)) {
                # code...
                $oth_doc_no2_img =veh_img_upload_all($mobile,$oth_doc_no2_img);
            }else{

                $oth_doc_no2_img =web_veh_img_upload_all($mobile,$web_oth_doc_no2_img);
            }

            $qr_code=$mobile.'-'.$user_veh_type.'-'.$user_veh_number;
            $sql = "INSERT INTO `user_vehicle_detail`(`user_veh_type`,`user_veh_number`,`user_veh_reg`,`user_veh_reg_exp_dt`,`user_admin_id`,`veh_qr_code`,`inserted_by`,`inserted_date`, `user_ins_number`, `user_ins_exp_dt`, `user_emu_number`, `user_emu_exp_dt`, `rc_img`, `user_ins_img`, `user_emu_img`, `oth_doc_no1`, `oth_doc_no1_exp_dt`, `oth_doc_no1_img`, `oth_doc_no2`, `oth_doc_no2_exp_dt`, `oth_doc_no2_img`) VALUE ('$user_veh_type','$user_veh_number','$user_veh_reg','$user_veh_reg_exp_dt','$user_admin_id','$qr_code','$mobile','".TIME."','$user_ins_number','$user_ins_exp_dt','$user_emu_number','$user_emu_exp_dt','$rc_img','$ins_img','$emu_img','$oth_doc_no1','$oth_doc_no1_exp_dt','$oth_doc_no1_img','$oth_doc_no2','$oth_doc_no2_exp_dt','$oth_doc_no2_img')";
            $query = $pdoconn->prepare($sql);
            if($query->execute()){
                $user_veh_dtl_id = $pdoconn->lastInsertId();
                $response['status'] = 1;
                $response['user_veh_dtl_id'] = $user_veh_dtl_id;
                $response['message'] = 'Add Vehicle Successful';
                $response = json_encode($response);
            }else{
                $response['status'] = 0;
                $response['message'] = 'Add Vehicle Not Successful';
                $response = json_encode($response); 
            }
        }else{
            $response = $v_numberCh;
        }
    }else{
        $response = $add_veh_co;
    }
    return ($response);
}
function veh_img_upload_all($mobile,$img){
    if (!empty($img)) {
        $PNG_TEMP_DIR ='./../uploades/'.$mobile.'/vehicle/';
        if (!file_exists($PNG_TEMP_DIR)) {
            $old_mask = umask(0);
            mkdir($PNG_TEMP_DIR, 0777, TRUE);
            umask($old_mask);
        }
        /*$name= uniqid().'.png'; 
        $img_part = explode(";base64,",$img);
        $img_type_aux = explode("image/",$img_part[0]);
        $ext = $img_type_aux[1];
        $image_base64 = base64_decode($img_part[1]);
        $file = $PNG_TEMP_DIR .$name;
        file_put_contents($file,$image_base64); 
        $file='uploades/'.$mobile.'/vehicle/'.$name;*/

        $decode_file = base64_decode($img);
        $file= uniqid().'.png';
        $file_dir=$PNG_TEMP_DIR.$file;
        if(file_put_contents($file_dir, $decode_file)){
            // api_image_upload_compress($file_dir);
            $file='uploades/'.$mobile.'/vehicle/'.$file;
        }else{
            $file='';
        }
    }else{
        $file='';
    }
    return $file;
}
function web_veh_img_upload_all($mobile,$img){
    // if (!empty($visitor_img)) {      
    if (!empty($img)) {
        $PNG_TEMP_DIR ='./../uploades/'.$mobile.'/vehicle/';
        if (!file_exists($PNG_TEMP_DIR)) {
            $old_mask = umask(0);
            mkdir($PNG_TEMP_DIR, 0777, TRUE);
            umask($old_mask);
        }
        $image_parts = explode(";base64,", $img);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
      
        $image_base64 = base64_decode($image_parts[1]);
        $fileName = uniqid() . '.png';
        $file = $PNG_TEMP_DIR . $fileName;
        file_put_contents($file, $image_base64);
        $file='uploades/'.$mobile.'/vehicle/'.$fileName;
        // $file = "uploades/".$inserted_by."/visitor_pro_img/".$fileName;
        // $visitor_img1 = $PNG_TEMP_DIR."".$time."_".$_FILES['visitor_img']['name'];
        // @copy($_FILES['visitor_img']['tmp_name'],$visitor_img1);
    }else{

        $file = '';
    }
    return $file;
}
?>