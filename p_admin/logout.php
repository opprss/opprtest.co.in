<?php
session_start();
include_once '../global_asset/config.php';
include_once 'p_admin_middle_tire/api/parkAreaReg_api.php';
if(isset($_SESSION['prk_admin_id']) && isset($_SESSION['token'])){
	//header('location:../signin.php');
	$response = prk_area_logout($_SESSION['token']);
	$json = json_decode($response);

	if ($json->status) {
		session_destroy();
		header('location: ../signin');
	}else{
		header('location: dashboard');
	}
}else{
	header('location: ../signin');
}

?>