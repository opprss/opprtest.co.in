<?php 
/*
Description: parking area rate delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','prk_veh_trc_dtl_id'))){
 	if(isEmpty(array('prk_admin_id','prk_veh_trc_dtl_id'))){ 
	 	$prk_admin_id = $_POST['prk_admin_id'];
	 	$prk_veh_trc_dtl_id = trim($_POST['prk_veh_trc_dtl_id']);

		 $response = veh_out_verify($prk_admin_id,$prk_veh_trc_dtl_id);
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 	$response = json_encode($response);
}
echo $response;
?>