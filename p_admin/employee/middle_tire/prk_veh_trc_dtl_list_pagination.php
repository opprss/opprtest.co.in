<?php 
/*
Description: parking area in all vehicle list
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
// require_once 'api/global_api.php';
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','prk_area_gate_id','token'))){
    if(isEmpty(array('prk_admin_id','prk_area_user_id','prk_area_gate_id','token'))){            
        $prk_admin_id = trim($_POST['prk_admin_id']);            
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $prk_area_gate_id = trim($_POST['prk_area_gate_id']);
        $count = trim($_REQUEST['count']);
        if (!isset($_REQUEST['page'])) {
          $page = 1;
        }else{
          $page = trim($_REQUEST['page']);
        }
        $token = ($_POST['token']);
        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            $this_page_first_result = ($page-1)*$count;

            $response = array();
            $allplayerdata = array();
            $park = array();
            $sql = "SELECT  `payment_receive`.`payment_receive_id`,
                `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id`,
                `payment_receive`.`in_out_flag`,
                `prk_veh_trc_dtl`.`prk_veh_type`,
                `prk_veh_trc_dtl`.`veh_number`,
                `prk_veh_trc_dtl`.`veh_token`,
                `prk_veh_trc_dtl`.`veh_out_verify_flag`,
                DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%d-%m-%Y') AS `prk_veh_in_date`,
                DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%I:%i %p') AS `prk_veh_in_time`,
                DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%d-%m-%Y') AS `prk_veh_out_date`,
                DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%I:%i %p') AS `prk_veh_out_time`,
                `vehicle_type`.`vehicle_typ_img`
                FROM `prk_veh_trc_dtl`join `vehicle_type` 
                on `prk_veh_trc_dtl`.`prk_veh_type`=`vehicle_type`.`vehicle_sort_nm`
                left join `payment_receive` 
                on `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id` =  `payment_receive`.`prk_veh_trc_dtl_id`
                where `prk_veh_trc_dtl`.`prk_admin_id`= '$prk_admin_id'
                AND `prk_veh_trc_dtl`.`prk_veh_out_time` IS NULL
                ORDER BY `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id` ASC";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $to_count=$query->rowCount();
            $sql = "SELECT  `payment_receive`.`payment_receive_id`,
                `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id`,
                `payment_receive`.`in_out_flag`,
                `prk_veh_trc_dtl`.`prk_veh_type`,
                `prk_veh_trc_dtl`.`veh_number`,
                `prk_veh_trc_dtl`.`veh_token`,
                `prk_veh_trc_dtl`.`veh_out_verify_flag`,
                DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%d-%m-%Y') AS `prk_veh_in_date`,
                DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_in_time`,'%I:%i %p') AS `prk_veh_in_time`,
                DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%d-%m-%Y') AS `prk_veh_out_date`,
                DATE_FORMAT(`prk_veh_trc_dtl`.`prk_veh_out_time`,'%I:%i %p') AS `prk_veh_out_time`,
                `vehicle_type`.`vehicle_typ_img`
                FROM `prk_veh_trc_dtl`join `vehicle_type` 
                on `prk_veh_trc_dtl`.`prk_veh_type`=`vehicle_type`.`vehicle_sort_nm`
                left join `payment_receive` 
                on `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id` =  `payment_receive`.`prk_veh_trc_dtl_id`
                where `prk_veh_trc_dtl`.`prk_admin_id`= '$prk_admin_id'
                AND `prk_veh_trc_dtl`.`prk_veh_out_time` IS NULL
                ORDER BY `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id` ASC
                LIMIT $this_page_first_result,$count";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $arr_catagory = $query->fetchAll();
                foreach($arr_catagory as $val) 
                {
                    $park['payment_receive_id'] = $val['payment_receive_id'];
                    $park['in_out_flag'] = $val['in_out_flag'];
                    $park['prk_veh_trc_dtl_id'] = $val['prk_veh_trc_dtl_id'];
                    $park['prk_veh_type'] = $val['prk_veh_type'];
                    $park['veh_number'] = $val['veh_number'];
                    $park['prk_veh_in_date'] = $val['prk_veh_in_date'];
                    $park['prk_veh_in_time'] = $val['prk_veh_in_time'];
                    $park['prk_veh_out_date'] = $val['prk_veh_out_date'];
                    $park['prk_veh_out_time'] = $val['prk_veh_out_time'];
                    $park['vehicle_typ_img'] = $val['vehicle_typ_img'];
                    $park['veh_token'] = $val['veh_token'];
                    $park['veh_out_verify_flag'] = $val['veh_out_verify_flag'];
                    array_push($allplayerdata, $park);
                }
                $response['status'] = 1;
                $response['message'] = 'data Successful';
                $response['total_count'] = $to_count;
                $response['count'] = $count;
                $response['parking'] = $allplayerdata;
            }else{
                $response['status'] = 0;
                $response['message'] = 'List Empty';
            }
        }else{
            $response['status'] = 0; 
            $response['session'] = 0;
            $response['message'] = 'Session Expired Please Login Again';
            // echo json_encode($response);
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        // echo json_encode($response); 
     }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
}
echo json_encode($response); 
?>