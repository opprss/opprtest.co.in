<?php
/*
Description: user address insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_family_group($user_admin_id,$mobile,$user_add_id,$prk_admin_id,$user_family_name,$user_family_gender,$user_family_relation,$user_family_mobile,$intercom_per,$user_family_img,$call_type){
    $response = array();
    global $pdoconn;

    if(!empty($user_family_img)){
        $PNG_TEMP_DIR ='./../uploades/'.$mobile.'/family_img/';
        // include "phpqrcode/qrlib.php";
        if (!file_exists($PNG_TEMP_DIR)) {
            $old_mask = umask(0);
            mkdir($PNG_TEMP_DIR, 0777, TRUE);
            umask($old_mask);
        }
        if ($call_type=='M') {
            $decode_file = base64_decode($user_family_img);
            $file= uniqid().'.jpg';
            $file_dir=$PNG_TEMP_DIR.$file;
            if(file_put_contents($file_dir, $decode_file)){
                api_image_upload_compress($file_dir);
                $user_family_img='uploades/'.$mobile.'/family_img/'.$file;
            }else{
                $user_family_img = NULL;
            }
        }else if ($call_type=='W') {
            $image_parts = explode(";base64,", $user_family_img);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
          
            $image_base64 = base64_decode($image_parts[1]);
            $fileName = uniqid() . '.png';
            $file = $PNG_TEMP_DIR . $fileName;
            if(file_put_contents($file, $image_base64)){
                # code...
                $user_family_img='uploades/'.$mobile.'/family_img/'.$fileName;
            }else{
                # code...
                $user_family_img = '';
            }
        }else{
            # code...
            $user_family_img = '';
        }
    }else{
        # code...
        $user_family_img = '';
    }
    $user_family_id=uniqid();
    $sql="INSERT INTO `user_family_group`(`user_family_id`, `user_admin_id`, `user_add_id`, `prk_admin_id`, `user_family_name`, `user_family_gender`, `user_family_relation`, `user_family_mobile`, `inserted_by`, `inserted_date`, `intercom_per`, `user_family_img`) VALUES ('$user_family_id','$user_admin_id','$user_add_id','$prk_admin_id','$user_family_name','$user_family_gender','$user_family_relation','$user_family_mobile','$mobile','".TIME."','$intercom_per','$user_family_img')";
        $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response = json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
        $response = json_encode($response);
    }
    return ($response); 
}
?>