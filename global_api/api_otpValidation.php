<?php
/*
Description: OTP Validate. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function otpValidation($mobile,$otpo){
    $response = array();
    global $pdoconn;
    $active_flag_first = ACTIVE_FLAG_Y;
    $active_flag_second = ACTIVE_FLAG_N;
    $sql ="SELECT `otp`, `otp_start_time` FROM `otp_details` WHERE (`mobile_no`='$mobile' OR `admin_email`='$mobile' ) AND `active_flag`='$active_flag_first'";
    $query  = $pdoconn->prepare($sql);
    $query->execute(array('user_mobile'=>$mobile));
    $count=$query->rowCount();
    if($count>0){
        $row = $query->fetch();
        $otp =  $row['otp'];
        $otp_start_time =  $row['otp_start_time'];
        if($otp==$otpo){
            $sql = "UPDATE `otp_details` SET `active_flag` = '$active_flag_second' WHERE (`mobile_no`='$mobile' OR `admin_email`='$mobile' ) AND `active_flag`='$active_flag_first'";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){
                $db = TIME;
                $to_time = strtotime($otp_start_time);
                $from_time = strtotime($db);
                $exp_time =round(abs($to_time - $from_time) / 60,2). " minute";
                if($exp_time<=OTP_EXP_TIME){
                    $response['status']=1;
                    $response['message'] = 'OTP Valid';
                    return json_encode($response);
                }else{
                    $response['status']=0;
                    $response['message'] = 'OTP Expired';
                    return json_encode($response);
                }
            }
        }else{
            $response['status']=0;
            $response['message'] = 'OTP Not Valid';
            return json_encode($response);
        }
    }else{
            $response['status']=0;
            $response['message'] = 'OTP Not Valid';
            return json_encode($response);
    }
}
?>