<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function kids_out($prk_admin_id,$prk_user_username,$status,$kids_allow_id,$prk_area_gate_id){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;

    $sql="SELECT ka.`user_admin_id`,
        ka.`alone_flag`,
        ka.`rep_name`,
        ufg.`user_family_name`,
        ufg.`user_family_gender`,
        ufg.`user_family_relation`
        FROM `kids_allow` ka, `user_family_group` ufg 
        WHERE ufg.`user_family_id`= ka.`user_family_id`
        AND ufg.`active_flag`='".FLAG_Y."'
        AND ufg.`del_flag`='".FLAG_N."'
        AND ka.`kids_allow_id`='$kids_allow_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $user_admin_id =  $val['user_admin_id'];
    $alone_flag =  $val['alone_flag'];
    $rep_name =  $val['rep_name'];
    $user_family_name =  $val['user_family_name'];
    $rep_name_send = ($alone_flag==FLAG_N)?' WITH '.$rep_name:'';

    $sql="UPDATE `kids_allow` SET `status`='$status' ,`out_rep_name_by`='$prk_user_username' ,`out_date`='".TIME."', `out_prk_area_gate_id`='$prk_area_gate_id', `updated_by`='$prk_user_username', `updated_date`='".TIME."' WHERE `kids_allow_id`='$kids_allow_id'";
    $query  = $pdoconn->prepare($sql);
    if($query->execute()){

        $sql="SELECT `token`.`device_token_id`,`token`.`device_type`,`token`.`token_value` FROM `token` WHERE `token`.`active_flag`='".FLAG_Y."' AND `token`.`user_admin_id`='$user_admin_id'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $count=$query->rowCount();
        if($count>0){
            $val = $query->fetch();
            $device_token_id =  $val['device_token_id'];
            $device_type =  $val['device_type'];
            if ($device_type=='ANDR') {
                $payload = array();
                // $payload['area_name'] = $prk_area_name;
                // $payload['name'] = $user_family_name;
                $payload['in_date'] = TIME;
                $payload['url'] = 'KID';
                $title = 'OPPRSS';
                $message = $user_family_name.' ALLOWED TO GO OUT'.$rep_name_send.'.';
                $push_type = 'individual';
                $include_image='';
                $clickaction='KID';
                $oth1='';
                $oth2='';
                $oth3='';
                $paylo = push_send($title,$message,$push_type,$include_image,$device_token_id,$payload,$clickaction,$oth1,$oth2,$oth3);
            }
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>