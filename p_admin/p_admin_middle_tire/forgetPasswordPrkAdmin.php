<?php 
/*
Description: Parking adimn forget password.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// include_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('user_name','email','password','repassword'))){
 	if(isEmpty(array('user_name','email','password','repassword'))){

	 	$user_name = trim($_POST['user_name']);
	 	$email = trim($_POST['email']);
	 	$password = trim($_POST['password']);
        $repassword = trim($_POST['repassword']);
        
        $response=forgetPasswordPrkAdmin($user_name,$email,$password,$repassword);
        echo ($response);
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response);
 }
?>