<!-- ################################################
  
Description: Tower detalis.
Developed by: Rakhal Raj 
Created Date: 12-03-2018

 ####################################################-->
<?php 
  include "all_nav/header.php";
  $previous_url =$_SERVER['HTTP_REFERER'];
  $cus_bill_dtl_id = isset($_REQUEST['list_id']) ? trim($_REQUEST['list_id']) : ''; 
  $cus_bill_show = cus_bill_generate_show($cus_bill_dtl_id);
  $cus_show = json_decode($cus_bill_show);
  $prk_admin_id=$cus_show->prk_admin_id;
  $invoice_number=$cus_show->invoice_number;
  $billing_month=$cus_show->billing_month;
  $date_of_invoice=$cus_show->date_of_invoice;
  $bill_due_date=$cus_show->bill_due_date;
  $bill_period_start_date=$cus_show->bill_period_start_date;
  $bill_period_end_date=$cus_show->bill_period_end_date;
  $bill_amount=$cus_show->bill_amount;
  $discount_amount=$cus_show->discount_amount;
  $tax_value=$cus_show->tax_value;
  $cgst_amount=$cus_show->cgst_amount;
  $sgst_amount=$cus_show->sgst_amount;
  $igst_amount=$cus_show->igst_amount;
  $round_of_amount=$cus_show->round_of_amount;
  $total_invoice_value=$cus_show->total_invoice_value;
  $date_of_invoice=$cus_show->date_of_invoice;

  $prk_inf = prk_inf($prk_admin_id);
  $prk_inf = json_decode($prk_inf, true);
  $prk_area_name = $prk_inf['prk_area_name'];
?>
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Bill Payment</h5>
  </div>
  <!-- 'cus_bill_dtl_id','payment_type','payment_receive_amount','payment_receive_by','inserted_by','bank_name','bank_branch_name','beneficiary_name','cheque_number','online_payment_dtl_id' -->
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40" >
      <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
        <form id="admin_customer" method="POST" action="pgRedirect">
          <input type="hidden" name="tower_na" id="tower_na">
          <input type="hidden" name="online_payment_dtl_id" id="online_payment_dtl_id">
          <input type="hidden" name="cus_bill_dtl_id" id="cus_bill_dtl_id" value="<?=$cus_bill_dtl_id?>">
          <input type="hidden" name="prk_admin_id" id="prk_admin_id" value="<?=$prk_admin_id?>">
          <input type="hidden" name="payment_type" id="payment_type" value="ONLINE">
          <input type="hidden" name="payment_receive_by" id="payment_receive_by" value="PRK_ADMIN">
          <input type="hidden" name="inserted_by" id="inserted_by" value="<?=$parking_admin_name?>">
          <input type="hidden" name="bank_name" id="bank_name" value="">
          <input type="hidden" name="bank_branch_name" id="bank_branch_name" value="">
          <input type="hidden" name="beneficiary_name" id="beneficiary_name" value="">
          <input type="hidden" name="cheque_number" id="cheque_number" value="">
          <input type="hidden" name="callback_url" id="callback_url" value="<?=PRK_BASE_URL.'pgResponse'?>">
          <div class="row mg-b-25">
            <!-- <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Premises Name<span class="tx-danger">*</span></label>
                <input type="text" class="form-control" readonly="" placeholder="Kind Alt Name" name="prk_area_name" id="prk_area_name" value="<?=$prk_area_name?>">
              </div>
            </div> -->
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Invoice Number<span class="tx-danger">*</span></label>
                <input type="text" name="invoice_number" id="invoice_number" placeholder="Invoice Number" class="form-control" autocomplete="off" readonly="" value="<?=$invoice_number?>">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Billing Month<span class="tx-danger">*</span></label>
                <input type="text" name="billing_month" id="billing_month" placeholder="Billing Month" class="form-control" autocomplete="off" readonly="" value="<?=$billing_month?>">
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Date Period<span class="tx-danger">*</span></label>
                <input type="text" name="date_period" id="date_period" placeholder="Date Period" class="form-control" autocomplete="off" readonly="" value="<?=$bill_period_start_date.' to '.$bill_period_end_date?>">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Total Invoice Value<span class="tx-danger">*</span></label>
                <input type="text" name="payment_receive_amount" id="payment_receive_amount" placeholder="Total Invoice Value" class="form-control" autocomplete="off" readonly="" value="<?=$total_invoice_value?>">
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-layout-footer mg-t-30">
                <div class="row">
                  <div class="col-lg-6">
                    <button type="button" class="btn btn-block btn-primary prk_button" name="pay_resiv" id="pay_resiv"><span id="refresh_button" style="display: none;"><i class="fa fa-refresh fa-spin"></i></span>Pay Now</button>
                  </div>
                  <div class="col-lg-6">
                    <a href="<?=$previous_url?>"><button class="btn btn-block btn-primary prk_button" type="button">BACK</button></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php include"all_nav/footer.php"; ?>
<script type="text/javascript">
  $('#pay_resiv').click(function(){
    $('#refresh_button').show();
    $('#admin_customer').submit();
  });
</script>
