<?php
/*
Description: user vehicle show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
require_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('prk_visit_dtl_id','user_mobile','user_admin_id','token'))){
 	if(isEmpty(array('prk_visit_dtl_id','user_mobile','user_admin_id','token'))){
        $prk_visit_dtl_id = trim($_POST['prk_visit_dtl_id']);
	 	$user_name = trim($_POST['user_mobile']);
        $user_admin_id = trim($_POST['user_admin_id']);
        $token = trim($_POST['token']);
		$verify_status = isset($_POST['verify_status']) ? $_POST['verify_status'] : FLAG_Y;
        $resp=user_token_check($user_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            //global api
            $response = visitor_visit_in_verify($prk_visit_dtl_id,$user_name,$verify_status);
        }else{
            $response = $resp;
        }
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
 }
 echo $response;
?>