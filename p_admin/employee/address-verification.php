<!-- ################################################
Description: User Address verify
Developed by: Rakhal Raj Mandal
Created Date: 27-08-2019
####################################################-->
<?php 
  include "all_nav/header.php";
  $respon1=prk_user_add_list($prk_admin_id);
  $respon = json_decode($respon1, true);
?>
<style type="text/css">
  .size{
    font-size: 11px;
    text-transform: uppercase;
  }
  .error_size{
    font-size: 11px;
    color: red;
  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
</style> 
<link href="../../lib/highlightjs/github.css" rel="stylesheet">
<link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">User Address Verify<?//=$respon1?></h5>
  </div>
</div>
<div class="am-mainpanel">
  <div class="am-pagebody" id="data">
    <div class="card pd-20 pd-sm-40" style="padding: 10px;">
      <div class="table-wrapper">
        <table id="datatable1" class="table display responsive nowrap" cellspacing="0" width="100%">
          <thead>
          <tr>
            <th class="wd-5p">Image</th>
            <th class="">Name</th>
            <th class="">Mobile</th>
            <th class="">Tower & Flat</th>
            <th class="wd-18p">Send Date</th>
            <th class="wd-18p" style="text-align: center;">Action</th>
          </tr>
          </thead>
          <?php if($respon['status']){
            foreach($respon['user_address'] as $value){
              $user_add_verify_color = ($value['user_add_verify_flag']==FLAG_Y)?'#def4f7':'';
              $user_add_verify_disabled = ($value['user_add_verify_flag']==FLAG_Y)?'disabled':'';
              $user_add_verify_valu = ($value['user_add_verify_flag']==FLAG_Y)?'Verified':'Verify';
          ?>
          <tr style="background-color: <?=$user_add_verify_color?>">
            <td><img src="<?php echo $value['user_img']; ?>" style="height: 40px; border-radius: 20%;"> </td>
            <td><?php echo $value['user_name']; ?></td>
            <td><?php echo $value['user_mobile']; ?></td>
            <td><?php echo $value['tower_name'].','.$value['falt_name']; ?></td>
            <td><?php echo $value['in_date']; ?></td>
            <td style="text-align: center;">
              <button style="color:white; background-color:green;width: 62px;" type="button" class="out btn" id="<?=$value['user_add_id']?>" onclick="user_add_verify(this.id)" data-toggle="tooltip" data-placement="top" <?=$user_add_verify_disabled?>><i class="fa fa-sign-out"></i><?=$user_add_verify_valu?></button>
              <button style="color:white; background-color:red;" type="button" class="out btn" id="<?php echo($value['user_add_id'])?>" onclick="user_add_delete(this.id)" data-toggle="tooltip" data-placement="top"><i class="fa fa-sign-out"></i> Reject</button>
            </td>
          </tr><?php }}?>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include"all_nav/footer.php"; ?>
<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
</script>
<script type="text/javascript">
  function user_add_verify(user_add_id){
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will verify the user address</p>",
      theme: 'modern',
      type: 'green',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Verify',
          btnClass: 'btn-green',
          keys: ['Y', 'shift'],
          action: function(){
            // alert(user_add_id);
            if (user_add_id != '') {
              var urlDtl = pkrEmpUrl+'prk_user_add_verify.php';
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :
                {
                  'prk_admin_id':prk_admin_id,
                  'prk_area_user_id':prk_area_user_id,
                  'prk_user_username':prk_user_username,
                  'token':token,
                  'user_add_id':user_add_id
                },
                dataType:'html',
                success  :function(data)
                {
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Address Verified Successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                          location.reload(true);
                          // visitor_list();
                        }
                      }
                    });
                  }else{
                    if (json.session=='0') {
                      window.location.replace("logout.php");
                    }
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
  function user_add_delete(user_add_id){
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>This will reject user's Address</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Reject',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            if (user_add_id != '') {
              var urlDtl = pkrEmpUrl+'prk_user_add_reject.php';
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :{
                  'prk_admin_id':prk_admin_id,
                  'prk_area_user_id':prk_area_user_id,
                  'prk_user_username':prk_user_username,
                  'token':token,
                  'user_add_id':user_add_id
                },
                dataType:'html',
                success  :function(data){
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Address rejected Successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                          location.reload(true);
                          // visitor_list();
                        }
                      }
                    });
                  }else{
                    if (json.session=='0') {
                      window.location.replace("logout.php");
                    }
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
</script>
