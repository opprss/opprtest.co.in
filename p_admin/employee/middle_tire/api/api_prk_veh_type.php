<?php
/*
Description: show the all vehicle type.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function prk_veh_type($prk_admin_id){
    $allplayerdata = array();
    global $pdoconn;
   // $sql = "SELECT vehicle_type_Dec,vehicle_typ_img,vehicle_Sort_nm FROM `vehicle_type` WHERE `active_flag`='".ACTIVE_FLAG_Y."'";
    $sql = "SELECT `prk_area_rate`.`prk_rate_id`,
        `vehicle_type`.`vehicle_sort_nm`,
        `vehicle_type`.`vehicle_type_dec` as vehicle_type
        FROM `prk_area_rate`,`vehicle_type`
        WHERE `prk_area_rate`.`prk_admin_id` = '$prk_admin_id'
        AND `prk_area_rate`.`active_flag` = '".FLAG_Y."'
        AND `prk_area_rate`.`del_flag`= '".FLAG_N."'
        AND `vehicle_type`.`active_flag` = '".FLAG_Y."'
        AND  '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(`prk_area_rate`.`prk_rate_eff_date`, '%d-%m-%Y'), '%Y-%m-%d')    
        AND  DATE_FORMAT(STR_TO_DATE(IFNULL(`prk_area_rate`.`prk_rate_end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')
        AND `vehicle_type`.`vehicle_sort_nm` = `prk_area_rate`.`prk_vehicle_type`
        order by 1,2";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val)
    {
        $response['status'] = 1;
        $response['message'] = 'Successfully';
        $vehicle['vehicle_type'] = $val['vehicle_type'];
        $vehicle['vehicle_Sort_nm'] = $val['vehicle_sort_nm'];
        array_push($allplayerdata, $vehicle);
        $response['vehicle'] = $allplayerdata;

    }
   // return ($response);
  return json_encode($response);
}
?>