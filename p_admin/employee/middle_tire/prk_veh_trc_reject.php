<?php 
/*
Description: vehicle Reject
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','user_prk_veh_varify_id','prk_area_user_id','token'))){
 	if(isEmpty(array('prk_admin_id','user_prk_veh_varify_id','prk_area_user_id','token'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);     
        $user_prk_veh_varify_id = trim($_POST['user_prk_veh_varify_id']);
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $token = trim($_POST['token']);
        $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status=='1'){
            global $pdoconn;
            $sql = "SELECT * FROM `user_prk_veh_verify` WHERE `user_prk_veh_varify_id`='$user_prk_veh_varify_id' AND `prk_admin_id`='$prk_admin_id' ";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $val = $query->fetch();
            $user_admin_id = $val['user_admin_id']; 
            $prk_veh_type = $val['user_veh_type'];
            $prk_veh_number = $val['user_veh_number'];
            $prk_user_name = $val['prk_user_name'];
            $prk_veh_remark='test writing';
            $respon=prk_veh_trc_reject($prk_admin_id,$user_admin_id,$prk_veh_type,$prk_veh_number,$prk_user_name,$prk_veh_remark);
            $json = json_decode($respon);
            $status=$json->status;
            $prk_veh_trc_dtl_id=$json->prk_veh_trc_dtl_id;
            if($status=='1')
            {
                $active_flag= ACTIVE_FLAG_N;
                $user_prk_veh_verify_status= USER_PRK_VEH_VERITY_STATUS_R;
                $user_prk_remark='Message Sucessfull';
                $response= user_prk_veh_verify_out($user_prk_veh_varify_id,$user_prk_veh_verify_status,$user_prk_remark,$prk_veh_trc_dtl_id,$active_flag);
                echo ($response);
            }
        }else{
            $response['status'] = 0; 
            $response['session'] = 0;
            $response['message'] = 'Session Expired Please Login Again';
            echo json_encode($response);
        }
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response);
}

?>