<?php
/*
Description: Parking visitor gate pass issue.
Developed by: Rakhal Raj Mandal
Created Date: 31-10-2018
Update date :31-10-2018
*/ 
    require_once 'api/parkAreaReg_api.php';
    $response = array();
 
    if(isAvailable(array('prk_admin_id','prk_area_user_id','prk_user_username','token','visitor_gate_pass_dtl_id','end_date'))){
        if(isEmpty(array('prk_admin_id','prk_area_user_id','prk_user_username','token','visitor_gate_pass_dtl_id'))){
            $prk_admin_id = trim($_POST['prk_admin_id']);
            $prk_area_user_id = trim($_POST['prk_area_user_id']);
            $prk_user_username = trim($_POST['prk_user_username']);
            $token = trim($_POST['token']);
            $visitor_gate_pass_dtl_id = trim($_POST['visitor_gate_pass_dtl_id']);
            $end_date = empty($_POST['end_date'])?FUTURE_DATE_WEB:trim($_POST['end_date']);
            $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
            $json = json_decode($resp);
            if($json->status=='1'){
                $sql1 = "UPDATE `visitor_gate_pass_dtl` SET `updated_by`= '$prk_user_username',`updated_date`= '".TIME."',`end_date`= '$end_date' WHERE `visitor_gate_pass_dtl_id`='$visitor_gate_pass_dtl_id'";
                $query = $pdoconn->prepare($sql1);
                if($query->execute()){
                    $response['status'] = 1;
                    $response['message'] = 'Updated Successful';
                    $response = json_encode($response);
                }else{
                    $response['status'] = 0;
                    $response['message'] = 'Updated Not Successful';
                    $response = json_encode($response);
                }  
            }else{

                $response = $resp;
            }
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
        }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        $response = json_encode($response);
    }
    echo $response;
?>