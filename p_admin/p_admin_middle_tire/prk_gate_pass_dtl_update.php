<?php 
/*
Description: Parking area gate pass dtl update.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('prk_gate_pass_id','prk_admin_id','user_admin_id','veh_type','veh_number','veh_owner_name','mobile','end_date','effective_date','user_name','prk_gate_pass_num','prk_sub_unit_id','token'))){
 	if(isEmpty(array('prk_gate_pass_id','prk_admin_id','user_admin_id','veh_type','veh_number','veh_owner_name','mobile','effective_date','user_name','prk_gate_pass_num','prk_sub_unit_id','token'))){

        $prk_gate_pass_id = trim($_POST['prk_gate_pass_id']);
	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$user_admin_id = trim($_POST['user_admin_id']);
	 	$veh_type = trim($_POST['veh_type']);
	 	$veh_number = trim($_POST['veh_number']);
        $prk_sub_unit_id= trim($_POST['prk_sub_unit_id']);
	 	$veh_owner_name = trim($_POST['veh_owner_name']);
        $mobile = trim($_POST['mobile']);
        $end_date = trim($_POST['end_date']);
        $effective_date = trim($_POST['effective_date']);
        $user_name = trim($_POST['user_name']);
        $prk_gate_pass_num = trim($_POST['prk_gate_pass_num']);
        $token = ($_POST['token']);

        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            $respon=prk_gate_pass_dtl_update($prk_gate_pass_id,$prk_admin_id,$user_admin_id,$veh_type,$veh_number,$prk_sub_unit_id,$mobile,$end_date,$veh_owner_name,$effective_date,$prk_gate_pass_num,$user_name);
            echo ($respon);
        }else{
            $response = $resp;
            echo $response;
        }
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?>