<?php
/*
Description: Parking area sub unit one vehicle type space SHOW.
Developed by: Rakhal Raj Mandal
Created Date: 05-05-2018
Update date : -------
*/ 
function prk_sub_unit_veh_space_show($prk_sub_unit_veh_space_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();

    $sql = "SELECT `prk_sub_unit_veh_space`.`prk_sub_unit_veh_space_id`,
    `prk_sub_unit_veh_space`.`prk_sub_unit_id`,
    `prk_sub_unit_veh_space`.`prk_admin_id`,
    `prk_sub_unit_veh_space`.`veh_type`,
    `prk_sub_unit_veh_space`.`veh_space`,
    `prk_sub_unit_veh_space`.`prk_sub_unit_veh_space_eff_date`,
    `prk_sub_unit_veh_space`.`prk_sub_unit_veh_space_end_date`,
    `prk_sub_unit`.`prk_sub_unit_short_name`,
    `prk_sub_unit`.`prk_sub_unit_name`,
    `vehicle_type`.`vehicle_type_dec`
    FROM `prk_sub_unit_veh_space` ,`prk_sub_unit`,`vehicle_type`
    WHERE `prk_sub_unit_veh_space`.`prk_sub_unit_veh_space_id`='$prk_sub_unit_veh_space_id' 
    AND `prk_sub_unit_veh_space`.`active_flag`='".FLAG_Y."' 
    AND `prk_sub_unit_veh_space`.`del_flag`='".FLAG_N."' 
    AND `prk_sub_unit_veh_space`.`veh_type`= `vehicle_type`.`vehicle_sort_nm`
    AND   `vehicle_type`.`active_flag` ='".FLAG_Y."'
    AND `prk_sub_unit`.`prk_sub_unit_id` = `prk_sub_unit_veh_space`.`prk_sub_unit_id`
    AND  '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(`prk_sub_unit_veh_space`.`prk_sub_unit_veh_space_eff_date`, '%d-%m-%Y'), '%Y-%m-%d') 
    AND DATE_FORMAT(STR_TO_DATE(IFNULL(`prk_sub_unit_veh_space`.`prk_sub_unit_veh_space_end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')";

    $query  = $pdoconn->prepare($sql);
    $query->execute();
   
    $val = $query->fetch();

        $response['status'] = 1;
        $response['message'] = 'data Successful';
        $response['prk_sub_unit_veh_space_id'] =  $val['prk_sub_unit_veh_space_id'];
        $response['prk_sub_unit_id'] =  $val['prk_sub_unit_id'];
        $response['prk_admin_id'] =  $val['prk_admin_id'];
        $response['veh_type'] =  $val['veh_type'];
        $response['veh_space'] =  $val['veh_space'];
        $response['prk_sub_unit_veh_space_eff_date'] =  $val['prk_sub_unit_veh_space_eff_date'];
        $response['prk_sub_unit_veh_space_end_date'] =  $val['prk_sub_unit_veh_space_end_date'];
        $response['prk_sub_unit_short_name'] =  $val['prk_sub_unit_short_name'];
        $response['prk_sub_unit_name'] =  $val['prk_sub_unit_name'];
        $response['vehicle_type_dec'] =  $val['vehicle_type_dec'];
array_push($allplayerdata, $response);
    return ($allplayerdata);
}
?>