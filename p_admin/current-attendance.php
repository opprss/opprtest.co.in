<!-- ################################################
  
Description: Tower detalis.
Developed by: Rakhal Raj 
Created Date: 12-03-2018

 ####################################################-->
<?php 
  include "all_nav/header.php";
  //$apartment_balance_sheet=balance_sheet($prk_admin_id);
  //$apartment_balance_sheet_list = json_decode($apartment_balance_sheet, true);
  if (isset($_SESSION['prk_admin_id'])) {
    $insert_by=$_SESSION['parking_admin_name'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
  }
  $prk_sub_user_list =prk_sub_user_list($prk_admin_id);

  $all_month1=all_month();
  $all_month = json_decode($all_month1, true);
  $all_year1=all_year();
  $all_year = json_decode($all_year1, true);
?>
<style type="text/css">
  .size{
    font-size: 11px;
  }
  .deactivate-bg{
    background-color: #ffe5e5 !important;
  }
  .error-text::-webkit-input-placeholder {
    color: red;
    border-color: red !important;
  }
  .error_size{
    font-size: 14px;
    color: red;
  }
  .error{
    font-size: 14px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
  th,td{
    text-align: center;
  }
</style>
<link href="../lib/highlightjs/github.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../lib/select2/css/select2.min.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">

<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Current Attendance Details<?//=$apartment_balance_sheet?></h5>
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40">
      <div class="table-wrapper">
        <table id="datatable1" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="wd-5p">Image</th>
              <th class="wd-5p">Month</th>
              <th class="wd-15p">Employee Name</th>
              <th class="wd-5p">In Date</th>
              <th class="wd-5p">In Time</th>
              <th class="wd-5p">Out Date</th>
              <th class="wd-5p">Out Time</th>
              <th class="wd-5p">Action</th>
            </tr>
          </thead>
          <tbody id="list_ve"></tbody>
        </table>
      </div>
    </div>
  </div>
<?php include"all_nav/footer.php"; ?>
<!-- scripts for datatable -->
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
<script src="../lib/datatables-responsive/jquery.dataTables.min.js"></script>
<script src="../lib/datatables-responsive/dataTables.buttons.min.js"></script>
<script src="../lib/datatables-responsive/jszip.min.js"></script>
<script src="../lib/datatables-responsive/pdfmake.min.js"></script>
<script src="../lib/datatables-responsive/vfs_fonts.js"></script>
<script src="../lib/datatables-responsive/buttons.html5.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    current_veh_loadAjax();
    // setInterval(function(){
    //     current_veh_loadAjax();// this will run after every 5 seconds 
    // }, 5000);
  });
  function datatable_show(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      language: {
      searchPlaceholder: 'Search Here',
      sSearch: '',
      lengthMenu: '_MENU_ Page',
      },
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      buttons: [
        {
          text: '<span id="refresh_button"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
          action: function ( e, dt, node, config ) {
              // alert( 'Button activated' );
              current_veh_loadAjax();
          }
        },
        { 
          extend: 'copyHtml5', 
          //footer: true,
          title: 'CURRENT ATTENDANCE DETAILS SHEET' 
        },
        { 
          extend: 'excelHtml5', 
          //footer: true,
          title: 'CURRENT ATTENDANCE DETAILS SHEET'  
        },
        {
            extend: 'pdfHtml5',
            //footer: true,
            title: 'CURRENT ATTENDANCE DETAILS SHEET',
            customize: function(doc) {
              doc.content[1].margin = [ 60, 0, 0, 0 ] //left, top, right, bottom
            },
            exportOptions: {
                columns: [0,1,2,3,4,5,6]
            }
        },
      ]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    $('#refresh_button').hide();
   }
</script>
<script type="text/javascript">
  function current_veh_loadAjax(){
    $('#refresh_button').show();
    var prkUrl = "<?php echo PRK_URL; ?>";
    var prk_admin_id = "<?php echo $prk_admin_id; ?>";
    var parking_admin_name = "<?php echo $insert_by;?>";
    var token = "<?php echo $token; ?>";
    var current_veh = prkUrl+'current_attendance_list.php';
    $.ajax({
      url :current_veh,
      type:'POST',
      data :{
        'prk_admin_id':prk_admin_id,
        'token':token
      },
      dataType:'html',
      success  :function(data){
        // alert(data);
        var demoLines = '';
        var json = $.parseJSON(data);
        if (json.status){
          var c = 0;
          for (var key in json.atten_list) {
            c +=1;
            var keyy = json.atten_list[key];
            demoLines += '<tr>\
              <td> <img src="'+keyy['prk_user_img']+'" style="height: 40px;"></td>\
              <td>'+keyy['this_month']+'</td>\
              <td>'+keyy['prk_user_name']+'</td>\
              <td>'+keyy['in_date']+'</td>\
              <td>'+keyy['in_time']+'</td>\
              <td>'+keyy['out_date']+'</td>\
              <td>'+keyy['out_time']+'</td>\
              <td>\
                <button style="color:white; background-color:green;" type="button" class="out btn prk_button" id="'+keyy['prk_user_username']+'" onclick="attendance_out(this.id)" data-toggle="tooltip" data-placement="top"><i class="fa fa-sign-out"></i> OUT</button>\
              </td>\
              </tr>';
          }
        }else{
          demoLines = '';
        }
        $('#datatable1').dataTable().fnDestroy();
        $("#list_ve").html(demoLines);
        datatable_show();
      }
    });
  }
</script>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var prk_admin_id = "<?php echo $prk_admin_id; ?>";
  var parking_admin_name = "<?php echo $insert_by;?>";
  var token = "<?php echo $token; ?>";
  function attendance_out(prk_user_username){
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will Out Visitor</p>",
      theme: 'modern',
      type: 'green',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Out',
          btnClass: 'btn-green',
          keys: ['Y', 'shift'],
          action: function(){
            //alert(prk_user_username);
            if (prk_user_username != '') {
              var urlDtl = prkUrl+'employee_attendance_out.php';
              //alert(urlDtl);
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :{
                  'prk_area_user_id':prk_user_username,
                  'prk_admin_id':prk_admin_id,
                  'parking_admin_name':parking_admin_name,
                  'token':token
                },
                dataType:'html',
                success  :function(data)
                {
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Attendance Out successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                           current_veh_loadAjax();
                        }
                      }
                    });
                  }else{
                    if (json.session=='0') {
                      window.location.replace("logout.php");
                    }
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>"+json.message+"</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
</script>
<script type="text/javascript">
  $( document ).ready( function () {
    $( "#attnd_list" ).validate( {
      rules: {
        prk_area_user: "required",
        month: "required",
        year: "required"
      },
      messages: {
      }
    });
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>



