<!-- ################################################
  
Description: Tower detalis.
Developed by: Rakhal Raj 
Created Date: 12-03-2018

 ####################################################-->
<?php include "all_nav/header.php";
  $board_members_id=base64_decode($_REQUEST['list_id']);
  $crud_type='S';
  $board_of_members=board_of_members($prk_admin_id,'','','','',$parking_admin_name,$crud_type,$board_members_id);
  $board_of_members_show = json_decode($board_of_members, true);

  $crud_type='LS';
  $designation = designation($prk_admin_id,'','','','',$parking_admin_name,$crud_type,'');
  $designation_list = json_decode($designation, true);
?>
<!-- header position -->
  <style type="text/css">
    .size{
      font-size: 11px;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .error{
      font-size: 11px;
      color: red;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .success{
      font-size: 11px;
      color: green;
    }
  </style>
  <!-- links for datatable -->
  <link href="../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
  
    <div class="am-mainpanel">

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Board Members Modify<//?=$board_of_members?></h5>
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <div class="card pd-10 pd-sm-40" >
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form id="park_flat">
              <div class="row mg-b-5">
                <div class="col-lg-5">
                  <div class="row">
                    <div class="col-lg-5">
                      <div class="form-group">
                        <label class="form-control-label size">Select Designation <span class="tx-danger">*</span></label>
                        <div class="select">
                          <select name="designation_id" id="designation_id" style="opacity: 0.8;font-size:14px">
                            <option value="">Select Type</option>
                            <?php if($designation_list['status']){
                              foreach($designation_list['designation'] as $value){?>
                              <option value="<?=$value['designation_id'];?>"><?=$value['desig_name'];?></option>
                            <?php }}?>
                          </select>
                        </div>
                        <span style="position: absolute; display: none;" id="error_designation_id" class="error">This field is required.</span>
                      </div>
                    </div>
                    <div class="col-lg-7">
                      <div class="form-group">
                        <label class="form-control-label size">Board Member Name <span class="tx-danger">*</span></label>
                        <input type="text" name="owner_name" id="owner_name" placeholder="Owner Name" onchange="search_func(this.value)" class="form-control">
                        <input type="hidden" id="owner_id" name="owner_id">
                        <span style="position: absolute; display: none;" id="error_owner_name" class="error">This field is required.</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-7">
                  <div class="row">
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label class="form-control-label size">EFFECTIVE DATE<span class="tx-danger">*</span></label>
                        <input type="text" name="effective_date" id="effective_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                        <span style="position: absolute; display: none;" id="error_effective_date" class="error">This field is required.</span>
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label class="form-control-label size">END DATE</label>
                        <input type="text" name="end_date" id="end_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                       <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-layout-footer mg-t-30">
                        <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-layout-footer mg-t-30">
                        <a href="board-of-members"><button class="btn btn-block btn-primary prk_button" type="button">BACK</button></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#end_date', {
      default_date   : false,
      position       : 'bottom',
      format  : 'Y-m-d',
      hide_on_select : true,
      render : function (date) {
        if (date < now) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        // $('#error_effective_date').text('');
        return {};
      } 
    });
  });
</script>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE ?>";
  var prk_admin_id="<?=$prk_admin_id?>"
  var parking_admin_name="<?=$parking_admin_name?>"
  var token="<?=$token?>"
  var board_members_id ="<?=$board_of_members_show['board_members_id']?>";
  $("#designation_id").val("<?=$board_of_members_show['designation_id']?>");
  $("#owner_name").val("<?=$board_of_members_show['owner_name']?>");
  $("#owner_id").val("<?=$board_of_members_show['owner_id']?>");
  $("#effective_date").val("<?=$board_of_members_show['effective_date']?>");
  if ("<?=$board_of_members_show['end_date']?>"==FUTURE_DATE) {
    var end_date_o='';
  }else{

    var end_date_o="<?=$board_of_members_show['end_date']?>";
  }
  $("#end_date").val(end_date_o);
  $('#designation_id').prop('disabled', true);
  $('#owner_name').prop('disabled', true);
  $('#effective_date').prop('disabled', true);

  var urlPrkAreGateDtl = prkUrl+'board_of_members.php';
  $('#save').click(function(){
    var effective_date=($("#effective_date").val());
    if($("#park_flat").valid()&effective_date!=''&owner_name()==true){
      var effective_date=($("#effective_date").val());
      var end_date=($("#end_date").val());
      // var str_array = end_date.split('-');
      // var dd = str_array[0];
      // var mm = str_array[1];
      // var yy = str_array[2];
      // var end_dat = yy+'-'+mm+'-'+dd;
      end_date = (!end_date == '') ? end_date : FUTURE_DATE;
      // var str_array = effective_date.split('-');
      // var dd = str_array[0];
      // var mm = str_array[1];
      // var yy = str_array[2];
      // var effective_dat = yy+'-'+mm+'-'+dd;
      var designation_id=($("#designation_id").val());
      var owner_id=($("#owner_id").val());
      $('#save').val('Wait ...').prop('disabled', true);
      $.ajax({
        url :urlPrkAreGateDtl,
        type:'POST',//'prk_admin_id','designation_id','owner_id','effective_date','end_date','parking_admin_name','token','crud_type','board_members_id'
        data :{
          'prk_admin_id':prk_admin_id,
          'designation_id':designation_id,
          'owner_id':owner_id,
          'effective_date':effective_date,
          'end_date':end_date,
          'parking_admin_name':parking_admin_name,
          'token': token,
          'crud_type':'U',
          'board_members_id':board_members_id
        },
        dataType:'html',
        success  :function(data){
          $('#save').val('SAVE').prop('disabled', false);
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'> Board of Members Details Updated Successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                  // location.reload(true);
                  window.location.href='board-of-members';
                }
              }
            });
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>"+json.message+"</p>",
              type: 'red'
            });
            }
          }
        }
      });
    }
  });
</script>
<script type="text/javascript">
  $( document ).ready( function () {
    $( "#park_flat" ).validate( {
      rules: {
        designation_id: "valueNotEqualsdesi",
        effective_date: "required"
      },
      messages: {
      }
    });
    $("#designation_id").on('change', function(){
      var v_type = $('#tower_name').val();
      if(v_type == "")
      {
        $('#error_designation_id').show();
        return false;
      }else{
          $('#error_designation_id').hide();
          return true;
      }
    });
  });
  $.validator.addMethod("valueNotEqualsdesi", function(value){
    // alert(value);
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_designation_id").show();
      return false;
    }
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
  function owner_name() {
    var emp_code = $("#owner_name").val();
    //alert("ok");
    if(emp_code== ''){  
      $("#error_owner_name").show();         
      return false;          
    }else{            
      $("#error_owner_name").hide();         
      return true;          
    }        
  }
</script>
<!-- OWNER Search -->
<!-- <script type="text/javascript" src="../search/jquery.tokeninput.js"></script>
<link rel="stylesheet" href="../search/token-input.css" type="text/css" />
<link rel="stylesheet" href="../search/token-input-facebook.css" type="text/css" />

<script type="text/javascript">
  function search_func(str,t) {
    var xhttp;
    if (str.length == 0) {
      return;
    }
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var json = $.parseJSON(this.responseText);
          if (json.status) {
              $('#owner_name').val(json.emp_name);
              $('#owner_id').val(json.emp_id);
              owner_name();  
          }else{
              alert(json.message);
          }
      }
    };
    xhttp.open("GET", "p_admin_middle_tire/owner_list.php?q="+str+"&pid="+prk_admin_id+"&type=SE", true);
    xhttp.send();
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#owner_name').tokenInput("p_admin_middle_tire/owner_list.php?pid="+prk_admin_id+"&type=SO", {
      theme: "facebook",
      hintText: "Enter Code.",
      noResultsText: "No matching results.",
      preventDuplicates: true,
      searchingText: "Searching...",
      tokenLimit:1
    });
    $('#token-input-owner_name').attr('placeholder','Please type');
  });
</script> -->
<!-- end Search -->

