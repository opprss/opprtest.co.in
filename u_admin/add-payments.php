<!-- ################################################
  
Description: N/A
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php
include "all_nav/header.php"; 
?>
<style>
  .size{
    font-size: 11px;
  }
.error_size{
  font-size: 11px;
  color: gray;

}
.success{
  font-size: 11px;
  color: green;
}
.user-lebel{
    font-size: 11px !important;
    text-transform: uppercase;
  }
</style>
<!-- header position -->


    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Add Payment Getway</h5>
        <!--<form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form> search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card single pd-20 pd-sm-40 col-md-12">
          <!-- <h6 class="card-body-title">Sign Up Here</h6>
          <p class="mg-b-20 mg-sm-b-30">A form with a label on top of each form control.</p> -->
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          
            <div class="row">
              <div class="col-md-12">
                
                  <form id="regForm2" method="post" action="">
                    <div class="form-layout">
                      <div class="row mg-b-25">
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Select Wallet <span class="tx-danger">*</span></label>

                              <div class="select" style="font-size:14px">
                                <select name="payment_method" id="payment_method" value="null">
                                    <option value="">SELECT PAYMENT</option>
                                    <option value="1">Paytm</option>
                                    <option value="2">Instamojo</option>
                                    <option value="3">Paypal</option>
                                </select>
                              </div>
                              <span id="error_state" style="display: none;">SELECT YOUR STATE</span>
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Something <span class="tx-danger">*</span></label>

                            <input type="text" class="form-control" placeholder="Something" name="somthing">
                          </div>
                        </div>
                        <div class="col-lg-2">
                          
                        </div>
                        <div class="col-lg-2 error_show">
                          <div class="form-group">
                            <button type="submit" class="btn btn-block wl_user_button" name="save" id="save">SAVE</button>
                          </div>
                        </div>
                        <div class="col-lg-2 error_show">
                          <div class="form-group">
                            <button type="button" class="btn btn-block wl_user_button_skip" name="skip" id="skip" onclick="document.location.href='vehicle-list';">SKIP</button>
                          </div>
                        </div>

                    </div><!-- form-layout -->
                  </form>
              </div>
              
            </div>
          </div>
        </div><!-- card -->
        <!-- /add employee form -->
        <!-- <span class="pd-20"></span> -->

      </div><!-- am-pagebody -->
      

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">

        /*validation*/
        $( document ).ready( function () {

            /*custom validation*/

        } );
    </script>