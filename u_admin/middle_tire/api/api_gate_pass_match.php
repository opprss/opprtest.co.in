<?php 
/*
Description: user vehicle in and gate pass match. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function gate_pass_match($prk_admin_id,$user_veh_type,$user_vehicle_no){
    global $pdoconn;
    $response = array();
    $sql = "SELECT `prk_gate_pass_num`,`prk_sub_unit_id` 
    FROM `prk_gate_pass_dtl` 
    WHERE `prk_admin_id`='$prk_admin_id' 
    AND `veh_number`='$user_vehicle_no' 
    AND `veh_type`='$user_veh_type' 
    AND `active_flag`='".FLAG_Y."' 
    AND `del_flag`='".FLAG_N."'
    AND  '".TIME_TRN."' BETWEEN DATE_FORMAT(STR_TO_DATE(`effective_date`, '%d-%m-%Y'), '%Y-%m-%d')    
    AND  DATE_FORMAT(STR_TO_DATE(IFNULL(`end_date`,'".FUTURE_DATE_WEB."'), '%d-%m-%Y'), '%Y-%m-%d')";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $response['prk_gate_pass_num'] = $val['prk_gate_pass_num'];
        $response['prk_sub_unit_id'] = $val['prk_sub_unit_id'];
        $response['status'] = 1;
        $response['message'] = 'Right Gate Pass Number';
    }else{

        $response['status'] = 0;
        $response['message'] = 'wrong Gate Pass Number';
    }
    return json_encode($response);
}
?>