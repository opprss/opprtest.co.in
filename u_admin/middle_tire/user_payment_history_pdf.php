<?php
/*
Description: user payment history.
Developed by: Rakhal Raj Mandal/Manoranjan Sarkar
Created Date: 08-05-2018
Update date : -------
*/
    include_once 'api/reg_api.php';
    $response = array();
    if(isAvailable(array('user_admin_id','month','start_date','end_date','payment_type','token'))){
        if(isEmpty(array('user_admin_id','token'))){
            $user_admin_id = trim($_POST['user_admin_id']);
            $month = trim($_POST['month']);
            $start_date = trim($_POST['start_date']);
            $end_date = trim($_POST['end_date']);
            $payment_type = trim($_POST['payment_type']);

            $token = ($_POST['token']);
            $resp=user_token_check($user_admin_id,$token);
            $json = json_decode($resp);
            if($json->status=='1'){
               $respo= user_payment_history($user_admin_id,$month,$start_date,$end_date,$payment_type);
               // $response = $respo;
                $json = json_decode($respo);
                if($json->status=='1'){
                   $response= user_payment_history_pdf($respo);
                }else{
                    $response['status']=0;
                    $response['message']='PDF Created Failed';
                    $response= json_encode($response); 
                }
            }else{
                $response = $resp;
            }
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response= json_encode($response); 
        }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        $response= json_encode($response); 
    } 
    echo $response;
?> 