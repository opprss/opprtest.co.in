<?php  
/*
Description: user insurance image upload.
Developed by: Rakhal Raj Mandal
Created Date: 08-04-2018
Update date :08-04-2018
*/
function user_address_default($user_admin_id,$mobile,$user_add_id){
	global $pdoconn;
	//$mobile=md5($mobile);
    $response = array();

    $sql = "UPDATE `user_address` SET `default_flag`='".FLAG_N."' WHERE `user_admin_id`='$user_admin_id' AND `default_flag`='".FLAG_Y."' AND `active_flag`='".FLAG_Y."' AND `del_flag`='".FLAG_N."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
    	$sql="UPDATE `user_address` SET `default_flag`='".FLAG_Y."' WHERE `user_admin_id`='$user_admin_id' AND `user_add_id`='$user_add_id'";
    	$query = $pdoconn->prepare($sql);
	    if($query->execute()){
	    	$response['status'] = 1;
	        $response['message'] = 'Successful';
	    }else{
	        $response['status'] = 0;
	        $response['message'] = 'Not Successful';
	    } 
	}else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
	}
	return json_encode($response); 
}
?>