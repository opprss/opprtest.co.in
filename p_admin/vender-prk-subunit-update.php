<!-- ################################################
  
Description: Parking area admin update parking rate.
Developed by: namoranjan
Created Date: 27-05-2018

 ####################################################-->
<?php 
include "all_nav/header.php";
if (isset($_SESSION['prk_admin_id'])) {
 $parking_admin_name=$_SESSION['parking_admin_name'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
    }
$prk_sub_unit_veh_space_id = base64_decode($_REQUEST['list_id']);
$respose=prk_sub_unit_veh_space_show($prk_sub_unit_veh_space_id);
// print_r($respose);
$prk_sub_unit_id = $respose[0]['prk_sub_unit_id'];
$vehicle_type_dec= $respose[0]['vehicle_type_dec'];
$veh_type = $respose[0]['veh_type'];
$prk_sub_unit_name=$respose[0]['prk_sub_unit_name'];

$veh_space = $respose[0]['veh_space'];
$prk_sub_unit_veh_space_eff_date = $respose[0]['prk_sub_unit_veh_space_eff_date'];
$prk_sub_unit_veh_space_end_date = $respose[0]['prk_sub_unit_veh_space_end_date'];
// $prk_ins_hr_rate = $respose[0]['prk_ins_hr_rate'];
// $prk_per_hr_rate = $respose[0]['prk_per_hr_rate'];
// $prk_rate_eff_date = $respose[0]['prk_rate_eff_date'];
// $prk_rate_end_date = $respose[0]['prk_rate_end_date'];
// $tot_prk_space = $respose[0]['tot_prk_space'];
// $prk_no_of_mt_dis = $respose[0]['prk_no_of_mt_dis'];
?>

<style type="text/css">
  .date{background-color: white}

  .size{
    font-size: 11px;
  }
.success{
  font-size: 11px;
  color: green;
}
</style>

    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">SubUnit Update</h5>
        <!-- <form id="searchBar" class="search-bar" action="">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">

        <div class="card single pd-20 pd-sm-40">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <div class="row mg-b-25">
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">SUB-UNIT</label>
                  <input type="text" class="form-control" name="rate" id="prk_rate_type" value="<?php echo $prk_sub_unit_name?>" disabled>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">VEHICLE TYPE</label>
                  <input type="text" class="form-control" name="prk_ins_no_of_hr" id="prk_ins_no_of_hr" value="<?php echo  $vehicle_type_dec; ?>" disabled>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">NO OF ALLOCATED SPACE<span class="tx-danger">*</span></label>
                  <input type="text" class="form-control" name="prk_ins_hr_rate" id="prk_ins_hr_rate" value="<?php echo $veh_space; ?>">
                </div>
                <span id="error_space" style="position: absolute; color: red; font-size: 11px" class="error_size"></span>
              </div><!-- col-4-->
              <div class="col-lg-4">
              
                 <div class="form-group">
                  <label class="form-control-label size">EFFECTIVE DATE</label>
                  <input type="text" class="form-control readonly"  name="" id="Effective_Date" value="<?php echo $prk_sub_unit_veh_space_eff_date; ?>" disabled readonly="readonly">
                </div>
              
              </div>
            
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label size">END DATE</label>
         <input type="text" class="form-control prevent_readonly" placeholder="MM/DD/YYYY"  name="end_date" id="end_date" value="<?php if($prk_sub_unit_veh_space_end_date == FUTURE_DATE_WEB) echo ''; else echo $prk_sub_unit_veh_space_end_date; ?>" readonly="readonly" style="background-color:transparent">
     
                </div>
              </div>
             <!--  -->
              <div class="col-lg-4">
                <div class="form-layout-footer mg-t-30">
                  <div class="row">
                    <div class="col-lg-6">
                      <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" id="save">
                    </div>
                    <div class="col-lg-6">
                      <button type="button" class="btn btn-block prk_button_skip" onclick="document.location.href='vender-prk-rate-subunit';">BACK</button>
                    </div>
                    <?php   ?>
                  </div>
                </div>  
              </div>
            </div>  
            </div>
          </div>
        </div>
      </div>     


      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
    
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE_WEB ?>"
 
  $(document).ready(function(){
    /*type check*/  

    $('#save').click(function(){
          var prk_sub_unit_veh_space_id ='<?php echo $prk_sub_unit_veh_space_id; ?>';
          var prk_sub_unit_id ='<?php echo $prk_sub_unit_id; ?>';
          
          var veh_type = '<?php echo $veh_type; ?>';
          var user_name = '<?php echo $parking_admin_name; ?>';
          var prk_admin_id ='<?php echo $prk_admin_id; ?>';
          //var veh_space ='<?php echo $veh_space; ?>';
          var veh_space =$('#prk_ins_hr_rate').val();
          var prk_sub_unit_veh_space_eff_date=$('#Effective_Date').val();
          var prk_sub_unit_veh_space_end_date=$('#end_date').val();
          prk_sub_unit_veh_space_end_date = (!prk_sub_unit_veh_space_end_date == '') ? prk_sub_unit_veh_space_end_date : FUTURE_DATE;
// alert(prk_sub_unit_veh_space_eff_date);

          
          if(prk_sub_unit_veh_space_end_date!=''  && veh_space!=''){
          
          var urlPrkAreRateUpdate = prkUrl+'prk_sub_unit_veh_space_update.php';
          $('#save').val('Wait ...').prop('disabled', true);
          $.ajax({
              url :urlPrkAreRateUpdate,
              type:'POST',
              data :
              {
                
            'prk_sub_unit_veh_space_id':prk_sub_unit_veh_space_id,
            'prk_sub_unit_id':prk_sub_unit_id,
            'prk_admin_id':prk_admin_id,
            'veh_type':veh_type,
            'veh_space':veh_space,
            'prk_sub_unit_veh_space_eff_date':prk_sub_unit_veh_space_eff_date,
            'prk_sub_unit_veh_space_end_date':prk_sub_unit_veh_space_end_date,
            'user_name':user_name,
            'token': '<?php echo $token;?>'                      
              },
              dataType:'html',
              success  :function(data)
              {
                $('#save').val('SAVE').prop('disabled', false);
                // alert(data);
                var json = $.parseJSON(data);
                  if (json.status){
                      $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success !',
                      content: "<p style='font-size:0.9em;'>Subunit updated successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                            window.location='vender-prk-rate-subunit';
                        }
                      }
                    });
                  }else if(!json.session){
                    window.location.replace("logout.php");
                  }
                  else{
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                  }
              }
            });
        }else{
          if(veh_space==''){
            $('#error_space').text('Enter Allocated Space');
          }
          
        }
    });

    // allow only numeric value for rat
    $("#first_hour,#second_hour,#initial_hr,#tot_prk_space,#mm").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });


  });
  $("#mm").keyup(function(){
    var mm = $('#mm').val()
    if (mm > 15 && mm < 59){
      $.confirm({
        icon: 'fa fa-exclamation-circle',
        theme: 'modern',
        title: 'Hold on!',
        content: "<p style='font-size:0.9em;'>Round up time is more then <b><span style='color:red'>'"+mm+"'</span></b> minute. Are you sure? </p>",
        type: 'red',
        buttons: {
          Yes: function () {
            //ok no job
          },
          No: function () {
            $('#mm').val('');
          }
        }
      });
    }else if(mm > 59){
      $('#mm').val('59');
    }
  });

  $('#end_date').click(function ()  {
    // alert();
    var start = new Date;
    var end = "<?php echo $prk_sub_unit_veh_space_eff_date; ?>";

    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
            if (date < now_end) {
                return {
                  disabled : true,
                  class_name : 'date-in-past'
                };
            }
            return {};
        } 
    });
  });
  
 $("#prk_ins_hr_rate").change(function () {
   var veh_space =$('#prk_ins_hr_rate').val();
    if(veh_space==''){
      $('#error_space').text('Vehicle type is required');
      return false;
    }else{
      $('#error_space').text('');
    }
  });
 $("#prk_ins_hr_rate").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0)){
      event.preventDefault();
    }
  });

</script>
