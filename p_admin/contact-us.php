<!-- ################################################
  
Description: parking area admin can contact with oppr team with the following contact form.
Developed by: Manranjan sarkar & Soemen Banerjee
Created Date: 29-03-2018

 ####################################################-->
<?php include "all_nav/header.php";  ?>
 <div class="am-mainpanel"><!-- closing in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Contact Us</h5>
        <form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form><!-- search-bar-->
      </div><!-- am-pagetitle -->
  


      <div class="am-pagebody">
        <div class="card single pd-20 pd-sm-40">
         <h1 style="text-align: center;color: black;">Contact us</h1>
          <div class="row">
            <div class="col-lg-5">
             <div class="row">
               <div class="col-lg-4">
                
               </div>
               <div class="col-lg-6">
                 <span class="icon_size"><i class="icon ion-location"></i></span>
                 <span class="mg-b-20 mg-sm-b-30">OPPR Software Solution New Town ,Rajarhat ,Kolkata ,700156.</span>
                 <br>  <br>
                 <span class="icon_size">
                   <i class="fa fa-envelope-o"></i>
                 </span><span class="mg-b-20 mg-sm-b-30">admin@opprss.com</span>
                          <br><br>
                           <span class="icon_size">
                <i class="fa fa-phone"></i></span>
                <span>&nbsp; +918095609383</span>
    
            
               </div>
               <div class="col-lg-2"></div>
             </div>

   
            </div>
            <div class="col-lg-4">
             
          <!-- <span class="mg-b-20 mg-sm-b-30">A basic form control with.</span> -->
          <span style="position:absolute;" id="msg"></span>
          <br>
          <form action="" method="post" id="contactFrom">
              <input class="form-control" placeholder="Your Name" type="text" maxlength="20" size="20" name="name" id="name" autofocus="autofocus">
              <input class="form-control" placeholder="Your Mobile No"  type="text" name="phone" id="mobile" maxlength="10">
              <input class="form-control" placeholder="Your Email"  type="email" name="email" id="email">
              <input class="form-control" placeholder="Subject"  type="text" name="subject" id="subject">
              <br>
              <textarea rows="3" class="form-control" placeholder="Message" name="message" id="message" style="background-color: transparent; border-color: #00b8d4;"></textarea >
              <br>
              <input type="button" value="SUBMIT" class="btn prk_button" id="submit" style="width: 100px;">
            </form>
             </div>
          <div class="col-lg-3"></div>

         </div>
        </div><!-- card -->
  




</div>
<?php include"all_nav/footer.php"; ?>
<style>
  .icon_size{
  font-size: 40px; line-height: 5px;text-align: justify;
  color: #00e6e6;
  }
</style>
<script> 
$(document).ready(function() {

  $("#mobile").keydown(function (e) {
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
          (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
          (e.keyCode >= 35 && e.keyCode <= 40)) {
               return;
      }
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
  });
  function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
  }

  $("#email").blur( function () {
    if (!ValidateEmail($("#email").val())) {
        $('#msg').text("Invalid email address.").css('color', 'red');
        $('#email').focus();
    }
    else {
        $('#msg').text("Valid email address.").css('color', 'green');
        $("#msg").hide();
    }
  });

  $('#submit').click(function(){
    var name=$('#name').val();
    var mobile=$('#mobile').val();
    var email=$('#email').val();
    var subject=$('#subject').val();
    var message=$('#message').val();
    var CONTATUS_PRK_PARKING="<?php echo PRK_URL; ?>";
    if(name!='' && mobile!='' && email!='' && submit!='' && message!=''){
      $.ajax({
        type: "POST",
        url: CONTATUS_PRK_PARKING+'contect_us_prk.php',
        data: {name:name,phone:mobile,email:email,message:message,subject:subject,inserted_by:name},
        dataType: "html",
        success: function(data) {
          var json = $.parseJSON(data);
          if (json.status) {
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.8em;'>Successfully send</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                    $('#contactFrom')[0].reset();
                }
              }
            });
          }else{
            $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
              type: 'red'
            });
          }
        },
        error: function(err) {
          $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
              type: 'red'
            });
        }
      });

    }else{
     $('#msg').text("Please mention blank fields").css('color', 'red');

    }
  });

});

</script>



