<?php 
/*
Description: parkin area wallet QR code img show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function meet_to_user($prk_admin_id,$meet_mobile,$meet_to_status,$tower_id,$flat_id){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
    if ($meet_to_status=='M') {
        /*$sql = "SELECT user_admin.user_admin_id,
            user_admin.user_mobile as 'meet_mobile',
            user_detail.user_name,
            user_address.tower_name as 'tower_id',
            user_address.falt_name as 'flat_id',
            'Y' as 'app_use',
            if(tower_details.tower_name IS NULL,'NA',tower_details.tower_name)as 'tower_name',
            if(flat_details.flat_name IS NULL,'NA',flat_details.flat_name) as 'flat_name'
            FROM user_address JOIN user_admin JOIN user_detail
            LEFT JOIN tower_details ON tower_details.tower_id=user_address.tower_name
            AND tower_details.prk_admin_id=user_address.prk_admin_id 
            AND tower_details.active_flag='Y' 
            AND tower_details.del_flag='N'
            LEFT JOIN  flat_details ON flat_details.flat_id=user_address.falt_name 
            AND flat_details.prk_admin_id=user_address.prk_admin_id 
            AND flat_details.active_flag='Y'
            AND flat_details.del_flag='N'
            WHERE user_address.prk_admin_id='$prk_admin_id'
            AND user_address.active_flag='Y' 
            AND user_address.del_flag='N'
            AND user_address.user_add_verify_flag='Y' 
            AND user_admin.user_admin_id=user_address.user_admin_id
            AND user_admin.user_ac_status='A'
            AND user_detail.user_admin_id=user_admin.user_admin_id
            AND user_detail.active_flag='Y'
            AND user_admin.user_mobile='$meet_mobile'";*/

        $sql = "SELECT mel.user_admin_id,
            mel.meet_mobile,
            mel.user_name,
            mel.tower_id,
            mel.flat_id,
            mel.app_use,
            mel.tower_name,
            mel.flat_name
            FROM (SELECT '' as user_admin_id,
            mtpl.`me_to_per_mobile` as 'meet_mobile',
            mtpl.`me_to_per_name` as user_name,
            '' as 'tower_id',
            '' as 'flat_id',
            'Y' as 'app_use',
            'NA' as 'tower_name',
            'NA'  as 'flat_name',
            mtpl.`prk_admin_id`
            FROM `meet_to_person_list` mtpl
            WHERE mtpl.`active_flag`='Y'
            AND mtpl.`del_flag`='N'
            UNION
            SELECT user_admin.user_admin_id,
            user_admin.user_mobile as 'meet_mobile',
            user_detail.user_name,
            user_address.tower_name as 'tower_id',
            user_address.falt_name as 'flat_id',
            'Y' as 'app_use',
            if(tower_details.tower_name IS NULL,'NA',tower_details.tower_name)as 'tower_name',
            if(flat_details.flat_name IS NULL,'NA',flat_details.flat_name) as 'flat_name',
            user_address.prk_admin_id
            FROM user_address JOIN user_admin JOIN user_detail
            LEFT JOIN tower_details ON tower_details.tower_id=user_address.tower_name
            AND tower_details.prk_admin_id=user_address.prk_admin_id 
            AND tower_details.active_flag='Y' 
            AND tower_details.del_flag='N'
            LEFT JOIN  flat_details ON flat_details.flat_id=user_address.falt_name 
            AND flat_details.prk_admin_id=user_address.prk_admin_id 
            AND flat_details.active_flag='Y'
            AND flat_details.del_flag='N'
            WHERE user_address.active_flag='Y' 
            AND user_address.del_flag='N'
            AND user_address.user_add_verify_flag='Y' 
            AND user_admin.user_admin_id=user_address.user_admin_id
            AND user_admin.user_ac_status='A'
            AND user_detail.user_admin_id=user_admin.user_admin_id
            AND user_detail.active_flag='Y') mel 
            WHERE mel.prk_admin_id='$prk_admin_id'
            AND mel.meet_mobile='$meet_mobile'";
    }else{
        $sql="SELECT '' as user_admin_id,
            `flat_master`.`flat_master_id`,
            `flat_master`.`contact_number` as 'meet_mobile',
            `flat_master`.`flat_id`,
            `flat_details` .`flat_name`,
            `flat_master`.`tower_id`,
            `tower_details`.`tower_name`,
            `flat_master`.`owner_id`,
            `flat_master`.`living_status`,
            `flat_master`.`living_name` as user_name,
            `flat_master`.`tenant_id`,
            `flat_master`.`app_use`
            FROM `flat_master`,`flat_details`,`tower_details` 
            WHERE `flat_master`.`prk_admin_id`='$prk_admin_id' 
            AND `flat_master`.`flat_id`='$flat_id'
            AND `flat_master`.`tower_id`='$tower_id'
            AND `flat_master`.`flat_id`=`flat_details`.`flat_id`
            AND `flat_details`.`active_flag`='".FLAG_Y."'
            AND `flat_details`.`del_flag`='".FLAG_N."'
            AND `tower_details`.`tower_id`=`flat_master`.`tower_id`
            AND `tower_details`.`active_flag`='".FLAG_Y."'
            AND `tower_details`.`del_flag`='".FLAG_N."'
            AND `flat_master`.`active_flag`='".FLAG_Y."'
            AND `flat_master`.`del_flag`='".FLAG_N."'
            AND '".TIME_TRN."' BETWEEN `flat_master`.`effective_date` AND `flat_master`.`end_date`";
    }
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $tower_id=$val['tower_id'];
        $flat_id=$val['flat_id'];
        $user_name=$val['user_name'];
        $meet_mobile=$val['meet_mobile'];
        $sql="SELECT '$user_name' as 'user_family_name',
            '$meet_mobile' as 'user_family_mobile'
            UNION
            SELECT ufg.user_family_name,
            ufg.user_family_mobile
            FROM user_address uadd JOIN user_family_group ufg
            WHERE uadd.user_add_id=ufg.user_add_id
            AND ufg.active_flag='".FLAG_Y."'
            AND ufg.del_flag='".FLAG_N."'
            AND ufg.intercom_per='".FLAG_Y."'
            AND uadd.tower_name='$tower_id' 
            AND uadd.falt_name='$flat_id' 
            AND uadd.prk_admin_id='$prk_admin_id'
            AND uadd.active_flag='".FLAG_Y."'
            AND uadd.del_flag='".FLAG_N."'
            AND uadd.user_add_verify_flag='".FLAG_Y."'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $count=$query->rowCount();
            if($count>0){
                $arr_catagory = $query->fetchAll();
                $c=0;
                foreach($arr_catagory as $vall){
                    $c+=1;
                    $park['id'] = $c;
                    $park['user_family_name'] = $vall['user_family_name'];
                    $park['user_family_mobile'] = $vall['user_family_mobile'];
                    array_push($allplayerdata, $park);
                }
            }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['user_admin_id'] = $val['user_admin_id'];
        $response['meet_mobile'] = $val['meet_mobile'];
        $response['user_name'] = $val['user_name'];
        $response['family_group'] = $allplayerdata;
        $response['tower_id'] = $val['tower_id'];
        $response['tower_name'] = $val['tower_name'];
        $response['flat_id'] = $val['flat_id'];
        $response['falt_name'] = $val['flat_name'];
        $response['me_address'] = $val['tower_name'].', '.$val['flat_name'];
        $response['app_use'] = $val['app_use'];
    }else{
        $response['status'] = 0;
        $response['message'] = 'Invalid Meet to Mobile Number';
    }
    return json_encode($response);
}
?>