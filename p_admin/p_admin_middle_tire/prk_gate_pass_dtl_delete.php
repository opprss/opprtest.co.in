<?php 
/*
Description: Parking area gate pass delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();

 if(isAvailable(array('prk_admin_id','user_name','prk_gate_pass_id','token'))){
    if(isEmpty(array('prk_admin_id','user_name','prk_gate_pass_id','token'))){ 
        $prk_admin_id = $_POST['prk_admin_id'];
        $prk_gate_pass_id = $_POST['prk_gate_pass_id'];
        $user_name = trim($_POST['user_name']);
        $token = trim($_POST['token']);

        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            $response = prk_gate_pass_dtl_delete($prk_gate_pass_id,$prk_admin_id,$user_name);
            //echo $respon;
        }else{
            $response = $resp;
            //echo $response;
        }
    }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
?>