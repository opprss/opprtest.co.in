<?php 
/*
Description: vehicle verify and data insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :26-04-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','prk_area_user_id_search','token','month','year'))){
    if(isEmpty(array('prk_admin_id','prk_area_user_id','token'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);      
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $prk_area_user_id_search = trim($_POST['prk_area_user_id_search']);
        $month = trim($_POST['month']);
        $year = trim($_POST['year']);
        $token = trim($_POST['token']);
        $user_admin_id=$oth1=$oth2='';
        $token_check = prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($token_check);
        if($json->status=='1'){
            
            $response = attendance_list($prk_admin_id,$prk_area_user_id_search,$month,$year);
        }else{
            $response = $token_check;
        }   
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>