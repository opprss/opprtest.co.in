<!-- ################################################
  
Description: user can add or modify address here. add and modify is difided with a reffer(from where page requist is comming that URL) variable in JS.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
  include "all_nav/header.php";
  $user_add_id=base64_decode($_REQUEST['user_add_id']);
  $prk_admin_id=base64_decode($_REQUEST['prk_admin_id']);
  $family_group_list=user_family_group_list($user_admin_id,$user_add_id);
  $family_group_list = json_decode($family_group_list, true);
  $category_type='FAMILY_GROUP';
  $drop_down= common_drop_down_list($category_type);
  $family_drop_down_list = json_decode($drop_down, true);
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
  .error{
    font-size: 11px;
    color: red;
  }
  td,th{
    text-align: center;
  }
</style>
<script src="./../js/webcam.min.js"></script>
<script src="js/timepicker.min.js"></script>
<link href="css/timepicker.min.css" rel="stylesheet">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script src="../lib/validation/jquery.validate.js"></script>
<link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../lib/select2/css/select2.min.css" rel="stylesheet">

<div class="am-mainpanel">
    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Family Group<?//=$drop_down?></h5>
    </div>
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40" >
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <form role="form" id="family_grp" method="post"  enctype="multipart/form-data">
            <div class="row mg-b-25">
            	<input type="hidden" name="user_add_id" id="user_add_id" value="<?php echo $user_add_id;?>">
            	<input type="hidden" name="prk_admin_id" id="prk_admin_id" value="<?php echo $prk_admin_id;?>">
              <div class="col-lg-2">
        				<div class="form-group">
        					<label class="form-control-label user-lebel">Name<span class="tx-danger">*</span></label>
        					<input type="text" class="form-control" placeholder="Name" name="user_family_name" id="user_family_name" onkeyup="addressUppercase()" value="">
        					<span style="position: absolute;" id="error_user_family_name" class="error_size"></span>
        				</div>
        			</div>
              <div class="col-lg-1">
                	<div class="form-group">
                  	<label class="form-control-label size">Gender<span class="tx-danger">*</span></label>
                    	<label class="rdiobox">
                      	<input name="user_family_gender" id="male" value="M" type="radio" checked>
                      	<span style="font-size: 11px;">Male </span>
                    	</label>
                      <label class="rdiobox">
                          <input name="user_family_gender" id="female" value="F" type="radio">
                          <span style="font-size: 11px;">Female </span>
                      </label>
                	</div>
              </div>
              <div class="col-lg-2">
  	            <div class="form-group">
  	                <label class="form-control-label size">Mobile<span class="tx-danger">*</span></label>
  	                <input type="text"  name="user_family_mobile" class="form-control" placeholder=" Mobile" id="user_family_mobile" maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
  	                <span style="position: absolute;" id="error_user_family_mobile" class="error_size"></span>
  	            </div>
              </div>
              <div class="col-lg-2">
  	            <div class="form-group">
  	                <label class="form-control-label user-lebel">Relationship<span class="tx-danger">*</span></label>
                    <div class="select" style="font-size:14px">
                    <select name="user_family_relation" id="user_family_relation">
                      <option value="">Select Type</option>
                      <?php foreach($family_drop_down_list['drop_down'] as $val){?>
                      <option value="<?=$val['sort_name']?>"><?=$val['full_name']?></option>
                      <?php } ?>
                    </select>
  	                <span style="position: absolute;" id="error_relation" class="error_size"></span>
                  </div>
  	            </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <div id="my_camera"></div><br/>
                </div>
              </div>
              <div class="col-md-1">
                <div class="form-group">
                  <button type="button" class="btn btn-block btn-primary" onClick="take_snapshot()"><p style="margin-bottom: 0px;font-size: 11px;margin-left: -3px;">Capture<br>Image</p></button>
                  <button type="button" class="btn btn-block btn-primary" id="my-button"><p style="margin-bottom: 0px;font-size: 11px;margin-left: -2px;">Upload<br>Image</p></button>
                  <input type="file" name="visitor_img" accept="image/*" id="visitor_img" style="visibility: hidden;">
                  <input type="hidden" name="user_family_img" class="user_family_img" id="user_family_img">
                </div>
                
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <div id="results">captured Visitor image will appear here...</div>
                  <!-- <span  id="error_vis_image" class="error_size"></span> -->
                  <label style="position: absolute;" class="error_size" id="error_vis_image"></label>
                </div>
              </div>
            </div> 
            <div class="row" style="margin-top: -80px;">
              <div class="col-lg-2">
              	<div class="form-group">
                	<label class="form-control-label size">Intercom permission<span class="tx-danger">*</span></label>
                  	<label class="rdiobox">
                    	<input name="intercom_per" id="yes" value="Y" type="radio" checked="">
                    	<span>Yes</span>
                  	</label>
                    <label class="rdiobox">
                        <input name="intercom_per" id="no" value="N" type="radio">
                        <span>No</span>
                    </label>
              	</div>
              </div>
              <div class="col-lg-2">
                <div class="form-group mg-t-35">
                <!-- <div class="form-group"> -->
                  <input type="button" value="SAVE" class="btn btn-block btn-primary wl_user_button" name="save" id="save">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group mg-t-35">
                <!-- <div class="form-group"> -->
                  <button type="reset" class="btn btn-block wl_user_button btn-primary">Reset</button>
                </div>
              </div>
              <div class="col-lg-2">
  	            <div class="form-group mg-t-35">
  	                <button type="button" class="btn btn-block wl_user_button_skip" name="skip" id="skip" onclick="document.location.href='address-list';">Back</button>
  	            </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40">
        <div class="table-wrapper">
          <table id="datatable1" class="table display" cellspacing="0" width="100%">
      		  <thead>
              <tr>
                <th class="wd-10p">Sl No.</th>
                <th class="wd-20p">Name</th>
                <th class="wd-10p">Gender</th>
                <th class="wd-12p">Mobile</th>
                <th class="wd-6p">Permission</th>
                <th class="wd-15p">Relationship</th>
                <th class="wd-10p">Action</th>
              </tr>
          	</thead>
          	<tbody>
          		<?php 
              $x=1;
              if ($family_group_list['status']) {
              foreach($family_group_list['guest_list'] as $value){
              $user_family_id = $value['user_family_id'];
              $user_add_id = $value['user_add_id'];
              $gender=($value['user_family_gender']=='M')?'MALE':'FEMALE';
              $intercom_per=($value['intercom_per']=='Y')?'YES':'NO';
              ?>
  	      		<tr>
  	      			<td><img style="width: 50px;height: 50px;" src="<?=$value['user_family_img'];?>"></td>
  	      			<td><?php echo $value['user_family_name']; ?></td>
  	      			<td><?php echo $gender; ?></td>
  	      			<td><?php echo $value['user_family_mobile']; ?></td>
  	      			<td><?php echo $intercom_per; ?></td>
  	      			<td><?php echo $value['user_family_relation']; ?></td>
  	      			<td>
  	      				<a href="<?php echo 'user-family-group-modify?user_family_id='.base64_encode($user_family_id).'&user_add_id='.base64_encode($user_add_id);?>" class="" data-toggle="tooltip" data-placement="top" title="Modify">
                            <i class="fa fa-edit" style="font-size:18px"></i>
                          </a>&nbsp;
                          <button type="button" class="clean-button" name="delete" id="delete<%=count++%>" value="<?php echo $user_family_id; ?>" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
  	      			</td>
  	      		</tr>
        		  <?php $x++; }} ?>
      		  </tbody>
          </table>
        </div>
      </div>
    </div>
<?php include"all_nav/footer.php"; ?>
<script type="text/javascript">
	var userUrl = "<?php echo USER_URL; ?>";
  	var user_admin_id = "<?php echo $user_admin_id; ?>";
  	var token = "<?php echo $token; ?>";
  	var mobile = "<?php echo $user_mobile; ?>";

  $('#user_family_relation').change(function () {
    vis_image();
    var myID = $(this).val();
    if(myID == 'KID'){
      $('input[name="intercom_per"]')[1].checked = true;
      $('#yes').prop('disabled', true);
      $('#no').prop('disabled', true);
    }else{
      $('input[name="intercom_per"]')[0].checked = true;
      $('#yes').prop('disabled', false);
      $('#no').prop('disabled', false);
    }
  });
  function vis_image() {
    var user_family_relation = $('#user_family_relation').val();
    var name = $("#user_family_img").val();
    if(user_family_relation== 'KID'){
      if(name== ''){
        $("#error_vis_image").text('This field is required.');
        return false;
      }else{
        $("#error_vis_image").text('');
        return true;
      }
    }else{
      $("#error_vis_image").text('');
      return true;
    }
  }
	$( document ).ready( function () {
		$( "#family_grp" ).validate( {
	      rules: {
	        user_family_name: "required",
	        user_family_mobile:  {
            required: function(){
              if($("#user_family_relation").val() !="KID"){
                return true;
              }else{
                return false;
              }
            }
          },
	        user_family_relation: "required",
          user_family_gender: "required",
	        user_family_img: "required"
	      },
	      messages: {
          
	      }
	    });
		$('#save').click(function () {
			var user_add_id = $('#user_add_id').val();
			var prk_admin_id = $('#prk_admin_id').val();
		  var user_family_name = $('#user_family_name').val();
    	var user_family_gender =  $('input[name=user_family_gender]:checked').val();
    	var user_family_mobile = $('#user_family_mobile').val();
    	var user_family_relation = $('#user_family_relation').val();
    	var intercom_per =  $('input[name=intercom_per]:checked').val();
      var user_family_img = $('#user_family_img').val();
      
			if($("#family_grp").valid() & vis_image()==true){
				//alert("ok");
        var family_group = userUrl+'user_family_group.php';
        $.ajax({
          url :family_group,
          type:'POST',
          data :{
            'user_admin_id':user_admin_id,
            'mobile':mobile,
            'token':token,
            'user_add_id':user_add_id,
            'prk_admin_id':prk_admin_id,
            'user_family_name':user_family_name,
            'user_family_gender':user_family_gender,
            'user_family_relation':user_family_relation,
            'user_family_mobile':user_family_mobile,
            'intercom_per':intercom_per,
            'user_family_img':user_family_img,
            'call_type':'W'
          },
          dataType:'html',
          success  :function(data){
            $('#save').val('SAVE').prop('disabled', false);
            //alert(data);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.9em;'>Added Successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                      location.reload();
                  }
                }
              });
            }else{
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.7em;'>Somthing went wrong</p>",
                type: 'red'
              });
              if (json.session==0) {
                window.location.replace("logout.php");
              }
            }
          }
        });
			}
		});
	});  
</script>
<script type="text/javascript">
  $(document).ready(function(){
    var userUrl = "<?php echo USER_URL; ?>";
    $('button[id^="delete"]').on('click', function() {
      var user_admin_id = "<?php echo $user_admin_id;?>";
      var mobile=' <?php echo $_SESSION['mobile']; ?>';
      var token = "<?php echo $token; ?>";
      var user_add_id = $('#user_add_id').val();
      var user_family_id = this.value;
      $.confirm({
        title: 'Are You Sure?',
        content: "<p style='font-size:0.8em;'>It will delete the item permanently</p>",
        theme: 'modern',
        type: 'red',
        buttons: {
          cancel: function () {
          },
          somethingElse: {
            text: 'Delete',
            btnClass: 'btn-red',
            keys: ['Y', 'shift'],
            action: function(){
              if (user_family_id != '') {
                var urlDtl = userUrl+'user_family_group_delete.php';
                //alert(urlDtl);
                $.ajax({
                  url :urlDtl,
                  type:'POST',
                  data :
                  {
                    'user_admin_id':user_admin_id,
                    'mobile':mobile,
                    'token': token,
                    'user_add_id':user_add_id,
                    'user_family_id':user_family_id
                  },
                  dataType:'html',
                  success  :function(data)
                  {
                    //alert(data);
                    var json = $.parseJSON(data);
                    if (json.status) {
                      $.alert({
                        icon: 'fa fa-smile-o',
                        theme: 'modern',
                        title: 'Success !',
                        content: "<p style='font-size:0.8em;'>Family Group Delete Successfully</p>",
                        type: 'green',
                        buttons: {
                          Ok: function () {
                             location.reload();
                          }
                        }
                      });
                    }else{
                      if( typeof json.session !== 'undefined'){
                        if (!json.session) { 
                          window.location.replace("logout.php"); 
                        }                  
                      }else{                    
                        $.alert({                    
                          icon: 'fa fa-frown-o',                    
                          theme: 'modern',                    
                          title: 'Error !',                    
                          content: "Somting went wrong",                    
                          type: 'red'                  
                        });                  
                      }                
                    }
                  }
                });
              }else{
                $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
                type: 'red'
                });
              }
            }
          }
        }
      });
    });
  });
  let d=document.getElementsByClassName('clean-button');
  for(let i=0;i<d.length;i++){
    d[i].addEventListener("mouseover",function(){
      d[i].style.cursor="pointer";
    });
  }    
</script>

<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#user_family_name,#user_family_relation").keypress(function(event){
			var inputValue = event.charCode;
			if(!(inputValue >= 60) && (inputValue != 0 & inputValue !=32)){
			// if(!(inputValue >= 97) && (inputValue != 46)){
			event.preventDefault();
			}
		});
	});
</script>
<script language="JavaScript">
  Webcam.set({
    width: 130,
    height: 100,
    image_format: 'jpeg',
    jpeg_quality: 90
  });
  Webcam.attach( '#my_camera' );
  function take_snapshot() {
    Webcam.snap( function(data_uri) {
      $(".user_family_img").val(data_uri);
      document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
      vis_image();
    });
  }
  $(function() {
    $("#visitor_img").change(function() {
      var file = this.files[0];
      var imagefile = file.type;
      var match= ["image/jpeg","image/png","image/jpg"];
      if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
      {
        $('#previewing').attr('src','../images/noimage.jpg');
        return false;
      }else{
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(this.files[0]);
      }
    });
  });
  function imageIsLoaded(e) {
    // alert('ok');
    $(".user_family_img").val(e.target.result);
    document.getElementById('results').innerHTML = '<img style="height: 100px;width: 130px;" src="'+e.target.result+'"/>';
    vis_image();
  };
  $('#my-button').click(function(){
    $('#visitor_img').click();
  });
</script>