<?php
/*
Description: user licence delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function user_lic_dtl_delete($user_lic_id,$user_admin_id,$mobile){
    global $pdoconn;
    $response = array();
    $sql ="UPDATE `user_lic_dtl` SET `updated_by`='$mobile', `updated_date`='".TIME."', `del_flag`='".FLAG_Y."' WHERE `user_admin_id`='$user_admin_id' AND `user_lic_id`='$user_lic_id' AND `del_flag`='".FLAG_N."' ";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Licane Delete Successful';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Delete Not Successful';
        return json_encode($response); 
    }
}
?>