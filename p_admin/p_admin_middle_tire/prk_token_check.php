<?php
/*
Description: Parking adimn contect us.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id', 'token'))){
    if(isEmpty(array('prk_admin_id', 'token'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
        $token = trim($_POST['token']);
        $response = prk_token_check($prk_admin_id,$token);
        echo $response;
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        echo json_encode($response);      
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response); 
} 
?>