<?php 
/*
Description: Parking employ gate logout.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date : 07-04-2018
*/ 
function prk_user_gate_logout($prk_admin_id,$out_prk_user_username,$prk_user_gate_login_id){
    global $pdoconn;
    $response = array();

    $sql="UPDATE `prk_user_gate_login_dtl` SET `end_date`='".TIME."',`updated_by`='$out_prk_user_username',`updated_date`='".TIME."' WHERE `prk_admin_id`='$prk_admin_id' AND `prk_user_username`='$out_prk_user_username' AND `end_date` is null AND prk_user_gate_login_id = '$prk_user_gate_login_id'";

   $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Successful';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Successful';
        return json_encode($response);
    }          
}
?>