<?php
/*
Description: Parking area vehicle rate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function park_area_rate($prk_vehicle_type,$prk_rate_type,$prk_ins_no_of_hr,$prk_ins_hr_rate,$prk_no_of_mt_dis,$prk_rate_eff_date,$prk_admin_id,$prk_rate_end_date,$user_name,$tot_prk_space,$veh_rate_hour,$veh_rate){
    global $pdoconn;
    $response = array();
    
    
     $sql = "INSERT INTO `prk_area_rate`(`prk_vehicle_type`,`prk_rate_type`,`prk_ins_no_of_hr`,`prk_ins_hr_rate`,`prk_no_of_mt_dis`,`prk_rate_eff_date`,`prk_rate_end_date`,`prk_admin_id`,`inserted_by`,`tot_prk_space`,`inserted_date`,`veh_rate_hour`,`veh_rate`) VALUE ('$prk_vehicle_type','$prk_rate_type','$prk_ins_no_of_hr','$prk_ins_hr_rate','$prk_no_of_mt_dis','$prk_rate_eff_date','$prk_rate_end_date','$prk_admin_id','$user_name','$tot_prk_space','".TIME."','$veh_rate_hour','$veh_rate')";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Data Save sucessfull';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Data Not Save';
        return json_encode($response);
    }
}
?>