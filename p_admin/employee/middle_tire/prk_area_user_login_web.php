<?php 
/*
Description: parking employ login
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
// include_once 'api/global_api.php';
include_once 'api/parkAreaReg_api.php';
$response = array();

if(isAvailable(array('prk_user_username','prk_user_password'))){

    if(isEmpty(array('prk_user_username','prk_user_password'))){
        $device_token_id = isset($_POST['device_token_id']) ? trim($_POST['device_token_id']) : '';
        $device_type = isset($_POST['device_type']) ? trim($_POST['device_type']) : 'OTH';
        $prk_user_username = trim($_POST['prk_user_username']);
        $prk_user_password = trim($_POST['prk_user_password']);
        $respon = prk_area_user_login($prk_user_username,$prk_user_password,$device_token_id,$device_type);

        $json = json_decode($respon);
         if ($json->status == 1) {
            $_SESSION["prk_user_username"]= $json->prk_user_username;
            $_SESSION["prk_area_user_id"]= $json->prk_area_user_id;
            $_SESSION["prk_admin_id"]= $json->prk_admin_id;
            $_SESSION["prk_user_name"]= $json->prk_user_name;
            $_SESSION["token"]= $json->token;
           // header('Location: ' . $_SERVER['HTTP_REFERER']);
            echo $respon;
         }else{
            echo $respon;
         }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        echo json_encode($response);
    }
}else{
     $response['status'] = 0;
     $response['message'] = 'Invalid API Call';
     echo json_encode($response);
}
?>