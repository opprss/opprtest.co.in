<?php
/*
Description: Parking gate delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','token'))){
 	if(isEmpty(array('prk_admin_id','token'))){ 
	 	$prk_admin_id = $_POST['prk_admin_id'];
	 	$prk_area_user_id = isset($_POST['prk_area_user_id']) ? trim($_POST['prk_area_user_id']) : ''; 
	 	$date_list = isset($_POST['date_list']) ? trim($_POST['date_list']) : ''; 
	 	$token = trim($_POST['token']);
        $resp=prk_token_check($prk_admin_id,$token);
		$json = json_decode($resp);
		if($json->status){
		    $response = current_attendance_list($prk_admin_id,$prk_area_user_id,$date_list);
		}else{
		    $response = $resp;
		}
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 	$response = json_encode($response);
}
echo $response;
?>