<?php 
/*
Description: parking area all detais show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
function prk_area_super_user_dtl($prk_user_username){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql ="SELECT `prk_area_dtl`.`prk_area_name`,
    `prk_area_address`.`prk_add_area`,
    `prk_area_address`.`prk_add_city`,
    `prk_area_address`.`prk_add_state`,
    `prk_area_address`.`prk_add_country`,
    ifnull(`prk_area_dtl`.`prk_area_logo_img`, `prk_area_dtl`.`prk_area_pro_img`) AS `prk_area_pro_img`,
    `city`.`city_name`,
    `state`.`state_name`,
    `prk_area_user`.`active_flag`
    FROM `prk_area_dtl`, `prk_area_address`,`city`,`state`,`prk_area_user` 
    where `prk_area_dtl`.`prk_admin_id` = `prk_area_address`.`prk_admin_id` 
    AND `prk_area_dtl`.`active_flag`='".FLAG_Y."'
    AND `prk_area_address`.`active_flag`='".FLAG_Y."' 
    AND `prk_area_address`.`del_flag`='".FLAG_N."'
    AND `city`.`e_id`= `prk_area_address`.`prk_add_city`
    AND `state`.`e`= `prk_area_address`.`prk_add_state`
    AND `prk_area_dtl`.`prk_admin_id`=`prk_area_user`.`prk_admin_id`
    AND `prk_area_user`.`prk_user_username`='$prk_user_username'
    AND `prk_area_user`.`del_flag` = '".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        if ($val['active_flag']==FLAG_Y) {
            $response['status'] = 1;
            $response['message'] = 'Successful';
            $response['prk_area_name'] = $val['prk_area_name'];
            $response['prk_add_area'] = $val['prk_add_area'];
            $response['prk_add_city'] = $val['prk_add_city'];
            $response['prk_add_state'] = $val['prk_add_state'];
            $response['prk_add_country'] = $val['prk_add_country'];
            $response['city_name'] = $val['city_name'];
            $response['state_name'] = $val['state_name'];
            $response['prk_area_pro_img'] = $val['prk_area_pro_img'];
        }else{
            $response['status'] = 0;
            $response['message'] = 'User ID Locked';
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'Invalid User ID';
    }
    return json_encode($response);
}
?>