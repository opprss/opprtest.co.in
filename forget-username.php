<?php
include('global_asset/config.php');
session_start();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Meta -->
    <!-- <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels"> -->


    <title>OPPRSS | FORGET USERNAME</title>
    <link rel="shortcut icon" href="<?php echo FAVICON ?>" type="image/x-icon">

    <!-- vendor css -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">
    <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <script src="lib/jquery/jquery.js"></script>
    <!-- <link rel="stylesheet" type="text/css" href="../css/style.css"> -->

    <!-- Amanda CSS -->
    <link rel="stylesheet" href="css/amanda.css">
    <link rel="stylesheet" href="css/style-up.css">
    <!-- popup -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <style>
      .size{
        font-size: 11px;
      }
    .error_size{
      font-size: 0.9em;
      color: #FFA07A;

    }
    .success{
      font-size: 11px;
      color: green;
    }
    </style>

  </head>

  <body>

    <div class="am-signin-wrapper">
      <div class="am-signin-box">
        <div class="row no-gutters">
          <div class="col-lg-7" style="background:url('img/index-banner.jpg');padding-bottom: 0px;">
            <div class="signin-logo">
              <div style="text-align: left;">
              <a href="http://opprss.com" target="_blank"><img style="width: 210px; text-align: left;" src="<?php echo LOGO ?>"></a></div>
              <div class="content">
                <h1>Welcome to OPPRSS</h1>
                <h5 class="d-none d-sm-block pd-b-10">We provide an innovative and executive parking & security solution that improves parking & security experience more than ever before.</h5>  
              </div>
              <div class="row features-icon">
                <div class="col-lg-4">
                  <i class="fa fa-tasks fa-3x"></i>
                  <h6 class="pd-t-5">Digitally Tracked</h6>
                </div> 
                <div class="col-lg-4">
                  <i class="fa fa-american-sign-language-interpreting fa-3x"></i>
                  <h6 class="pd-t-5">Better Communication</h6>
                </div> 
                <div class="col-lg-4">
                  <i class="fa fa-android fa-3x "></i>
                  <h6 class="pd-t-5">App Based</h6>
                </div>                
              </div>                           
              <div class="content-footer features-icon mg-t-50" style="text-align: right;">
                <button class="btn-tab" onclick="window.open('https://www.opprss.com/contact-us', '_blank');" id="con_us">Contact Us</button>
                <button class="btn-tab" id="user_tab">User</button>
                <button class="btn-tab" id="admin_tab">Parking Area</button>
                <button class="btn-tab" id="super_admin_tab">Super Adnin</button>
                 <hr>
                <p class="tx-center tx-white-5 tx-12 mg-t-0 mg-b-0"><?php echo COPYRIGHT_EMAIL?></p>
              </div>
            </div>
          </div>
          <div class="col-lg-5 custom-width">
            <!-- <h5 class="tx-gray-800 mg-b-25">Forget Username</h5> -->
            <form id="forgetUsernameForm" method="post" action="">
              <h2 class="tx-gray-800 mg-b-50 text-center">Forget Username</h2>
              <div class="form-group tx-center mg-b-30" id="tab">
                <!-- <div class="col-lg-12"> -->
                  <div class="input-group">
                    <div id="radioBtn" class="btn-group">
                    <div class="row">
                      <div class="col-lg-6" style="margin-left: -18px;"><a class="btn btn-custom active radio" data-toggle="login_for" data-title="P" name="login_for" id="parking" style="width: 150px">Parking Area</a></div>

                      <div class="col-lg-6" style="margin-left: -18px;"><a class="btn btn-custom notActive radio" data-toggle="login_for" data-title="U" name="login_for" value="u" id="user" style="width: 150px;">User</a></div>
                    </div>
                    </div>
                    <input type="hidden" name="login_for" id="login_for" value="U">
                  </div>
                <!-- </div> -->
              </div>
              <div id="for_user" style="display: none;">
                <div class="row">
                  <div class="col-lg-12 mg-t-10">
                    <p class="d-none d-sm-block">Your registered <b>mobile</b> number is your username </p>
                  </div>
                </div>
              </div>
              <div id="for_parking"> 
                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group error_show">
                      <input type="text" name="email" id="email" class="form-control inputText" required>
                      <span class="floating-label" id="both">EMAIL</span>
                      <span class="error_email error_size"></span>
                    </div>
                  </div>
                </div>
              </div>
              <br/>
              <button type="button" class="btn btn-block" name="send" id="send">Send</button>
            </form>
            <div class="mg-t-10">
              <!-- <?php //echo $a;?> -->
              <?php
                if (isset($_SESSION['error'])) {
                  echo "<p style=color:red>".$_SESSION['error']."</p>";
                  /*unset($_SESSION['login_error_msg'];*/
                  unset($_SESSION['error']);
                }
              ?>
            </div>
            <div class="mg-t-10 pull-right" style="font-size: 1em;"><a href="signin">Sign In</a></div>
          </div><!-- col-7 -->
        </div><!-- row -->
        <!-- <p class="tx-center tx-white-5 tx-12 mg-t-10 mg-b-0">&copy; 2018 by Oppr Software Solution. All Rights Reserved</p>
         -->
      </div><!-- signin-box -->
    </div><!-- am-signin-wrapper -->

    <!-- <script src="lib/jquery/jquery.js"></script> -->
    <script src="lib/popper.js/popper.js"></script>
    <script src="lib/bootstrap/bootstrap.js"></script>
    <script src="lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>

    <script src="js/amanda.js"></script>

    <!-- validation -->
    <!-- <script src="../lib/validation/jquery-1.11.1.js"></script> -->
    <script src="lib/validation/jquery.validate.js"></script>
  </body>

<script type="text/javascript">
  var userUrl = "<?php echo USER_URL; ?>";
  var prkUrl = "<?php echo PRK_URL; ?>";

  /*radio button*/
  $('#radioBtn a').on('click', function(){
      var login_for = $(this).data('title');
      //alert(login_for);
      var tog = $(this).data('toggle');
      $('#'+tog).prop('value', login_for);

      //alert(hid);            
      $('a[data-toggle="'+tog+'"]').not('[data-title="'+login_for+'"]').removeClass('active').addClass('notActive');
      $('a[data-toggle="'+tog+'"][data-title="'+login_for+'"]').removeClass('notActive').addClass('active');
      if(login_for == 'P'){

        $('#forgatePassForm').trigger("reset");
        $("#for_parking").show();
        $("#for_user").hide();
        $("#login_for").attr("value","P");
        $("#send").show();
      }else if(login_for == 'U'){
        //alert("u");
        //$("#forgatePassForm").attr("action","user_admin/web/login.php");
        $('#forgatePassForm').trigger("reset");
        $("#for_user").show();
        $("#for_parking").hide();
        $("#send").hide();
        $("#login_for").attr("value","U");
      }else{
        //alert("chose somthing");
      }
  });


        /*validation*/
        $( document ).ready( function () {

          $( "#forgetUsernameForm" ).validate( {
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    email: "Please enter email",
                }
            } );

          $('#send').click(function () {

                //alert("send");
                if($("#forgetUsernameForm").valid()){
                
                  var email = $('#email').val();
                  var urlAccountStatusEmail = prkUrl+'accountStatusEmail.php';
                 //alert(urlAccountStatusEmail);

                  $.ajax({
                      url :urlAccountStatusEmail,
                      type:'POST',
                      data :
                      {
                        'email':email
                      },
                      dataType:'html',
                      success  :function(data)
                      {
                        //var json = $.parseJSON(data);
                        //alert(json.status)
                        if (data== 1) {
                          //alert("redirect");
                          $.alert({
                            icon: 'fa fa-smile-o',
                            theme: 'modern',
                            title: 'Success !',
                            content: "<p style='font-size:0.9em;'>Username sent to your email</p>",
                            type: 'green',
                            buttons: {
                              Ok: function () {
                                  window.location='signin';
                              }
                            }
                          });
                        }else{
                          $(".error_email").text("Email address is not registerd with us.")
                          /*$.alert({
                            icon: 'fa fa-frown-o',
                            theme: 'modern',
                            title: 'Error !',
                            content: "<p style='font-size:0.7em;'>Something went wrong</p>",
                            type: 'red'
                          });*/
                        }
                      }
                  }).responseText;

                }//if valid end
            });

          $("#email").on('change keyup paste', function() {

                $('.error_email').text('');
              
            });
        } );

    </script>


</html>
