<?php
/*
Description: Parking area login.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
    //oppr_super_employee_login
    require_once 'api/parkAreaReg_api.php';
    $response = array();
 
    if(isAvailable(array('super_admin_user','super_admin_password'))){

        if(isEmpty(array('super_admin_user','super_admin_password'))){
            $super_admin_user = trim($_POST['super_admin_user']);
            $super_admin_password = trim($_POST['super_admin_password']);
            $response = employee_user_login($super_admin_user,$super_admin_password);
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandator';
            $response = json_encode($response);
        }
    }else{
        $response['status'] = 0; 
        $response['message'] = 'Invalid API Call';
        $response = json_encode($response);
    }
    echo $response;
?>