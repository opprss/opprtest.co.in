<?php
/*
Description: Parking adimn Address save.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('book_driver_dtl_id'))){

    if(isEmpty(array('book_driver_dtl_id'))){
        $book_driver_dtl_id = trim($_POST['book_driver_dtl_id']);
        $response = driver_booking_list($book_driver_dtl_id);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
     $response['status'] = 0; 
     $response['message'] = 'Invalid API Call';
     $response = json_encode($response);
}
echo ($response);
?>