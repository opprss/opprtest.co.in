<!-- ################################################
  
Description: user can change password for login.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
include "all_nav/header.php"; 
?>
<style>
  .size{
    font-size: 11px;
  }
.error_size{
  font-size: 11px;
  color: red;

}
.success{
  font-size: 11px;
  color: green;
}
.user-lebel{
    font-size: 11px !important;
    text-transform: uppercase;
  }
</style>
<!-- header position -->


    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Change Password</h5>
         <!-- <form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form> search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card single pd-20 pd-sm-40 col-md-12">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          
            <div class="row">
              <div class="col-md-12">
                
                  <form id="regForm2" method="post" action="">
                    <div class="form-layout">
                      <div class="row mg-b-25">
                        
                        <div class="col-lg-3">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">mobile number</label>

                            <input type="text" class="form-control readonly" placeholder="Mobile" name="mobile" id="mobile" value="<?php echo $user_mobile; ?>" readonly="readonly" style="background-color: transparent;">
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">old password <span class="tx-danger">*</span></label>

                            <input type="password" class="form-control" placeholder="Old Password" name="oldPasword" id="oldPasword">
                            <span style="position: absolute;" id="error_oldPasword" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">New password <span class="tx-danger">*</span></label>

                            <input type="password" class="form-control readonly" placeholder="New Password" name="password" id="password" disabled="disabled" style="background-color: transparent;">
                            <span style="position: absolute;" id="error_password" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">confirm password <span class="tx-danger">*</span></label>

                            <input type="password" class="form-control readonly" placeholder="Confirm Password" name="repassword" id="repassword" disabled="disabled" style="background-color: transparent;">
                            <span style="position: absolute;" id="error_repassword" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-2 error_show">
                          <div class="form-group">
                            <button type="button" class="btn btn-block wl_user_button mg-t-10" name="change" id="change" disabled="disabled">CHANGE</button>
                          </div>
                        </div>
                        <div class="col-lg-2 error_show">
                          <div class="form-group">
                            <button type="button" class="btn btn-block wl_user_button_skip mg-t-10" name="skip" id="skip" onclick="document.location.href='vehicle-list';">BACK</button>
                          </div>
                        </div>
                    </div><!-- form-layout -->
                  </form>
              </div>
              
            </div>
          </div>
        </div><!-- card -->
      </div><!-- am-pagebody -->
      

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">

  var userUrl = "<?php echo USER_URL; ?>";

        /*validation*/
        $( document ).ready( function () {


            $("#oldPasword").keyup(function () {
              var oldPasword=$('#oldPasword').val();
              var mobile=$('#mobile').val();
              if(oldPasword==''){
                $('#error_oldPasword').text('Please enter your old password');
                $('#oldPasword').focus();

                }else if(oldPasword != ''){
                  var urlChangePass = userUrl+'change_password.php';
                  //alert('hello');
                  //alert(urlChangePass)
                    $.ajax({
                      url :urlChangePass,
                      type:'POST',
                      data :
                      {
                       'mobile':mobile,
                       'password':oldPasword
                      },
                      dataType:'html',
                      success  :function(data)
                      {
                        var json = $.parseJSON(data);
                        if (json.status)  {
                          $('#error_oldPasword').text('');
                          $('#password').prop("disabled",false);
                          $('#password').removeClass("readonly");
                          $('#repassword').prop("disabled",false);
                          $('#repassword').removeClass("readonly");
                          $('#change').prop("disabled",false);
                          $('#password').focus();
                        }else{
                          $('#error_oldPasword').text('Incorrect old password');
                          $('#error_password').text('');
                          $('#error_repassword').text('');
                          $('#oldPasword').focus();
                          $('#password').prop("disabled",true);
                          $('#repassword').prop("disabled",true);
                          $('#change').prop("disabled",true);
                          $('#password').val('');
                          $('#repassword').val('');
                        }
                      }
                    }); 
                }else{
                  $('#error_oldPasword').hide();
              }

            });

            $('#change').click(function(){

              var password = $('#password').val();
              var repassword = $('#repassword').val();
              var mobile = $('#mobile').val();
              var user_admin_id ="<?php echo $_SESSION['user_admin_id']; ?>";
              if(password!='' && repassword!=''){
                var urlForgetPass = userUrl+'forgetPassword.php';

                if (password == repassword) {
                  $.ajax({
                    url :urlForgetPass,
                    type:'POST',
                    data :
                    {
                      'user_admin_id':user_admin_id,
                      'mobile':mobile,
                      'password':password,
                      'repassword':repassword
                    },
                    dataType:'html',
                    success  :function(data)
                    {
                      var json = $.parseJSON(data);

                      if (json.status) {
                        $.alert({
                          icon: 'fa fa-smile-o',
                          theme: 'modern',
                          title: 'Success !',
                          content: "<p style='font-size:0.8em;'>Password changed successfully</p>",
                          type: 'green',
                          buttons: {
                            Ok: function () {
                                window.location='vehicle-list';
                            }
                          }
                        });
                      }else{
                        $.alert({
                          icon: 'fa fa-frown-o',
                          theme: 'modern',
                          title: 'Error !',
                          content: "<p style='font-size:0.8em;'>Something went wrong</p>",
                          type: 'red'
                        });
                      }
                    }
                  });  
                }else{
                  $('#error_repassword').text('Password & confirm password mismatch');
                  //$('#repassword').focus();
                }
              }else{
                //alert(password);
                if (password == '') {
                  $('#error_password').text('Password is required');
                }
                if (repassword == '') {
                  $('#error_repassword').text('Confirm password is required'); 
                }              
              }

            });


            $("#password").blur(function () {
              var password=$('#password').val();
              if(password==''){
                $('#error_password').text('Password is required');
                return false;

              }else if (password.length < 5){
                $('#error_password').text('Password should be minimum 5 charecter long');
                return false;
              }else{
                $('#error_password').text('');
              }

            });

            $("#repassword").blur(function () {
              var repassword=$('#repassword').val();
              var password=$('#password').val();
              if(repassword==''){
              $('#error_repassword').text('Confirm password is required ');

              }else if(repassword != password){
                $('#error_repassword').text('Password & confirm password mismatch');
              }else{
                $('#error_repassword').text('');
              }

            });

        } );
    </script>