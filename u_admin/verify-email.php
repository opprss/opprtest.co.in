<!-- header position -->
<?php
include "all_nav/header.php"; 

if (isset($_SESSION['user_admin_id'])) {

  $user_admin_id = $_SESSION['user_admin_id'];
  $response = array();

  $response = user_profile_show($user_admin_id);
  $response = json_decode($response, true);

  if (!empty($response)) {
    $email = $response['user_email']; 
    
  }

}
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: gray;

  }
  .success{
    font-size: 11px;
    color: green;
  }
  .user-lebel{
    font-size: 11px !important;
    text-transform: uppercase;
  }
</style>
<!-- header position -->


    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Verify Your Email</h5>
        <!-- <form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form> search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card single pd-20 pd-sm-40 col-md-12">
          <!-- <h6 class="card-body-title">Sign Up Here</h6>
          <p class="mg-b-20 mg-sm-b-30">A form with a label on top of each form control.</p> -->
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">

            <div class="row">
              <div class="col-md-12">
                
                  <form id="emailverify" method="post" action="">
                    <div class="form-layout">
                      <div class="row mg-b-0">
                        <div class="col-lg-3 error_show">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">email <span class="tx-danger">*</span></label>

                            <input type="email" class="form-control readonly" placeholder="Enter your email" name="email" id="email" value="<?php echo $email; ?>" style="background-color: transparent;" readonly="readonly">
                            <span style="position: absolute;" id="error_email" class="error_size"></span>
                          </div>
                        </div>

                        <div class="col-lg-2">
                          <div class="form-group">
                            <label class="form-control-label user-lebel">Enter OTP <span class="tx-danger">*</span></label>

                            <input type="text" class="form-control" placeholder="Enter your OTP" name="otp" id="otp" maxlength="6" style="background-color: transparent;" disabled="disabled">
                            <span style="position: absolute; color:red" id="msg_otp" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-lg-2">
                          <div class="form-group">
                            <button type="button" class="btn btn-block wl_user_button" name="send_otp" id="send_otp">SEND OTP</button>
                          </div>
                        </div>
                        <div class="col-lg-1">
                        </div>
                        <?php
                        //print_r($response) ;
                        //echo $user_admin_id;
                          if (isset($_SESSION['error'])) {
                            echo "<p style=color:red>".$_SESSION['error']."</p>";
                            /*unset($_SESSION['login_error_msg'];*/
                            unset($_SESSION['error']);
                          }
                        ?>
                        <div class="col-lg-2 error_show">
                          <div class="form-group">
                            <button type="button" class="btn btn-block wl_user_button" name="verify" id="verify" disabled="disabled">Verify</button>
                            <!--  -->
                          </div>
                        </div>
                        <div class="col-lg-2 error_show">
                          <div class="form-group">
                            <button type="button" class="btn btn-block wl_user_button_skip" name="skip" id="skip" onclick="document.location.href='profile';">BACK</button>
                          </div>
                        </div>                        
                    </div><!-- form-layout -->
                  </form>
              </div>
              
            </div>
          </div>
        </div><!-- card -->
        <!-- /add employee form -->
        <!-- <span class="pd-20"></span> -->

      </div><!-- am-pagebody -->
      

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">
  var userUrl = "<?php echo USER_URL; ?>";
  //var prkUrl = "<?php //echo PRK_URL; ?>";

  /*validation*/
  $( document ).ready( function () {
// alert('hi');
    $('#send_otp').click(function () {

      var email = $('#email').val();
      var name = "<?php echo $user_mobile ?>";
      var mobile = "";
      
          
      if(email!=''){

        //alert("CHECKED AND CLICK OTP BTN");
        //alert('mobile-'+mobile+'--username-'+name+'--email-'+email);
        var urlOtpSend = userUrl+'otpSend.php';
        //alert(urlOtpSend);
        $.ajax({
            url :urlOtpSend,
            type:'POST',
            data :
            {
              'mobile':mobile,
              'name':name,
              'email':email,
              'token':"<?php echo $token?>"
            },
            dataType:'html',
            success  :function(data)
            {
              //alert(data);
              var json = $.parseJSON(data);

                if (json.status) {
                  //window.location='signin.php';
                  $('#otp').prop("disabled",false);
                  $('#otp').focus();
                  $('#msg_otp').html('OTP Sent, Please check');
                   $('#send_otp').prop("disabled",true);

                 /* var myVar = setInterval(myTimer, 20000);
                  function myTimer() {
                      var d = new Date();
                      //$('#resend_otp').removeAttr('disabled');
                      $('#send_otp').prop("disabled",true);
                  }*/
                }else{
                  if( typeof json.session !== 'undefined'){
                    if (!json.session) {
                      window.location.replace("logout.php");
                    }
                  }else{
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                    }
                }
            }
        }).responseText;

      }else{
        $('#error_email').text('Please enter email ');
      }
    });

           
    $('#otp').keyup(function(){
       var ch = $('#otp').val().length;
         var i = 0;
          if (ch == 6) {
            $('#verify').prop("disabled",false);
            //$('#verify').show();
          }
    });
// ------------------
$('#verify').click(function () {

      var email = $('#email').val();
      var mobile = "";
      var otp = $('#otp').val();
      //alert('otp validation--mobile-'+mobile+'--name-'+name+'--email-'+email+'--otp--'+otp);
      var urlOtpVal = userUrl+'otpVal.php';
      var urlEmailVeryfi = userUrl+'email_verify.php';

      <?php echo $_SESSION['user_admin_id'] ?>
   // alert(urlOtpVal);
      $.ajax({
          url :urlOtpVal,
          type:'POST',
          data :
          {
            'mobile':mobile,
            'otp':otp,
            'email':email,
            'token': '<?php echo $token;?>'
          },
          dataType:'html',
          success  :function(data)
          {
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status) {
              // alert("update korte hobe");
              var email = $('#email').val();
              var user_admin_id = "<?php echo $_SESSION['user_admin_id'] ?>";
             //second ajax 
               $.ajax({
                url :urlEmailVeryfi,
                type:'POST',
                data :
                {
                  'email':email,
                  'user_admin_id':user_admin_id,
                  'token': '<?php echo $token;?>'
                },
                dataType:'html',
                success  :function(data)
                {
                // alert("verify");
                 // alert(data);
                 var json = $.parseJSON(data);
                 //alert(json);
                  if (json.status) {
                  // alert("ok");
                    //window.location='signin.php';
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success !',
                      content: "<p style='font-size:0.8em;'>Email verified sucessfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                            window.location='profile';
                             $('#msg_otp').html(' ');
                        }
                      }
                    });
                  }else{
                      if( typeof json.session !== 'undefined'){
                        if (!json.session) {
                          window.location.replace("logout.php");
                        }
                      }else{
                        $.alert({
                          icon: 'fa fa-frown-o',
                          theme: 'modern',
                          title: 'Error !',
                          content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                          type: 'red'
                        });
                      }
                  }
                }
              });
             //end
            }else{
              if( typeof json.session !== 'undefined'){
                if (!json.session) {
                  window.location.replace("logout.php");
                }
              }else{
                $('#msg_otp').html('OTP Not Matched');
              }
            }
          }
      });

         // i = i+1;
        //} 
    });






// -----------------------
    

});
  /* manoranjan*/
  
  
  setInterval(function() {
  var d = new Date();
    $('#send_otp').prop("disabled",false);
}, 25000);

</script>