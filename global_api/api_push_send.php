<?php
/*
Description: Notification Android App.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function push_send($title,$message,$push_type,$include_image,$regId,$payload,$clickaction,$oth1,$oth2,$oth3){
    $firbase_api_kay=($oth1=='EMP')?'AIzaSyDen_6pWtoh3E54rzkseGvVkSHGA4DgLLo':'AIzaSyCh4g8LX_0eRd_wIXxjUs285364-itrgB8';
    // define('FIREBASE_API_KEY', $firbase_kay);
    // if ($oth1=='EMP') {
        # Employee App
        // define('FIREBASE_API_KEY', 'AIzaSyDen_6pWtoh3E54rzkseGvVkSHGA4DgLLo');
    /*  }else{
        # User App
        define('FIREBASE_API_KEY', 'AIzaSyCh4g8LX_0eRd_wIXxjUs285364-itrgB8');
    }*/
    error_reporting(-1);
    ini_set('display_errors', 'On');
    $firebase = new Firebase();
    $push = new Push();
    // optional payload
    $include_img = $include_image;
    $include_image = empty($include_image) ? FALSE : TRUE;
    $push->setTitle($title);
    $push->setMessage($message);
    if ($include_image) {
        $push->setImage($include_img);
        // $push->setImage('http://api.androidhive.info/images/minion.jpg');
    } else {

        $push->setImage('');
    }
    $push->setIsBackground(FALSE);
    $push->setPayload($payload);
    $push->clickAction($clickaction);
    $json = '';
    $response = '';
    if ($push_type == 'topic') {
        $json = $push->getPush();
        return $response = $firebase->sendToTopic('global', $json,$firbase_api_kay);
    } else if ($push_type == 'individual') {
        $json = $push->getPush();
        return $response = $firebase->send($regId, $json,$firbase_api_kay);
    }
}
class Firebase {
    public function send($to, $message, $firbase_api_kay) {
        $fields = array(
            'to' => $to,
            'data' => $message,
        );
        return $this->sendPushNotification($fields, $firbase_api_kay);
    }
    public function sendToTopic($to, $message, $firbase_api_kay) {
        $fields = array(
            'to' => '/topics/' . $to,
            'data' => $message,
        );
        return $this->sendPushNotification($fields, $firbase_api_kay);
    }
    /*public function sendMultiple($registration_ids, $message) {
        $fields = array(
            'to' => $registration_ids,
            'data' => $message,
        );

        return $this->sendPushNotification($fields);
    }*/
    private function sendPushNotification($fields, $firbase_api_kay) {
        $url = 'https://fcm.googleapis.com/fcm/send';
        // define('FIREBASE_AI_KEY', 'AIzaSyCZy2efY1j8A3QmTm79OjJFcVyUfcqN9GM');
        // define('FIREBASE_API_KEY', 'AIzaSyCh4g8LX_0eRd_wIXxjUs285364-itrgB8');
        // define('FIREBASE_API_KEY', 'AIzaSyDen_6pWtoh3E54rzkseGvVkSHGA4DgLLo');
        $headers = array(
            'Authorization: key='.$firbase_api_kay,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);

        return $result;
    }
}
class Push {
    // push message title
    private $title;
    private $message;
    private $image;
    // push message payload
    private $data;
    // flag indicating whether to show the push
    // notification or not
    // this flag will be useful when perform some opertation
    // in background when push is recevied
    private $is_background;

    function __construct() {
        
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function setImage($imageUrl) {
        $this->image = $imageUrl;
    }

    public function setPayload($data) {
        $this->data = $data;
    }

    public function clickAction($clickaction) {
        $this->click_action = $clickaction;
    }

    public function setIsBackground($is_background) {
        $this->is_background = $is_background;
    }

    public function getPush() {
        $res = array();
        $res['data']['title'] = $this->title;
        $res['data']['is_background'] = $this->is_background;
        $res['data']['message'] = $this->message;
        $res['data']['image'] = $this->image;
        $res['data']['payload'] = $this->data;
        $res['data']['click_action'] = $this->click_action;
        $res['data']['timestamp'] = date('Y-m-d G:i:s');
        return $res;
    }
}
?>