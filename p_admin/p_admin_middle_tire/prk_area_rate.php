<?php  
/*
Description: Parking area vehicle rate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
//require_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('prk_vehicle_type','prk_admin_id','prk_rate_type','prk_ins_no_of_hr','prk_ins_hr_rate','prk_no_of_mt_dis','prk_rate_eff_date','prk_rate_end_date','user_name','tot_prk_space','veh_rate_hour','veh_rate','token'))){
 	if(isEmpty(array('prk_vehicle_type','prk_admin_id','prk_rate_type','prk_rate_eff_date','user_name','tot_prk_space','token'))){

        if (isNumeric(array('prk_ins_no_of_hr','prk_ins_hr_rate'))) {
            $prk_vehicle_type = trim($_POST['prk_vehicle_type']);
            $prk_rate_type = trim($_POST['prk_rate_type']);
            $prk_ins_no_of_hr = trim($_POST['prk_ins_no_of_hr']);
            $prk_admin_id = trim($_POST['prk_admin_id']);
            $prk_ins_hr_rate = trim($_POST['prk_ins_hr_rate']);
            $prk_rate_eff_date = trim($_POST['prk_rate_eff_date']);
            $prk_rate_end_date = trim($_POST['prk_rate_end_date']);
            $user_name = trim($_POST['user_name']);
            $prk_no_of_mt_dis = trim($_POST['prk_no_of_mt_dis']);
            $tot_prk_space = trim($_POST['tot_prk_space']);
            $veh_rate_hour = trim($_POST['veh_rate_hour']);
            $veh_rate = trim($_POST['veh_rate']);
            $token = ($_POST['token']);

            $resp=prk_token_check($prk_admin_id,$token);
            $json = json_decode($resp);
            if($json->status){
                $respon= park_area_rate($prk_vehicle_type,$prk_rate_type,$prk_ins_no_of_hr,$prk_ins_hr_rate,$prk_no_of_mt_dis,$prk_rate_eff_date,$prk_admin_id,$prk_rate_end_date,$user_name,$tot_prk_space,$veh_rate_hour,$veh_rate);
                echo ($respon);
            }else{
                $response = $resp;
                echo $response;
            }
        }else{
            $response['status'] = 0;
            $response['message'] = 'Wrong Value Entered';
            echo json_encode($response);
        }
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    echo json_encode($response);
}
?>