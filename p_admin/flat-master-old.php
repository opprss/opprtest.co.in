<!-- ################################################
  
Description: Tower detalis.
Developed by: Rakhal Raj 
Created Date: 12-03-2018

 ####################################################-->
<?php include "all_nav/header.php";?>
<?php 
$crud_type='LS';
$flat_master=flat_master($prk_admin_id,'','','','','','','','',$crud_type,'');
// $flat_details=flat_details($prk_admin_id,$tower_name,$flat_name,$effective_date,$end_date,$parking_admin_name,$flat_id,$crud_type,$living_status);
$flat_master_list = json_decode($flat_master, true);

$tower_lis = tower_list($prk_admin_id);
$tower_list = json_decode($tower_lis, true);

$category_type='APT_FLT_HLD';
$prk_user_emp_cat = common_drop_down_list($category_type);
$prk_user_emp_category = json_decode($prk_user_emp_cat, true);
?>

<!-- header position -->
  <style type="text/css">
    .size{
      font-size: 11px;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .error{
      font-size: 11px;
      color: red;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .success{
      font-size: 11px;
      color: green;
    }
  </style>
  <!-- links for datatable -->
  <link href="../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
  
    <div class="am-mainpanel">

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Flat Master<//?=$flat_master?></h5>
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <div class="card pd-10 pd-sm-40" >
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form id="park_flat">
              <!-- <form role="form" id="park_flat" method="post"  enctype="multipart/form-data"> -->
              <div class="row mg-b-5">
                <div class="col-lg-3">
                  <div class="form-group">
                    <label class="form-control-label size">TOWER NAME <span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="tower_name" id="tower_name" style="opacity: 0.8;font-size:14px">
                        <option value="">Select Tower</option>
                        <?php if($tower_list['status']){
                          foreach($tower_list['tower_list'] as $value){?>
                          <option value="<?=$value['tower_id'];?>"><?=$value['tower_name'];?></option>
                        <?php }}?>
                      </select>
                    </div>
                  <span style="position: absolute; display: none;" id="error_tower_name" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label class="form-control-label size">FLAT NAME <span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="flat_name" id="flat_name" style="opacity: 0.8;font-size:14px">
                        <option value="">Select Flat</option>
                      </select>
                    </div>
                  <span style="position: absolute; display: none;" id="error_flat_name" class="error">This field is required.</span>
                  </div>
                </div>
                <!-- <div class="col-lg-6">
                  <div class="form-layout-footer mg-t-30">
                    <div class="row">
                      <div class="col-lg-6">
                        <input type="button" value="SHOW" class="btn btn-block btn-primary prk_button" name="ok" id="ok">
                      </div>
                      <div class="col-lg-6">
                        <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                      </div>
                    </div>
                  </div>
                </div> -->
              <!-- </div>
              <div style="display:" class="row mg-b-10"> -->
                <div class="col-lg-3">
                  <div class="form-group">
                    <label class="form-control-label size">OWNER NAME <span class="tx-danger">*</span></label>
                    <input type="text" name="owner_name" id="owner_name" placeholder="Owner Name" onchange="search_func(this.value)" class="form-control">
                    <input type="hidden" id="owner_id" name="owner_id">
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">LIVING STATUS<span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="living_status" id="living_status" style="opacity: 0.8;font-size:14px">
                        <option value="">Select Living</option>
                          <?php
                          foreach ($prk_user_emp_category['drop_down'] as $val){ ?>  
                            <option value="<?php echo $val['sort_name']?>"><?php echo $val['full_name']?></option>
                          <?php          
                          }
                        ?>
                      </select>
                    </div>
                  <span style="position: absolute; display: none;" id="error_living_status" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label class="form-control-label size">TENANT NAME <span class="tx-danger">*</span></label>
                    <input type="text" name="tenant_name" id="tenant_name" placeholder="Tenant Name" class="form-control" onchange="search_func_tent(this.value)">
                    <input type="hidden" id="tenant_id" name="tenant_id">
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">EFFECTIVE DATE<span class="tx-danger">*</span></label>
                    <input type="text" name="effective_date" id="effective_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                    <span style="position: absolute; display: none;" id="error_effective_date" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">END DATE</label>
                    <input type="text" name="end_date" id="end_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                   <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-layout-footer mg-t-30">
                    <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-layout-footer mg-t-30">
                     <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- data table -->
      <div class="am-pagebody">
        <div class="card pd-10 pd-sm-40">
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive">
              <thead>
                <th >Tower Name</th>
                <th >Flat Name</th>
                <th >Owner Name</th>
                <th >Living Status</th>
                <th >Tenant Name</th>
                <th >Effective Date</th>
                <th >End Date</th>
                <th >Action</th>
              </thead>
              <?php 
                if($flat_master_list['flat_mas_list']){
                  foreach($flat_master_list['flat_mas_list'] as $value){?>
                    <tr style="<?php if($value['activ_status']==FLAG_N){ ?>background-color: bisque;<?php }?>">
                    <td ><?=$value['tower_name'];?></td>
                    <td ><?=$value['flat_name'];?></td>
                    <td ><?=$value['owner_name'];?></td>
                    <!-- <td ><//?=$value['activ_status'];?></td> -->
                    <td ><?=$value['living_status_full'];?></td>
                    <td ><?=$value['tenant_name'];?></td>
                    <td ><?=$value['effective_date'];?></td>
                    <td ><?=$value['end_date'];?></td>
                    <td >
                      <a href="<?php echo 'flat-master-delails?list_id='.base64_encode($value['flat_master_id']);?>" class="pd-l-10" data-toggle="tooltip" data-placement="top" title="Modify"><i class="fa fa-pencil" style="font-size:18px; font-size:18px;"></i></a>
                      <button type="button" class="clean-button" name="delete" id="<?php echo $value['flat_master_id'];?>" data-toggle="tooltip" onclick="flat_mas_delete(this.id)" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
                    </td>
                    </tr>
                  <?php }
                }
              ?>
            </table>
          </div>
        </div>
      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#effective_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
        if (date < now) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        // $('#error_effective_date').text('');
        return {};
      } 
    });
  });
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#effective_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
        if (date < now_end) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
        }
        return {};
      } 
    });
  });
  $('#effective_date').focus(function(){
    $('#end_date').val('');
  });
</script>
<!-- scripts for datatable -->
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>

<script>
  // alert(prkUrl);
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      },
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
</script>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE ?>";
  var prk_admin_id="<?=$prk_admin_id?>"
  var parking_admin_name="<?=$parking_admin_name?>"
  var token="<?=$token?>"
  var urlPrkAreGateDtl = prkUrl+'flat_master.php';
  // alert(token);
  // $( document ).ready( function () {
  $('#save').click(function(){
    var effective_date=($("#effective_date").val());
    var end_date=($("#end_date").val());

    var str_array = end_date.split('-');
    var dd = str_array[0];
    var mm = str_array[1];
    var yy = str_array[2];
    var end_dat = yy+'-'+mm+'-'+dd;
    end_date = (!end_date == '') ? end_dat : FUTURE_DATE;
    // alert(end_date);
    var str_array = effective_date.split('-');
    var dd = str_array[0];
    var mm = str_array[1];
    var yy = str_array[2];
    var effective_dat = yy+'-'+mm+'-'+dd;
    var tower_name=($("#tower_name").val());
    var flat_name=($("#flat_name").val());
    var owner_name=($("#owner_id").val());
    var living_status=($("#living_status").val());
    var tenant_name=($("#tenant_id").val());
    var living_status=($("#living_status").val());
    var living_status=($("#living_status").val());
    var living_status=($("#living_status").val());
    if($("#park_flat").valid()&effective_date!=''){
      // alert('ok');
       $('#save').val('Wait ...').prop('disabled', true);
      $.ajax({
        url :urlPrkAreGateDtl,
        type:'POST',//'prk_admin_id','tower_name','flat_name','owner_name','living_status','tenant_name','effective_date','end_date','parking_admin_name','token','crud_type','flat_master_id'
        data :{
          'prk_admin_id':prk_admin_id,
          'tower_name':tower_name,
          'flat_name':flat_name,
          'owner_name':owner_name,
          'tenant_name':tenant_name,
          'living_status':living_status,
          'effective_date':effective_dat,
          'end_date':end_date,
          'parking_admin_name':parking_admin_name,
          'flat_master_id':'',
          'crud_type':'I',
          'token': token
        },
        dataType:'html',
        success  :function(data){
          $('#save').val('SAVE').prop('disabled', false);
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>"+json.message+"</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                    location.reload(true);
                }
              }
            });
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>"+json.message+"</p>",
              type: 'red'
            });
            }
          }
        }
      });
    }
  });
  function flat_mas_delete(flat_master_id){
    // alert(tower_id);
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            if (flat_master_id != '') {
                $.ajax({
                  url :urlPrkAreGateDtl,
                  type:'POST',
                  data :{
                    'prk_admin_id':prk_admin_id,
                    'tower_name':'',
                    'flat_name':'',
                    'owner_name':'',
                    'tenant_name':'',
                    'living_status':'',
                    'effective_date':'',
                    'end_date':'',
                    'parking_admin_name':parking_admin_name,
                    'flat_master_id':flat_master_id,
                    'crud_type':'D',
                    'token': token
                  },
                  dataType:'html',
                  success  :function(data)
                  {
                    // alert(data);
                    var json = $.parseJSON(data);
                    if (json.status){
                      $.alert({
                        icon: 'fa fa-smile-o',
                        theme: 'modern',
                        title: 'Success',
                        content: "<p style='font-size:0.9em;'>Delete successfully</p>",
                        type: 'green',
                        buttons: {
                          Ok: function () {
                            location.reload(true);
                          }
                        }
                      });
                    }else{
                       $.alert({
                        icon: 'fa fa-frown-o',
                        theme: 'modern',
                        title: 'Error !',
                        content: "<p style='font-size:0.7em;'>Somting went wrong 1</p>",
                        type: 'red'
                      });
                    }
                  }
                });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
</script>
<script type="text/javascript">
  $( document ).ready( function () {
    $( "#park_flat" ).validate( {
      rules: {
        flat_name: "valueNotEqualsflat",
        effective_date: "valueNotEqualsEff",
        tower_name: "valueNotEqualstower",
        owner_name: "required",
        living_status: "valueNotEqualsliving"
      },
      messages: {
      }
    });
    $("#effective_date").blur(function () {
      var effective_date = $('#effective_date').val();
      if(effective_date == ''){
        $('#error_effective_date').show();
        return false;
      }else{
        $('#error_effective_date').hide();
        return true;
      }
    });
    $("#tower_name").on('change', function(){
      var v_type = $('#tower_name').val();
      if(v_type == "")
      {
        $('#error_tower_name').show();
        return false;
      }else{
          $('#error_tower_name').hide();
          return true;
      }
    });
    $("#flat_name").on('change', function(){
      var v_type = $('#flat_name').val();
      if(v_type == "")
      {
        $('#error_flat_name').show();
        return false;
      }else{
          $('#error_flat_name').hide();
          return true;
      }
    });
    $("#living_status").on('change', function(){
      var v_type = $('#living_status').val();
      if(v_type == "")
      {
        $('#error_living_status').show();
        return false;
      }else{
          $('#error_living_status').hide();
          return true;
      }
    });
  });
  $.validator.addMethod("valueNotEqualsEff", function(value){
    // alert('ok');
    arg = "";
    if (value != "") {
      return arg !== value;
    }else{
      $("#error_effective_date").show();
      return true;
    }
  });
  $.validator.addMethod("valueNotEqualstower", function(value){
    // alert(value);
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_tower_name").show();
      return false;
    }
  });
  $.validator.addMethod("valueNotEqualsflat", function(value){
    // alert(value);
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_flat_name").show();
      return false;
    }
  });
  $.validator.addMethod("valueNotEqualsliving", function(value){
    // alert(value);
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_living_status").show();
      return false;
    }
  });
  /*jQuery.validator.addMethod("acceptww", function(value, element, param) {

    return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
  },'please enter a valid email');*/
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>
<script type="text/javascript">
  $('#tower_name').on("change", function() {
    var id=($("#tower_name").val());
    var urlCity = prkUrl+'flat_list.php';
    $.ajax ({
      type: 'POST',
      url: urlCity,
      data: {
        tower_id:id,
        prk_admin_id:prk_admin_id,
        parking_admin_name:parking_admin_name,
        token:token
      },
      success : function(data) {
        // alert(data);
        var obj=JSON.parse(data);
        var areaOption = "<option value=''>Select Flat</option>";
        // alert(obj['status']);
        if (obj['status']) {
          $.each(obj['flat_list'], function(val, key) {
            areaOption += '<option value="' + key['flat_id'] + '">' + key['flat_name'] + '</option>'
          });
        }
        $("#flat_name").html(areaOption);
      }
    });
  });
</script>
<!-- OWNER Search -->
<script type="text/javascript" src="../search/jquery.tokeninput.js"></script>
<link rel="stylesheet" href="../search/token-input.css" type="text/css" />
<link rel="stylesheet" href="../search/token-input-facebook.css" type="text/css" />

<script type="text/javascript">
  function search_func(str,t) {
    var xhttp;
    if (str.length == 0) {
      return;
    }
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var json = $.parseJSON(this.responseText);
          if (json.status) {
              $('#owner_name').val(json.emp_name);
              $('#owner_id').val(json.emp_id);
          }else{
              alert(json.message);
          }
      }
    };
    xhttp.open("GET", "p_admin_middle_tire/owner_list.php?q="+str+"&pid="+prk_admin_id+"&type=SE", true);
    xhttp.send();
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#owner_name').tokenInput("p_admin_middle_tire/owner_list.php?pid="+prk_admin_id+"&type=SO", {
      theme: "facebook",
      hintText: "Enter Code.",
      noResultsText: "No matching results.",
      preventDuplicates: true,
      searchingText: "Searching...",
      tokenLimit:1
    });
    $('#token-input-owner_name').attr('placeholder','Please type');
  });
</script>
<!-- Tenant Search -->
<script type="text/javascript">
  function search_func_tent(str,t) {
    var xhttp;
    if (str.length == 0) {
      return;
    }
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var json = $.parseJSON(this.responseText);
          if (json.status) {
              $('#tenant_name').val(json.emp_name);
              $('#tenant_id').val(json.emp_id);
          }else{
              alert(json.message);
          }
      }
    };
    xhttp.open("GET", "p_admin_middle_tire/tenant_list.php?q="+str+"&pid="+prk_admin_id+"&type=SE", true);
    xhttp.send();
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#tenant_name').tokenInput("p_admin_middle_tire/tenant_list.php?pid="+prk_admin_id+"&type=SO", {
      theme: "facebook",
      hintText: "Enter Code.",
      noResultsText: "No matching results.",
      preventDuplicates: true,
      searchingText: "Searching...",
      tokenLimit:1
    });
    $('#token-input-tenant_name').attr('placeholder','Please type');
  });
</script>
<!-- end Search -->

