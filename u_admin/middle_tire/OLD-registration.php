<?php 
/*
Description: User Registration.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
 require_once 'api/reg_api.php';
// include_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('mobile','password','repassword','name','email','dob','gender','v_type','v_number'))){
 	if(isEmpty(array('mobile','password','repassword','name','email','dob','gender','v_type','v_number'))){

	 	$mobile = trim($_POST['mobile']);
	 	$password = trim($_POST['password']);
	 	$repassword = trim($_POST['repassword']);
	 	$name = trim($_POST['name']);
	 	$email = trim($_POST['email']);
	 	$dob = trim($_POST['dob']);
	 	$gender = trim($_POST['gender']);
	 	$v_type = trim($_POST['v_type']);
	 	$v_number = trim($_POST['v_number']);
        
        $respon=registration($mobile,$password,$repassword,$email,$name,$dob,$gender,$v_number,$v_type);
        echo ($respon);
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?>