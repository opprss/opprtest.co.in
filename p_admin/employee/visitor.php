<!-- ################################################
Description: Parkig admin can register(login ID and password)/delete/change password/activate-deactivate there employee(security gurd) for there gate managment. with this login credentials parking area employee can lgin intu the parking gate managment mobile app.
Developed by: Rakhal RAj Mandal
Created Date: 17-03-2018
 ####################################################-->
<?php 
  include "all_nav/header.php";
  if ($visitor_token_flag!='Y') {
    // $caallmd=3;
    $caallmd=2;
  }else{
    $caallmd=2;
  }
  $meet_to_person_list=meet_to_person_list($prk_admin_id);
  $mtpl = json_decode($meet_to_person_list, true);
?>
  <style type="text/css">
   .select2 .select2-container .select2-container--default{
      width: 307px !important;
    }
    .select2 {
      width:100%!important;
    }
    .size{
      font-size: 11px;
      text-transform: uppercase;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
  </style>
  <script src="../../js/webcam.min.js"></script>
  <link href="../../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
  <link href="../../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
  <link href="../../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
  <div class="am-mainpanel">
    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Visitor</h5>
    </div>
  </div>
  <div class="am-mainpanel">
    <div class="am-pagebody" style="<?php if(isset($prk_area_gate_type)){
      if ($prk_area_gate_type == "O") { echo "display: none"; }}?>">
      <div class="card pd-20 pd-sm-40" style="padding: 10px;">        
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <form id="addVehicleForm" action="#" method="post">
            <input type="hidden" name="unique_id" id="unique_id">
            <input type="hidden" name="in_verify" id="in_verify">
            <input type="hidden" name="out_verify" id="out_verify">
            <input type="hidden" name="tower_id" id="tower_id">
            <input type="hidden" name="tower_name" id="tower_name">
            <input type="hidden" name="flat_id" id="flat_id">
            <input type="hidden" name="flat_name" id="flat_name">
            <div class="row">
              <!-- <input type="text" class="form-control" placeholder="ADDRESS" name="address" id="address"> -->
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">MOBILE NUMBER <span class="tx-danger">*</span></label>
                  <input type="text" name="mobile" id="mobile" maxlength="10" class="form-control" placeholder="Mobile Number" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"/>
                  <span style="position: absolute;" id="error_mobile" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">Visitor Type <span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="visitor_type" id="visitor_type" value="" style="opacity: 0.8; font-size:14px">
                      </select>
                    </div>
                    <span id="error_visitor_type" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">VISITOR NAME <span class="tx-danger">*</span></label>
                  <input type="text" class="form-control" placeholder="VISITOR NAME" name="visitor_name" id="visitor_name">
                  <span style="position: absolute;" id="error_visitor_name" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-1">
                <div class="form-group">
                  <label class="form-control-label size">GENDER <span class="tx-danger">*</span></label>
                    <label class="rdiobox">
                      <input name="gender" id="male" value="M" type="radio">
                      <span style="font-size: 11px;">MALE </span>
                    </label>

                    <label class="rdiobox">
                      <input name="gender" id="female" value="F" type="radio">
                      <span style="font-size: 9px;">FAMALE </span>
                    </label>
                  <span style="position: absolute; margin-top: -12px;" id="error_gender" class="error_size"></span>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <div id="my_camera"></div><br/>
                </div>
              </div>
              <div class="col-md-1">
                <div class="form-group">
                  <button type="button" class="btn btn-block prk_button" onClick="take_snapshot()"><p style="margin-bottom: 0px;font-size: 11px;margin-left: -3px;">Capture<br>Image</p></button>
                  <button type="button" class="btn btn-block prk_button" id="my-button"><p style="margin-bottom: 0px;font-size: 11px;margin-left: -2px;">Upload<br>Image</p></button>
                  <input type="file" name="visitor_img" accept="image/*" id="visitor_img" style="visibility: hidden;">
                  <input type="hidden" name="vis_image" class="vis_image" id="vis_image">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <div id="results">captured Visitor image will appear here...</div>
                  <!-- <span  id="error_vis_image" class="error_size"></span> -->
                  <label style="position: absolute;" class="error_size" id="error_vis_image"></label>
                </div>
              </div>
            </div>
            <div class="row" style="margin-top: -45px;">
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">ADDRESS <span class="tx-danger"></span></label>
                  <input type="text" class="form-control" placeholder="ADDRESS" name="address" id="address">
                  <span style="position: absolute;" id="error_address" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">PURPOSE <span class="tx-danger"></span></label>
                  <input type="text" class="form-control" placeholder="PURPOSE" name="purpose" id="purpose">
                  <span style="position: absolute;" id="error_purpose" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">ID PROOF</label>
                  <input type="text" class="form-control" placeholder="ID PROOF" name="id_proof" id="id_proof">
                  <span style="position: absolute;" id="error_id_proof" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-2" style="display: <?=($parking_type=='R')?'':'none'?>;">
                <div class="form-group">
                  <label class="form-control-label size">Tower Name<span class="tx-danger">*</span></label>
                    <!-- <div class="select"> -->
                      <select name="tower_id_list" id="tower_id_list" value="" style="opacity: 0.8; font-size:14px">
                        <option value="">Select Tower</option>
                      </select>
                    <!-- </div> -->
                </div>
                <span id="error_tower_id_list" style="position: absolute; margin-top: -13px;" class="error_size"></span>
              </div>
              <div class="col-lg-2" style="display: <?=($parking_type=='R')?'':'none'?>;">
                <div class="form-group">
                  <label class="form-control-label size">Flate Name<span class="tx-danger">*</span></label>
                    <!-- <div class="select"> -->
                      <select name="flat_id_list" id="flat_id_list" value="" style="opacity: 0.8; font-size:14px">
                        <option value="">Select Flat</option>
                      </select>
                    <!-- </div> -->
                </div>
                <span id="error_flat_id_list" style="position: absolute; margin-top: -13px;" class="error_size"></span>
              </div>
              <div class="col-lg-2" style="display: <?=($parking_type=='C')?'':'none'?>;">
                <div class="form-group">
                  <label class="form-control-label size">MEET TO NAME<span class="tx-danger">*</span></label>
                    <!-- <div class="select"> -->
                      <select name="meet_to_name_list" id="meet_to_name_list" value="" style="opacity: 0.8; font-size:14px">
                        <option value="">Select Name</option>
                        <?php if (isset($mtpl['status'])) {
                          foreach ($mtpl['meet_to_person_list'] as $key => $value) {?>
                            <option value="<?=$value['me_to_name_mobile']?>"><?=$value['me_to_per_name']?></option>
                        <?php }}?>
                      </select>
                    <!-- </div> -->
                </div>
                <span id="error_meet_to_name_list" style="position: absolute; margin-top: -13px;" class="error_size"></span>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">MEET TO MOBILE <span class="tx-danger"></span></label>
                  <input type="text" class="form-control" placeholder="MEET TO" name="meet_to" id="meet_to" maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                  <span style="position: absolute;" id="error_meet_to" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-<?php echo $caallmd ?>" id="mee_name_div">
                <div class="form-group" >
                  <label class="form-control-label size">MEET TO NAME</label>
                  <input type="text" class="form-control" placeholder="Meet To Name" name="meet_to_name" id="meet_to_name">
                  <span style="position: absolute;" id="error_meet_to_name" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-<?php echo $caallmd ?>" id="me_add_div">
                <div class="form-group">
                  <label class="form-control-label size">MEET TO ADDRESS <span class="tx-danger"></span></label>
                  <input type="text" class="form-control" placeholder="Meet To Address" name="meet_to_add" id="meet_to_add">
                  <span style="position: absolute;" id="error_meet_to_add" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-2" id="me_add_div" <?php if ($visitor_token_flag!='Y') { ?>style="display: none;" <?php }?>>
                <div class="form-group">
                  <label class="form-control-label size">VISITOR TOKEN<span class="tx-danger">*</span></label>
                  <input type="text" class="form-control" placeholder="Visitor Token" name="visitor_token" id="visitor_token" maxlength="6">
                  <span style="position: absolute;" id="error_visitor_token" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label size">VISIT WITH VEHICLE <span class="tx-danger">*</span></label>
                    <label class="rdiobox">
                      <input name="visit_with_vehicle" id="yes" value="Y" type="radio">
                      <span>YES </span>
                    </label>

                    <label class="rdiobox">
                      <input name="visit_with_vehicle" id="no" value="N" type="radio" checked>
                      <span>NO </span>
                    </label>
                </div>
              </div>
              <div class="col-lg-2 veh_with" style="display: none;">
                <div class="form-group visit_with_vehicle_yes" >
                  <label class="form-control-label size">VEHICLE TYPE <span class="tx-danger">*</span></label>
                    <div class="select">
                      <select name="vehicle_type" id="vehicle_type" value="" style="opacity: 0.8; font-size:14px">
                      </select>
                    </div>
                    <span id="error_vehicle_type" style="position: absolute;" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-2 veh_with" style="display: none;">
                <div class="form-group visit_with_vehicle_yes">
                  <label class="form-control-label size">VEHICLE NUMBER <span class="tx-danger">*</span></label>
                  <input type="text" class="form-control" placeholder="Vehicle Number" name="veh_number" id="veh_number" onkeyup="vehNoUppercase();" maxlength="10">
                  <span style="position: absolute;" id="error_veh_number" class="error_size"></span>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-layout-footer mg-t-0">
                  <div class="pd-t-10 mg-t-30">
                    <input type="button" value="IN" class="btn btn-block btn-primary prk_button" name="visotor_in" id="visotor_in">
                  </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-layout-footer mg-t-0">
                  <div class="pd-t-10 mg-t-30">
                    <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="am-pagebody" id="data">
      <div class="card pd-20 pd-sm-40" style="padding: 10px;">
        <div class="table-wrapper">
          <table id="datatable1" class="table display responsive nowrap" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="wd-10p">Image</th>
                <th class="wd-15p">name</th>
                <th class="wd-15p">mobile</th>
                <th class="wd-10p">Visitor Token</th>
                <th class="wd-15p">In Verify Status</th>
                <th class="wd-10p">In date/Time</th>
                <th class="wd-20p text-center">Action</th>
              </tr>
            </thead>
            <tbody id="list_vis"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <?php include"all_nav/footer.php"; ?>
  <script src="../../lib/highlightjs/highlight.pack.js"></script>
  <script src="../../lib/datatables/jquery.dataTables.js"></script>
  <script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
  <script src="../../lib/select2/js/select2.full.min.js"></script>
  <script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
  <script src="../../lib/datatables-responsive/jquery.dataTables.min.js"></script>
  <script src="../../lib/datatables-responsive/dataTables.buttons.min.js"></script>
  <script src="../../lib/datatables-responsive/jszip.min.js"></script>
  <script src="../../lib/datatables-responsive/pdfmake.min.js"></script>
  <script src="../../lib/datatables-responsive/vfs_fonts.js"></script>
  <script src="../../lib/datatables-responsive/buttons.html5.min.js"></script>
  <script type="text/javascript">
    var pkrEmpUrl = "<?php echo PRK_EMP_URL; ?>";
    var falgY = "<?php echo FLAG_Y; ?>";
    var prk_admin_id = "<?php echo $prk_admin_id; ?>";
    var prk_area_user_id = "<?php echo $prk_area_user_id; ?>";
    var prk_area_gate_type = "<?php echo $prk_area_gate_type; ?>";
    var vis_prient_in_flag = "<?php echo $vis_prient_in_flag; ?>";
    var vis_prient_out_flag = "<?php echo $vis_prient_out_flag; ?>";
    var token = "<?php echo $token; ?>";
    var visitor_token_flag = "<?php echo $visitor_token_flag; ?>";
    $('input[type="radio"][name="visit_with_vehicle"]').click(function(){
      if ($(this).is(':checked'))
      {
        var value=$(this).val();
        veh_with_show(value);
      }
    });
    $( document ).ready( function () {
      $('#meet_to_name').attr("disabled", true);
      $('#meet_to_add').attr("disabled", true);
      $('#visotor_in').attr("disabled", true);
      visitor_list();
      /*setInterval(function(){
        var input = $('#myInput').val();
        if(input==''){
          visitor_list();
        }
      }, 5000);*/
      var urlVehicleType = pkrEmpUrl+'vehicle_type.php';
      $('[data-toggle="tooltip"]').tooltip(); 
      $.ajax ({
        type: 'POST',
        url: urlVehicleType,
        data: "",
        success : function(data) {
          //alert(data);
          var obj=JSON.parse(data);
          var areaOption = "<option value='' style='color:gray;' selected='selected'>VEHICLE TYPE</option>";
          //alert(obj['vehicle'][0][])
          $.each(obj, function(val, key) {
          // alert("Value is :" + key['e']);
          areaOption += '<option value="' + key['vehicle_Sort_nm'] + '">' + key['vehicle_type'] + '</option>'
          });
          $("#vehicle_type").html(areaOption);
        }
      });
      var visitor_type_list = pkrEmpUrl+'visitor_type_list.php';
      $.ajax ({
        type: 'POST',
        url: visitor_type_list,
        data: {},
        success : function(data) {
          // alert(data);
          var obj=JSON.parse(data);
          var areaOption = "";
          $.each(obj['type_list'], function(val, key) {
            areaOption += '<option value="' + key['sort_name'] + '">' + key['full_name'] + '</option>'
          });
          $("#visitor_type").html(areaOption);
        }
      });
      $("#vehicle_type").blur(function () {
        var veh_type = $('#vehicle_type').val();
        
        if(veh_type==''){
          $('#error_vehicle_type').text('Vehicle type is required');
        }else{
          $('#error_vehicle_type').text('');;
        }
      });
      $("#veh_number").blur(function(){
        var veh_number = $('#veh_number').val();
        IsVehNumber(veh_number);
      });
      function IsVehNumber(veh_number) {
        var urlVehNoCheck = pkrEmpUrl+'v_numberChecker.php';
        $.ajax({
          url :urlVehNoCheck,
          type:'POST',
          data :
          {
            'v_number':veh_number,
            'prk_admin_id':prk_admin_id
          },
          dataType:'html',
          success  :function(data)
          {
            var json = $.parseJSON(data);
            vehNumberReturn(json.status);
          }
        });
      } 
      function vehNumberReturn(flag){
        if(!flag){
          $('#error_veh_number').text('Vehicle number is invalid');
          //return false;
        }else{
          $('#error_veh_number').text('');
          //return true;
        }
      }
      $('#reset').click(function (){
        // alert('ok');
        $("#tower_id_list").val(null).trigger('change');
        $('#tower_id_list').attr("disabled", false);
        $("#flat_id_list").val(null).trigger('change');
        $('#flat_id_list').attr("disabled", false);
        $("#meet_to_name_list").val(null).trigger('change');
        $('#meet_to_name_list').attr("disabled", false);

        $('.error_size').text('');
        $("#addVehicleForm :input").prop("disabled", false);
        $("#meet_to_name").prop("disabled", true);
        $("#meet_to_add").prop("disabled", true);
      });
      $('#visotor_in').click(function () {
        var visit_with_veh = $('input[name=visit_with_vehicle]:checked').val();
        var visit_card_id = $('#id_proof').val();
        var veh_type = $('#vehicle_type').val();
        var veh_number = $('#veh_number').val();
        if (visit_with_veh == falgY) {
          if(veh_type!='' && veh_number!=''){
            var urlVehNoCheck = pkrEmpUrl+'v_numberChecker.php';
            $.ajax({
              url :urlVehNoCheck,
              type:'POST',
              data :{
                'v_number':veh_number,
                'prk_admin_id':prk_admin_id
              },
              dataType:'html',
              success  :function(data){
                //alert(data);
                var json = $.parseJSON(data);
                //alert(json.status);
                if (json.status) {
                    ajaxCall();
                  }else{
                    $('#error_veh_number').text('Vehicle number is invalid');
                  }
              }
            });
          }else{
            if (veh_type == '') {
              $('#error_vehicle_type').text('Vehicle type is required');
            }if (veh_number == '') {
              $('#error_veh_number').text('Vehicle number is required');
            }
          }
        }else{
          ajaxCall();
        }
      });
      $("#veh_number").on('input', function(evt) {
        var input = $(this);
        var start = input[0].selectionStart;
        $(this).val(function (_, val) {
          return val.toUpperCase();
        });
        input[0].selectionStart = input[0].selectionEnd = start;
      }); 
      $("#visitor_name").blur(function () {

        visitor_name_val();
      });
      $('#mobile').blur(function() {

        mobile_val();
      });
      $("#meet_to").blur(function () {

        meet_to_val();
      });
      $("#visitor_token").blur(function () {

        visitor_token_val();
      });
      $('input[name=gender]').change(function(){

        gender_val();
      });
      $("#mobile").keyup(function () {
        var mobile = $('#mobile').val();
          gatepass_show(mobile);
      });
    });
    function meet_to_tower_flat() {
      var visitor_type = $('#visitor_type').val();
      var tower_id=($("#tower_id_list").val());
      var flat_id=($("#flat_id_list").val());
      var urlmeet_to_user = pkrEmpUrl+'meet_to_user.php';
      // alert(urlmeet_to_user);
      $.ajax({
        url :urlmeet_to_user,
        type:'POST',
        data :{
          'meet_mobile':'',
          'tower_id':tower_id,
          'flat_id':flat_id,
          'meet_to_status':'W',
          'prk_admin_id':prk_admin_id
        },
        dataType:'html',
        success  :function(data){
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $('#tower_id').val(json.tower_id);
            $('#tower_name').val(json.tower_name);
            $('#flat_id').val(json.flat_id);
            $('#flat_name').val(json.falt_name);
            $('#meet_to_name').val(json.user_name);
            $('#meet_to_add').val(json.me_address);
            $('#meet_to').val(json.meet_mobile);
            $('#visotor_in').attr("disabled", false);
          }else{
            $('#tower_id').val('');
            $('#tower_name').val('');
            $('#flat_id').val('');
            $('#flat_name').val('');
            $('#meet_to_name').val('');
            $('#meet_to_add').val('');
            $('#meet_to').val('');
            if(visitor_type=='O' || visitor_type=='U'){
              $('#visotor_in').attr("disabled", true);
            }else{
              $('#visotor_in').attr("disabled", false);
            }
          }
        }
      });
    }
    function meet_to_name_search() {
      var visitor_type = $('#visitor_type').val();
      var id123=$("#meet_to_name_list").val();
      if (id123!='') {
        var res = id123.split("||");
        $('#meet_to_name').val(res[0]);
        $('#meet_to').val(res[1]);
        $('#meet_to_add').val(res[2]);
        $('#visotor_in').attr("disabled", false);
      }else{
        $('#meet_to_name').val('');
        $('#meet_to').val('');
        $('#meet_to_add').val('');
        if(visitor_type=='O' || visitor_type=='U'){
          $('#visotor_in').attr("disabled", true);
        }else{
          $('#visotor_in').attr("disabled", false);
        }
      }
    }
    function gatepass_show(mobile){
      var length= mobile.toString().length;
      var visit_with_veh='N';
      // alert(length);
      if(length>=10){
        var urlgate_pass_find = pkrEmpUrl+'visitor_gate_pass_find.php';
        $.ajax({
          url :urlgate_pass_find,
          type:'POST',
          data :{
            'visitor_mobile':mobile,
            'visitor_type':'',
            'prk_admin_id':prk_admin_id
          },
          dataType:'html',
          success  :function(data){
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status){
              $('.error_size').text('');
              // alert(json.visitor_type);
              $('#visitor_name').attr("disabled", true);
              $('input:radio[name="gender"]').attr("disabled", true);
              $('#address').attr("disabled", true);
              $('#purpose').attr("disabled", true);
              $('#id_proof').attr("disabled", true);
              $('#visitor_type').attr("disabled", true);

              $('#unique_id').val(json.id);
              $('#visitor_name').val(json.name);
              $('input:radio[name="gender"][value="'+json.gender+'"]').attr('checked',true);
              $('#purpose').val(json.purpose);
              $('#id_proof').val(json.id_number);
              $('#address').val(json.address);
              $('#id_proof').val(json.id_number);
              $('#in_verify').val(json.in_verify);
              $('#out_verify').val(json.out_verify);
              $('#meet_to_name').val(json.me_to_name);
              $('#tower_id').val(json.tower_id);
              $('#tower_name').val(json.tower_name);
              $('#flat_id').val(json.flat_id);
              $('#flat_name').val(json.flat_name);
              $('#meet_to_add').val(json.me_to_add);
              $('#meet_to').val(json.me_to_mobile);
              $('#visitor_type').val(json.visitor_type);
              $("#tower_id_list").val(null).trigger('change');
              $("#flat_id_list").val(null).trigger('change');
              $("#meet_to_name_list").val(null).trigger('change');

              var show_img=json.show_img;
              if (show_img!='') {
                // alert(show_img);
                document.getElementById('results').innerHTML = '<img style="height: 100px;width: 130px;" src="'+show_img+'"/>';
              }
              veh_with_show(json.vehicle_with);
              if (json.vehicle_with=='Y') {
                document.getElementById("yes").checked = true;
              }else{
                document.getElementById("no").checked = true;
              }
              $('#vehicle_type').val(json.vehicle_type);
              $('#veh_number').val(json.vehicle_number);
              switch (json.visitor_type) {
                case 'V':
                  $('#meet_to').attr("disabled", true);
                  $('#visotor_in').attr("disabled", false);
                  $('#tower_id_list').attr("disabled", true);
                  $('#flat_id_list').attr("disabled", true);
                  $('#meet_to_name_list').attr("disabled", true);
                  break;
                case 'C':
                  $('#meet_to').attr("disabled", true);
                  $('#visotor_in').attr("disabled", false);
                  $('#tower_id_list').attr("disabled", true);
                  $('#flat_id_list').attr("disabled", true);
                  $('#meet_to_name_list').attr("disabled", true);
                  break;
                case 'G':
                  $('#meet_to').attr("disabled", true);
                  $('#visotor_in').attr("disabled", false);
                  $('#tower_id_list').attr("disabled", true);
                  $('#flat_id_list').attr("disabled", true);
                  $('#meet_to_name_list').attr("disabled", true);
                  break;
                case 'U':
                  $('#meet_to').attr("disabled", false);
                  $('#purpose').attr("disabled", false);
                  $('#visotor_in').attr("disabled", true);
                  $('#tower_id_list').attr("disabled", false);
                  $('#flat_id_list').attr("disabled", false);
                  $('#meet_to_name_list').attr("disabled", false);
                  break;
                default: 
                  $('#visotor_in').attr("disabled", true);
                  $('#visitor_name').attr("disabled", false);
                  $('input:radio[name="gender"]').attr("disabled", false);
                  $('#purpose').attr("disabled", false);
                  $('#meet_to').attr("disabled", false);
                  $('#address').attr("disabled", false);
                  $('#id_proof').attr("disabled", false);
                  $('#tower_id_list').attr("disabled", false);
                  $('#flat_id_list').attr("disabled", false);
                  $('#meet_to_name_list').attr("disabled", false);
                  $('#unique_id').val('');
                  // $('input:radio[name="gender"]').attr('checked',false);
                  // $('#visitor_name').val('');
                  // $('#purpose').val('');
                  $('#meet_to').val('');
                  // $('#address').val('');
                  // $('#id_proof').val('');
                  $('#in_verify').val('');
                  $('#out_verify').val('');
                  $('#meet_to_name').val('');
                  $('#tower_id').val('');
                  $('#tower_name').val('');
                  $('#flat_id').val('');
                  $('#flat_name').val('');
                  $('#meet_to_add').val('');
                  veh_with_show(visit_with_veh);
                  document.getElementById("no").checked = true;
                  $('#vehicle_type').val('');
                  $('#veh_number').val('');
              }
            }else{ 
              $('#visotor_in').attr("disabled", true);
              $('#visitor_name').attr("disabled", false);
              $('input:radio[name="gender"]').attr("disabled", false);
              $('#purpose').attr("disabled", false);
              $('#meet_to').attr("disabled", false);
              $('#address').attr("disabled", false);
              $('#id_proof').attr("disabled", false);
              $('input:radio[name="gender"]').attr('checked',false);
              $('#unique_id').val('');
              $('#visitor_name').val('');
              $('#purpose').val('');
              $('#meet_to').val('');
              $('#address').val('');
              $('#id_proof').val('');
              $('#in_verify').val('');
              $('#out_verify').val('');
              $('#meet_to_name').val('');
              $('#tower_id').val('');
              $('#tower_name').val('');
              $('#flat_id').val('');
              $('#flat_name').val('');
              $('#meet_to_add').val('');
              veh_with_show(visit_with_veh);
              document.getElementById("no").checked = true;
              $('#vehicle_type').val('');
              $('#veh_number').val('');
              document.getElementById('results').innerHTML = '<p>captured Visitor image will appear here...</p>';
              $("#tower_id_list").val(null).trigger('change');
              $('#tower_id_list').attr("disabled", false);
              $("#flat_id_list").val(null).trigger('change');
              $('#flat_id_list').attr("disabled", false);
              $("#meet_to_name_list").val(null).trigger('change');
              $('#meet_to_name_list').attr("disabled", false);
            }
          }
        });
      }else{
        $('#visotor_in').attr("disabled", true);
        $('#visitor_name').attr("disabled", false);
        $('input:radio[name="gender"]').attr("disabled", false);
        $('#purpose').attr("disabled", false);
        $('#meet_to').attr("disabled", false);
        $('#address').attr("disabled", false);
        $('#id_proof').attr("disabled", false);
        $('input:radio[name="gender"]').attr('checked',false);
        $('#unique_id').val('');
        $('#visitor_name').val('');
        $('#purpose').val('');
        $('#meet_to').val('');
        $('#address').val('');
        $('#id_proof').val('');
        $('#in_verify').val('');
        $('#out_verify').val('');
        $('#meet_to_name').val('');
        $('#tower_id').val('');
        $('#tower_name').val('');
        $('#flat_id').val('');
        $('#flat_name').val('');
        $('#meet_to_add').val('');
        veh_with_show(visit_with_veh);
        document.getElementById("no").checked = true;
        $('#vehicle_type').val('');
        $('#veh_number').val('');
        document.getElementById('results').innerHTML = '<p>captured Visitor image will appear here...</p>';
        $("#tower_id_list").val(null).trigger('change');
        $('#tower_id_list').attr("disabled", false);
        $("#flat_id_list").val(null).trigger('change');
        $('#flat_id_list').attr("disabled", false);
        $("#meet_to_name_list").val(null).trigger('change');
        $('#meet_to_name_list').attr("disabled", false);
      }
    }
    function veh_with_show(value){
      if(value == falgY){
        //alert("Y");
        //$('.visit_with_vehicle_yes').show();
        $(".visit_with_vehicle_yes").show();
        // $("#vehicle_type").prop('disabled', false);          
        // $("#veh_number").prop('disabled', false);          
        $(".veh_with").css("display","");         
      }else{
        $('.visit_with_vehicle_yes').hide();
        // $("#veh_number").prop('disabled', true);          
        // $("vehicle_type").prop('disabled', true);
        $(".veh_with").css("display","none");         
      }
    }
    function visitor_list() {
      $('#refresh_button').show();
      var visit_dtl_list = pkrEmpUrl+'prk_visit_dtl_list.php';
      $.ajax({
        url :visit_dtl_list,
        type:'POST',
        data :{
          'prk_admin_id':prk_admin_id,
          'prk_area_user_id':prk_area_user_id,
          'token':token
        },
        dataType:'html',
        success  :function(data){
          // alert(data);
          var demoLines = '';
          var json = $.parseJSON(data);
          if (json.status){
            for (var key in json.visit) {
              var keyy = json.visit[key];
              var img_url= "<?php echo OPPR_BASE_URL; ?>";
              var img_url= keyy['visit_img'];
              demoLines += '<tr>\
                <td><img src="'+img_url+'" style="height: 40px; border-radius: 50%;"></td>\
                <td>'+keyy['visit_name']+'</td>\
                <td>'+keyy['visit_mobile']+'</td>\
                <td>'+keyy['visitor_token']+'</td>\
                <td>'+keyy['in_verify_status']+'</td>\
                <td>'+keyy['visit_in_date']+'    '+keyy['visit_in_time']+'</td>\
                <td class="text-center">';
              var dis_bal ='';
              if(prk_area_gate_type == 'I'){
                var dis_bal ='N';
              }else{
                if(keyy['visitor_in_verify_flag'] == 'Y'){
                  if(keyy['visitor_out_verify_flag'] == 'Y'){
                    var dis_bal ='Y';
                  }else{
                    var dis_bal ='N';
                  }
                }else{
                  var dis_bal ='N';
                }
              }
              if(dis_bal =='Y'){

                demoLines += '<button style="color:white; background-color:red;" type="button" class="out btn prk_button" id="'+keyy['prk_visit_dtl_id']+'" onclick="vistor_out(this.id)" data-toggle="tooltip" data-placement="top"><i class="fa fa-sign-out"></i> OUT</button>';
              }else{

                demoLines += '<button style="" type="button" class="out btn" id="'+keyy['prk_visit_dtl_id']+'" onclick="vistor_out(this.id)" data-toggle="tooltip" data-placement="top" disabled><i class="fa fa-sign-out"></i> OUT</button>';
              }
              if(vis_prient_in_flag == falgY){

                demoLines += '<button class="out btn prk_button print" id="'+keyy['prk_visit_dtl_id']+'" onclick="visitor_in_print(this.id)" style="color: #FFF;"><i class="fa fa-print"></i> PRINT</button>';
              }else{

                demoLines += '<button class="out btn prk_button print" id="'+keyy['prk_visit_dtl_id']+'" onclick="visitor_in_print(this.id)" style="color:white; background-color:#808080;" disabled><i class="fa fa-print"></i> PRINT</button>';
              }
              demoLines += '</td></tr>';
            }
          }else{              
            if (!json.session) {
              window.location.replace("logout.php");
            }
          }
          $('#datatable1').dataTable().fnDestroy();
          $("#list_vis").html(demoLines);
          datatable_show();
        }
      });
    }
    function ajaxCall(){
      var unique_id = $('#unique_id').val();
      var visitor_type = $('#visitor_type').val();
      var in_verify = $('#in_verify').val();
      var out_verify = $('#out_verify').val();
      // alert(in_verify);
      var visit_name = $('#visitor_name').val();
      var visit_gender = $('input[name=gender]:checked').val();
      var visit_mobile = $('#mobile').val();
      var visit_purpose = $('#purpose').val();
      var visit_meet_to = $('#meet_to').val();
      var visit_address = $('#address').val();
      var visit_with_veh = $('input[name=visit_with_vehicle]:checked').val();
      var visit_card_id = $('#id_proof').val();
      var visit_img = $('#vis_image').val();
      // var visit_img = '';
      var prk_admin_id = "<?php echo $prk_admin_id; ?>";
      var prk_user_username = "<?php echo $prk_user_username; ?>";
      var prk_area_user_id = "<?php echo $prk_area_user_id; ?>";
      var veh_type = $('#vehicle_type').val();
      var veh_number = $('#veh_number').val();
      var meet_to_name = $('#meet_to_name').val();
      var tower_id = $('#tower_id').val();
      var tower_name = $('#tower_name').val();
      var flat_id = $('#flat_id').val();
      var flat_name = $('#flat_name').val();
      var meet_to_add = $('#meet_to_add').val();
      var visitor_token = $('#visitor_token').val();
      var token = "<?php echo $token; ?>";
      /*veh_add*/
      var urlVisitorIn = pkrEmpUrl+'prk_visit_dtl.php';
      // address_val()==true & purpose_val()==true &
      if (meet_to_name_list_val()==true & tower_id_list_val()==true & flat_id_list_val()==true & meet_to_val()==true &  mobile_val() & visitor_name_val()== true & visitor_token_val()== true & gender_val() == true) {
        // alert('ok');
        $('#visotor_in').val('Wait ...').prop('disabled', true);
        $.ajax({
          url :urlVisitorIn,
          type:'POST',
          data :{
            'visit_name':visit_name,
            'visit_gender':visit_gender,
            'visit_mobile':visit_mobile,
            'visit_purpose':visit_purpose,
            'visit_meet_to':visit_meet_to,
            'visit_address':visit_address,
            'visit_with_veh':visit_with_veh,
            'visit_card_id':visit_card_id,
            'visit_img':visit_img,
            'prk_admin_id':prk_admin_id,
            'prk_area_user_id':prk_area_user_id,
            'prk_user_username':prk_user_username,
            'veh_type':veh_type,
            'veh_number':veh_number,
            'visitor_meet_to_name':meet_to_name,
            'tower_id':tower_id,
            'flat_id':flat_id,
            'visitor_meet_to_address':meet_to_add,
            'visitor_token':visitor_token,
            'unique_id':unique_id,
            'visitor_type':visitor_type,
            'in_verify':in_verify,
            'out_verify':out_verify,
            'call_type':'W',
            'token':token
          },
          dataType:'html',
          success  :function(data){
            // alert(data);
            $('#visotor_in').val('IN').prop('disabled', false);
            var json = $.parseJSON(data);
            if (json.status){
              if (vis_prient_in_flag == "Y") {
                urlPrint = pkrEmpUrl+"visitor_in_print?id="+json.prk_visit_dtl_id+'&duplicate=N';
                window.open(urlPrint,'mywindow','width=850,height=600');
                 location.reload();
              }else{
                $.alert({
                  icon: 'fa fa-smile-o',
                  theme: 'modern',
                  title: 'Success !',
                  content: "<p style='font-size:0.9em;'>Visitor in successfully</p>",
                  type: 'green',
                  buttons: {
                    Ok: function () {
                        location.reload();
                    }
                  }
                });
              }
            }else{
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.7em;'>"+json.message+"</p>",
                type: 'red'
              });
              if (json.session==0) {
                window.location.replace("logout.php");
              }
            }
          }
        });
      }
    }    
    function vistor_out(str){
      // alert(str);
      var prk_admin_id = "<?php echo $prk_admin_id;?>";
      var prk_user_username = "<?php echo $prk_user_username;?>";
      var prk_area_user_id = "<?php echo $prk_area_user_id;?>";
      var prk_area_gate_id = "<?php echo $prk_area_gate_id;?>";
      var token = "<?php echo $token;?>";
      var prk_visit_dtl_id = str;        
      $.confirm({
        title: 'Are You Sure?',
        content: "<p style='font-size:0.9em;'>It will end the current trip</p>",
        theme: 'modern',
        type: 'red',
        buttons: {
          cancel: function () {
          },
          somethingElse: {
            text: 'Out',
            btnClass: 'btn-red',
            keys: ['Y', 'shift'],
            action: function(){
              //alert(prk_veh_trc_dtl_id);
              if (prk_visit_dtl_id!='') {
              var urlPrkVehTrnDtl = pkrEmpUrl +'prk_visit_dtl_out.php';
                $('#prk_visit_dtl_id').prop('disabled', true);
                $.ajax({
                  url :urlPrkVehTrnDtl,
                  type:'POST',
                  data :
                  {
                    'prk_admin_id':prk_admin_id,
                    'prk_area_user_id':prk_area_user_id,
                    'token':token,
                    'prk_visit_dtl_id':prk_visit_dtl_id,
                    'prk_user_username':prk_user_username,
                    'prk_area_gate_id':prk_area_gate_id
                  },
                  dataType:'html',
                  success  :function(data)
                  {
                    $('#prk_visit_dtl_id').prop('disabled', false);
                    var json = $.parseJSON(data);
                    if (json.status){
                      if (vis_prient_out_flag == "Y") {
                        visitor_out_print(prk_visit_dtl_id);
                      }else{
                        $.alert({
                          icon: 'fa fa-smile-o',
                          theme: 'modern',
                          title: 'Success !',
                          content: "<p style='font-size:0.9em;'>Visitor out successfully</p>",
                          type: 'green',
                          buttons: {
                            Ok: function () {
                              visitor_list();
                              // location.reload();
                            }
                          }
                        });
                      }
                    }else{  
                      $.alert({
                        icon: 'fa fa-frown-o',
                        theme: 'modern',
                        title: 'Error !',
                        content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
                        type: 'red'
                      });
                      if (json.session==0) {
                        window.location.replace("logout.php");
                      }
                    }
                  }
                });
              }else{
                alert("Somting went wrong");
              }
            }
          }
        }
      });
    }
    function meet_to_val() {
      var meet_to = $('#meet_to').val();
      var visitor_type = $('#visitor_type').val();
      if (visitor_type=='O' || visitor_type=='U') {
        if(meet_to==''){
          $('#error_meet_to').text('Meeet to is required');
          return false;
        }else{
          $('#error_meet_to').text('');
          return true;
        }
      }else{
        $('#error_meet_to').text('');
        return true;
      }
    }
    function meet_to_name_val() {
      var meet_to_name = $('#meet_to_name').val();
      var visitor_type = $('#visitor_type').val();
      if (visitor_type=='O' || visitor_type=='U') {
        if(meet_to_name==''){
          $('#error_meet_to_name').text('Meeet to Name is required');
          return false;
        }else{
          $('#error_meet_to_name').text('');
          return true;
        }
      }else{
        $('#error_meet_to_name').text('');
        return true;
      }
    }
    function meet_to_add_val() {
      var meet_to_add = $('#meet_to_add').val();
      var visitor_type = $('#visitor_type').val();
      if (visitor_type=='O' || visitor_type=='U') {
        if(meet_to_add==''){
          $('#error_meet_to_add').text('Meeet to Address is required');
          return false;
        }else{
          $('#error_meet_to_add').text('');
          return true;
        }
      }else{
        $('#error_meet_to_add').text('');
        return true;
      }
    }
    function mobile_val() {
      var mobile = $('#mobile').val();
      if(mobile==''){
        $('#error_mobile').text('Mobile number is required');
        return false;
      }else if (validatePhone(mobile)) {
        $('#error_mobile').text('');
        return true;
      }else {
        $('#error_mobile').text('Mobile number is invalid');
        return false;
      }
    }
    function validatePhone(txtPhone) {
      /*var filter = /[0-9]{2}\d{8}/;
      if (filter.test(txtPhone)) {*/
        return true;
     /* }
      else {
        return false;
      }*/
    }
    function visitor_name_val() {
      var visitor_name = $('#visitor_name').val();
      if(visitor_name==''){
        $('#error_visitor_name').text('Name is required');
        return false;
      }else{
        $('#error_visitor_name').text('');
        return true;
      }
    }
    function visitor_token_val() {
      if(visitor_token_flag=='Y'){
        var visitor_token = $('#visitor_token').val();
        if(visitor_token==''){
          $('#error_visitor_token').text('Token is required');
          return false;
        }else{
          $('#error_visitor_token').text('');
          return true;
        }
      }else{
        $('#error_visitor_token').text('');
        return true;
      }
    }
    function gender_val() {
      if (!$('input[name=gender]:checked').val() ) {
        $("#error_gender").text('Select Gender');
        return false;
      }else{
        $("#error_gender").text('');
        return true;
      }
    }
    function tower_id_list_val() {
      var visitor_type = $('#visitor_type').val();
      if(parking_type=='R' && (visitor_type=='O' || visitor_type=='U')){
        var tower_id_list1 = $('#tower_id_list').val();
        if(tower_id_list1==''){
          $('#error_tower_id_list').text('Tower Name is required');
          return false;
        }else{
          $('#error_tower_id_list').text('');
          return true;
        }
      }else{
        $('#error_tower_id_list').text('');
        return true;
      }
    }
    function flat_id_list_val() {
      var visitor_type = $('#visitor_type').val();
      if(parking_type=='R' && (visitor_type=='O' || visitor_type=='U')){
        var flat_id_list1 = $('#flat_id_list').val();
        if(flat_id_list1==''){
          $('#error_flat_id_list').text('Flat Name is required');
          return false;
        }else{
          $('#error_flat_id_list').text('');
          return true;
        }
      }else{
        $('#error_flat_id_list').text('');
        return true;
      }
    }
    function meet_to_name_list_val() {
      var visitor_type = $('#visitor_type').val();
      if(parking_type=='C' && (visitor_type=='O' || visitor_type=='U')){
        var flat_id_list1 = $('#meet_to_name_list').val();
        if(flat_id_list1==''){
          $('#error_meet_to_name_list').text('Meet to name is required');
          return false;
        }else{
          $('#error_meet_to_name_list').text('');
          return true;
        }
      }else{
        $('#error_meet_to_name_list').text('');
        return true;
      }
    }
    function visitor_in_print(str) {
      if (vis_prient_in_flag == "Y") {
        urlPrint = pkrEmpUrl+"visitor_in_print?id="+str+'&duplicate='+falgY;
        window.open(urlPrint,'mywindow','width=850,height=600');
         location.reload();
      }
    }
    function visitor_out_print(str) {
      if (vis_prient_out_flag == "Y") {
        urlPrint = pkrEmpUrl+"visitor_out_print?id="+str+'&duplicate=N';
        window.open(urlPrint,'mywindow','width=850,height=600');
         location.reload();
      }
    }
    function datatable_show(){
      'use strict';
      $('#datatable1').DataTable({
        responsive: true,
        dom: 'Blfrtip',
        language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
        },
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
          {
            text: '<span id="refresh_button"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
            action: function ( e, dt, node, config ) {
                // alert( 'Button activated' );
                visitor_list();
            }
          },
          { 
            extend: 'copyHtml5', 
            //footer: true,
            title: 'CURRENT VISITOR SHEET' 
          },
          { 
            extend: 'excelHtml5', 
            //footer: true,
            title: 'CURRENT VISITOR SHEET'  
          },
          {
            extend: 'pdfHtml5',
            //footer: true,
            title: 'CURRENT VISITOR SHEET',
            customize: function(doc) {
              doc.content[1].margin = [ 0, 0, 0, 0 ]
            }
          },
        ]
      });
      $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
      $('#refresh_button').hide();
    }
  </script>
  <script>
    function upperCase(a){
      setTimeout(function(){
          a.value = a.value.toUpperCase();
      }, 1);
    }
    function lowerCase(a){
      setTimeout(function(){
        //alert(a);
          a.value = a.value.toLowerCase();
      }, 1);
    }
    $("#name").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
        event.preventDefault();
      }
    });
    $('input[type=text]').keyup(function(){
      // alert('ok');
      this.value = this.value.toUpperCase();
    });
  </script>
  <script language="JavaScript">
    Webcam.set({
      width: 130,
      height: 100,
      image_format: 'jpeg',
      jpeg_quality: 90
    });
    Webcam.attach( '#my_camera' );
    function take_snapshot() {
      Webcam.snap( function(data_uri) {
        $(".vis_image").val(data_uri);
        document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
        vis_image();
      });
    }
    $(function() {
      $("#visitor_img").change(function() {
        var file = this.files[0];
        var imagefile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
        {
          $('#previewing').attr('src','../images/noimage.jpg');
          return false;
        }else{
          var reader = new FileReader();
          reader.onload = imageIsLoaded;
          reader.readAsDataURL(this.files[0]);
        }
      });
    });
    function imageIsLoaded(e) {
      // alert('ok');
      $(".vis_image").val(e.target.result);
      document.getElementById('results').innerHTML = '<img style="height: 100px;width: 130px;" src="'+e.target.result+'"/>';
      vis_image();
    };
    $('#my-button').click(function(){
      $('#visitor_img').click();
    });
  </script>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#tower_id_list").select2();
      $("#flat_id_list").select2();
      $("#meet_to_name_list").select2();
    });
    var urlTowerList = pkrEmpUrl+'tower_list.php';
    // 'prk_admin_id','prk_area_user_id','prk_user_username','token'
    $.ajax ({
      type: 'POST',
      url: urlTowerList,
      data: {
        'prk_admin_id':prk_admin_id,
        'prk_area_user_id':prk_area_user_id,
        'prk_user_username':prk_user_name,
        'token':token
      },
      success : function(data) {
        // alert(data);
        var obj=JSON.parse(data);
        //console.log(obj);
        var areaOption = "<option value=''>Select Tower</option>";
        $.each(obj['tower_list'], function(val, key) {
          areaOption += '<option value="' + key['tower_id'] + '">' + key['tower_name'] + '</option>'
        });
        $("#tower_id_list").html(areaOption);
        $("#tower_id_list").select2();
      }
    });
    $('#tower_id_list').on("change", function() {
      tower_id_list_val();
      meet_to_tower_flat();
      var id=($("#tower_id_list").val());
      var urlFlatList = pkrEmpUrl+'flat_list.php';
      // 'prk_admin_id','prk_area_user_id','prk_user_username','token','tower_id'
      $.ajax ({
        type: 'POST',
        url: urlFlatList,
        data: {
          prk_admin_id:prk_admin_id,
          prk_area_user_id:prk_area_user_id,
          prk_user_username:prk_user_name,
          token:token,
          tower_id:id
        },
        success : function(data) {
          // alert(data);
          var obj=JSON.parse(data);
          var areaOption = "<option value=''>Select Flat</option>";
          if (obj['status']) {
            $.each(obj['flat_list'], function(val, key) {
              areaOption += '<option value="' + key['flat_id'] + '">' + key['flat_name'] + '</option>'
            });
          }
          $("#flat_id_list").html(areaOption);
          $("#flat_id_list").select2();
        }
      });
    });
    $('#flat_id_list').on("change", function() {
      flat_id_list_val();
      meet_to_tower_flat();
    });
    $('#meet_to_name_list').on("change", function() {
      meet_to_name_search();
      meet_to_name_list_val();
    });
  </script>
