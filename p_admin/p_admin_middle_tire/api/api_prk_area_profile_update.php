<?php 
/*
Description: Parking area profile update.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_area_profile_update($prk_admin_id,$prk_area_name,$prk_area_short_name,$prk_area_email,$prk_area_rep_name,$prk_area_rep_mobile,$prk_admin_name,$prk_email_verify,$prk_mobile_verify,$prk_area_pro_img,$last_due_day,$choose_due_day,$prk_area_display_name){
    $response = array();
    global $pdoconn;
    $response = array();

    $sql = "SELECT * FROM `prk_area_dtl` WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    $prk_area_pro_img = $val['prk_area_pro_img'];
    $prk_area_logo_img = $val['prk_area_logo_img'];

    $sql ="UPDATE `prk_area_dtl` SET `active_flag`='".FLAG_N."', `updated_date`='".TIME."',`updated_by`='$prk_admin_name' WHERE `prk_admin_id`='$prk_admin_id' AND `active_flag`='".FLAG_Y."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $sql = "INSERT INTO `prk_area_dtl`(`prk_admin_id`,`prk_area_name`,`prk_area_short_name`,`prk_area_email`,`prk_area_rep_name`,`prk_area_rep_mobile`,`inserted_by`,`prk_email_verify`,`prk_mobile_verify`,`prk_area_pro_img`,`prk_area_logo_img`,`inserted_date`,`last_due_day`,`choose_due_day`,`prk_area_display_name`) VALUE ('$prk_admin_id','$prk_area_name','$prk_area_short_name','$prk_area_email','$prk_area_rep_name','$prk_area_rep_mobile','$prk_admin_name','$prk_email_verify','$prk_mobile_verify','$prk_area_pro_img','$prk_area_logo_img','".TIME."','$last_due_day','$choose_due_day','$prk_area_display_name')";
            $query = $pdoconn->prepare($sql);
        if($query->execute()){
            $response['status'] = 1;
            $response['message'] = 'Updated Sucessful';
            return json_encode($response);
        }else{
            $response['status'] = 0;
            $response['message'] = 'Updated Not Sucessful';
            return json_encode($response);
        }
        return json_encode($response);
    }
}
?>