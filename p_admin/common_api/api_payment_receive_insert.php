<?php 
/*
Description: Parking employ vehicle verify and data insert trc table.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function payment_receive_insert($payment_receive_number,$prk_admin_id,$prk_veh_trc_dtl_id,$payment_type,$payment_amount,$payment_statas,$prk_user_username,$in_out_flag){
    global $pdoconn;
    $response = array();
    $sql = "INSERT INTO `payment_receive`(`payment_receive_number`, `prk_admin_id`, `prk_veh_trc_dtl_id`, `payment_type`, `payment_amount`, `payment_statas`,`payment_rec_by`,`payment_rec_date`, `inserted_by`, `inserted_date`,`in_out_flag`) VALUES ('$payment_receive_number','$prk_admin_id','$prk_veh_trc_dtl_id','$payment_type','$payment_amount','$payment_statas','$prk_user_username','".TIME."','$prk_user_username','".TIME."','$in_out_flag')";    
    $query = $pdoconn->prepare($sql); 
    if($query->execute()){
        $id = $pdoconn->lastInsertId();
        $response['status'] = 1;
        $response['payment_receive_id'] = $id;
        $response['message'] = 'Sucessful';
        return json_encode($response); 
    }else{
        $response['status'] = 0;
        $response['message'] = 'Not Sucessful';
        return json_encode($response);
    }   
}
?>