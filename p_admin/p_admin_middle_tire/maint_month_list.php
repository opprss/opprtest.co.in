<?php 
/*
Description: parking area rate delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','parking_admin_name','token','flat_master_id','tower_id','flat_id'))){
 	if(isEmpty(array('prk_admin_id','parking_admin_name','token','year','flat_master_id','tower_id','flat_id'))){ 
	 	$prk_admin_id = $_POST['prk_admin_id'];
	 	$parking_admin_name = $_POST['parking_admin_name'];
	 	$token = trim($_POST['token']);
	 	$flat_master_id = trim($_POST['flat_master_id']);
	 	$tower_id = trim($_POST['tower_id']);
	 	$flat_id = trim($_POST['flat_id']);

        $resp=prk_token_check($prk_admin_id,$token);
		$json = json_decode($resp);
		if($json->status){
		    $response = maint_month_list($prk_admin_id,$parking_admin_name,$flat_master_id,$tower_id,$flat_id);
			//echo $respon;
		}else{
		    $response = $resp;
		    //echo $response;
		}
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 	$response = json_encode($response);
}
echo $response;
?>