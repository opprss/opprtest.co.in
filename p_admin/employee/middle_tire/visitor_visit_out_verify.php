<?php 
/*
Description: vehicle verify and data insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :26-04-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','prk_user_username','token','prk_visit_dtl_id'))){
    if(isEmpty(array('prk_admin_id','prk_area_user_id','prk_user_username','token','prk_visit_dtl_id'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);      
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $prk_user_username = trim($_POST['prk_user_username']);
        $token = trim($_POST['token']);
        $prk_visit_dtl_id = trim($_POST['prk_visit_dtl_id']);
        $token_check = prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($token_check);
        if($json->status=='1'){
            $sql = "UPDATE `prk_visit_dtl` SET `visitor_out_verify_date`='".TIME."',`visitor_out_verify_flag`='".FLAG_Y."',`visitor_in_verify_flag`='".FLAG_Y."',`visitor_out_verify_name`='$prk_user_username' WHERE `prk_visit_dtl_id`='$prk_visit_dtl_id'";
            $query  = $pdoconn->prepare($sql);
            if($query->execute()){ 
                $response['status']=1;
                $response['message'] = 'Visitor is Verified for OUT';
                $response = json_encode($response);
            }else{
                $response['status']=0;
                $response['message'] = 'Visitor is not Verified for OUT';
                $response = json_encode($response);
            } 
        }else{
            $response = $token_check;
        }   
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>