<!-- ################################################
  
  Description: Parking area can issue gate pass to a particular Visitor.
  Developed by: Soemen Banerjee
  Created Date: 17-03-2018
####################################################-->
<?php 
  include "all_nav/header.php";
  if (isset($_SESSION['prk_admin_id'])) {
    $user_name=$_SESSION['prk_admin_id'];
    $parking_admin_name=$_SESSION['parking_admin_name'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
    $visitor_gate_pass_dt_id=$visitor_gate_pass_dtl_id=base64_decode($_REQUEST['list_id']);
  }
  $visitor_gate_pass_dtl=visitor_gate_pass_dtl_show($visitor_gate_pass_dtl_id);
  $visitor_gate_pass_dtl_id = json_decode($visitor_gate_pass_dtl, true);
  // $visitor_gate_pass_dtl_id = $visitor_gate_pass_dtl_id['state'];
  $visitor_gender = $visitor_gate_pass_dtl_id['visitor_gender'];
  $id_proof_list=id_proof_list();
  $id_proof_list = json_decode($id_proof_list, true);
  // $visitor_service_dtl_show=visitor_service_dtl_show($visitor_gate_pass_dtl_id,$prk_admin_id);
  // $visitor_service_dtl_show = json_decode($visitor_service_dtl_show, true);
  $state = state();
  $state = json_decode($state, true);

?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: gray;

  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
    <!-- for datatable -->
    <link href="../lib/highlightjs/github.css" rel="stylesheet">
    <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet"> 
    <!-- header position -->
    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Visitor Gate Pass<?//=$visitor_gate_pass_dtl?></h5>
          <!--<form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card pd-20 pd-sm-40 col-md-12">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <div class="row">
              <div class="col-md-12">
                
                  <form role="form" id="addVis" method="post"  enctype="multipart/form-data">
                    <div class="form-layout">
                      <div class="row mg-b-25">
                        <input type="hidden" name="visitor_img" id="visitor_img" value="<?php echo $visitor_gate_pass_dtl_id['visitor_img']; ?>">
                        <input type="hidden" name="vis_id_prooof_img" id="vis_id_prooof_img" value="<?php echo $visitor_gate_pass_dtl_id['visitor_id_img']; ?>">
                        <input type="hidden" name="inserted_by" id="inserted_by" class="form-control name_list" readonly="" value="<?php echo $user_name; ?>">
                        <input type="hidden" name="prk_admin_id" id="prk_admin_id" class="form-control name_list" readonly="" value="<?php echo $prk_admin_id; ?>">
                        <input type="hidden" name="visitor-gate-pass-issue-show" id="visitor-gate-pass-issue-show" value="<?php echo $visitor_gate_pass_dtl_id; ?>">
                        <input type="hidden" name="parking_admin_name" id="parking_admin_name" value="<?php echo $parking_admin_name; ?>">
                        <input type="hidden" name="visitor_gate_pass_dtl_id" id="visitor_gate_pass_dtl_id" value="<?php echo $visitor_gate_pass_dt_id; ?>">
                        <input type="hidden" name="visitor_gate_pass_num" id="visitor_gate_pass_num" value="<?=$visitor_gate_pass_dtl_id['visitor_gate_pass_num']; ?>">
                        <div class="col-lg-3">
                          <div class="form-group">
                            <label class="form-control-label size">Name <span class="tx-danger">*</span></label>
                            <input type="text" class="form-control readonly" placeholder="Visitor Name" name="visitor_name" id="visitor_name" onkeyup="vehNoUppercase();" style="background-color: transparent;" value="<?php echo $visitor_gate_pass_dtl_id['visitor_name']; ?>" readonly>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="form-control-label size">Mobile Number<span class="tx-danger">*</span></label>
                            <input type="text" class="form-control readonly" placeholder="Mobile Number" name="visitor_mobile" id="visitor_mobile" maxlength="10" style="background-color: transparent;" value="<?php echo $visitor_gate_pass_dtl_id['visitor_mobile']; ?>" readonly>
                            <span style="position: absolute;" id="error_vis_mobile" class="error_size"></span>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label class="form-control-label size">Gender<span class="tx-danger">*</span></label>
                            <?php
                            if ($visitor_gender == "M") {
                            ?>
                              <label class="rdiobox">
                                <input type="radio" name="visitor_gender" id="vis_gender_m" value="M" type="radio" checked>
                                <span>Male </span>
                              </label>
                              <label class="rdiobox">
                                <input type="radio" name="visitor_gender" id="vis_gender_f" value="F" type="radio" disabled="disabled">
                                <span>Female </span>
                              </label>
                              <?php
                              }else{
                              ?>
                              <label class="rdiobox">
                                <input type="radio" name="visitor_gender" id="vis_gender_m" value="M" type="radio" disabled="disabled">
                                <span>Male </span>
                              </label>
                              <label class="rdiobox">
                                <input type="radio" name="visitor_gender" id="vis_gender_f" value="F" type="radio" checked>
                                <span>Female </span>
                              </label>
                              <span style="position: absolute;" id="error_vis_gender" class="error_size"></span>
                              <?php
                              }
                              ?>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label class="form-control-label size">Select Vehicle Type<span class="tx-danger"></span></label>
                            <div class="select">
                              <input type="text" class="form-control " placeholder="Vehicle Type" name="visitor_veh_type" id="visitor_veh_type" value="<?php echo $visitor_gate_pass_dtl_id['vehicle_type_dec']; ?>" style="background-color: transparent;">
                            </div>
                            <span style="position: absolute; display: none;" id="error_vis_veh_type" class="error_size">Please Select Vehicle type</span>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label class="form-control-label size">Vehicle Number<span class="tx-danger"></span></label>
                            <input type="text" class="form-control" value="<?php echo $visitor_gate_pass_dtl_id['visitor_vehicle_number']; ?>" placeholder="Vehicle Number" name="visitor_veh_num" id="visitor_veh_num" style="background-color: transparent;">
                            <span style="position: absolute;" id="error_vis_veh_num" class="error_size"></span>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-10">
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label class="form-control-label size">Address 1<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control readonly" placeholder="Address 1" name="visitor_add1" id="visitor_add1" style="background-color: transparent;" value="<?php echo $visitor_gate_pass_dtl_id['visitor_add1']; ?>" readonly>
                                <span style="position: absolute;" id="error_vis_add1" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label class="form-control-label size">Address 2<span class="tx-danger"></span></label>
                                <input type="text" class="form-control readonly" placeholder="Address 2" name="visitor_add2" id="visitor_add2" style="background-color: transparent;" value="<?php echo $visitor_gate_pass_dtl_id['visitor_add2']; ?>" readonly>
                                <span style="position: absolute;" id="error_vis_add2" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label class="form-control-label size">Landmark<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control readonly" placeholder="Landmark" name="visitor_landmark" id="visitor_landmark" style="background-color: transparent;" value="<?php echo $visitor_gate_pass_dtl_id['visitor_landmark']; ?>" readonly>
                                <span style="position: absolute;" id="error_vis_landmark" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <label class="form-control-label size">Country Name<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control readonly" placeholder="Gate Pass Number" name="visitor_country" id="visitor_country" value="INDIA" style="background-color: transparent;" readonly="">
                                <span style="position: absolute;" id="error_vis_country" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <label class="form-control-label size">State Name<span class="tx-danger">*</span></label>
                                <div class="select">
                                  <select name="visitor_state_o" id="visitor_state_o" style="opacity: 0.8;font-size:14px" >
                                      <option value="">Select State</option>
                                       <?php
                                        foreach ($state['state'] as $val){ ?>
                                          <option value="<?php echo $val['sta_id']?>"><?php echo $val['state_name']?></option>
                                        <?php
                                        }
                                      ?>
                                  </select>
                                  <input type="hidden" name="visitor_state" id="visitor_state">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <label class="form-control-label size">City Name<span class="tx-danger">*</span></label>
                                <div class="select">
                                  <select name="visitor_city_o" id="visitor_city_o" style="opacity: 0.8;font-size:14px" >
                                      <option value="">SELECT CITY</option>
                                  </select>
                                  <input type="hidden" name="visitor_city" id="visitor_city">
                                </div>
                                <span style="position: absolute;" id="error_vis_city_name" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <label class="form-control-label size">Pin Code<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control readonly" placeholder="Pin Code" name="visitor_pin_cod" id="visitor_pin_cod" style="background-color: transparent;" maxlength="6" value="<?php echo $visitor_gate_pass_dtl_id['visitor_pin_cod']; ?>" readonly>
                                <span style="position: absolute;" id="error_vis_pin_code" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-lg-3 ">
                              <div class="form-group">
                                <label class="form-control-label size">Select ID Type <?php //echo($id_proof_list);?><span class="tx-danger">*</span></label>
                                  <div class="select">
                                    <select name="id_name_o" id="id_name_o" style="opacity: 0.8;font-size:14px" >
                                        <option value="">SELECT ID TYPE</option>
                                        <?php
                                        foreach ($id_proof_list['state'] as $val){ ?>
                                          <option value="<?php echo $val['all_drop_down_id']?>" <?php  if($visitor_gate_pass_dtl_id['id_name'] == $val['all_drop_down_id']) { echo "selected='selected'"; }   ?>><?php echo $val['full_name']?></option>
                                        <?php
                                        }
                                      ?>
                                    </select>
                                    <input type="hidden" name="id_name" id="id_name" value="<?php echo $visitor_gate_pass_dtl_id['id_name']; ?>">
                                  </div>
                                  <span style="position: absolute;" id="error_vis_id_name" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <label class="form-control-label size">ID Number<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control readonly" placeholder="Id Number" name="id_number" id="id_number" maxlength="10" style="background-color: transparent;" value="<?php echo $visitor_gate_pass_dtl_id['id_number']; ?>" readonly>
                                <span style="position: absolute;" id="error_vis_id_number" class="error_size"></span>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="form-group">
                                <label class="form-control-label size">EFFECTIVE DATE <span class="tx-danger">*</span></label>
                                <input type="text" name="effective_date_o" id="effective_date_o" placeholder="Effective Date" class="form-control prevent_readonly" value="<?php echo $visitor_gate_pass_dtl_id['effective_date']; ?>" >
                                <span style="position: absolute;" id="error_effective_date" class="error_size"></span>

                                <input type="hidden" name="effective_date" id="effective_date" value="<?php echo $visitor_gate_pass_dtl_id['effective_date']; ?>">
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="form-group">
                                <label class="form-control-label size">END DATE</label>
                                <input type="text" name="end_date" id="end_date" placeholder="End Date" class="form-control prevent_readonly" readonly="readonly" value="<?php echo $visitor_gate_pass_dtl_id['end_date']; ?>">
                                <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-2">
                          <div class="row">

                            <div class="col-md-11">
                              <div class="form-group">
                                <div id="results">
                                <label class="form-control-label size">Visitor Image /ID Proof</label>
                                  <img id="previewing" src="<?php echo PRK_BASE_URL.''.$visitor_gate_pass_dtl_id['visitor_img']; ?>" style="height: 100px;width: 130px;">
                                </div>
                                <!-- <span  id="error_vis_image" class="error_size"></span> -->
                                <label style="position: absolute;" class="error_size" id="error_vis_image"></label>
                              </div>
                            </div><div class="col-md-1"></div>
                            <div class="col-md-11">
                              <div class="form-group">
                                <div id="results2">
                                  <img id="previewing" src="<?php echo PRK_BASE_URL.''.$visitor_gate_pass_dtl_id['visitor_id_img']; ?>" style="height: 100px;width: 130px;">
                                </div>
                                <!-- <span style="position: absolute;" id="error_id_image" class="error_size"></span> -->
                                <label style="position: absolute;" class="error_size" id="error_id_image"></label>
                              </div>
                            </div><div class="col-md-1"></div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-2">
                          <div class="form-layout-footer mg-t-30">
                            <button style="margin-bottom:10px;" type="button" class="btn btn-block prk_button" name="vis_work_add" id="vis_work_add">+ ADD ROW</button>
                          </div>
                        </div>
                      <div>
                      <!-- </form> -->

                        <table border="1" cellspacing="0" class="table table-bordered" id="dynamic_field">
                          <tr style="font-size: 14px;text-align: center;">
                            <th>Serial No</th>
                            <th>Tower</th>
                            <th>Flat No</th>
                              <th>In Time </br>Use 24-hour</th>
                              <th>Out Time </br>Use 24-hour</th>
                            <th>Remark<?php //echo($prk_admin_id); ?></th>
                            <th>Action</th>
                          </tr>
                          <?php
                          $visitor_service_dtl_show= visitor_service_dtl_show($prk_admin_id,$visitor_gate_pass_dt_id);
                          $visitor_service_dtl_show1 = json_decode($visitor_service_dtl_show, true);
                          // echo $visitor_service_dtl_show;
                          if($visitor_service_dtl_show1['status']){
                          $si_r=0;
                          foreach($visitor_service_dtl_show1['state'] as $visitor_service_dtl_show)
                          {
                          $si_r+=1;
                          ?>
                          <tr>
                            <td><?php echo $si_r ?></td>
                            <td>
                              <input readonly="" type="text" name="tower_name[]" id="tower_name" class="form-control name_list" value="<?php echo $visitor_service_dtl_show['tower_name']; ?>" placeholder="Tower Name" / >
                            </td>
                            <td>
                                <input readonly="" type="text" placeholder="Flat Name" name="falt_name[]" id="falt_name" value="<?php echo $visitor_service_dtl_show['falt_name']; ?>" class="form-control name_list"/>
                            </td>
                            <td>
                                <input readonly="" type="text" placeholder="HH:MM" name="start_date[]" id="start_date" value="<?php echo $visitor_service_dtl_show['in_time']; ?>" class="form-control name_list"/>
                            </td>
                            <td>
                                <input readonly="" type="text" placeholder="HH:MM" name="end_date1[]" id="end_date1" value="<?php echo $visitor_service_dtl_show['out_time']; ?>" class="form-control name_list" />
                            </td>
                            <td>
                                <input readonly="" type="text" placeholder="Remark" name="remark[]" id="remark" value="<?php echo $visitor_service_dtl_show['visitor_remark']; ?>" class="form-control name_list" />
                            </td>
                            <!-- <input type="hidden" name="prk_admin_id[]" id="prk_admin_id" class="form-control name_list" readonly="" value="<?php echo $prk_admin_id; ?>"> -->
                            <td>
                              <button type="button" class="clean-button" name="delete" id="<?php echo $visitor_service_dtl_show['visitor_service_dtl_id']; ?>" value="" data-toggle="tooltip" data-placement="top" title="Delete" onclick="visitor_service_dtl_dlt(this.id)"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
                            </td>
                          </tr>
                          <?php } }?>
                         </table> 
                      </div>
                      <div class="row mg-b-25">
                      <div class="col-lg-2">
                        <div class="form-layout-footer mg-t-30">
                              <!-- <button type="submit" class="btn btn-block prk_button" name="vis_work_save" id="vis_work_save">SAVE</button> -->
                              <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="vis_work_save" id="vis_work_save">
                        </div>
                      </div>
                      <div class="col-lg-8"></div>
                      <div class="col-lg-2">
                        <div class="form-layout-footer mg-t-30">
                              <button type="button" class="btn btn-block prk_button" id="back">BACK</button>
                        </div>
                      </div>
                      </div>
                    </div>
                  </form>
              </div>
              
            </div>
          </div>
        </div>
      </div>
      <div class="am-pagebody">
      <!-- footer part -->
      <?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<style>
  .error_size{
    font-size: 11px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var prk_admin_id = "<?php echo $prk_admin_id; ?>";
  var parking_admin_name = "<?php echo $parking_admin_name; ?>";
  var visitor_city = "<?php echo $visitor_gate_pass_dtl_id['visitor_city']; ?>";
  var city_name = "<?php echo $visitor_gate_pass_dtl_id['city_name']; ?>";
  var visitor_state = "<?php echo $visitor_gate_pass_dtl_id['visitor_state']; ?>";
  // alert(visitor_state);
  $( document ).ready( function () {
    $('#visitor_state_o').val(visitor_state);
    $('#visitor_state').val(visitor_state);
    $('#visitor_city').val(visitor_city);
    var demoLin = '';
    demoLin +='<option value='+visitor_city+'>'+city_name+'</option>';
    $("#visitor_city_o").html(demoLin);
    // alert('ok');
    var j=1;
    var t=2;
    var i=2;
    $("#vis_work_add").on('click',function(){
      // alert('ok');
      i++;
      t++;
      j++;
      count=$('table tr').length;
      // alert(count);
      var data="<tr id='row"+i+"'><td><span id='snum"+i+"'>"+count+".</span></td>";


      data +='<td><input type="text" name="tower_name[]" id="tower_name' +i+ '" class="form-control name_list" placeholder="Tower Name" required=""/></td><td><input type="text" placeholder="Flat Name" name="falt_name[]" id="falt_name' +i+ '" class="form-control name_list" required=""/></td><td><input type="text" placeholder="HH:MM" name="start_date[]" id="start_date' +i+ '" class="form-control name_list time_pic" required=""/></td><td><input type="text" placeholder="HH:MM" name="end_date1[]" id="end_date1' +i+ '" class="form-control name_list time_pic" required=""/></td><td><input type="text" placeholder="Remark" name="remark[]" id="remark' +i+ '" class="form-control name_list"/></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td><td style="visibility:hidden;"><input type="hidden" name="hide_select[]" id="myText'+t+'"></td></tr>';
      $('table').append(data);
      $(document).on('click', '.btn_remove', function(){
        var button_id = $(this).attr("id"); 
        $('#row'+button_id+'').remove();
      });
    });
    $("#effective_date").blur(function () {
      var effective_date = $('#effective_date').val();
      if(effective_date == ''){
        $('#error_effective_date').text('Effective date is required');
        return false;
      }else{
        $('#error_effective_date').text('');
        return true;
      }
    });
    $('#end_date').focus(function () {
      var start = new Date;
      var end = $('#effective_date').val();
      var d = new Date(end.split("-").reverse().join("-"));
      var dd=d.getDate();
      var mm=d.getMonth()+1;
      var yy=d.getFullYear();
      var hh = start.getHours();
      var min = start.getMinutes();
      var sec = start.getSeconds();
      var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
      var end = new Date(newdate);
      days = (end - start) / (1000 * 60 * 60 * 24)-1;
      days = Math.round(days);
      now_end = start.setDate(start.getDate() + days);
      pickmeup('#end_date', {
        default_date   : false,
        /*position       : 'top',*/
        hide_on_select : true,
        render : function (date) {
              if (date < now_end) {
                  return {
                    disabled : true,
                    class_name : 'date-in-past'
                  };
              }
              return {};
          } 
      });
    });
    $('#effective_date').focus(function(){
      $('#end_date').val('');
    });

  });
  /*image preview*/
  function visitor_service_dtl_dlt(id) {
    
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the item permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            if (id != '') {
              var urlPrkVehTrnDtl = prkUrl +'visitor_service_dtl_delete.php';
                $.ajax({
                  url :urlPrkVehTrnDtl,
                  type:'POST',
                  data :
                  {
                    'prk_admin_id':prk_admin_id,
                    'visitor_service_dtl_id':id,
                    'parking_admin_name':parking_admin_name
                  },
                  dataType:'html',
                  success  :function(data)
                  {
                    // alert(data);
                    var json = $.parseJSON(data);
                    if (json.status){
                      $.alert({
                        icon: 'fa fa-smile-o',
                        theme: 'modern',
                        title: 'Success',
                        content: "<p style='font-size:0.9em;'>Delet successfully</p>",
                        type: 'green',
                        buttons: {
                          Ok: function () {
                            location.reload(true);
                          }
                        }
                      });
                    }else{
                       $.alert({
                        icon: 'fa fa-frown-o',
                        theme: 'modern',
                        title: 'Error !',
                        content: "<p style='font-size:0.7em;'>Somting went wrong 1</p>",
                        type: 'red'
                      });
                    }
                  }
                });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
</script>
<script>
  $(document).ready(function(){
    $('#send_otp').attr("disabled", true);
    $('#otp_verify').attr("disabled", true);
    $('#vis_otp').attr("disabled", true);
    $('#effective_date_o').attr("disabled", true);
    $('#visitor_state_o').attr("disabled", true);
    $('#visitor_city_o').attr("disabled", true);
    $('#id_name_o').attr("disabled", true);
    $('#visitor_veh_type').attr("disabled", true);
    $('#visitor_veh_num').attr("disabled", true);
    // $('#vis_work_save').click(function(){
    $('#vis_work_save').bind('click', function() {
      var prkUrl = "<?php echo PRK_URL; ?>";
      var urlCity = prkUrl+'visitor_gate_pass_dtl_update.php';
      $('#vis_work_save').val('Wait ...').prop('disabled', true);
      $.ajax({  
        url:urlCity,  
        method:"POST",  
        data:$('#addVis').serialize(),  
        success:function(data)  
        {  
          $('#vis_work_save').val('SAVE').prop('disabled', false);
          var json = $.parseJSON(data);
          // alert(data);
          if (json.status){
            window.location.href='visitor-gate-pass-issue-list';
            // $.alert({
            //   icon: 'fa fa-smile-o',
            //   theme: 'modern',
            //   title: 'Success',
            //   content: "<p style='font-size:0.9em;'>Visitor Gatepass Issue successfully</p>",
            //   type: 'green',
            //   buttons: {
            //     Ok: function () {
            //     }
            //   }
            // });
          }else{
            $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.7em;'>Somting went wrong 1</p>",
              type: 'red'
            });
          }
        }  
      });  
    }); 
    $(".time_pic").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0 &inputValue != 58)){
        event.preventDefault();
      }
    });
    $('#back').on('click',function(){
      window.location.href='visitor-gate-pass-issue-list';
    });
  });
</script>