<?php 
/*
Description: vehicle verify and data insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :26-04-2018
*/ 
require_once 'api/parkAreaReg_api.php';
$response = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','me_to_name','token'))){
    if(isEmpty(array('prk_admin_id','prk_area_user_id','token'))){
        $prk_admin_id = trim($_POST['prk_admin_id']);      
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $me_to_name = trim($_POST['me_to_name']);
        $token = trim($_POST['token']);
        $user_admin_id=$oth1=$oth2='';
        $token_check = prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
        $json = json_decode($token_check);
        if($json->status=='1'){
            
            $response = meet_to_person_search($prk_admin_id,$me_to_name);
        }else{
            $response = $token_check;
        }   
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>