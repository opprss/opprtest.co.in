<!-- ################################################
Description: List of user's vechile | user can delete vehicle from this page.
Developed by: Soemen Banerjee
Created Date: 17-03-2018
 ####################################################-->
<?php 
  include "all_nav/header.php";
  $prk_area_list1=user_add_prk_area_list($user_admin_id);
  $prk_area_list = json_decode($prk_area_list1, true);
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
  .error{
    font-size: 11px;
    color: red;
  }
</style>
<!-- Time Picker -->
  <script src="js/timepicker.min.js"></script>
  <link href="css/timepicker.min.css" rel="stylesheet">
  <!-- <script src="http://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script> -->
  <!-- <link href="http://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/> -->
<!-- Time Picker -->
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script src="../lib/validation/jquery.validate.js"></script>
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
  <div class="am-mainpanel"><!-- cloding in footer -->
    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Guest List<?php //echo $prk_area_list1 ?></h5>
    </div><!-- am-pagetitle -->
    
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40" >
        <!-- <h6 class="card-body-title">Add Employee</h6> -->
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <form role="form" id="addGuest" method="post"  enctype="multipart/form-data">
          <div class="row mg-b-25">
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Choose Area<span class="tx-danger">*</span></label>
                <div class="select" style="font-size:14px">
                  <select name="prk_admin_id" id="prk_admin_id" style="color: skyblue">
                    <option value="">SELECT AREA</option>
                     <?php
                      foreach ($prk_area_list['prk_area'] as $val){ ?>  
                        <option value="<?php echo $val['prk_admin_id']?>"><?php echo $val['prk_area_name']?></option>
                      <?php          
                      }
                    ?> 
                  </select>
                </div>
                <span style="position: absolute; display: none;" id="error_prk_admin_id" class="error_size">Select Area</span>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Visitor Name<span class="tx-danger">*</span></label>
                <input type="text"  name="vis_name" class="form-control" placeholder="Visitor Name" id="vis_name">
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Visitor Mobile<span class="tx-danger">*</span></label>
                <input type="text"  name="vis_mob" class="form-control" placeholder="Visitor Mobile" id="vis_mob" maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
            </div>
            <div class="col-lg-3">
              <div class="row mg-b-25">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label size">Number of Visitor<span class="tx-danger">*</span></label>
                    <input type="text"  name="num_of_vis" class="form-control" placeholder="No. of Visito" id="num_of_vis" value="1">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label size">Visitor Gender<span class="tx-danger">*</span></label>
                      <label class="rdiobox">
                        <input name="gender" id="male" value="M" type="radio" checked>
                        <span>Male </span>
                      </label>
                      <label class="rdiobox">
                        <input name="gender" id="female" value="F" type="radio">
                        <span>Female </span>
                      </label>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Visitor Address<span class="tx-danger">*</span></label>
                <input type="text"  name="vis_add" class="form-control" placeholder="Visitor Address" id="vis_add">
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Visitor Purpose<span class="tx-danger">*</span></label>
                <input type="text"  name="vis_pur" class="form-control" placeholder="Visitor Purpose" id="vis_pur">
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Visitor ID Proof<span class="tx-danger"></span></label>
                <input type="text"  name="vis_id_pro" class="form-control" placeholder="Visitor ID Proof" id="vis_id_pro">
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="form-control-label size">Visitor IN Date<span class="tx-danger">*</span></label>
                <input type="text"  name="vis_in_da" class="form-control prevent_readonly" placeholder="Visitor IN Date" id="vis_in_da" readonly="readonly">
              </div>
            </div>
            <div class="col-lg-3">
              <div class="row mg-b-25">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label size">Time (From)<span class="tx-danger">*</span></label>
                    <input type="text"  name="to_time" class="form-control prevent_readonly" placeholder="HH:MM" id="to_time" readonly="readonly">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label size">Time (To)<span class="tx-danger">*</span></label>
                    <input type="text"  name="form_time" class="form-control prevent_readonly" placeholder="HH:MM" id="form_time" readonly="readonly">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Verify Visitor In<span class="tx-danger">*</span></label>
                  <div class="select">
                    <select name="vis_in_ver" id="vis_in_ver" value="" style="opacity: 0.8; font-size:14px">
                      <option value="">Select</option>
                      <option value="Y">YES</option>
                      <option value="N" selected="">NO</option>
                    </select>
                  </div>
                  <span style="position: absolute; display: none;" id="error_vis_in_ver" class="error_size">Please Select</span>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Verify Visitor Out<span class="tx-danger">*</span></label>
                  <div class="select">
                    <select name="vis_out_ver" id="vis_out_ver" value="" style="opacity: 0.8; font-size:14px">
                      <option value="">Select</option>
                      <option value="Y">YES</option>
                      <option value="N" selected="">NO</option>
                    </select>
                  </div>
                  <span style="position: absolute; display: none;" id="error_vis_out_ver" class="error_size">Please Select</span>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group mg-t-35">
              <!-- <div class="form-group"> -->
                <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="vis_save" id="vis_save">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group mg-t-35">
              <!-- <div class="form-group"> -->
                <button type="reset" class="btn btn-block prk_button btn-primary">Reset</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div><!-- am-pagebody -->
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40">
      <?php $respon=user_guest_list($user_admin_id);
           // print_r($respon);
      ?>
      <div class="table-wrapper">
        <table id="datatable1" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="wd-10p">Sl No.</th>
              <th class="wd-20p">Guest Name</th>
              <th class="wd-12p">Guest Mobile</th>
              <th class="wd-15p">Guest Purpase</th>
              <th class="wd-10p">No. OF Guest</th>
              <th class="wd-15p">IN Date</th>
              <th class="wd-10p">TO Time</th>
              <th class="wd-10p">Form Time</th>
              <th class="wd-10p">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $respon=user_guest_list($user_admin_id);
            $respon = json_decode($respon, true);
            // print_r($respon);
            $x=1;
            if ($respon['status']) {
            foreach($respon['guest_list'] as $value){
            $user_guest_list_id = $value['user_guest_list_id'];
             ?>

            <tr>
             <td ><?php echo $x; ?></td>
             <td ><?php echo $value['guest_name']; ?></td>
             <td ><?php echo $value['guest_mobile']; ?></td>
             <td><?php echo $value['guest_purpose']; ?></td>
             <td><?php echo $value['no_of_guest']; ?></td>
             <!-- <td><?php echo $value['permission']; ?></td> -->
             <td><?php echo $value['in_date']; ?></td>
             <td><?php echo $value['to_time']; ?></td>
             <td><?php echo $value['form_time']; ?></td>
             <?php if ($value['permission']=='M') {?>
             <td>
              <a href="user-guest-edit?list_id='<?php echo base64_encode($value['user_guest_list_id']) ?>'&re='<?php echo base64_encode('N') ?>'"><i class="fa fa-pencil" style="font-size:18px; font-size:18px;"></i></a>
              <button class="clean-button" onclick="guest_dalete(this.id)" id="<?php echo $value['user_guest_list_id']; ?>"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
             </td>
             <?php }else{ ?>
             <td>
              <a href="user-guest-edit?list_id='<?php echo base64_encode($value['user_guest_list_id']) ?>'&re='<?php echo base64_encode('Y') ?>'"><button style="background: #00b8d4; border: solid 1px; border-radius: 10%;" class="clean-button">REVISIT</button></a>
             </td>
             <?php }?>
            </tr>
            <?php $x++; }} ?>
          </tbody>
        </table>
      </div><!-- table-wrapper -->
    </div>
  </div>
  <!-- footer part -->
  <?php include"all_nav/footer.php"; ?>
  <!-- footer part -->
<script type="text/javascript">
  var userUrl = "<?php echo USER_URL; ?>";
  var user_admin_id = "<?php echo $user_admin_id; ?>";
  var token = "<?php echo $token; ?>";
  var mobile = "<?php echo $user_mobile; ?>";
  // alert(mobile);
  $( document ).ready( function () {  
    $('#vis_save').click(function () {
      // alert('ok');
      var vis_name=$('#vis_name').val();
      var vis_mob=$('#vis_mob').val();
      var gender =  $('input[name=gender]:checked').val();
      var vis_add=$('#vis_add').val();
      var vis_pur=$('#vis_pur').val();
      var num_of_vis=$('#num_of_vis').val();
      var vis_id_pro=$('#vis_id_pro').val();

      var in_date=$('#vis_in_da').val();
      var to_time=$('#to_time').val();
      var form_time=$('#form_time').val();
      // alert(in_date);
      var vis_in_ver=$('#vis_in_ver').val();
      var vis_out_ver=$('#vis_out_ver').val();
      var prk_admin_id=$('#prk_admin_id').val();
      if($("#addGuest").valid()){
        $(this).val('Wait ...').prop('disabled', true);
        var user_guest_list_in = userUrl+'user_guest_list_in.php';
        $.ajax({
          url :user_guest_list_in,
          type:'POST',
          data :{
            'guest_name':vis_name,
            'user_admin_id':user_admin_id,
            'guest_mobile':vis_mob,
            'guest_gender':gender,
            'guest_address':vis_add,
            'guest_purpose':vis_pur,
            'no_of_guest':num_of_vis,
            'guest_id_proof':vis_id_pro,
            'in_date':in_date,
            'to_time':to_time,
            'form_time':form_time,
            'guest_in_verify':vis_in_ver,
            'guest_out_verify':vis_out_ver,
            'mobile':mobile,
            'token':token,
            'prk_admin_id':prk_admin_id
          },
          dataType:'html',
          success  :function(data){
            $('#vis_save').val('SAVE').prop('disabled', false);
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.9em;'>Guest Add Successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                      location.reload();
                  }
                }
              });
            }else{
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
                type: 'red'
              });
              if (json.session==0) {
                window.location.replace("logout.php");
              }
            }
          }
        });
      }
    });
    $( "#addGuest" ).validate( {
      rules: {
        vis_name: "required",
        vis_mob:{
          required:true,
          maxlength:10,
          minlength:10
        },
        vis_add: "required",
        vis_pur: "required",
        vis_in_da: "required",
        to_time: "required",
        form_time: "required",
        vis_in_ver: "vis_in_ver_check",
        vis_out_ver: "vis_out_ver_check",
        num_of_vis:"required",
        prk_admin_id:"prk_admin_id_check"
      },
      messages: {
        vis_name:"Please Enter the Name",
        vis_mob:{
          required:"Please Enter the Mobile No.",
          minlength:"Minimum length is 10",
          maxlength:"Maximum length is 10"
        },
        vis_add:"Please Enter the Address",
        vis_pur:"Please Enter the Purpose",
        vis_in_da:"Please Enter the IN Date",
        to_time:"Select Time",
        form_time:"Select Time",
        num_of_vis:"Enter The No. of Visitor"
      }
    });
    $.validator.addMethod("vis_in_ver_check", function(value){
      // alert(value);
      arg = "";
      if (value != "") {
        //return true;
        $("#error_vis_in_ver").hide();
        return arg !== value;
      }else{
        $("#error_vis_in_ver").show();
        return false;
      }
    });
    $.validator.addMethod("vis_out_ver_check", function(value){
      // alert(value);
      arg = "";
      if (value != "") {
        //return true;
        $("#error_vis_out_ver").hide();
        return arg !== value;
      }else{
        $("#error_vis_out_ver").show();
        return false;
      }
    });
    $.validator.addMethod("prk_admin_id_check", function(value){
      // alert(value);
      arg = "";
      if (value != "") {
        //return true;
        $("#error_prk_admin_id").hide();
        return arg !== value;
      }else{
        $("#error_prk_admin_id").show();
        return false;
      }
    });
  });
  function guest_dalete(user_guest_list_id){
    // alert(user_guest_list_id);
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.9em;'>Delete Guest List</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            var user_guest_list_delete = userUrl+'user_guest_list_delete.php';
            $.ajax({
              url :user_guest_list_delete,
              type:'POST',
              data :{
                'user_guest_list_id':user_guest_list_id,
                'user_admin_id':user_admin_id,
                'mobile':mobile,
                'token':token
              },
              dataType:'html',
              success  :function(data){
                // alert(data);
                var json = $.parseJSON(data);
                if (json.status){
                  $.alert({
                    icon: 'fa fa-smile-o',
                    theme: 'modern',
                    title: 'Success !',
                    content: "<p style='font-size:0.9em;'>"+json.message+"</p>",
                    type: 'green',
                    buttons: {
                      Ok: function () {
                          location.reload();
                      }
                    }
                  });
                }else{
                  $.alert({
                    icon: 'fa fa-frown-o',
                    theme: 'modern',
                    title: 'Error !',
                    content: "<p style='font-size:0.7em;'>"+json.message+"</p>",
                    type: 'red'
                  });
                  if(json.session==0) {
                    window.location.replace("logout.php");
                  }
                }
              }
            });
          }
        }
      }
    });
  }
</script>
<script>
  $(document).ready(function () {
    addEventListener('DOMContentLoaded', function () {
      var now = new Date;
      var now = now.setDate(now.getDate() - 1);
      pickmeup('#vis_in_da', {
        default_date   : false,
        format  : 'd-m-Y',
        hide_on_select : true,
        render : function (date) {
          if (now > date) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
          }
          return {};
        } 
      });
    });
    $("#vis_name").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 60) && (inputValue != 0 & inputValue !=32)){
        // if(!(inputValue >= 97) && (inputValue != 46)){
        event.preventDefault();
      }
    });
  });
</script>
<style type="text/css">
  ._jw-tpk-container{
    height: 180px;
  }
</style>
<script type="text/javascript">
  var form_time = document.getElementById('form_time');
  var timepicker = new TimePicker(['to_time', 'form_time'], {
      lang: 'en',
      theme: 'dark'
  });
  timepicker.on('change', function(evt) {
      var value = (evt.hour || '00') + ':' + (evt.minute || '00');
      if (evt.element.id === 'form_time') {
        form_time.value = value;
      } else {
        to_time.value = value;
      }
  });
</script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>
