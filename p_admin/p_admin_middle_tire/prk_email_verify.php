<?php 
/*
Description: parking area email verified.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
// require_once '../../connection-pdo.php';
require_once 'api/parkAreaReg_api.php';
// require_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('prk_admin_id','prk_area_email','token'))){
 	if(isEmpty(array('prk_admin_id','prk_area_email','token'))){

	 	$prk_admin_id = trim($_POST['prk_admin_id']);
	 	$prk_area_email = trim($_POST['prk_area_email']);
        $token = ($_POST['token']);

        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            $response= prk_email_verify($prk_admin_id, $prk_area_email);
        echo ($response);
        }else{
            $response = $resp;
            echo $response;
        }
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?> 