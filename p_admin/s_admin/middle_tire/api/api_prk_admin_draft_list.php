<?php
//require_once 'api/parkAreaReg_api.php';
function prk_admin_draft_list(){


	global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    // $sql = "SELECT * FROM `prk_area_admin` WHERE `park_ac_status`='A'";
    $sql = "SELECT prk_area_admin.prk_admin_id,
        prk_area_admin.prk_admin_name,
        prk_area_admin.park_ac_status,
        prk_area_admin.account_number,
        prk_area_dtl.prk_area_name,
        prk_area_dtl.prk_area_short_name,
        prk_area_dtl.prk_area_email,
        prk_area_dtl.prk_area_rep_name,
        prk_area_dtl.prk_area_rep_mobile,
        prk_area_dtl.prk_area_pro_img,
        prk_area_dtl.prk_area_logo_img 
        FROM prk_area_admin,prk_area_dtl
        WHERE prk_area_admin.prk_admin_id=prk_area_dtl.prk_admin_id
        -- AND prk_area_admin.park_ac_status !='".FLAG_A."'
        AND prk_area_dtl.active_flag='".FLAG_Y."'
        ORDER BY 2 ASC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val) 
        {
            $response['status'] = 1;
            $response['message'] = 'Successful';
            $park['prk_admin_id'] = $val['prk_admin_id'];
            $park['prk_admin_name'] = $val['prk_admin_name'];
            $park['park_ac_status'] = $val['park_ac_status'];
            $park['account_number'] = $val['account_number'];
            $park['prk_area_name'] = $val['prk_area_name'];
            $park['prk_area_short_name'] = $val['prk_area_short_name'];
            $park['prk_area_email'] = $val['prk_area_email'];
            $park['prk_area_rep_name'] = $val['prk_area_rep_name'];
            $park['prk_area_rep_mobile'] = $val['prk_area_rep_mobile'];
            $park['prk_area_pro_img'] = $val['prk_area_pro_img'];
            $park['prk_area_logo_img'] = $val['prk_area_logo_img'];
            array_push($allplayerdata, $park);
            $response['parking_area'] = $allplayerdata;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?> 