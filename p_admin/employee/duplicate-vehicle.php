<!-- ################################################
  
Description: 
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
 
<?php include "all_nav/header.php";?>
  <!-- vendor css -->
  <style type="text/css">
    .size{
      font-size: 11px;
      text-transform: uppercase;
    }
    .error_size,.error{
      font-size: 11px;
      color: red;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
    th,tr{
      text-align: center;
    }
  </style> 
  <!-- links for datatable -->
  <link href="../../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../../lib/select2/css/select2.min.css" rel="stylesheet">

  <link href="../../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
  <link href="../../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
  <div class="am-mainpanel">
    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Duplicate Vehicle</h5>
    </div>
  </div>
  <div class="am-mainpanel">
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40" style="padding: 10px;">
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <form id="duplicatePayRecForm" action="" method="post">
            <div class="row">
              <div class="col-lg-2 error_show">
                <div class="form-group">
                  <label class="form-control-label size">Vehicle Number <span class="tx-danger"></span></label>
                  <input type="text" name="veh_number" id="veh_number" placeholder="Vehicle Number" class="form-control">
                </div>
              </div>
              <div class="col-lg-2 error_show">
                <div class="form-group">
                  <label class="form-control-label size">start Date<span class="tx-danger">*</span></label>
                  <input type="text" name="start_date" id="start_date" placeholder="DD/MM/YYYY" class="form-control prevent_readonly" readonly="readonly">
                </div>
              </div>
              <div class="col-lg-2 error_show">
                <div class="form-group">
                  <label class="form-control-label size">End Date<span class="tx-danger">*</span></label>
                  <input type="text" name="end_date" id="end_date" placeholder="DD/MM/YYYY" class="form-control prevent_readonly" readonly="readonly">
                </div>
              </div>
              <div class="col-lg-5">
                <div class="form-layout-footer mg-t-20">
                  <div class="row pd-t-10">
                    <div class="col-lg-5">
                        <button type="button" class="btn btn-block prk_button" id="show">SHOW</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="am-pagebody" id="data">
      <div class="card pd-20 pd-sm-40" style="padding: 10px;">
        <div class="table-wrapper">
          <table id="datatable1" class="table display responsive nowrap" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="wd-5p">Type</th>
                <th class="wd-10p">Vechicle Number</th>
                <th class="wd-10p">In Date</th>
                <th class="wd-10p">In Time</th>
                <th class="wd-10p">Out Date</th>
                <th class="wd-10p">Out Time</th>
                <th class="wd-5p">Action</th>
              </tr>
            </thead>
            <tbody id="dataLines">
            </tbody>
          </table>
        </div><!-- table-wrapper -->
      </div>
    </div>
  <?php include"all_nav/footer.php"; ?>
  <script type="text/javascript">
    var print_in_flag = "<?php echo $print_in_flag ;?>";
    var print_out_flag = "<?php echo $print_out_flag ;?>";
    $( document ).ready( function () {
      duplicateVehicleTransactions('','','');
      $('#show').click(function () {
        if($("#duplicatePayRecForm").valid()){
          // alert('ok');
          var start_date = $('#start_date').val();
          var end_date = $('#end_date').val();
          var veh_number = $('#veh_number').val();
          duplicateVehicleTransactions(start_date,end_date,veh_number);
        }
      });
    });
    addEventListener('DOMContentLoaded', function () {
      var now = new Date;
      var now = now.setDate(now.getDate() - 1);
      pickmeup('#start_date', {
        default_date   : false,
        position       : 'bottom',
        hide_on_select : true,
        render : function (date) {
              /*if (date < now) {
                  return {
                    disabled : true,
                    class_name : 'date-in-past'
                  };
              }*/
              $('#error_start_date').text('');
              return {};
          } 
      });
    });
    $('#end_date').focus(function () {
      var start = new Date;
      var end = $('#start_date').val();
      var d = new Date(end.split("-").reverse().join("-"));
      var dd=d.getDate();
      var mm=d.getMonth()+1;
      var yy=d.getFullYear();
      var hh = start.getHours();
      var min = start.getMinutes();
      var sec = start.getSeconds();
      var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
      var end = new Date(newdate);
      days = (end - start) / (1000 * 60 * 60 * 24)-1;
      days = Math.round(days);
      now_end = start.setDate(start.getDate() + days);
      pickmeup('#end_date', {
        default_date   : false,
        /*position       : 'top',*/
        hide_on_select : true,
        render : function (date) {
              if (date < now_end) {
                  return {
                    disabled : true,
                    class_name : 'date-in-past'
                  };
              }
              $('#error_end_date').text('');

              return {};
          } 
      });
    });
    $('#start_date').focus(function(){
      $('#end_date').val('');
    });
  </script>
  <script src="../../lib/highlightjs/highlight.pack.js"></script>
  <script src="../../lib/datatables/jquery.dataTables.js"></script>
  <script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
  <script src="../../lib/datatables-responsive/jquery.dataTables.min.js"></script>
  <script src="../../lib/datatables-responsive/dataTables.buttons.min.js"></script>
  <script src="../../lib/datatables-responsive/jszip.min.js"></script>
  <script src="../../lib/datatables-responsive/pdfmake.min.js"></script>
  <script src="../../lib/datatables-responsive/vfs_fonts.js"></script>
  <script src="../../lib/datatables-responsive/buttons.html5.min.js"></script>
  <script>
    function duplicateVehicleTransactions(start_date,end_date,veh_number){
      $('#refresh_button').show();
      var urlVehNoCheck = pkrEmpUrl+'duplicate_payment_dtl_list.php';
      // 'prk_admin_id','veh_number','start_date','end_date'
      $.ajax({
        url :urlVehNoCheck,
        type:'POST',
        data :{
          'prk_admin_id':prk_admin_id,
          'veh_number':veh_number,
          'start_date':start_date,
          'end_date':end_date
        },
        dataType:'html',
        success  :function(data){
          // alert(data);
          var demoLines = '';
          var json = $.parseJSON(data);
          if (json.status) {
            for (var key in json.parking) {
              var keyy = json.parking[key];
              var img_url= "<?php echo OPPR_BASE_URL; ?>"+keyy['vehicle_typ_img'];
              demoLines += '<tr>\
                <td class="wd-5p"><img src="'+img_url+'" style="height: 30px;"></td>\
                <td class="wd-10p">'+keyy['veh_number']+'</td>\
                <td class="wd-10p">'+keyy['in_date']+'</td>\
                <td class="wd-10p">'+keyy['in_date']+'</td>\
                <td class="wd-10p">'+keyy['out_date']+'</td>\
                <td class="wd-10p">'+keyy['out_time']+'</td>\
                <td class="wd-5p">';
              if(print_out_flag == 'Y'){
                demoLines += '<button class="out btn prk_button print" id="'+keyy['payment_dtl_id']+'" onclick="duplicatePrint(this.id)" style="color: #FFF;"><i class="fa fa-print"></i> PRINT</button></td></tr>';
              }else{
                demoLines += '<button class="out btn prk_button print" id="'+keyy['payment_dtl_id']+'" onclick="duplicatePrint(this.id)" style="color:white; background-color:#808080;" disabled><i class="fa fa-print"></i> PRINT</button></td></tr>';
              }
            }
          }
          $('#datatable1').dataTable().fnDestroy();
          $("#dataLines").html(demoLines);
          datatable_show();
        }
      });
    }
    function datatable_show(){
      'use strict';
      $('#datatable1').DataTable({
        responsive: true,
        dom: 'Blfrtip',
        language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
        },
        // lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
          {
            text: '<span id="refresh_button"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
            action: function ( e, dt, node, config ) {
              duplicateVehicleTransactions('','','');
              $('#start_date').val('');
              $('#end_date').val('');
              $('#veh_number').val('');
            }
          },
          { 
            extend: 'copyHtml5', 
            footer: true,
            title: 'VISITOR TRANSACTION DETAILS SHEET' 
          },
          { 
            extend: 'excelHtml5', 
            footer: true,
            title: 'VISITOR TRANSACTION DETAILS SHEET'  
          },
          {
              extend: 'pdfHtml5',
              footer: true,
              title: 'VISITOR TRANSACTION DETAILS SHEET',
              customize: function(doc) {
                doc.content[1].margin = [ 60, 0, 0, 0 ] //left, top, right, bottom
              },
              exportOptions: {
                  columns: [1,2,3,4,5,6]
              }
          },
        ]
      });
      $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
      $('#refresh_button').hide();
    }
    function upperCase(a){
      setTimeout(function(){
          a.value = a.value.toUpperCase();
      }, 1);
    }
    function duplicatePrint(str){
      urlPrint = pkrEmpUrl+"cash_memo_out_print.php?id="+str+"&duplicate=Y";
      window.open(urlPrint,'Print Window','width=850,height=600');
      location.reload();
    }
  </script>
<script type="text/javascript">
  $( document ).ready( function () {  
    var urlVehNoCheck = prkUrl+'v_numberChecker.php';
    $( "#duplicatePayRecForm" ).validate( {
      rules: {
        start_date: "required",
        end_date: "required"
      },
      messages: {
      }
    });
  });
</script>
