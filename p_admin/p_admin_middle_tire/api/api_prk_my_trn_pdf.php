<?php
/*
Description: parking employ today payment type history api
Developed by: Manoranjan
Created Date: -------
Update date :27-05-2018
*/
@include '../../../../library/dompdf/autoload.inc.php';
@include '../../../library/dompdf/autoload.inc.php';
@include '../../library/dompdf/autoload.inc.php';
@include '../library/dompdf/autoload.inc.php';
use Dompdf\Dompdf;
function prk_my_trn_pdf($response){
    $res=json_decode($response,true);
    $res=json_decode($response,true);
    $words=api_number_to_word($res['total_pay']);
    $html="<html>
        <head>
        <style>
        .tbl_body {
            width:92%;
            border-collapse: collapse;
            margin-left: 50px;
        }
        th,td{
            border-bottom: 1px solid #ddd;
            padding: 4px;
        }
        .header{
            float: left;
            margin: 10px 0 50px;
        }
        </style>
        </head>
        <body>
        <div>
        <div class='header' style='width:400px;padding-left: 62px;'><img style='height: 70px;padding-left: 50px;' src='./../../img/logo_pdf.png'></div>
        <div class='header' style='width:300px;'>".$res['prk_area_name']."<br>Vehicle Transaction Report</div>
        <div class='header' style='width:200px;'>Start Date: ".$res['start_date']."<br>End Date: ".$res['end_date']."</div>
        </div>
        <table class='tbl_body' style='padding-top:100px;'>
            <tr>
                <th>Vehicle Type</th>
                <th>Vehicle Number</th>
                <th>Transaction Date</th>
                <th>In Date</th>
                <th>Out Date</th>
                <th>Total Hr</th>
                <th>Payment Type</th>
                <th>Total Pay</th>
            </tr>";
            $html3="";
           foreach ($res['payment_history'] as $key => $item){
                foreach ($item as $key_p => $values) {
                    // echo $item['veh_number'];
                    $html1="<tr class='center_align'>
                                <td>".$values['vehicle_type_dec']."</td>
                                <td>".$values['veh_number']."</td>
                                <td>".$values['transition_date']."</td>
                                <td>".$values['veh_in_date']." ".$values['veh_in_time']."</td>
                                <td>".$values['veh_out_date']." ".$values['veh_out_time']."</td>
                                <td>".$values['total_hr']."</td>
                                <td>".$values['payment_type']."</td>
                                <td>".$values['total_pay']."</td>
                            </tr>";
                    $html3.=$html1;
                }
            }
            $html2="
            <tr>
                <th colspan='5'></th>
                <th colspan='3'>Grand Total RS. ".$res['total_pay']."/-</th>
            </tr>
            <tr>
                <td colspan='8'>RUPEES: ".$words."</td>
            </tr>
        </table>
        </body>
        </html>";
    // echo ($mess);
    $html=$html."".$html3."".$html2;
    // echo ($html);
    // use Dompdf\Dompdf;
    // instantiate and use the dompdf class
    $dompdf = new Dompdf();
    $dompdf->loadHtml($html);
    // $dompdf->loadHtml('');
    // (Optional) Setup the paper size and orientation
    $dompdf->setPaper('A4', 'landscape');
    // Render the HTML as PDF
    $dompdf->render();
    // Output the generated PDF to Browser
    $name= uniqid();
    /*web pdf*/
       // $fulpath=$name.'.pdf';
       // $dompdf->stream($name,array('Attachment'=>0));
    $dompdf->stream($name.'.pdf',array('Attachment'=>false));
    // return json_encode($report);
}
?>