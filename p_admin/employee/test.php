<?php 
include "all_nav/header.php";
 ?>
<!-- vendor css -->
  <style type="text/css">
    .size{
      font-size: 11px;
      text-transform: uppercase;
    }
    .error_size{
      font-size: 11px;
      color: red;

    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
  </style> 
  <!-- links for datatable -->
  <link href="../../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../../lib/select2/css/select2.min.css" rel="stylesheet">
  <div class="am-mainpanel"><!-- cloding in footer -->

    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Duplicate Payment Receipt</h5>
    </div><!-- am-pagetitle -->
  </div>

  <div class="am-mainpanel">
  <div class="am-pagebody" id="data">

      <div class="card pd-20 pd-sm-40" style="padding: 10px;">
        <div class="table-wrapper">
          <table id="datatable1" class="table display responsive nowrap">
            <thead>
              <tr>
                <th class="wd-5p">Img</th>
                <th class="wd-20p">Visitor Name</th>
                <th class="wd-20p">Meet To</th>
                <th class="wd-20p">Meet Dtl</th>
                <th class="wd-10p">In Date</th>
                <th class="wd-10p">In Time</th>
                <th class="wd-10p">Out Date</th>
                <th class="wd-10p">Out Time</th>
              </tr>
            </thead>
              <tr>
                <td class="wd-5p">Img</td>
                <td class="wd-20p">Visitor Name</td>
                <td class="wd-20p">Meet To</td>
                <td class="wd-20p">Meet Dtl</td>
                <td class="wd-10p">In Date</td>
                <td class="wd-10p">In Time</td>
                <td class="wd-10p">Out Date</td>
                <td class="wd-10p">Out Time</td>
              </tr>
            <tbody id="dataLines">
            </tbody>
          </table>
        </div><!-- table-wrapper -->
      </div><!-- card -->
<?php include"all_nav/footer.php"; ?>
<script src="../../lib/highlightjs/highlight.pack.js"></script>
<script src="../../lib/datatables/jquery.dataTables.js"></script>
<script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

  });
</script>