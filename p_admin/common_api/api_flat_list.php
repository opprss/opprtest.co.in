<?php
/*
Description: State select.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
function flat_list($prk_admin_id,$tower_id){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
    $sql="SELECT `flat_id`,`flat_name` FROM `flat_details` WHERE `prk_admin_id`='$prk_admin_id' AND `tower_id`='$tower_id' AND `active_flag`='".FLAG_Y."' AND`del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['flat_id'] = $val['flat_id'];
            $park['flat_name'] = $val['flat_name'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['flat_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>