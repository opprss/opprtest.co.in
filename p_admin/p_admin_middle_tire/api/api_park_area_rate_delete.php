<?php
/*
Description: Parking area vehicle rate delete.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function park_area_rate_delete($prk_rate_id,$prk_admin_id,$user_name){
    global $pdoconn;
    $response = array();
    $sql ="UPDATE `prk_area_rate` SET `del_flag`='".FLAG_Y."', `updated_date`='".TIME."',`updated_by`='$user_name' WHERE `prk_admin_id`='$prk_admin_id' AND `prk_rate_id`='$prk_rate_id' AND `del_flag`='".FLAG_N."'";
    $query = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Delete Sucessfull';
        return json_encode($response);
    }else{
        $response['status'] = 0;
        $response['message'] = 'Delete Not Sucessfull';
        return json_encode($response);
    }
}
?>