<!-- ################################################
Description: List of user's vechile | user can delete vehicle from this page.
Developed by: Soemen Banerjee
Created Date: 17-03-2018
 ####################################################-->
<?php  
  include "all_nav/header.php";
  $prk_area_list1=user_add_prk_area_list($user_admin_id);
  $prk_area_list = json_decode($prk_area_list1, true);
  $incident_type_list=incident_type_list($user_admin_id);
  $incident_type_list = json_decode($incident_type_list, true);
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
  .error{
    font-size: 11px;
    color: red;
  }
</style>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script src="../lib/validation/jquery.validate.js"></script>
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
  <div class="am-mainpanel"><!-- cloding in footer -->
    <div class="am-pagetitle" id="nav_1">
      <h5 class="am-title">Incident Details</h5>
    </div><!-- am-pagetitle -->
    
    <div class="am-pagebody">
      <div class="card pd-20 pd-sm-40" >
        <!-- <h6 class="card-body-title">Add Employee</h6> -->
        <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
          <form role="form" id="addGuest" method="post"  enctype="multipart/form-data">
          <div class="row mg-b-25">
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Choose Area<span class="tx-danger">*</span></label>
                <div class="select" style="font-size:14px">
                  <select name="prk_admin_id" id="prk_admin_id" style="color: skyblue">
                    <option value="">SELECT AREA</option>
                     <?php
                      foreach ($prk_area_list['prk_area'] as $val){ ?>  
                        <option value="<?php echo $val['prk_admin_id']?>"><?php echo $val['prk_area_name']?></option>
                      <?php          
                      }
                    ?> 
                  </select>
                </div>
                <span style="position: absolute; display: none;" id="error_prk_admin_id" class="error_size">Select Area</span>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Incident Name<span class="tx-danger">*</span></label>
                <input type="text"  name="inc_name" class="form-control" placeholder="Incident Name" id="inc_name">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label class="form-control-label size">Incident Type<span class="tx-danger">*</span></label>
                <div class="select" style="font-size:14px">
                  <select name="inc_typ" id="inc_typ" style="color: skyblue">
                    <option value="">SELECT TYPE</option>
                     <?php
                      foreach ($incident_type_list['type_list'] as $val){ ?>  
                        <option value="<?php echo $val['all_drop_down_id']?>"><?php echo $val['full_name']?></option>
                      <?php          
                      }
                    ?> 
                  </select>
                </div>
                <span style="position: absolute; display: none;" id="error_inc_typ" class="error_size">Select Type</span>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="form-group">
                <label class="form-control-label size">Incident Details<span class="tx-danger">*</span></label>
                <input type="text"  name="inc_dtl" class="form-control" placeholder="Incident Details" id="inc_dtl">
              </div>
            </div>
            <div class="col-lg-1">
              <div class="form-group mg-t-35">
             <!--  <div class="form-group"> -->
                <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="vis_save" id="vis_save">
              </div>
            </div>
            <div class="col-lg-1">
              <div class="form-group mg-t-35">
              <!-- <div class="form-group"> -->
                <button type="reset" class="btn btn-block prk_button btn-primary">Reset</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div><!-- am-pagebody -->
  </div>
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40">
      <?php $respon=user_incident_list($user_admin_id,$user_mobile);
        // print_r($respon);
      ?>
      <div class="table-wrapper">
        <table id="datatable1" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="wd-3p">Sl No.</th>
              <th class="wd-10p">Incident Numer</th>
              <th class="wd-20p">Incident Name</th>
              <th class="wd-15p">Incident Type</th>
              <th class="wd-25p">Incident Details</th>
              <th class="wd-10p">Incident Date</th>
              <th class="wd-10p">Status</th>
              <th class="wd-10p">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $respon = json_decode($respon, true);
            $x=1;
            foreach($respon['incident_list'] as $value){
             ?>
            <tr>
             <td ><?php echo $x; ?></td>
             <td ><?php echo $value['user_incident_number']; ?></td>
             <td ><?php echo $value['user_incident_name']; ?></td>
             <td><?php echo $value['user_incident_type']; ?></td>
             <td><?php echo $value['user_incident_dtl']; ?></td>
             <td><?php echo $value['incident_in_date']; ?></td>
             <td><?php echo $value['status']; ?></td>
             <td>
                <a href="user-incident-edit?list_id='<?php echo base64_encode($value['user_incident_id']) ?>"><i class="fa fa-pencil" style="font-size:18px; font-size:18px;"></i></a>

                <button class="clean-button" onclick="incident_dalete(this.id)" id="<?php echo $value['user_incident_id']; ?>"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
             </td>
            </tr>
            <?php $x++; } ?>
          </tbody>
        </table>
      </div><!-- table-wrapper -->
    </div>
  </div>
  <!-- footer part -->
  <?php include"all_nav/footer.php"; ?>
  <!-- footer part -->
<script type="text/javascript">
  var userUrl = "<?php echo USER_URL; ?>";
  var user_admin_id = "<?php echo $user_admin_id; ?>";
  var token = "<?php echo $token; ?>";
  var mobile = "<?php echo $user_mobile; ?>";
  // alert(mobile);
  $( document ).ready( function () {  
    $('#vis_save').click(function () {
      // alert('ok');
      var inc_name=$('#inc_name').val();
      var inc_typ=$('#inc_typ').val();
      var inc_dtl=$('#inc_dtl').val();
      var prk_admin_id=$('#prk_admin_id').val();
      if($("#addGuest").valid()){
        $(this).val('Wait ...').prop('disabled', true);
        var user_incident_in = userUrl+'user_incident_in.php';
        //user_incident_name','user_incident_type','user_incident_dtl','prk_admin_id','user_admin_id','mobile','token
        $.ajax({
          url :user_incident_in,
          type:'POST',
          data :{
            'user_incident_name':inc_name,
            'user_incident_type':inc_typ,
            'user_incident_dtl':inc_dtl,
            'prk_admin_id':prk_admin_id,
            'user_admin_id':user_admin_id,
            'mobile':mobile,
            'token':token
          },
          dataType:'html',
          success  :function(data){
            $('#vis_save').val('SAVE').prop('disabled', false);
            // alert(data);
            var json = $.parseJSON(data);
            if (json.status){
              $.alert({
                icon: 'fa fa-smile-o',
                theme: 'modern',
                title: 'Success !',
                content: "<p style='font-size:0.9em;'>Incident Add Successfully</p>",
                type: 'green',
                buttons: {
                  Ok: function () {
                      location.reload();

                  }
                }
              });
            }else{
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
                type: 'red'
              });
              if (json.session==0) {
                window.location.replace("logout.php");
              }
            }
          }
        });
      }
    });
    $( "#addGuest" ).validate( {
      rules: {
        prk_admin_id:"prk_admin_id_check",
        inc_name: "required",
        inc_typ: "inc_typ_check",
        inc_dtl: "required"
      },
      messages: {
        inc_name:"Please Enter the Name"
        // inc_dtl:"Please Enter the Address"
      }
    });
    $.validator.addMethod("inc_typ_check", function(value){
      // alert(value);
      arg = "";
      if (value != "") {
        //return true;
        $("#error_inc_typ").hide();
        return arg !== value;
      }else{
        $("#error_inc_typ").show();
        return false;
      }
    });
    $.validator.addMethod("prk_admin_id_check", function(value){
      // alert(value);
      arg = "";
      if (value != "") {
        //return true;
        $("#error_prk_admin_id").hide();
        return arg !== value;
      }else{
        $("#error_prk_admin_id").show();
        return false;
      }
    });
  });
  function incident_dalete(user_incident_id){
    // alert(user_incident_id);
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.9em;'>Delete Incident List</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            var user_incident_delete = userUrl+'user_incident_delete.php';
            $.ajax({
              url :user_incident_delete,
              type:'POST',
              data :{
                'user_incident_id':user_incident_id,
                'user_admin_id':user_admin_id,
                'mobile':mobile,
                'token':token
              },
              dataType:'html',
              success  :function(data){
                // alert(data);
                var json = $.parseJSON(data);
                if (json.status){
                  $.alert({
                    icon: 'fa fa-smile-o',
                    theme: 'modern',
                    title: 'Success !',
                    content: "<p style='font-size:0.9em;'>Guest Delete Successfully</p>",
                    type: 'green',
                    buttons: {
                      Ok: function () {
                          location.reload();
                      }
                    }
                  });
                }else{
                  $.alert({
                    icon: 'fa fa-frown-o',
                    theme: 'modern',
                    title: 'Error !',
                    content: "<p style='font-size:0.7em;'>Somting went wrong</p>",
                    type: 'red'
                  });
                  if(json.session==0) {
                    window.location.replace("logout.php");
                  }
                }
              }
            });
          }
        }
      }
    });
  }
</script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
</script>
