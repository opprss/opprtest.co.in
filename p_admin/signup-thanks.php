<!-- ################################################
  
Description: After successfull registration this page will dispaly with some static content.
Developed by: Soemen Banerjee
Created Date: 18-03-2018

 ####################################################-->
<?php
@session_start();
if(isset($_SESSION['prk_admin_id'])){
header('location:dashboard');
}
include('../global_asset/config.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">


    <title>OPPR | Thank You</title>
    <link rel="shortcut icon" href="../img/favicon.png" type="image/x-icon">
    <!-- vendor css -->
    <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Amanda CSS -->
    <link rel="stylesheet" href="../css/amanda.css">
    <!-- font -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- custom css -->
    <link rel="stylesheet" type="text/css" href="css/thank-you-style.css">
  </head>

  <body style="">
    <div class="container-fluid ht-100v">
        <div class="row" >
            <div class="col-lg-12 heaing" style="">
                <img src="<?php echo LOGO ?>" width="180px" class="pd-b-0">
            </div>
            <div class="col-lg-12">
                <div class=" d-flex align-items-center justify-content-center">

                <div class="wd-lg-100p wd-xl-70p tx-center pd-x-40 pd-t-30">
                    <i class="fa fa-check fa-5x tick-icon" style="font-size: 40px"></i>
                    <h1 class="tx-50 tx-xs-50  tx-gray-800 mg-b-0">Thank You!</h1>
                    <p class="tx-16 mg-b-30 mg-t-20">
                        <?php 
                        echo THANK_YOU_MSG;
                        ?>
                    </p>

                    <div class="justify-content-center">

                      <div class="contact-details" style="padding-bottom:0px;margin-top: 0px">

                      <span style="float:left; font-size:30px;margin:0px; padding:0px;width:10px;margin-left:30px;padding-top: 0px;margin-top: 0px;"><i class="fa fa-envelope">											 
   			</i></span>
                        
                        <p class="mg-b-0">
                             &nbsp; <?php echo OPPRS_SALES_EMAIL;?></p><p> &nbsp;<?php echo OPPRS_CARE_EMAIL;?> </p>
                      </div>
                      &nbsp;
                      
                      <div class="contact-details" style="padding-bottom:0px;margin-top: 0px">
                        
                            <span style="float:left; font-size:30px;margin:0px; padding:0px;width:10px;margin-left:30px;padding-top: 0px;margin-top: 0px;">
                        <i class="fa fa-phone-square"></i> </span>
                        <p class="mg-b-0">
                        &nbsp;<?php  echo OPPRS_THANK_YOU_MOBILE; ?></p><p>&nbsp;+91-6290032500</p>
                        
                      </div>
                    </div><!-- d-flex -->

                    <div class="tx-center mg-t-30 footer">
                        <p class="tx-16 mg-b-30">Follow us</p>
                        <div class="social tx-20">
                            <a href="https://www.facebook.com/opprskolkata/" class="fa fa-facebook" target="_BLANK"></a>
                            
                            <a href="https://twitter.com/opprsskolkata" class="fa fa-twitter" aria-hidden="true" target="_BLANK"></a>
                            
                            <a href="https://plus.google.com/u/0/115462157570652821420" target="_BLANK" class="fa fa-google"></a>
                            <a href="https://www.instagram.com/opprsskolkata/" target="_BLANK" class="fa fa-instagram" aria-hidden="true"></a>
                            
                            <a href="https://www.linkedin.com/in/oppr-software-solution-191108163/"  target="_BLANK" class="fa fa-linkedin"></a>
                        </div>
                        <a href="../signin.php" class="btn btn-info tx-orange hover-info" style="color:white">Home</a> 
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
  </body>
</html>
