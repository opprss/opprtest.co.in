<?php
/*
Description: parking area employ update.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
    // require_once 'api/global_api.php';
    require_once 'api/parkAreaReg_api.php';
    $response = array();
 
    if(isAvailable(array('prk_area_user_id','prk_admin_id','prk_user_username','prk_user_emp_no','prk_user_name','prk_user_mobile','prk_user_gender','prk_user_address','prk_user_landmark','prk_user_state','prk_user_city','prk_user_pincode','inserted_by','prk_user_emp_category'))){

        if(isEmpty(array('prk_area_user_id','prk_admin_id','prk_user_username','prk_user_emp_no','prk_user_name','prk_user_mobile','prk_user_gender','prk_user_address','prk_user_state','prk_user_city','prk_user_pincode','inserted_by','prk_user_emp_category'))){
            $prk_area_user_id = trim($_POST['prk_area_user_id']);
            $prk_admin_id = trim($_POST['prk_admin_id']);
            $prk_user_username = trim($_POST['prk_user_username']);
            $prk_user_emp_no = trim($_POST['prk_user_emp_no']);
            $prk_user_name = trim($_POST['prk_user_name']);
            $prk_user_mobile = trim($_POST['prk_user_mobile']);
            $prk_user_gender = trim($_POST['prk_user_gender']);
            $prk_user_address = trim($_POST['prk_user_address']);
            $prk_user_landmark = trim($_POST['prk_user_landmark']);
            $prk_user_state = trim($_POST['prk_user_state']);
            $prk_user_city = trim($_POST['prk_user_city']);
            $prk_user_pincode = trim($_POST['prk_user_pincode']);
            $inserted_by = trim($_POST['inserted_by']);
            $prk_user_emp_category = trim($_POST['prk_user_emp_category']);
            $prk_user_img = isset($_POST['prk_user_img']) ? trim($_POST['prk_user_img']) : '';
            if (!empty($prk_user_img)) {
                $PNG_TEMP_DIR ='./../uploades/'.$inserted_by.'/prk_user_img/';
                if (!file_exists($PNG_TEMP_DIR)) {
                    $old_mask = umask(0);
                    mkdir($PNG_TEMP_DIR, 0777, TRUE);
                    umask($old_mask);
                }
                $image_parts = explode(";base64,", $prk_user_img);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];
              
                $image_base64 = base64_decode($image_parts[1]);
                $fileName = uniqid() . '.png';
                $file = $PNG_TEMP_DIR . $fileName;
                file_put_contents($file, $image_base64);
                $prk_user_img='uploades/'.$inserted_by.'/prk_user_img/'.$fileName;
            }else{
                $sql="SELECT `prk_user_img` FROM `prk_area_user` WHERE `prk_admin_id`='$prk_admin_id' AND `prk_area_user_id`='$prk_area_user_id' AND `del_flag`='".FLAG_N."'";
                $query  = $pdoconn->prepare($sql);
                $query->execute();
                $val = $query->fetch();
                $prk_user_img = $val['prk_user_img'];
            }
            $response = prk_area_user_update($prk_area_user_id,$prk_admin_id,$prk_user_username,$prk_user_emp_no,$prk_user_name,$prk_user_mobile,$prk_user_gender,$prk_user_address,$prk_user_landmark,$prk_user_state,$prk_user_city,$prk_user_pincode,$inserted_by,$prk_user_emp_category,$prk_user_img);

        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            $response = json_encode($response);
        }
    }else{
         $response['status'] = 0; 
         $response['message'] = 'Invalid API Call';
         $response = json_encode($response);
    }
    echo($response);
?>