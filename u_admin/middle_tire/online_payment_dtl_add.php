<?php 
/*
Description: user address show.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/
require_once 'api/reg_api.php';
$response = array();
if(isAvailable(array('user_admin_id','mobile','token','payment_company','payment_for','transaction_id_for','status','checksumhash','mid','orderid','bankname','txnamount','txndate','txnid','respcode','paymentmode','banktxnid','currency','gatewayname','respmsg'))){
 	if(isEmpty(array('user_admin_id','mobile','token','payment_company','payment_for'))){
        $user_admin_id = trim($_POST['user_admin_id']);
        $inserted_by = trim($_POST['mobile']);
        $token = trim($_POST['token']);
        $payment_company = trim($_POST['payment_company']);
        $payment_for = trim($_POST['payment_for']);
        $transaction_by = isset($_POST['transaction_by']) ? trim($_POST['transaction_by']) : 'U';
        $transaction_id_for = trim($_POST['transaction_id_for']);
        $status = trim($_POST['status']);
        $checksumhash = trim($_POST['checksumhash']);
        $mid = trim($_POST['mid']);
        $orderid = trim($_POST['orderid']);
        $bankname = trim($_POST['bankname']);
        $txnamount = trim($_POST['txnamount']);
        $txndate = trim($_POST['txndate']);
        $txnid = trim($_POST['txnid']);
        $respcode = trim($_POST['respcode']);
        $paymentmode = trim($_POST['paymentmode']);
        $banktxnid = trim($_POST['banktxnid']);
        $currency = trim($_POST['currency']);
        $gatewayname = trim($_POST['gatewayname']);
        $respmsg = trim($_POST['respmsg']);
        // $resp=user_token_check($user_admin_id,$token);
        // $json = json_decode($resp);
        // if($json->status=='1'){ 
            $response = online_payment_dtl_add($payment_company,$payment_for,$transaction_by,$transaction_id_for,$status,$checksumhash,$mid,$orderid,$bankname,$txnamount,$txndate,$txnid,$respcode,$paymentmode,$banktxnid,$currency,$gatewayname,$respmsg,$inserted_by);
        // }else{
        //     $response = $resp; 
        // }
 	}else{
        $response['status'] = 0;
 		$response['message'] = 'All Fields Are Mandatory';
        $response =  json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo($response);
?>