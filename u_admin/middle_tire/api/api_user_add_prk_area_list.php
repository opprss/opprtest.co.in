<?php 
/*
Description: subumit lisst.
Developed by: Rakhal Raj Mandal
Created Date: 14-06-2018
Update date :---------
*/
function user_add_prk_area_list($user_admin_id){
    $response = array();
    $allplayerdata = array();
    $park= array();
    global $pdoconn;
	$sql="SELECT `user_address`.`user_add_id`,
		`user_address`.`prk_admin_id`,
		`prk_area_dtl`.`prk_area_name`
		FROM `user_address`,`prk_area_dtl` 
		WHERE `user_address`.`user_admin_id`='$user_admin_id'
		AND `user_address`.`active_flag` ='".FLAG_Y."' 
		AND `user_address`.`del_flag`='".FLAG_N."'
		AND `user_address`.`user_add_verify_flag` ='".FLAG_Y."' 
		AND `prk_area_dtl`.`prk_admin_id`=`user_address`.`prk_admin_id` 
		AND `prk_area_dtl`.`active_flag`='".FLAG_Y."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
	    $arr_catagory = $query->fetchAll();
	    foreach($arr_catagory as $val)
	    {
	        $park['prk_admin_id'] = $val['prk_admin_id'];
	        $park['prk_area_name'] = $val['prk_area_name'];
	        array_push($allplayerdata, $park);
	    }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['prk_area'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>