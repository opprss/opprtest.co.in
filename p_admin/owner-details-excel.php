<!-- ################################################
  
Description: Tower detalis.
Developed by: Rakhal Raj 
Created Date: 12-03-2018

 ####################################################-->
<?php include "all_nav/header.php";?>
<?php
$owner_email=''; 
$owner_gender=''; 
$owner_id=''; 
$owner_name='';
$owner_mobile='';
$effective_date='';
$end_date='';
$tower_id='';
$flat_id='';
$crud_type='LS';
// $flat_details=flat_details($prk_admin_id,$tower_name,$effective_date,$end_date,$parking_admin_name,$tower_id,$crud_type);
$owner_details=owner_details($prk_admin_id,$tower_id,$flat_id,$owner_name,$owner_mobile,$owner_email,$owner_gender,$owner_id,$effective_date,$end_date,$parking_admin_name,$crud_type);
$owner_details_list = json_decode($owner_details, true);

//$tower_lis = tower_list($prk_admin_id);
//$tower_list = json_decode($tower_lis, true);
$category_type='APT_FLT_HLD';
$prk_user_emp_cat = common_drop_down_list($category_type);
$prk_user_emp_category = json_decode($prk_user_emp_cat, true);
?>

<!-- header position -->
  <style type="text/css">
    .size{
      font-size: 11px;
    }
    .deactivate-bg{
      background-color: #ffe5e5 !important;
    }
    .error-text::-webkit-input-placeholder {
      color: red;
      border-color: red !important;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .error{
      font-size: 11px;
      color: red;
    }
    .error_size{
      font-size: 11px;
      color: red;
    }
    .success{
      font-size: 11px;
      color: green;
    }
  </style>
  <!-- links for datatable -->
  <link href="../lib/highlightjs/github.css" rel="stylesheet">
  <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
  
    <div class="am-mainpanel">

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Owner Info<//?=$owner_details?></h5>
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40" >
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <form id="park_owner">
              <div class="row mg-b-25">
                <div class="col-lg-4">
                  <div class="form-group mg-t-20">
                    <input type="file" class="form-control"  id="file_path" name="file_path">
                  </div>
                </div>
                <!-- <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">OWNER NAME<span class="tx-danger">*</span></label>
                    <input type="text" name="owner_name" id="owner_name" placeholder="Owner Name" class="form-control prevent_readonly">
                    <span style="position: absolute; display: none;" id="error_owner_name" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">MOBILE NUMBER<span class="tx-danger"></span></label>
                    <input type="text" name="owner_mobile" id="owner_mobile" placeholder="Mobile No." class="form-control prevent_readonly" maxlength="10">
                    <span style="position: absolute; display: none;" id="error_owner_mobile" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">EMAIL ID<span class="tx-danger"></span></label>
                    <input type="text" name="owner_email" id="owner_email" placeholder="EMAIL ID" class="form-control prevent_readonly" style="text-transform: none;">
                    <span style="position: absolute; display: none;" id="error_owner_email" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">Gender<span class="tx-danger">*</span></label>
                      <label class="rdiobox">
                        <input type="radio" name="owner_gender" id="own_gender_m" value="M" type="radio" checked>
                        <span>Male </span>
                      </label>

                      <label class="rdiobox">
                        <input type="radio" name="owner_gender" id="own_gender_f" value="F" type="radio">
                        <span>Female </span>
                      </label>
                      <span style="position: absolute;" id="error_own_gender" class="error_size"></span>
                  </div>
                </div> -->
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">EFFECTIVE DATE<span class="tx-danger">*</span></label>
                    <input type="text" name="effective_date" id="effective_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                    <span style="position: absolute; display: none;" id="error_effective_date" class="error">This field is required.</span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label size">END DATE</label>
                    <input type="text" name="end_date" id="end_date" placeholder="dd-mm-yyyy" class="form-control prevent_readonly" readonly="readonly">
                   <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-layout-footer mg-t-30">
                    <div class="row">
                      <div class="col-lg-6">
                        <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
                      </div>
                      <div class="col-lg-6">
                        <button type="reset" value="Reset" id="reset" class="btn btn-block prk_button_skip">RESET</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- data table -->
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40">
          <div class="table-wrapper">
            <table id="datatable1" class="table display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th >Sl No.</th>
                  <th >Owner Name</th>
                  <th >Mobile Number</th>
                  <th >Email Id</th>
                  <th >Effective Date</th>
                  <th >End Date</th>
                  <th >Action</th>
                </tr>
              </thead>
                <?php if($owner_details_list['status']){$c=0;
                foreach($owner_details_list['owner_list'] as $value){$c+=1;?>
                  <tr style="<?php if($value['activ_status']==FLAG_N){ ?>background-color: bisque;<?php }?>">
                    <td ><?=$c;?></td>
                    <td ><?=$value['owner_name'];?></td>
                    <td ><?=$value['owner_mobile'];?></td>
                    <td ><?=$value['owner_email'];?></td>
                    <td ><?=$value['eff_date'];?></td>
                    <td ><?=$value['end_date'];?></td>
                    <td >
                      <a href="<?php echo 'owner?list_id='.base64_encode($value['owner_id']);?>" class="pd-l-10" data-toggle="tooltip" data-placement="top" title="Modify"><i class="fa fa-pencil" style="font-size:18px; font-size:18px;"></i></a>
                      <button type="button" class="clean-button" name="delete" id="<?php echo $value['owner_id'];?>" data-toggle="tooltip" onclick="owner_delete(this.id)" data-placement="top" title="Delete"><i class="fa fa-trash-o" style="font-size:18px; color:red;"></i></button>
                    </td>
                  </tr>
                <?php }}?>
            </table>
          </div>
        </div>
      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#effective_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
        if (date < now) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        // $('#error_effective_date').text('');
        return {};
      } 
    });
  });
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#effective_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
        if (date < now_end) {
            return {
              disabled : true,
              class_name : 'date-in-past'
            };
        }
        return {};
      } 
    });
  });
  $('#effective_date').focus(function(){
    $('#end_date').val('');
  });
</script>
<!-- scripts for datatable -->
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
  $(function(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search Here',
        sSearch: '',
        lengthMenu: '_MENU_ Page',
      }
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });
</script>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE ?>";
  var prk_admin_id="<?=$prk_admin_id?>"
  var parking_admin_name="<?=$parking_admin_name?>"
  var token="<?=$token?>"
  var urlPrkAreGateDtl = prkUrl+'owner_details_excel.php';
  // alert(token);
  // $( document ).ready( function () {
  $('#save').click(function(){
    var effective_date=($("#effective_date").val());
    var end_date=($("#end_date").val());
    var str_array = end_date.split('-');
    var dd = str_array[0];
    var mm = str_array[1];
    var yy = str_array[2];
    var end_dat = yy+'-'+mm+'-'+dd;
    end_date = (!end_date == '') ? end_dat : FUTURE_DATE;
    // var owner_name=($("#owner_name").val());
    // var owner_mobile=($("#owner_mobile").val());
    // var owner_email=($("#owner_email").val());
    // var owner_gender =  $('input[name=owner_gender]:checked').val();
    var str_array = effective_date.split('-');
    var dd = str_array[0];
    var mm = str_array[1];
    var yy = str_array[2];
    var effective_dat = yy+'-'+mm+'-'+dd;
    var file_data = $('#file_path').prop('files')[0];
    if($("#park_owner").valid()&effective_date!=''){
      var form_data = new FormData();
      form_data.append('prk_admin_id', prk_admin_id);
      form_data.append('owner_name', '');
      form_data.append('owner_mobile', '');
      form_data.append('owner_email', '');
      form_data.append('owner_gender', '');
      form_data.append('effective_date', effective_dat);
      form_data.append('end_date', end_date);
      form_data.append('parking_admin_name', parking_admin_name);
      form_data.append('crud_type', 'I');
      form_data.append('owner_id', '');
      form_data.append('token', token);
      form_data.append('file', file_data);

      $('#save').val('Wait ...').prop('disabled', true);
      //'prk_admin_id','owner_name','owner_mobile','owner_email','owner_gender','living_status','parking_admin_name','effective_date','end_date','owner_id','token','crud_type'
      $.ajax({
        url :urlPrkAreGateDtl,
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,   
        type:'POST',
        dataType:'html',
        success  :function(data){
          // alert(data);
          $('#save').val('SAVE').prop('disabled', false);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success !',
              content: "<p style='font-size:0.9em;'>Owner Details Added Successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                    location.reload(true);
                }
              }
            });
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.9em;'>Something went wrong</p>",
              type: 'red'
            });
            }
          }
        }
      });
    }
  });
  function owner_delete(owner_id){
    // alert(tower_id);
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will delete the Owner permanently</p>",
      theme: 'modern',
      type: 'red',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Delete',
          btnClass: 'btn-red',
          keys: ['Y', 'shift'],
          action: function(){
            /*if (owner_id != '') {
              $.ajax({
                url :urlPrkAreGateDtl,
                type:'POST',
                data :{
                  'prk_admin_id':prk_admin_id,
                  'tower_id':'',
                  'flat_id':'',
                  'owner_name':'',
                  'owner_mobile':'',
                  'owner_email':'',
                  'owner_gender':'',
                  'effective_date':'',
                  'end_date':'',
                  'parking_admin_name':parking_admin_name,
                  'crud_type':'D',
                  'owner_id':owner_id,
                  'token': token
                },
                dataType:'html',
                success  :function(data)
                {
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Owner Details Deleted Successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                          location.reload(true);
                        }
                      }
                    });
                  }else{
                     $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.7em;'>Something went wrong 1</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{*/
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.8em;'>Delete Not Allow this Page Allow</p>",
                type: 'red'
              });
            // }
          }
        }
      }
    });
  }
</script>
<script type="text/javascript">
  $( document ).ready( function () {
    $( "#park_owner" ).validate( {
      rules: {
        file_path: "required",
        // owner_name: "required",
        /*owner_email: { 
          required: true,
          accept:true 
        },
        owner_mobile: {
          required: true,
          number: true,
          minlength: 10,
        },*/
        effective_date: "valueNotEqualsEff"
      },
      messages: {
        owner_mobile: {
          required: "This field is required.",
          number: "Enter valid mobile Numberr.",
          minlength: "Enter valid mobile Number."
        }
      }
    });
    $("#owner_name").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 60) && (inputValue != 0 & inputValue !=32)){
        // if(!(inputValue >= 97) && (inputValue != 46)){
        event.preventDefault();
      }
    });
    $("#owner_mobile").keypress(function(event){
      var inputValue = event.charCode;
      if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 0)){
        event.preventDefault();
      }
    });
    $("#effective_date").blur(function () {
      var effective_date = $('#effective_date').val();
      if(effective_date == ''){
        $('#error_effective_date').show();
        return false;
      }else{
        $('#error_effective_date').hide();
        return true;
      }
    });
    $("#owner_living_status").on('change', function(){
      var v_type = $('#owner_living_status').val();
      if(v_type == "")
      {
        $('#error_owner_living_status').show();
        return false;
      }else{
          $('#error_owner_living_status').hide();
          return true;
      }
    });
  });
  $.validator.addMethod("valueNotEqualsEff", function(value){
    // alert('ok');
    arg = "";
    if (value != "") {
      return arg !== value;
    }else{
      $("#error_effective_date").show();
      return true;
    }
  });
  $.validator.addMethod("valueNotEqualsliving", function(value){
    // alert(value);
    arg = "";
    if (value != "") {
      //return true;
      return arg !== value;
    }else{
      $("#error_owner_living_status").show();
      return false;
    }
  });
  jQuery.validator.addMethod("accept", function(value, element, param) {

    return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
  },'please enter a valid email');
  $('input[type=text]').keyup(function(){
    // alert('ok');
    this.value = this.value.toUpperCase();
  });
  $('#owner_email').keyup(function(){
    // alert('ok');
    this.value = this.value.toLowerCase();
  });
</script>