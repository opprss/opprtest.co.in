<?php
function common_drop_down_list($category_type){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql ="SELECT `all_drop_down_id`,`sort_name`,`full_name`,`description` FROM `all_drop_down` WHERE `category`='$category_type' AND `del_flag`='".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val) {
            $park['all_drop_down_id'] = $val['all_drop_down_id'];
            $park['sort_name'] = $val['sort_name'];
            $park['full_name'] = $val['full_name'];
            $park['description'] = $val['description'];
            
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['drop_down'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response); 
}
?>