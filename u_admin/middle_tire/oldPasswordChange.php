<?php 
/*
Description: user old passwod change. 
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :30-03-2018
*/ 
 require_once 'api/reg_api.php';
// include_once 'api/global_api.php';
 $response = array();
 if(isAvailable(array('mobile','oldpassword'))){
 	if(isEmpty(array('mobile','oldpassword'))){

	 	$mobile = trim($_POST['mobile']);
	 	$oldpassword = trim($_POST['oldpassword']);
        
        $response=oldPasswordChange($mobile,$oldpassword);
        echo ($response);
 	}else{
            $response['status'] = 0;
	 		$response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
    }
 }else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
 echo json_encode($response);
 }
?>