<?php
/*
Description: Parking adimn Address save.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function prk_address($prk_address,$prk_land_mark,$prk_add_area,$prk_add_city,$prk_add_state,$prk_add_country,$prk_add_pin,$prk_admin_id,$inserted_by){
   $response = array();
    global $pdoconn;
    $sql = "INSERT INTO `prk_area_address`(`prk_address`,`prk_land_mark`,`prk_add_area`,`prk_add_city`,`prk_add_state`,`prk_add_country`,`prk_add_pin`,`prk_admin_id`,`inserted_by`,`inserted_date`) VALUE('$prk_address','$prk_land_mark','$prk_add_area','$prk_add_city','$prk_add_state','$prk_add_country','$prk_add_pin','$prk_admin_id','$inserted_by','".TIME."')";
    $query  = $pdoconn->prepare($sql);
    if($query->execute()){
        $response['status'] = 1;
        $response['message'] = 'Address Save';
    }else{
        $response['status'] = 0;
        $response['message'] = 'Address Not Save ';
    }  
    return json_encode($response); 
}
?>