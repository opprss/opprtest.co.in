<!-- ################################################
  
Description: user full profile show.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php
include "all_nav/header.php"; 

if (isset($_SESSION['user_admin_id'])) {

  $user_admin_id = $_SESSION['user_admin_id'];
  $response = array();
  $response_address = array();

  $response = user_profile_show($user_admin_id);
  $response = json_decode($response, true);
  if (!empty($response)) {
    $user_name = $response['user_name'];
    $user_nick_name = $response['user_nick_name'];
    $user_gender = $response['user_gender'];
    $user_dob = $response['user_dob'];
    $user_email = $response['user_email'];
    $user_email_verify = $response['user_email_verify'];
    $user_aadhar_id = $response['user_aadhar_id'];  
    $user_img = $response['user_img'];  
    $visitor_verify_flag = $response['visitor_verify_flag'];  
    
  }

  /*$response_address = user_address_show($user_admin_id);
  $response_address = json_decode($response_address, true);
  
  if ($response_address['status'] == 1) {
    $user_address = $response_address['user_address'];
    $landmark = $response_address['landmark'];
    $user_add_area = $response_address['user_add_area'];
    $user_add_city = $response_address['user_add_city'];
    $user_add_state = $response_address['user_add_state'];
    $user_add_country = $response_address['user_add_country'];
    $user_add_pin = $response_address['user_add_pin'];
    $state_name = $response_address['state_name'];
    $city_name = $response_address['city_name'];
  }*/
}
?>

<!-- header position -->

    <div class="am-mainpanel"><!-- cloding in footer -->

      <div class="am-pagetitle" id="nav_1">
        <h5 class="am-title">Profile<?php //echo $visitor_verify_flag; ?></h5>
        <!-- <form id="searchBar" class="search-bar" action="#">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div>
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form>search-bar-->
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <!-- your content goes here -->
        <!-- add employee form -->
        <div class="card single pd-20 pd-sm-40 col-md-12">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">

            <div class="row">
                <div class="col-md-2 text-center mg-b-20 mg-t-20">
                  
                  <?php
                    if (!empty($user_img)) {
                  ?>
                    <img src="<?php echo USER_BASE_URL.$user_img;?>" alt="" style="width: 100px;">                      
                  <?php
                    }else{
                  ?>
                    <i class="fa fa-user-circle-o tx-info tx-100 tx"></i>
                  <?php
                    }
                  ?>
                </div>
                <div class="col-md-10 pd-l-40">
                  <h5><?php echo $user_name ?></h5>
                  <h6><?php echo $user_nick_name ?></h6>
                  <hr>
                    <div class="tx-15">
                      <a href="edit-profile" class="btn btn-info btn-icon mg-b-20 pull-right" data-toggle="tooltip" data-placement="top" title="Modify basic">
                      <div style=""><i class="fa fa-pencil"></i></div>
                    </a>
                      <p class="mg-b-0">Mobile - <?php echo $user_mobile ?> <font color="green"><i class="fa fa-check" aria-hidden="true"></i></font></p>
                      <p class="mg-b-0">Email - <?php echo $user_email ?> 
                        <?php
                          if ($user_email_verify == "T") {
                           ?>
                           <font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>
                           <?php 
                          }else{
                          ?>
                          <a class="verify" href="verify-email" data-toggle="tooltip" data-placement="top" title="Verify">
                          <i class="fa fa-times"></i>
                        </a>
                          <?php
                          }
                         ?>
                      </p>
                      <p class="mg-b-0">DOB - <?php echo $user_dob ?></p>
                      <p class="mg-b-0">Gender - <?php
                       		//echo $user_gender
                       		if($user_gender == "M")
                       			echo "Male";
                       		elseif($user_gender == "F")
                       			echo "Female";
                       		else
                       			echo "Other";
                       ?></p>
                      <p class="mg-b-0">Aadhar ID - <?php if (isset($user_aadhar_id) && !empty($user_aadhar_id)) echo $user_aadhar_id; else echo "N/A"; ?></p>
                      <p class="mg-b-0">Visitor Verify -
                        <select style="width:70px;" id="visitor_verify">
                          <option value="Y">Yes</option>
                          <option value="N">NO</option>
                        </select><i class="fa fa-caret-down" aria-hidden="true"></i>
                      </p>
                      
                      
                    </div>
                  <!-- <hr>
                    <div class="tx-15">
                      <a href="address" class="btn btn-info btn-icon mg-b-20 pull-right" data-toggle="tooltip" data-placement="top" title="Modify address">
                      <div style=""><i class="fa fa-pencil"></i></div>
                      </a>
                    
                      <p class="mg-b-0">Address - <?php if (isset($user_address)) echo $user_address; else echo "N/A" ;?></p>
                      <p class="mg-b-0">Landmark - <?php if (isset($landmark) && !empty($landmark)) echo $landmark; else echo "N/A"; ?></p>
                      <p class="mg-b-0">Area - <?php if (isset($user_add_area)) echo $user_add_area; else echo "N/A"; ?></p>
                      <p class="mg-b-0">City - <?php if (isset($city_name)) echo $city_name; else echo "N/A" ;?></p>
                      <p class="mg-b-0">State - <?php if (isset($state_name)) echo $state_name; else echo "N/A" ;?></p>
                      <p class="mg-b-0">Country - <?php if (isset($user_add_country)) echo $user_add_country; else echo "N/A" ;?></p>
                      <p class="mg-b-0">Pin - <?php if (isset($user_add_pin)) echo $user_add_pin; else echo "N/A"; ?></p>
                    </div> -->
                </div>
            </div>
          </div>
        </div><!-- card -->
      </div><!-- am-pagebody -->
      

      <!-- footer part -->
<?php include"all_nav/footer.php"; ?>
      <!-- footer part -->
<script type="text/javascript">
  var usrUrl = "<?php echo USER_URL; ?>"; 
  var user_admin_id = "<?php echo $user_admin_id; ?>"; 
  var user_mobile = "<?php echo $user_mobile; ?>"; 
  var token = "<?php echo $token; ?>"; 
  var visitor_verify_flag = "<?php echo $visitor_verify_flag; ?>"; 
  var visitor_verif = usrUrl+'user_visitor_verify.php';

  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
    $('#visitor_verify').val(visitor_verify_flag);

    $('#visitor_verify').on("change", function() {
      var visitor_verify=($("#visitor_verify").val());
      $.ajax({
        url :visitor_verif,
        type:'POST',
        data :
        {
          'visitor_verify_flag':visitor_verify,
          'user_admin_id':user_admin_id,
          'mobile':user_mobile,
          'token':token
        },
        dataType:'html',
        success  :function(data)
        {
          // alert(data);
          var json = $.parseJSON(data);
          if (json.status){
            $.alert({
              icon: 'fa fa-smile-o',
              theme: 'modern',
              title: 'Success',
              content: "<p style='font-size:0.9em;'>Successfully</p>",
              type: 'green',
              buttons: {
                Ok: function () {
                    location.reload(true);
                }
              }
            });
          }else{                
            if (!json.session) {
              window.location.replace("logout.php");
            }else{
              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                type: 'red',
                buttons: {
                  Ok: function () {
                      location.reload(true);
                  }
                }
              });
            }
          }
        }
      });
    });
  });
</script>