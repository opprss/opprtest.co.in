<?php
/*
Description: parking area employ create.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function flat_master_add($prk_admin_id,$tower_id,$flat_id,$owner_id,$living_status,$tenant_id,$effective_date,$end_date,$inserted_by,$flat_master_id,$two_wh_spa_allw,$four_wh_spa_allw,$two_wh_spa,$four_wh_spa,$contact_number,$living_name,$app_use,$res_mem_id,$club_membership,$oth1,$oth2,$oth3,$oth4,$oth5){
    $response = array();
    $allplayerdata = array();
    global $pdoconn;
    $sql="SELECT `flat_master_id` FROM `flat_master` WHERE `tower_id`='$tower_id' AND `flat_id`='$flat_id' AND `prk_admin_id`='$prk_admin_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $val = $query->fetch();
        $flat_master_id =  $val['flat_master_id'];
    }else{
        #...
        $flat_master_id=uniqid();
    }
    $end_date_lod=date("Y-m-d",strtotime($effective_date." -1 day"));
    $sql="UPDATE `flat_master` SET 
        `end_date`='$end_date_lod',
        `updated_by`='$inserted_by',
        `updated_date`='".TIME."' 
        WHERE `tower_id`='$tower_id' 
        AND `flat_id`='$flat_id' 
        AND `prk_admin_id`='$prk_admin_id' 
        AND `active_flag`='".FLAG_Y."' 
        AND `del_flag`='".FLAG_N."' 
        AND '".TIME_TRN."' BETWEEN `effective_date` AND `end_date`";
    $query  = $pdoconn->prepare($sql);
    if($query->execute()){
        $sql="UPDATE `flat_master` SET 
            `active_flag`='".FLAG_N."',
            `updated_by`='$inserted_by',
            `updated_date`='".TIME."' 
            WHERE `tower_id`='$tower_id' 
            AND `flat_id`='$flat_id' 
            AND `active_flag`='".FLAG_Y."' 
            AND `del_flag`='".FLAG_N."' 
            AND `end_date`<'".TIME_TRN."'
            AND `prk_admin_id`='$prk_admin_id'";
        $query  = $pdoconn->prepare($sql);
        $query->execute();
        $sql="INSERT INTO `flat_master`(`flat_master_id`, `prk_admin_id`, `flat_id`, `tower_id`, `owner_id`, `living_status`, `tenant_id`, `res_mem_id`, `effective_date`, `end_date`, `inserted_by`, `inserted_date`, `two_wh_spa_allw`, `four_wh_spa_allw`, `contact_number`, `two_wh_spa`, `four_wh_spa`, `living_name`, `app_use`, `club_membership`) VALUES ('$flat_master_id','$prk_admin_id','$flat_id','$tower_id','$owner_id','$living_status','$tenant_id','$res_mem_id','$effective_date','$end_date','$inserted_by','".TIME."','$two_wh_spa_allw','$four_wh_spa_allw','$contact_number','$two_wh_spa','$four_wh_spa','$living_name','$app_use','$club_membership')";
        $query  = $pdoconn->prepare($sql);
        if($query->execute()){
            $response['status'] = 1;
            $response['message'] = 'Flat Details Save';
            $response['flat_master_id'] = $flat_master_id;
        }else{
            $response['status'] = 0;
            $response['message'] = 'Flat Details Not Save ';
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'Flat Details Not Save ';
    }
    return json_encode($response);  
}
?>