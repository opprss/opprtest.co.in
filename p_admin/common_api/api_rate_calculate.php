<?php 
/*
Description: parking rate calculate.
Developed by: Rakhal Raj Mandal
Created Date: 24-06-2018
Update date : ----------
*/ 
function rate_calculate($prk_veh_trc_dtl_id,$prk_out_rep_name,$prk_admin_id,$prk_area_gate_id,$who_veh_out){
    $paymentCal =paymentCalculate($prk_veh_trc_dtl_id);
    // $response = ($paymentCal);
    $json = json_decode($paymentCal);
    if($json->status=='1'){
        $prk_vehicle_type=$json->prk_vehicle_type;
        $veh_number=$json->veh_number;
        $prk_admin_id=$json->prk_admin_id;
        $user_admin_id=$json->user_admin_id;
        $total_pay=$json->total_pay;
        $Out_time=$json->Out_time;
        $tot_hr=$json->need_hour;
        $round_hr=$json->total_hour;
        $advance_pay=$json->advance_pay;
        $need_pay=$json->need_pay;
        $payment_type=$json->payment_type;
        $respo = prk_veh_trc_dtl_update($prk_veh_trc_dtl_id,$prk_out_rep_name,$total_pay,$payment_type,$Out_time);
        $json = json_decode($respo);
        if($json->status=='1'){
            user_veh_prk_verify_active($veh_number,$prk_admin_id,$user_admin_id);
            $payment_rec_status= FLAG_I; 

            global $pdoconn;
            $sql = "SELECT  ifnull( `user_detail`.`user_nick_name`,'Guest') as `user_nick_name`,
                `user_admin`.`user_mobile` 
                FROM `prk_veh_trc_dtl`  join  `user_admin`  
                on `prk_veh_trc_dtl`.`user_admin_id`= `user_admin`.`user_admin_id`
                left join `user_detail` 
                on `prk_veh_trc_dtl`.`user_admin_id`= `user_detail`.`user_admin_id` 
                and `user_detail`.`active_flag`='".FLAG_Y."'
                WHERE  `prk_veh_trc_dtl`.`prk_veh_trc_dtl_id`='$prk_veh_trc_dtl_id'";
            $query  = $pdoconn->prepare($sql);
            $query->execute();
            $val = $query->fetch();
            $user_mobile = $val['user_mobile'];
            $user_nick_name = $val['user_nick_name'];

            $response = payment_dtl($prk_veh_trc_dtl_id, $user_admin_id,$prk_admin_id, $user_mobile, $user_nick_name,$veh_number,$tot_hr, $round_hr,$total_pay, $payment_type,$payment_rec_status,$prk_vehicle_type,$prk_out_rep_name,$prk_area_gate_id,$who_veh_out);
        }
    }else{
        $response = $paymentCal;
    }
    return $response;
}
?>