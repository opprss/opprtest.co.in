<?php
/*
Description: Parking particular gate pass data show.
Developed by: Rakhal Raj Mandal
Created Date: 31-03-2018
Update date : 27-05-2018
*/ 
function prk_gate_pass_dtl_show($prk_gate_pass_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    $sql = "SELECT `prk_gate_pass_dtl`.`prk_gate_pass_id`,
    `prk_gate_pass_dtl`.`user_admin_id`,
    `prk_gate_pass_dtl`.`veh_type`,
    `prk_gate_pass_dtl`.`veh_number`,
    `prk_gate_pass_dtl`.`prk_sub_unit_id`,
    `prk_gate_pass_dtl`.`tower_id`,
    `prk_gate_pass_dtl`.`flat_id`,
    `prk_gate_pass_dtl`.`living_status`,
    `prk_gate_pass_dtl`.`park_lot_no`,
    `prk_gate_pass_dtl`.`veh_owner_name`,
    `prk_gate_pass_dtl`.`mobile`,
    `prk_gate_pass_dtl`.`effective_date`,
    `prk_gate_pass_dtl`.`end_date`,
    `prk_gate_pass_dtl`.`prk_gate_pass_num`,
    `vehicle_type`.`vehicle_typ_img`,
    `vehicle_type`.`vehicle_type_dec`,
    `prk_sub_unit`.`prk_sub_unit_name`,
    `prk_sub_unit`.`prk_sub_unit_short_name`,
    `tower_details`.`tower_name`,
    `flat_details`.`flat_name`
    FROM `prk_gate_pass_dtl` JOIN `vehicle_type` JOIN `prk_sub_unit` 
    LEFT JOIN `tower_details` ON `tower_details`.`tower_id`=`prk_gate_pass_dtl`.`tower_id`
    AND `tower_details`.`active_flag`='".FLAG_Y."'
    AND `tower_details`.`del_flag`='".FLAG_N."'
    LEFT JOIN `flat_details` ON `flat_details`.`flat_id`=`prk_gate_pass_dtl`.`flat_id`
    AND `flat_details`.`active_flag`='".FLAG_Y."'
    AND `flat_details`.`del_flag`='".FLAG_N."'
    WHERE `prk_gate_pass_dtl`.`prk_gate_pass_id`='$prk_gate_pass_id'
    AND `prk_gate_pass_dtl`.`active_flag`='".FLAG_Y."' 
    AND `prk_gate_pass_dtl`.`del_flag`='".FLAG_N."'
    AND `prk_sub_unit`.`prk_sub_unit_id` = `prk_gate_pass_dtl`.`prk_sub_unit_id`
    AND `vehicle_type`.`active_flag`='".FLAG_Y."'
    AND `vehicle_type`.`vehicle_sort_nm`=`prk_gate_pass_dtl`.`veh_type`";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $val = $query->fetch();
    //$response['status'] = 1;
    //$response['message'] = 'data ssucessfull';
    // `prk_gate_pass_id`, `prk_admin_id`, `user_admin_id`, `prk_sub_unit_id`, `veh_type`, `veh_number`, `sub_unit_name`, `tower_id`, `flat_id`, `living_status`, `park_lot_no`, `veh_owner_name`, `mobile`, `effective_date`, `end_date`
    $park['prk_gate_pass_id'] = $val['prk_gate_pass_id'];
    $park['user_admin_id'] = $val['user_admin_id'];
    $park['prk_sub_unit_id'] = $val['prk_sub_unit_id'];
    $park['veh_type'] = $val['veh_type'];
    $park['veh_number'] = $val['veh_number'];
    $park['tower_id'] = $val['tower_id'];
    $park['tower_name'] = $val['tower_name'];
    $park['flat_id'] = $val['flat_id'];
    $park['flat_name'] = $val['flat_name'];
    $park['living_status'] = $val['living_status'];
    $park['park_lot_no'] = $val['park_lot_no'];
    $park['veh_owner_name'] = $val['veh_owner_name'];
    $park['mobile'] = $val['mobile'];
    $park['effective_date'] = $val['effective_date'];
    $park['end_date'] = $val['end_date'];
    $park['prk_gate_pass_num'] = $val['prk_gate_pass_num'];
    $park['vehicle_typ_img'] = $val['vehicle_typ_img'];
    $park['vehicle_type_dec'] = $val['vehicle_type_dec'];
    $park['prk_sub_unit_name'] = $val['prk_sub_unit_name'];
    $park['prk_sub_unit_short_name'] = $val['prk_sub_unit_short_name'];
    //$allplayerdata = array_merge($allplayerdata, $response);
    //echo json_encode($response);
    //array_push($allplayerdata, $park);
    //$response['park'] = $allplayerdata;
    return ($park);
}
?>