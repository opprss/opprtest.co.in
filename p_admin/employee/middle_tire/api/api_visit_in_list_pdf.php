<?php
/*
Description: parking visitor list api
Developed by: Manoranjan
Created Date: 31-05-2018
Update date :
*/ 
// include_once "../../fpdf/fpdf.php";
@include '../../../../library/dompdf/autoload.inc.php';
@include '../../../library/dompdf/autoload.inc.php';
@include '../../library/dompdf/autoload.inc.php';
@include '../library/dompdf/autoload.inc.php';
use Dompdf\Dompdf;
function visit_in_list_pdf($response){
    // $response = visit_in_list($prk_admin_id,$start_date,$end_date);
    // echo $response;
    $res=json_decode($response,true);

    $html="<html>
        <head>
        <style>
        .tbl_body {
            width:92%;
            border-collapse: collapse;
            margin-left: 50px;
        }
        th,td{
            border-bottom: 1px solid #ddd;
            padding: 4px;
        }
        .header{
            float: left;
            margin: 10px 0 50px;
        }
        </style>
        </head>
        <body>
        <div>
        <div class='header' style='width:400px;padding-left: 62px;'><img style='height: 80px;padding-left: 50px;' src='../../../img/logo_pdf.png'></div>
        <div class='header' style='width:300px;'>".$res['prk_area_name']."<br>Parking Visitor Transaction</div>
        <div class='header' style='width:200px;'>Start Date: ".$res['start_date']."<br>End Date: ".$res['end_date']."</div>
        </div>
        <table class='tbl_body' style='padding-top:100px;'>
            <tr>
                <th>Visitor Name</th>
                <th>Mobile No</th>
                <th>Meet To</th>
                <th>Address</th>
                <th>Visitor Id</th>
                <th>In Date</th>
                <th>Out Date</th>
                <th>Vehicle Number</th>
            </tr>";
            $html3="";
            foreach ($res['visiter'] as $key => $item){
                foreach ($item as $key_p => $values) {
                    // echo $item['veh_number'];
                    $html1="<tr class='center_align'>
                      <td>".$values['visit_name']."</td>
                      <td>".$values['visit_mobile']."</td>
                      <td>".$values['visit_meet_to']."</td>
                      <td>".$values['visit_address']."</td>
                      <td>".$values['visit_card_id']."</td>
                      <td>".$values['visiter_in_date']."</td>
                      <td>".$values['visiter_out_date']."</td>
                      <td>".$values['veh_number']."</td>
                    </tr>";
                    $html3.=$html1;
                }
            }
            $html2="<tr><td colspan='8'></td></tr>
        </table>
        </body>
        </html>";
    // echo ($mess);
    $html=$html."".$html3."".$html2;
    // echo ($html);
    // use Dompdf\Dompdf;
    // instantiate and use the dompdf class
    $dompdf = new Dompdf();
    $dompdf->loadHtml($html);
    // $dompdf->loadHtml('');
    // (Optional) Setup the paper size and orientation
    $dompdf->setPaper('A4', 'landscape');
    // Render the HTML as PDF
    $dompdf->render();
    // Output the generated PDF to Browser
    $name= uniqid();
    /*web pdf*/
        // $fulpath=$name.'.pdf';
        // $dompdf->stream($name,array('Attachment'=>0));
    /*web pdf end*/
    /*mobile pdf*/
    $filename='../../user-pdf-dwn/';
    // $filename='../user-pay-pdf/';
    if (!file_exists($filename)) {
        $old_mask = umask(0);
        mkdir($filename, 0777, TRUE);
        umask($old_mask);
    }
    $fulpath=$filename.'/'.$name.".pdf";
    $path='user-pdf-dwn/'.$name.".pdf";
    if(file_put_contents($fulpath, $dompdf->output())){
        $report['status']=1;
        $report['url']= $path;
        $report['message']='PDF Created Successful';
    }else{
        $report['status']=0;
        $report['message']='PDF Created Failed';
    }
    return json_encode($report);
}
?>