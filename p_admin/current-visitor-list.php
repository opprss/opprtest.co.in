<!-- ################################################
  
Description: It will dispaly list of all current vehicle availabe in the parking area
Developed by: Soemen Banerjee
Created Date: 17-03-2018

 ####################################################-->
<?php 
  include "all_nav/header.php"; 
  if (isset($_SESSION['prk_admin_id'])) {
    $insert_by=$_SESSION['parking_admin_name'];
    $prk_admin_id = $_SESSION['prk_admin_id'];
  }
  $response = prk_inf($prk_admin_id);
  $asas = $response;
  $response = json_decode($response, true);
  $advance_flag = $response['advance_flag'];
  $advance_pay = $response['advance_pay'];
  $print_in_flag = $response['prient_in_flag'];
  $print_out_flag = $response['prient_out_flag'];
  $veh_out_otp_flag = $response['veh_out_otp_flag'];
  $veh_token_flag = $response['veh_token_flag'];
?>
<link href="../lib/highlightjs/github.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../lib/select2/css/select2.min.css" rel="stylesheet">
<link href="../lib/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="../lib/datatables/buttons.dataTables.min.css" rel="stylesheet">
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: red;

  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
<div class="am-mainpanel">
  <div class="am-pagetitle" id="nav_1">
    <h5 class="am-title">Current Visitor</h5>
  </div>
</div>
<div class="am-mainpanel">
  <div class="am-pagebody">
    <div class="card pd-20 pd-sm-40" style="padding: 10px;">
      <div class="table-wrapper mg-t-15">
        <table id="datatable1" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="wd-5p">Img</th>
              <th class="wd-15p">name</th>
              <th class="wd-10p">mobile</th>
              <th class="wd-10p">Visitor Token</th>
              <th class="wd-10p">In date</th>
              <th class="wd-10p">In Time</th>
              <th class="wd-10p text-center">Action</th>
            </tr>
          </thead>
          <tbody id="list_vis">
          </tbody>
        </table>
      </div><!-- table-wrapper -->
    </div><!-- card -->
<?php include"all_nav/footer.php"; ?>
<!-- scripts for datatable -->
<script src="../lib/highlightjs/highlight.pack.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="../lib/datatables-responsive/jquery.dataTables.min.js"></script>
<script src="../lib/datatables-responsive/dataTables.buttons.min.js"></script>
<script src="../lib/datatables-responsive/jszip.min.js"></script>
<script src="../lib/datatables-responsive/pdfmake.min.js"></script>
<script src="../lib/datatables-responsive/vfs_fonts.js"></script>
<script src="../lib/datatables-responsive/buttons.html5.min.js"></script>
<script>
  var prkUrl = "<?php echo PRK_URL; ?>";
  var pkrEmpUrl = "<?php echo PRK_EMP_URL; ?>";
  var prk_admin_id = "<?php echo $prk_admin_id; ?>";
  var user_name = "<?php echo $insert_by; ?>";
  var token = "<?php echo $token; ?>";
  var table = $('#datatable1').DataTable();
  $(document).ready(function(){
    visitor_list();
    /*setInterval(function(){
      // alert('ok');
      // visitor_list();
      var dat_search=table.search( this.value ).draw();
      alert(dat_search);
    }, 5000);*/
  });
  function visitor_list() {
    $('#refresh_button').show();
    var visit_dtl_list = prkUrl+'prk_visit_dtl_list.php';
    $.ajax({
      url :visit_dtl_list,
      type:'POST',
      data :{
        'prk_admin_id':prk_admin_id
      },
      dataType:'html',
      success  :function(data){
        // alert(data);
        var demoLines = '';
        var json = $.parseJSON(data);
        if (json.status){
          $('#datatable1').dataTable().fnDestroy();
          for (var key in json.visit) {
            var keyy = json.visit[key];
            demoLines += '<tr>\
              <td><img src="'+keyy['visit_img']+'" style="height: 40px; border-radius: 50%;"></td>\
              <td>'+keyy['visit_name']+'</td>\
              <td>'+keyy['visit_mobile']+'</td>\
              <td>'+keyy['visitor_token']+'</td>\
              <td>'+keyy['visit_in_date']+'</td>\
              <td>'+keyy['visit_in_time']+'</td>\
              <td class="text-center">';
           
            if(keyy['visitor_out_verify_flag'] != 'Y'){
              demoLines += '<button style="color:white; background-color:green;" type="button" class="out btn prk_button" id="'+keyy['prk_visit_dtl_id']+'" onclick="vistor_out_verify(this.id)" data-toggle="tooltip" data-placement="top"><i class="fa fa-sign-out"></i> Verify</button>';
            }else{
              demoLines += '<button style="" type="button" class="out btn" id="'+keyy['prk_visit_dtl_id']+'" onclick="vistor_out_verify(this.id)" data-toggle="tooltip" data-placement="top" disabled><i class="fa fa-sign-out"></i> Verified</button>';
            }
            demoLines += '</td>\
            </tr>';
          }
        }
        $("#list_vis").html(demoLines);
        datatable_show();
      }
    });
  }
  function vistor_out_verify(prk_visit_dtl_id){
    $.confirm({
      title: 'Are You Sure?',
      content: "<p style='font-size:0.8em;'>It will Out Verify Visitor</p>",
      theme: 'modern',
      type: 'green',
      buttons: {
        cancel: function () {
        },
        somethingElse: {
          text: 'Verify',
          btnClass: 'btn-green',
          keys: ['Y', 'shift'],
          action: function(){
            // alert(prk_visit_dtl_id);
            if (prk_visit_dtl_id != '') {
              var urlDtl = prkUrl+'visitor_visit_out_verify.php';
              $.ajax({
                url :urlDtl,
                type:'POST',
                data :
                {
                  'prk_visit_dtl_id':prk_visit_dtl_id,
                  'prk_admin_id':prk_admin_id,
                  'user_name':user_name,
                  'token':token
                },
                dataType:'html',
                success  :function(data)
                {
                  // alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success',
                      content: "<p style='font-size:0.9em;'>Visitor Verify successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                            // location.reload(true);
                          visitor_list();
                        }
                      }
                    });
                  }else{
                    if (json.session=='0') {
                      window.location.replace("logout.php");
                    }
                    $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                  }
                }
              });
            }else{
              $.alert({
              icon: 'fa fa-frown-o',
              theme: 'modern',
              title: 'Error !',
              content: "<p style='font-size:0.8em;'>Somthing went wrong</p>",
              type: 'red'
              });
            }
          }
        }
      }
    });
  }
  function datatable_show(){
    'use strict';
    $('#datatable1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      language: {
      searchPlaceholder: 'Search Here',
      sSearch: '',
      lengthMenu: '_MENU_ Page',
      },
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      buttons: [
        {
          text: '<span id="refresh_button"><i class="fa fa-refresh fa-spin"></i></span> Refresh',
          action: function ( e, dt, node, config ) {
              // alert( 'Button activated' );
              visitor_list();
          }
        },
        { 
          extend: 'copyHtml5', 
          //footer: true,
          title: 'CURRENT VISITOR SHEET' 
        },
        { 
          extend: 'excelHtml5', 
          //footer: true,
          title: 'CURRENT VISITOR SHEET'  
        },
        {
          extend: 'pdfHtml5',
          //footer: true,
          title: 'CURRENT VISITOR SHEET',
          customize: function(doc) {
            doc.content[1].margin = [ 60, 0, 0, 0 ]
          }
        },
      ]
    });
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    $('#refresh_button').hide();
  }
</script>
