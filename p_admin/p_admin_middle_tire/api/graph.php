<?php 
@include_once '../../connection-pdo.php';
//@include_once '../connection-pdo.php';
//@include_once 'connection-pdo.php';

function vehicle_chart_by_date($start_date,$end_date,$prk_admin_id){
    $allplayerdata = array();
    $response = array();
    $w2=0;
    $w2_sum=0;
    $w3=0;
    $w3_sum=0;
    $w4=0;
    $w4_sum=0;
    $w8=0;
    $w8_sum=0;
    $format = "Y-m-d";
    if(date($format, strtotime($start_date)) == date($start_date) && date($format, strtotime($end_date)) == date($end_date)) {
        $start_date= $start_date;
        $end_date=  $end_date ;
    } else {
        $start_date = date("Y-m-d", strtotime($start_date));
        $end_date = date("Y-m-d", strtotime($end_date));
    }
    global $pdoconn;
    $sql = "SELECT * FROM `prk_veh_trc_dtl` WHERE `transition_date` BETWEEN '$start_date' AND '$end_date' AND `prk_veh_type`='2W' AND `prk_admin_id`='$prk_admin_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val)
    {
        $total_pay=$val['total_pay'];
        $w2_sum=$w2_sum+$total_pay;
        $w2+=1;
    }
    $sql = "SELECT * FROM `prk_veh_trc_dtl` WHERE `transition_date` BETWEEN '$start_date' AND '$end_date' AND `prk_veh_type`='3W' AND `prk_admin_id`='$prk_admin_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val)
    {
        $total_pay=$val['total_pay'];
        $w3_sum=$w3_sum+$total_pay;
        $w3+=1;
    }
    $sql = "SELECT * FROM `prk_veh_trc_dtl` WHERE `transition_date` BETWEEN '$start_date' AND '$end_date' AND `prk_veh_type`='4W' AND `prk_admin_id`='$prk_admin_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val)
    {
        $total_pay=$val['total_pay'];
        $w4_sum=$w4_sum+$total_pay;
        $w4+=1;
    }
    $sql = "SELECT * FROM `prk_veh_trc_dtl` WHERE `transition_date` BETWEEN '$start_date' AND '$end_date' AND `prk_veh_type`='8W' AND `prk_admin_id`='$prk_admin_id'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $arr_catagory = $query->fetchAll();
    foreach($arr_catagory as $val)
    {
        $total_pay=$val['total_pay'];
        $w8_sum=$w8_sum+$total_pay;
        $w8+=1;
    }
    $total_pay=($w2_sum+$w3_sum+$w4_sum+$w8_sum);
    $response['2W'] = $w2;
    $response['2W_total_pay'] = $w2_sum;
    $response['3W'] = $w3;
    $response['3W_total_pay'] = $w3_sum;
    $response['4W'] = $w4;
    $response['4W_total_pay'] = $w4_sum;
    $response['8W'] = $w8;
    $response['8W_total_pay'] = $w8_sum;
    $response['total_pay'] = $total_pay;

    return json_encode($response);
}

 ?>