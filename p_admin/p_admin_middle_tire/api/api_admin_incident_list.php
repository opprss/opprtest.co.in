<?php
/*
Description: Parking visiter in list
Developed by: Rakhal Raj Mandal
Created Date: 31-05-2018
Update date : 
*/ 
function admin_incident_list($prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $resp = array();
    $sql = "SELECT `user_incident`.`user_incident_id`,
        `user_incident`.`user_incident_name`,
        `user_incident`.`user_incident_type`, 
        `user_incident`.`user_incident_dtl`,
        `user_incident`. `user_incident_number`,
        `user_incident`.`remarks`,
        `user_incident`. `status` as status1,
        (CASE `user_incident`.`status` WHEN 'D' THEN 'DRAFT' WHEN 'I' THEN 'IN PROGRESS' WHEN 'F' THEN 'FIXED' WHEN 'A' THEN 'APPROVE' WHEN 'N' THEN 'NOT FIXED' END)AS `status`, 
        `user_incident`.`prk_admin_id`,
        `user_incident`.`prk_area_user_id`,DATE_FORMAT(`user_incident`.`incident_in_date`,'%d-%m-%Y %I:%i %p') AS `incident_in_date`, 
        `user_incident`.`prk_area_user_id`,
        `user_incident`. `user_admin_id`,
        `user_detail`.`user_name`,
        `user_detail`.`user_nick_name`,
        `user_detail`.`user_email`,
        `user_admin`.`user_mobile`,
        IF(`user_detail`.`user_img` IS NULL or `user_detail`.`user_img` = '', (CASE WHEN user_detail.user_gender= '".FLAG_M."' then '".MALE_IMG_D."' WHEN user_detail.user_gender= '".FLAG_F."' THEN '".FEMALE_IMG_D."' end), CONCAT('".USER_BASE_URL."',`user_detail`.`user_img`)) as `user_img`
        FROM `user_incident`,`user_detail`,`user_admin` 
        WHERE `user_incident`.`prk_admin_id`='$prk_admin_id'
        AND `user_incident`.`active_flag`='".FLAG_Y."'
        AND `user_incident`.`del_flag`='".FLAG_N."'
        AND `user_detail`.`user_admin_id`=`user_incident`. `user_admin_id`
        AND `user_detail`.`active_flag`='".FLAG_Y."'
        AND `user_admin`.`user_admin_id`=`user_detail`.`user_admin_id`
        ORDER BY 1 DESC";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val)
        {
            $resp['user_incident_id'] =$val['user_incident_id'];
            $resp['user_incident_name'] =$val['user_incident_name'];
            $resp['user_incident_type'] =$val['user_incident_type'];
            $resp['user_incident_dtl'] =$val['user_incident_dtl'];
            $resp['user_incident_number'] =$val['user_incident_number'];
            $resp['status'] =$val['status'];
            // $resp['prk_admin_id'] =$val['prk_admin_id'];
            $resp['incident_in_date'] =$val['incident_in_date'];
            // $resp['prk_area_user_id'] =$val['prk_area_user_id'];
            // $resp['user_admin_id'] =$val['user_admin_id'];
            $resp['remarks'] =$val['remarks'];
            $resp['user_name'] =$val['user_name'];
            $resp['user_nick_name'] =$val['user_nick_name'];
            $resp['user_email'] =$val['user_email'];
            $resp['user_mobile'] =$val['user_mobile'];
            $resp['user_img'] =$val['user_img'];
            array_push($allplayerdata, $resp);
        }
        $response['status']=1;
        $response['message'] = 'Successful';
        $response['incident_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>