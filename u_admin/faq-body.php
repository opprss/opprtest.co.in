<?php 
  //include('../global_asset/config.php');
  @include_once("connection/connection-pdo.php");
  require_once 'web/api/global_api.php';
  require_once 'web/api/reg_api.php';
  //include 'web/api/session_api.php';
?>
    <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="../lib/jquery-toggles/toggles-full.css" rel="stylesheet">
    <link href="../lib/rickshaw/rickshaw.min.css" rel="stylesheet">

    <!-- calender -->  
    <link rel="stylesheet" href="date/css/pickmeup.css" type="text/css" />
    <script type="text/javascript" src="date/js/pickmeup.js"></script>

    <!-- icon -->
    <script src="https://use.fontawesome.com/f9a581fe37.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    
    <!-- Amanda CSS -->
    <link rel="stylesheet" href="../css/amanda.css">
    <link rel="stylesheet" href="../css/style-user.css">
    <div class="am-pagebody">
      <div class="card single pd-20 pd-sm-40">
        <!--  -->
        <div class="row">
          <div class="col-md-10">
            <h6 class="card-body-title">Frequently Ask Questions</h6>
            <p class="mg-b-20 mg-sm-b-30">The default collapse behavior to create an accordion.</p>
          </div>
          <div class="col-md-2">
            <button class="btn btn-block user_button" type="button" name="add_question" id="add_question">ADD QUESTION</button>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div id="accordion" class="accordion" role="tablist" aria-multiselectable="true">
              <?php
                $row=opprss_faq();
                //echo $row[0]['question'];
                //print_r($row);
                $count = count($row);
                 for ($i=0; $i < $count; $i++) { 
                   //echo '<br>'.$row[$i]['question'];
                   //echo '<br><br>'.$row[$i]['answer'];
              ?>
              <div class="card">
                <div class="card-header" role="tab" id="heading<?php echo $i?>">
                  <h6 class="mg-b-0">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i?>" aria-expanded="<?php if($i==0) echo 'true'; else echo 'false'; ?>" aria-controls="collapse<?php echo $i?>" class="<?php if($i==0) echo 'tx-gray-800'; else echo 'collapsed' ?> transition">
                      <?php echo $row[$i]['question']?>
                      <i class="fa fa-plus pull-right"></i>
                    </a>
                  </h6>
                </div><!-- card-header -->

                <div id="collapse<?php echo $i?>" class="collapse <?php if($i==0) echo 'show'; else echo ''?>" role="tabpanel" aria-labelledby="heading<?php echo $i?>">
                  <div class="card-body">
                    <?php echo $row[$i]['answer']?>
                  </div>
                </div>
              </div>
              <?php
            }
            ?>
            </div><!-- accordion -->
          </div>
        </div>
          
      </div><!-- card -->
    </div>
    <?php include"all_nav/footer.php"; ?>

    <script type="text/javascript">
      var userUrl = "<?php echo USER_URL; ?>";

      $('#add_question').on('click', function() {
        $.confirm({
        title: 'Add Question',
        useBootstrap: false,
        boxWidth: '40%',
        content: '' +
        '<hr>'+
        '<form action="" class="formName" style="padding-top:40px;">' +
        '<div class="form-group">' +
        '<input type="text" placeholder="What is your question?" class="question form-control" required />' +
        '</div>' +
        '</form>',
        buttons: {
            formSubmit: {
                text: 'ADD',
                btnClass: 'btn-green',
                action: function () {
                    var question = this.$content.find('.question').val();

                    if(question == '' ){
                      $.alert({
                        title:'Error',
                        theme: 'modern',
                        content: '<p style="font-size:0.9em;">Please enter your question</p>',
                        type: 'red'
                      });
                        return false;
                    }
                    var urlFaqQuestionAdd = userUrl+'faq_question_add.php';
                    $.ajax({
                      url :urlFaqQuestionAdd,
                      type:'POST',
                      data :
                      {
                        'question':question
                      },
                      dataType:'html',
                      success  :function(data)
                      {
                        var json = $.parseJSON(data);
                        if (json.status){
                            $.alert({
                            icon: 'fa fa-smile-o',
                            theme: 'modern',
                            title: 'Success',
                            content: "Your question add sucessfully",
                            type: 'green',
                            buttons: {
                              Ok: function () {
                                  //location.reload(true);
                              }
                            }
                          });
                        }else{
                          $.alert({
                            icon: 'fa fa-frown-o',
                            theme: 'modern',
                            title: 'Error !',
                            content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                            type: 'red'
                          });
                        }
                      }
                    });
                }
            },
            cancel: function () {
                //close
            },
        },
        onContentReady: function () {
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
      });
    </script>