<?php 
/*
Description: Parking gate insert.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :12-03-2019
*/ 
require_once 'api/parkAreaReg_api.php';
$previous_url =$_SERVER['HTTP_REFERER'];
$response = array();
if(isAvailable(array('prk_admin_id','prk_area_user_id','user_name','checkbox','inserted_by','token'))){
    if(isEmpty(array('prk_admin_id','prk_area_user_id','user_name','checkbox','inserted_by','token'))){

        $prk_admin_id = trim($_POST['prk_admin_id']);
        $prk_area_user_id = trim($_POST['prk_area_user_id']);
        $user_name = trim($_POST['user_name']);
        $form_id = $_POST['checkbox'];
        $inserted_by = trim($_POST['inserted_by']);
        $token = trim($_POST['token']);
        $resp=prk_token_check($prk_admin_id,$token);
        $json = json_decode($resp);
        if($json->status){
            $response = p_employee_permi_form($prk_admin_id,$prk_area_user_id,$user_name,$form_id,$inserted_by);
        }else{
            $response = $resp;
        }
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo $response;
header("Location:".$previous_url);
?>