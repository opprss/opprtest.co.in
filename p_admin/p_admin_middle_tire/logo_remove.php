<?php
require_once 'api/parkAreaReg_api.php';
if(isAvailable(array('token','prk_admin_id'))){
    if(isEmpty(array('token','prk_admin_id'))){
		$token = $_POST['token'];
		$prk_admin_id = $_POST['prk_admin_id'];
		$response  = array();
		$resp=prk_token_check($prk_admin_id,$token);
		$json = json_decode($resp);
		if($json->status){
		    if (logoImageRemove($prk_admin_id)) {
				$response['status'] = 1;
				$response['msg'] = 'sucessfull';
	        	$response = json_encode($response);
			}else{
				$response['status'] = 0;
				$response['msg'] = 'Unsucessfull';
        		$response = json_encode($response);
			}
		}else{
		    $response = $resp;
		}
	}else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
	$response['status'] = 0; 
	$response['message'] = 'Invalid API Call';
	$response = json_encode($response);
 }	
echo $response;
?>