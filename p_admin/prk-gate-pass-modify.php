<!-- ################################################
  
Description: Parking area can issue gate pass to a particular user.
Developed by: Soemen Banerjee
Created Date: 17-03-2018

####################################################-->
<?php 
  include "all_nav/header.php";
  if (isset($_SESSION['prk_admin_id'])) {
    $prk_admin_id = $_SESSION['prk_admin_id'];
  }
  $list_id = isset($_REQUEST['list_id']) ? trim($_REQUEST['list_id']) : ''; 
  $prk_gate_pass_id = base64_decode($list_id);
  $response = array();
  $response = prk_gate_pass_dtl_show($prk_gate_pass_id);
  if (!empty($response)) {
    // , `tower_id`, `flat_id`, `living_status`, `park_lot_no`
    $prk_gate_pass_id = $response['prk_gate_pass_id']; 
    $user_admin_id = $response['user_admin_id'];
    $veh_type = $response['veh_type'];
    $vehicle_type_dec = $response['vehicle_type_dec'];
    $veh_number = $response['veh_number'];
    $tower_id = $response['tower_id'];
    $flat_id = $response['flat_id'];
    $flat_name = $response['flat_name'];
    $tower_name = $response['tower_name'];
    $living_status = $response['living_status'];
    $park_lot_no = $response['park_lot_no'];
    $sub_unit_name = $response['prk_sub_unit_name'];
    $prk_sub_unit_id = $response['prk_sub_unit_id'];
    $veh_owner_name = $response['veh_owner_name'];
    $mobile = $response['mobile'];
    $prk_gate_pass_num =$response['prk_gate_pass_num'];
    $effective_date = $response['effective_date'];
    $end_date = $response['end_date'];
  }
  $tower_lis = tower_list($prk_admin_id);
  $tower_list = json_decode($tower_lis, true);
  $category_type='APT_FLT_HLD';
  $prk_user_emp_cat = common_drop_down_list($category_type);
  $prk_user_emp_category = json_decode($prk_user_emp_cat, true);
  $prk_inf_dtl = prk_inf($prk_admin_id);
  $prk_inf_dtll = json_decode($prk_inf_dtl, true);
  $parking_type = $prk_inf_dtll['parking_type'];
?>
<style>
  .size{
    font-size: 11px;
  }
  .error_size{
    font-size: 11px;
    color: gray;
  }
  .success{
    font-size: 11px;
    color: green;
  }
  input::-webkit-input-placeholder {
    font-size: 12px;
  }
</style>
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet"> 
    <div class="am-mainpanel">
      <div class="am-pagetitle" id="nav_1">

        <h5 class="am-title">Digital Gate Pass Update<?=$parking_type?></h5>
      </div>
      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40 col-md-12">
          <div class="editable tx-16 bd pd-30 tx-inverse" style="padding-bottom: 0px;padding: 5px;">
            <div class="row">
              <div class="col-md-12">
                <form id="addGate" method="get" action="">
                  <div class="form-layout">
                    <div class="row mg-b-25">
                      <div class="col-lg-3 error_show">
                        <div class="form-group">
                            <label class="form-control-label size">VEHICLE TYPE <span class="tx-danger">*</span></label>
                            <div class="select">
                              <select name="veh_type" id="veh_type" style="font-size: 14px; opacity: 0.8;">
                                  <option value="">Select Type</option>
                                  <?php
                                      $response = array();
                                      $response =vehicleType();
                                      $count = count($response);

                                      for ($i=0; $i < $count ; $i++) { 
                                     ?>
                                      <option value="<?php echo $response[$i]['vehicle_Sort_nm']?>"><?php echo $response[$i]['vehicle_type']?></option>

                                      <?php
                                      }
                                  ?>
                              </select>
                            </div>
                            <span style="position: absolute;" id="error_user_veh_type" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="form-control-label size">VEHICLE NUMBER <span class="tx-danger">*</span></label>
                          <input type="text" class="form-control" placeholder="Vehicle Number" name="veh_number" id="veh_number" onkeyup="vehNoUppercase();" maxlength="10">
                          <span style="position: absolute;" id="error_veh_number" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-lg-3 error_show">
                        <div class="form-group">
                          <label class="form-control-label size">SUB UNIT LIST <span class="tx-danger">*</span></label>
                            <div class="select">
                              <select name="sub_unit_name" id="sub_unit_name" style="opacity: 0.8;font-size:14px">
                                  <option value="">Select Type</option>
                                  <?php
                                      $response = array();
                                      $response =prk_sub_unit_dtl_list($prk_admin_id);
                                      //s$response_count = json_decode($response, true);
                                      $count = count($response);
                                      if ($count == 0) {
                                  ?>
                                      <option value="NA">NA</option>
                                  <?php     
                                      }else{
                                        foreach($response as $value){ 
                                     ?>
                                        <option value="<?php echo $value['prk_sub_unit_id']?>"><?php echo $value['prk_sub_unit_name']?></option>
                                      <?php
                                    }  }
                                  ?>
                              </select>
                            </div>
                            <span style="position: absolute;" id="error_sub_unit_name" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="form-control-label size">GATE PASS NUMBER <span class="tx-danger">*</span></label>
                          <input type="text" class="form-control readonly" placeholder="Gate Pass Number" name="prk_gate_pass_num" id="prk_gate_pass_num" readonly="readonly" style="background-color: transparent;">
                          <span style="position: absolute;" id="error_prk_gate_pass_num" class="error_size"></span>
                        </div>
                      </div>
                      <div class=" row col-md-6" id="for_residential">
                        <div class="col-lg-4">
                          <div class="form-group">
                            <label class="form-control-label size">TOWER NAME <span class="tx-danger">*</span></label>
                            <div class="select">
                              <select name="tower_name" id="tower_name" style="opacity: 0.8;font-size:14px">
                                <option value="">Select Tower</option>
                                <?php if($tower_list['status']){
                                  foreach($tower_list['tower_list'] as $value){?>
                                  <option value="<?=$value['tower_id'];?>"><?=$value['tower_name'];?></option>
                                <?php }}?>
                              </select>
                            </div>
                          <span style="position: absolute; display: none;" id="error_tower_name" class="error">This field is required.</span>
                          </div>
                        </div>
                        <div class="col-lg-4">
                          <div class="form-group">
                            <label class="form-control-label size">FLAT NAME <span class="tx-danger">*</span></label>
                            <div class="select">
                              <select name="flat_name" id="flat_name" style="opacity: 0.8;font-size:14px">
                                <option value="">Select Flat</option>
                              </select>
                            </div>
                          <span style="position: absolute; display: none;" id="error_flat_name" class="error">This field is required.</span>
                          </div>
                        </div>
                        <div class="col-lg-4">
                          <div class="form-group">
                            <label class="form-control-label size">LIVING STATUS<span class="tx-danger">*</span></label>
                            <div class="select">
                              <select name="living_status" id="living_status" style="opacity: 0.8;font-size:14px">
                                <option value="">Select Living</option>
                                  <?php
                                  foreach ($prk_user_emp_category['drop_down'] as $val){ 
                                    if($val['sort_name']!='B'& $val['sort_name']!='V'){?>  
                                    <option value="<?php echo $val['sort_name']?>"><?php echo $val['full_name']?></option>
                                  <?php          
                                  }}
                                ?>
                              </select>
                            </div>
                          <span style="position: absolute; display: none;" id="error_living_status" class="error">This field is required.</span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label class="form-control-label size">VEHICLE OWNER NAME <span class="tx-danger">*</span></label>
                          <input type="text" class="form-control" placeholder="Owner Name" name="veh_owner_name" id="veh_owner_name" onkeydown="upperCase(this)">
                          <span style="position: absolute;" id="error_veh_owner_name" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label class="form-control-label size">MOBILE NUMBER <span class="tx-danger">*</span></label>
                          <input type="text" class="form-control" placeholder="Mobile Number" name="mobile" id="mobile" maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                          <span style="position: absolute;" id="error_mobile" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-lg-2" id="park_lot_id">
                        <div class="form-group">
                          <label class="form-control-label size">Parking Lot No.<span class="tx-danger"></span></label>
                          <input type="text" name="park_lot_no" id="park_lot_no" placeholder="Parking Lot No." class="form-control prevent_readonly" readonly="readonly">
                        <span style="position: absolute; display: none;" id="error_living_status" class="error">This field is required.</span>
                        </div>
                      </div>
                      <div class="col-lg-2 error_show">
                        <div class="form-group">
                          <label class="form-control-label size">EFFECTIVE DATE <span class="tx-danger">*</span></label>
                          <input type="text" name="effective_date" id="effective_date" placeholder="Effective Date" class="form-control prevent_readonly" readonly="readonly">
                          <span style="position: absolute;" id="error_effective_date" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-lg-2 error_show">
                        <div class="form-group">
                          <label class="form-control-label size">END DATE</label>
                          <input type="text" name="end_date" id="end_date" placeholder="End Date" class="form-control prevent_readonly" readonly="readonly">
                          <span style="position: absolute;" id="error_end_date" class="error_size"></span>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-layout-footer mg-t-30">
                          <div class="row">
                            <div class="col-lg-6 pd-b-10">
                              <input type="button" value="SAVE" class="btn btn-block btn-primary prk_button" name="save" id="save">
                            <span style="position: absolute; color: red;" id="error_all" class="error_size"></span>
                            </div>
                            <div class="col-lg-6">
                                <button type="button" class="btn btn-block prk_button_skip" name="skip" id="skip" onclick="document.location.href='prk-gate-pass-issue';">Back</button>
                              </div>
                          </div>
                        </div>
                      </div>
                  </div><!-- form-layout -->
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
<?php include"all_nav/footer.php"; ?>
<style>
  .error_size{
    font-size: 11px;
    color: red;
  }
  .success{
    font-size: 11px;
    color: green;
  }
</style>
<script type="text/javascript">
  var prkUrl = "<?php echo PRK_URL; ?>";
  var FUTURE_DATE = "<?php echo FUTURE_DATE_WEB ?>";
  var prk_admin_id = "<?php echo $prk_admin_id;?>";
  var user_name=' <?php echo $parking_admin_name; ?>';
  var token='<?php echo $token;?>';
  var prk_gate_pass_id = "<?php echo $prk_gate_pass_id;?>";
  var user_admin_id = "<?php echo $user_admin_id;?>";
  var parking_type ='<?=$parking_type?>';
  if (parking_type=='R') {
    $("#for_residential").show();
    $("#park_lot_id").show();
  }else{
    $("#for_residential").hide();
    $("#park_lot_id").hide();
  }
  $('#veh_type').val('<?=$veh_type?>').prop('disabled', true);
  $('#veh_number').val('<?=$veh_number?>').prop('disabled', true);
  $('#sub_unit_name').val('<?=$prk_sub_unit_id?>').prop('disabled', true);
  $('#prk_gate_pass_num').val('<?=$prk_gate_pass_num?>').prop('disabled', true);
  $('#tower_name').val('<?=$tower_id?>').prop('disabled', true);
  $('#park_lot_no').val('<?=$park_lot_no?>').prop('disabled', true);
  // $('#flat_name').val('<?=$flat_id?>');
  var flat_id='<?=$flat_id?>';
  var flat_name='<?=$flat_name?>';
  if(flat_id!=''){
    var areaOption = '<option value="' +flat_id+ '">'+flat_name+'</option>'
    $("#flat_name").html(areaOption).prop('disabled', true);
  }
  $('#living_status').val('<?=$living_status?>').prop('disabled', true);
  /*var park_lot_no='<?=$park_lot_no?>';
  if(park_lot_no!=''){
    var areaLotno = '<option value="' + park_lot_no + '">' + park_lot_no + '</option>';
    $("#park_lot_no").html(areaLotno).prop('disabled', true);
  }*/
  $('#veh_owner_name').val('<?=$veh_owner_name?>').prop('disabled', true);
  $('#mobile').val('<?=$mobile?>').prop('disabled', true);
  $('#effective_date').val('<?=$effective_date?>').prop('disabled', true);
  var end_date_lod = ('<?=$end_date?>'==FUTURE_DATE)?'':'<?=$end_date?>';
  $('#end_date').val(end_date_lod);
  $( document ).ready( function () {
    $('#reset').click(function(){

      $('.error_size').text('');
    });
    /*FORM SUBMIT*/
    $('#save').click(function(){
      var veh_type = $('#veh_type').val();
      var prk_sub_unit_id =$('#sub_unit_name').val();
      var veh_number = $('#veh_number').val();
      var veh_owner_name = $('#veh_owner_name').val();
      var mobile = $('#mobile').val();
      var end_date = $('#end_date').val();
      end_date = (!end_date == '') ? end_date : FUTURE_DATE;
      var effective_date = $('#effective_date').val();
      var prk_gate_pass_num = $('#prk_gate_pass_num').val();
      var tower_id = $('#tower_name').val();
      var flat_id = $('#flat_name').val();
      var living_status = $('#living_status').val();
      var park_lot_no = $('#park_lot_no').val();
      // var park_lot_no = '';
      if (veh_type!='' && veh_number!='' && veh_owner_name!='' && mobile!='' && effective_date!=''&& prk_sub_unit_id!='') {
        var urlVehNoCheck = prkUrl+'v_numberChecker.php';
        $.ajax({
          url :urlVehNoCheck,
          type:'POST',
          data :{

            'v_number':veh_number
          },
          dataType:'html',
          success  :function(data){
            //alert(data);
            var json = $.parseJSON(data);
            if (json.status) {
              $('#save').val('Wait ...').prop('disabled', true);
              var urlPrkAreGaGatePassDtl = prkUrl+'prk_gate_pass_dtl_update.php';
              $.ajax({
                url :urlPrkAreGaGatePassDtl,
                type:'POST',
                data :{
                  'prk_gate_pass_id':prk_gate_pass_id,
                  'prk_admin_id':prk_admin_id,
                  'user_admin_id':user_admin_id,
                  'veh_type':veh_type,
                  'veh_number':veh_number,
                  'veh_owner_name':veh_owner_name,
                  'mobile':mobile,
                  'effective_date':effective_date,
                  'end_date':end_date,
                  'user_name':user_name,
                  'prk_gate_pass_num':prk_gate_pass_num,
                  'prk_sub_unit_id':prk_sub_unit_id,
                  'tower_id':tower_id,
                  'flat_id':flat_id,
                  'living_status':living_status,
                  'park_lot_no':park_lot_no,
                  'token': token
                },
                dataType:'html',
                success  :function(data){
                  $('#save').val('SAVE').prop('disabled', false);
                  //alert(data);
                  var json = $.parseJSON(data);
                  if (json.status){
                    $.alert({
                      icon: 'fa fa-smile-o',
                      theme: 'modern',
                      title: 'Success !',
                      content: "<p style='font-size:0.9em;'>Gate pass added successfully</p>",
                      type: 'green',
                      buttons: {
                        Ok: function () {
                            window.location='prk-gate-pass-issue';
                        }
                      }
                    });
                  }else{
                    if( typeof json.session !== 'undefined'){
                      if (!json.session) {
                        window.location.replace("logout.php");
                      }
                    }else{
                      $.alert({
                      icon: 'fa fa-frown-o',
                      theme: 'modern',
                      title: 'Error !',
                      content: "<p style='font-size:0.9em;'>Somting went wrong</p>",
                      type: 'red'
                    });
                    }
                  }
                }
              });
            }else{

              $('#error_veh_number').text('Vehicle number is invalid');
            }
          }
        });
      }else{
        if (veh_type == '') {
          $('#error_user_veh_type').text('Vehical type is required');
        //return false;
        }if (prk_sub_unit_id == '') {
          $('#error_sub_unit_name').text('Sub unit name is required');
          //return false;
        }if (veh_number == '') {
          $('#error_veh_number').text('Vehical number is required');
          //return false;
        }if (veh_owner_name == '') {
          $('#error_veh_owner_name').text('Owner name is required');
          //return false;
        }if (mobile == '') {
          $('#error_mobile').text('Mobile number is required');
          //return false;
        }if (effective_date == '') {
          $('#error_effective_date').text('Effective date is required');
          //return false;
        }
      }
    });
    /*effective date*/
    $("#effective_date").blur(function () {
      var effective_date = $('#effective_date').val();
      if(effective_date == ''){
        $('#error_effective_date').text('Effective date is required');
        return false;
      }else{
        $('#error_effective_date').text('');
        return true;
      }
    });
    /*sub unit*/
    $("#sub_unit_name").blur(function () {
      var sub_unit_name = $('#sub_unit_name').val();
      if(sub_unit_name == ''){
         $('#error_sub_unit_name').text('Sub unit name is required');
        return false;
      }else{
        $('#error_sub_unit_name').text('');
        return true;
      }
    });
    /*vehicle type*/
    $("#veh_type").blur(function () {
      var veh_type = $('#veh_type').val();
      if(veh_type == ''){
      $('#error_user_veh_type').text('Vehicle type is required');
      return false;
      }else{
        $('#error_user_veh_type').text('');
        return true;
      }
    });
    /*Owner number*/
    $("#veh_owner_name").blur(function () {
      var veh_owner_name = $('#veh_owner_name').val();
      if(veh_owner_name == ''){
      $('#error_veh_owner_name').text('Owner name is required');
      return false;
      }else{
        $('#error_veh_owner_name').text('');
        return true;
      }
    });
    $("#veh_number").blur(function(){
      var veh_number = $('#veh_number').val();
      IsVehNumber(veh_number);
    });
    function IsVehNumber(veh_number) {
      var urlVehNoCheck = prkUrl+'v_numberChecker.php';
      $.ajax({
        url :urlVehNoCheck,
        type:'POST',
        data :
        {
          'v_number':veh_number
        },
        dataType:'html',
        success  :function(data)
        {
          //alert(data);
          var json = $.parseJSON(data);
          vehNumberReturn(json.status);
        }
      });
    } 
    function vehNumberReturn(flag){
      if(!flag){
        $('#error_veh_number').text('Vehicle number is invalid');
        //return false;
      }else{
        $('#error_veh_number').text('');
        //return true;
      }
    }
    $('#mobile').blur(function() {
      var mobile = $('#mobile').val();
      //alert(mobile);
      if (validatePhone(mobile)) {
        $('#error_mobile').text('');
      }else {
        $('#error_mobile').text('Mobile number is invalid');
        // $('#mobile').focus();
        return false;
      }
    });
    function validatePhone(txtPhone) {
      var filter = /[0-9]{2}\d{8}/;
      if (filter.test(txtPhone)) {
        return true;
      }else {
        return false;
      }
    }
    $( "#sub_unit_name" ).change(function() {
      var prk_sub_unit_id = $('#sub_unit_name').val();
      var urlGatePassGenerate = prkUrl+'gate_pass_generate.php';
      $.ajax({
        url :urlGatePassGenerate,
        type:'POST',
        data :{
          'prk_admin_id':prk_admin_id,
          'prk_sub_unit_id':prk_sub_unit_id,
          'token': token
        },
        dataType:'html',
        success  :function(data){
          //alert(data);
          var json = $.parseJSON(data);
          if (json.status) {
            $("#prk_gate_pass_num").val(json.gate_pass);
          }else{
            if( typeof json.session !== 'undefined'){
              if (!json.session) {
                window.location.replace("logout.php");
              }
            }else{
              $('#sub_unit_name').val('');
              $('#prk_gate_pass_num').val('');

              $.alert({
                icon: 'fa fa-frown-o',
                theme: 'modern',
                title: 'Error !',
                content: "<p style='font-size:0.9em;'>First you have to select SUB UNIT to generate DIGITAL GATE PASS </p>",
                type: 'red'
              });
            }
          }
        }
      });
    });
  });
  addEventListener('DOMContentLoaded', function () {
    var now = new Date;
    var now = now.setDate(now.getDate() - 1);
    pickmeup('#effective_date', {
      default_date   : false,
      position       : 'bottom',
      hide_on_select : true,
      render : function (date) {
        if (date < now) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        $('#error_effective_date').text('');
        return {};
      } 
    });
  });
  $('#end_date').focus(function () {
    var start = new Date;
    var end = $('#effective_date').val();
    var d = new Date(end.split("-").reverse().join("-"));
    var dd=d.getDate();
    var mm=d.getMonth()+1;
    var yy=d.getFullYear();
    var hh = start.getHours();
    var min = start.getMinutes();
    var sec = start.getSeconds();
    var newdate=yy+"/"+mm+"/"+dd+" "+hh+":"+min+":"+sec;
    var end = new Date(newdate);
    days = (end - start) / (1000 * 60 * 60 * 24)-1;
    days = Math.round(days);
    now_end = start.setDate(start.getDate() + days);
    pickmeup('#end_date', {
      default_date   : false,
      /*position       : 'top',*/
      hide_on_select : true,
      render : function (date) {
        if (date < now_end) {
          return {
            disabled : true,
            class_name : 'date-in-past'
          };
        }
        return {};
      } 
    });
  });
  $('#effective_date').focus(function(){
    $('#end_date').val('');
  });
  function upperCase(a){
    setTimeout(function(){
      a.value = a.value.toUpperCase();
    }, 1);
  }
  function gatePassUppercase() {
    var x = document.getElementById("user_gate_pass_num");
    x.value = x.value.toUpperCase();
  }
  function vehNoUppercase() {
    var x = document.getElementById("veh_number");
    x.value = x.value.toUpperCase();
  }
  $("#veh_owner_name").keypress(function(event){
    var inputValue = event.charCode;
    // if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
    if(!(inputValue >= 60) && (inputValue != 0 & inputValue !=32)){
    // if(!(inputValue >= 97) && (inputValue != 46)){
        event.preventDefault();
    }
  });
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
  });
</script>
<script type="text/javascript">
  $('#tower_name').on("change", function() {
    var id=($("#tower_name").val());
    var urlCity = prkUrl+'flat_list.php';
    $.ajax ({
      type: 'POST',
      url: urlCity,
      data: {
        tower_id:id,
        prk_admin_id:prk_admin_id,
        parking_admin_name:user_name,
        token:token
      },
      success : function(data) {
        // alert(data);
        var obj=JSON.parse(data);
        var areaOption = "<option value=''>Select Flat</option>";
        // alert(obj['status']);
        if (obj['status']) {
          $.each(obj['flat_list'], function(val, key) {
            areaOption += '<option value="' + key['flat_id'] + '">' + key['flat_name'] + '</option>'
          });
        }
        $("#flat_name").html(areaOption);
      }
    });
  });
  $('#flat_name').on("change", function() {
    var tower_id=($("#tower_name").val());
    var flat_id=($("#flat_name").val());
    var tower_id=($("#tower_name").val());
    var flat_id=($("#flat_name").val());
    var urlCity = prkUrl+'flat_master.php';
    $.ajax ({
      type: 'POST',
      url: urlCity,
      data: {
        'prk_admin_id':prk_admin_id,
        'parking_admin_name':user_name,
        'token':token,
        'crud_type':'S',
        'tower_name':tower_id,
        'flat_name':flat_id,
        'owner_name':'',
        'living_status':'',
        'tenant_name':'',
        'effective_date':'',
        'end_date':'',
        'flat_master_id':'',
        'two_wh_spa_allw':'',
        'four_wh_spa_allw':'',
        'two_wh_spa':'',
        'four_wh_spa':'',
        'contact_number':'',
        'living_name':''
      },
      success : function(data) {
        // alert(data);
        var json = $.parseJSON(data);
        if (json.status) {
          // alert(json.owner_mobile);
          $('#living_status').val(json.living_status);
          if (json.living_status=='O') {
            $('#veh_owner_name').val(json.owner_name);
            $('#mobile').val(json.owner_mobile);
          }else{
            $('#veh_owner_name').val(json.tenant_name);
            $('#mobile').val(json.tenant_mobile);
          }
        }else{
          $.alert({
            icon: 'fa fa-frown-o',
            theme: 'modern',
            title: 'Error !',
            content: "<p style='font-size:0.8em;'>No Person Is There In The Flat</p>",
            type: 'red'
          });
          $('#living_status').val('');
          $('#veh_owner_name').val('');
          $('#mobile').val('');
        }
      }
    });
  });
</script>