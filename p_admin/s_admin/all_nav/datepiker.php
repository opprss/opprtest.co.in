
<!DOCTYPE html>
<html>
<head>
    <base href="https://demos.telerik.com/kendo-ui/datepicker/index">
    <style>html { font-size: 14px; font-family: Arial, Helvetica, sans-serif; }</style>
    <title></title>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.material.mobile.min.css" />

    <script
  src="https://code.jquery.com/jquery-3.2.1.js"
  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
  crossorigin="anonymous"></script>
    <script src="https://kendo.cdn.telerik.com/2017.3.1026/js/kendo.all.min.js"></script>
    <style type="text/css">
        #datepicker{
            border-top: none;
        }
    </style>

</head>
<body>

        <div id="">
            <div class="">

                <h4>Show e-mails from:</h4>
                <input type="text" id="datepicker"  title="datepicker" style="width: 250px" />

               <!--  <h4 style="margin-top: 2em;">Add to archive mail from:</h4>
                <input id="monthpicker" value="November 2011" title="monthpicker" style="width: 100%" /> -->
            </div>
        <script>
            $(document).ready(function() {
                // create DatePicker from input HTML element
                $("#datepicker").kendoDatePicker();                 
                $("#monthpicker").kendoDatePicker({

                    // defines the start view
                    start: "year",

                    // defines when the calendar should return date
                    depth: "year",

                    // display month and year in the input
                    format: "MMMM yyyy",

                    // specifies that DateInput is used for masking the input element
                    dateInput: true
                });



            });
        </script>
        </div>

<!-- <div class="input-append date form_datetime"> -->
   <!--  <input size="16" type="hidden" value="" readonly class="input-append date form_datetime">
    <span class="add-on"><i class="icon-th"></i></span> -->
<!-- </div> -->
 
<style type="text/css">
    

</style>