<?php 
/*
Description: parking employ gate login
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date : 07-04-2018
*/ 
// require_once 'api/global_api.php';
    require_once 'api/parkAreaReg_api.php';
    $response = array();
    
    if(isAvailable(array('prk_admin_id','prk_area_gate_id','prk_user_username','prk_area_gate_name','prk_area_gate_type','prk_area_user_id','token'))){

        if(isEmpty(array('prk_admin_id','prk_area_gate_id','prk_user_username','prk_area_gate_name','prk_area_gate_type','prk_area_user_id','token'))){
            $prk_admin_id = trim($_POST['prk_admin_id']);
            $prk_area_gate_id = trim($_POST['prk_area_gate_id']);
            $prk_user_username = trim($_POST['prk_user_username']);
            $prk_area_gate_name = trim($_POST['prk_area_gate_name']);
            $prk_area_gate_type = trim($_POST['prk_area_gate_type']);
            $prk_area_user_id = trim($_POST['prk_area_user_id']);
            $token = trim($_POST['token']);

            $resp=prk_user_token_check($prk_area_user_id,$prk_admin_id,$token);
            $json = json_decode($resp);
            if($json->status=='1'){
                $respon = prk_user_gate_login($prk_admin_id,$prk_user_username,$prk_area_gate_id,$prk_area_gate_name,$prk_area_gate_type,$prk_area_user_id);
                echo $respon;
            }else{
                $response['status'] = 0; 
                $response['session'] = 0;
                $response['message'] = 'Session Expired Please Login Again';
                echo json_encode($response);
            }            
        }else{
            $response['status'] = 0;
            $response['message'] = 'All Fields Are Mandatory';
            echo json_encode($response);
        }
    }else{
         $response['status'] = 0; 
         $response['message'] = 'Invalid API Call';
         echo json_encode($response);
    }
 
?>