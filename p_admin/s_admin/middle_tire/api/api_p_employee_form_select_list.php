<?php
/*
Description: Parking area account status check.
Developed by: Rakhal Raj Mandal
Created Date: -------
Update date :31-03-2018
*/ 
function p_employee_form_select_list($form_id,$prk_admin_id){
    global $pdoconn;
    $response = array();
    $allplayerdata = array();
    $park = array();
    // $sql = "SELECT * FROM `permission` WHERE `form_id` = '$value['form_id']' AND `user_name` = '$prk_admin_name'";
    $sql ="SELECT `permission_id`,`prk_admin_id`,`user_name`,`form_id` FROM `prk_user_permission` WHERE `prk_admin_id`='$prk_admin_id'  AND `form_id` = '$form_id' AND `active_flag` = '".FLAG_Y."' AND `del_flag` = '".FLAG_N."'";
    $query  = $pdoconn->prepare($sql);
    $query->execute();
    $count=$query->rowCount();
    if($count>0){
        $arr_catagory = $query->fetchAll();
        foreach($arr_catagory as $val){
            $park['permission_id'] = $val['permission_id'];
            $park['prk_admin_id'] = $val['prk_admin_id'];
            $park['user_name'] = $val['user_name'];
            $park['form_id'] = $val['form_id'];
            array_push($allplayerdata, $park);
        }
        $response['status'] = 1;
        $response['message'] = 'Successful';
        $response['from_list'] = $allplayerdata;
    }else{
        $response['status'] = 0;
        $response['message'] = 'List Empty';
    }
    return json_encode($response);
}
?>