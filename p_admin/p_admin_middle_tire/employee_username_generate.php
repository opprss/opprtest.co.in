<?php 
/*
Description: Admi  User Name Generate.
Developed by: Rakhal Raj Mandal
Created Date: 13-Nov-2019
Update date : -------
*/ 
require_once 'api/parkAreaReg_api.php';
if(isAvailable(array('employee_name'))){
    if(isEmpty(array('employee_name'))){
        $employee_name = trim($_POST['employee_name']);
        $response = employee_username_generate($employee_name);
    }else{
        $response['status'] = 0;
        $response['message'] = 'All Fields Are Mandatory';
        $response = json_encode($response);
    }
}else{
    $response['status'] = 0; 
    $response['message'] = 'Invalid API Call';
    $response = json_encode($response);
}
echo ($response);
?>